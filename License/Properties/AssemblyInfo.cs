using System;
using System.Diagnostics;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;
using System.Web;

[assembly: CLSCompliant(true)]
[assembly: AssemblyDescription("Licensing support for .NET")]
//[assembly: Debuggable(DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints)]
//[assembly: AllowPartiallyTrustedCallers]
[assembly: AssemblyTitle("DeployLX® Licensing Runtime v5.0 [.NET 4.0] Release")]
[assembly: AssemblyFileVersion("5.0.2000.5827")]
[assembly: Guid("4B2F67C1-DE43-481E-A7EC-FCF4A8A61C09")]
[assembly: CompilationRelaxations(8)]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows = true)]
[assembly: AssemblyProduct("DeployLX® Licensing v5.0 Release")]
[assembly: ComVisible(false)]
//[assembly: AssemblyConfiguration("Release")]
[assembly: AssemblyCompany("XHEO® INC")]
[assembly: AssemblyCopyright("© 2002-2012 XHEO® INC. All rights reserved.")]
[assembly: AssemblyTrademark("XHEO, DeployLX and DeployLX® Licensing are either trademarks or registered trademarks of XHEO INC.")]
[assembly: SatelliteContractVersion("5.0.0.0")]
[assembly: SecurityPermission(SecurityAction.RequestMinimum, UnmanagedCode = true, Execution = true, ControlAppDomain = true, ControlThread = true, SerializationFormatter = true)]
[assembly: EnvironmentPermission(SecurityAction.RequestMinimum, Unrestricted = true)]
[assembly: RegistryPermission(SecurityAction.RequestMinimum, Read = "HKEY_CURRENT_USER", Write = "HKEY_CURRENT_USER")]
[assembly: SecurityPermission(SecurityAction.RequestMinimum, SkipVerification = true)]
[assembly: PermissionSet(SecurityAction.RequestOptional, Name = "FullTrust", Unrestricted = true)]
[assembly: IsolatedStorageFilePermission(SecurityAction.RequestOptional, UserQuota = 1048576L)]
[assembly: SecurityPermission(SecurityAction.RequestOptional, Unrestricted = true)]
[assembly: FileIOPermission(SecurityAction.RequestOptional, Unrestricted = true)]
[assembly: AspNetHostingPermission(SecurityAction.RequestOptional, Level = AspNetHostingPermissionLevel.Minimal)]
[assembly: RegistryPermission(SecurityAction.RequestOptional, Read = "HKEY_LOCAL_MACHINE", Write = "HKEY_LOCAL_MACHINE")]
[assembly: UIPermission(SecurityAction.RequestOptional, Clipboard = UIPermissionClipboard.OwnClipboard, Window = UIPermissionWindow.AllWindows)]
[assembly: AssemblyVersion("5.0.2000.0")]
