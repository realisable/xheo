namespace DeployLX.Licensing.v5
{
	public enum CodeAlgorithm
	{
		NotSet,
		SerialNumber = 5,
		ActivationCode,
		Simple,
		Basic,
		Advanced
	}
}
