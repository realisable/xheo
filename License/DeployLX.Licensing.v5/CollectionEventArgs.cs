using System;
using System.ComponentModel;

namespace DeployLX.Licensing.v5
{
	public class CollectionEventArgs : EventArgs
	{
		private object _element;

		private CollectionChangeAction _action;

		public object Element
		{
			get
			{
				return _element;
			}
			set
			{
				_element = value;
			}
		}

		public CollectionChangeAction Action
		{
			get
			{
				return _action;
			}
			set
			{
				_action = value;
			}
		}

		public CollectionEventArgs()
		{
		}

		public CollectionEventArgs(CollectionChangeAction action, object element)
		{
			_element = element;
			_action = action;
		}
	}
}
