using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	public class InfoPanel : SuperFormPanel
	{
		private IContainer components;

		private LabelOrBrowserControl _infoLabel;

		private Uri _additionalInfoUri;

		public InfoPanel(string message, Uri additionalInfoUri)
			: base(null)
		{
			Check.NotNull(message, "message");
			InitializeComponent();
			base.DockPadding.All = 8;
			base.DockPadding.Bottom = 32;
			_additionalInfoUri = additionalInfoUri;
			_infoLabel.Text = message;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			_infoLabel = new LabelOrBrowserControl();
			base.SuspendLayout();
			_infoLabel.BackColor = Color.Transparent;
			_infoLabel.Dock = DockStyle.Fill;
			_infoLabel.Location = new Point(8, 8);
			_infoLabel.Name = "_infoLabel";
			_infoLabel.Size = new Size(674, 381);
			_infoLabel.TabIndex = 0;
			base.Controls.Add(_infoLabel);
			base.Name = "InfoPanel";
			base.ResumeLayout(false);
		}

		protected internal override void InitializePanel()
		{
			Button button = base.AddBottomButton("#UI_OK");
			button.Click += btn_Click;
			if (_additionalInfoUri != (Uri)null)
			{
				Label label = base.AddBottomLabel(_additionalInfoUri.ToString(), _additionalInfoUri.ToString(), Images.WebLink_png);
				label.Click += ll_LinkClicked;
			}
			button.Focus();
			base._superForm.AcceptButton = button;
			base._superForm.CancelButton = button;
		}

		private void ll_LinkClicked(object sender, EventArgs e)
		{
			ThemeLabel themeLabel = (ThemeLabel)sender;
			if (Toolbox.IsResourceAddress(themeLabel.Url))
			{
				base.ShowPanel(new InfoPanel(themeLabel.Url, null), null);
			}
			else
			{
				SharedToolbox.OpenUrl(themeLabel.Url, true);
			}
		}

		private void btn_Click(object sender, EventArgs e)
		{
			base.Return(FormResult.Success);
		}

		protected internal override void UpdateFromTheme()
		{
			base.UpdateFromTheme();
			_infoLabel.Font = base._superForm.MediumFont;
			_infoLabel.ForeColor = base.GetThemeColor(Color.White, "Text");
		}
	}
}
