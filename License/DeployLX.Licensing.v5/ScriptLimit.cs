using Microsoft.CSharp;
using Microsoft.VisualBasic;
using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.IO;
using System.Reflection;
using System.Text;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	[LimitEditor("ScriptLimitEditor", IconResource = "asmres://DeployLX.Licensing.DlxAddIn.v5/DeployLX.Licensing.DlxAddIn.v5.Resources.Limits.Script.png")]
	public sealed class ScriptLimit : SuperFormLimit
	{
		private Type _compiledScript;

		private string _precompiledSource;

		private MethodInfo _validateInvoke;

		private MethodInfo _grantedInvoke;

		private string _language;

		private string _validateScript;

		private string _grantedScript;

		private bool _shouldPreCompile;

		private LicenseValuesDictionary _properties = new LicenseValuesDictionary();

		public string Language
		{
			get
			{
				return _language;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_language != value)
				{
					if (value != null)
					{
						switch (value.ToUpper())
						{
						default:
							throw new SecureLicenseException("E_InvalidScriptLanguage");
						case "CS":
						case "C#":
						case "CSHARP":
						case "VB":
						case "VB.NET":
						case "VISUAL BASIC":
							break;
						}
					}
					_language = value;
					base.OnChanged("Language");
				}
			}
		}

		public string ValidateScript
		{
			get
			{
				return _validateScript;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_validateScript != value)
				{
					CheckInvokeSignature(value);
					_validateScript = value;
					base.OnChanged("ValidateScript");
					_precompiledSource = null;
					_compiledScript = null;
				}
			}
		}

		public string GrantedScript
		{
			get
			{
				return _grantedScript;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_grantedScript != value)
				{
					CheckInvokeSignature(value);
					_grantedScript = value;
					base.OnChanged("GrantedScript");
					_precompiledSource = null;
					_compiledScript = null;
				}
			}
		}

		public bool ShouldPreCompile
		{
			get
			{
				return _shouldPreCompile;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_shouldPreCompile != value)
				{
					_shouldPreCompile = value;
					base.OnChanged("ShouldPreCompile");
				}
			}
		}

		public LicenseValuesDictionary Properties
		{
			get
			{
				return _properties;
			}
		}

		public override string Description
		{
			get
			{
				return SR.GetString("M_ScriptLimitDescription");
			}
		}

		public override string Name
		{
			get
			{
				return "Script";
			}
		}

		public ScriptLimit()
		{
			_properties.Changed += _properties_Changed;
		}

		private void _properties_Changed(object sender, CollectionEventArgs e)
		{
			base.OnCollectionChanged(sender, "Properties", e);
		}

		private void CheckInvokeSignature(string value)
		{
			if (value == null)
			{
				return;
			}
			if (!value.StartsWith("invoke::"))
			{
				return;
			}
			int num = value.IndexOf(',');
			if (num == -1)
			{
				throw new SecureLicenseException("E_InvalidInvokeSignature");
			}
			int num2 = value.LastIndexOf('.', num);
			if (num2 != -1)
			{
				return;
			}
			throw new SecureLicenseException("E_InvalidInvokeSignature");
		}

		private void MakeCompiledScript(bool preCompile)
		{
			if (_compiledScript == null)
			{
				if (_validateScript != null && !_validateScript.StartsWith("invoke::"))
				{
					goto IL_0045;
				}
				if (_grantedScript != null && !_grantedScript.StartsWith("invoke::"))
				{
					goto IL_0045;
				}
				_compiledScript = typeof(object);
			}
			return;
			IL_0045:
			if (!preCompile && _precompiledSource != null && _shouldPreCompile)
			{
				try
				{
					byte[] array = Convert.FromBase64String(_precompiledSource);
					string path = null;
					int num = 0;
					while (true)
					{
						if (num >= 4)
						{
							break;
						}
						try
						{
							switch (num)
							{
							case 0:
								path = Path.GetTempFileName();
								goto default;
							case 1:
								if (AppDomain.CurrentDomain.DynamicDirectory != null)
								{
									path = Path.Combine(AppDomain.CurrentDomain.DynamicDirectory, string.Format("SCRIPT_{0}.dll", base.LimitId));
									goto default;
								}
								break;
							case 2:
								path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), string.Format("SCRIPT_{0}.dll", base.LimitId));
								goto default;
							case 3:
								path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.System), string.Format("..\\Temp\\SCRIPT_{0}.dll", base.LimitId));
								goto default;
							default:
								using (FileStream fileStream = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.None))
								{
									fileStream.Write(array, 0, array.Length);
									fileStream.Flush();
									fileStream.Close();
								}
								goto end_IL_0071;
							}
						}
						catch
						{
						}
						num++;
						continue;
						continue;
						end_IL_0071:
						break;
					}
					Assembly assembly = Assembly.LoadFile(path);
					_compiledScript = assembly.GetType(string.Format("{0}.SCRIPT_{1}_0", typeof(ScriptLimit).Namespace, base.LimitId), false, true);
					if (_compiledScript == null)
					{
						File.Delete(path);
						goto end_IL_0061;
					}
					return;
					end_IL_0061:;
				}
				catch
				{
				}
			}
			string text = string.Format("SCRIPT_{0}_{1}", base.LimitId, preCompile ? 0L : DateTime.Now.Ticks);
			CodeCompileUnit codeCompileUnit = new CodeCompileUnit();
			CodeDomProvider codeDomProvider = MakeCode(codeCompileUnit, text);
			ICodeCompiler codeCompiler = codeDomProvider.CreateCompiler();
			CompilerParameters compilerParameters = new CompilerParameters(new string[3]
			{
				"System.dll",
				"System.Windows.Forms.dll",
				typeof(ScriptLimit).Assembly.Location
			});
			compilerParameters.GenerateInMemory = (!_shouldPreCompile || !preCompile);
			compilerParameters.IncludeDebugInformation = false;
			CompilerResults compilerResults = codeCompiler.CompileAssemblyFromDom(compilerParameters, codeCompileUnit);
			if (compilerResults.Errors.HasErrors)
			{
				SecureLicenseException ex = new SecureLicenseException("E_CouldNotCompileScript");
				StringBuilder stringBuilder = new StringBuilder();
				foreach (CompilerError error in compilerResults.Errors)
				{
					stringBuilder.Append(error.ErrorText);
					stringBuilder.Append(Environment.NewLine);
				}
				ex.DetailedMessage = stringBuilder.ToString();
				throw ex;
			}
			_compiledScript = compilerResults.CompiledAssembly.GetType(typeof(ScriptLimit).Namespace + '.' + text, true, true);
		}

		private CodeDomProvider MakeCode(CodeCompileUnit code, string typeName)
		{
			CodeDomProvider codeDomProvider = null;
			string text = _language;
			if (text == null || text.Length == 0)
			{
				text = "CSHARP";
			}
			CodeNamespace codeNamespace;
			CodeTypeDeclaration codeTypeDeclaration;
			CodeMemberMethod codeMemberMethod;
			switch (text.ToUpper())
			{
			case "CSHARP":
			case "C#":
			case "CS":
				codeDomProvider = new CSharpCodeProvider();
				goto IL_00cd;
			case "VB":
			case "VISUAL BASIC":
			case "VB.NET":
				codeDomProvider = new VBCodeProvider();
				goto IL_00cd;
			default:
				{
					throw new SecureLicenseException("E_InvalidScriptLanguage");
				}
				IL_00cd:
				codeNamespace = new CodeNamespace(typeof(ScriptLimit).Namespace);
				code.Namespaces.Add(codeNamespace);
				codeTypeDeclaration = new CodeTypeDeclaration(typeName);
				codeNamespace.Types.Add(codeTypeDeclaration);
				codeMemberMethod = null;
				if (_validateScript != null)
				{
					codeMemberMethod = new CodeMemberMethod();
					codeMemberMethod.Name = "Validate";
					codeMemberMethod.Attributes = (MemberAttributes)24579;
					codeMemberMethod.Parameters.Add(new CodeParameterDeclarationExpression(typeof(SecureLicenseContext), "Context"));
					codeMemberMethod.Parameters.Add(new CodeParameterDeclarationExpression(typeof(ScriptLimit), "Limit"));
					codeMemberMethod.Parameters.Add(new CodeParameterDeclarationExpression(typeof(SecureLicense), "License"));
					codeMemberMethod.Statements.Add(new CodeSnippetStatement(_validateScript));
					codeMemberMethod.ReturnType = new CodeTypeReference(typeof(ValidationResult));
					codeTypeDeclaration.Members.Add(codeMemberMethod);
				}
				if (_grantedScript != null)
				{
					codeMemberMethod = new CodeMemberMethod();
					codeMemberMethod.Name = "Granted";
					codeMemberMethod.Attributes = (MemberAttributes)24579;
					codeMemberMethod.Parameters.Add(new CodeParameterDeclarationExpression(typeof(SecureLicenseContext), "Context"));
					codeMemberMethod.Parameters.Add(new CodeParameterDeclarationExpression(typeof(ScriptLimit), "Limit"));
					codeMemberMethod.Parameters.Add(new CodeParameterDeclarationExpression(typeof(SecureLicense), "License"));
					codeMemberMethod.Statements.Add(new CodeSnippetStatement(_grantedScript));
					codeMemberMethod.ReturnType = new CodeTypeReference(typeof(ValidationResult));
					codeTypeDeclaration.Members.Add(codeMemberMethod);
				}
				return codeDomProvider;
			}
		}

		public override ValidationResult Validate(SecureLicenseContext context)
		{
			if (_validateScript != null)
			{
				MakeCompiledScript(false);
				ValidationResult validationResult;
				if (_validateScript.StartsWith("invoke::"))
				{
					if (_validateInvoke == null)
					{
						_validateInvoke = GetInvokeScript(_validateScript);
					}
					validationResult = (ValidationResult)_validateInvoke.Invoke(null, BindingFlags.IgnoreCase | BindingFlags.Static | BindingFlags.Public, null, new object[3]
					{
						context,
						this,
						base.License
					}, null);
				}
				else
				{
					validationResult = (ValidationResult)_compiledScript.GetMethod("Validate").Invoke(null, new object[3]
					{
						context,
						this,
						base.License
					});
				}
				if (validationResult != ValidationResult.Valid)
				{
					return validationResult;
				}
			}
			return base.Validate(context);
		}

		private static MethodInfo GetInvokeScript(string script)
		{
			int num = script.IndexOf(',');
			int num2 = script.LastIndexOf('.', num);
			Type type = TypeHelper.FindType(script.Substring(8, num2 - 8) + "," + script.Substring(num + 1), true);
			string name = script.Substring(num2 + 1, num - num2 - 1);
			MethodInfo method = type.GetMethod(name, BindingFlags.IgnoreCase | BindingFlags.Static | BindingFlags.Public);
			if (method == null)
			{
				throw new SecureLicenseException("E_InvalidInvokeSignature");
			}
			return method;
		}

		public override ValidationResult Granted(SecureLicenseContext context)
		{
			if (_grantedScript != null)
			{
				MakeCompiledScript(false);
				ValidationResult validationResult;
				if (_grantedScript.StartsWith("invoke::"))
				{
					if (_grantedInvoke == null)
					{
						_grantedInvoke = GetInvokeScript(_grantedScript);
					}
					validationResult = (ValidationResult)_grantedInvoke.Invoke(null, BindingFlags.IgnoreCase | BindingFlags.Static | BindingFlags.Public, null, new object[3]
					{
						context,
						this,
						base.License
					}, null);
				}
				else
				{
					validationResult = (ValidationResult)_compiledScript.GetMethod("Granted").Invoke(null, new object[3]
					{
						context,
						this,
						base.License
					});
				}
				if (validationResult != ValidationResult.Valid)
				{
					return validationResult;
				}
			}
			return base.Validate(context);
		}

		protected override bool ReadFromXml(XmlReader reader)
		{
			_validateScript = null;
			_grantedScript = null;
			_properties.Clear();
			_compiledScript = null;
			_shouldPreCompile = (reader.GetAttribute("shouldPreCompile") == "true");
			_language = reader.GetAttribute("language");
			if (reader.IsEmptyElement)
			{
				reader.Read();
				return true;
			}
			reader.Read();
			while (!reader.EOF)
			{
				if (reader.IsStartElement())
				{
					switch (reader.Name)
					{
					case "Compiled":
						reader.Read();
						_precompiledSource = reader.ReadString();
						reader.Read();
						break;
					case "Limit":
						if (base.ReadChildLimit(reader))
						{
							break;
						}
						return false;
					case "Properties":
						_properties.ReadFromXml(reader);
						break;
					case "Granted":
						reader.Read();
						_grantedScript = reader.ReadString();
						reader.Read();
						break;
					case "Validate":
						reader.Read();
						_validateScript = reader.ReadString();
						reader.Read();
						break;
					default:
						reader.Read();
						break;
					}
					continue;
				}
				reader.Read();
				break;
			}
			return true;
		}

		protected override bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			if (_language != null)
			{
				writer.WriteAttributeString("language", _language);
			}
			if (_shouldPreCompile)
			{
				writer.WriteAttributeString("shouldPreCompile", "true");
			}
			if (_validateScript != null)
			{
				writer.WriteStartElement("Validate");
				writer.WriteCData(_validateScript);
				writer.WriteEndElement();
			}
			if (_grantedScript != null)
			{
				writer.WriteStartElement("Granted");
				writer.WriteCData(_grantedScript);
				writer.WriteEndElement();
			}
			if (_properties.Count > 0)
			{
				_properties.WriteToXml(writer, "Properties");
			}
			if (_shouldPreCompile)
			{
				if (_precompiledSource == null && signing == LicenseSaveType.Signing)
				{
					MakeCompiledScript(true);
					using (FileStream fileStream = new FileStream(_compiledScript.Assembly.Location, FileMode.Open, FileAccess.Read, FileShare.Read))
					{
						byte[] array = new byte[fileStream.Length];
						fileStream.Read(array, 0, array.Length);
						_precompiledSource = Convert.ToBase64String(array);
					}
				}
				writer.WriteStartElement("Compiled");
				writer.WriteString(_precompiledSource);
				writer.WriteEndElement();
			}
			return true;
		}

		protected override SuperFormPanel CreatePanel(string pageId, SecureLicenseContext context)
		{
			if (base.CustomForm == null)
			{
				throw new SecureLicenseException("E_MustDefineCustomGui");
			}
			return base.CreatePanel(pageId, context);
		}
	}
}
