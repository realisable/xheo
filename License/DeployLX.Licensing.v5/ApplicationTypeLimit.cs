using System.Collections.Generic;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[LimitEditor("ApplicationTypeLimitEditor", IconResource = "asmres://DeployLX.Licensing.DlxAddIn.v5/DeployLX.Licensing.DlxAddIn.v5.Resources.Limits.ApplicationType.png", Category = "State and Filters")]
	public class ApplicationTypeLimit : Limit
	{
		public class ValidationStateIds
		{
			public const string WindowsApplication = "Windows Application";

			public const string ConsoleApplication = "Console Application";

			public const string AspNet = "ASP.NET";

			public const string XmlWebService = "XML Web Service";

			public const string WindowsService = "Windows Service";
		}

		private static readonly List<string> _validationStates;

		private string _state;

		public override string Description
		{
			get
			{
				return SR.GetString("M_ApplicationTypeLimitDescription");
			}
		}

		public override string Name
		{
			get
			{
				return "Application Type";
			}
		}

		public override string State
		{
			get
			{
				return _state ?? "Windows Application";
			}
		}

		public override IList<string> ValidationStates
		{
			get
			{
				return _validationStates;
			}
		}

		static ApplicationTypeLimit()
		{
			_validationStates = new List<string>();
			_validationStates.Add("Windows Application");
			_validationStates.Add("Console Application");
			_validationStates.Add("ASP.NET");
			_validationStates.Add("XML Web Service");
			_validationStates.Add("Windows Service");
		}

		public override ValidationResult Validate(SecureLicenseContext context)
		{
			_state = DetermineApplicationType(context);
			return base.Validate(context);
		}

		private static string DetermineApplicationType(SecureLicenseContext context)
		{
			if (context.IsServiceRequest)
			{
				return "Windows Service";
			}
			if (context.IsWebRequest)
			{
				if (context.GetWebPage() != null)
				{
					return "ASP.NET";
				}
				return "XML Web Service";
			}
			if (!context.IsConsoleRequest)
			{
				return "Windows Application";
			}
			return "Console Application";
		}

		protected override bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			return true;
		}

		protected override bool ReadFromXml(XmlReader reader)
		{
			return base.ReadChildLimits(reader);
		}
	}
}
