using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	public class ActivationInfoPanel : SplitPanel
	{
		private bool _showRequirementNotice;

		private ShadowLabel _summaryNotice;

		private ShadowLabel _requiredSummary;

		private ShadowLabel _requiredTitle;

		private BufferedPictureBox _requiredIcon;

		private ShadowLabel _activateTitle;

		private int _bulletTop;

		public ActivationInfoPanel(ActivationLimit limit, bool showRequirementNotice)
			: base(limit)
		{
			InitializeComponent();
			_requiredIcon.Visible = showRequirementNotice;
			_requiredSummary.Visible = showRequirementNotice;
			_showRequirementNotice = showRequirementNotice;
			_requiredTitle.Visible = showRequirementNotice;
		}

		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			_activateTitle = new ShadowLabel();
			_summaryNotice = new ShadowLabel();
			_requiredSummary = new ShadowLabel();
			_requiredTitle = new ShadowLabel();
			_requiredIcon = new BufferedPictureBox();
			base.BodyPanel.SuspendLayout();
			((ISupportInitialize)_requiredIcon).BeginInit();
			base.SuspendLayout();
			base.SidePanel.Location = new Point(0, 0);
			base.SidePanel.Size = new Size(170, 421);
			base.BodyPanel.Controls.Add(_requiredIcon);
			base.BodyPanel.Controls.Add(_requiredSummary);
			base.BodyPanel.Controls.Add(_summaryNotice);
			base.BodyPanel.Controls.Add(_requiredTitle);
			base.BodyPanel.Controls.Add(_activateTitle);
			base.BodyPanel.Location = new Point(170, 0);
			base.BodyPanel.Size = new Size(514, 421);
			_activateTitle.Location = new Point(14, 16);
			_activateTitle.Name = "_activateTitle";
			_activateTitle.Size = new Size(482, 34);
			_activateTitle.TabIndex = 1;
			_activateTitle.Text = "#UI_AboutActivationTitle";
			_activateTitle.ThemeColor = new string[2]
			{
				"PanelTitleText",
				"Text"
			};
			_summaryNotice.Location = new Point(16, 50);
			_summaryNotice.Name = "_summaryNotice";
			_summaryNotice.Size = new Size(482, 151);
			_summaryNotice.TabIndex = 2;
			_summaryNotice.ThemeFont = ThemeFont.Medium;
			_requiredSummary.Location = new Point(38, 356);
			_requiredSummary.Name = "_requiredSummary";
			_requiredSummary.Size = new Size(463, 52);
			_requiredSummary.TabIndex = 2;
			_requiredTitle.Location = new Point(38, 337);
			_requiredTitle.Name = "_requiredTitle";
			_requiredTitle.Size = new Size(463, 19);
			_requiredTitle.TabIndex = 1;
			_requiredTitle.Text = "#UI_AboutActivationRequiredTitle";
			_requiredIcon.BackColor = Color.Transparent;
			_requiredIcon.BackgroundImageLayout = ImageLayout.None;
			_requiredIcon.Location = new Point(16, 340);
			_requiredIcon.Name = "_requiredIcon";
			_requiredIcon.OffsetBackgroundImage = false;
			_requiredIcon.Size = new Size(16, 16);
			_requiredIcon.TabIndex = 3;
			_requiredIcon.TabStop = false;
			base.AutoScaleDimensions = new SizeF(96f, 96f);
			base.DefaultIconName = "DefaultInfoIcon,DefaultIcon";
			base.Name = "ActivationInfoPanel";
			base.BodyPanel.ResumeLayout(false);
			((ISupportInitialize)_requiredIcon).EndInit();
			base.ResumeLayout(false);
		}

		protected internal override void InitializePanel()
		{
			Button button = base.AddBottomButton("UI_OK");
			button.Click += btn_Click;
			base.AddLicenseAgreementLink();
		}

		protected internal override void LoadPanel(SecureLicenseContext context)
		{
			_summaryNotice.Text = SR.GetString("UI_AboutActivationSummary", context.SupportInfo.Product);
			ActivationLimit activationLimit = base.Limit as ActivationLimit;
			if (_showRequirementNotice)
			{
				_requiredIcon.Image = Images.Notice_png;
				_requiredSummary.Text = ActivationLimit.MakeRequiredText(activationLimit, context);
			}
			AddBullet(SR.GetString("UI_AboutActivationPrivacy"));
			AddBullet(SR.GetString("UI_AboutActivationHardware"));
			AddBullet(SR.GetString("UI_AboutActivationHardwareMajor"));
			if (activationLimit.CanDeactivate)
			{
				AddBullet(SR.GetString("UI_AboutActivationDeactivate"));
			}
		}

		private void AddBullet(string text)
		{
			ShadowLabel shadowLabel = new ShadowLabel();
			shadowLabel.Image = Images.Bullet_png;
			shadowLabel.BackColor = Color.Transparent;
			shadowLabel.ForeColor = base.GetThemeColor(Color.White, "Text");
			shadowLabel.AutoSize = true;
			shadowLabel.Text = text;
			shadowLabel.TextAlign = ContentAlignment.TopLeft;
			shadowLabel.TextOffset = new Point(0, -1);
			ShadowLabel shadowLabel2 = shadowLabel;
			shadowLabel2.SetBounds(16, _summaryNotice.Bottom + _bulletTop, base.BodyPanel.Width - 62, 0);
			base.BodyPanel.Controls.Add(shadowLabel2);
			if (shadowLabel2.Width > base.BodyPanel.Width - 62)
			{
				int height = shadowLabel2.Height;
				shadowLabel2.AutoSize = false;
				shadowLabel2.Height = height * 2;
				shadowLabel2.Width = base.BodyPanel.Width - 62;
			}
			_bulletTop += shadowLabel2.Height + 4;
		}

		private void btn_Click(object sender, EventArgs e)
		{
			base.Return(FormResult.Success);
		}

		protected internal override void UpdateFromTheme()
		{
			base.UpdateFromTheme();
			_activateTitle.Font = base.SuperForm.TitleFont;
			_requiredTitle.Font = base._superForm.HeaderFont;
		}
	}
}
