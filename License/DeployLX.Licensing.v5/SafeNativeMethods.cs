using System;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Principal;
using System.Text;
using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	[SuppressUnmanagedCodeSecurity]
	[SecuritySafeCritical]
	internal sealed class SafeNativeMethods
	{
		public struct EXPLICIT_ACCESS
		{
			public int AccessPermissions;

			public int AccessMode;

			public int Inheritance;

			public TRUSTEE Trustee;
		}

		public struct TRUSTEE
		{
			public IntPtr MultipleTrustee;

			public int MultipleTrusteeOperation;

			public int TrusteeForm;

			public int TrusteeType;

			public IntPtr Name;
		}

		public struct UNICODE_STRING
		{
			public ushort Length;

			public ushort MaximumLength;

			public IntPtr Buffer;
		}

		public struct OBJECT_ATTRIBUTES
		{
			public uint Length;

			public IntPtr RootDirectory;

			public IntPtr ObjectName;

			public uint Attributes;

			public IntPtr SecurityDescriptor;

			public IntPtr SecurityQualityOfService;
		}

		public class USWrapper : IDisposable
		{
			public UNICODE_STRING Str;

			private GCHandle dataHandle;

			private GCHandle handle;

			public USWrapper(string value)
			{
				Str = default(UNICODE_STRING);
				Str.Length = (ushort)(value.Length * 2);
				Str.MaximumLength = Str.Length;
				dataHandle = GCHandle.Alloc(value, GCHandleType.Pinned);
				Str.Buffer = dataHandle.AddrOfPinnedObject();
			}

			~USWrapper()
			{
				Free();
			}

			public IntPtr GetPtr()
			{
				if (!handle.IsAllocated)
				{
					handle = GCHandle.Alloc(Str, GCHandleType.Pinned);
				}
				return handle.AddrOfPinnedObject();
			}

			public void Free()
			{
				if (handle.IsAllocated)
				{
					handle.Free();
				}
				if (dataHandle.IsAllocated)
				{
					dataHandle.Free();
				}
			}

			public void Dispose()
			{
				GC.SuppressFinalize(this);
				Free();
			}
		}

		public class OAWrapper : IDisposable
		{
			public OBJECT_ATTRIBUTES OA;

			private GCHandle ptrHandle;

			private USWrapper usname;

			private IntPtr sdptr;

			public OAWrapper()
			{
				OA = default(OBJECT_ATTRIBUTES);
			}

			~OAWrapper()
			{
				Free();
			}

			public void Initialize(string name, uint attributes, IntPtr rootDirectory, IntPtr securityDescriptor)
			{
				Free();
				OA.Length = (uint)Marshal.SizeOf(OA);
				OA.RootDirectory = rootDirectory;
				OA.Attributes = attributes;
				usname = new USWrapper(name);
				OA.ObjectName = usname.GetPtr();
				if (securityDescriptor == new IntPtr(-1))
				{
					string ssd = string.Format("O:{0}G:S-1-3-1D:(A;;GA;;;S-1-1-0)", GetCurrentUserSid());
					ulong num;
					ConvertStringSecurityDescriptorToSecurityDescriptor(ssd, 1, out sdptr, out num);
					securityDescriptor = sdptr;
				}
				OA.SecurityDescriptor = securityDescriptor;
				OA.SecurityQualityOfService = IntPtr.Zero;
			}

			public IntPtr GetPtr()
			{
				ptrHandle = GCHandle.Alloc(OA, GCHandleType.Pinned);
				return ptrHandle.AddrOfPinnedObject();
			}

			public void Free()
			{
				if (ptrHandle.IsAllocated)
				{
					ptrHandle.Free();
				}
				if (usname != null)
				{
					usname.Dispose();
					usname = null;
				}
				if (sdptr != IntPtr.Zero)
				{
					Marshal.FreeHGlobal(sdptr);
					sdptr = IntPtr.Zero;
				}
			}

			public void Dispose()
			{
				GC.SuppressFinalize(this);
				Free();
			}
		}

		private const int WM_SETCURSOR = 32;

		private const int IDC_HAND = 32649;

		public static readonly bool IsWin64 = IntPtr.Size == 8;

		public static readonly bool IsOsWin64 = ResolveIsOsWin64();

		private SafeNativeMethods()
		{
		}

		private static bool ResolveIsOsWin64()
		{
			return Environment.GetEnvironmentVariable("PROCESSOR_ARCHITEW6432") != null;
		}

		[DllImport("kernel32", CharSet = CharSet.Unicode, SetLastError = true)]
		public static extern IntPtr LoadLibraryExW([MarshalAs(UnmanagedType.LPWStr)] string filename, IntPtr hFile, uint flags);

		[DllImport("kernel32", CharSet = CharSet.Unicode, SetLastError = true)]
		public static extern IntPtr GetProcAddress(IntPtr module, string name);

		[DllImport("kernel32", CharSet = CharSet.Unicode, SetLastError = true)]
		public static extern IntPtr GetModuleHandle(string name);

		[DllImport("kernel32", CharSet = CharSet.Unicode, SetLastError = true)]
		public static extern int FreeLibrary(IntPtr hModule);

		[DllImport("kernel32", CharSet = CharSet.Unicode, SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool DeviceIoControl(IntPtr handle, int ioControlCode, IntPtr inBuffer, int inBufferSize, [Out] byte[] outBuffer, int outBufferSize, ref int bytesReturned, IntPtr overlapped);

		[DllImport("kernel32", CharSet = CharSet.Unicode, SetLastError = true)]
		public static extern int QueryDosDevice(string deviceName, StringBuilder targetPath, int max);

		[DllImport("iphlpapi", CharSet = CharSet.Unicode)]
		public static extern int GetAdaptersInfo([Out] byte[] pAdapterInfo, ref uint pOutBufLen);

		[DllImport("kernel32", CharSet = CharSet.Unicode)]
		public static extern void GlobalMemoryStatus(byte[] memstat);

		[DllImport("kernel32", CharSet = CharSet.Unicode, SetLastError = true)]
		public static extern int GlobalMemoryStatusEx(byte[] memstat);

		[DllImport("ntdll")]
		public static extern int NtQuerySystemInformation(int infoClass, byte[] info, int infoLength, out int returnInfoLength);

		[DllImport("advapi32", CharSet = CharSet.Unicode, SetLastError = true)]
		public static extern int SetEntriesInAcl(int countOfExplicitEntries, [In] [Out] ref EXPLICIT_ACCESS explicitEntries, IntPtr oldAcl, out IntPtr newAcl);

		[DllImport("advapi32", CharSet = CharSet.Unicode, SetLastError = true)]
		public static extern int SetNamedSecurityInfo(string name, int objectType, uint securityInfo, IntPtr sidOwner, IntPtr sidGroup, IntPtr dacl, IntPtr sacl);

		[DllImport("advapi32", CharSet = CharSet.Unicode, SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool DeleteAce(IntPtr pacl, int index);

		[DllImport("advapi32", CharSet = CharSet.Unicode, SetLastError = true)]
		public static extern int GetNamedSecurityInfo(string name, int objectType, int securityInfo, [Out] IntPtr sidOwner, [Out] IntPtr sidGroup, out IntPtr dacl, [Out] IntPtr sacl, out IntPtr securityDescriptor);

		[DllImport("kernel32", CharSet = CharSet.Unicode, SetLastError = true)]
		public static extern IntPtr CreateFile(string fileName, uint desiredAccess, uint shareMode, IntPtr securityAttributes, uint creationDisposition, uint flagsAndAttributes, IntPtr templateFile);

		[DllImport("ntdll", CharSet = CharSet.Unicode, SetLastError = true)]
		public static extern uint NtFsControlFile(IntPtr handle, IntPtr eventHandle, IntPtr apcRoutine, IntPtr apcContext, [Out] byte[] IoStatusBlock, uint controlCode, [In] [Out] byte[] inputBuffer, uint inputBufferLength, [Out] ulong[] ouputBuffer, uint outputBufferLength);

		[DllImport("ntdll", CharSet = CharSet.Unicode)]
		public static extern int NtCreateKey([In] [Out] ref IntPtr handle, uint desiredAccess, IntPtr objectAttributes, int titleIndex, IntPtr className, int createOptions, [In] [Out] ref int disposition);

		[DllImport("ntdll", CharSet = CharSet.Unicode)]
		public static extern int NtSetSecurityObject(IntPtr handle, int securityInformation, IntPtr descriptor);

		[DllImport("ntdll", CharSet = CharSet.Unicode)]
		public static extern int NtSetValueKey(IntPtr handle, IntPtr name, int titleIndex, int type, byte[] data, int dataSize);

		[DllImport("ntdll", CharSet = CharSet.Unicode)]
		public static extern int NtQueryValueKey(IntPtr handle, IntPtr name, int informationClass, [Out] byte[] data, int length, out int resultLength);

		[DllImport("ntdll", CharSet = CharSet.Unicode)]
		public static extern int NtEnumerateValueKey(IntPtr handle, int index, int informationClass, [Out] byte[] data, int length, out int resultLength);

		[DllImport("ntdll", CharSet = CharSet.Unicode)]
		public static extern int NtOpenKey(out IntPtr handle, uint desiredAccess, IntPtr objectAttributes);

		[DllImport("ntdll", CharSet = CharSet.Unicode)]
		public static extern int NtDeleteValueKey(IntPtr handle, IntPtr name);

		[DllImport("ntdll", CharSet = CharSet.Unicode)]
		public static extern int NtDeleteKey(IntPtr handle);

		[DllImport("kernel32")]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool CloseHandle(IntPtr handle);

		[DllImport("advapi32", SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool GetTokenInformation(IntPtr token, int tokenInformationClass, IntPtr tokenInformation, int tokenInformationLength, ref int returnLength);

		[DllImport("advapi32", SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool ConvertSidToStringSid(IntPtr sid, ref IntPtr strSid);

		[DllImport("advapi32", BestFitMapping = false, CharSet = CharSet.Auto, SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool ConvertStringSidToSid([MarshalAs(UnmanagedType.LPTStr)] string stringSid, ref IntPtr sid);

		[DllImport("user32", CharSet = CharSet.Unicode, SetLastError = true)]
		public static extern IntPtr SendMessage(IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam);

		[DllImport("kernel32", CharSet = CharSet.Unicode, SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool GetVersionEx(IntPtr lpVersionInfo);

		[DllImport("kernel32", CharSet = CharSet.Unicode, SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool VirtualProtect([In] byte[] bytes, IntPtr size, int newProtect, out int oldProtect);

		[DllImport("kernel32", CharSet = CharSet.Unicode, SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool GetSystemPowerStatus(byte[] lpSystemPowerStatus);

		[DllImport("kernel32", CharSet = CharSet.Unicode, SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool GetVolumeInformation([In] string lpRootPathName, [Out] StringBuilder volumeNameBuffer, int volumeNameSize, out int volumeSerialNumber, out int maximumComponentLength, out int fileSystemFlags, [Out] StringBuilder fileSystemNameBuffer, int fileSystemNameSize);

		[DllImport("kernel32", CharSet = CharSet.Unicode, SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool GetDiskFreeSpaceEx([In] string lpDirectoryName, out long lpFreeBytesAvailable, out long lpTotalNumberOfBytes, out long lpTotalNumberOfFreeBytes);

		[DllImport("kernel32", CharSet = CharSet.Unicode, SetLastError = true)]
		public static extern int GetDriveType(string path);

		[DllImport("kernel32", CharSet = CharSet.Unicode, SetLastError = true)]
		public static extern bool Wow64DisableWow64FsRedirection(ref IntPtr oldValue);

		[DllImport("kernel32", CharSet = CharSet.Unicode, SetLastError = true)]
		public static extern bool Wow64RevertWow64FsRedirection(IntPtr oldValue);

		[DllImport("gdi32")]
		public static extern IntPtr SelectObject(IntPtr hdc, IntPtr obj);

		[DllImport("gdi32")]
		public static extern int SetBkMode(IntPtr hdc, int mode);

		[DllImport("user32", CharSet = CharSet.Unicode, ExactSpelling = true)]
		public static extern int GetSystemMetrics(int nIndex);

		[DllImport("gdi32")]
		public static extern IntPtr CreatePatternBrush(IntPtr bitmap);

		[DllImport("gdi32")]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool DeleteObject(IntPtr obj);

		[DllImport("gdi32", CharSet = CharSet.Unicode)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool TextOut(IntPtr hdc, int x, int y, string text, int textLength);

		[DllImport("gdi32")]
		public static extern int SetTextColor(IntPtr hdc, int color);

		[DllImport("gdi32", CharSet = CharSet.Unicode)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool GetTextExtentPoint32(IntPtr hdc, string text, int textLength, [MarshalAs(UnmanagedType.LPArray, SizeConst = 8)] byte[] size);

		[DllImport("user32", SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool MessageBeep(int type);

		[DllImport("user32")]
		public static extern int FillRect(IntPtr hdc, byte[] rect, IntPtr hbr);

		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		private static extern IntPtr LoadCursor(IntPtr hInstance, int lpCursorName);

		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		private static extern IntPtr SetCursor(IntPtr hCursor);

		public static bool PreFilterMessage(ref Message m)
		{
			if (m.HWnd == IntPtr.Zero)
			{
				return false;
			}
			if (m.Msg != 32)
			{
				return false;
			}
			Control control = Control.FromHandle(m.HWnd);
			if (control != null && !(control.Cursor != Cursors.Hand))
			{
				SetCursor(LoadCursor(IntPtr.Zero, 32649));
				m.Result = IntPtr.Zero;
				return true;
			}
			return false;
		}

		[DllImport("advapi32", BestFitMapping = false, CharSet = CharSet.Unicode)]
		[return: MarshalAs(UnmanagedType.Bool)]
		internal static extern bool ConvertStringSecurityDescriptorToSecurityDescriptor([In] [MarshalAs(UnmanagedType.LPTStr)] string ssd, int revision, out IntPtr sd, out ulong size);

		public static string GetCurrentUserSid()
		{
			byte[] array = new byte[256];
			GCHandle gCHandle = GCHandle.Alloc(array, GCHandleType.Pinned);
			try
			{
				WindowsIdentity current = WindowsIdentity.GetCurrent();
				int num = 0;
				if (!GetTokenInformation(current.Token, 1, gCHandle.AddrOfPinnedObject(), array.Length, ref num))
				{
					Marshal.ThrowExceptionForHR(Marshal.GetHRForLastWin32Error());
				}
				IntPtr zero = IntPtr.Zero;
				try
				{
					IntPtr sid = Marshal.ReadIntPtr(gCHandle.AddrOfPinnedObject());
					ConvertSidToStringSid(sid, ref zero);
					return Marshal.PtrToStringAnsi(zero);
				}
				finally
				{
					if (zero != IntPtr.Zero)
					{
						Marshal.FreeHGlobal(zero);
					}
				}
			}
			finally
			{
				gCHandle.Free();
			}
		}

		[DllImport("advapi32", BestFitMapping = false, CharSet = CharSet.Auto, SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool LogonUser([MarshalAs(UnmanagedType.LPTStr)] string lpszUserName, [MarshalAs(UnmanagedType.LPTStr)] string lpszDomain, [MarshalAs(UnmanagedType.LPTStr)] string lpszPassword, int dwLogonType, int dwLogonProvider, ref IntPtr phToken);

		[DllImport("advapi32", CharSet = CharSet.Auto, SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool DuplicateToken(IntPtr hToken, int impersonationLevel, ref IntPtr hNewToken);

		[DllImport("shell32", CharSet = CharSet.Unicode)]
		public static extern int SHGetFolderPath(IntPtr hwndOwner, int nFolder, IntPtr hToken, int dwFlags, StringBuilder pszPath);

		[DllImport("setupapi.dll", CharSet = CharSet.Unicode, SetLastError = true)]
		public static extern int CM_Locate_DevNode(out IntPtr devInst, string deviceId, int flags);
	}
}
