using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	[LimitEditor("TypeLimitEditor", Category = "Extendable Limits", MiniType = "TypeLimitEditorControl", IconResource = "asmres://DeployLX.Licensing.DlxAddIn.v5/DeployLX.Licensing.DlxAddIn.v5.Resources.Limits.Type.png")]
	public class TypeLimit : FeatureLimit
	{
		private bool _checkEntireStack;

		private bool _forceStrongNameVerification;

		public override string Description
		{
			get
			{
				return SR.GetString("M_TypeLimitDescription");
			}
		}

		public override string Name
		{
			get
			{
				return "Type";
			}
		}

		public bool CheckEntireStack
		{
			get
			{
				return _checkEntireStack;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_checkEntireStack != value)
				{
					_checkEntireStack = value;
					base.OnChanged("CheckEntireStack");
				}
			}
		}

		public bool ForceStrongNameVerification
		{
			get
			{
				return _forceStrongNameVerification;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_forceStrongNameVerification != value)
				{
					_forceStrongNameVerification = value;
					base.OnChanged("ForceStrongNameVerification");
				}
			}
		}

		public override string SummaryText
		{
			get
			{
				int lastNonDefaultIndex = base.Names.GetLastNonDefaultIndex();
				switch (lastNonDefaultIndex)
				{
				case -1:
					return null;
				case 0:
				{
					string text = base.Names[lastNonDefaultIndex];
					int num = text.IndexOf(',');
					int num2;
					if (num == -1)
					{
						num = text.Length;
						num2 = text.LastIndexOf('.');
					}
					else
					{
						num2 = text.LastIndexOf('.', num);
					}
					if (num2 == -1)
					{
						return text;
					}
					return text.Substring(num2 + 1, num - num2 - 1);
				}
				default:
					return string.Format("{0} types", lastNonDefaultIndex + 1);
				}
			}
		}

		protected override FeatureNamesCollection CreateNamesCollection()
		{
			return new FeatureNamesCollection(31, "Type");
		}

		protected override bool IsFeatureValid(SecureLicenseContext context, bool reportError)
		{
			int num = 0;
			while (true)
			{
				if (num < base.Names.Count)
				{
					if (((int)base.Features & 1 << num) != 0)
					{
						string text = base.Names[num];
						bool fullyQualified = text.IndexOf(',') > -1;
						if (IsTypeMatch(text, context.LicensedType, context, fullyQualified))
						{
							break;
						}
						if (_checkEntireStack)
						{
							foreach (Type stackType in GetStackTypes(context))
							{
								if (IsTypeMatch(text, stackType, context, fullyQualified))
								{
									return true;
								}
							}
						}
					}
					num++;
					continue;
				}
				if (reportError)
				{
					context.ReportError("E_NoComponentInLicense", this, context.LicensedType);
				}
				return false;
			}
			return true;
		}

		private IEnumerable<Type> GetStackTypes(SecureLicenseContext context)
		{
			StackTrace trace = context.StackTrace;
			List<string> tried = new List<string>();
			int index = 0;
			while (true)
			{
				if (index >= trace.FrameCount)
				{
					break;
				}
				StackFrame frame = trace.GetFrame(index);
				MethodBase method = frame.GetMethod();
				if (method != null)
				{
					Type declaringType = method.DeclaringType;
					if (declaringType != null && declaringType.Assembly != null && declaringType.Assembly != typeof(SecureLicense).Assembly && declaringType.Assembly != typeof(object).Assembly)
					{
						Assembly assembly = declaringType.Assembly;
						string name = declaringType.AssemblyQualifiedName;
						if (!tried.Contains(name))
						{
							tried.Add(name);
							yield return declaringType;
						}
					}
				}
				index++;
			}
		}

		private bool IsTypeMatch(string typeName, Type testType, SecureLicenseContext context, bool fullyQualified)
		{
			if (typeName.StartsWith("!"))
			{
				if (TestType(typeName.Substring(1), testType, fullyQualified))
				{
					return true;
				}
			}
			else
			{
				for (Type type = testType; type != null; type = type.BaseType)
				{
					if (TestType(typeName, type, fullyQualified))
					{
						return true;
					}
				}
			}
			return false;
		}

		private bool TestType(string typeName, Type type, bool fullyQualified)
		{
			if (_forceStrongNameVerification && type.Assembly.Location != null && !Check.Crc(type.Assembly.Location))
			{
				return false;
			}
			if (fullyQualified)
			{
				if (!(type.AssemblyQualifiedName == typeName) && !(TypeHelper.GetNonVersionedAssemblyQualifiedName(type) == typeName))
				{
					goto IL_005a;
				}
				return true;
			}
			if (type.FullName == typeName)
			{
				return true;
			}
			goto IL_005a;
			IL_005a:
			return false;
		}

		protected override bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			if (_checkEntireStack)
			{
				writer.WriteAttributeString("checkEntireStack", "true");
			}
			if (_forceStrongNameVerification)
			{
				writer.WriteAttributeString("forceStrongNameVerification", "true");
			}
			return base.WriteToXml(writer, signing);
		}

		protected override bool ReadFromXml(XmlReader reader)
		{
			_checkEntireStack = (reader.GetAttribute("checkEntireStack") == "true");
			_forceStrongNameVerification = (reader.GetAttribute("forceStrongNameVerification") == "true");
			return base.ReadFromXml(reader);
		}
	}
}
