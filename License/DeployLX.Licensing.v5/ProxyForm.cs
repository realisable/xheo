using System;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Net;
using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	public sealed class ProxyForm : Form
	{
		private GradientPanel _controlHost;

		private Button _cancel;

		private Button _ok;

		private GroupBox groupBox1;

		private LicenseCodeTextBox _username;

		private LicenseCodeTextBox _address;

		private LicenseCodeTextBox _password;

		private Label _adminNotice;

		private PictureBox _tellMeMoreIcon;

		private Label shadowLabel3;

		private Label shadowLabel2;

		private Label shadowLabel4;

		private GradientPanel _noticePanel;

		private PictureBox _icon;

		private ShadowLabel _message;

		private IContainer components;

		public ProxyForm(Uri url, string message)
		{
			base.AutoScaleMode = AutoScaleMode.Dpi;
			Font = SystemFonts.MessageBoxFont;
			InitializeComponent();
			_password.PasswordChar = '●';
			_tellMeMoreIcon.Image = Images.Notice_png;
			_icon.Image = Images.BigWarning_png;
			if (message != null)
			{
				_message.Text = message;
			}
			else
			{
				_noticePanel.Visible = false;
				_controlHost.Height -= _noticePanel.Height;
				base.Height -= _message.Height;
			}
			_message.Font = new Font(SystemFonts.MessageBoxFont.FontFamily, 11f);
			_controlHost.Font = new Font(SystemFonts.MessageBoxFont.FontFamily, 11f);
			SR.LocalizeControl(this, true);
			IWebProxy proxy = Config.Proxy;
			if (proxy != null)
			{
				if (url == (Uri)null)
				{
					url = new Uri(null);
				}
				Uri uri = proxy.GetProxy(url);
				if (uri != (Uri)null)
				{
					if (uri == url)
					{
						Uri uri2 = new Uri("http://www.xheo.com");
						uri = proxy.GetProxy(uri2);
						if (uri == uri2)
						{
							uri = null;
						}
					}
					if (uri != (Uri)null)
					{
						_address.Text = uri.ToString();
					}
				}
				if (proxy.Credentials != null && uri != (Uri)null)
				{
					NetworkCredential credential = proxy.Credentials.GetCredential(uri, "Basic");
					if (credential.Domain != null && credential.Domain.Length != 0)
					{
						_username.Text = string.Format(CultureInfo.InvariantCulture, "{0}\\{1}", credential.Domain, credential.UserName);
					}
					else
					{
						_username.Text = credential.UserName;
					}
					_password.Text = credential.Password;
				}
			}
			else
			{
				_address.Text = "http://";
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			_controlHost = new GradientPanel();
			shadowLabel4 = new Label();
			_address = new LicenseCodeTextBox();
			groupBox1 = new GroupBox();
			_password = new LicenseCodeTextBox();
			_username = new LicenseCodeTextBox();
			shadowLabel3 = new Label();
			shadowLabel2 = new Label();
			_tellMeMoreIcon = new PictureBox();
			_adminNotice = new Label();
			_ok = new SkinnedButton();
			_cancel = new SkinnedButton();
			_noticePanel = new GradientPanel();
			_icon = new PictureBox();
			_message = new ShadowLabel();
			_controlHost.SuspendLayout();
			groupBox1.SuspendLayout();
			((ISupportInitialize)_tellMeMoreIcon).BeginInit();
			_noticePanel.SuspendLayout();
			((ISupportInitialize)_icon).BeginInit();
			base.SuspendLayout();
			_controlHost.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			_controlHost.BackColor = Color.White;
			_controlHost.BackgroundImageLayout = ImageLayout.None;
			_controlHost.Controls.Add(shadowLabel4);
			_controlHost.Controls.Add(_address);
			_controlHost.Controls.Add(groupBox1);
			_controlHost.Location = new Point(0, 74);
			_controlHost.Name = "_controlHost";
			_controlHost.Size = new Size(443, 243);
			_controlHost.TabIndex = 0;
			shadowLabel4.AutoSize = true;
			shadowLabel4.Cursor = Cursors.Default;
			shadowLabel4.Location = new Point(15, 14);
			shadowLabel4.Name = "shadowLabel4";
			shadowLabel4.Size = new Size(126, 13);
			shadowLabel4.TabIndex = 0;
			shadowLabel4.Text = "#UI_ProxyServerAddress";
			_address.CharacterSet = null;
			_address.Location = new Point(15, 40);
			_address.Mask = null;
			_address.Name = "_address";
			_address.Prompt = "";
			_address.Size = new Size(411, 20);
			_address.TabIndex = 1;
			groupBox1.Controls.Add(_password);
			groupBox1.Controls.Add(_username);
			groupBox1.Controls.Add(shadowLabel3);
			groupBox1.Controls.Add(shadowLabel2);
			groupBox1.Controls.Add(_tellMeMoreIcon);
			groupBox1.Controls.Add(_adminNotice);
			groupBox1.FlatStyle = FlatStyle.System;
			groupBox1.Location = new Point(15, 76);
			groupBox1.Name = "groupBox1";
			groupBox1.Size = new Size(411, 148);
			groupBox1.TabIndex = 2;
			groupBox1.TabStop = false;
			groupBox1.Text = "#UI_Credentials";
			_password.CharacterSet = null;
			_password.Location = new Point(15, 101);
			_password.Mask = null;
			_password.Name = "_password";
			_password.Prompt = "";
			_password.Size = new Size(161, 20);
			_password.TabIndex = 3;
			_username.CharacterSet = null;
			_username.Location = new Point(15, 47);
			_username.Mask = null;
			_username.Name = "_username";
			_username.Prompt = "";
			_username.Size = new Size(161, 20);
			_username.TabIndex = 1;
			shadowLabel3.AutoSize = true;
			shadowLabel3.Cursor = Cursors.Default;
			shadowLabel3.Location = new Point(15, 80);
			shadowLabel3.Name = "shadowLabel3";
			shadowLabel3.Size = new Size(77, 13);
			shadowLabel3.TabIndex = 2;
			shadowLabel3.Text = "#UI_Password";
			shadowLabel2.AutoSize = true;
			shadowLabel2.Cursor = Cursors.Default;
			shadowLabel2.Location = new Point(15, 26);
			shadowLabel2.Name = "shadowLabel2";
			shadowLabel2.Size = new Size(79, 13);
			shadowLabel2.TabIndex = 0;
			shadowLabel2.Text = "#UI_Username";
			_tellMeMoreIcon.Location = new Point(198, 50);
			_tellMeMoreIcon.Name = "_tellMeMoreIcon";
			_tellMeMoreIcon.Size = new Size(16, 17);
			_tellMeMoreIcon.TabIndex = 12;
			_tellMeMoreIcon.TabStop = false;
			_adminNotice.Cursor = Cursors.Default;
			_adminNotice.Location = new Point(220, 47);
			_adminNotice.Name = "_adminNotice";
			_adminNotice.Size = new Size(176, 77);
			_adminNotice.TabIndex = 4;
			_adminNotice.Text = "#UI_ProxyHelpNote";
			_ok.Anchor = (AnchorStyles.Bottom | AnchorStyles.Right);
			_ok.Location = new Point(221, 324);
			_ok.Name = "_ok";
			_ok.Size = new Size(100, 28);
			_ok.TabIndex = 1;
			_ok.Text = "#UI_OK";
			_ok.UseVisualStyleBackColor = false;
			_ok.Click += _ok_Click;
			_cancel.Anchor = (AnchorStyles.Bottom | AnchorStyles.Right);
			_cancel.DialogResult = DialogResult.Cancel;
			_cancel.Location = new Point(327, 324);
			_cancel.Name = "_cancel";
			_cancel.Size = new Size(100, 28);
			_cancel.TabIndex = 2;
			_cancel.Text = "#UI_Cancel";
			_cancel.UseVisualStyleBackColor = false;
			_noticePanel.BackColor = Color.FromArgb(0, 128, 192);
			_noticePanel.BackColor2 = Color.FromArgb(0, 153, 204);
			_noticePanel.BackgroundImageLayout = ImageLayout.None;
			_noticePanel.Controls.Add(_icon);
			_noticePanel.Controls.Add(_message);
			_noticePanel.Dock = DockStyle.Top;
			_noticePanel.GradientAngle = -90f;
			_noticePanel.Location = new Point(0, 0);
			_noticePanel.Name = "_noticePanel";
			_noticePanel.Size = new Size(443, 74);
			_noticePanel.TabIndex = 3;
			_icon.BackColor = Color.Transparent;
			_icon.Location = new Point(12, 12);
			_icon.Name = "_icon";
			_icon.Size = new Size(48, 48);
			_icon.TabIndex = 6;
			_icon.TabStop = false;
			_message.BackColor = Color.Transparent;
			_message.ForeColor = Color.White;
			_message.Location = new Point(66, 12);
			_message.Name = "_message";
			_message.Size = new Size(360, 48);
			_message.TabIndex = 5;
			_message.Text = "...";
			_message.TextAlign = ContentAlignment.MiddleLeft;
			base.AcceptButton = _ok;
			base.CancelButton = _cancel;
			base.ClientSize = new Size(443, 362);
			base.Controls.Add(_ok);
			base.Controls.Add(_cancel);
			base.Controls.Add(_controlHost);
			base.Controls.Add(_noticePanel);
			base.FormBorderStyle = FormBorderStyle.FixedDialog;
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "ProxyForm";
			base.StartPosition = FormStartPosition.CenterParent;
			Text = "#UI_ProxyFormTitle";
			_controlHost.ResumeLayout(false);
			_controlHost.PerformLayout();
			groupBox1.ResumeLayout(false);
			groupBox1.PerformLayout();
			((ISupportInitialize)_tellMeMoreIcon).EndInit();
			_noticePanel.ResumeLayout(false);
			((ISupportInitialize)_icon).EndInit();
			base.ResumeLayout(false);
		}

		protected override void OnVisibleChanged(EventArgs e)
		{
			base.OnVisibleChanged(e);
			if (base.Visible)
			{
				base.BringToFront();
			}
		}

		private void _ok_Click(object sender, EventArgs e)
		{
			try
			{
				if (_address.Text.Length == 0)
				{
					Config.SetProxy(GlobalProxySelection.GetEmptyWebProxy());
				}
				else
				{
					string username;
					string domain;
					Toolbox.SplitUserName(_username.Text, out username, out domain);
					Config.SetProxy(Toolbox.MakeWebProxy(_address.Text, username, _password.Text, domain));
				}
				base.DialogResult = DialogResult.OK;
			}
			catch (Exception ex)
			{
				LicenseEventArgs licenseEventArgs = new LicenseEventArgs(SecureLicenseManager.CurrentContext);
				SecureLicenseManager.OnErrorOccurred(licenseEventArgs);
				if (!licenseEventArgs.Handled)
				{
					MessageBoxEx.ShowException(this, null, ex);
				}
			}
		}
	}
}
