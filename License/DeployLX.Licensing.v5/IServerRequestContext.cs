using System;
using System.Collections;

namespace DeployLX.Licensing.v5
{
	public interface IServerRequestContext
	{
		Hashtable State
		{
			get;
		}

		string Command
		{
			get;
		}

		SecureLicense License
		{
			get;
		}

		string SerialNumber
		{
			get;
		}

		string MachineProfileHash
		{
			get;
		}

		LicenseValuesDictionary Properties
		{
			get;
		}

		bool IsClientServiceRequest
		{
			get;
		}

		bool IsClientWebRequest
		{
			get;
		}

		string LicensedType
		{
			get;
		}

		string LicensedAssembly
		{
			get;
		}

		string LicensedAssemblyName
		{
			get;
		}

		Version LicensedAssemblyVersion
		{
			get;
		}

		string LicensedAssemblyFileVersion
		{
			get;
		}

		bool DeveloperMode
		{
			get;
		}

		DateTime CurrentDateAndTime
		{
			get;
		}

		string MachineName
		{
			get;
		}
	}
}
