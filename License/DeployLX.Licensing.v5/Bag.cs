using System;
using System.Collections;
using System.ComponentModel;
using System.Threading;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public abstract class Bag : IXmlPersistable, IChange
	{
		private readonly Hashtable _items = new Hashtable(new CaseInsensitiveHashCodeProvider(), new CaseInsensitiveComparer());

		private bool _isReadOnly;

		private readonly string _name;

		private int _suspendUpdates;

		public IDictionary Dictionary
		{
			get
			{
				return _items;
			}
		}

		public event ChangeEventHandler Changed;

		private void OnChanged(ChangeEventArgs e)
		{
			if (_suspendUpdates <= 0 && this.Changed != null)
			{
				this.Changed(this, e);
			}
		}

		protected Bag(string name)
		{
			_name = name;
		}

		protected virtual object GetValue(string name, params string[] inherit)
		{
			object obj = _items[name];
			if (obj != null)
			{
				return obj;
			}
			if (inherit != null)
			{
				foreach (string key in inherit)
				{
					obj = _items[key];
					if (obj != null)
					{
						return obj;
					}
				}
			}
			return obj;
		}

		protected void SetValue(string name, object value)
		{
			AssertNotReadOnly();
			if (value == null)
			{
				_items.Remove(name);
			}
			else
			{
				_items[name] = value;
			}
			OnChanged(new ChangeEventArgs(null, this, name, CollectionChangeAction.Add));
		}

		public void Empty()
		{
			AssertNotReadOnly();
			_items.Clear();
		}

		protected void AssertNotReadOnly()
		{
			if (!_isReadOnly)
			{
				return;
			}
			throw new InvalidOperationException(SR.GetString("E_ReadOnlyObject", _name + " Bag"));
		}

		public void MakeReadOnly()
		{
			_isReadOnly = true;
		}

		internal abstract bool WriteToXml(XmlWriter writer, LicenseSaveType signing);

		bool IXmlPersistable.WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			return WriteToXml(writer, signing);
		}

		bool IXmlPersistable.ReadFromXml(XmlReader reader)
		{
			return ReadFromXml(reader);
		}

		internal abstract bool ReadFromXml(XmlReader reader);

		internal virtual bool ShouldSerialize()
		{
			return Dictionary.Count > 0;
		}

		public void BeginUpdate()
		{
			Interlocked.Increment(ref _suspendUpdates);
		}

		public void EndUpdate()
		{
			int num = Interlocked.Decrement(ref _suspendUpdates);
			if (num < 0)
			{
				throw new InvalidOperationException(SR.GetString("E_UnbalancedUpdate"));
			}
			if (num == 0)
			{
				OnChanged(new ChangeEventArgs(null, this, "[ALL]", CollectionChangeAction.Refresh));
			}
		}
	}
}
