using System;

namespace DeployLX.Licensing.v5
{
	[Flags]
	public enum MessageBoxExOptions
	{
		None = 0x0,
		AllowRememberChoice = 0x1,
		CopyButton = 0x2,
		PrintButton = 0x4,
		EmailButton = 0x8,
		DontUseHistory = 0x20,
		NonCritical = 0x40,
		SupportButtons = 0xE,
		ScrollableMessage = 0x100,
		Default = 0xE
	}
}
