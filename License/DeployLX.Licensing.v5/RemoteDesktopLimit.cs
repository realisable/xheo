using System;
using System.Collections.Generic;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[LimitEditor("RemoteDesktopLimitEditor", IconResource = "asmres://DeployLX.Licensing.DlxAddIn.v5/DeployLX.Licensing.DlxAddIn.v5.Resources.Limits.RemoteDesktop.png", Category = "State and Filters")]
	public class RemoteDesktopLimit : Limit
	{
		public class ValidationStateIds
		{
			public const string Local = "Local";

			public const string Remote = "Remote";
		}

		private static readonly List<string> _validationStates;

		private string _state;

		public override string Description
		{
			get
			{
				return SR.GetString("M_RemoteDesktopLimitDescription");
			}
		}

		public override string Name
		{
			get
			{
				return "Remote Desktop";
			}
		}

		public override string State
		{
			get
			{
				return _state ?? "Local";
			}
		}

		public override IList<string> ValidationStates
		{
			get
			{
				return _validationStates;
			}
		}

		static RemoteDesktopLimit()
		{
			_validationStates = new List<string>();
			_validationStates.Add("Local");
			_validationStates.Add("Remote");
		}

		public override ValidationResult Validate(SecureLicenseContext context)
		{
			_state = "Local";
			if (IsRemoteDesktopSession(context))
			{
				_state = "Remote";
				if (!base.Limits.HasStateLimits(_state))
				{
					return ValidationResult.Invalid;
				}
			}
			return base.Validate(context);
		}

		public override ValidationResult Granted(SecureLicenseContext context)
		{
			_state = "Local";
			if (IsRemoteDesktopSession(context))
			{
				_state = "Remote";
				if (!base.Limits.HasStateLimits(_state))
				{
					return ValidationResult.Invalid;
				}
			}
			return base.Granted(context);
		}

		protected virtual bool IsRemoteDesktopSession(SecureLicenseContext context)
		{
			if ((Environment.OSVersion.Platform & PlatformID.WinCE) != 0)
			{
				return SafeNativeMethods.GetSystemMetrics(4096) != 0;
			}
			return false;
		}

		protected override bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			return true;
		}

		protected override bool ReadFromXml(XmlReader reader)
		{
			return base.ReadChildLimits(reader);
		}
	}
}
