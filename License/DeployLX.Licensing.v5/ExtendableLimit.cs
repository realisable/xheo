using System;
using System.Collections;
using System.Collections.Specialized;
using System.Threading;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	[LimitEditor("ExtendableLimitEditor", Category = "Extendable Limits")]
	public abstract class ExtendableLimit : SuperFormLimit, IPurchaseLimit, IResettable, IServerLimit, IExtendableLimit
	{
		public class PageIds
		{
			public const string Extend = "EXTEND";

			public const string Default = "DEFAULT";

			public const string ExtendOnline = "EXTENDONLINE";

			internal const string ExtendOnlineAutomatically = "AUTOONLINE";
		}

		public const int MaxExtendableValue = 134217727;

		public static readonly string[] EmailTemplateVariables = new string[2]
		{
			"SerialNumber",
			"LimitCode"
		};

		private bool _canExtendByCode;

		private bool _canExtend;

		private int _extensionAmount = 1;

		private readonly UriCollection _servers;

		private bool _autoExtend;

		private int _serverRetries;

		private bool _useExtensionForm;

		private string _codeMask;

		private CodeAlgorithm _codeAlgorithm;

		private bool _isSubscription;

		private string _purchaseUrl;

		private int _extendableValue;

		private int _realExtendableValue = -1;

		private int _minExtendableValue = -1;

		private int _currentValue = -1;

		public bool CanExtendByCode
		{
			get
			{
				if (_canExtendByCode)
				{
					return _canExtend;
				}
				return false;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_canExtendByCode != value)
				{
					_canExtendByCode = value;
					base.OnChanged("CanExtendByCode");
				}
			}
		}

		public bool CanExtend
		{
			get
			{
				return _canExtend;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_canExtend != value)
				{
					_canExtend = value;
					base.OnChanged("CanExtend");
				}
			}
		}

		public int ExtensionAmount
		{
			get
			{
				return _extensionAmount;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_extensionAmount != value)
				{
					_extensionAmount = value;
					base.OnChanged("ExtensionAmount");
				}
			}
		}

		public UriCollection Servers
		{
			get
			{
				return _servers;
			}
		}

		public bool AutoExtend
		{
			get
			{
				return _autoExtend;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_autoExtend != value)
				{
					_autoExtend = value;
					base.OnChanged("AutoExtend");
				}
			}
		}

		public int ServerRetries
		{
			get
			{
				return _serverRetries;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_serverRetries != value)
				{
					_serverRetries = value;
					base.OnChanged("ServerRetries");
				}
			}
		}

		public bool UseExtensionForm
		{
			get
			{
				return _useExtensionForm;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_useExtensionForm != value)
				{
					_useExtensionForm = value;
					base.OnChanged("UseExtensionForm");
				}
			}
		}

		public string CodeMask
		{
			get
			{
				return _codeMask;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_codeMask != value)
				{
					_codeMask = value;
					base.OnChanged("CodeMask");
				}
			}
		}

		public CodeAlgorithm CodeAlgorithm
		{
			get
			{
				if (_codeAlgorithm != 0)
				{
					return _codeAlgorithm;
				}
				return CodeAlgorithm.Advanced;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_codeAlgorithm != value)
				{
					_codeAlgorithm = value;
					base.OnChanged("CodeAlgorithm");
				}
			}
		}

		public bool IsSubscription
		{
			get
			{
				return _isSubscription;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_isSubscription != value)
				{
					_isSubscription = value;
					base.OnChanged("IsSubscription");
				}
			}
		}

		public string PurchaseUrl
		{
			get
			{
				return _purchaseUrl;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_purchaseUrl != value)
				{
					SharedToolbox.ValidateUrl(value, true);
					_purchaseUrl = value;
					base.OnChanged("PurchaseUrl");
				}
			}
		}

		int IExtendableLimit.ExtendableValue
		{
			get
			{
				if (base.License != null && !base.License.IsEditing)
				{
					if (_realExtendableValue == -1)
					{
						SecureLicenseContext.WriteDiagnosticToContext("_realExtendableValue == -1, getting stored value");
                        object persistentData = base.GetPersistentData("ExtendableValue", _extendableValue, PersistentDataLocationType.SharedOrUser);

                        //
						//- Force to use _extendable value.
                        if (persistentData == null || true)
						{
							_realExtendableValue = _extendableValue;
						}
						else
						{
							_realExtendableValue = (int)persistentData;
						}
					}
					SecureLicenseContext.WriteDiagnosticToContext("_realExtendableValue = {0}, _extendableValue = {1}", _realExtendableValue, _extendableValue);
                    return _extendableValue;
				}
				return _extendableValue;
			}
			set
			{
				SecureLicenseContext.WriteDiagnosticToContext("Setting ExtendableValue to {0}", value);
				base.AssertNotReadOnly();
				if (_extendableValue != value)
				{
					_extendableValue = value;
					_realExtendableValue = -1;
					base.OnChanged("ExtendableValue");
				}
			}
		}

		int IExtendableLimit.MaxExtendableValue
		{
			get
			{
				return 2147483647;
			}
		}

		int IExtendableLimit.MinExtendableValue
		{
			get
			{
				if (_minExtendableValue == -1)
				{
					object persistentData = base.GetPersistentData("MinExtendableValue", 0, PersistentDataLocationType.SharedOrUser);
					if (persistentData != null)
					{
						_minExtendableValue = (int)persistentData;
					}
					else
					{
						_minExtendableValue = 0;
					}
				}
				return _minExtendableValue;
			}
		}

		protected ExtendableLimit()
		{
			_servers = new UriCollection();
			_servers.Changed += _servers_Changed;
		}

		private void _servers_Changed(object sender, CollectionEventArgs e)
		{
			base.OnCollectionChanged(sender, "Servers", e);
		}

		int IExtendableLimit.GetCurrentValue()
		{
			return GetCurrentValue();
		}

		protected virtual int GetCurrentValue()
		{
			if (!Check.CalledByThisType())
			{
				return ((IExtendableLimit)this).MaxExtendableValue;
			}
			if (_currentValue == -1)
			{
				object persistentData = base.GetPersistentData("CurrentValue");
				if (persistentData != null)
				{
					_currentValue = (int)persistentData;
				}
				else
				{
					_currentValue = ((IExtendableLimit)this).MinExtendableValue;
				}
			}
			return _currentValue;
		}

		public virtual bool ExtendByCode(SecureLicenseContext context, string code)
		{
			int amount;
			if (context.CheckLicenseExtension(code, this, out amount))
			{
				return UpdateValue(amount, false);
			}
			return false;
		}

		private bool UpdateValue(int amount, bool fromServer)
		{
			if (!Check.CalledByThisType())
			{
				SecureLicenseContext.WriteDiagnosticToContext("Not callbed by this type.");
				return true;
			}
			SecureLicenseContext.WriteDiagnosticToContext("Extendable limit UpdateValue( amount = {0}, fromServer = {1} )", amount, fromServer);
			SecureLicenseContext.WriteDiagnosticToContext("\tExtendableValue = {0}, MaxExtendableValue = {1}", ((IExtendableLimit)this).ExtendableValue, ((IExtendableLimit)this).MaxExtendableValue);
			if (amount > ((IExtendableLimit)this).ExtendableValue && amount <= ((IExtendableLimit)this).MaxExtendableValue)
			{
				SecureLicenseContext.WriteDiagnosticToContext("Calling CheckLimitReached");
				if (CheckLimitReached(amount))
				{
					return false;
				}
				_minExtendableValue = GetCurrentValue();
				_realExtendableValue = amount;
				if (fromServer)
				{
					_extendableValue = amount;
					base.SetPersistentData("ExtendableValue", null);
				}
				else
				{
					base.SetPersistentData("ExtendableValue", amount);
				}
				base.SetPersistentData("MinExtendableValue", _minExtendableValue);
				SecureLicenseContext.WriteDiagnosticToContext("Updated successfully.");
				return true;
			}
			return false;
		}

		public virtual void Reset(SecureLicenseContext context, int value)
		{
			if (!Check.CalledByType(typeof(ResetLimit)) && !Check.CalledByThisType())
			{
				return;
			}
			_minExtendableValue = -1;
			_realExtendableValue = -1;
			if (value == -1)
			{
				base.SetPersistentData("ExtendableValue", null);
				base.SetPersistentData("MinExtendableValue", null);
			}
			else
			{
				base.SetPersistentData("ExtendableValue", value);
				base.SetPersistentData("MinExtendableValue", null);
			}
		}

		public virtual bool InitializeFromSerialNumber(SecureLicenseContext context, string serialNumber, int index)
		{
			Check.NotNull(serialNumber, "serialNumber");
			if (!Check.CheckCalledByThisAssembly())
			{
				return false;
			}
			if (_canExtend && _canExtendByCode)
			{
				int num;
				byte[] value;
				if (!context.CheckSerialNumber(serialNumber, base.License.SerialNumberInfo, out num, out value))
				{
					return false;
				}
				int num2 = _realExtendableValue = (BitConverter.ToInt32(value, index) & 0x7FFFFFF);
				_minExtendableValue = 0;
				base.SetPersistentData("ExtendableValue", _realExtendableValue);
				base.SetPersistentData("MinExtendableValue", _minExtendableValue);
				return true;
			}
			return false;
		}

		public ValidationResult ExtendAtServer(SecureLicenseContext context, bool reportError)
		{
			return ExtendAtServerInternal(context, reportError, null);
		}

		private ValidationResult ExtendAtServerInternal(SecureLicenseContext context, bool reportError, object[] args)
		{
			ValidationRecord validationRecord = new ValidationRecord("E_CannotExtendAtServer", this, null, ErrorSeverity.Normal);
			try
			{
				if (Servers.Count == 0)
				{
					validationRecord.SubRecords.Add("E_NoServersDefined", this, null, ErrorSeverity.Low);
				}
				else
				{
					foreach (string item in (IEnumerable)Servers)
					{
						LicenseValuesDictionary licenseValuesDictionary = new LicenseValuesDictionary();
						Limit[] array = base.License.Limits.FindExtendableLimits();
						if (array != null)
						{
							Limit[] array2 = array;
							for (int i = 0; i < array2.Length; i++)
							{
								IExtendableLimit extendableLimit = (IExtendableLimit)array2[i];
								((Limit)extendableLimit).Peek(context);
								((StringDictionary)licenseValuesDictionary)[((Limit)extendableLimit).LimitId + ".ExtendableValue"] = extendableLimit.ExtendableValue.ToString();
								((StringDictionary)licenseValuesDictionary)[((Limit)extendableLimit).LimitId + ".CurrentValue"] = extendableLimit.GetCurrentValue().ToString();
							}
						}
						context.WriteDiagnostic("Extendable limit calling server to extend.");
						ServerResult serverResult = context.CallServer("EXTEND", item, this, null, licenseValuesDictionary, false);
						context.WriteDiagnostic("result.Success = {0}, .Retry = {1}", serverResult.Success, serverResult.Retry);
						if (serverResult.Success)
						{
							if (!serverResult.Retry)
							{
								IExtendableLimit extendableLimit2 = serverResult.CopyLicense.Limits.FindLimitById(base.LimitId) as IExtendableLimit;
								if (extendableLimit2 != null)
								{
									context.WriteDiagnostic("Found copy limit");
									lock (SecureLicenseManager.AsyncLock)
									{
										serverResult.CopyLicense.IsEditing = true;
										if (UpdateValue(extendableLimit2.ExtendableValue, true))
										{
											context.WriteDiagnostic("Updated value.");
											base.License.Signature = extendableLimit2.License.Signature;
											return ValidationResult.Valid;
										}
									}
									context.WriteDiagnostic("Could not copy limit or could not extend.");
									goto IL_0220;
								}
								return context.RetryWith(serverResult.CopyLicense.LicenseFile, serverResult.CopyLicense.LicenseId);
							}
							return ValidationResult.Retry;
						}
						goto IL_0220;
						IL_0220:
						if (serverResult.Exception != null)
						{
							SecureLicenseException ex = serverResult.Exception as SecureLicenseException;
							if (ex == null)
							{
								validationRecord.SubRecords.Add("E_UnexpectedErrorFromServer", this, serverResult.Exception, ErrorSeverity.High, item);
							}
							else
							{
								validationRecord.SubRecords.Add(ex.ErrorId, this, serverResult.Exception, ErrorSeverity.High, item);
							}
						}
					}
				}
			}
			catch (ThreadAbortException exception)
			{
				validationRecord.SubRecords.Add("E_AbortedByUser", this, exception, ErrorSeverity.Low);
			}
			if (reportError)
			{
				context.ReportError(validationRecord);
			}
			return ValidationResult.Invalid;
		}

		public AsyncValidationRequest ExtendAtServerAsync(SecureLicenseContext context, bool reportError, EventHandler completedHandler)
		{
			return new AsyncValidationRequest(ExtendAtServerInternal, context, reportError, null, completedHandler);
		}

		public virtual void UpdateAtServer(LicenseValuesDictionary values, int extensionAmount)
		{
			base.AssertNotReadOnly();
			int num = _extendableValue;
			int num2 = 0;
			if (((StringDictionary)values)[base.LimitId + ".ExtendableValue"] != null)
			{
				num = int.Parse(((StringDictionary)values)[base.LimitId + ".ExtendableValue"]);
			}
			if (((StringDictionary)values)[base.LimitId + ".CurrentValue"] != null)
			{
				num2 = int.Parse(((StringDictionary)values)[base.LimitId + ".CurrentValue"]);
			}
			if (extensionAmount == -1)
			{
				extensionAmount = -_extensionAmount;
			}
			SecureLicenseContext.WriteDiagnosticToContext("UpdateAtServer( max = {0}, current = {1}, extensionAmount = {2} )", num, num2, extensionAmount);
			if (extensionAmount < -1 && num2 > 0)
			{
				_extendableValue = num2 + -extensionAmount;
			}
			else
			{
				_extendableValue = num + extensionAmount;
			}
		}

		protected virtual bool CheckLimitReached(int maxAmount)
		{
			int currentValue = GetCurrentValue();
			SecureLicenseContext.WriteDiagnosticToContext("CheckLimitReached( maxAmount = {0} )", maxAmount);
			SecureLicenseContext.WriteDiagnosticToContext("\tcurrent = {0}, MinExtendableValue = {1}", currentValue, ((IExtendableLimit)this).MinExtendableValue);
			if (currentValue <= maxAmount)
			{
				return currentValue < ((IExtendableLimit)this).MinExtendableValue;
			}
			return true;
		}

		protected override bool ReadFromXml(XmlReader reader)
		{
			_canExtendByCode = (reader.GetAttribute("canExtendByCode") == "true");
			_canExtend = (reader.GetAttribute("canExtend") == "true");
			_autoExtend = (reader.GetAttribute("autoExtend") == "true");
			string attribute = reader.GetAttribute("serverRetries");
			if (attribute != null)
			{
				_serverRetries = SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_serverRetries = 0;
			}
			_isSubscription = (reader.GetAttribute("isSubscription") == "true");
			_purchaseUrl = reader.GetAttribute("purchaseUrl");
			_useExtensionForm = (reader.GetAttribute("useExtensionForm") == "true");
			_codeMask = reader.GetAttribute("codeMask");
			attribute = reader.GetAttribute("extendableValue");
			if (attribute != null)
			{
				_extendableValue = SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_extendableValue = 0;
			}
			attribute = reader.GetAttribute("extensionAmount");
			if (attribute != null)
			{
				_extensionAmount = SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_extensionAmount = 1;
			}
			attribute = reader.GetAttribute("codeAlgorithm");
			if (attribute != null)
			{
				_codeAlgorithm = (CodeAlgorithm)SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_codeAlgorithm = CodeAlgorithm.Advanced;
			}
			return base.ReadFromXml(reader);
		}

		protected override bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			if (_extendableValue != 0)
			{
				writer.WriteAttributeString("extendableValue", _extendableValue.ToString());
			}
			if (_canExtendByCode)
			{
				writer.WriteAttributeString("canExtendByCode", "true");
			}
			if (_canExtend)
			{
				writer.WriteAttributeString("canExtend", "true");
			}
			if (_isSubscription)
			{
				writer.WriteAttributeString("isSubscription", "true");
			}
			if (_autoExtend)
			{
				writer.WriteAttributeString("autoExtend", "true");
			}
			if (_serverRetries > 0)
			{
				writer.WriteAttributeString("serverRetries", _serverRetries.ToString());
			}
			if (_extensionAmount != 1)
			{
				writer.WriteAttributeString("extensionAmount", _extensionAmount.ToString());
			}
			if (_useExtensionForm)
			{
				writer.WriteAttributeString("useExtensionForm", "true");
			}
			if (_codeMask != null)
			{
				writer.WriteAttributeString("codeMask", _codeMask);
			}
			if (_codeAlgorithm != 0 && _codeAlgorithm != CodeAlgorithm.Advanced)
			{
				int codeAlgorithm = (int)_codeAlgorithm;
				writer.WriteAttributeString("codeAlgorithm", codeAlgorithm.ToString());
			}
			if (_purchaseUrl != null)
			{
				writer.WriteAttributeString("purchaseUrl", _purchaseUrl);
			}
			return base.WriteToXml(writer, signing);
		}

		public abstract string GetUseDescription(SecureLicenseContext context);

		public virtual void GetUseRange(SecureLicenseContext context, out int min, out int max, out int current)
		{
			min = ((IExtendableLimit)this).MinExtendableValue;
			max = ((IExtendableLimit)this).ExtendableValue;
			current = GetCurrentValue();
		}

		protected override SuperFormPanel CreatePanel(string pageId, SecureLicenseContext context)
		{
			if (((StringDictionary)base.CustomForms)[pageId] == null)
			{
				switch (pageId)
				{
				case "EXTENDONLINE":
				case "AUTOONLINE":
					return new ExtendOnlinePanel(new Limit[1]
					{
						this
					});
				case null:
				case "EXTEND":
				case "DEFAULT":
					return new ExtensionPanel(true, this);
				}
			}
			return base.CreatePanel(pageId, context);
		}

		protected void ExtendWithRequestExtensionCodes(SecureLicenseContext context)
		{
			if (context != null && context.RequestInfo != null && context.RequestInfo.ExtensionCodes != null && CanExtendByCode)
			{
				string[] extensionCodes = context.RequestInfo.ExtensionCodes;
				foreach (string code in extensionCodes)
				{
					ExtendByCode(context, code);
				}
			}
		}

		protected ValidationResult TryExtend(SecureLicenseContext context)
		{
			if (AutoExtend && context.Items[base.UniqueId + ".TriedAuto"] == null)
			{
				context.Items[base.UniqueId + ".TriedAuto"] = true;
				if (base.ResolveShouldShowForm(context) && context.CanShowWindowsForm && context.IsSuperFormVisible)
				{
					switch (ShowForm(context, "AUTOONLINE"))
					{
					case FormResult.Retry:
						return ValidationResult.Retry;
					case FormResult.Success:
						return ValidationResult.Valid;
					}
				}
				else
				{
					ValidationResult validationResult = ExtendAtServer(context, true);
					if (validationResult != 0)
					{
						return validationResult;
					}
				}
			}
			if (base.ResolveShouldShowForm(context) && context.CanShowWindowsForm)
			{
				switch (ShowForm(context, "DEFAULT"))
				{
				case FormResult.Retry:
					return ValidationResult.Retry;
				case FormResult.Success:
					return ValidationResult.Valid;
				}
			}
			return ValidationResult.Invalid;
		}

		protected override bool ResolveDefaultShouldShowForm(SecureLicenseContext context)
		{
			if (UseExtensionForm)
			{
				return CanExtend;
			}
			return false;
		}
	}
}
