namespace DeployLX.Licensing.v5
{
	internal interface IMachineProfileEntryMaker
	{
		string[] GetHardwareHash(MachineProfileEntryType type);
	}
}
