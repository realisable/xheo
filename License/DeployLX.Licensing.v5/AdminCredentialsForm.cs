using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	public sealed class AdminCredentialsForm : Form
	{
		private SecureLicenseContext _licenseContext;

		private Button _cancel;

		private Button _ok;

		private PictureBox _icon;

		private ShadowLabel _noticeLabel;

		private GroupBox _credentialsGroup;

		private ShadowLabel shadowLabel3;

		private ShadowLabel shadowLabel2;

		private PictureBox _tellMeMoreIcon;

		private ShadowLabel _adminNotice;

		private LicenseCodeTextBox _password;

		private LicenseCodeTextBox _username;

		private GradientPanel _controlHost;

		private ShadowLabel _vistaNotice;

		private IContainer components;

		private bool _callElevation;

		private WindowsImpersonationContext _context;

		public bool CallElevation
		{
			get
			{
				return _callElevation;
			}
		}

		public AdminCredentialsForm(SecureLicenseContext context)
		{
			base.AutoScaleMode = AutoScaleMode.Dpi;
			Font = SystemFonts.MessageBoxFont;
			InitializeComponent();
			_licenseContext = context;
			_password.PasswordChar = '●';
			_tellMeMoreIcon.Image = Images.Notice_png;
			_icon.Image = Images.BigWarning_png;
			_noticeLabel.Text = SR.GetString("UI_AdminRequiredNotice", context.SupportInfo.Product);
			SR.LocalizeControl(this, true);
			if (OSRecord.ThisMachine.Product >= OSProduct.WindowsVista)
			{
				_callElevation = true;
				_ok.Text = SR.GetString("UI_Authorize");
				_vistaNotice.Text = SR.GetString("UI_AdminVistaNotice");
				_ok.FlatStyle = FlatStyle.System;
				SafeNativeMethods.SendMessage(_ok.Handle, 5644u, IntPtr.Zero, new IntPtr(1));
			}
			else if (OSRecord.ThisMachine.Product <= OSProduct.Windows2000)
			{
				_callElevation = true;
				_ok.Text = SR.GetString("UI_Authorize");
				_vistaNotice.Text = SR.GetString("UI_AdminVistaNotice");
				_ok.FlatStyle = FlatStyle.System;
				SafeNativeMethods.SendMessage(_ok.Handle, 5644u, IntPtr.Zero, new IntPtr(1));
			}
			else
			{
				_vistaNotice.Visible = false;
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			_ok = new SkinnedButton();
			_cancel = new SkinnedButton();
			_controlHost = new GradientPanel();
			_credentialsGroup = new GroupBox();
			shadowLabel3 = new ShadowLabel();
			shadowLabel2 = new ShadowLabel();
			_tellMeMoreIcon = new PictureBox();
			_adminNotice = new ShadowLabel();
			_password = new LicenseCodeTextBox();
			_username = new LicenseCodeTextBox();
			_vistaNotice = new ShadowLabel();
			_noticeLabel = new ShadowLabel();
			_icon = new PictureBox();
			_controlHost.SuspendLayout();
			_credentialsGroup.SuspendLayout();
			base.SuspendLayout();
			_ok.Anchor = (AnchorStyles.Bottom | AnchorStyles.Right);
			_ok.Location = new Point(215, 224);
			_ok.Name = "_ok";
			_ok.Size = new Size(100, 26);
			_ok.TabIndex = 1;
			_ok.Text = "#UI_OK";
			_ok.Click += _ok_Click;
			_cancel.Anchor = (AnchorStyles.Bottom | AnchorStyles.Right);
			_cancel.DialogResult = DialogResult.Cancel;
			_cancel.Location = new Point(321, 224);
			_cancel.Name = "_cancel";
			_cancel.Size = new Size(100, 26);
			_cancel.TabIndex = 2;
			_cancel.Text = "#UI_Cancel";
			_controlHost.BackColor = Color.White;
			_controlHost.Controls.Add(_vistaNotice);
			_controlHost.Controls.Add(_noticeLabel);
			_controlHost.Controls.Add(_icon);
			_controlHost.Controls.Add(_credentialsGroup);
			_controlHost.Dock = DockStyle.Top;
			_controlHost.Location = new Point(0, 0);
			_controlHost.Name = "_controlHost";
			_controlHost.Size = new Size(429, 216);
			_controlHost.TabIndex = 0;
			_credentialsGroup.Controls.Add(shadowLabel3);
			_credentialsGroup.Controls.Add(shadowLabel2);
			_credentialsGroup.Controls.Add(_tellMeMoreIcon);
			_credentialsGroup.Controls.Add(_adminNotice);
			_credentialsGroup.Controls.Add(_password);
			_credentialsGroup.Controls.Add(_username);
			_credentialsGroup.FlatStyle = FlatStyle.System;
			_credentialsGroup.Location = new Point(73, 75);
			_credentialsGroup.Name = "_credentialsGroup";
			_credentialsGroup.Size = new Size(346, 125);
			_credentialsGroup.TabIndex = 8;
			_credentialsGroup.TabStop = false;
			_credentialsGroup.Text = "#UI_Credentials";
			shadowLabel3.AutoSize = true;
			shadowLabel3.Location = new Point(12, 68);
			shadowLabel3.Name = "shadowLabel3";
			shadowLabel3.Size = new Size(74, 15);
			shadowLabel3.TabIndex = 2;
			shadowLabel3.Text = "#UI_Password";
			shadowLabel2.AutoSize = true;
			shadowLabel2.Location = new Point(12, 19);
			shadowLabel2.Name = "shadowLabel2";
			shadowLabel2.Size = new Size(76, 15);
			shadowLabel2.TabIndex = 0;
			shadowLabel2.Text = "#UI_Username";
			_tellMeMoreIcon.Location = new Point(198, 38);
			_tellMeMoreIcon.Name = "_tellMeMoreIcon";
			_tellMeMoreIcon.Size = new Size(16, 17);
			_tellMeMoreIcon.TabIndex = 12;
			_tellMeMoreIcon.TabStop = false;
			_adminNotice.Location = new Point(220, 35);
			_adminNotice.Name = "_adminNotice";
			_adminNotice.Size = new Size(120, 52);
			_adminNotice.TabIndex = 4;
			_adminNotice.Text = "#UI_ProxyHelpNote";
			_password.CharacterSet = null;
			_password.Location = new Point(15, 87);
			_password.Mask = null;
			_password.Name = "_password";
			_password.Prompt = "";
			_password.Size = new Size(161, 20);
			_password.TabIndex = 3;
			_username.CharacterSet = null;
			_username.Location = new Point(15, 38);
			_username.Mask = null;
			_username.Name = "_username";
			_username.Prompt = "";
			_username.Size = new Size(161, 20);
			_username.TabIndex = 1;
			_vistaNotice.Location = new Point(73, 75);
			_vistaNotice.Name = "_vistaNotice";
			_vistaNotice.Size = new Size(346, 125);
			_vistaNotice.TabIndex = 9;
			_vistaNotice.Text = "#UI_AdminVistaNotice";
			_noticeLabel.Location = new Point(70, 12);
			_noticeLabel.Name = "_noticeLabel";
			_noticeLabel.Size = new Size(349, 57);
			_noticeLabel.TabIndex = 7;
			_noticeLabel.Text = "#UI_AdminRequiredNotice";
			_icon.BackColor = Color.Transparent;
			_icon.Location = new Point(12, 12);
			_icon.Name = "_icon";
			_icon.Size = new Size(48, 48);
			_icon.TabIndex = 6;
			_icon.TabStop = false;
			base.AcceptButton = _ok;
			base.CancelButton = _cancel;
			base.ClientSize = new Size(429, 258);
			base.Controls.Add(_ok);
			base.Controls.Add(_cancel);
			base.Controls.Add(_controlHost);
			base.FormBorderStyle = FormBorderStyle.FixedDialog;
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "AdminCredentialsForm";
			base.StartPosition = FormStartPosition.CenterParent;
			Text = "#UI_AdminFormTitle";
			_controlHost.ResumeLayout(false);
			_credentialsGroup.ResumeLayout(false);
			_credentialsGroup.PerformLayout();
			base.ResumeLayout(false);
		}

		protected override void OnActivated(EventArgs e)
		{
			base.OnActivated(e);
			_username.Select();
		}

		private void _ok_Click(object sender, EventArgs e)
		{
			if (_callElevation)
			{
				Process process = null;
				try
				{
					ProcessStartInfo processStartInfo = new ProcessStartInfo();
					processStartInfo.UseShellExecute = true;
					string location = typeof(SecureLicense).Assembly.Location;
					if (location != null)
					{
						processStartInfo.WorkingDirectory = Path.GetDirectoryName(location);
						processStartInfo.Arguments = string.Format("\"{0}\"", location);
					}
					processStartInfo.Verb = "runas";
					if (_licenseContext.RequestInfo.ElevationCommand != null)
					{
						processStartInfo.FileName = _licenseContext.RequestInfo.ElevationCommand;
					}
					else
					{
						processStartInfo.FileName = Path.Combine(Path.GetTempPath(), "License Authorization.exe");
						try
						{
							using (FileStream fileStream = File.OpenWrite(processStartInfo.FileName))
							{
								fileStream.Write(AdminStub.License_Authorization_exe, 0, AdminStub.License_Authorization_exe.Length);
							}
						}
						catch (IOException)
						{
							MessageBoxEx.Show(this, "UI_CouldNotAuthorizeLicense", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
						}
					}
					process = Process.Start(processStartInfo);
					if (process.WaitForExit(30000) && process.ExitCode == 0)
					{
						base.DialogResult = DialogResult.OK;
					}
					else
					{
						MessageBoxEx.Show(this, "UI_CouldNotAuthorizeLicense", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
					}
				}
				catch (Win32Exception)
				{
					if (process != null)
					{
						MessageBoxEx.Show(this, process.StandardOutput.ToString());
					}
				}
			}
			else if (_username.Text.Length != 0 && _password.Text.Length != 0)
			{
				IntPtr zero = IntPtr.Zero;
				IntPtr zero2 = IntPtr.Zero;
				try
				{
					string text;
					string environmentVariable;
					Toolbox.SplitUserName(_username.Text, out text, out environmentVariable);
					if (environmentVariable == null && text.IndexOf('@') < 0)
					{
						environmentVariable = Environment.GetEnvironmentVariable("USERDOMAIN");
					}
					if (SafeNativeMethods.LogonUser(text, environmentVariable, _password.Text, 2, 0, ref zero))
					{
						if (SafeNativeMethods.DuplicateToken(zero, 2, ref zero2))
						{
							WindowsIdentity windowsIdentity = new WindowsIdentity(zero2);
							WindowsPrincipal windowsPrincipal = new WindowsPrincipal(windowsIdentity);
							if (windowsPrincipal.IsInRole(WindowsBuiltInRole.Administrator))
							{
								_context = windowsIdentity.Impersonate();
								goto end_IL_0166;
							}
							MessageBoxEx.Show(this, SR.GetString("UI_UserIsNotAdmin", _username.Text), null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
							return;
						}
						goto end_IL_0166;
					}
					int lastWin32Error = Marshal.GetLastWin32Error();
					Win32Exception ex3 = new Win32Exception(lastWin32Error);
					MessageBoxEx.Show(this, string.Format("{0}\r\n\r\nError {1}\r\n{2}", SR.GetString("UI_InvalidCredentials"), lastWin32Error, ex3.ToString()), null, MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1, MessageBoxExOptions.SupportButtons);
					return;
					end_IL_0166:;
				}
				finally
				{
					if (zero != IntPtr.Zero)
					{
						SafeNativeMethods.CloseHandle(zero);
					}
					if (zero2 != IntPtr.Zero)
					{
						SafeNativeMethods.CloseHandle(zero2);
					}
				}
				base.DialogResult = DialogResult.OK;
			}
			else
			{
				MessageBoxEx.Show(this, "UI_PleaseEnterValidCredentials", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		internal static bool GetAdminContext(SecureLicenseContext context, ref ImpHandle handle)
		{
			using (AdminCredentialsForm adminCredentialsForm = new AdminCredentialsForm(context))
			{
				if (context.ShowDialog(adminCredentialsForm) == DialogResult.OK)
				{
					handle.Imp = adminCredentialsForm._context;
					handle.CalledElevation = adminCredentialsForm.CallElevation;
				}
			}
			return handle.IsValid;
		}
	}
}
