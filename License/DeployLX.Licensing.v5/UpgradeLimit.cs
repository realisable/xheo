using System;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	[LimitEditor("UpgradeLimitEditor", IconResource = "asmres://DeployLX.Licensing.DlxAddIn.v5/DeployLX.Licensing.DlxAddIn.v5.Resources.Limits.Upgrade.png")]
	public class UpgradeLimit : Limit
	{
		private string _serialNumberMask;

		private SerialNumberInfo _serialNumberInfo;

		private bool _originalUsedExtensions;

		public override string Description
		{
			get
			{
				return SR.GetString("M_UpgradeLimitDescription");
			}
		}

		public override string Name
		{
			get
			{
				return "Upgrade";
			}
		}

		public string SerialNumberMask
		{
			get
			{
				return _serialNumberMask;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_serialNumberMask != value)
				{
					_serialNumberMask = value;
					base.OnChanged("SerialNumberMask");
				}
			}
		}

		public string SerialNumber
		{
			get
			{
				return _serialNumberInfo.SerialNumber;
			}
			set
			{
				_serialNumberInfo.SerialNumber = value;
			}
		}

		public SerialNumberInfo SerialNumberInfo
		{
			get
			{
				return _serialNumberInfo;
			}
		}

		public bool OriginalUsedExtensions
		{
			get
			{
				return _originalUsedExtensions;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_originalUsedExtensions != value)
				{
					_originalUsedExtensions = value;
					base.OnChanged("OriginalUsedExtensions");
				}
			}
		}

		public override string SummaryText
		{
			get
			{
				return SerialNumberInfo.Prefix;
			}
		}

		public UpgradeLimit()
		{
			_serialNumberInfo = new SerialNumberInfo(null);
			((IChange)_serialNumberInfo).Changed += _serialNumberInfo_Changed;
		}

		private void _serialNumberInfo_Changed(object sender, ChangeEventArgs e)
		{
			base.OnChanged(e);
		}

		public override ValidationResult Validate(SecureLicenseContext context)
		{
			if (!context.RequestInfo.ShouldGetNewSerialNumber && _serialNumberInfo.SerialNumber != null && !_serialNumberInfo.SerialExternal)
			{
				if (_serialNumberInfo.SerialNumber == null && !context.RequestInfo.DeveloperMode)
				{
					string serialNumber = base.GetPersistentData("SerialNumber" + base.License.LicenseFile.ReleaseVersion, PersistentDataLocationType.SharedOrUser) as string;
					if (VerifySerialNumber(context, serialNumber) == ValidationResult.Valid)
					{
						_serialNumberInfo.SerialNumber = serialNumber;
					}
				}
				if (_serialNumberInfo.SerialNumber == null)
				{
					return context.ReportError("E_MissingSerialNumberUpgrade", this, null, ErrorSeverity.Low);
				}
				ValidationResult validationResult = VerifySerialNumber(context, _serialNumberInfo.SerialNumber);
				if (validationResult != ValidationResult.Valid)
				{
					return validationResult;
				}
				goto IL_0150;
			}
			string text = GetUpgradeSerialFromContext(context);
			bool serialExternal = text != null;
			if (text == null)
			{
				string text2 = base.GetPersistentData("SerialNumber" + base.License.LicenseFile.ReleaseVersion, PersistentDataLocationType.SharedOrUser) as string;
				if (VerifySerialNumber(context, text2) == ValidationResult.Valid)
				{
					text = text2;
				}
			}
			if (text != null && text.Length != 0)
			{
				if (_serialNumberInfo.SerialNumber != text)
				{
					_serialNumberInfo.SerialNumber = text;
					_serialNumberInfo.SerialExternal = serialExternal;
				}
				goto IL_0150;
			}
			return context.ReportError(context.RequestInfo.ShouldGetNewSerialNumber ? "E_RequestingNewSerialNumber" : "E_MissingSerialNumberUpgrade", this, null, ErrorSeverity.Low);
			IL_0150:
			return base.Validate(context);
		}

		private ValidationResult TrySerialNumber(SecureLicenseContext context, string serialNumber)
		{
			byte[] array;
			if (!context.CheckSerialNumber(serialNumber, _serialNumberInfo, out _serialNumberInfo._seed, out array))
			{
				return context.ReportError("E_InvalidSerialNumber", this, serialNumber);
			}
			if (_serialNumberInfo._seed >= _serialNumberInfo.MinSeed && _serialNumberInfo._seed <= _serialNumberInfo.MaxSeed && (_serialNumberInfo._seed - _serialNumberInfo.MinSeed) % _serialNumberInfo.Step == 0)
			{
				if (!context.RequestInfo.DeveloperMode)
				{
					base.SetPersistentData("SerialNumber" + base.License.LicenseFile.ReleaseVersion, serialNumber, PersistentDataLocationType.SharedOrUser);
				}
				return ValidationResult.Valid;
			}
			return context.ReportError("E_InvalidSerialNumber", this, serialNumber);
		}

		protected virtual ValidationResult VerifySerialNumber(SecureLicenseContext context, string serialNumber)
		{
			return TrySerialNumber(context, serialNumber);
		}

		public string GetUpgradeSerialFromContext(SecureLicenseContext context)
		{
			string text = null;
			string[] serialNumbers = context.RequestInfo.SerialNumbers;
			if (serialNumbers != null && serialNumbers != null)
			{
				string[] array = serialNumbers;
				foreach (string text2 in array)
				{
					if (text2 != null && (_serialNumberInfo.SerialExternal || text2 != _serialNumberInfo.SerialNumber) && VerifySerialNumber(context, text2) == ValidationResult.Valid)
					{
						text = text2;
						break;
					}
				}
			}
			if (text == null)
			{
				serialNumbers = Config.GetSerialNumber(context.LicensedType.Assembly.GetName().Name);
				if (serialNumbers != null)
				{
					string[] array2 = serialNumbers;
					foreach (string text3 in array2)
					{
						if (text3 != null && (_serialNumberInfo.SerialExternal || text3 != _serialNumberInfo.SerialNumber) && VerifySerialNumber(context, text3) == ValidationResult.Valid)
						{
							text = text3;
							break;
						}
					}
				}
			}
			return text;
		}

		public bool Unlock(SecureLicenseContext context, string serialNumber)
		{
			if (VerifySerialNumber(context, serialNumber) == ValidationResult.Valid)
			{
				_serialNumberInfo.SerialNumber = serialNumber;
				return true;
			}
			return false;
		}

		public bool CanUnlock(SecureLicenseContext context, string serialNumber)
		{
			return VerifySerialNumber(context, serialNumber) == ValidationResult.Valid;
		}

		protected override bool ReadFromXml(XmlReader reader)
		{
			_serialNumberInfo.Clear();
			_serialNumberMask = reader.GetAttribute("mask");
			_originalUsedExtensions = (reader.GetAttribute("originalUsedExtensions") == "true");
			if (reader.IsEmptyElement)
			{
				reader.Read();
				return true;
			}
			reader.Read();
			while (!reader.EOF)
			{
				if (reader.IsStartElement())
				{
					string name;
					if ((name = reader.Name) != null && name == "SerialNumber")
					{
						if (_serialNumberInfo.ReadFromXml(reader))
						{
							continue;
						}
						return false;
					}
					reader.Skip();
					continue;
				}
				reader.Read();
				break;
			}
			return true;
		}

		protected override bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			if (_serialNumberMask != null)
			{
				writer.WriteAttributeString("mask", _serialNumberMask);
			}
			if (_originalUsedExtensions)
			{
				writer.WriteAttributeString("originalUsedExtensions", "true");
			}
			_serialNumberInfo.WriteToXml(writer, signing);
			return true;
		}
	}
}
