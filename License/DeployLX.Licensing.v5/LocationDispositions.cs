using System;

namespace DeployLX.Licensing.v5
{
	[Flags]
	public enum LocationDispositions
	{
		NotSet = 0x0,
		FileSystem = 0x1,
		Embedded = 0x2,
		Shared = 0x4
	}
}
