using System;
using System.Drawing;
using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	internal class LabelOrBrowserControl : Control
	{
		private SmallIe _browser;

		private Label _label;

		private bool _isBrowser;

		private bool _nextPaintIsPrint;

		public override string Text
		{
			get
			{
				if (base.DesignMode)
				{
					return base.Text;
				}
				if (_isBrowser && _browser != null)
				{
					return _browser.LocationUrl;
				}
				if (_label != null)
				{
					return _label.Text;
				}
				return null;
			}
			set
			{
				if (base.DesignMode)
				{
					base.Text = value;
					base.Invalidate();
				}
				else
				{
					lock (this)
					{
						if (Toolbox.IsUriResource(value))
						{
							if (!_isBrowser && _browser == null)
							{
								_browser = new SmallIe();
								_browser.Dock = DockStyle.Fill;
								base.Controls.Add(_browser);
							}
							_isBrowser = true;
							_browser.BringToFront();
							_browser.Navigate(Toolbox.ResolveUrl(value, SecureLicenseContext.ResolveContext).ToString());
						}
						else
						{
							CreateLabel();
							_isBrowser = false;
							_label.BringToFront();
							_label.Text = value;
						}
					}
				}
			}
		}

		public LabelOrBrowserControl()
		{
			base.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
		}

		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
			if (_browser != null)
			{
				try
				{
					_browser.Dispose();
					_browser = null;
				}
				catch
				{
				}
			}
			if (_label != null)
			{
				_label.Dispose();
				_label = null;
			}
		}

		private void CreateLabel()
		{
			if (_label == null)
			{
				_label = new ShadowLabel();
				_label.BackColor = BackColor;
				_label.ForeColor = ForeColor;
				_label.Font = Font;
				_label.Dock = DockStyle.Fill;
				base.Controls.Add(_label);
			}
		}

		protected override void WndProc(ref Message m)
		{
			if (m.Msg == 791)
			{
				_nextPaintIsPrint = true;
			}
			base.WndProc(ref m);
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);
			if (_nextPaintIsPrint)
			{
				if (_isBrowser)
				{
					IntPtr hdc = e.Graphics.GetHdc();
					SafeNativeMethods.SendMessage(_browser.Handle, 791u, IntPtr.Zero, (IntPtr)22);
					e.Graphics.ReleaseHdc(hdc);
				}
				_nextPaintIsPrint = false;
			}
			if (base.DesignMode)
			{
				if (Toolbox.IsUriResource(base.Text))
				{
					using (SolidBrush brush = new SolidBrush(ForeColor))
					{
						e.Graphics.DrawString("*Will display URL in browser.*", Font, brush, 0f, 0f, StringFormat.GenericDefault);
					}
				}
				else
				{
					using (SolidBrush brush2 = new SolidBrush(ForeColor))
					{
						e.Graphics.DrawString(base.Text, Font, brush2, new RectangleF(0f, 0f, (float)base.Width, (float)base.Height), StringFormat.GenericDefault);
					}
				}
			}
		}

		protected override void OnForeColorChanged(EventArgs e)
		{
			if (_label != null)
			{
				_label.ForeColor = ForeColor;
			}
			base.OnForeColorChanged(e);
		}

		protected override void OnFontChanged(EventArgs e)
		{
			if (_label != null)
			{
				_label.Font = Font;
			}
			base.OnFontChanged(e);
		}
	}
}
