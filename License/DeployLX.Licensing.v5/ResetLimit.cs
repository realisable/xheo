using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	[LimitEditor("ResetLimitEditor", Category = "Extendable Limits", MiniType = "ResetLimitEditorControl", IconResource = "asmres://DeployLX.Licensing.DlxAddIn.v5/DeployLX.Licensing.DlxAddIn.v5.Resources.Limits.Reset.png")]
	public class ResetLimit : Limit
	{
		private readonly StringCollection _limitIds;

		private int _value = -1;

		private bool _resetImmediately;

		public override string Description
		{
			get
			{
				return SR.GetString("M_ResetLimitDescription");
			}
		}

		public StringCollection LimitIds
		{
			get
			{
				return _limitIds;
			}
		}

		public override string Name
		{
			get
			{
				return "Reset";
			}
		}

		public override string SummaryText
		{
			get
			{
				if (_limitIds.Count == 1)
				{
					Limit limit = base.License.Limits[_limitIds[0]];
					if (limit != null)
					{
						return string.Format("{0} limit", limit.Name);
					}
				}
				return string.Format("{0} limits", _limitIds.Count);
			}
		}

		public int Value
		{
			get
			{
				return _value;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_value != value)
				{
					_value = value;
					base.OnChanged("Value");
				}
			}
		}

		public bool ResetImmediately
		{
			get
			{
				return _resetImmediately;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_resetImmediately != value)
				{
					_resetImmediately = value;
					base.OnChanged("ResetImmediately");
				}
			}
		}

		public ResetLimit()
		{
			_limitIds = new StringCollection();
			_limitIds.Changed += OnLimitIdsChanged;
		}

		private void OnLimitIdsChanged(object sender, CollectionEventArgs e)
		{
			base.OnCollectionChanged(sender, "LimitIds", e);
		}

		protected override bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			if (signing == LicenseSaveType.Signing)
			{
				base.AssertMinimumVersion(SecureLicense.v4_0);
				foreach (string item in (IEnumerable)_limitIds)
				{
					if (base.License.Limits[item] != null)
					{
						continue;
					}
					throw new SecureLicenseException("E_TargetResetLimitDoesNotExist", item);
				}
			}
			if (_value >= 0)
			{
				writer.WriteAttributeString("value", _value.ToString());
			}
			if (_resetImmediately)
			{
				writer.WriteAttributeString("resetImmediately", "true");
			}
			_limitIds.WriteToXml(writer, "LimitIds", "Id", "value");
			return true;
		}

		protected override bool ReadFromXml(XmlReader reader)
		{
			_limitIds.Clear();
			string attribute = reader.GetAttribute("value");
			_value = ((attribute == null) ? (-1) : SafeToolbox.FastParseInt32(attribute));
			_resetImmediately = (reader.GetAttribute("resetImmediately") == "true");
			if (reader.IsEmptyElement)
			{
				reader.Read();
				return true;
			}
			reader.Read();
			while (!reader.EOF)
			{
				if (reader.IsStartElement())
				{
					switch (reader.Name)
					{
					case "Limit":
						if (base.ReadChildLimit(reader))
						{
							break;
						}
						return false;
					case "LimitIds":
						_limitIds.ReadFromXml(reader, "LimitIds", "Id", "value", false);
						break;
					}
					continue;
				}
				reader.Read();
				break;
			}
			return true;
		}

		protected internal override void MakeReadOnly()
		{
			base.MakeReadOnly();
			_limitIds.MakeReadOnly();
		}

		public override ValidationResult Validate(SecureLicenseContext context)
		{
			if (_resetImmediately)
			{
				ResetTargets(context);
			}
			return base.Validate(context);
		}

		public override ValidationResult Granted(SecureLicenseContext context)
		{
			ValidationResult validationResult = base.Granted(context);
			if (validationResult != ValidationResult.Valid)
			{
				return validationResult;
			}
			if (!_resetImmediately)
			{
				ResetTargets(context);
			}
			return ValidationResult.Valid;
		}

		private void ResetTargets(SecureLicenseContext context)
		{
			foreach (string item in (IEnumerable)_limitIds)
			{
				IResettable resettable = base.License.Limits[item] as IResettable;
				if (resettable == null)
				{
					context.ReportError("E_TargetLimitIsNotResettable", this, item);
				}
				else
				{
					resettable.Reset(context, _value);
				}
			}
		}

		protected override void ResolveCloneLinks(IDictionary<Limit, Limit> clones)
		{
			Limit limit = clones[this];
			_limitIds.Clear();
			foreach (string item in (IEnumerable)((ResetLimit)limit)._limitIds)
			{
				foreach (KeyValuePair<Limit, Limit> clone in clones)
				{
					if (clone.Value.LimitId == item)
					{
						_limitIds.Add(clone.Key.LimitId);
					}
				}
			}
		}
	}
}
