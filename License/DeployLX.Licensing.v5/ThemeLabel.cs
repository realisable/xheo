using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public class ThemeLabel : Label
	{
		private string[] _themeColor;

		private string _url;

		private ThemeFont _themeFont;

		private int _imagePadding;

		private Point _textOffset;

		private Point _imageOffset;

		private TextImageRelation _textImageRelation = TextImageRelation.ImageBeforeText;

		[Category("Appearance")]
		[DefaultValue(null)]
		public string[] ThemeColor
		{
			get
			{
				return _themeColor;
			}
			set
			{
				_themeColor = value;
			}
		}

		[Category("Appearance")]
		[DefaultValue(ThemeFont.Normal)]
		public ThemeFont ThemeFont
		{
			get
			{
				return _themeFont;
			}
			set
			{
				_themeFont = value;
				if (value == ThemeFont.Title)
				{
					base.AutoEllipsis = true;
				}
			}
		}

		[DefaultValue(0)]
		[Category("Appearance")]
		public int ImagePadding
		{
			get
			{
				return _imagePadding;
			}
			set
			{
				_imagePadding = value;
				base.Invalidate();
			}
		}

		[Category("Appearance")]
		public Point TextOffset
		{
			get
			{
				return _textOffset;
			}
			set
			{
				_textOffset = value;
				base.Invalidate();
			}
		}

		[Category("Appearance")]
		public Point ImageOffset
		{
			get
			{
				return _imageOffset;
			}
			set
			{
				_imageOffset = value;
				base.Invalidate();
			}
		}

		[DefaultValue(TextImageRelation.ImageBeforeText)]
		[Category("Appearance")]
		public TextImageRelation TextImageRelation
		{
			get
			{
				return _textImageRelation;
			}
			set
			{
				_textImageRelation = value;
				base.Invalidate();
			}
		}

		[Category("Behavior")]
		[DefaultValue(null)]
		public string Url
		{
			get
			{
				return _url;
			}
			set
			{
				if (value != null && value.Length == 0)
				{
					value = null;
				}
				_url = value;
				UpdateCursor();
			}
		}

		[Browsable(false)]
		public override Cursor Cursor
		{
			get
			{
				return base.Cursor;
			}
			set
			{
				base.Cursor = value;
			}
		}

		public override Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				if (value == Color.Empty)
				{
					value = Color.Transparent;
				}
				base.BackColor = value;
			}
		}

		protected override CreateParams CreateParams
		{
			get
			{
				CreateParams createParams = base.CreateParams;
				createParams.ClassName = null;
				return createParams;
			}
		}

		[Category("Action")]
		public event EventHandler LinkClicked;

		public ThemeLabel()
		{
			base.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
			BackColor = Color.Transparent;
		}

		private bool ShouldSerializeTextOffset()
		{
			return !_textOffset.IsEmpty;
		}

		private void ResetTextOffset()
		{
			_textOffset = Point.Empty;
		}

		private bool ShouldSerializeImageOffset()
		{
			return !_imageOffset.IsEmpty;
		}

		private void ResetImageOffset()
		{
			_imageOffset = Point.Empty;
		}

		private bool ShouldSerializeUrl()
		{
			return _url != null;
		}

		private bool ShouldSerializeCursor()
		{
			return false;
		}

		private bool ShouldSerializeBackColor()
		{
			return BackColor != Color.Transparent;
		}

		private new void ResetBackColor()
		{
			BackColor = Color.Transparent;
		}

		private void OnLinkClicked(EventArgs e)
		{
			if (this.LinkClicked != null)
			{
				this.LinkClicked(this, e);
			}
		}

		protected override void OnClick(EventArgs e)
		{
			base.OnClick(e);
			if (_url != null)
			{
				OnLinkClicked(e);
			}
		}

		protected override void OnParentChanged(EventArgs e)
		{
			base.OnParentChanged(e);
			UpdateCursor();
		}

		private void UpdateCursor()
		{
			if (!base.DesignMode)
			{
				Cursor = ((_url == null) ? Cursors.Default : Cursors.Hand);
			}
		}

		protected override void WndProc(ref Message m)
		{
			if (Cursor == Cursors.Hand && SafeNativeMethods.PreFilterMessage(ref m))
			{
				return;
			}
			base.WndProc(ref m);
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			OnPaintBackground(e);
			TextImagePainter painter = GetPainter();
			painter.Draw(e.Graphics);
			if (painter.Txt.Font != Font)
			{
				painter.Txt.Font.Dispose();
			}
		}

		internal virtual TextImagePainter GetPainter()
		{
			Font font = (Url == null) ? Font : new Font(Font, Font.Style | FontStyle.Underline);
			TextImagePainter textImagePainter = new TextImagePainter();
			textImagePainter.Bounds = base.ClientRectangle;
			textImagePainter.TextImageRelation = TextImageRelation;
			textImagePainter.Txt.Value = Text;
			textImagePainter.Txt.Color = ForeColor;
			textImagePainter.Txt.Font = font;
			textImagePainter.Txt.Alignment = TextAlign;
			textImagePainter.Txt.Format = MakeStringFormat();
			textImagePainter.Txt.Offset = TextOffset;
			textImagePainter.Img.Value = base.Image;
			textImagePainter.Img.Padding = ImagePadding;
			textImagePainter.Img.Offset = ImageOffset;
			return textImagePainter;
		}

		public override Size GetPreferredSize(Size proposedSize)
		{
			if (Text != null && Text.Length != 0)
			{
				TextFormatFlags textFormatFlags = MakeStringFormat();
				textFormatFlags = textFormatFlags;
				Size result = TextRenderer.MeasureText(Text, Font, new Size(2147483647, 2147483647), textFormatFlags);
				if (base.Image != null)
				{
					switch (TextImageRelation)
					{
					case TextImageRelation.ImageAboveText:
					case TextImageRelation.TextAboveImage:
						result.Width = Math.Max(base.Image.Width, result.Width);
						result.Height += base.Image.Height + ImagePadding;
						break;
					case TextImageRelation.ImageBeforeText:
					case TextImageRelation.TextBeforeImage:
						result.Width += base.Image.Width + ImagePadding;
						result.Height = Math.Max(result.Height, base.Image.Height);
						break;
					}
				}
				return result;
			}
			return base.GetPreferredSize(proposedSize);
		}

		internal TextFormatFlags MakeStringFormat()
		{
			TextFormatFlags textFormatFlags = ImageEffects.BuildTextFormatFlags(AutoSize ? ContentAlignment.MiddleLeft : TextAlign, AutoSize);
			if (!base.UseMnemonic)
			{
				textFormatFlags |= TextFormatFlags.HidePrefix;
			}
			if (base.AutoEllipsis)
			{
				textFormatFlags &= ~(TextFormatFlags.NoClipping | TextFormatFlags.WordBreak);
				textFormatFlags |= (TextFormatFlags.EndEllipsis | TextFormatFlags.SingleLine);
			}
			return textFormatFlags;
		}
	}
}
