using System;
using System.IO;
using System.Text;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public sealed class LicenseResource : IChange
	{
		private bool _isReadOnly;

		private string _name;

		private byte[] _data;

		private string _encodedData;

		private bool _isText;

		private string _text;

		private string _source;

		private string _address;

		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				AssertNotReadOnly();
				if (_name != value)
				{
					_name = value;
					OnChanged(new ChangeEventArgs("Name", this));
				}
			}
		}

		public byte[] Data
		{
			get
			{
				if (_data == null)
				{
					if (_address != null)
					{
						_data = Toolbox.GetResourceData(_address);
					}
					else if (_encodedData != null)
					{
						if (_isText)
						{
							_data = Encoding.UTF8.GetBytes(_encodedData);
						}
						else
						{
							_data = Convert.FromBase64String(_encodedData);
						}
					}
					else if (_source != null)
					{
						try
						{
							using (FileStream fileStream = new FileStream(_source, FileMode.Open, FileAccess.Read))
							{
								_data = new byte[fileStream.Length];
								fileStream.Read(_data, 0, _data.Length);
							}
						}
						catch
						{
						}
					}
				}
				return _data;
			}
			set
			{
				AssertNotReadOnly();
				if (!object.ReferenceEquals(_data, value))
				{
					_data = value;
					_text = null;
					_encodedData = null;
					_address = null;
					_source = null;
					OnChanged(new ChangeEventArgs("Data", this));
				}
			}
		}

		public bool IsText
		{
			get
			{
				return _isText;
			}
			set
			{
				AssertNotReadOnly();
				if (_isText != value)
				{
					_isText = value;
					OnChanged(new ChangeEventArgs("IsText", this));
				}
			}
		}

		public string Text
		{
			get
			{
				if (!_isText)
				{
					return null;
				}
				if (_text == null)
				{
					if (_address == null && _encodedData != null)
					{
						_text = _encodedData;
					}
					else if (Data != null)
					{
						_text = Encoding.UTF8.GetString(Data);
					}
				}
				return _text;
			}
			set
			{
				if (value != Text)
				{
					_isText = true;
					_data = null;
					_encodedData = value;
					_text = value;
					_address = null;
					_source = null;
					OnChanged(new ChangeEventArgs("Data", this));
				}
			}
		}

		public string Source
		{
			get
			{
				return _source;
			}
			set
			{
				AssertNotReadOnly();
				if (_source != value)
				{
					_source = value;
					OnChanged(new ChangeEventArgs("Source", this));
				}
			}
		}

		public string Address
		{
			get
			{
				return _address;
			}
			set
			{
				AssertNotReadOnly();
				if (_address != value)
				{
					SharedToolbox.ValidateUrl(value, true);
					_address = value;
					OnChanged(new ChangeEventArgs("Address", this));
				}
			}
		}

		public string Location
		{
			get
			{
				if (_address != null)
				{
					return _address;
				}
				return _source;
			}
		}

		public event ChangeEventHandler Changed;

		private void OnChanged(ChangeEventArgs e)
		{
			if (this.Changed != null)
			{
				this.Changed(this, e);
			}
		}

		public LicenseResource()
		{
		}

		public LicenseResource(string name, byte[] data)
		{
			_name = name;
			_data = data;
		}

		public void WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			Check.NotNull(writer, "writer");
			writer.WriteStartElement("Resource");
			if (_name == null)
			{
				throw new SecureLicenseException("E_PropertyNotSet", "Name", base.GetType());
			}
			if (_name == null && _source != null)
			{
				_name = Path.GetFileName(_source);
			}
			writer.WriteAttributeString("name", _name);
			if (signing == LicenseSaveType.Signing && _source != null)
			{
				try
				{
					using (FileStream fileStream = new FileStream(_source, FileMode.Open, FileAccess.Read, FileShare.Read))
					{
						_data = new byte[fileStream.Length];
						_encodedData = null;
						fileStream.Read(_data, 0, _data.Length);
					}
				}
				catch
				{
					if (_data != null)
					{
						goto end_IL_00cc;
					}
					if (_encodedData != null)
					{
						goto end_IL_00cc;
					}
					throw;
					end_IL_00cc:;
				}
			}
			if (_source != null)
			{
				writer.WriteAttributeString("source", _source);
			}
			if (_address != null)
			{
				writer.WriteAttributeString("address", SafeToolbox.XmlEncode(_address));
			}
			if (_isText)
			{
				writer.WriteAttributeString("isText", "true");
				writer.WriteAttributeString("xml:space", "preserve");
				if (_data != null || _encodedData != null)
				{
					if (_encodedData == null)
					{
						_encodedData = Encoding.UTF8.GetString(_data);
					}
					writer.WriteCData(_encodedData);
				}
			}
			else if (_data != null || _encodedData != null)
			{
				if (_encodedData == null)
				{
					_encodedData = Convert.ToBase64String(_data);
				}
				writer.WriteString(_encodedData);
			}
			writer.WriteEndElement();
		}

		public void ReadFromXml(XmlReader reader)
		{
			Check.NotNull(reader, "reader");
			AssertNotReadOnly();
			_data = null;
			_encodedData = null;
			_name = reader.GetAttribute("name");
			_source = reader.GetAttribute("source");
			if (_name == null)
			{
				if (_source == null)
				{
					throw new SecureLicenseException("E_MissingXmlAttribute", "name", reader.Name);
				}
				_name = Path.GetFileName(_source);
			}
			string attribute = reader.GetAttribute("isText");
			_isText = (attribute != null && attribute == "true");
			_address = SafeToolbox.XmlDecode(reader.GetAttribute("address"));
			_encodedData = reader.ReadElementString();
			if (_encodedData.Length == 0)
			{
				_encodedData = null;
			}
		}

		void IChange.MakeReadOnly()
		{
			_isReadOnly = true;
		}

		private void AssertNotReadOnly()
		{
			if (!_isReadOnly)
			{
				return;
			}
			throw new SecureLicenseException("E_ReadOnlyObject", "LicenseResource");
		}
	}
}
