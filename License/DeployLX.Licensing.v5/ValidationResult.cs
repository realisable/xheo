namespace DeployLX.Licensing.v5
{
	public enum ValidationResult
	{
		Invalid,
		Canceled,
		Retry,
		Valid = 4
	}
}
