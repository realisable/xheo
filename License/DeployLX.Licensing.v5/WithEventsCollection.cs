using System;
using System.Collections;
using System.ComponentModel;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public abstract class WithEventsCollection : IList, ICollection, IEnumerable, IChange
	{
		private ArrayList _list;

		private bool _isReadOnly;

		private int _suspendEvents;

		[NonSerialized]
		private CollectionEventHandler _adding;

		[NonSerialized]
		private CollectionEventHandler _added;

		[NonSerialized]
		private CollectionEventHandler _removing;

		[NonSerialized]
		private CollectionEventHandler _removed;

		[NonSerialized]
		private CollectionEventHandler _changing;

		[NonSerialized]
		private CollectionEventHandler _changed;

		[NonSerialized]
		private ChangeEventHandler _ichanged;

		protected IList List
		{
			get
			{
				return this;
			}
		}

		private IList InnerList
		{
			get
			{
				return _list;
			}
		}

		object IList.this[int index]
		{
			get
			{
				return InnerList[index];
			}
			set
			{
				object obj = InnerList[index];
				CollectionEventArgs e = null;
				if (obj != value)
				{
					e = new CollectionEventArgs(CollectionChangeAction.Remove, obj);
					OnRemoving(e);
				}
				CollectionEventArgs collectionEventArgs = new CollectionEventArgs(CollectionChangeAction.Add, value);
				OnAdding(collectionEventArgs);
				InnerList[index] = collectionEventArgs.Element;
				if (obj != value)
				{
					OnRemoved(e);
				}
				OnAdded(collectionEventArgs);
			}
		}

		public virtual int Count
		{
			get
			{
				return InnerList.Count;
			}
		}

		bool ICollection.IsSynchronized
		{
			get
			{
				return InnerList.IsSynchronized;
			}
		}

		public virtual object SyncRoot
		{
			get
			{
				return InnerList.SyncRoot;
			}
		}

		bool IList.IsFixedSize
		{
			get
			{
				return InnerList.IsFixedSize;
			}
		}

		public bool IsReadOnly
		{
			get
			{
				return _isReadOnly;
			}
		}

		public bool EventsSuspended
		{
			get
			{
				return _suspendEvents > 0;
			}
		}

		public event CollectionEventHandler Adding
		{
			add
			{
				lock (this)
				{
					_adding = (CollectionEventHandler)Delegate.Combine(_adding, value);
				}
			}
			remove
			{
				lock (this)
				{
					_adding = (CollectionEventHandler)Delegate.Remove(_adding, value);
				}
			}
		}

		public event CollectionEventHandler Added
		{
			add
			{
				lock (this)
				{
					_added = (CollectionEventHandler)Delegate.Combine(_added, value);
				}
			}
			remove
			{
				lock (this)
				{
					_added = (CollectionEventHandler)Delegate.Remove(_added, value);
				}
			}
		}

		public event CollectionEventHandler Removing
		{
			add
			{
				lock (this)
				{
					_removing = (CollectionEventHandler)Delegate.Combine(_removing, value);
				}
			}
			remove
			{
				lock (this)
				{
					_removing = (CollectionEventHandler)Delegate.Remove(_removing, value);
				}
			}
		}

		public event CollectionEventHandler Removed
		{
			add
			{
				lock (this)
				{
					_removed = (CollectionEventHandler)Delegate.Combine(_removed, value);
				}
			}
			remove
			{
				lock (this)
				{
					_removed = (CollectionEventHandler)Delegate.Remove(_removed, value);
				}
			}
		}

		public event CollectionEventHandler Changing
		{
			add
			{
				lock (this)
				{
					_changing = (CollectionEventHandler)Delegate.Combine(_changing, value);
				}
			}
			remove
			{
				lock (this)
				{
					_changing = (CollectionEventHandler)Delegate.Remove(_changing, value);
				}
			}
		}

		public event CollectionEventHandler Changed
		{
			add
			{
				lock (this)
				{
					_changed = (CollectionEventHandler)Delegate.Combine(_changed, value);
				}
			}
			remove
			{
				lock (this)
				{
					_changed = (CollectionEventHandler)Delegate.Remove(_changed, value);
				}
			}
		}

		event ChangeEventHandler IChange.Changed
		{
			add
			{
				lock (this)
				{
					_ichanged = (ChangeEventHandler)Delegate.Combine(_ichanged, value);
				}
			}
			remove
			{
				lock (this)
				{
					_ichanged = (ChangeEventHandler)Delegate.Remove(_ichanged, value);
				}
			}
		}

		protected virtual void OnAdding(CollectionEventArgs e)
		{
			if (e.Element != null && _suspendEvents <= 0)
			{
				if (_adding != null)
				{
					_adding(this, e);
				}
				OnChanging(e);
			}
		}

		protected virtual void OnAdded(CollectionEventArgs e)
		{
			if (e.Element != null && _suspendEvents <= 0)
			{
				if (_added != null)
				{
					_added(this, e);
				}
				OnChanged(e);
			}
		}

		protected virtual void OnRemoving(CollectionEventArgs e)
		{
			if (e.Element != null && _suspendEvents <= 0)
			{
				if (_removing != null)
				{
					_removing(this, e);
				}
				OnChanging(e);
			}
		}

		protected virtual void OnRemoved(CollectionEventArgs e)
		{
			if (e.Element != null && _suspendEvents <= 0)
			{
				if (_removed != null)
				{
					_removed(this, e);
				}
				OnChanged(e);
			}
		}

		protected virtual void OnChanging(CollectionEventArgs e)
		{
			if (e.Element != null && _suspendEvents <= 0 && _changing != null)
			{
				_changing(this, e);
			}
		}

		protected virtual void OnChanged(CollectionEventArgs e)
		{
			if (e.Element != null && _suspendEvents <= 0)
			{
				if (_changed != null)
				{
					_changed(this, e);
				}
				OnIChanged(e);
			}
		}

		protected virtual void OnIChanged(CollectionEventArgs e)
		{
			if (e.Element != null && _suspendEvents <= 0 && _ichanged != null)
			{
				_ichanged(this, new ChangeEventArgs(null, this, e.Element, e.Action));
			}
		}

		protected WithEventsCollection()
		{
			_list = new ArrayList();
		}

		protected WithEventsCollection(WithEventsCollection source)
		{
			Check.NotNull(source, "source");
			_list = source._list;
			source.Added += source_Added;
			source.Removed += source_Removed;
			source.Removing += source_Removing;
			source.Adding += source_Adding;
			source.Changing += source_Changing;
			source.Changed += source_Changed;
		}

		private void source_Changed(object sender, CollectionEventArgs e)
		{
			OnChanged(e);
		}

		private void source_Changing(object sender, CollectionEventArgs e)
		{
			OnChanging(e);
		}

		private void source_Adding(object sender, CollectionEventArgs e)
		{
			OnAdding(e);
		}

		private void source_Removing(object sender, CollectionEventArgs e)
		{
			OnRemoving(e);
		}

		private void source_Removed(object sender, CollectionEventArgs e)
		{
			OnRemoved(e);
		}

		private void source_Added(object sender, CollectionEventArgs e)
		{
			OnAdded(e);
		}

		public void MakeReadOnly()
		{
			MakeReadOnly(true);
		}

		protected virtual void MakeReadOnly(bool isReadOnly)
		{
			_isReadOnly = isReadOnly;
			if (isReadOnly)
			{
				foreach (object item in _list)
				{
					IChange change = item as IChange;
					if (change != null)
					{
						change.MakeReadOnly();
					}
				}
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return InnerList.GetEnumerator();
		}

		int IList.Add(object value)
		{
			if (value == null)
			{
				return -1;
			}
			int num = -1;
			CollectionEventArgs collectionEventArgs = null;
			if (_added != null || _adding != null || _changed != null || _changing != null)
			{
				collectionEventArgs = new CollectionEventArgs(CollectionChangeAction.Add, value);
			}
			if (collectionEventArgs != null)
			{
				OnAdding(collectionEventArgs);
				value = collectionEventArgs.Element;
			}
			num = InnerList.Add(value);
			if (collectionEventArgs != null)
			{
				OnAdded(collectionEventArgs);
			}
			return num;
		}

		bool IList.Contains(object value)
		{
			return InnerList.Contains(value);
		}

		int IList.IndexOf(object value)
		{
			return InnerList.IndexOf(value);
		}

		void IList.Insert(int index, object value)
		{
			if (value != null)
			{
				CollectionEventArgs collectionEventArgs = null;
				if (_adding != null || _added != null || _changed != null || _changing != null)
				{
					collectionEventArgs = new CollectionEventArgs(CollectionChangeAction.Add, value);
				}
				if (collectionEventArgs != null)
				{
					OnAdding(collectionEventArgs);
				}
				InnerList.Insert(index, value);
				if (collectionEventArgs != null)
				{
					OnAdded(collectionEventArgs);
				}
			}
		}

		void IList.Remove(object value)
		{
			if (value != null)
			{
				CollectionEventArgs collectionEventArgs = null;
				if (_removing != null || _removed != null || _changed != null || _changing != null)
				{
					collectionEventArgs = new CollectionEventArgs(CollectionChangeAction.Remove, value);
				}
				if (collectionEventArgs != null)
				{
					OnRemoving(collectionEventArgs);
					value = collectionEventArgs.Element;
				}
				InnerList.Remove(value);
				if (collectionEventArgs != null)
				{
					OnRemoved(collectionEventArgs);
				}
			}
		}

		public virtual void RemoveAt(int index)
		{
			object obj = null;
			CollectionEventArgs collectionEventArgs = null;
			if (_removing != null || _removed != null || _changed != null || _changing != null)
			{
				obj = InnerList[index];
				collectionEventArgs = new CollectionEventArgs(CollectionChangeAction.Remove, obj);
			}
			if (collectionEventArgs != null)
			{
				OnRemoving(collectionEventArgs);
			}
			InnerList.RemoveAt(index);
			if (collectionEventArgs != null)
			{
				OnRemoved(collectionEventArgs);
			}
		}

		public virtual void Clear()
		{
			if (Count != 0)
			{
				if (_removed == null && _removing == null && _changed == null && _changing == null)
				{
					InnerList.Clear();
				}
				else
				{
					ArrayList arrayList = new ArrayList(InnerList);
					InnerList.Clear();
					if (_removing != null || _changing != null)
					{
						for (int i = 0; i < arrayList.Count; i++)
						{
							CollectionEventArgs e = new CollectionEventArgs(CollectionChangeAction.Remove, arrayList[i]);
							OnRemoving(e);
						}
					}
					if (_removed == null && _changed == null)
					{
						return;
					}
					for (int j = 0; j < arrayList.Count; j++)
					{
						CollectionEventArgs e2 = new CollectionEventArgs(CollectionChangeAction.Remove, arrayList[j]);
						OnRemoved(e2);
					}
				}
			}
		}

		void ICollection.CopyTo(Array array, int index)
		{
			InnerList.CopyTo(array, index);
		}

		public void SuspendEvents()
		{
			_suspendEvents++;
		}

		public void ResumeEvents()
		{
			_suspendEvents--;
			if (_suspendEvents < 0)
			{
				_suspendEvents = 0;
			}
		}
	}
}
