namespace DeployLX.Licensing.v5
{
	public interface ISuperFormLimit
	{
		string CustomForm
		{
			get;
			set;
		}

		bool FormShown
		{
			get;
			set;
		}

		SuperFormPanel CreatePanel(string pageId, SecureLicenseContext context);
	}
}
