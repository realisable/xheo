using System;
using System.Collections.Generic;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	[LimitEditor("VirtualMachineLimitEditor", IconResource = "asmres://DeployLX.Licensing.DlxAddIn.v5/DeployLX.Licensing.DlxAddIn.v5.Resources.Limits.VirtualMachine.png", Category = "State and Filters")]
	public class VirtualMachineLimit : Limit
	{
		public class ValidationStateIds
		{
			public const string Unknown = "Unknown";

			public const string Physical = "Physical";

			public const string Virtual = "Virtual";
		}

		private static readonly List<string> _validationStates;

		private string _state;

		public override string Description
		{
			get
			{
				return SR.GetString("M_VirtualMachineLimitDescription");
			}
		}

		public override string Name
		{
			get
			{
				return "Virtual Machine";
			}
		}

		public override string State
		{
			get
			{
				return _state ?? "Unknown";
			}
		}

		public override IList<string> ValidationStates
		{
			get
			{
				return _validationStates;
			}
		}

		static VirtualMachineLimit()
		{
			_validationStates = new List<string>();
			_validationStates.Add("Unknown");
			_validationStates.Add("Physical");
			_validationStates.Add("Virtual");
		}

		protected virtual bool? IsVirtualMachine(SecureLicenseContext context)
		{
			VirtualMachineLevel virtualMachineLevel = (VirtualMachineLevel)Dossier.Compile();
			if (virtualMachineLevel != 0 && virtualMachineLevel != VirtualMachineLevel.Error)
			{
				return virtualMachineLevel > VirtualMachineLevel.Physical;
			}
			return null;
		}

		public override ValidationResult Validate(SecureLicenseContext context)
		{
			bool? nullable = IsVirtualMachine(context);
			if (!nullable.HasValue)
			{
				_state = "Unknown";
			}
			else
			{
				_state = (nullable.Value ? "Virtual" : "Physical");
			}
			return base.Validate(context);
		}

		protected override bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			if (signing == LicenseSaveType.Signing)
			{
				base.AssertMinimumVersion(SecureLicense.v5_0);
			}
			return true;
		}

		protected override bool ReadFromXml(XmlReader reader)
		{
			return base.ReadChildLimits(reader);
		}
	}
}
