namespace DeployLX.Licensing.v5
{
	public enum ShowFormOption
	{
		Never,
		Always,
		BeforeExpires,
		AfterExpires,
		FirstTimeAndExpires
	}
}
