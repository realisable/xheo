using System.Collections;

namespace DeployLX.Licensing.v5
{
	internal sealed class TreeListNodeCollection : WithEventsCollection
	{
		public TreeListNode this[int index]
		{
			get
			{
				return base.List[index] as TreeListNode;
			}
			set
			{
				base.List[index] = value;
			}
		}

		public int Add(TreeListNode node)
		{
			return base.List.Add(node);
		}

		public void AddRange(TreeListNode[] nodes)
		{
			if (nodes != null)
			{
				foreach (TreeListNode node in nodes)
				{
					Add(node);
				}
			}
		}

		public void AddRange(TreeListNodeCollection nodes)
		{
			if (nodes != null)
			{
				foreach (TreeListNode item in (IEnumerable)nodes)
				{
					Add(item);
				}
			}
		}

		public void Insert(int index, TreeListNode node)
		{
			base.List.Insert(index, node);
		}

		public void Remove(TreeListNode node)
		{
			base.List.Remove(node);
		}

		public void CopyTo(TreeListNode[] array, int index)
		{
			base.List.CopyTo(array, index);
		}

		public int IndexOf(TreeListNode node)
		{
			return base.List.IndexOf(node);
		}

		public bool Contains(TreeListNode node)
		{
			return base.List.Contains(node);
		}
	}
}
