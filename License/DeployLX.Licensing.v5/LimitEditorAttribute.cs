using System;
using System.Drawing;
using System.IO;

namespace DeployLX.Licensing.v5
{
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
	public sealed class LimitEditorAttribute : Attribute
	{
		public const string ExtendableGroup = "Extendable Limits";

		public const string SuperFormGroup = "Super Form Limits";

		public const string FilterGroup = "State and Filters";

		public const string WebGroup = "Web Limits";

		private readonly string _type;

		private string _contextHelp;

		private string _selectionHelp;

		private string _referenceHelp;

		private string _iconResource;

		private object _icon;

		private object _icon16;

		private string _category;

		public string Category
		{
			get
			{
				return _category;
			}
			set
			{
				_category = value;
			}
		}

		public string ContextHelp
		{
			get
			{
				return _contextHelp;
			}
			set
			{
				_contextHelp = value;
			}
		}

		public Image Icon
		{
			get
			{
				if (_icon == null)
				{
					_icon = Toolbox.GetImage(_iconResource, null);
					if (_icon == null)
					{
						_icon = new object();
					}
				}
				return _icon as Image;
			}
		}

		public Image Icon16
		{
			get
			{
				if (_icon16 == null)
				{
					if (_iconResource != null)
					{
						string address = Path.ChangeExtension(_iconResource, ".16" + Path.GetExtension(_iconResource));
						_icon16 = Toolbox.GetImage(address, null);
						if (_icon16 == null && Icon != null)
						{
							_icon16 = new Bitmap(Icon, 16, 16);
						}
					}
					if (_icon16 == null)
					{
						_icon16 = new object();
					}
				}
				return _icon16 as Image;
			}
		}

		public string IconResource
		{
			get
			{
				return _iconResource;
			}
			set
			{
				_iconResource = value;
			}
		}

		public string MiniType
		{
			get;
			set;
		}

		public string ReferenceHelp
		{
			get
			{
				return _referenceHelp;
			}
			set
			{
				_referenceHelp = value;
			}
		}

		public string SelectionHelp
		{
			get
			{
				return _selectionHelp;
			}
			set
			{
				_selectionHelp = value;
			}
		}

		public string Type
		{
			get
			{
				return _type;
			}
		}

		public LimitEditorAttribute(string type)
		{
			_type = type;
		}

		public static Image GetIcon(Limit limit, bool small)
		{
			if (limit == null)
			{
				return null;
			}
			LimitEditorAttribute limitEditorAttribute = Attribute.GetCustomAttribute(limit.GetType(), typeof(LimitEditorAttribute)) as LimitEditorAttribute;
			if (limitEditorAttribute == null)
			{
				return null;
			}
			if (!small)
			{
				return limitEditorAttribute.Icon;
			}
			return limitEditorAttribute.Icon16;
		}
	}
}
