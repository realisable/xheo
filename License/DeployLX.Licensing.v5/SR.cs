using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	internal sealed class SR
	{
		private static readonly StaticResources _sr = new StaticResources("DeployLX.Licensing.v5");

		public static StaticResources Resources
		{
			get
			{
				return _sr;
			}
		}

		private SR()
		{
		}

		public static string GetString(string name, params object[] args)
		{
			return _sr.GetString(name, true, args);
		}

		public static void LocalizeObject(object root, bool includeChildren, params object[] args)
		{
			_sr.LocalizeObject(root, true, includeChildren, args);
		}

		public static void LocalizeControl(Control root, bool includeChildren, params object[] args)
		{
			_sr.LocalizeControl(root, true, includeChildren, args);
		}
	}
}
