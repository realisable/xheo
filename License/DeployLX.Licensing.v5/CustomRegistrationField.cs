using System;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public sealed class CustomRegistrationField : IChange
	{
		private bool _isReadOnly;

		private string _name;

		private string _displayName;

		private CustomFieldType _fieldType;

		private string _customTypeName;

		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				AssertNotReadOnly();
				if (_name != value)
				{
					_name = value;
					OnChanged(new ChangeEventArgs("Name", this));
				}
			}
		}

		public string DisplayName
		{
			get
			{
				return _displayName;
			}
			set
			{
				AssertNotReadOnly();
				if (_displayName != value)
				{
					_displayName = value;
					OnChanged(new ChangeEventArgs("DisplayName", this));
				}
			}
		}

		public CustomFieldType FieldType
		{
			get
			{
				return _fieldType;
			}
			set
			{
				AssertNotReadOnly();
				if (_fieldType != value)
				{
					_fieldType = value;
					OnChanged(new ChangeEventArgs("FieldType", this));
				}
			}
		}

		public string CustomTypeName
		{
			get
			{
				return _customTypeName;
			}
			set
			{
				AssertNotReadOnly();
				if (_customTypeName != value)
				{
					_customTypeName = value;
					OnChanged(new ChangeEventArgs("CustomTypeName", this));
				}
			}
		}

		public event ChangeEventHandler Changed;

		private void OnChanged(ChangeEventArgs e)
		{
			if (this.Changed != null)
			{
				this.Changed(this, e);
			}
		}

		public CustomRegistrationField()
		{
		}

		public CustomRegistrationField(string name, string displayName, CustomFieldType fieldType, string customType)
		{
			_name = name;
			_displayName = displayName;
			_fieldType = fieldType;
			_customTypeName = customType;
		}

		public override string ToString()
		{
			if (_displayName != null)
			{
				return string.Format("{0} ({1})", _displayName, _name);
			}
			if (_name != null)
			{
				return _name;
			}
			return base.ToString();
		}

		void IChange.MakeReadOnly()
		{
			_isReadOnly = true;
		}

		private void AssertNotReadOnly()
		{
			if (!_isReadOnly)
			{
				return;
			}
			throw new SecureLicenseException("E_LicenseReadOnly");
		}
	}
}
