using Microsoft.Win32;
using System;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	public sealed class MachineProfile
	{
		private struct HardwareEntry
		{
			public string Path;

			public string ClassGUID;

			public string[] HardwareIds;

			public string DeviceDesc;

			public string Bus;

			public string DeviceId;
		}

		private const int v3_0 = 3;

		private const int v4_0 = 4;

		private const int CurrentVersion = 4;

		private const int MinVersion = 3;

		private const int MaxVersion = 4;

		private static MachineProfile _profile;

		private string _hash;

		private int _version = 3;

		private MachineProfileEntryCollection _hardwareList = new MachineProfileEntryCollection();

		private static int _isLaptop;

		private static bool _use64BitCompatibleCpuid;

		private static MachineProfile _default = GetDefaultProfile();

		private static readonly byte[] ScsiQuery = new byte[39]
		{
			28,
			0,
			0,
			0,
			83,
			67,
			83,
			73,
			68,
			73,
			83,
			75,
			16,
			39,
			0,
			0,
			1,
			5,
			27,
			0,
			0,
			0,
			0,
			0,
			17,
			2,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			236
		};

		private static readonly int[] _offsets32 = new int[6]
		{
			416,
			268,
			8,
			400,
			404,
			648
		};

		private static readonly int[] _offsets64 = new int[6]
		{
			420,
			272,
			12,
			404,
			408,
			704
		};

		private static ArrayList _hardwareEntries;

		public static MachineProfile Profile
		{
			get
			{
				if (_profile == null && _profile == null)
				{
					_profile = GetDefaultProfile();
				}
				return _profile;
			}
		}

		public string Hash
		{
			get
			{
				if (_hash == null)
				{
					_hash = GetComparableHash(false, null);
				}
				return _hash;
			}
		}

		public int Version
		{
			get
			{
				return _version;
			}
			set
			{
				Check.GreaterThanEqual(value, 3, "Version");
				Check.LessThanEqual(value, 4, "Version");
				_version = value;
				_hash = null;
			}
		}

		public MachineProfileEntryCollection HardwareList
		{
			get
			{
				return _hardwareList;
			}
		}

		public static MachineType MachineType
		{
			get
			{
				if (!IsLaptop)
				{
					return MachineType.Desktop;
				}
				return MachineType.Laptop;
			}
		}

		public static bool IsLaptop
		{
			get
			{
				if (_isLaptop == 0)
				{
					_isLaptop = ((HasBattery() && HasLid()) ? 1 : (-1));
				}
				return _isLaptop == 1;
			}
		}

		public static bool IsVirtualMachine
		{
			get
			{
				return Dossier.Compile() > 2;
			}
		}

		public static bool Use64BitCompatibleCpuid
		{
			get
			{
				return _use64BitCompatibleCpuid;
			}
			set
			{
				_use64BitCompatibleCpuid = value;
				_profile = null;
			}
		}

		public MachineProfile()
		{
			_hardwareList.Changed += _hardwareList_Changed;
		}

		private void _hardwareList_Changed(object sender, CollectionEventArgs e)
		{
			_hash = null;
			if (_hardwareList.Count <= 16)
			{
				return;
			}
			throw new SecureLicenseException("E_TooMuchHardware");
		}

		internal MachineProfile(MachineProfileEntryCollection hardware)
		{
			_hardwareList = hardware;
		}

		private static bool HasBattery()
		{
			byte[] array = new byte[24];
			if (SafeNativeMethods.GetSystemPowerStatus(array))
			{
				return array[1] < 128;
			}
			return false;
		}

		private static bool HasLid()
		{
			using (RegistryKey registryKey = Registry.LocalMachine.OpenSubKey("SYSTEM\\CurrentControlSet\\Control\\Class\\{4D36E97D-E325-11CE-BFC1-08002BE10318}", false))
			{
				if (registryKey != null)
				{
					string[] subKeyNames = registryKey.GetSubKeyNames();
					int num = 0;
					while (true)
					{
						if (num >= subKeyNames.Length)
						{
							break;
						}
						string name = subKeyNames[num];
						try
						{
							using (RegistryKey registryKey2 = registryKey.OpenSubKey(name, false))
							{
								if (registryKey2 != null && registryKey2.GetValue("MatchingDeviceId") as string== "*pnp0c0d")
								{
									return true;
								}
							}
						}
						catch
						{
						}
						num++;
					}
				}
			}
			return false;
		}

		public static MachineProfile GetDefaultProfile()
		{
			MachineProfile machineProfile = new MachineProfile();
			machineProfile._hardwareList.Add(new MachineProfileEntry(MachineProfileEntryType.MACAddress, 3, 1, 3));
			machineProfile._hardwareList.Add(new MachineProfileEntry(MachineProfileEntryType.Cpu, 1, 1, 2));
			machineProfile._hardwareList.Add(new MachineProfileEntry(MachineProfileEntryType.SystemDrive, 3, 2, 3));
			machineProfile._hardwareList.Add(new MachineProfileEntry(MachineProfileEntryType.Memory, 1, 1, 1));
			machineProfile._hardwareList.Add(new MachineProfileEntry(MachineProfileEntryType.CDRom, 1, 1, 1));
			machineProfile._hardwareList.Add(new MachineProfileEntry(MachineProfileEntryType.VideoCard, 1, 1, 1));
			machineProfile._hardwareList.Add(new MachineProfileEntry(MachineProfileEntryType.Ide, 1, 1, 1));
			machineProfile._hardwareList.Add(new MachineProfileEntry(MachineProfileEntryType.Scsi, 1, 1, 1));
			return machineProfile;
		}

		public static bool CheckIsDefault(MachineProfileEntryCollection profile)
		{
			Check.NotNull(profile, "profile");
			if (profile.Count != _default._hardwareList.Count)
			{
				return false;
			}
			foreach (MachineProfileEntry item in (IEnumerable)_default._hardwareList)
			{
				bool flag = false;
				foreach (MachineProfileEntry item2 in (IEnumerable)profile)
				{
					if (item2.Type == item.Type)
					{
						if (item2.Weight == item.Weight)
						{
							if (item2.PartialMatchWeight == item.PartialMatchWeight)
							{
								if (item2.EntryMaker == item.EntryMaker)
								{
									flag = true;
									continue;
								}
								return false;
							}
							return false;
						}
						return false;
					}
				}
				if (!flag)
				{
					return false;
				}
			}
			return true;
		}

		public string GetComparableHash(bool firstOnly, byte[] additional)
		{
			if (_hardwareList.Count == 0)
			{
				return null;
			}
			if (additional != null)
			{
				if (additional.Length > 8)
				{
					throw new SecureLicenseException("E_AdditionalProfileDataTooLong");
				}
				if (additional.Length == 0)
				{
					additional = null;
				}
			}
			ArrayList lengths = new ArrayList();
			using (MemoryStream ms = new MemoryStream())
			{
				foreach (MachineProfileEntry item in (IEnumerable)_hardwareList)
				{
					string text = null;
					string[] array = null;
					switch (item.Type & MachineProfileEntryType.TypeMask)
					{
					default:
						text = "";
						break;
					case MachineProfileEntryType.Custom1:
					case MachineProfileEntryType.Custom2:
					case MachineProfileEntryType.Custom3:
					case MachineProfileEntryType.Custom4:
						if (item.EntryMaker == null)
						{
							text = "";
						}
						else
						{
							array = item.EntryMaker.GetHardwareHash(item.Type);
						}
						break;
					case MachineProfileEntryType.Cpu:
						text = GetCPUID();
						break;
					case MachineProfileEntryType.Memory:
						text = GetPhysicalMemory();
						break;
					case MachineProfileEntryType.MACAddress:
						array = GetMacAddress();
						break;
					case MachineProfileEntryType.SystemDrive:
						text = GetSystemDrive();
						break;
					case MachineProfileEntryType.CDRom:
						array = GetHardwareId("{4D36E965-E325-11CE-BFC1-08002BE10318}", false, null);
						break;
					case MachineProfileEntryType.VideoCard:
						array = GetHardwareId("{4D36E968-E325-11CE-BFC1-08002BE10318}", false, "03");
						break;
					case MachineProfileEntryType.Scsi:
						array = GetHardwareId("{4D36E97B-E325-11CE-BFC1-08002BE10318}", false, "01", "00", "01", "04");
						break;
					case MachineProfileEntryType.Ide:
						array = GetHardwareId("{4D36E96A-E325-11CE-BFC1-08002BE10318}", false, "01", "01");
						break;
					case MachineProfileEntryType.Motherboard:
						array = GetMotherboardIds(false);
						break;
					}
					if (array == null)
					{
						array = new string[1]
						{
							text
						};
					}
					WriteValues(item, firstOnly, array, lengths, ms);
				}
				return FinalizeComparableHash(firstOnly, additional, lengths, ms);
			}
		}

		private string FinalizeComparableHash(bool firstOnly, byte[] additional, ArrayList lengths, MemoryStream ms)
		{
			int num = 0;
			if (additional != null)
			{
				num += additional.Length * 8;
			}
			if (!firstOnly)
			{
				num += (int)Math.Ceiling((double)lengths.Count / 2.0) * 8;
			}
			int num2 = SharedToolbox.CalculateLongestStringForBit((int)((ms.Length + 3L) * 8L + 4L + num), "U9VWT2FG3Q7RS0AC1DEYMNX6P8HJ4KL5", -1);
			for (num2 %= 5; num2 != 0; num2 %= 5)
			{
				ms.WriteByte(133);
				num2 = SharedToolbox.CalculateLongestStringForBit((int)((ms.Length + 3L) * 8L + 4L + num), "U9VWT2FG3Q7RS0AC1DEYMNX6P8HJ4KL5", -1);
			}
			if (additional != null)
			{
				ms.Write(additional, 0, additional.Length);
			}
			if (!firstOnly)
			{
				for (int i = 0; i < lengths.Count; i++)
				{
					byte b = (byte)((int)lengths[i++] << 4);
					if (i < lengths.Count)
					{
						b = (byte)(b | (byte)(int)lengths[i]);
					}
					ms.WriteByte(b);
				}
			}
			int num4 = _hardwareList.Count - 1;
			if (additional != null)
			{
				num4 |= additional.Length << 4;
			}
			ms.WriteByte((byte)num4);
			ms.Flush();
			byte[] array = ms.ToArray();
			int num5 = 5381;
			byte[] array2 = array;
			foreach (byte b2 in array2)
			{
				num5 = ((num5 << 5) + num5 ^ b2);
			}
			ms.WriteByte((byte)num5);
			byte value = (byte)(0x30 | (IsLaptop ? 2 : 0) | (firstOnly ? 1 : 0));
			ms.WriteByte(value);
			value = 8;
			ms.WriteByte(8);
			array = ms.ToArray();
			Array.Reverse(array);
			StringBuilder stringBuilder = new StringBuilder(SharedToolbox.ByteToString(array, "U9VWT2FG3Q7RS0AC1DEYMNX6P8HJ4KL5", -1));
			char[] array3 = stringBuilder.ToString().ToCharArray();
			int num6 = 3 | (int)array3[array3.Length - 1] % (array3.Length - 2);
			if (num6 >= array3.Length)
			{
				num6 = array3.Length - 1;
			}
			Array.Reverse(array3, 0, num6);
			num5 = (((int)array3[0] % (array3.Length - 1) & 7) | 1);
			Array.Reverse(array3, num5, array3.Length - num5);
			stringBuilder.Length = 0;
			stringBuilder.Append(array3);
			int k = stringBuilder.Length % 5;
			if (k == 0)
			{
				k = 5;
			}
			for (; k < stringBuilder.Length; k += 6)
			{
				stringBuilder.Insert(k, '-');
			}
			return stringBuilder.ToString();
		}

		private void WriteValues(MachineProfileEntry entry, bool firstOnly, string[] values, ArrayList lengths, MemoryStream ms)
		{
			if (values.Length > 15)
			{
				throw new InvalidOperationException(SR.GetString("E_TooManyHardwareVariations", entry.DisplayName));
			}
			if (!firstOnly)
			{
				lengths.Add(values.Length);
			}
			foreach (string s in values)
			{
				ms.WriteByte((byte)SharedToolbox.GetShortHashCode(s));
				if (firstOnly)
				{
					break;
				}
			}
		}

		public string GetComparableHashFromDiagnostic(bool firstOnly, string diagnosticFragment)
		{
			if (_hardwareList.Count == 0)
			{
				return null;
			}
			XmlDocument xmlDocument = new XmlDocument();
			xmlDocument.LoadXml(diagnosticFragment);
			ArrayList lengths = new ArrayList();
			using (MemoryStream ms = new MemoryStream())
			{
				foreach (MachineProfileEntry item in (IEnumerable)_hardwareList)
				{
					string[] array = null;
					XmlNode xmlNode = xmlDocument.SelectSingleNode(string.Format("//Component[@type='{0}']", item.Type));
					if (xmlNode == null)
					{
						array = new string[1]
						{
							""
						};
					}
					else
					{
						array = new string[xmlNode.ChildNodes.Count];
						int num = 0;
						foreach (XmlNode childNode in xmlNode.ChildNodes)
						{
							if (childNode.Attributes["raw"] != null)
							{
								string value = childNode.Attributes["raw"].Value;
								byte[] array2 = Convert.FromBase64String(value);
								char[] array3 = new char[array2.Length];
								for (int i = 0; i < array2.Length; i++)
								{
									array3[i] = (char)array2[i];
								}
								array[num] = new string(array3);
							}
							else
							{
								array[num] = childNode.Attributes["id"].Value;
							}
							num++;
						}
					}
					WriteValues(item, firstOnly, array, lengths, ms);
				}
				return FinalizeComparableHash(firstOnly, null, lengths, ms);
			}
		}

		public string GetDiagnosticHash()
		{
			StringBuilder stringBuilder = new StringBuilder();
			using (StringWriter w = new StringWriter(stringBuilder))
			{
				using (XmlTextWriter xmlTextWriter = new XmlTextWriter(w))
				{
					xmlTextWriter.Formatting = Formatting.Indented;
					xmlTextWriter.Indentation = 1;
					xmlTextWriter.IndentChar = '\t';
					xmlTextWriter.WriteStartDocument();
					xmlTextWriter.WriteStartElement("MachineProfile");
					xmlTextWriter.WriteAttributeString("isLaptop", IsLaptop ? "true" : "false");
					xmlTextWriter.WriteAttributeString("isVirtual", IsVirtualMachine ? "true" : "false");
					xmlTextWriter.WriteElementString("Hash", GetComparableHash(false, null));
					xmlTextWriter.WriteStartElement("Os");
					OSRecord thisMachine = OSRecord.ThisMachine;
					xmlTextWriter.WriteAttributeString("product", thisMachine.Product.ToString());
					xmlTextWriter.WriteAttributeString("edition", thisMachine.Edition.ToString());
					if (thisMachine.Version != (Version)null)
					{
						xmlTextWriter.WriteAttributeString("version", thisMachine.Version.ToString());
					}
					xmlTextWriter.WriteAttributeString("servicePack", thisMachine.ServicePack);
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteStartElement("Laptop");
					xmlTextWriter.WriteAttributeString("isLaptop", IsLaptop ? "yes" : "no");
					byte[] lpSystemPowerStatus = new byte[24];
					bool systemPowerStatus = SafeNativeMethods.GetSystemPowerStatus(lpSystemPowerStatus);
					xmlTextWriter.WriteAttributeString("battery", HasBattery() ? "yes" : "no");
					xmlTextWriter.WriteAttributeString("lid", HasLid() ? "yes" : "no");
					xmlTextWriter.WriteAttributeString("gotPower", systemPowerStatus ? "yes" : "no");
					xmlTextWriter.WriteEndElement();
					foreach (MachineProfileEntry item in (IEnumerable)_hardwareList)
					{
						xmlTextWriter.WriteStartElement("Component");
						xmlTextWriter.WriteAttributeString("displayName", item.DisplayName);
						xmlTextWriter.WriteAttributeString("type", item.Type.ToString());
						string text = null;
						string[] array = null;
						switch (item.Type & MachineProfileEntryType.TypeMask)
						{
						default:
							text = "";
							break;
						case MachineProfileEntryType.Custom1:
						case MachineProfileEntryType.Custom2:
						case MachineProfileEntryType.Custom3:
						case MachineProfileEntryType.Custom4:
							if (item.EntryMaker == null)
							{
								text = "";
							}
							else
							{
								array = item.EntryMaker.GetHardwareHash(item.Type);
							}
							break;
						case MachineProfileEntryType.Cpu:
							text = GetCPUID();
							break;
						case MachineProfileEntryType.Memory:
							text = GetPhysicalMemory();
							break;
						case MachineProfileEntryType.MACAddress:
							array = GetMacAddress();
							break;
						case MachineProfileEntryType.SystemDrive:
							text = GetSystemDrive();
							break;
						case MachineProfileEntryType.CDRom:
							array = GetHardwareId("{4D36E965-E325-11CE-BFC1-08002BE10318}", true, null);
							break;
						case MachineProfileEntryType.VideoCard:
							array = GetHardwareId("{4D36E968-E325-11CE-BFC1-08002BE10318}", true, "03");
							break;
						case MachineProfileEntryType.Scsi:
							array = GetHardwareId("{4D36E97B-E325-11CE-BFC1-08002BE10318}", true, "01", "00", "01", "04");
							break;
						case MachineProfileEntryType.Ide:
							array = GetHardwareId("{4D36E96A-E325-11CE-BFC1-08002BE10318}", true, "01", "01");
							break;
						case MachineProfileEntryType.Motherboard:
							array = GetMotherboardIds(true);
							break;
						}
						if (text == null)
						{
							if (array != null)
							{
								string[] array2 = array;
								foreach (string value in array2)
								{
									AppendValue(xmlTextWriter, value);
								}
							}
						}
						else
						{
							AppendValue(xmlTextWriter, text);
						}
						xmlTextWriter.WriteEndElement();
					}
					xmlTextWriter.WriteEndElement();
				}
			}
			return stringBuilder.ToString();
		}

		private static void AppendValue(XmlTextWriter xtw, string value)
		{
			if (value != null)
			{
				int num = value.IndexOf('\0');
				if (num > -1 && value.Length > num && value[num + 1] == '[')
				{
					xtw.WriteStartElement("Instance");
					xtw.WriteAttributeString("id", value.Substring(0, num));
					xtw.WriteAttributeString("details", value.Substring(num + 1));
					xtw.WriteEndElement();
				}
				else if (num > -1)
				{
					xtw.WriteStartElement("Instance");
					xtw.WriteAttributeString("id", value.Replace('\0', ' '));
					byte[] array = new byte[value.Length];
					for (int i = 0; i < array.Length; i++)
					{
						array[i] = (byte)value[i];
					}
					xtw.WriteAttributeString("raw", Convert.ToBase64String(array));
					xtw.WriteEndElement();
				}
				else
				{
					xtw.WriteStartElement("Instance");
					xtw.WriteAttributeString("id", value);
					xtw.WriteEndElement();
				}
			}
		}

		public static bool IsHashValid(string hash)
		{
			Check.NotNull(hash, "hash");
			if (hash.Length < 5)
			{
				return false;
			}
			try
			{
				StringBuilder stringBuilder = ParseHash(hash);
				byte[] array = SharedToolbox.StringToByte(stringBuilder.ToString(), "U9VWT2FG3Q7RS0AC1DEYMNX6P8HJ4KL5");
				byte b = array[1];
				if ((b >> 2 & 7) >= 3 && (b >> 2 & 7) <= 4)
				{
					Array.Reverse(array);
					int num = 5381;
					for (int i = 0; i < array.Length - 3; i++)
					{
						num = ((num << 5) + num ^ array[i]);
					}
					if (array[array.Length - 3] != (byte)num)
					{
						return false;
					}
					goto end_IL_0016;
				}
				return false;
				end_IL_0016:;
			}
			catch
			{
				return false;
			}
			return true;
		}

		public static bool IsLaptopHash(string hash)
		{
			Check.NotNull(hash, "hash");
			if (hash.Length < 5)
			{
				return false;
			}
			StringBuilder stringBuilder = ParseHash(hash);
			byte[] array = SharedToolbox.StringToByte(stringBuilder.ToString(), "U9VWT2FG3Q7RS0AC1DEYMNX6P8HJ4KL5");
			return (array[1] & 2) != 0;
		}

		private static StringBuilder ParseHash(string hash)
		{
			StringBuilder stringBuilder = new StringBuilder(hash);
			if (hash.Length < 5)
			{
				return stringBuilder;
			}
			stringBuilder.Replace("-", "");
			char[] array = stringBuilder.ToString().ToCharArray();
			int num = ((int)array[0] % (array.Length - 1) & 7) | 1;
			Array.Reverse(array, num, array.Length - num);
			num = (3 | (int)array[array.Length - 1] % (array.Length - 2));
			if (num >= array.Length)
			{
				num = array.Length - 1;
			}
			Array.Reverse(array, 0, num);
			stringBuilder.Length = 0;
			stringBuilder.Append(array);
			return stringBuilder;
		}

		public static int GetHashVersion(string hash)
		{
			Check.NotNullOrEmpty(hash, "hash");
			byte[] array;
			ArrayList arrayList = SplitHash(hash, out array);
			return ((int[])arrayList[0])[0] >> 2 & 7;
		}

		public static int CompareHash(string hash, string comparedHash, bool fileMoved, MachineProfileEntryCollection hardwareList, out MachineProfileEntryType[] diffs)
		{
			Check.NotNullOrEmpty(hash, "hash");
			Check.NotNullOrEmpty(comparedHash, "comparedHash");
			Check.NotNull(hardwareList, "hardwareList");
			diffs = null;
			if (IsHashValid(hash) && IsHashValid(comparedHash))
			{
				byte[] array;
				ArrayList arrayList = SplitHash(hash, out array);
				ArrayList arrayList2 = SplitHash(comparedHash, out array);
				ArrayList arrayList3 = new ArrayList();
				int num = ((int[])arrayList[0])[0] >> 2 & 7;
				int num2 = ((int[])arrayList2[0])[0] >> 2 & 7;
				if (num <= 4 && num >= 3 && num2 <= 4 && num2 >= 3)
				{
					bool flag = (((int[])arrayList[0])[0] & 1) != 0;
					int num3 = 0;
					for (int i = 0; i < hardwareList.Count; i++)
					{
						int[] array2 = (arrayList.Count <= i + 1) ? new int[0] : (arrayList[i + 1] as int[]);
						int[] array3 = (arrayList2.Count <= i + 1) ? new int[0] : (arrayList2[i + 1] as int[]);
						bool flag2 = array2.Length == 0 && array3.Length == 0;
						for (int j = 0; j < array2.Length; j++)
						{
							for (int k = 0; k < array3.Length; k++)
							{
								if (array2[j] == array3[k])
								{
									flag2 = true;
									array3[k] = -1;
									array2[j] = -1;
								}
							}
						}
						if (flag2)
						{
							bool flag3 = false;
							int num4 = 0;
							while (num4 < array2.Length)
							{
								if (array2[num4] == -1)
								{
									num4++;
									continue;
								}
								flag3 = true;
								break;
							}
							if (!flag3 && !flag)
							{
								int num5 = 0;
								while (num5 < array3.Length)
								{
									if (array3[num5] == -1)
									{
										num5++;
										continue;
									}
									flag3 = true;
									break;
								}
							}
							if (flag3)
							{
								num3 = ((!fileMoved) ? (num3 + hardwareList[i].PartialMatchWeight) : (num3 + Math.Max(hardwareList[i].FileMovedWeight, hardwareList[i].PartialMatchWeight)));
								arrayList3.Add(hardwareList[i].Type | MachineProfileEntryType.PartialFlag);
							}
						}
						else
						{
							num3 += (fileMoved ? hardwareList[i].FileMovedWeight : hardwareList[i].Weight);
							arrayList3.Add(hardwareList[i].Type);
						}
					}
					if (arrayList3.Count > 0)
					{
						diffs = (arrayList3.ToArray(typeof(MachineProfileEntryType)) as MachineProfileEntryType[]);
					}
					return Math.Min(num3, 10);
				}
				return 10002;
			}
			return 10001;
		}

		private static ArrayList SplitHash(string hash, out byte[] additional)
		{
			additional = null;
			ArrayList arrayList = new ArrayList();
			StringBuilder stringBuilder = ParseHash(hash);
			byte[] array = SharedToolbox.StringToByte(stringBuilder.ToString(), "U9VWT2FG3Q7RS0AC1DEYMNX6P8HJ4KL5");
			byte b = array[1];
			Array.Reverse(array);
			bool flag = (b & 1) != 0;
			arrayList.Add(new int[1]
			{
				b
			});
			int num = (array[array.Length - 4] & 0xF) + 1;
			int i = 0;
			if (flag)
			{
				for (; i < num; i++)
				{
					arrayList.Add(new int[1]
					{
						array[i]
					});
				}
			}
			else
			{
				int[] array2 = new int[num];
				int num2 = 0;
				for (int j = array.Length - (4 + (int)Math.Ceiling((double)num / 2.0)); j < array.Length - 4; j++)
				{
					byte b2 = array[j];
					array2[num2++] = b2 >> 4;
					if (num2 < array2.Length)
					{
						array2[num2++] = (b2 & 0xF);
					}
				}
				for (int k = 0; k < num; k++)
				{
					int[] array3 = new int[array2[k]];
					for (num2 = 0; num2 < array2[k]; num2++)
					{
						array3[num2] = array[i++];
					}
					arrayList.Add(array3);
				}
			}
			int num6 = array[array.Length - 4] >> 4;
			if (num6 > 0)
			{
				additional = new byte[num6];
				Buffer.BlockCopy(array, array.Length - (4 + num6), additional, 0, num6);
			}
			return arrayList;
		}

		public static int CompareHash(string hash, MachineProfile profile, bool fileMoved)
		{
			return CompareHash(hash, profile.GetComparableHash(false, null), fileMoved, profile._hardwareList);
		}

		public static int CompareHash(string hash, string comparedHash, bool fileMoved, MachineProfileEntryCollection hardwareList)
		{
			MachineProfileEntryType[] array;
			return CompareHash(hash, comparedHash, fileMoved, hardwareList, out array);
		}

		public static MachineProfileEntryType[] GetDifferences(string hash, string comparedHash, MachineProfileEntryCollection hardwareList)
		{
			MachineProfileEntryType[] result;
			CompareHash(hash, comparedHash, false, hardwareList, out result);
			return result;
		}

		public static MachineProfileEntryType[] GetDifferences(string hash, MachineProfile profile)
		{
			return GetDifferences(hash, profile.GetComparableHash(false, null), profile._hardwareList);
		}

		public static byte[] GetAdditionalData(string hash)
		{
			Check.NotNullOrEmpty(hash, "hash");
			StringBuilder stringBuilder = ParseHash(hash);
			byte[] array = SharedToolbox.StringToByte(stringBuilder.ToString(), "U9VWT2FG3Q7RS0AC1DEYMNX6P8HJ4KL5");
			Array.Reverse(array);
			byte[] array2 = null;
			int num = array[array.Length - 4] >> 4;
			if (num > 0)
			{
				array2 = new byte[num];
				Buffer.BlockCopy(array, array.Length - (4 + num), array2, 0, num);
			}
			return array2;
		}

		private static string GetSystemDrive()
		{
			return GetHddSerialNumberForPath(Environment.GetFolderPath(Environment.SpecialFolder.System));
		}

		private static string GetHddSerialNumberForPath(string path)
		{
			string text = null;
			string text2;
			string deviceNumber = MapFileToDeviceNumber(path, out text2);
			if (Environment.OSVersion.Platform == PlatformID.Win32NT)
			{
				text = GetHddSerialNumberScsi(deviceNumber);
				if (text == null)
				{
					text = GetHddSerialNumberIde(text2);
				}
			}
			if (text == null || text.Length == 0 || text.StartsWith("VEND:"))
			{
				int num;
				int num2;
				if (!SafeNativeMethods.GetVolumeInformation(text2 + '\\', null, 0, out num, out num2, out num2, null, 0))
				{
					num = new Random().Next(2147483647);
				}
				long num3 = 0L;
				long num4 = 0L;
				if (OSRecord.ThisMachine.Version.Major >= 5)
				{
					IntPtr handle = SafeNativeMethods.CreateFile(string.Format("\\\\.\\{0}", text2), 0u, 3u, IntPtr.Zero, 3u, 0u, IntPtr.Zero);
					byte[] array = new byte[256];
					try
					{
						int num5 = 0;
						if (SafeNativeMethods.DeviceIoControl(handle, 458824, IntPtr.Zero, 0, array, array.Length, ref num5, IntPtr.Zero))
						{
							num3 = BitConverter.ToInt64(array, 16) - 4096L;
						}
					}
					finally
					{
						SafeNativeMethods.CloseHandle(handle);
					}
				}
				if (num3 == 0L && !SafeNativeMethods.GetDiskFreeSpaceEx(text2 + '\\', out num4, out num3, out num4))
				{
					num3 = new Random().Next(2147483647);
				}
				text = string.Format("{3}{0:X4}-{1:X4}:{2}", num >> 16, num & 0xFFFF, num3, text);
			}
			return text;
		}

		private static string GetHddSerialNumberIde(string deviceNumber)
		{
			IntPtr handle = SafeNativeMethods.CreateFile(string.Format("\\\\.\\{0}", deviceNumber), 0u, 3u, IntPtr.Zero, 3u, 0u, IntPtr.Zero);
			if (handle.ToInt32() == -1)
			{
				return null;
			}
			try
			{
				byte[] value = new byte[12];
				byte[] array = new byte[10000];
				int num = 0;
				GCHandle gCHandle = GCHandle.Alloc(value, GCHandleType.Pinned);
				try
				{
					if (!SafeNativeMethods.DeviceIoControl(handle, 2954240, gCHandle.AddrOfPinnedObject(), 12, array, array.Length, ref num, IntPtr.Zero))
					{
						return null;
					}
				}
				finally
				{
					gCHandle.Free();
				}
				int num2 = BitConverter.ToInt32(array, 24);
				if (num2 <= num && num2 >= 0)
				{
					string text = FlipAndCodeBytes(array, num2);
					if (text != null && text.Length != 0)
					{
						return text.Trim();
					}
					num2 = BitConverter.ToInt32(array, 12);
					text = null;
					if (num2 != 0)
					{
						text = ReadZeroTermString(array, num2).Trim();
					}
					num2 = BitConverter.ToInt32(array, 16);
					if (num2 != 0)
					{
						text = text + '-' + ReadZeroTermString(array, num2).Trim();
					}
					num2 = BitConverter.ToInt32(array, 20);
					if (num2 != 0)
					{
						text = text + '.' + ReadZeroTermString(array, num2).Trim();
					}
					return "VEND:" + text;
				}
				return null;
			}
			finally
			{
				if (handle.ToInt32() != -1)
				{
					SafeNativeMethods.CloseHandle(handle);
				}
			}
		}

		private static string GetHddSerialNumberScsi(string deviceNumber)
		{
			IntPtr handle = SafeNativeMethods.CreateFile(string.Format("\\\\.\\Scsi{0}:", deviceNumber), 3221225472u, 3u, IntPtr.Zero, 3u, 0u, IntPtr.Zero);
			if (handle.ToInt32() == -1)
			{
				return null;
			}
			try
			{
				int num = 0;
				byte[] array;
				while (true)
				{
					if (num < 2)
					{
						array = new byte[557];
						Buffer.BlockCopy(ScsiQuery, 0, array, 0, ScsiQuery.Length);
						GCHandle gCHandle = GCHandle.Alloc(array, GCHandleType.Pinned);
						int num2 = 0;
						if (SafeNativeMethods.DeviceIoControl(handle, 315400, gCHandle.AddrOfPinnedObject(), 60, array, array.Length, ref num2, IntPtr.Zero))
						{
							break;
						}
						gCHandle.Free();
						num++;
						continue;
					}
					return null;
				}
				for (int i = 64; i < 80; i += 2)
				{
					Array.Reverse(array, i, 2);
				}
				return Encoding.ASCII.GetString(array, 64, 16);
			}
			finally
			{
				if (handle.ToInt32() != -1)
				{
					SafeNativeMethods.CloseHandle(handle);
				}
			}
		}

		private static string MapFileToDeviceNumber(string fileName, out string drive)
		{
			drive = null;
			if (fileName != null && fileName.Length != 0)
			{
				StringBuilder stringBuilder = new StringBuilder(255);
				string pathRoot = Path.GetPathRoot(fileName);
				drive = ((pathRoot == fileName) ? pathRoot : pathRoot.Substring(0, pathRoot.Length - 1));
				SafeNativeMethods.QueryDosDevice(drive, stringBuilder, 255);
				if (stringBuilder.Length > 0)
				{
					pathRoot = Path.GetDirectoryName(stringBuilder.ToString());
					if (pathRoot == "\\Device")
					{
						pathRoot = stringBuilder.ToString();
					}
					int num = pathRoot.Length;
					while (num > 0 && char.IsDigit(pathRoot[num - 1]))
					{
						num--;
					}
					if (num > 0)
					{
						return pathRoot.Substring(num);
					}
				}
				return null;
			}
			return null;
		}

		private static string FlipAndCodeBytes(byte[] buffer, int offset)
		{
			StringBuilder stringBuilder = new StringBuilder();
			int num = 0;
			for (int i = offset; i < buffer.Length && buffer[i] != 0; i++)
			{
				num++;
			}
			for (int j = 0; j < num; j += 4)
			{
				for (int num2 = 1; num2 >= 0; num2--)
				{
					int num3 = 0;
					for (int k = 0; k < 2; k++)
					{
						num3 *= 16;
						byte b = buffer[offset + j + num2 * 2 + k];
						if (b >= 48 && b <= 57)
						{
							num3 += b - 48;
						}
						else if (b >= 97 && b <= 102)
						{
							num3 += b - 81;
						}
						else if (b >= 65 && b <= 70)
						{
							num3 += b - 49;
						}
					}
					if (num3 > 0)
					{
						stringBuilder.Append((char)num3);
					}
				}
			}
			return stringBuilder.ToString();
		}

		private static string ReadZeroTermString(byte[] data, int start)
		{
			int num = start;
			byte b;
			do
			{
				b = data[num];
				num++;
			}
			while (b != 0);
			return Encoding.ASCII.GetString(data, start, num - start - 1);
		}

		private static string GetPhysicalMemory()
		{
			ulong num = 0uL;
			try
			{
				byte[] array = new byte[128];
				if (Environment.OSVersion.Platform == PlatformID.Win32NT && !(Environment.OSVersion.Version < new Version(4, 0)))
				{
					array[0] = 64;
					SafeNativeMethods.GlobalMemoryStatusEx(array);
					num = BitConverter.ToUInt64(array, 8);
				}
				else
				{
					array[0] = 32;
					SafeNativeMethods.GlobalMemoryStatus(array);
					num = BitConverter.ToUInt32(array, 8);
				}
			}
			catch (Exception)
			{
				return "Exception";
			}
			return num.ToString(CultureInfo.InvariantCulture);
		}

		private static string GetCPUID()
		{
			byte[] array = new byte[48];
			Native.Cpuid(array);
			if (Use64BitCompatibleCpuid)
			{
				for (int i = 40; i < array.Length; i++)
				{
					array[i] = 0;
				}
			}
			char[] array2 = new char[array.Length];
			for (int j = 0; j < array.Length; j++)
			{
				array2[j] = (char)array[j];
			}
			return new string(array2);
		}

		private static string[] GetMacAddress()
		{
			try
			{
				byte[] array = null;
				uint num = 0u;
				int[] array2 = SafeNativeMethods.IsWin64 ? _offsets64 : _offsets32;
				int num2 = 0;
				SafeNativeMethods.GetAdaptersInfo(null, ref num);
				array = new byte[num];
				SafeNativeMethods.GetAdaptersInfo(array, ref num);
				if (array != null && array.Length != 0)
				{
					ArrayList arrayList = new ArrayList();
					while (true)
					{
						int num3 = BitConverter.ToInt32(array, array2[0]);
						if (num3 == 6)
						{
							string trimmedString = GetTrimmedString(array, array2[1], 128);
							if (trimmedString.IndexOf("Windows Mobile") <= -1 && trimmedString.IndexOf("Bluetooth") <= -1 && trimmedString.IndexOf("VPN") <= -1 && (array[array2[4]] & 2) != 2)
							{
								string trimmedString2 = GetTrimmedString(array, array2[2], 256);
								using (RegistryKey registryKey = SafeToolbox.GetRegistryKey("HKLM\\SYSTEM\\CurrentControlSet\\Control\\Class\\{4D36E972-E325-11CE-BFC1-08002bE10318}", false))
								{
									if (registryKey != null)
									{
										bool flag = false;
										string[] subKeyNames = registryKey.GetSubKeyNames();
										foreach (string name in subKeyNames)
										{
											using (RegistryKey registryKey2 = registryKey.OpenSubKey(name, false))
											{
												string text = registryKey2.GetValue("NetCfgInstanceId") as string;
												if (text != null && string.Compare(text, trimmedString2, true) == 0)
												{
													string text2 = registryKey2.GetValue("NetworkAddress") as string;
													if (text2 != null && text2.Length > 0)
													{
														flag = true;
														num2++;
													}
													break;
												}
											}
										}
										if (flag)
										{
											goto IL_004e;
										}
									}
								}
								StringBuilder stringBuilder = new StringBuilder();
								int num4 = BitConverter.ToInt32(array, array2[3]) + array2[4];
								for (int j = array2[4]; j < num4; j++)
								{
									byte b = array[j];
									stringBuilder.AppendFormat(CultureInfo.InvariantCulture, "{0:X2}:", b);
								}
								arrayList.Add(stringBuilder.ToString());
							}
						}
						goto IL_004e;
						IL_004e:
						if (SafeNativeMethods.IsWin64)
						{
							long num5 = BitConverter.ToInt64(array, 0);
							if (num5 == 0L)
							{
								break;
							}
							Marshal.Copy(new IntPtr(num5), array, 0, array2[5]);
						}
						else
						{
							int num6 = BitConverter.ToInt32(array, 0);
							if (num6 == 0)
							{
								break;
							}
							Marshal.Copy(new IntPtr(num6), array, 0, array2[5]);
						}
					}
					if (num2 > 0)
					{
						if (arrayList.Count > 0)
						{
							while (num2 > 0)
							{
								arrayList.Add("MANUALSAFE");
								num2--;
							}
						}
						else
						{
							while (num2 > 0)
							{
								arrayList.Add(new Random().Next(1048575).ToString("X05"));
								num2--;
							}
						}
					}
					if (arrayList.Count == 0)
					{
						return null;
					}
					return arrayList.ToArray(typeof(string)) as string[];
				}
				return null;
			}
			catch
			{
			}
			return null;
		}

		private static string GetTrimmedString(byte[] data, int startIndex, int maxLen)
		{
			int num = 0;
			while (true)
			{
				if (num < maxLen)
				{
					if (data[num + startIndex] == 0)
					{
						break;
					}
					num++;
					continue;
				}
				return Encoding.ASCII.GetString(data, startIndex, maxLen);
			}
			return Encoding.ASCII.GetString(data, startIndex, num);
		}

		private static string[] GetHardwareId(string classId, bool diagnostic, string pnpClass, params string[] pnpSubClasses)
		{
			EnumHardware();
			ArrayList arrayList = new ArrayList();
			foreach (HardwareEntry hardwareEntry in _hardwareEntries)
			{
				HardwareEntry entry = hardwareEntry;
				if (!(entry.ClassGUID != classId))
				{
					if (pnpClass != null)
					{
						string[] hardwareIds = entry.HardwareIds;
						foreach (string text in hardwareIds)
						{
							int num = text.IndexOf("&CC_");
							if (num >= 0 && string.Compare(pnpClass, 0, text, num + 4, 2) == 0)
							{
								if (pnpSubClasses != null && pnpSubClasses.Length > 0)
								{
									bool flag = false;
									int num2 = 0;
									while (num2 < pnpSubClasses.Length)
									{
										string strA = pnpSubClasses[num2];
										if (string.Compare(strA, 0, text, num + 6, 2) != 0)
										{
											num2++;
											continue;
										}
										AppendHardwareEntry(diagnostic, arrayList, entry);
										flag = true;
										break;
									}
									if (flag)
									{
										break;
									}
								}
								else
								{
									AppendHardwareEntry(diagnostic, arrayList, entry);
								}
							}
						}
					}
					else
					{
						AppendHardwareEntry(diagnostic, arrayList, entry);
					}
				}
			}
			return arrayList.ToArray(typeof(string)) as string[];
		}

		private static void AppendHardwareEntry(bool diagnostic, ArrayList sigs, HardwareEntry entry)
		{
			string text = (!diagnostic) ? entry.DeviceId : string.Format("{0}\0[{1}]", entry.DeviceId, entry.DeviceDesc);
			if (!sigs.Contains(text))
			{
				sigs.Add(text);
			}
		}

		private static void EnumHardware()
		{
			if (_hardwareEntries == null)
			{
				lock (typeof(MachineProfile))
				{
					_hardwareEntries = new ArrayList();
					ArrayList arrayList = new ArrayList();
					arrayList.Add("{4D36E97B-E325-11CE-BFC1-08002BE10318}");
					arrayList.Add("{4D36E965-E325-11CE-BFC1-08002BE10318}");
					arrayList.Add("{4D36E96A-E325-11CE-BFC1-08002BE10318}");
					arrayList.Add("{4D36E968-E325-11CE-BFC1-08002BE10318}");
					arrayList.Add("{4D36E97D-E325-11CE-BFC1-08002BE10318}");
					using (RegistryKey registryKey = Registry.LocalMachine.OpenSubKey("SYSTEM\\CurrentControlSet\\Enum", false))
					{
						if (registryKey != null)
						{
							string[] array = new string[3]
							{
								"ACPI",
								"PCI",
								"IDE"
							};
							int num = 0;
							while (true)
							{
								if (num >= array.Length)
								{
									break;
								}
								string text = array[num];
								using (RegistryKey registryKey2 = registryKey.OpenSubKey(text, false))
								{
									if (registryKey2 != null)
									{
										string[] subKeyNames = registryKey2.GetSubKeyNames();
										int num2 = 0;
										while (true)
										{
											if (num2 >= subKeyNames.Length)
											{
												break;
											}
											string name = subKeyNames[num2];
											using (RegistryKey registryKey3 = registryKey2.OpenSubKey(name, false))
											{
												if (registryKey3 != null)
												{
													string[] subKeyNames2 = registryKey3.GetSubKeyNames();
													int num3 = 0;
													while (true)
													{
														if (num3 >= subKeyNames2.Length)
														{
															break;
														}
														string name2 = subKeyNames2[num3];
														using (RegistryKey registryKey4 = registryKey3.OpenSubKey(name2, false))
														{
															if (registryKey4 != null)
															{
																string text2 = registryKey4.GetValue("ClassGUID") as string;
																text2 = ((text2 == null) ? null : text2.ToUpper());
																if (text2 != null && arrayList.Contains(text2))
																{
																	HardwareEntry hardwareEntry = default(HardwareEntry);
																	hardwareEntry.Path = registryKey4.Name;
																	hardwareEntry.ClassGUID = text2;
																	hardwareEntry.DeviceDesc = (registryKey4.GetValue("DeviceDesc") as string);
																	hardwareEntry.Bus = text;
																	hardwareEntry.HardwareIds = (registryKey4.GetValue("HardwareID") as string[]);
																	if (hardwareEntry.HardwareIds != null)
																	{
																		int num4 = hardwareEntry.Path.IndexOf(hardwareEntry.HardwareIds[0]);
																		if (num4 != -1)
																		{
																			hardwareEntry.DeviceId = hardwareEntry.Path.Substring(num4);
																			IntPtr intPtr;
																			if (SafeNativeMethods.CM_Locate_DevNode(out intPtr, hardwareEntry.DeviceId, 0) == 0)
																			{
																				_hardwareEntries.Add(hardwareEntry);
																			}
																		}
																	}
																}
															}
														}
														num3++;
													}
												}
											}
											num2++;
										}
									}
								}
								num++;
							}
						}
					}
				}
			}
		}

		private static string[] GetMotherboardIds(bool diagnostic)
		{
			ArrayList arrayList = new ArrayList();
			string[] array = null;
			array = GetHardwareId("{4D36E97D-E325-11CE-BFC1-08002BE10318}", diagnostic, "05");
			if (array != null && array.Length > 0)
			{
				arrayList.AddRange(array);
			}
			array = GetHardwareId("{4D36E97D-E325-11CE-BFC1-08002BE10318}", diagnostic, "06");
			if (array != null && array.Length > 0)
			{
				arrayList.AddRange(array);
			}
			array = GetHardwareId("{4D36E97D-E325-11CE-BFC1-08002BE10318}", diagnostic, "08");
			if (array != null && array.Length > 0)
			{
				arrayList.AddRange(array);
			}
			array = GetHardwareId("{4D36E97D-E325-11CE-BFC1-08002BE10318}", diagnostic, "0C", "03", "00");
			if (array != null && array.Length > 0)
			{
				arrayList.AddRange(array);
			}
			if (diagnostic)
			{
				return arrayList.ToArray(typeof(string)) as string[];
			}
			long num = 0L;
			foreach (string item in arrayList)
			{
				num += SharedToolbox.GetHashCode(item);
			}
			return new string[1]
			{
				num.ToString()
			};
		}
	}
}
