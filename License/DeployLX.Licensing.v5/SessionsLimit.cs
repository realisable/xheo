using System;
using System.Collections;
using System.Web;
using System.Web.Services;
using System.Web.SessionState;
using System.Web.UI;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	[LimitEditor("SessionsLimitEditor", Category = "Web Limits", IconResource = "asmres://DeployLX.Licensing.DlxAddIn.v5/DeployLX.Licensing.DlxAddIn.v5.Resources.Limits.Sessions.png")]
	public class SessionsLimit : Limit
	{
		private int _allowed = 1;

		private int _timeout = 60;

		public int Allowed
		{
			get
			{
				return _allowed;
			}
			set
			{
				base.AssertNotReadOnly();
				Check.GreaterThan(value, 0, "Allowed");
				if (_allowed != value)
				{
					_allowed = value;
					base.OnChanged("Allowed");
				}
			}
		}

		public int Timeout
		{
			get
			{
				return _timeout;
			}
			set
			{
				base.AssertNotReadOnly();
				Check.GreaterThan(value, 0, "Timeout");
				if (_timeout != value)
				{
					_timeout = value;
					base.OnChanged("Timeout");
				}
			}
		}

		public override string Description
		{
			get
			{
				return SR.GetString("M_SessionsLimitDescription");
			}
		}

		public override string Name
		{
			get
			{
				return "Sessions";
			}
		}

		public override ValidationResult Validate(SecureLicenseContext context)
		{
			if (!IsSessionValid(context, true))
			{
				return ValidationResult.Invalid;
			}
			return base.Validate(context);
		}

		protected bool IsSessionValid(SecureLicenseContext context, bool reportError)
		{
			HttpContext httpContext = context.GetHttpContext();
			HttpSessionState httpSessionState = null;
			if (httpContext != null)
			{
				httpSessionState = httpContext.Session;
			}
			if (httpContext != null && httpSessionState != null)
			{
				if (httpSessionState.Timeout * 60 < Timeout)
				{
					if (reportError)
					{
						context.ReportError("E_SessionTimeoutTooShort", this, SafeToolbox.FormatTime(Timeout, 3600L, true));
					}
					return false;
				}
				string key = '#' + base.UniqueId;
				Hashtable hashtable = base.License.RuntimeState[key] as Hashtable;
				if (hashtable == null)
				{
					hashtable = new Hashtable();
					base.License.RuntimeState[key] = hashtable;
				}
				if (hashtable[httpSessionState.SessionID] == null)
				{
					if (hashtable.Count >= _allowed)
					{
						foreach (string key2 in hashtable.Keys)
						{
							DateTime t = ((DateTime)hashtable[key2]).AddSeconds((double)_timeout);
							if (t >= context.CurrentDateAndTime)
							{
								continue;
							}
							hashtable.Remove(key2);
							break;
						}
						if (hashtable.Count >= _allowed)
						{
							if (reportError)
							{
								context.ReportError("E_TooManySessions", this);
							}
							return false;
						}
					}
					hashtable[httpSessionState.SessionID] = context.CurrentDateAndTime;
					return true;
				}
				httpSessionState["Ping"] = context.CurrentDateAndTime;
				hashtable[httpSessionState.SessionID] = context.CurrentDateAndTime;
				return true;
			}
			if (!(context.Instance is Control) && !(context.Instance is WebService))
			{
				return true;
			}
			if (reportError)
			{
				context.ReportError("E_MissionContextOrSession", this);
			}
			return false;
		}

		public override ValidationResult Granted(SecureLicenseContext context)
		{
			if (!IsSessionValid(context, true))
			{
				return ValidationResult.Invalid;
			}
			return base.Granted(context);
		}

		public override PeekResult Peek(SecureLicenseContext context)
		{
			if (!IsSessionValid(context, false))
			{
				return PeekResult.Invalid;
			}
			return base.Peek(context);
		}

		protected override bool ReadFromXml(XmlReader reader)
		{
			string attribute = reader.GetAttribute("allowed");
			if (attribute != null)
			{
				_allowed = SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_allowed = 1;
			}
			attribute = reader.GetAttribute("timeout");
			if (attribute != null)
			{
				_timeout = SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_timeout = 60;
			}
			return base.ReadChildLimits(reader);
		}

		protected override bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			if (_allowed != 1)
			{
				writer.WriteAttributeString("allowed", _allowed.ToString());
			}
			if (_timeout != 60)
			{
				writer.WriteAttributeString("timeout", _timeout.ToString());
			}
			return true;
		}
	}
}
