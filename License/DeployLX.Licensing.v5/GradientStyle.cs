namespace DeployLX.Licensing.v5
{
	public enum GradientStyle
	{
		Linear,
		Reflected,
		Radial
	}
}
