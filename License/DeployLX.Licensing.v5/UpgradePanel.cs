using System;
using System.Collections.Specialized;
using System.Drawing;
using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	public class UpgradePanel : SplitPanel
	{
		private RegistrationLimit _registrationLimit;

		private Button _upgrade;

		private ShadowLabel _upgradeTitle;

		private SkinnedPanel _upgradePanel;

		private LicenseCodeTextBox _serialNumber;

		private ThemeLabel _serialPrompt;

		private ShadowLabel _upgradeNotice;

		private SecureLicense _upgradeLicense;

		public SecureLicense UpgradeLicense
		{
			get
			{
				return _upgradeLicense;
			}
		}

		public UpgradePanel(SecureLicense license, RegistrationLimit registrationLimit)
			: base(null)
		{
			Check.NotNull(license, "license");
			Check.NotNull(registrationLimit, "registrationLimit");
			InitializeComponent();
			_registrationLimit = registrationLimit;
			_upgradeLicense = license;
		}

		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			_upgradeTitle = new ShadowLabel();
			_upgradePanel = new SkinnedPanel();
			_serialPrompt = new ThemeLabel();
			_serialNumber = new LicenseCodeTextBox();
			_upgradeNotice = new ShadowLabel();
			base.BodyPanel.SuspendLayout();
			_upgradePanel.SuspendLayout();
			base.SuspendLayout();
			base.SidePanel.Location = new Point(0, 0);
			base.SidePanel.Size = new Size(170, 421);
			base.SidePanel.TabIndex = 0;
			base.BodyPanel.Controls.Add(_upgradeNotice);
			base.BodyPanel.Controls.Add(_upgradePanel);
			base.BodyPanel.Controls.Add(_upgradeTitle);
			base.BodyPanel.Location = new Point(170, 0);
			base.BodyPanel.Size = new Size(514, 421);
			_upgradeTitle.Location = new Point(14, 16);
			_upgradeTitle.Name = "_upgradeTitle";
			_upgradeTitle.Size = new Size(482, 34);
			_upgradeTitle.TabIndex = 0;
			_upgradeTitle.Text = "#UI_UpgradeTitle";
			_upgradeTitle.ThemeColor = new string[2]
			{
				"PanelTitleText",
				"Text"
			};
			_upgradePanel.Anchor = (AnchorStyles.Bottom | AnchorStyles.Left);
			_upgradePanel.BackColor = Color.Transparent;
			_upgradePanel.BackgroundImageLayout = ImageLayout.None;
			_upgradePanel.Controls.Add(_serialPrompt);
			_upgradePanel.Controls.Add(_serialNumber);
			_upgradePanel.Location = new Point(13, 132);
			_upgradePanel.Name = "_upgradePanel";
			_upgradePanel.PaintFakeBackground = true;
			_upgradePanel.Size = new Size(488, 90);
			_upgradePanel.TabIndex = 0;
			_serialPrompt.AutoSize = true;
			_serialPrompt.Location = new Point(15, 15);
			_serialPrompt.Name = "_serialPrompt";
			_serialPrompt.Size = new Size(147, 15);
			_serialPrompt.TabIndex = 1;
			_serialPrompt.Text = "#UI_PreviousSerialNumber";
			_serialPrompt.ThemeFont = ThemeFont.Bold;
			_serialNumber.CharacterSet = null;
			_serialNumber.Location = new Point(15, 37);
			_serialNumber.Mask = null;
			_serialNumber.Name = "_serialNumber";
			_serialNumber.Prompt = null;
			_serialNumber.Size = new Size(458, 23);
			_serialNumber.TabIndex = 0;
			_upgradeNotice.Location = new Point(16, 49);
			_upgradeNotice.Name = "_upgradeNotice";
			_upgradeNotice.Size = new Size(482, 80);
			_upgradeNotice.TabIndex = 13;
			_upgradeNotice.Text = "#UI_UpgradeNotice";
			_upgradeNotice.ThemeFont = ThemeFont.Medium;
			base.AutoScaleDimensions = new SizeF(96f, 96f);
			base.Name = "UpgradePanel";
			base.BodyPanel.ResumeLayout(false);
			_upgradePanel.ResumeLayout(false);
			_upgradePanel.PerformLayout();
			base.ResumeLayout(false);
		}

		protected internal override void LoadPanel(SecureLicenseContext context)
		{
			base.LoadPanel(context);
			if (_registrationLimit.LogoResource != null)
			{
				base.Logo = Toolbox.GetImage(_registrationLimit.LogoResource, context);
			}
			string text = null;
			string text2 = null;
			Limit[] array = _upgradeLicense.Limits.FindLimitsByType(typeof(UpgradeLimit), true);
			for (int i = 0; i < array.Length; i++)
			{
				UpgradeLimit upgradeLimit = (UpgradeLimit)array[i];
				if (text2 == null)
				{
					text2 = upgradeLimit.SerialNumberInfo.CharacterSet;
					goto IL_007e;
				}
				if (!(text2 != upgradeLimit.SerialNumberInfo.CharacterSet))
				{
					goto IL_007e;
				}
				text2 = null;
				text = null;
				break;
				IL_007e:
				if (text == null)
				{
					text = upgradeLimit.SerialNumberMask;
					continue;
				}
				if (!(text != upgradeLimit.SerialNumberMask))
				{
					continue;
				}
				text2 = null;
				text = null;
				break;
			}
			_serialNumber.Mask = text;
			_serialNumber.CharacterSet = text2;
			_serialNumber.Font = new Font("Lucida Console", (float)((text == null || text.Length < 41) ? 14 : 10), FontStyle.Regular);
			_serialNumber.HideMaskOnLeave = true;
			try
			{
				_serialNumber.Text = ((StringDictionary)context.RequestInfo.RegistrationInfo)["suggestedupgradeserialnumber"];
			}
			catch
			{
			}
		}

		protected internal override void InitializePanel()
		{
			Button button = base.AddBottomButton("UI_Cancel", 30);
			button.Click += _cancel_Click;
			base._superForm.CancelButton = button;
			_upgrade = base.AddBottomButton("UI_Register", 30);
			_upgrade.Click += _upgrade_Click;
			base._superForm.AcceptButton = _upgrade;
		}

		protected virtual void HandleUpgrade()
		{
			_serialNumber.Text = _serialNumber.Text.Trim();
			if (_serialNumber.Text.Length == 0)
			{
				base.ShowMessageBox(SR.GetString("UI_FieldRequiredNotice", SR.GetString("UI_PreviousSerialNumber")), null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				_serialNumber.Select();
			}
			else
			{
				UpgradeLimit upgradeLimit = _registrationLimit.FindTargetUpgrade(base._superForm.Context, _upgradeLicense, _serialNumber.Text);
				if (upgradeLimit != null && upgradeLimit.CanUnlock(base._superForm.Context, _serialNumber.Text))
				{
					((StringDictionary)base._superForm.Context.RequestInfo.RegistrationInfo)["upgradeSerialNumber"] = _serialNumber.Text;
					base.Return(FormResult.Success);
				}
				else
				{
					base.ShowMessageBox(SR.GetString("E_InvalidSerialOrFailure"), null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
		}

		private void _upgrade_Click(object sender, EventArgs e)
		{
			HandleUpgrade();
		}

		private void _cancel_Click(object sender, EventArgs e)
		{
			base.Return(FormResult.Failure);
		}

		protected internal override void PanelShown(bool safeToChange)
		{
			base.PanelShown(safeToChange);
			base.SelectNextControl(_upgradePanel, true, true, true, true);
			if (safeToChange)
			{
				PopulatePotentialSerialNumber();
			}
		}

		private void PopulatePotentialSerialNumber()
		{
			if (_serialNumber.Text.Length == 0)
			{
				string potentialSerialFromClipboard = base.SuperForm.Context.GetPotentialSerialFromClipboard();
				if (potentialSerialFromClipboard != null)
				{
					try
					{
						_serialNumber.Text = potentialSerialFromClipboard;
					}
					catch (ArgumentOutOfRangeException)
					{
					}
				}
			}
		}

		protected internal override void FormActivated()
		{
			base.FormActivated();
			PopulatePotentialSerialNumber();
		}

		protected internal override void UpdateFromTheme()
		{
			base.UpdateFromTheme();
			_upgradeTitle.Font = base.SuperForm.TitleFont;
		}
	}
}
