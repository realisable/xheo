using System;
using System.IO;

namespace DeployLX.Licensing.v5
{
	internal interface ICustomProtocol
	{
		bool Navigate
		{
			get;
		}

		string Scheme
		{
			get;
		}

		bool CanProcessUri(Uri uri);

		Stream GetResourceStream(Uri uri);
	}
}
