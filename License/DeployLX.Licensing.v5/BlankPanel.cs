using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	internal sealed class BlankPanel : SuperFormPanel
	{
		public BlankPanel()
			: base(null)
		{
			Dock = DockStyle.Fill;
		}

		protected internal override void InitializePanel()
		{
		}
	}
}
