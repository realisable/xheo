namespace DeployLX.Licensing.v5
{
	public enum PersistentDataLocationType
	{
		NotSet,
		User,
		Shared,
		SharedOrUser
	}
}
