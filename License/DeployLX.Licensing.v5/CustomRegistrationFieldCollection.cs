using System;
using System.Collections;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public sealed class CustomRegistrationFieldCollection : WithEventsCollection
	{
		public CustomRegistrationField this[int index]
		{
			get
			{
				return base.List[index] as CustomRegistrationField;
			}
			set
			{
				base.List[index] = value;
			}
		}

		public CustomRegistrationField this[string name]
		{
			get
			{
				foreach (CustomRegistrationField item in (IEnumerable)this)
				{
					if (string.Compare(item.Name, name, true) == 0)
					{
						return item;
					}
				}
				return null;
			}
		}

		public void Add(CustomRegistrationField customField)
		{
			base.List.Add(customField);
		}

		public void Insert(int index, CustomRegistrationField customField)
		{
			base.List.Insert(index, customField);
		}

		public void Remove(CustomRegistrationField customField)
		{
			base.List.Remove(customField);
		}

		public void CopyTo(CustomRegistrationField[] array, int index)
		{
			base.List.CopyTo(array, index);
		}

		public int IndexOf(CustomRegistrationField customField)
		{
			return base.List.IndexOf(customField);
		}

		public bool Contains(CustomRegistrationField customField)
		{
			return base.List.Contains(customField);
		}
	}
}
