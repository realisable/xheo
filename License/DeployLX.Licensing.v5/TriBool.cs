using System;
using System.ComponentModel;
using System.Globalization;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	[TypeConverter(typeof(TriBoolConverter))]
	[CLSCompliant(true)]
	public struct TriBool : IComparable, IFormattable
	{
		public const TriBoolValue No = TriBoolValue.No;

		public const TriBoolValue Yes = TriBoolValue.Yes;

		public const TriBoolValue Default = TriBoolValue.Default;

		private TriBoolValue _value;

		public TriBoolValue Value
		{
			get
			{
				return _value;
			}
			set
			{
				_value = value;
			}
		}

		public bool IsSet
		{
			get
			{
				return _value != TriBoolValue.Default;
			}
		}

		public TriBool(TriBoolValue value)
		{
			_value = value;
		}

		public TriBool(bool value)
		{
			_value = (value ? TriBoolValue.Yes : TriBoolValue.No);
		}

		public int CompareTo(object obj)
		{
			TriBool triBool = (TriBool)obj;
			if (!IsSet && !triBool.IsSet)
			{
				return 0;
			}
			if (!IsSet)
			{
				return -1;
			}
			if (!triBool.IsSet)
			{
				return 1;
			}
			return ((Enum)(object)Value).CompareTo((object)triBool.Value);
		}

		public override string ToString()
		{
			return ToString("", CultureInfo.InvariantCulture);
		}

		public string ToString(string format, IFormatProvider formatProvider)
		{
			return Value.ToString();
		}

		public bool Resolve(bool defaultValue)
		{
			if (_value == TriBoolValue.Default)
			{
				return defaultValue;
			}
			return _value == TriBoolValue.Yes;
		}

		public static bool operator ==(TriBool value, TriBool value2)
		{
			return value.Equals(value2);
		}

		public static bool operator <(TriBool value, TriBool value2)
		{
			return value.CompareTo(value2) < 0;
		}

		public static bool operator >(TriBool value, TriBool value2)
		{
			return value.CompareTo(value2) > 0;
		}

		public static bool operator !=(TriBool value, TriBool value2)
		{
			return !value.Equals(value2);
		}

		public static implicit operator bool(TriBool value)
		{
			if (!value.IsSet)
			{
				throw new SecureLicenseException("E_ValueNotSet");
			}
			return value.Value == TriBoolValue.Yes;
		}

		public static implicit operator TriBool(bool value)
		{
			return new TriBool(value ? TriBoolValue.Yes : TriBoolValue.No);
		}

		public static implicit operator TriBool(int value)
		{
			switch (value)
			{
			case 1:
				return new TriBool(TriBoolValue.Yes);
			case 0:
				return new TriBool(TriBoolValue.No);
			default:
				return TriBoolValue.Default;
			}
		}

		public static implicit operator int(TriBool value)
		{
			return (int)value.Value;
		}

		public static implicit operator TriBool(TriBoolValue value)
		{
			return new TriBool(value);
		}

		public static implicit operator TriBoolValue(TriBool value)
		{
			return value.Value;
		}

		public static TriBool FromString(string value)
		{
			if (value != null && value.Length != 0 && string.Compare(value, "Default", true) != 0 && !(value == "D"))
			{
				if (string.Compare(value, "Yes", true) != 0 && string.Compare(value, "True", true) != 0 && !(value == "T") && !(value == "1") && !(value == "Y"))
				{
					if (string.Compare(value, "No", true) != 0 && string.Compare(value, "False", true) != 0 && !(value == "F") && !(value == "0") && !(value == "N"))
					{
						throw new InvalidCastException();
					}
					return TriBoolValue.No;
				}
				return TriBoolValue.Yes;
			}
			return TriBoolValue.Default;
		}

		public override bool Equals(object obj)
		{
			if (obj == null)
			{
				return false;
			}
			TriBool triBool = (TriBool)obj;
			return _value == triBool.Value;
		}

		public override int GetHashCode()
		{
			return _value.GetHashCode();
		}
	}
}
