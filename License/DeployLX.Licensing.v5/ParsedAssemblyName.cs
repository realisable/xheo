using System;
using System.Globalization;
using System.Reflection;
using System.Text;

namespace DeployLX.Licensing.v5
{
	internal sealed class ParsedAssemblyName
	{
		private string _name;

		private Version _version;

		private string _culture;

		private string _publicKeyToken;

		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}

		public Version Version
		{
			get
			{
				return _version;
			}
			set
			{
				_version = value;
			}
		}

		public string Culture
		{
			get
			{
				return _culture;
			}
			set
			{
				_culture = value;
			}
		}

		public string PublicKeyToken
		{
			get
			{
				return _publicKeyToken;
			}
			set
			{
				_publicKeyToken = value;
			}
		}

		public ParsedAssemblyName(string name)
		{
			Check.NotNull(name, "name");
			string[] array = name.Split(',');
			string[] array2 = array;
			foreach (string text in array2)
			{
				int num = text.IndexOf('=');
				if (num == -1)
				{
					_name = text.Trim();
				}
				else
				{
					string text2 = text.Substring(0, num).ToLower(CultureInfo.InvariantCulture).Trim();
					switch (text2)
					{
					case "publickeytoken":
						_publicKeyToken = text.Substring(num + 1).Trim();
						break;
					case "culture":
						_culture = text.Substring(num + 1).Trim();
						break;
					case "version":
						_version = new Version(text.Substring(num + 1).Trim());
						break;
					default:
						throw new NotSupportedException();
					}
				}
			}
		}

		public AssemblyName MakeAssemblyName()
		{
			AssemblyName assemblyName = new AssemblyName();
			assemblyName.Name = Name;
			assemblyName.Version = Version;
			if (Culture != null && Culture != "neutral")
			{
				assemblyName.CultureInfo = new CultureInfo(Culture);
			}
			if (PublicKeyToken != null)
			{
				byte[] array = new byte[8];
				for (int i = 0; i < PublicKeyToken.Length; i += 2)
				{
					byte b = (byte)Convert.ToInt32(PublicKeyToken.Substring(i, 2), 16);
					array[i >> 1] = b;
				}
				assemblyName.SetPublicKeyToken(array);
			}
			return assemblyName;
		}

		public string MakeFullName()
		{
			StringBuilder stringBuilder = new StringBuilder();
			if (_name != null)
			{
				stringBuilder.Append(_name);
				if (_version != (Version)null && _culture != null && _publicKeyToken != null)
				{
					stringBuilder.Append(", ");
					if (_version != (Version)null)
					{
						stringBuilder.Append("Version=");
						stringBuilder.Append(_version.ToString());
					}
					stringBuilder.Append(", ");
					if (_culture != null)
					{
						stringBuilder.Append("Culture=");
						stringBuilder.Append(_culture);
					}
					stringBuilder.Append(", ");
					if (_publicKeyToken != null)
					{
						stringBuilder.Append("PublicKeyToken=");
						stringBuilder.Append(_publicKeyToken);
					}
				}
			}
			return stringBuilder.ToString();
		}
	}
}
