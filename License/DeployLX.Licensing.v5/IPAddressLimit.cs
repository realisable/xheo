using System;
using System.Collections;
using System.Net;
using System.Web;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	[LimitEditor("IPAddressLimitEditor", Category = "Web Limits", IconResource = "asmres://DeployLX.Licensing.DlxAddIn.v5/DeployLX.Licensing.DlxAddIn.v5.Resources.Limits.Ip.png")]
	public class IPAddressLimit : Limit
	{
		private IPRangeCollection _ranges = new IPRangeCollection();

		public IPRangeCollection Ranges
		{
			get
			{
				return _ranges;
			}
		}

		public override string Description
		{
			get
			{
				return SR.GetString("M_IPLimitDescription");
			}
		}

		public override string Name
		{
			get
			{
				return "IP Address";
			}
		}

		public IPAddressLimit()
		{
			_ranges.Changed += _ranges_Changed;
		}

		private void _ranges_Changed(object sender, CollectionEventArgs e)
		{
			base.OnCollectionChanged(sender, "Ranges", e);
			if (base.License != null)
			{
				base.License.AttachChangeBubbler(e);
			}
		}

		public override ValidationResult Validate(SecureLicenseContext context)
		{
			if (IsIPValid(context, true))
			{
				return base.Validate(context);
			}
			return ValidationResult.Invalid;
		}

		protected bool IsIPValid(SecureLicenseContext context, bool reportError)
		{
			try
			{
				HttpContext httpContext = context.GetHttpContext();
				if (httpContext != null)
				{
					IPAddress iPAddress = IPAddress.Parse(httpContext.Request.ServerVariables["LOCAL_ADDR"]);
					foreach (IPRange item in (IEnumerable)Ranges)
					{
						if (item.Contains(iPAddress))
						{
							return true;
						}
					}
					if (reportError)
					{
						context.ReportError("E_InvalidRequestIP", this, iPAddress);
					}
				}
				else
				{
					IPHostEntry iPHostEntry = context.Items[base.LimitId + ".MachineIp"] as IPHostEntry;
					if (iPHostEntry == null)
					{
						iPHostEntry = Dns.Resolve(Environment.MachineName);
						context.Items[base.LimitId + ".MachineIp"] = iPHostEntry;
					}
					IPAddress[] addressList = iPHostEntry.AddressList;
					foreach (IPAddress address in addressList)
					{
						foreach (IPRange item2 in (IEnumerable)Ranges)
						{
							if (item2.Contains(address))
							{
								return true;
							}
						}
					}
					if (reportError)
					{
						context.ReportError("E_InvalidMachineIP", this);
					}
				}
			}
			catch (Exception exception)
			{
				if (reportError)
				{
					context.ReportError("E_InvalidMachineIP", this, exception, ErrorSeverity.High);
				}
			}
			return false;
		}

		public static int CompareIPAddresses(IPAddress ip1, IPAddress ip2)
		{
			if (ip1 == null && ip2 == null)
			{
				return 0;
			}
			if (ip1 == null)
			{
				return -1;
			}
			if (ip2 == null)
			{
				return 1;
			}
			if (ip1.AddressFamily != ip2.AddressFamily)
			{
				return ((Enum)(object)ip1.AddressFamily).CompareTo((object)ip2.AddressFamily);
			}
			byte[] addressBytes = ip1.GetAddressBytes();
			byte[] addressBytes2 = ip2.GetAddressBytes();
			int num = 0;
			int num2;
			while (true)
			{
				if (num < addressBytes.Length)
				{
					num2 = addressBytes[num].CompareTo(addressBytes2[num]);
					if (num2 != 0)
					{
						break;
					}
					num++;
					continue;
				}
				return 0;
			}
			return num2;
		}

		public override ValidationResult Granted(SecureLicenseContext context)
		{
			if (IsIPValid(context, true))
			{
				return base.Granted(context);
			}
			return ValidationResult.Invalid;
		}

		public override PeekResult Peek(SecureLicenseContext context)
		{
			if (IsIPValid(context, false))
			{
				return base.Peek(context);
			}
			return PeekResult.Invalid;
		}

		protected override bool ReadFromXml(XmlReader reader)
		{
			_ranges.Clear();
			if (reader.IsEmptyElement)
			{
				reader.Read();
				return true;
			}
			reader.Read();
			while (!reader.EOF)
			{
				if (reader.IsStartElement())
				{
					switch (reader.Name)
					{
					case "Limit":
						if (base.ReadChildLimit(reader))
						{
							break;
						}
						return false;
					case "Range":
					{
						string attribute = reader.GetAttribute("first");
						string attribute2 = reader.GetAttribute("last");
						if (attribute != null && attribute.Length != 0)
						{
							if (attribute2 != null && attribute2.Length != 0)
							{
								_ranges.Add(new IPRange(IPAddress.Parse(attribute), IPAddress.Parse(attribute2)));
								reader.Read();
								break;
							}
							return false;
						}
						return false;
					}
					default:
						reader.Skip();
						break;
					}
					continue;
				}
				reader.Read();
				break;
			}
			return true;
		}

		protected override bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			foreach (IPRange item in (IEnumerable)_ranges)
			{
				if (item.First == null || item.Last == null)
				{
					return false;
				}
				writer.WriteStartElement("Range");
				writer.WriteAttributeString("first", item.First.ToString());
				writer.WriteAttributeString("last", item.Last.ToString());
				writer.WriteEndElement();
			}
			return true;
		}
	}
}
