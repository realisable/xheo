using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	internal class SystemCursorPictureBox : PictureBox
	{
		protected override void WndProc(ref Message m)
		{
			if (Cursor == Cursors.Hand && SafeNativeMethods.PreFilterMessage(ref m))
			{
				return;
			}
			base.WndProc(ref m);
		}
	}
}
