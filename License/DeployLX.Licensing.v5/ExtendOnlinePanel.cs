using System;
using System.Drawing;
using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	public class ExtendOnlinePanel : SuperFormPanel
	{
		private sealed class RequestEntry
		{
			public AsyncValidationRequest Request;

			public ValidationResult Result;

			private EventHandler _completed;

			public RequestEntry(IExtendableLimit limit, SecureLicenseContext context, EventHandler completed)
			{
				Result = ValidationResult.Invalid;
				_completed = completed;
				Request = limit.ExtendAtServerAsync(context, true, Completed);
			}

			public void Completed(object sender, EventArgs e)
			{
				Result = Request.GetResult();
				if (_completed != null)
				{
					_completed(sender, e);
				}
			}
		}

		private RequestEntry[] _requests;

		private Limit[] _limits;

		private Timer _timer;

		private Button _cancel;

		private Button _goBack;

		private Button _retry;

		private bool _isSubscription;

		private ProgressBar _progressing;

		private ShadowLabel _extendingNotice;

		private Panel _errorPanel;

		private ShadowLabel _errorNotice;

		private Button _details;

		private PictureBox _errorImage;

		private ShadowLabel _successLabel;

		public ExtendOnlinePanel(Limit[] limits)
			: base(null)
		{
			InitializeComponent();
			try
			{
				_progressing.Style = ProgressBarStyle.Marquee;
			}
			catch
			{
				_timer = new Timer();
				_timer.Interval = 1000;
				_timer.Tick += _timer_Tick;
			}
			_errorImage.Image = Images.BigError7_png;
			_limits = limits;
			_isSubscription = true;
			int num = 0;
			while (true)
			{
				if (num < limits.Length)
				{
					IExtendableLimit extendableLimit = (IExtendableLimit)limits[num];
					if (!extendableLimit.IsSubscription)
					{
						break;
					}
					num++;
					continue;
				}
				return;
			}
			_isSubscription = false;
		}

		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			_progressing = new ProgressBar();
			_extendingNotice = new ShadowLabel();
			_errorPanel = new Panel();
			_errorImage = new PictureBox();
			_details = new SkinnedButton();
			_errorNotice = new ShadowLabel();
			_successLabel = new ShadowLabel();
			_errorPanel.SuspendLayout();
			base.SuspendLayout();
			_progressing.Location = new Point(146, 160);
			_progressing.Maximum = 10;
			_progressing.Name = "_progressing";
			_progressing.Size = new Size(392, 23);
			_progressing.TabIndex = 0;
			_extendingNotice.AutoSize = true;
			_extendingNotice.Location = new Point(143, 131);
			_extendingNotice.Name = "_extendingNotice";
			_extendingNotice.Size = new Size(4, 17);
			_extendingNotice.TabIndex = 1;
			_extendingNotice.ThemeFont = ThemeFont.Medium;
			_extendingNotice.BackColor = Color.Transparent;
			_errorPanel.BackColor = Color.Transparent;
			_errorPanel.Controls.Add(_errorImage);
			_errorPanel.Controls.Add(_details);
			_errorPanel.Controls.Add(_errorNotice);
			_errorPanel.Location = new Point(146, 212);
			_errorPanel.Name = "_errorPanel";
			_errorPanel.Size = new Size(395, 116);
			_errorPanel.TabIndex = 3;
			_errorPanel.Visible = false;
			_errorImage.Location = new Point(0, 0);
			_errorImage.Name = "_errorImage";
			_errorImage.Size = new Size(48, 48);
			_errorImage.TabIndex = 6;
			_errorImage.TabStop = false;
			_details.Location = new Point(297, 88);
			_details.Name = "_details";
			_details.Size = new Size(95, 26);
			_details.TabIndex = 5;
			_details.Text = "#UI_Details";
			_details.Click += _details_Click;
			_errorNotice.Location = new Point(54, 0);
			_errorNotice.Name = "_errorNotice";
			_errorNotice.Size = new Size(341, 72);
			_errorNotice.TabIndex = 2;
			_errorNotice.ThemeFont = ThemeFont.Medium;
			_successLabel.AutoSize = true;
			_successLabel.Location = new Point(144, 131);
			_successLabel.Name = "_successLabel";
			_successLabel.Size = new Size(122, 17);
			_successLabel.TabIndex = 4;
			_successLabel.Text = "#UI_ExtensionSuccess";
			_successLabel.ThemeFont = ThemeFont.Medium;
			_successLabel.Visible = false;
			_successLabel.BackColor = Color.Transparent;
			base.Controls.Add(_successLabel);
			base.Controls.Add(_errorPanel);
			base.Controls.Add(_extendingNotice);
			base.Controls.Add(_progressing);
			base.Name = "ExtendOnlinePanel";
			_errorPanel.ResumeLayout(false);
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		protected override void DestroyHandle()
		{
			base.DestroyHandle();
			if (_requests != null)
			{
				RequestEntry[] requests = _requests;
				foreach (RequestEntry requestEntry in requests)
				{
					if (requestEntry.Request != null && !requestEntry.Request.IsComplete)
					{
						requestEntry.Request.Cancel();
					}
				}
			}
		}

		public void StartRequest()
		{
			_extendingNotice.Text = SR.GetString(_isSubscription ? "UI_RenewingOnlineNotice" : "UI_ExtendingOnlineNotice");
			if (_requests == null)
			{
				_requests = new RequestEntry[_limits.Length];
			}
			for (int i = 0; i < _limits.Length; i++)
			{
				IExtendableLimit extendableLimit = _limits[i] as IExtendableLimit;
				if (extendableLimit.Servers.Count != 0 && (_requests[i] == null || _requests[i].Request == null || (_requests[i].Request.IsComplete && _requests[i].Result != ValidationResult.Valid && _requests[i].Result != ValidationResult.Retry)))
				{
					_requests[i] = new RequestEntry((IExtendableLimit)_limits[i], base._superForm.Context, Completed);
				}
			}
		}

		protected internal override void InitializePanel()
		{
			if (_timer != null)
			{
				_timer.Enabled = true;
			}
			_errorNotice.Text = SR.GetString(_isSubscription ? "UI_RenewalOnlineErrors" : "UI_ExtensionOnlineErrors");
			if (base.FirstInitialization)
			{
				_cancel = base.AddBottomButton("UI_Stop", 30);
			}
			else
			{
				_cancel = base.AddBottomButton("UI_Cancel", 30);
			}
			_cancel.Click += _cancel_Click;
			_retry = base.AddBottomButton("UI_Retry");
			_retry.Click += _retry_Click;
			_retry.Visible = !base.FirstInitialization;
			_goBack = base.AddBottomButton("UI_GoBack");
			_goBack.Image = Images.Back_png;
			_goBack.TextImageRelation = TextImageRelation.ImageBeforeText;
			_goBack.Click += _goBack_Click;
			_goBack.Visible = !base.FirstInitialization;
			base.SuperForm.CancelButton = _cancel;
			base.SuperForm.AcceptButton = _retry;
		}

		private void Completed(object sender, EventArgs e)
		{
			if (base.Parent != null)
			{
				bool flag = true;
				bool flag2 = false;
				bool flag3 = false;
				RequestEntry[] requests = _requests;
				foreach (RequestEntry requestEntry in requests)
				{
					if (requestEntry != null)
					{
						if (requestEntry.Request != null && !requestEntry.Request.IsComplete)
						{
							return;
						}
						flag2 |= (requestEntry.Result == ValidationResult.Canceled);
						flag3 |= (requestEntry.Result == ValidationResult.Retry);
						flag &= (requestEntry.Result == ValidationResult.Retry || requestEntry.Result == ValidationResult.Valid);
					}
				}
				if (flag2)
				{
					base.Return(FormResult.Incomplete);
				}
				else if (flag3)
				{
					base.Return(FormResult.Retry);
				}
				else if (flag)
				{
					base.BeginInvoke(new MethodInvoker(ShowOK));
				}
				else
				{
					base.BeginInvoke(new MethodInvoker(ShowErrorPanel));
				}
			}
		}

		private void ShowOK()
		{
			base.ShowControlsWithEffects(TriBoolValue.Default, _successLabel, _progressing, _cancel, _extendingNotice);
			base.ClearBottomControls();
			Button button = base.AddBottomButton("UI_OK");
			button.Click += ok_Click;
			base.SuperForm.AcceptButton = button;
			button.Select();
		}

		private void ShowErrorPanel()
		{
			_cancel.Text = SR.GetString("UI_Cancel");
			base.ShowControlsWithEffects(TriBoolValue.Yes, _errorPanel, _retry, _goBack);
			_retry.Select();
			_progressing.Style = ProgressBarStyle.Blocks;
		}

		private void ok_Click(object sender, EventArgs e)
		{
			base.Return((FormResult)524289);
		}

		private void _cancel_Click(object sender, EventArgs e)
		{
			bool flag = true;
			RequestEntry[] requests = _requests;
			foreach (RequestEntry requestEntry in requests)
			{
				flag &= requestEntry.Request.IsComplete;
			}
			if (flag)
			{
				base.Return((FormResult)524290);
			}
			else
			{
				RequestEntry[] requests2 = _requests;
				foreach (RequestEntry requestEntry2 in requests2)
				{
					requestEntry2.Request.Cancel();
				}
			}
		}

		private void _timer_Tick(object sender, EventArgs e)
		{
			_progressing.Increment(1);
			if (_progressing.Value == _progressing.Maximum)
			{
				_progressing.Value = _progressing.Minimum;
			}
			RequestEntry[] requests = _requests;
			foreach (RequestEntry requestEntry in requests)
			{
				if (requestEntry.Request != null && !requestEntry.Request.IsComplete)
				{
					return;
				}
			}
			_timer.Enabled = false;
			_progressing.Value = _progressing.Minimum;
		}

		private void _goBack_Click(object sender, EventArgs e)
		{
			base.Return(FormResult.Incomplete);
		}

		private void _retry_Click(object sender, EventArgs e)
		{
			_cancel.Text = SR.GetString("UI_Stop");
			base.ShowControlsWithEffects(TriBoolValue.No, _errorPanel, _retry, _goBack);
			if (_timer != null)
			{
				_timer.Enabled = true;
			}
			else
			{
				_progressing.Style = ProgressBarStyle.Marquee;
			}
			StartRequest();
		}

		private void _details_Click(object sender, EventArgs e)
		{
			ValidationRecordCollection records = new ValidationRecordCollection(base.SuperForm.Context.LatestValidationRecord);
			base.ShowPanel(new ErrorReportPanel(null, null, SR.GetString("UI_ErrorsConnectingToServer"), records, true), null);
		}

		protected internal override void PanelShown(bool safeToChange)
		{
			base.PanelShown(safeToChange);
			if (base._firstInitialization && safeToChange)
			{
				StartRequest();
			}
		}
	}
}
