namespace DeployLX.Licensing.v5
{
	public enum LicenseSaveType
	{
		Normal,
		Signing,
		SignatureCheck
	}
}
