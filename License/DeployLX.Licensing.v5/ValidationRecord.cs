using System;
using System.Collections;
using System.Text;
using System.Net;
using System.Web.Services.Protocols;
using Realisable.Utils.Net;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public sealed class ValidationRecord
	{
		private string _errorCode;

		private string _message;

		private object _relatedObject;

		private ErrorSeverity _severity;

		private Exception _exception;

		private ValidationRecordCollection _subRecords = new ValidationRecordCollection();

		private bool _isReadOnly;

		public string ErrorCode
		{
			get
			{
				return _errorCode;
			}
		}

		public string Message
		{
			get
			{
				return _message;
			}
		}

		public object RelatedObject
		{
			get
			{
				return _relatedObject;
			}
			set
			{
				AssertNotReadOnly();
				_relatedObject = value;
			}
		}

		public ErrorSeverity Severity
		{
			get
			{
				if (_severity == ErrorSeverity.Derived)
				{
					ErrorSeverity errorSeverity = ErrorSeverity.NotSet;
					{
						foreach (ValidationRecord item in (IEnumerable)_subRecords)
						{
							ErrorSeverity severity = item.Severity;
							if (severity > errorSeverity)
							{
								errorSeverity = severity;
							}
						}
						return errorSeverity;
					}
				}
				return _severity;
			}
			set
			{
				AssertNotReadOnly();
				_severity = value;
			}
		}

		public Exception Exception
		{
			get
			{
				return _exception;
			}
			set
			{
				AssertNotReadOnly();
				_exception = value;
			}
		}

		public ValidationRecordCollection SubRecords
		{
			get
			{
				return _subRecords;
			}
		}

		public bool IsReadOnly
		{
			get
			{
				return _isReadOnly;
			}
		}

		public ValidationRecord(string errorCodeOrMessage)
			: this(errorCodeOrMessage, null, null, ErrorSeverity.NotSet)
		{
		}

		public ValidationRecord(string errorCodeOrMessage, object relatedObject, Exception exception, ErrorSeverity severity, params object[] args)
			: this(errorCodeOrMessage, relatedObject, exception, severity, false, args)
		{
		}

		private ValidationRecord(string errorCodeOrMessage, object relatedObject, Exception exception, ErrorSeverity severity, bool recursive, params object[] args)
		{
			if (errorCodeOrMessage == null)
			{
				errorCodeOrMessage = "E_UnknownError";
			}
			_errorCode = errorCodeOrMessage;
			_relatedObject = relatedObject;
			_exception = exception;
			if (exception != null && severity == ErrorSeverity.NotSet)
			{
				_severity = ErrorSeverity.High;
			}
			else
			{
				_severity = severity;
			}
			try
			{
				_message = SR.GetString(errorCodeOrMessage, args);
			}
			catch
			{
				_message = errorCodeOrMessage;
			}
			try
			{
				if (!recursive)
				{
					SecureLicenseException ex = exception as SecureLicenseException;
					if (!_errorCode.EndsWith("Detailed"))
					{
						string @string = SR.GetString(_errorCode + "Detailed");
						if (@string != _errorCode + "Detailed")
						{
							SubRecords.Add(new ValidationRecord(_errorCode + "Detailed", relatedObject, exception, ErrorSeverity.NotSet, true, args));
						}
						else if (exception != null)
						{
							@string = PreprocessException(exception);
							if (@string != errorCodeOrMessage)
							{
								SubRecords.Add(new ValidationRecord(@string, relatedObject, exception, ErrorSeverity.High, true, args));
							}
						}
					}
					else if (exception != null)
					{
						string text = PreprocessException(exception);
						if (text != errorCodeOrMessage)
						{
							SubRecords.Add(new ValidationRecord(text, relatedObject, exception, ErrorSeverity.High, true, args));
						}
					}
					if (ex != null && ex.DetailedMessage != null)
					{
						SubRecords[SubRecords.Count - 1].SubRecords.Add(new ValidationRecord(ex.DetailedMessage, relatedObject, exception, ErrorSeverity.Low, true, args));
					}
				}
			}
			catch
			{
			}
		}

		public static string PreprocessException(Exception ex, SecureLicenseContext context)
		{
			if (ex == null)
			{
				return "";
			}
			try
			{
				if (context == null)
				{
					context = SecureLicenseManager.CurrentContext;
				}
				if (ex is SoapException)
				{
					string text = ex.Message;
					if (text.IndexOf("Exception: ") > -1)
					{
						text = text.Substring(text.LastIndexOf("Exception: ") + 11);
						text = text.Substring(0, text.IndexOf('\n'));
					}
					if (text.IndexOf("-->") > -1)
					{
						text = text.Substring(text.LastIndexOf("-->") + 3);
					}
					return text.Trim();
				}
                if (ex is WebException)
                {
                    WebException webEx = ex as WebException;
                    HttpWebResponse httpResponse = webEx.Response as HttpWebResponse;
                    if (httpResponse != null)
                    {
                        return httpResponse.GetResponseString(Encoding.UTF8);
                    }
                    else
                    {
                        return ex.Message;
                    }
                }

				if (context.RequestInfo.DeveloperMode)
				{
					return ex.ToString();
				}
				if (ex.InnerException != null)
				{
					StringBuilder stringBuilder = new StringBuilder(ex.Message.Length * 3);
					for (Exception ex2 = ex; ex2 != null; ex2 = ex2.InnerException)
					{
						if (stringBuilder.Length > 0)
						{
							stringBuilder.Append(" --> ");
						}
						stringBuilder.Append(ex2.Message);
					}
					return stringBuilder.ToString();
				}
				return ex.Message;
			}
			catch
			{
				return (ex == null) ? "" : ex.Message;
			}
		}

		public static string PreprocessException(Exception ex)
		{
			return PreprocessException(ex, null);
		}

		public override string ToString()
		{
			return ToString(null);
		}

		public string ToString(SecureLicenseContext context)
		{
			if (context == null)
			{
				context = SecureLicenseManager.CurrentContext;
			}
			StringBuilder stringBuilder = new StringBuilder(255);
			stringBuilder.Append(_message);
			if (_subRecords.Count > 0)
			{
				foreach (ValidationRecord item in (IEnumerable)_subRecords)
				{
					item.AppendTo(stringBuilder, context, 1);
				}
			}
			return stringBuilder.ToString();
		}

		internal void AppendTo(StringBuilder result, SecureLicenseContext context, int depth)
		{
			if (result.Length > 0)
			{
				result.Append(Environment.NewLine);
			}
			result.Append('\t', depth);
			result.Append(_message);
			if (context != null && context.RequestInfo.DeveloperMode)
			{
				result.Append(Environment.NewLine);
				if (_relatedObject != null)
				{
					result.Append('\t', depth);
					result.Append("Related Object: ");
					result.Append(_relatedObject.ToString());
					result.Append(" [");
					result.Append(_relatedObject.GetType().ToString());
					result.Append("]");
					result.Append(Environment.NewLine);
					LicenseFile licenseFile = _relatedObject as LicenseFile;
					if (licenseFile == null && _relatedObject is SecureLicense)
					{
						licenseFile = ((SecureLicense)_relatedObject).LicenseFile;
					}
					if (licenseFile != null)
					{
						result.Append('\t', depth);
						result.Append("At: ");
						result.Append(licenseFile.Location);
						result.Append(Environment.NewLine);
					}
				}
				if (_exception != null)
				{
					result.Append('\t', depth);
					result.Append("Exception: ");
					result.Append(_exception.ToString());
					result.Append(Environment.NewLine);
				}
			}
			if (_subRecords.Count > 0)
			{
				foreach (ValidationRecord item in (IEnumerable)_subRecords)
				{
					item.AppendTo(result, context, depth + 1);
				}
			}
		}

		public void MakeReadOnly()
		{
			_isReadOnly = true;
			foreach (ValidationRecord item in (IEnumerable)_subRecords)
			{
				item.MakeReadOnly();
			}
			_subRecords.MakeReadOnly();
		}

		internal void AssertNotReadOnly()
		{
			if (!_isReadOnly)
			{
				return;
			}
			throw new SecureLicenseException("E_ReadOnlyObject", "ValidationRecord");
		}
	}
}
