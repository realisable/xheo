using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public class BufferedPanel : Panel
	{
		private bool _offsetBackgroundImage;

		private bool _paintFakeBackground;

		[Category("Appearance")]
		[DefaultValue(false)]
		public bool OffsetBackgroundImage
		{
			get
			{
				return _offsetBackgroundImage;
			}
			set
			{
				_offsetBackgroundImage = value;
				base.Invalidate();
			}
		}

		[DefaultValue(false)]
		public bool PaintFakeBackground
		{
			get
			{
				return _paintFakeBackground;
			}
			set
			{
				_paintFakeBackground = value;
				base.Invalidate(true);
			}
		}

		public BufferedPanel()
		{
			base.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer, true);
			BackgroundImageLayout = ImageLayout.None;
		}

		protected override void OnCreateControl()
		{
			base.OnCreateControl();
			if (_paintFakeBackground && Environment.Version.Major < 2)
			{
				BackColor = Color.Transparent;
				_paintFakeBackground = false;
			}
		}

		protected override void OnPaintBackground(PaintEventArgs e)
		{
			if (BackgroundImage != null && _offsetBackgroundImage)
			{
				int num = base.Left;
				int num2 = base.Top;
				Control parent = base.Parent;
				while (parent != null && !(parent is Form))
				{
					num += parent.Left;
					num2 += parent.Top;
					parent = parent.Parent;
				}
				e.Graphics.DrawImage(BackgroundImage, 0, 0, new Rectangle(num, num2, base.Width, base.Height), GraphicsUnit.Pixel);
			}
			else if (_paintFakeBackground)
			{
				if (base.Parent is BufferedPanel)
				{
					((BufferedPanel)base.Parent).PaintFakeParentBackground(e, this);
				}
				else if (base.Parent is SuperFormPanel)
				{
					GraphicsState gstate = e.Graphics.Save();
					e.Graphics.TranslateTransform((float)(-base.Left), (float)(-base.Top));
					((SuperFormPanel)base.Parent).PaintPanelBackground(e);
					e.Graphics.Restore(gstate);
				}
				else
				{
					base.OnPaintBackground(e);
				}
			}
			else
			{
				base.OnPaintBackground(e);
			}
		}

		public void PaintFakeParentBackground(PaintEventArgs e, Control ctrl)
		{
			GraphicsState gstate = e.Graphics.Save();
			e.Graphics.TranslateTransform((float)(-ctrl.Left), (float)(-ctrl.Top));
			OnPaintBackground(e);
			OnPaint(e);
			e.Graphics.Restore(gstate);
		}
	}
}
