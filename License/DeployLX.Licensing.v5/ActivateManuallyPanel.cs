using System;
using System.Collections;
using System.Drawing;
using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	public class ActivateManuallyPanel : SplitPanel
	{
		private ShadowLabel _activateTitle;

		private ShadowLabel _activateManuallyNotice;

		private SkinnedPanel _activationPanel;

		private LicenseCodeTextBox _activationCode;

		private ThemeLabel _serialNumber;

		private ThemeLabel _serialNumberLabel;

		private ThemeLabel _activationCodeLabel;

		private ThemeLabel _machineCodeLabel;

		private ThemeLabel _machineCode;

		private ThemeLabel _machineProfileLabel;

		private SkinnedButton _copyToClipboard;

		private SkinnedButton _copyToEmail;

		private ComboBox _machineProfile;

		public ActivateManuallyPanel(ActivationLimit limit)
			: base(limit)
		{
			InitializeComponent();
		}

		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			_activateTitle = new ShadowLabel();
			_activationPanel = new SkinnedPanel();
			_machineProfile = new ComboBox();
			_activationCode = new LicenseCodeTextBox();
			_machineCodeLabel = new ThemeLabel();
			_machineCode = new ThemeLabel();
			_machineProfileLabel = new ThemeLabel();
			_activationCodeLabel = new ThemeLabel();
			_serialNumberLabel = new ThemeLabel();
			_serialNumber = new ThemeLabel();
			_activateManuallyNotice = new ShadowLabel();
			_copyToClipboard = new SkinnedButton();
			_copyToEmail = new SkinnedButton();
			base.SidePanel.SuspendLayout();
			base.BodyPanel.SuspendLayout();
			_activationPanel.SuspendLayout();
			base.SuspendLayout();
			base.SidePanel.Controls.Add(_copyToEmail);
			base.SidePanel.Controls.Add(_copyToClipboard);
			base.SidePanel.Location = new Point(0, 0);
			base.SidePanel.Size = new Size(170, 421);
			base.SidePanel.TabIndex = 0;
			base.SidePanel.Controls.SetChildIndex(_copyToClipboard, 0);
			base.SidePanel.Controls.SetChildIndex(_copyToEmail, 0);
			base.BodyPanel.Controls.Add(_activateManuallyNotice);
			base.BodyPanel.Controls.Add(_activationPanel);
			base.BodyPanel.Controls.Add(_activateTitle);
			base.BodyPanel.Location = new Point(170, 0);
			base.BodyPanel.Size = new Size(514, 421);
			_activateTitle.AutoEllipsis = true;
			_activateTitle.Location = new Point(14, 16);
			_activateTitle.Name = "_activateTitle";
			_activateTitle.Size = new Size(482, 34);
			_activateTitle.TabIndex = 0;
			_activateTitle.Text = "#UI_ActivateManuallyTitle";
			_activateTitle.ThemeColor = new string[2]
			{
				"PanelTitleText",
				"Text"
			};
			_activateTitle.ThemeFont = ThemeFont.Title;
			_activationPanel.Anchor = (AnchorStyles.Bottom | AnchorStyles.Left);
			_activationPanel.BackColor = Color.Transparent;
			_activationPanel.BackgroundImageLayout = ImageLayout.None;
			_activationPanel.Controls.Add(_machineProfile);
			_activationPanel.Controls.Add(_activationCode);
			_activationPanel.Controls.Add(_machineCodeLabel);
			_activationPanel.Controls.Add(_machineCode);
			_activationPanel.Controls.Add(_machineProfileLabel);
			_activationPanel.Controls.Add(_activationCodeLabel);
			_activationPanel.Controls.Add(_serialNumberLabel);
			_activationPanel.Controls.Add(_serialNumber);
			_activationPanel.Location = new Point(13, 107);
			_activationPanel.Name = "_activationPanel";
			_activationPanel.PaintFakeBackground = true;
			_activationPanel.Size = new Size(488, 198);
			_activationPanel.TabIndex = 0;
			_machineProfile.DropDownStyle = ComboBoxStyle.DropDownList;
			_machineProfile.Items.AddRange(new object[3]
			{
				"1 (Laptop)",
				"2 (Desktop)",
				"3"
			});
			_machineProfile.Location = new Point(327, 118);
			_machineProfile.Name = "_machineProfile";
			_machineProfile.Size = new Size(146, 23);
			_machineProfile.TabIndex = 6;
			_activationCode.BackColor = Color.White;
			_activationCode.CharacterSet = null;
			_activationCode.Location = new Point(15, 148);
			_activationCode.Mask = "XSL2-CCCCC-CCCCC-CCCCC-CCCCC-CCCCC-CCCCC";
			_activationCode.MaskColor = Color.FromArgb(200, 200, 200);
			_activationCode.Name = "_activationCode";
			_activationCode.Prompt = null;
			_activationCode.Size = new Size(458, 23);
			_activationCode.TabIndex = 1;
			_machineCodeLabel.Location = new Point(15, 67);
			_machineCodeLabel.Name = "_machineCodeLabel";
			_machineCodeLabel.Size = new Size(300, 20);
			_machineCodeLabel.TabIndex = 5;
			_machineCodeLabel.Text = "#UI_MachineCode";
			_machineCodeLabel.ThemeFont = ThemeFont.Bold;
			_machineCode.Location = new Point(28, 86);
			_machineCode.Name = "_machineCode";
			_machineCode.Size = new Size(445, 33);
			_machineCode.TabIndex = 4;
			_machineCode.ThemeColor = new string[2]
			{
				"GlassPanelHighlightText",
				"GlassPanelText"
			};
			_machineProfileLabel.Location = new Point(176, 121);
			_machineProfileLabel.Name = "_machineProfileLabel";
			_machineProfileLabel.Size = new Size(139, 20);
			_machineProfileLabel.TabIndex = 0;
			_machineProfileLabel.Text = "#UI_MachineProfile";
			_machineProfileLabel.TextAlign = ContentAlignment.TopRight;
			_activationCodeLabel.Location = new Point(15, 121);
			_activationCodeLabel.Name = "_activationCodeLabel";
			_activationCodeLabel.Size = new Size(191, 20);
			_activationCodeLabel.TabIndex = 0;
			_activationCodeLabel.Text = "#UI_ActivationCode";
			_activationCodeLabel.ThemeFont = ThemeFont.Bold;
			_serialNumberLabel.Location = new Point(15, 15);
			_serialNumberLabel.Name = "_serialNumberLabel";
			_serialNumberLabel.Size = new Size(300, 20);
			_serialNumberLabel.TabIndex = 1;
			_serialNumberLabel.Text = "#UI_SerialNumber";
			_serialNumberLabel.ThemeFont = ThemeFont.Bold;
			_serialNumber.Location = new Point(28, 34);
			_serialNumber.Name = "_serialNumber";
			_serialNumber.Size = new Size(445, 33);
			_serialNumber.TabIndex = 0;
			_serialNumber.ThemeColor = new string[2]
			{
				"GlassPanelHighlightText",
				"GlassPanelText"
			};
			_activateManuallyNotice.Location = new Point(16, 50);
			_activateManuallyNotice.Name = "_activateManuallyNotice";
			_activateManuallyNotice.Size = new Size(482, 54);
			_activateManuallyNotice.TabIndex = 13;
			_activateManuallyNotice.Text = "UI_ActivateManuallyNotice";
			_activateManuallyNotice.ThemeFont = ThemeFont.Medium;
			_copyToClipboard.Location = new Point(20, 163);
			_copyToClipboard.Name = "_copyToClipboard";
			_copyToClipboard.ShouldDrawFocus = false;
			_copyToClipboard.Size = new Size(130, 25);
			_copyToClipboard.TabIndex = 3;
			_copyToClipboard.Text = "#UI_CopyToClipboard";
			_copyToClipboard.UseVisualStyleBackColor = false;
			_copyToClipboard.Click += _copyToClipboard_Click;
			_copyToEmail.Location = new Point(20, 194);
			_copyToEmail.Name = "_copyToEmail";
			_copyToEmail.ShouldDrawFocus = false;
			_copyToEmail.Size = new Size(130, 25);
			_copyToEmail.TabIndex = 3;
			_copyToEmail.Text = "#UI_CopyToEmail";
			_copyToEmail.UseVisualStyleBackColor = false;
			_copyToEmail.Click += _copyToEmail_Click;
			base.AutoScaleDimensions = new SizeF(96f, 96f);
			KeepSupportVisible = true;
			base.Name = "ActivateManuallyPanel";
			base.SidePanel.ResumeLayout(false);
			base.BodyPanel.ResumeLayout(false);
			_activationPanel.ResumeLayout(false);
			base.ResumeLayout(false);
		}

		protected internal override void LoadPanel(SecureLicenseContext context)
		{
			base.LoadPanel(context);
			ActivationLimit activationLimit = base.Limit as ActivationLimit;
			if (activationLimit.LogoResource != null)
			{
				base.Logo = Toolbox.GetImage(activationLimit.LogoResource, context);
			}
			foreach (object control in _activationPanel.Controls)
			{
				ShadowLabel shadowLabel = control as ShadowLabel;
				if (shadowLabel != null)
				{
					shadowLabel.DropShadow = base.SuperForm.LightShadow;
				}
			}
		}

		protected internal override void InitializePanel()
		{
			ActivationLimit activationLimit = base.Limit as ActivationLimit;
			_serialNumber.Text = ((activationLimit.License.SerialNumber == null) ? activationLimit.License.LicenseId : activationLimit.License.SerialNumber);
			_serialNumber.Font = base.SuperForm.FixedFont;
			if (_serialNumber.Text.Length > 36)
			{
				_serialNumber.Font = new Font(base.SuperForm.FixedFont.FontFamily, 10f);
			}
			_machineCode.Font = base.SuperForm.FixedFont;
			_machineCode.Text = activationLimit.Profiles[0].MakeMachineProfile().Hash;
			if (_machineCode.Text.Length > 36)
			{
				_machineCode.Font = new Font(base.SuperForm.FixedFont.FontFamily, 10f);
			}
			_activationCode.Font = base.SuperForm.FixedFont;
			_activateManuallyNotice.Text = SR.GetString("UI_ActivateManuallyNotice", base.SuperForm.Context.SupportInfo.Product);
			Button button = base.AddBottomButton("UI_Activate");
			button.Click += _continue_Click;
			base.SuperForm.AcceptButton = button;
			button = base.AddBottomButton("UI_GoBack");
			button.Image = Images.Back_png;
			button.TextImageRelation = TextImageRelation.ImageBeforeText;
			button.Click += _goBack_Click;
			base.SuperForm.CancelButton = button;
			_machineProfile.Items.Clear();
			foreach (ActivationProfile item in (IEnumerable)activationLimit.GetProfilesForThisMachine())
			{
				_machineProfile.Items.Add(item);
			}
			if (_machineProfile.Items.Count == 0)
			{
				_machineProfile.Items.Add(SR.GetString("M_ProfileNone"));
			}
			_machineProfile.SelectedIndex = 0;
			_machineProfile.Enabled = (_machineProfile.Items.Count != 1);
			_activationCode.Mask = activationLimit.CodeMask;
			_activationCode.CharacterSet = activationLimit.License.SerialNumberInfo.CharacterSet;
			_activationCode.Select();
			_activationCode.Focus();
			base.AddLicenseAgreementLink();
		}

		protected virtual void HandleActivate()
		{
			ActivationLimit activationLimit = base.Limit as ActivationLimit;
			ActivationProfile activationProfile = _machineProfile.SelectedItem as ActivationProfile;
			activationProfile.AssignedName = (base.SuperForm.Context.Items["MachineName"] as string);
			if (!activationLimit.ActivateByCode(base.SuperForm.Context, _activationCode.Text, activationProfile))
			{
				base.ShowMessageBox("E_ActivationCodeInvalid", "UI_ValidationNoticeTitle", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			}
			else
			{
				base.Return((FormResult)524289);
			}
		}

		private void _cancel_Click(object sender, EventArgs e)
		{
			base.Return(FormResult.Incomplete);
		}

		private void _goBack_Click(object sender, EventArgs e)
		{
			base.Return(FormResult.Incomplete);
		}

		private void _continue_Click(object sender, EventArgs e)
		{
			HandleActivate();
		}

		private void _copyToClipboard_Click(object sender, EventArgs e)
		{
			ClipboardToolbox.SetClipboard(DataFormats.Text, TemplateFormatter.Format("M_CopyActivation", ActivationLimit.EmailTemplateVariables, new string[3]
			{
				_serialNumber.Text,
				_machineCode.Text,
				_machineProfile.SelectedItem.ToString()
			}));
			base.ShowMessageBox("UI_DetailsCopied", "UI_DetailsCopiedTitle", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
		}

		private void _copyToEmail_Click(object sender, EventArgs e)
		{
			ActivationLimit activationLimit = base.Limit as ActivationLimit;
			string unlockEmailTemplate = activationLimit.UnlockEmailTemplate;
			if (!Mapi.SendMail(base.SuperForm.Context.SupportInfo.Email, null, SR.GetString("M_ActivationRequest"), TemplateFormatter.Format(unlockEmailTemplate, new string[3]
			{
				"SerialNumber",
				"MachineCode",
				"ReferenceId"
			}, new string[3]
			{
				_serialNumber.Text,
				_machineCode.Text,
				_machineProfile.SelectedItem.ToString()
			}), null, null))
			{
				base.ShowMessageBox("E_CouldNotCreateEmail", null, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			}
		}

		protected internal override void PanelShown(bool safeToChange)
		{
			base.PanelShown(safeToChange);
			_activationCode.Focus();
			_activationCode.Select();
			PopulatePotentialActivationCode();
		}

		private void PopulatePotentialActivationCode()
		{
			if (_activationCode != null && _activationCode.Text.Length == 0)
			{
				foreach (ActivationProfile item in _machineProfile.Items)
				{
					string potentialActivationFromClipboard = base.SuperForm.Context.GetPotentialActivationFromClipboard(item, _machineCode.Text);
					if (potentialActivationFromClipboard == null)
					{
						continue;
					}
					try
					{
						_activationCode.Text = potentialActivationFromClipboard;
					}
					catch (ArgumentOutOfRangeException)
					{
					}
					_machineProfile.SelectedItem = item;
					break;
				}
			}
		}

		protected internal override void FormActivated()
		{
			base.FormActivated();
			PopulatePotentialActivationCode();
		}
	}
}
