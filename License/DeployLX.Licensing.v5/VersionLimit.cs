using System;
using System.ComponentModel;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	[LimitEditor("VersionLimitEditor", IconResource = "asmres://DeployLX.Licensing.DlxAddIn.v5/DeployLX.Licensing.DlxAddIn.v5.Resources.Limits.Version.png")]
	public class VersionLimit : Limit
	{
		private Version _minimum = new Version(0, 0);

		private Version _maximum = new Version(0, 0);

		private bool _checkFileVersion;

		[Description("The minimum assembly version allowed to use the license.")]
		public Version Minimum
		{
			get
			{
				return _minimum;
			}
			set
			{
				if (_minimum != value)
				{
					base.AssertNotReadOnly();
					_minimum = value;
					base.OnChanged("Minimum");
				}
			}
		}

		public Version Maximum
		{
			get
			{
				return _maximum;
			}
			set
			{
				if (_maximum != value)
				{
					base.AssertNotReadOnly();
					_maximum = value;
					base.OnChanged("Maximum");
				}
			}
		}

		public bool CheckFileVersion
		{
			get
			{
				return _checkFileVersion;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_checkFileVersion != value)
				{
					_checkFileVersion = value;
					base.OnChanged("CheckFileVersion");
				}
			}
		}

		public override string Description
		{
			get
			{
				return SR.GetString("M_VersionLimitDescription");
			}
		}

		public override string Name
		{
			get
			{
				return "Version";
			}
		}

		public override ValidationResult Validate(SecureLicenseContext context)
		{
			Check.NotNull(context, "context");
			Version version;
			Version version2;
			if (_maximum < _minimum)
			{
				version = _maximum;
				version2 = _minimum;
			}
			else
			{
				version = _minimum;
				version2 = _maximum;
			}
			Version v = (!_checkFileVersion) ? context.LicensedType.Assembly.GetName().Version : TypeHelper.GetAssemblyVersion(context.LicensedType.Assembly).ToVersion();
			if (v >= version && v <= version2)
			{
				return base.Validate(context);
			}
			if (version.CompareTo(version2) == 0)
			{
				return context.ReportError("E_VersionMismatch", null, null, ErrorSeverity.Normal, _maximum);
			}
			return context.ReportError("E_VersionMismatchRange", null, null, ErrorSeverity.Normal, _minimum, _maximum);
		}

		protected override bool ReadFromXml(XmlReader reader)
		{
			string attribute = reader.GetAttribute("minimum");
			string attribute2 = reader.GetAttribute("maximum");
			_checkFileVersion = (reader.GetAttribute("checkFileVersion") == "true");
			if (attribute == null && attribute2 == null)
			{
				return false;
			}
			if (attribute != null)
			{
				_minimum = new Version(attribute);
			}
			else
			{
				_minimum = null;
			}
			if (attribute2 != null)
			{
				_maximum = new Version(attribute2);
			}
			else
			{
				_maximum = null;
			}
			return base.ReadChildLimits(reader);
		}

		protected override bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			if (_minimum == (Version)null && _maximum == (Version)null)
			{
				return false;
			}
			if (_maximum != (Version)null)
			{
				writer.WriteAttributeString("maximum", _maximum.ToString());
			}
			if (_minimum != (Version)null)
			{
				writer.WriteAttributeString("minimum", _minimum.ToString());
			}
			if (_checkFileVersion)
			{
				writer.WriteAttributeString("checkFileVersion", "true");
			}
			return true;
		}
	}
}
