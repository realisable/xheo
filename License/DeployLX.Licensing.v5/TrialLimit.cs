using System;
using System.Collections.Specialized;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	[LimitEditor("TrialLimitEditor", Category = "Super Form Limits", IconResource = "asmres://DeployLX.Licensing.DlxAddIn.v5/DeployLX.Licensing.DlxAddIn.v5.Resources.Limits.Trial.png")]
	public class TrialLimit : SuperFormLimit, IPurchaseLimit
	{
		public class PageIds
		{
			public const string Default = "DEFAULT";

			public const string Extend = "EXTEND";
		}

		private string _terms;

		private string _purchaseUrl;

		private string _infoResource;

		private ShowFormOption _shouldShowForm = ShowFormOption.Always;

		private bool _showOnce = true;

		private string _bitmapResource;

		private string _logoResource;

		private int _continueDelay;

		private bool _hideExtendButton;

		private bool _dontShowIfFormAlreadyShown = true;

		private bool _showRegisterIfAvailable = true;

		public string BitmapResource
		{
			get
			{
				return _bitmapResource;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_bitmapResource != value)
				{
					SharedToolbox.ValidateUrl(value, true);
					_bitmapResource = value;
					base.OnChanged("BitmapResource");
				}
			}
		}

		public int ContinueDelay
		{
			get
			{
				return _continueDelay;
			}
			set
			{
				base.AssertNotReadOnly();
				Check.GreaterThanEqual(value, 0, "ContinueDelay");
				if (_continueDelay != value)
				{
					_continueDelay = value;
					base.OnChanged("ContinueDelay");
				}
			}
		}

		public override string Description
		{
			get
			{
				return SR.GetString("M_TrialLimitDescription");
			}
		}

		public bool DontShowIfFormAlreadyShown
		{
			get
			{
				return _dontShowIfFormAlreadyShown;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_dontShowIfFormAlreadyShown != value)
				{
					_dontShowIfFormAlreadyShown = value;
					base.OnChanged("DontShowIfFormAlreadyShown");
				}
			}
		}

		public bool HideExtendButton
		{
			get
			{
				return _hideExtendButton;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_hideExtendButton != value)
				{
					_hideExtendButton = value;
					base.OnChanged("HideExtendButton");
				}
			}
		}

		public string InfoResource
		{
			get
			{
				return _infoResource;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_infoResource != value)
				{
					SharedToolbox.ValidateUrl(value, true);
					_infoResource = value;
					base.OnChanged("InfoResource");
				}
			}
		}

		public override bool IsGui
		{
			get
			{
				return true;
			}
		}

		public string LogoResource
		{
			get
			{
				return _logoResource;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_logoResource != value)
				{
					SharedToolbox.ValidateUrl(value, true);
					_logoResource = value;
					base.OnChanged("LogoResource");
				}
			}
		}

		public override string Name
		{
			get
			{
				return "Trial";
			}
		}

		public string PurchaseUrl
		{
			get
			{
				return _purchaseUrl;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_purchaseUrl != value)
				{
					SharedToolbox.ValidateUrl(value, true);
					_purchaseUrl = value;
					base.OnChanged("PurchaseUrl");
				}
			}
		}

		public ShowFormOption ShouldShowForm
		{
			get
			{
				return _shouldShowForm;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_shouldShowForm != value)
				{
					_shouldShowForm = value;
					base.OnChanged("ShouldShowForm");
				}
			}
		}

		public bool ShowOnce
		{
			get
			{
				return _showOnce;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_showOnce != value)
				{
					_showOnce = value;
					base.OnChanged("ShowOnce");
				}
			}
		}

		public string Terms
		{
			get
			{
				return _terms;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_terms != value)
				{
					_terms = value;
					base.OnChanged("Terms");
				}
			}
		}

		public bool ShowRegisterIfAvailable
		{
			get
			{
				return _showRegisterIfAvailable;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_showRegisterIfAvailable != value)
				{
					_showRegisterIfAvailable = value;
					base.OnChanged("ShowRegisterIfAvailable");
				}
			}
		}

		public override ValidationResult Validate(SecureLicenseContext context)
		{
			try
			{
				if (base.ResolveShouldShowForm(context) && context.CanShowWindowsForm)
				{
					switch (ShowForm(context, "DEFAULT"))
					{
					case FormResult.Success:
						context.CanSkipFailureReport = true;
						return base.Validate(context);
					case FormResult.Retry:
						return ValidationResult.Retry;
					case FormResult.Failure:
					case FormResult.Closed:
						context.CanSkipFailureReport = true;
						return context.ReportError("E_DidNotAcceptTrial", this);
					}
				}
				if (!CanContinueTrial(context))
				{
					context.ReportError("E_TrialExpired", this);
					return ValidationResult.Invalid;
				}
				return base.Validate(context);
			}
			finally
			{
				if (base.FormShown)
				{
					base.SetPersistentData("ShownOnce", true, PersistentDataLocationType.SharedOrUser);
				}
			}
		}

		public override PeekResult Peek(SecureLicenseContext context)
		{
			if (_shouldShowForm == ShowFormOption.Never)
			{
				return base.Peek(context);
			}
			if (_shouldShowForm == ShowFormOption.Always)
			{
				return PeekResult.NeedsUser;
			}
			PeekResult peekResult = base.Peek(context);
			if (peekResult != PeekResult.Valid)
			{
				if (_shouldShowForm == ShowFormOption.AfterExpires)
				{
					return PeekResult.NeedsUser;
				}
				return peekResult;
			}
			if (_shouldShowForm == ShowFormOption.BeforeExpires)
			{
				return PeekResult.NeedsUser;
			}
			return peekResult;
		}

		public bool CanContinueTrial(SecureLicenseContext context)
		{
			PeekResult peekResult = base.Peek(context);
			if (peekResult != PeekResult.Invalid && peekResult != PeekResult.NeedsUser)
			{
				return true;
			}
			return false;
		}

		protected override bool ResolveDefaultShouldShowForm(SecureLicenseContext context)
		{
			if (_shouldShowForm == ShowFormOption.Never)
			{
				return false;
			}
			if (_dontShowIfFormAlreadyShown && context.HasFormBeenShown)
			{
				context.CanSkipFailureReport = true;
				return false;
			}
			if (_shouldShowForm == ShowFormOption.Always)
			{
				if (!_showOnce)
				{
					return true;
				}
				return !base.FormShown;
			}
			if (_shouldShowForm == ShowFormOption.FirstTimeAndExpires && !(bool)base.GetPersistentData("ShownOnce", false, PersistentDataLocationType.SharedOrUser))
			{
				return true;
			}
			if (CanContinueTrial(context))
			{
				return _shouldShowForm == ShowFormOption.BeforeExpires;
			}
			if (_shouldShowForm != ShowFormOption.AfterExpires)
			{
				return _shouldShowForm == ShowFormOption.FirstTimeAndExpires;
			}
			return true;
		}

		protected override bool ReadFromXml(XmlReader reader)
		{
			_terms = reader.GetAttribute("terms");
			_purchaseUrl = SafeToolbox.XmlDecode(reader.GetAttribute("purchaseUrl"));
			_infoResource = SafeToolbox.XmlDecode(reader.GetAttribute("infoResource"));
			_bitmapResource = SafeToolbox.XmlDecode(reader.GetAttribute("bitmapResource"));
			_logoResource = SafeToolbox.XmlDecode(reader.GetAttribute("logoResource"));
			string attribute = reader.GetAttribute("shouldShowForm");
			if (attribute != null)
			{
				_shouldShowForm = (ShowFormOption)SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_shouldShowForm = ShowFormOption.Always;
			}
			_showOnce = (reader.GetAttribute("showOnce") != "false");
			_hideExtendButton = (reader.GetAttribute("hideExtendButton") == "true");
			_dontShowIfFormAlreadyShown = (reader.GetAttribute("dontShowIfFormAlreadyShown") != "false");
			_showRegisterIfAvailable = (reader.GetAttribute("showRegisterIfAvailable") != "false");
			attribute = reader.GetAttribute("continueDelay");
			if (attribute != null)
			{
				_continueDelay = SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_continueDelay = 0;
			}
			if (!base.ReadFromXml(reader))
			{
				return false;
			}
			return base.ReadChildLimits(reader);
		}

		protected override bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			if (_terms != null)
			{
				writer.WriteAttributeString("terms", _terms);
			}
			if (_shouldShowForm != ShowFormOption.Always)
			{
				int shouldShowForm = (int)_shouldShowForm;
				writer.WriteAttributeString("shouldShowForm", shouldShowForm.ToString());
			}
			if (_hideExtendButton)
			{
				writer.WriteAttributeString("hideExtendButton", "true");
			}
			if (!_showOnce)
			{
				writer.WriteAttributeString("showOnce", "false");
			}
			if (_purchaseUrl != null)
			{
				writer.WriteAttributeString("purchaseUrl", SafeToolbox.XmlEncode(_purchaseUrl));
			}
			if (_infoResource != null)
			{
				writer.WriteAttributeString("infoResource", SafeToolbox.XmlEncode(_infoResource));
			}
			if (_bitmapResource != null)
			{
				writer.WriteAttributeString("bitmapResource", SafeToolbox.XmlEncode(_bitmapResource));
			}
			if (_logoResource != null)
			{
				writer.WriteAttributeString("logoResource", SafeToolbox.XmlEncode(_logoResource));
			}
			if (!_showRegisterIfAvailable)
			{
				writer.WriteAttributeString("showRegisterIfAvailable", "false");
			}
			if (_continueDelay != 0)
			{
				writer.WriteAttributeString("continueDelay", _continueDelay.ToString());
			}
			if (!_dontShowIfFormAlreadyShown)
			{
				writer.WriteAttributeString("dontShowIfFormAlreadyShown", "false");
			}
			return base.WriteToXml(writer, signing);
		}

		protected override SuperFormPanel CreatePanel(string pageId, SecureLicenseContext context)
		{
			if (((StringDictionary)base.CustomForms)[pageId] == null)
			{
				switch (pageId)
				{
				case "EXTEND":
					return new ExtensionPanel(!CanContinueTrial(context), base.Limits.FindLimitsByType(typeof(IExtendableLimit), true));
				case null:
				case "DEFAULT":
					return new TrialPanel(this);
				}
			}
			return base.CreatePanel(pageId, context);
		}
	}
}
