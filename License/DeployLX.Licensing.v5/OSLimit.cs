using System;
using System.Collections;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	[LimitEditor("OSLimitEditor", IconResource = "asmres://DeployLX.Licensing.DlxAddIn.v5/DeployLX.Licensing.DlxAddIn.v5.Resources.Limits.Os.png")]
	public class OSLimit : Limit
	{
		private OSRecordCollection _allowedOSList;

		private OSRecordCollection _restrictedOSList;

		public override string Description
		{
			get
			{
				return SR.GetString("M_OSLimitDescription");
			}
		}

		public override string Name
		{
			get
			{
				return "OS";
			}
		}

		public OSRecordCollection AllowedOSList
		{
			get
			{
				return _allowedOSList;
			}
		}

		public OSRecordCollection RestrictedOSList
		{
			get
			{
				return _restrictedOSList;
			}
		}

		public OSLimit()
		{
			_allowedOSList = new OSRecordCollection();
			_allowedOSList.Changed += _allowedOSList_Changed;
			_restrictedOSList = new OSRecordCollection();
			_restrictedOSList.Changed += _restrictedOSList_Changed;
		}

		private void _restrictedOSList_Changed(object sender, CollectionEventArgs e)
		{
			base.OnCollectionChanged(sender, "RestrictedOSList", e);
		}

		private void _allowedOSList_Changed(object sender, CollectionEventArgs e)
		{
			base.OnCollectionChanged(sender, "AllowedOSList", e);
		}

		public override ValidationResult Validate(SecureLicenseContext context)
		{
			if (IsMachineValid(context))
			{
				return base.Validate(context);
			}
			return context.ReportError("E_OSNotAllowed", this);
		}

		protected bool IsMachineValid(SecureLicenseContext context)
		{
			OSRecord thisMachine = OSRecord.ThisMachine;
			foreach (OSRecord item in (IEnumerable)_restrictedOSList)
			{
				if (thisMachine.IsMatch(item))
				{
					return false;
				}
			}
			if (_allowedOSList.Count == 0)
			{
				return true;
			}
			foreach (OSRecord item2 in (IEnumerable)_allowedOSList)
			{
				if (thisMachine.IsMatch(item2))
				{
					return true;
				}
			}
			return false;
		}

		public override PeekResult Peek(SecureLicenseContext context)
		{
			if (!IsMachineValid(context))
			{
				return PeekResult.Invalid;
			}
			return base.Peek(context);
		}

		protected override bool ReadFromXml(XmlReader reader)
		{
			_restrictedOSList.Clear();
			_allowedOSList.Clear();
			if (reader.IsEmptyElement)
			{
				reader.Read();
				return true;
			}
			reader.Read();
			while (!reader.EOF)
			{
				if (reader.IsStartElement())
				{
					switch (reader.Name)
					{
					case "Limit":
						if (base.ReadChildLimit(reader))
						{
							break;
						}
						return false;
					case "Restricted":
						if (ReadCollection(_restrictedOSList, reader))
						{
							break;
						}
						return false;
					case "Allowed":
						if (ReadCollection(_allowedOSList, reader))
						{
							break;
						}
						return false;
					default:
						reader.Skip();
						break;
					}
					continue;
				}
				reader.Read();
				break;
			}
			return true;
		}

		private static bool ReadCollection(OSRecordCollection collection, XmlReader reader)
		{
			if (reader.IsEmptyElement)
			{
				reader.Read();
				return true;
			}
			reader.Read();
			while (!reader.EOF)
			{
				if (reader.IsStartElement())
				{
					if (reader.Name == "OSRecord")
					{
						OSRecord oSRecord = new OSRecord();
						if (oSRecord.ReadFromXml(reader))
						{
							collection.Add(oSRecord);
							continue;
						}
						return false;
					}
					reader.Skip();
					continue;
				}
				reader.Read();
				break;
			}
			return true;
		}

		protected override bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			if (_allowedOSList.Count > 0)
			{
				writer.WriteStartElement("Allowed");
				foreach (OSRecord item in (IEnumerable)_allowedOSList)
				{
					if (!item.WriteToXml(writer))
					{
						return false;
					}
				}
				writer.WriteEndElement();
			}
			if (_restrictedOSList.Count > 0)
			{
				writer.WriteStartElement("Restricted");
				foreach (OSRecord item2 in (IEnumerable)_restrictedOSList)
				{
					if (!item2.WriteToXml(writer))
					{
						return false;
					}
				}
				writer.WriteEndElement();
			}
			return true;
		}

		protected internal override void MakeReadOnly()
		{
			base.MakeReadOnly();
			_allowedOSList.MakeReadOnly();
			_restrictedOSList.MakeReadOnly();
		}
	}
}
