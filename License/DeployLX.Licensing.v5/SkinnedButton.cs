using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public sealed class SkinnedButton : Button
	{
		private bool _isHot;

		private bool _isDown;

		private static Skin _defaultSkin;

		private Skin _skin;

		private int _imagePadding;

		private Point _textOffset;

		private Point _imageOffset;

		private bool _shouldDrawFocus;

		private bool _acceptsFocus = true;

		private static readonly byte[] buttonskin_png;

		private static readonly byte[] FlatButtonSkin_png;

		[Category("Appearance")]
		public Skin Skin
		{
			get
			{
				if (_skin == null)
				{
					CreateDefaultSkin();
				}
				return _skin;
			}
			set
			{
				if (_skin != null)
				{
					_skin.Changed -= _skin_Changed;
				}
				_skin = value;
				if (value != null)
				{
					_skin.Changed += _skin_Changed;
				}
				base.Invalidate();
			}
		}

		public static Skin DefaultSkin
		{
			get
			{
				return _defaultSkin;
			}
			set
			{
				_defaultSkin = value;
			}
		}

		public override Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}

		[DefaultValue(0)]
		[Category("Appearance")]
		public int ImagePadding
		{
			get
			{
				return _imagePadding;
			}
			set
			{
				_imagePadding = value;
				base.Invalidate();
			}
		}

		[Category("Appearance")]
		public Point TextOffset
		{
			get
			{
				return _textOffset;
			}
			set
			{
				_textOffset = value;
				base.Invalidate();
			}
		}

		[Category("Appearance")]
		public Point ImageOffset
		{
			get
			{
				return _imageOffset;
			}
			set
			{
				_imageOffset = value;
				base.Invalidate();
			}
		}

		public override Color ForeColor
		{
			get
			{
				return base.ForeColor;
			}
			set
			{
				base.ForeColor = value;
			}
		}

		[Description("Indicates if the focus rectangle should be drawn when focused.")]
		[Category("Appearance")]
		[DefaultValue(true)]
		public bool ShouldDrawFocus
		{
			get
			{
				return _shouldDrawFocus;
			}
			set
			{
				_shouldDrawFocus = value;
			}
		}

		[DefaultValue(true)]
		[Category("Behavior")]
		public bool AcceptsFocus
		{
			get
			{
				return _acceptsFocus;
			}
			set
			{
				_acceptsFocus = value;
			}
		}

		[DefaultValue(false)]
		[Obsolete("Too many bugs in the GDI+ support so it's no longer supported.")]
		public bool UseVisualStyle
		{
			get
			{
				return false;
			}
			set
			{
				base.Invalidate();
			}
		}

		public SkinnedButton()
		{
			base.SetStyle(ControlStyles.UserPaint | ControlStyles.ResizeRedraw | ControlStyles.SupportsTransparentBackColor | ControlStyles.AllPaintingInWmPaint | ControlStyles.CacheText | ControlStyles.OptimizedDoubleBuffer, true);
			BackColor = Color.Transparent;
			ForeColor = Skin.TextColor;
			base.Size = new Size(100, 25);
		}

		static SkinnedButton()
		{
			buttonskin_png = new byte[1409]
			{
				137,
				80,
				78,
				71,
				13,
				10,
				26,
				10,
				0,
				0,
				0,
				13,
				73,
				72,
				68,
				82,
				0,
				0,
				0,
				20,
				0,
				0,
				0,
				132,
				8,
				6,
				0,
				0,
				0,
				144,
				239,
				11,
				78,
				0,
				0,
				0,
				25,
				116,
				69,
				88,
				116,
				83,
				111,
				102,
				116,
				119,
				97,
				114,
				101,
				0,
				65,
				100,
				111,
				98,
				101,
				32,
				73,
				109,
				97,
				103,
				101,
				82,
				101,
				97,
				100,
				121,
				113,
				201,
				101,
				60,
				0,
				0,
				5,
				35,
				73,
				68,
				65,
				84,
				120,
				218,
				236,
				153,
				207,
				139,
				28,
				69,
				20,
				199,
				235,
				189,
				234,
				157,
				221,
				196,
				128,
				236,
				127,
				224,
				193,
				75,
				32,
				136,
				27,
				130,
				23,
				89,
				179,
				46,
				34,
				194,
				70,
				111,
				138,
				8,
				122,
				209,
				131,
				51,
				120,
				18,
				4,
				205,
				89,
				100,
				17,
				241,
				36,
				219,
				123,
				208,
				75,
				4,
				65,
				241,
				226,
				143,
				92,
				66,
				72,
				102,
				55,
				230,
				18,
				130,
				139,
				226,
				65,
				60,
				232,
				65,
				240,
				186,
				23,
				53,
				187,
				51,
				211,
				93,
				190,
				87,
				175,
				170,
				171,
				102,
				152,
				33,
				59,
				83,
				53,
				72,
				96,
				26,
				106,
				187,
				123,
				186,
				235,
				51,
				223,
				122,
				245,
				234,
				125,
				187,
				103,
				65,
				209,
				246,
				237,
				141,
				31,
				119,
				104,
				215,
				86,
				105,
				91,
				249,
				194,
				230,
				249,
				14,
				124,
				67,
				48,
				32,
				216,
				243,
				79,
				175,
				37,
				209,
				190,
				187,
				121,
				160,
				12,
				65,
				21,
				1,
				77,
				174,
				141,
				89,
				72,
				123,
				149,
				107,
				99,
				22,
				214,
				117,
				62,
				32,
				179,
				10,
				166,
				230,
				82,
				57,
				79,
				133,
				249,
				98,
				88,
				212,
				150,
				150,
				135,
				200,
				172,
				57,
				40,
				172,
				51,
				42,
				228,
				24,
				86,
				117,
				157,
				77,
				33,
				179,
				10,
				134,
				25,
				149,
				43,
				109,
				212,
				28,
				20,
				214,
				244,
				39,
				23,
				177,
				14,
				67,
				86,
				25,
				135,
				92,
				213,
				217,
				150,
				30,
				179,
				108,
				98,
				231,
				2,
				50,
				11,
				153,
				154,
				107,
				179,
				10,
				89,
				93,
				174,
				2,
				97,
				171,
				205,
				113,
				127,
				80,
				238,
				126,
				121,
				83,
				245,
				251,
				131,
				164,
				198,
				12,
				102,
				1,
				147,
				223,
				253,
				248,
				171,
				29,
				141,
				216,
				78,
				204,
				193,
				114,
				251,
				237,
				151,
				58,
				5,
				159,
				60,
				215,
				123,
				95,
				33,
				98,
				114,
				14,
				110,
				211,
				30,
				186,
				31,
				62,
				190,
				131,
				0,
				237,
				245,
				183,
				62,
				75,
				2,
				222,
				250,
				228,
				117,
				158,
				229,
				18,
				246,
				63,
				58,
				111,
				214,
				219,
				59,
				242,
				233,
				209,
				47,
				179,
				209,
				86,
				206,
				9,
				180,
				236,
				168,
				66,
				1,
				133,
				209,
				244,
				149,
				186,
				119,
				48,
				123,
				25,
				251,
				247,
				142,
				82,
				167,
				200,
				215,
				137,
				21,
				128,
				245,
				189,
				132,
				186,
				232,
				24,
				2,
				68,
				7,
				60,
				202,
				0,
				68,
				175,
				112,
				64,
				192,
				30,
				207,
				213,
				140,
				64,
				20,
				134,
				85,
				200,
				233,
				98,
				42,
				98,
				29,
				39,
				2,
				137,
				129,
				141,
				194,
				74,
				36,
				167,
				2,
				155,
				24,
				170,
				113,
				64,
				184,
				223,
				202,
				29,
				6,
				50,
				195,
				198,
				16,
				89,
				97,
				45,
				223,
				208,
				0,
				65,
				26,
				192,
				24,
				176,
				113,
				21,
				62,
				118,
				75,
				35,
				12,
				108,
				20,
				26,
				7,
				115,
				55,
				0,
				4,
				176,
				189,
				14,
				195,
				29,
				193,
				132,
				18,
				45,
				11,
				79,
				174,
				137,
				66,
				223,
				1,
				229,
				130,
				5,
				104,
				217,
				131,
				14,
				199,
				22,
				192,
				215,
				43,
				23,
				115,
				127,
				92,
				187,
				190,
				16,
				77,
				138,
				237,
				92,
				56,
				33,
				90,
				64,
				176,
				68,
				55,
				112,
				237,
				40,
				2,
				144,
				191,
				208,
				166,
				216,
				64,
				246,
				54,
				85,
				42,
				25,
				152,
				101,
				248,
				180,
				97,
				128,
				94,
				118,
				67,
				100,
				88,
				139,
				96,
				45,
				129,
				218,
				166,
				157,
				66,
				55,
				121,
				72,
				205,
				80,
				222,
				214,
				125,
				129,
				218,
				225,
				210,
				61,
				168,
				93,
				12,
				45,
				224,
				148,
				196,
				142,
				143,
				237,
				249,
				114,
				128,
				170,
				88,
				97,
				95,
				22,
				129,
				161,
				188,
				133,
				158,
				156,
				219,
				225,
				183,
				156,
				66,
				141,
				210,
				73,
				159,
				86,
				1,
				190,
				66,
				123,
				15,
				44,
				70,
				128,
				164,
				8,
				89,
				29,
				95,
				59,
				22,
				176,
				5,
				50,
				195,
				199,
				144,
				135,
				91,
				156,
				113,
				113,
				116,
				234,
				192,
				15,
				185,
				8,
				105,
				228,
				129,
				198,
				133,
				132,
				161,
				102,
				89,
				66,
				193,
				140,
				38,
				177,
				89,
				81,
				241,
				112,
				136,
				95,
				211,
				156,
				58,
				159,
				70,
				198,
				68,
				208,
				158,
				124,
				49,
				239,
				237,
				178,
				91,
				161,
				219,
				188,
				66,
				190,
				176,
				66,
				245,
				108,
				240,
				71,
				52,
				17,
				69,
				148,
				50,
				35,
				121,
				104,
				87,
				214,
				146,
				40,
				101,
				120,
				241,
				136,
				155,
				24,
				80,
				176,
				119,
				229,
				82,
				169,
				117,
				241,
				230,
				147,
				47,
				126,
				16,
				37,
				245,
				73,
				86,
				138,
				143,
				169,
				28,
				223,
				254,
				250,
				50,
				27,
				213,
				174,
				189,
				187,
				123,
				101,
				107,
				7,
				19,
				93,
				143,
				76,
				170,
				220,
				120,
				237,
				170,
				184,
				158,
				250,
				235,
				79,
				165,
				18,
				93,
				79,
				213,
				82,
				7,
				22,
				174,
				183,
				112,
				189,
				133,
				235,
				45,
				92,
				111,
				225,
				122,
				11,
				215,
				203,
				230,
				122,
				252,
				6,
				185,
				247,
				249,
				165,
				44,
				174,
				119,
				241,
				213,
				239,
				59,
				5,
				144,
				138,
				238,
				246,
				99,
				89,
				92,
				143,
				89,
				193,
				245,
				94,
				121,
				38,
				205,
				245,
				190,
				184,
				110,
				93,
				15,
				121,
				168,
				235,
				47,
				111,
				138,
				175,
				38,
				52,
				102,
				48,
				75,
				102,
				185,
				202,
				244,
				187,
				72,
				112,
				189,
				172,
				64,
				108,
				30,
				35,
				210,
				129,
				56,
				15,
				133,
				156,
				46,
				185,
				126,
				54,
				69,
				156,
				87,
				12,
				115,
				253,
				248,
				213,
				184,
				222,
				36,
				34,
				192,
				100,
				11,
				29,
				215,
				37,
				184,
				222,
				4,
				16,
				120,
				231,
				139,
				173,
				152,
				97,
				238,
				179,
				209,
				80,
				5,
				215,
				27,
				129,
				53,
				32,
				119,
				28,
				215,
				67,
				3,
				145,
				39,
				143,
				196,
				63,
				184,
				30,
				12,
				195,
				252,
				103,
				113,
				107,
				128,
				163,
				45,
				130,
				6,
				215,
				139,
				58,
				161,
				59,
				70,
				241,
				217,
				160,
				216,
				11,
				51,
				146,
				102,
				254,
				151,
				209,
				90,
				10,
				171,
				61,
				110,
				20,
				98,
				164,
				4,
				99,
				24,
				186,
				107,
				17,
				144,
				1,
				200,
				5,
				1,
				4,
				10,
				94,
				169,
				138,
				92,
				79,
				187,
				202,
				236,
				193,
				252,
				25,
				122,
				24,
				14,
				3,
				209,
				200,
				147,
				7,
				184,
				39,
				6,
				222,
				251,
				149,
				27,
				92,
				79,
				135,
				225,
				98,
				4,
				155,
				52,
				228,
				38,
				206,
				14,
				134,
				177,
				66,
				86,
				160,
				113,
				60,
				16,
				70,
				124,
				5,
				76,
				244,
				216,
				97,
				130,
				187,
				186,
				33,
				3,
				104,
				167,
				176,
				136,
				76,
				41,
				30,
				42,
				194,
				112,
				94,
				26,
				21,
				82,
				5,
				234,
				16,
				142,
				104,
				150,
				209,
				24,
				216,
				253,
				225,
				250,
				175,
				50,
				108,
				219,
				208,
				62,
				163,
				132,
				56,
				78,
				106,
				238,
				30,
				215,
				239,
				54,
				49,
				104,
				190,
				230,
				245,
				174,
				247,
				243,
				239,
				25,
				223,
				245,
				222,
				57,
				107,
				213,
				173,
				109,
				93,
				160,
				199,
				193,
				37,
				11,
				134,
				251,
				62,
				95,
				251,
				144,
				74,
				130,
				215,
				131,
				190,
				58,
				184,
				122,
				215,
				170,
				132,
				253,
				247,
				206,
				153,
				181,
				173,
				39,
				212,
				210,
				67,
				103,
				8,
				216,
				178,
				177,
				1,
				56,
				33,
				208,
				174,
				148,
				138,
				128,
				61,
				213,
				byte.MaxValue,
				231,
				111,
				130,
				222,
				145,
				89,
				214,
				203,
				43,
				212,
				78,
				211,
				227,
				76,
				139,
				158,
				151,
				240,
				4,
				111,
				0,
				145,
				198,
				138,
				12,
				94,
				23,
				4,
				141,
				94,
				124,
				248,
				3,
				11,
				91,
				98,
				160,
				158,
				178,
				236,
				87,
				18,
				59,
				98,
				200,
				90,
				166,
				92,
				3,
				30,
				166,
				70,
				11,
				227,
				227,
				233,
				39,
				4,
				165,
				31,
				177,
				176,
				89,
				17,
				39,
				30,
				230,
				132,
				151,
				71,
				87,
				88,
				48,
				57,
				93,
				70,
				92,
				15,
				155,
				229,
				149,
				5,
				56,
				31,
				133,
				185,
				129,
				58,
				227,
				144,
				245,
				131,
				49,
				41,
				58,
				163,
				66,
				141,
				15,
				70,
				30,
				206,
				35,
				177,
				147,
				byte.MaxValue,
				11,
				110,
				134,
				159,
				28,
				184,
				226,
				214,
				213,
				128,
				206,
				139,
				233,
				81,
				85,
				101,
				251,
				50,
				195,
				214,
				195,
				90,
				153,
				242,
				238,
				79,
				191,
				181,
				47,
				172,
				157,
				165,
				34,
				219,
				159,
				186,
				124,
				25,
				170,
				216,
				213,
				241,
				145,
				34,
				134,
				98,
				150,
				184,
				222,
				167,
				207,
				146,
				175,
				64,
				162,
				235,
				153,
				114,
				227,
				141,
				107,
				226,
				122,
				245,
				163,
				151,
				213,
				216,
				7,
				207,
				169,
				254,
				209,
				202,
				174,
				119,
				77,
				193,
				141,
				110,
				151,
				222,
				245,
				176,
				125,
				241,
				169,
				245,
				36,
				224,
				222,
				254,
				45,
				134,
				150,
				208,
				221,
				219,
				55,
				30,
				118,
				120,
				120,
				56,
				19,
				108,
				117,
				117,
				181,
				129,
				54,
				227,
				156,
				21,
				54,
				218,
				55,
				227,
				186,
				91,
				0,
				23,
				192,
				5,
				112,
				1,
				252,
				95,
				128,
				190,
				72,
				166,
				20,
				88,
				222,
				10,
				46,
				219,
				84,
				105,
				173,
				5,
				164,
				64,
				27,
				11,
				224,
				19,
				239,
				43,
				137,
				38,
				85,
				110,
				110,
				108,
				116,
				254,
				19,
				96,
				0,
				132,
				248,
				224,
				230,
				76,
				169,
				96,
				126,
				0,
				0,
				0,
				0,
				73,
				69,
				78,
				68,
				174,
				66,
				96,
				130
			};
			FlatButtonSkin_png = buttonskin_png;
			_defaultSkin = new Skin();
			using (MemoryStream stream = new MemoryStream(buttonskin_png))
			{
				_defaultSkin.Image = (Image.FromStream(stream) as Bitmap);
			}
			using (MemoryStream stream2 = new MemoryStream(FlatButtonSkin_png))
			{
				_defaultSkin.FlatImage = (Image.FromStream(stream2) as Bitmap);
			}
			_defaultSkin.EdgeBottomHeight = 3;
			_defaultSkin.EdgeLeftWidth = 3;
			_defaultSkin.EdgeRightWidth = 3;
			_defaultSkin.EdgeTopHeight = 3;
			_defaultSkin.Shadow.Opacity = 48;
		}

		private bool ShouldSerializeSkin()
		{
			return _skin != _defaultSkin;
		}

		private void ResetSkin()
		{
			if (_skin != null)
			{
				_skin.Changed -= _skin_Changed;
			}
			_skin = null;
			CreateDefaultSkin();
		}

		private bool ShouldSerializeBackColor()
		{
			if (BackColor != Color.Empty)
			{
				return BackColor != Color.Transparent;
			}
			return false;
		}

		private bool ShouldSerializeTextOffset()
		{
			return !_textOffset.IsEmpty;
		}

		private void ResetTextOffset()
		{
			_textOffset = Point.Empty;
		}

		private bool ShouldSerializeImageOffset()
		{
			return !_imageOffset.IsEmpty;
		}

		private void ResetImageOffset()
		{
			_imageOffset = Point.Empty;
		}

		private bool ShouldSerializeForeColor()
		{
			if (ForeColor != Color.Empty)
			{
				return ForeColor != Skin.TextColor;
			}
			return false;
		}

		protected override void OnMouseEnter(EventArgs e)
		{
			_isHot = true;
			base.Invalidate();
			base.OnMouseEnter(e);
		}

		protected override void OnMouseLeave(EventArgs e)
		{
			_isHot = false;
			base.Invalidate();
			base.OnMouseLeave(e);
		}

		protected override void OnMouseDown(MouseEventArgs mevent)
		{
			if (mevent.Button == MouseButtons.Left)
			{
				_isDown = true;
				base.Invalidate();
			}
			base.OnMouseDown(mevent);
		}

		protected override void OnMouseUp(MouseEventArgs mevent)
		{
			if (mevent.Button == MouseButtons.Left)
			{
				_isDown = false;
				base.Invalidate();
			}
			base.OnMouseUp(mevent);
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			if (_skin == null)
			{
				CreateDefaultSkin();
			}
			if (_skin != null && _skin.Image != null && _skin.Image.Height % 6 == 0)
			{
				base.OnPaintBackground(e);
				Rectangle clientRectangle = base.ClientRectangle;
				Graphics graphics = e.Graphics;
				Color textColor = _skin.TextColor;
				bool num = base.FlatStyle == FlatStyle.Popup || base.FlatStyle == FlatStyle.Flat;
				bool flat = num;
				Bitmap bitmap = num ? _skin.FlatImage : _skin.Image;
				if (bitmap == null)
				{
					base.OnPaint(e);
				}
				else
				{
					using (Bitmap state = GetStateImage(flat, bitmap))
					{
						DrawSkinState(graphics, state, clientRectangle);
					}
					int num2 = _skin.TextOffsetX;
					int num3 = _skin.TextOffsetY;
					if (_isDown)
					{
						num2 += _skin.DownTextDeltaX;
						num3 += _skin.DownTextDeltaY;
					}
					Rectangle rectangle = new Rectangle(_skin.EdgeLeftWidth, _skin.EdgeTopHeight, base.Width - (_skin.EdgeLeftWidth + _skin.EdgeRightWidth), base.Height - (_skin.EdgeBottomHeight + _skin.EdgeTopHeight));
					rectangle.Offset(num2, num3);
					TextImagePainter textImagePainter = new TextImagePainter();
					textImagePainter.Disabled = (!base.Enabled && !base.DesignMode);
					textImagePainter.Bounds = rectangle;
					textImagePainter.TextImageRelation = base.TextImageRelation;
					textImagePainter.Txt.Value = Text;
					textImagePainter.Txt.Color = textColor;
					textImagePainter.Txt.Font = Font;
					textImagePainter.Txt.Alignment = TextAlign;
					textImagePainter.Txt.Offset = TextOffset;
					textImagePainter.Img.Value = base.Image;
					textImagePainter.Img.Padding = ImagePadding;
					textImagePainter.Img.Offset = ImageOffset;
					TextImagePainter textImagePainter2 = textImagePainter;
					textImagePainter2.Draw(graphics);
					if (Focused && _shouldDrawFocus)
					{
						ControlPaint.DrawFocusRectangle(graphics, rectangle);
					}
				}
			}
			else
			{
				base.OnPaint(e);
			}
		}

		private void DrawSkinState(Graphics g, Bitmap state, Rectangle bounds)
		{
			if (state != null)
			{
				int num = _skin.EdgeRightWidth;
				int num2 = _skin.EdgeLeftWidth;
				int num3 = _skin.EdgeTopHeight;
				int num4 = _skin.EdgeBottomHeight;
				if (num2 < 1)
				{
					num2 = 1;
				}
				if (num < 1)
				{
					num = 1;
				}
				if (num3 < 1)
				{
					num3 = 1;
				}
				if (num4 < 1)
				{
					num4 = 1;
				}
				if (bounds.Width >= num2 + num && bounds.Height >= num3 + num4)
				{
					using (Image image = ImageEffects.Clone(state, new Rectangle(0, 0, num2, num3), PixelFormat.Format32bppArgb))
					{
						g.DrawImage(image, 0, 0);
					}
					using (Image image2 = ImageEffects.Clone(state, new Rectangle(state.Width - num, 0, num, num3), PixelFormat.Format32bppArgb))
					{
						g.DrawImage(image2, bounds.Right - num, 0);
					}
					using (Image image3 = ImageEffects.Clone(state, new Rectangle(state.Width - num, state.Height - num4, num, num4), PixelFormat.Format32bppArgb))
					{
						g.DrawImage(image3, bounds.Right - num, bounds.Bottom - num4);
					}
					using (Image image4 = ImageEffects.Clone(state, new Rectangle(0, state.Height - num4, num2, num4), PixelFormat.Format32bppArgb))
					{
						g.DrawImage(image4, 0, bounds.Bottom - num4);
					}
					if (_skin.EdgeScalingTop == EdgeScaling.Stretch)
					{
						g.DrawImage(state, new RectangleF((float)num2, 0f, (float)(bounds.Width - num2 - num), (float)num3), new RectangleF((float)num2, 0f, (float)(state.Width - num2 - num - 1), (float)num3), GraphicsUnit.Pixel);
					}
					else
					{
						using (Image image5 = ImageEffects.Clone(state, new Rectangle(num2, 0, state.Width - num2 - num, num3), PixelFormat.Format32bppArgb))
						{
							int i;
							for (i = num2; i < bounds.Right - num - image5.Width; i += image5.Width)
							{
								g.DrawImage(image5, i, 0);
							}
							if (i < bounds.Right - num)
							{
								g.DrawImage(image5, new Rectangle(i, 0, bounds.Right - i - num, image5.Height), new Rectangle(0, 0, bounds.Right - i - num, image5.Height), GraphicsUnit.Pixel);
							}
						}
					}
					if (_skin.EdgeScalingBottom == EdgeScaling.Stretch)
					{
						g.DrawImage(state, new RectangleF((float)num2, (float)(bounds.Bottom - num4), (float)(bounds.Width - num2 - num), (float)num4), new RectangleF((float)num2, (float)(state.Height - num4), (float)(state.Width - num2 - num - 1), (float)num4), GraphicsUnit.Pixel);
					}
					else
					{
						using (Image image6 = ImageEffects.Clone(state, new Rectangle(num2, state.Height - num4, state.Width - num2 - num - 1, num4), PixelFormat.Format32bppArgb))
						{
							int j;
							for (j = num2; j < bounds.Right - num - image6.Width; j += image6.Width)
							{
								g.DrawImage(image6, j, bounds.Height - image6.Height);
							}
							if (j < bounds.Right - num)
							{
								g.DrawImage(image6, new Rectangle(j, bounds.Height - image6.Height, bounds.Right - j - num, image6.Height), new Rectangle(0, 0, bounds.Right - j - num, image6.Height), GraphicsUnit.Pixel);
							}
						}
					}
					if (_skin.EdgeScalingLeft == EdgeScaling.Stretch)
					{
						g.DrawImage(state, new RectangleF(0f, (float)num3, (float)num2, (float)(bounds.Height - num3 - num4)), new RectangleF(0f, (float)num3, (float)num2, (float)(state.Height - num3 - num4 - 1)), GraphicsUnit.Pixel);
					}
					else
					{
						using (Image image7 = ImageEffects.Clone(state, new Rectangle(0, num3, num2, state.Height - num3 - num4), PixelFormat.Format32bppArgb))
						{
							int num5 = 0;
							for (num5 = num3; num5 < bounds.Bottom - num4 - image7.Height; num5 += image7.Height)
							{
								g.DrawImage(image7, 0, num5);
							}
							if (num5 < bounds.Bottom - num4)
							{
								g.DrawImage(image7, 0, num5, new Rectangle(0, 0, image7.Width, bounds.Height - num5 - num4), GraphicsUnit.Pixel);
							}
						}
					}
					if (_skin.EdgeScalingRight == EdgeScaling.Stretch)
					{
						g.DrawImage(state, new RectangleF((float)(bounds.Right - num), (float)num3, (float)num, (float)(bounds.Height - num3 - num4)), new RectangleF((float)(state.Width - num), (float)num3, (float)num, (float)(state.Height - num3 - num4 - 1)), GraphicsUnit.Pixel);
					}
					else
					{
						using (Image image8 = ImageEffects.Clone(state, new Rectangle(state.Width - num, num3, num, state.Height - num3 - num4), PixelFormat.Format32bppArgb))
						{
							int k;
							for (k = num3; k < bounds.Bottom - num4 - image8.Height; k += image8.Height)
							{
								g.DrawImage(image8, bounds.Right - num, k);
							}
							if (k < bounds.Bottom - num4)
							{
								g.DrawImage(image8, new Rectangle(bounds.Right - num, k, image8.Width, bounds.Height - k - num4), new Rectangle(0, 0, image8.Width, bounds.Height - k - num4), GraphicsUnit.Pixel);
							}
						}
					}
					if (_skin.EdgeScalingCenter == EdgeScaling.Stretch)
					{
						g.DrawImage(state, new RectangleF((float)num2, (float)num3, (float)(bounds.Right - num - num2), (float)(bounds.Height - num3 - num4)), new RectangleF((float)num2, (float)num3, (float)(state.Width - num - num2 - 1), (float)(state.Height - num3 - num4 - 1)), GraphicsUnit.Pixel);
					}
					else
					{
						using (Image image9 = ImageEffects.Clone(state, new Rectangle(num2, num3, state.Width - num - num2, state.Height - num3 - num4), PixelFormat.Format32bppArgb))
						{
							int l;
							for (l = num2; l < bounds.Right - num - image9.Width; l += image9.Width)
							{
								int m;
								for (m = num3; m < bounds.Bottom - num4 - image9.Height; m += image9.Height)
								{
									g.DrawImage(image9, l, m);
								}
								if (m < bounds.Bottom - num4)
								{
									g.DrawImage(image9, new Rectangle(l, m, image9.Width, bounds.Height - m - num4), new Rectangle(0, 0, image9.Width, bounds.Height - m - num4), GraphicsUnit.Pixel);
								}
							}
							if (l < bounds.Right - num)
							{
								int m;
								for (m = num3; m < bounds.Bottom - num4 - image9.Height; m += image9.Height)
								{
									g.DrawImage(image9, new Rectangle(l, m, bounds.Right - l - num, image9.Height), new Rectangle(0, 0, bounds.Right - l - num, image9.Height), GraphicsUnit.Pixel);
								}
								if (m < bounds.Bottom - num4)
								{
									g.DrawImage(image9, new Rectangle(l, m, bounds.Right - l - num, bounds.Height - m - num4), new Rectangle(0, 0, bounds.Right - l - num, bounds.Height - m - num4), GraphicsUnit.Pixel);
								}
							}
						}
					}
				}
			}
		}

		private Bitmap GetStateImage(bool flat, Bitmap image)
		{
			int num = _skin.Image.Height / 6;
			Bitmap result = null;
			if (base.DesignMode && flat)
			{
				result = image.Clone(new Rectangle(0, num + num, image.Width, num), PixelFormat.Format32bppArgb);
			}
			else if (!base.Enabled && !base.DesignMode)
			{
				if (!flat)
				{
					result = image.Clone(new Rectangle(0, num * 5, image.Width, num), PixelFormat.Format32bppArgb);
				}
			}
			else if (_isDown && _isHot)
			{
				result = image.Clone(new Rectangle(0, num * 4, image.Width, num), PixelFormat.Format32bppArgb);
			}
			else if (base.IsDefault && _isHot)
			{
				result = image.Clone(new Rectangle(0, num * 3, image.Width, num), PixelFormat.Format32bppArgb);
			}
			else if (_isHot)
			{
				result = image.Clone(new Rectangle(0, num + num, image.Width, num), PixelFormat.Format32bppArgb);
			}
			else if (base.IsDefault)
			{
				if (!flat)
				{
					result = image.Clone(new Rectangle(0, num, image.Width, num), PixelFormat.Format32bppArgb);
				}
			}
			else if (!flat)
			{
				result = image.Clone(new Rectangle(0, 0, image.Width, num), PixelFormat.Format32bppArgb);
			}
			return result;
		}

		private void CreateDefaultSkin()
		{
			if (_skin == null)
			{
				_skin = _defaultSkin;
				ForeColor = _skin.TextColor;
				_skin.Changed += _skin_Changed;
			}
		}

		private void _skin_Changed(object sender, EventArgs e)
		{
			if (_skin == _defaultSkin)
			{
				_skin.Changed -= _skin_Changed;
				_skin = new Skin(_defaultSkin);
				_skin.Changed += _skin_Changed;
			}
			if (ForeColor != _skin.TextColor)
			{
				ForeColor = _skin.TextColor;
			}
			base.Invalidate();
		}

		public override Size GetPreferredSize(Size proposedSize)
		{
			return new Size(100, 25);
		}

		protected override void WndProc(ref Message m)
		{
			if (!_acceptsFocus && m.Msg == 33)
			{
				m.Result = new IntPtr(3);
			}
			else
			{
				if (Cursor == Cursors.Hand && SafeNativeMethods.PreFilterMessage(ref m))
				{
					return;
				}
				base.WndProc(ref m);
			}
		}
	}
}
