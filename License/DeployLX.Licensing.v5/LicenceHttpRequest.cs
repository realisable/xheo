﻿using System;

namespace DeployLX.Licensing.v5
{
    public class LicenseHttpRequest
    {
        public string Command { get; set; }
        public byte[] LicenseXml { get; set; }
        public string LicenseId { get; set; }
        public string LimitId { get; set; }
        public string Key { get; set; }
        public string CurrentCulture { get; set; }
        public string Properties { get; set; }
        public string Salt { get; set; }
    }
}
