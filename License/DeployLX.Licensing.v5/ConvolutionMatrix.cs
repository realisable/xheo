namespace DeployLX.Licensing.v5
{
	internal struct ConvolutionMatrix
	{
		public static readonly ConvolutionMatrix Identity;

		public static readonly ConvolutionMatrix Median;

		public static readonly ConvolutionMatrix Sharpen;

		public static readonly ConvolutionMatrix Emboss;

		public static readonly ConvolutionMatrix EdgeDetect;

		public readonly int[,] Matrix;

		public readonly int Factor;

		public readonly int Offset;

		public readonly int DimX;

		public readonly int DimY;

		public readonly bool IsIsotropic;

		public ConvolutionMatrix(int[,] matrix, int factor, int offset)
		{
			Matrix = matrix;
			Factor = factor;
			Offset = offset;
			DimX = matrix.GetUpperBound(0) + 1;
			DimY = matrix.GetUpperBound(1) + 1;
			IsIsotropic = false;
		}

		public ConvolutionMatrix(int[,] matrix, int factor, int offset, bool isSymmetrical)
		{
			Matrix = matrix;
			Factor = factor;
			Offset = offset;
			DimX = matrix.GetUpperBound(0) + 1;
			DimY = matrix.GetUpperBound(1) + 1;
			IsIsotropic = isSymmetrical;
		}

		public ConvolutionMatrix(ConvolutionMatrix matrix)
		{
			Matrix = matrix.Matrix;
			Factor = matrix.Factor;
			Offset = matrix.Offset;
			DimX = matrix.DimX;
			DimY = matrix.DimY;
			IsIsotropic = matrix.IsIsotropic;
		}

		public ConvolutionMatrix Clone()
		{
			return new ConvolutionMatrix(this);
		}

		static ConvolutionMatrix()
		{
			int[,] array = new int[3, 3];
			array[1, 1] = 1;
			Identity = new ConvolutionMatrix(array, 1, 0);
			Median = new ConvolutionMatrix(new int[3, 3]
			{
				{
					1,
					1,
					1
				},
				{
					1,
					1,
					1
				},
				{
					1,
					1,
					1
				}
			}, 9, 0);
			Sharpen = new ConvolutionMatrix(new int[3, 3]
			{
				{
					0,
					-2,
					0
				},
				{
					-2,
					11,
					-2
				},
				{
					0,
					-2,
					0
				}
			}, 3, 0);
			Emboss = new ConvolutionMatrix(new int[3, 3]
			{
				{
					-1,
					0,
					-1
				},
				{
					0,
					4,
					0
				},
				{
					-1,
					0,
					-1
				}
			}, 1, 127);
			EdgeDetect = new ConvolutionMatrix(new int[3, 3]
			{
				{
					1,
					1,
					1
				},
				{
					0,
					0,
					0
				},
				{
					-1,
					-1,
					-1
				}
			}, 1, 127);
		}
	}
}
