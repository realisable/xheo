using System;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	[LimitEditor("InvalidLimitEditor", IconResource = "asmres://DeployLX.Licensing.DlxAddIn.v5/DeployLX.Licensing.DlxAddIn.v5.Resources.Limits.Invalid.png")]
	public class InvalidLimit : Limit
	{
		private string _message;

		private bool _dontReport;

		public override string Description
		{
			get
			{
				return SR.GetString("M_InvalidLimitDescription");
			}
		}

		public string Message
		{
			get
			{
				return _message;
			}
			set
			{
				base.AssertNotReadOnly();
				if (!(_message == value))
				{
					_message = value;
					base.OnChanged("Message");
				}
			}
		}

		public override string Name
		{
			get
			{
				return "Invalid";
			}
		}

		public bool DontReport
		{
			get
			{
				return _dontReport;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_dontReport != value)
				{
					_dontReport = value;
					base.OnChanged("DontReport");
				}
			}
		}

		public override ValidationResult Validate(SecureLicenseContext context)
		{
			if (_dontReport)
			{
				return ValidationResult.Invalid;
			}
			return context.ReportError("E_LicenseMarkedInvalid", this, _message ?? SR.GetString("M_DefaultInvalidLicenseMessage"));
		}

		public override ValidationResult Granted(SecureLicenseContext context)
		{
			if (_dontReport)
			{
				return ValidationResult.Invalid;
			}
			return context.ReportError("E_LicenseMarkedInvalid", this, _message ?? SR.GetString("M_DefaultInvalidLicenseMessage"));
		}

		protected override bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			if (_message != null)
			{
				writer.WriteAttributeString("message", _message);
			}
			if (_dontReport)
			{
				writer.WriteAttributeString("dontReport", "true");
			}
			return true;
		}

		protected override bool ReadFromXml(XmlReader reader)
		{
			_message = reader.GetAttribute("message");
			_dontReport = (reader.GetAttribute("dontReport") == "true");
			return base.ReadChildLimits(reader);
		}
	}
}
