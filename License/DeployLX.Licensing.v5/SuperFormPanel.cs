using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.IO;
using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	public class SuperFormPanel : UserControl
	{
		private delegate DialogResult ShowExceptionDelegate(string message, Exception ex, string messageId, MessageBoxExOptions options, MessageBoxButtons buttons);

		private delegate DialogResult ShowMessageBoxDelegate(string messageId, string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defaultButton, MessageBoxExOptions options, object param);

		internal PanelResultHandler _callBack;

		internal bool _skipPopOnSuccess;

		internal SuperForm _superForm;

		private Limit _limit;

		private bool _keepSupportVisible;

		internal bool _firstInitialization = true;

		private bool _canClose = true;

		public SuperForm SuperForm
		{
			get
			{
				return _superForm;
			}
		}

		public Limit Limit
		{
			get
			{
				return _limit;
			}
		}

		public virtual bool KeepSupportVisible
		{
			get
			{
				return _keepSupportVisible;
			}
			set
			{
				_keepSupportVisible = value;
				if (_superForm != null)
				{
					_superForm.KeepSupportVisible(value);
				}
			}
		}

		public bool FirstInitialization
		{
			get
			{
				return _firstInitialization;
			}
		}

		public bool CanClose
		{
			get
			{
				return _canClose;
			}
			set
			{
				_canClose = value;
			}
		}

		public event EventHandler ThemeChanged;

		protected internal virtual void OnThemeChanged(EventArgs e)
		{
			if (base.InvokeRequired)
			{
				base.Invoke(new EventHandler(FireOnThemeChanged), this, e);
			}
			else
			{
				FireOnThemeChanged(this, e);
			}
		}

		private void FireOnThemeChanged(object sender, EventArgs e)
		{
			UpdateFromTheme();
			if (this.ThemeChanged != null)
			{
				this.ThemeChanged(this, e);
			}
			base.Invalidate(true);
		}

		protected SuperFormPanel(Limit limit)
		{
			_limit = limit;
			base.SetStyle(ControlStyles.ContainerControl | ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer, true);
			base.Size = new Size(684, 421);
			BackColor = SuperFormTheme.DefaultBaseColor;
			base.DockPadding.All = 8;
			base.SetStyle(ControlStyles.SupportsTransparentBackColor, false);
			BackgroundImageLayout = ImageLayout.None;
			base.AutoScaleMode = AutoScaleMode.Dpi;
			Font = (SuperForm.GetInstalledFont(9f, FontStyle.Regular, "Segoe UI", "Tahoma") ?? Font);
		}

		private SuperFormPanel()
			: this(null)
		{
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			PaintPanelBackground(e);
		}

		internal void PaintPanelBackground(PaintEventArgs e)
		{
			base.OnPaintBackground(e);
			base.OnPaint(e);
			Image image = null;
			if (_superForm != null && _superForm.Theme != null)
			{
				image = _superForm.GetScaledImage(_superForm.Theme.Images["Panel", new string[1]
				{
					"Base"
				}], null);
			}
			if (image != null)
			{
				Point point = (base.Parent == null) ? base.Location : base.Parent.Location;
				e.Graphics.DrawImage(image, 0, 0, new Rectangle(point.X, point.Y, base.Width, base.Height), GraphicsUnit.Pixel);
			}
			else
			{
				Rectangle rect = new Rectangle(base.Width - 400, base.Height - 300, 700, 600);
				using (GraphicsPath graphicsPath = new GraphicsPath())
				{
					graphicsPath.AddEllipse(rect);
					using (PathGradientBrush pathGradientBrush = new PathGradientBrush(graphicsPath))
					{
						pathGradientBrush.CenterColor = GetThemeColor(Color.FromArgb(119, 145, 191), "PanelFade", "BaseLight", "Base");
						pathGradientBrush.SurroundColors = new Color[1]
						{
							Color.Transparent
						};
						e.Graphics.FillPath(pathGradientBrush, graphicsPath);
					}
				}
			}
			if (base.DesignMode)
			{
				e.Graphics.FillRectangle(Brushes.Black, new Rectangle(0, base.Height - 21, 170, 21));
			}
		}

		protected internal virtual void LoadPanel(SecureLicenseContext context)
		{
		}

		protected internal virtual void InitializePanel()
		{
			throw new NotSupportedException("You must override InitializePanel. Do not call base.InitializePanel.");
		}

		protected internal virtual void PanelShown(bool safeToChange)
		{
			Control control = _superForm.AcceptButton as Control;
			if (control != null)
			{
				control.Focus();
			}
		}

		protected Button AddBottomButton(string caption, int buffer)
		{
			SkinnedButton skinnedButton = new SkinnedButton();
			skinnedButton.Height = 27;
			skinnedButton.Top = 2;
			skinnedButton.Name = caption;
			skinnedButton.Text = SR.GetString(caption);
			SkinnedButton skinnedButton2 = skinnedButton;
			using (Bitmap image = new Bitmap(1, 1, PixelFormat.Format32bppArgb))
			{
				using (Graphics graphics = Graphics.FromImage(image))
				{
					StringFormat stringFormat = new StringFormat(StringFormat.GenericTypographic);
					stringFormat.HotkeyPrefix = HotkeyPrefix.Show;
					SizeF sizeF = graphics.MeasureString(skinnedButton2.Text, Font, SuperForm.Width, stringFormat);
					if (sizeF.Width > 95f)
					{
						skinnedButton2.Width = (int)Math.Ceiling((double)sizeF.Width) + 20;
					}
					else
					{
						skinnedButton2.Width = 95;
					}
				}
			}
			AddBottomControl(skinnedButton2, buffer);
			return skinnedButton2;
		}

		protected Button AddBottomButton(string caption)
		{
			return AddBottomButton(caption, -1);
		}

		protected Label AddBottomLabel(string caption, string link, Image icon, int buffer)
		{
			ThemeLabel themeLabel = new ThemeLabel();
			themeLabel.Text = ((caption == null) ? link : SR.GetString(caption));
			themeLabel.Url = link;
			themeLabel.Image = icon;
			themeLabel.AutoSize = true;
			themeLabel.TextAlign = ContentAlignment.MiddleLeft;
			themeLabel.BackColor = Color.Transparent;
			themeLabel.ThemeColor = new string[2]
			{
				"FooterLinkText",
				"FooterText"
			};
			themeLabel.Top = -1;
			ThemeLabel themeLabel2 = themeLabel;
			AddBottomControl(themeLabel2, buffer);
			int width = themeLabel2.Width;
			themeLabel2.AutoSize = false;
			themeLabel2.Height = 32;
			themeLabel2.Width = width + 8;
			themeLabel2.Top = 0;
			return themeLabel2;
		}

		protected Label AddBottomLabel(string caption, string link, Image icon)
		{
			return AddBottomLabel(caption, link, icon, -1);
		}

		protected void AddBottomControl(Control ctrl, int buffer)
		{
			_superForm.AddBottomControl(ctrl, buffer);
		}

		protected void AddBottomControl(Control ctrl)
		{
			_superForm.AddBottomControl(ctrl, -1);
		}

		protected void ClearBottomControls()
		{
			_superForm.ClearBottomControls();
		}

		protected void SetHeader(string title, string subTitle)
		{
			_superForm.SetHeader(title, subTitle);
		}

		protected void Return(FormResult result)
		{
			_superForm.Return(result);
		}

		public void ShowPage(string pageId, PanelResultHandler returnCallback, bool skipPopOnSuccess)
		{
			ShowPage(Limit as ISuperFormLimit, pageId, returnCallback, skipPopOnSuccess);
		}

		public void ShowPage(string pageId, PanelResultHandler returnCallback)
		{
			ShowPage(pageId, returnCallback, false);
		}

		public void ShowPage(ISuperFormLimit limit, string pageId, PanelResultHandler returnCallback, bool skipPopOnSuccess)
		{
			_superForm.Context.ShowForm(limit, pageId, returnCallback, skipPopOnSuccess);
		}

		protected void ShowPanel(SuperFormPanel panel, PanelResultHandler returnCallback, bool skipPopOnSuccess)
		{
			_callBack = returnCallback;
			_skipPopOnSuccess = skipPopOnSuccess;
			_superForm.ShowPanel(panel, true);
		}

		protected void ShowPanel(SuperFormPanel panel, PanelResultHandler returnCallback)
		{
			ShowPanel(panel, returnCallback, false);
		}

		protected internal virtual void FormActivated()
		{
		}

		protected DialogResult ShowDialogWithEffects(Form dialog)
		{
			return _superForm.ShowDialogWithEffects(dialog);
		}

		protected DialogResult ShowException(string message, Exception ex, string messageId, MessageBoxExOptions options, MessageBoxButtons buttons)
		{
			_superForm.BlurClient();
			if (base.InvokeRequired)
			{
				return (DialogResult)base.Invoke(new ShowExceptionDelegate(ShowException), message, ex, messageId, options, buttons);
			}
			MessageBoxEx messageBoxEx = MessageBoxEx.MakeExceptionMessageBox(this, message, ex, messageId, options, buttons);
			messageBoxEx.UsePanelStyle = true;
			return ShowDialogWithEffects(messageBoxEx);
		}

		protected DialogResult ShowMessageBox(string messageId, string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defaultButton, MessageBoxExOptions options, object param)
		{
			if (!base.IsDisposed && !SuperForm.IsDisposed)
			{
				if (caption == null)
				{
					caption = "UI_ValidationNoticeTitle";
				}
				if (base.InvokeRequired)
				{
					return (DialogResult)base.Invoke(new ShowMessageBoxDelegate(ShowMessageBox), messageId, text, caption, buttons, icon, defaultButton, options, param);
				}
				using (MessageBoxEx messageBoxEx = new MessageBoxEx(messageId, text, caption, buttons, icon, defaultButton, options, param))
				{
					messageBoxEx.UsePanelStyle = true;
					return ShowDialogWithEffects(messageBoxEx);
				}
			}
			return DialogResult.None;
		}

		protected DialogResult ShowMessageBox(string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon)
		{
			return ShowMessageBox(null, text, caption, buttons, icon, MessageBoxDefaultButton.Button1, MessageBoxExOptions.SupportButtons, null);
		}

		protected DialogResult ShowMessageBox(string text)
		{
			return ShowMessageBox(text, null, MessageBoxButtons.OK, MessageBoxIcon.None);
		}

		protected void ShowControlsWithEffects(TriBool show, params Control[] controls)
		{
			SuperForm.ShowControlsWithEffects(show, controls);
		}

		protected internal virtual void UpdateFromTheme()
		{
			if (_superForm != null)
			{
				BackColor = _superForm.Theme.Colors["Panel", new string[1]
				{
					"Base"
				}];
				UpdateChildControlTheme(base.Controls, this, false);
			}
		}

		private void UpdateChildControlTheme(ControlCollection controls, Control parent, bool skinnedPanel)
		{
			Color themeColor = GetThemeColor(Color.White, "Text");
			if (parent is SkinnedPanel || skinnedPanel)
			{
				themeColor = GetThemeColor(Color.Black, "GlassPanelText");
				skinnedPanel = true;
			}
			foreach (Control control in controls)
			{
				ThemeLabel themeLabel = control as ThemeLabel;
				if (themeLabel != null)
				{
					control.ForeColor = themeColor;
					switch (themeLabel.ThemeFont)
					{
					case ThemeFont.Bold:
						control.Font = _superForm.BoldFont;
						break;
					case ThemeFont.Medium:
						control.Font = _superForm.MediumFont;
						break;
					case ThemeFont.Title:
						control.Font = _superForm.TitleFont;
						break;
					case ThemeFont.Header:
						control.Font = _superForm.HeaderFont;
						break;
					case ThemeFont.Field:
						control.Font = _superForm.FieldFont;
						break;
					}
					if (themeLabel.ThemeColor != null && themeLabel.ThemeColor.Length > 0)
					{
						control.ForeColor = GetThemeColor(themeColor, themeLabel.ThemeColor[0], themeLabel.ThemeColor);
					}
					ShadowLabel shadowLabel = themeLabel as ShadowLabel;
					if (shadowLabel != null)
					{
						if (GetThemeProperty("UseTextDropShadows", "true") == "false")
						{
							shadowLabel.DropShadow.Visible = false;
						}
						else
						{
							shadowLabel.DropShadow.Color = GetThemeColor(Color.Black, "TextShadowColor");
						}
					}
				}
				if (control is RadioButton)
				{
					control.ForeColor = themeColor;
				}
				else if (control is CheckBox)
				{
					control.ForeColor = themeColor;
				}
				if (control is SkinnedButton)
				{
					((SkinnedButton)control).Skin = _superForm.Theme.ButtonSkin;
				}
				else if (control is Panel)
				{
					SkinnedPanel skinnedPanel2 = control as SkinnedPanel;
					if (skinnedPanel2 != null && _superForm != null)
					{
						skinnedPanel2.Skin = _superForm.Theme.Images["GlassPanelSkin"];
					}
					UpdateChildControlTheme(control.Controls, control, skinnedPanel);
				}
			}
		}

		protected Color GetThemeColor(Color defaultColor, string name, params string[] inherit)
		{
			if (_superForm == null)
			{
				return defaultColor;
			}
			Color color = _superForm.Theme.Colors[name, inherit];
			if (color == Color.Empty)
			{
				return defaultColor;
			}
			return color;
		}

		protected string GetThemeProperty(string name, string defaultValue)
		{
			if (_superForm == null)
			{
				return defaultValue;
			}
			if (_superForm.Theme.Properties.Dictionary.Contains(name))
			{
				return _superForm.Theme.Properties[name];
			}
			return defaultValue;
		}

		protected internal virtual void HandleKeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyData == (Keys)393337)
			{
				HandleSelectExistingLicense();
			}
			else if (e.KeyData == (Keys)393338)
			{
				HandleNewSerial();
			}
		}

		protected virtual void HandleNewSerial()
		{
			if (Limit != null && Limit.License.CanUnlockBySerial)
			{
				SecureLicenseContext context = SuperForm.Context;
				context.RequestInfo.ShouldGetNewSerialNumber = true;
				context.HasFormBeenShown = false;
				context.RetryWith(Limit.License.LicenseFile, null);
				Return(FormResult.Retry);
			}
		}

		protected virtual void HandleSelectExistingLicense()
		{
			using (OpenFileDialog openFileDialog = new OpenFileDialog())
			{
				openFileDialog.Title = SR.GetString("UI_SelectLicenseFile");
				openFileDialog.Filter = string.Format("{0} (*.lic)|*.lic;*.lic.xml", SR.GetString("UI_LicenseFiles"));
				if (openFileDialog.ShowDialog(this) == DialogResult.OK)
				{
					try
					{
						LicenseFile licenseFile = new LicenseFile(openFileDialog.FileName);
						string text = Path.Combine(Config.SharedFolder, Path.GetFileName(openFileDialog.FileName));
						if (!File.Exists(text) || ShowMessageBox("UI_LicenseFileExists", null, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation) == DialogResult.Yes)
						{
							licenseFile.Save(text, false, LicenseSaveType.Normal, false, true);
							_superForm.Context.RetryWith(licenseFile, null);
							Return(FormResult.Retry);
						}
					}
					catch (Exception ex)
					{
						ShowException(null, ex, null, MessageBoxExOptions.SupportButtons, MessageBoxButtons.OK);
					}
				}
			}
		}

		protected virtual void HandleEula()
		{
			ShowPage("EULA", null);
		}

		protected void AddLicenseAgreementLink()
		{
			if (Limit.License.EulaAddress != null)
			{
				Label label = AddBottomLabel("UI_LicenseAgreement", "#", Images.License_png);
				label.Left = _superForm.LeftmostButtonEdge + 4;
				label.BringToFront();
				label.Click += _eulaLink_LinkClicked;
			}
		}

		private void _eulaLink_LinkClicked(object sender, EventArgs e)
		{
			HandleEula();
		}

		protected virtual void HandleBuyNow()
		{
			if (Limit is IPurchaseLimit)
			{
				SharedToolbox.OpenUrl(Toolbox.ResolveUrl(((IPurchaseLimit)Limit).PurchaseUrl, _superForm.Context).ToString(), true);
			}
		}

		private void InitializeComponent()
		{
			base.SuspendLayout();
			base.AutoScaleDimensions = new SizeF(96f, 96f);
			base.AutoScaleMode = AutoScaleMode.Dpi;
			AutoSize = true;
			base.Name = "SuperFormPanel";
			base.ResumeLayout(false);
		}
	}
}
