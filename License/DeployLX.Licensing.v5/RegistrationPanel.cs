using System;
using System.Collections;
using System.Collections.Specialized;
using System.Drawing;
using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	public class RegistrationPanel : SplitPanel
	{
		private TextBox _name;

		private TextBox _organization;

		private LicenseCodeTextBox _serialNumber;

		private Button _register;

		private ShadowLabel _registerTitle;

		private SkinnedPanel _registrationPanel;

		private ShadowLabel _registrationNotice;

		private BufferedPanel _regControlsPanel;

		private ShadowLabel _privacyPolicyLink;

		private CheckBox _activateOption;

		private AsyncValidationRequest _registerRequest;

		private int _nextFieldTop;

		public RegistrationPanel(RegistrationLimit limit)
			: base(limit)
		{
			Check.NotNull(limit, "limit");
			InitializeComponent();
			_regControlsPanel.Scroll += _regControlsPanel_Scroll;
		}

		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			_registerTitle = new ShadowLabel();
			_registrationPanel = new SkinnedPanel();
			_regControlsPanel = new BufferedPanel();
			_registrationNotice = new ShadowLabel();
			_privacyPolicyLink = new ShadowLabel();
			_activateOption = new CheckBox();
			base.SidePanel.SuspendLayout();
			base.BodyPanel.SuspendLayout();
			_registrationPanel.SuspendLayout();
			base.SuspendLayout();
			base.SidePanel.Controls.Add(_privacyPolicyLink);
			base.SidePanel.Location = new Point(0, 0);
			base.SidePanel.Size = new Size(170, 421);
			base.SidePanel.TabIndex = 0;
			base.SidePanel.Controls.SetChildIndex(_privacyPolicyLink, 0);
			base.BodyPanel.Controls.Add(_activateOption);
			base.BodyPanel.Controls.Add(_registrationNotice);
			base.BodyPanel.Controls.Add(_registrationPanel);
			base.BodyPanel.Controls.Add(_registerTitle);
			base.BodyPanel.Location = new Point(170, 0);
			base.BodyPanel.Size = new Size(514, 421);
			_registerTitle.Location = new Point(14, 16);
			_registerTitle.Name = "_registerTitle";
			_registerTitle.Size = new Size(482, 34);
			_registerTitle.TabIndex = 0;
			_registerTitle.Text = "UI_RegistrationTitle";
			_registerTitle.ThemeColor = new string[2]
			{
				"PanelTitleText",
				"Text"
			};
			_registrationPanel.Anchor = (AnchorStyles.Bottom | AnchorStyles.Left);
			_registrationPanel.BackColor = Color.Transparent;
			_registrationPanel.BackgroundImageLayout = ImageLayout.None;
			_registrationPanel.Controls.Add(_regControlsPanel);
			_registrationPanel.Location = new Point(13, 122);
			_registrationPanel.Name = "_registrationPanel";
			_registrationPanel.PaintFakeBackground = true;
			_registrationPanel.Size = new Size(488, 30);
			_registrationPanel.TabIndex = 0;
			_regControlsPanel.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			_regControlsPanel.AutoScroll = true;
			_regControlsPanel.AutoScrollMargin = new Size(0, 4);
			_regControlsPanel.BackgroundImageLayout = ImageLayout.None;
			_regControlsPanel.Location = new Point(13, 13);
			_regControlsPanel.Name = "_regControlsPanel";
			_regControlsPanel.Size = new Size(462, 0);
			_regControlsPanel.TabIndex = 2;
			_registrationNotice.Location = new Point(16, 50);
			_registrationNotice.Name = "_registrationNotice";
			_registrationNotice.Size = new Size(482, 64);
			_registrationNotice.TabIndex = 13;
			_registrationNotice.Text = "UI_RegistrationNotice";
			_registrationNotice.ThemeFont = ThemeFont.Medium;
			_privacyPolicyLink.Location = new Point(20, 146);
			_privacyPolicyLink.Name = "_privacyPolicyLink";
			_privacyPolicyLink.Size = new Size(130, 18);
			_privacyPolicyLink.TabIndex = 5;
			_privacyPolicyLink.Text = "#UI_PrivacyPolicy";
			_privacyPolicyLink.LinkClicked += _privacyPolicyLink_LinkClicked;
			_activateOption.Anchor = (AnchorStyles.Bottom | AnchorStyles.Left);
			_activateOption.BackColor = Color.Transparent;
			_activateOption.Location = new Point(24, 158);
			_activateOption.Name = "_activateOption";
			_activateOption.Size = new Size(430, 17);
			_activateOption.TabIndex = 14;
			_activateOption.Text = "#UI_ActivateAutoOnline";
			_activateOption.UseVisualStyleBackColor = false;
			base.AutoScaleDimensions = new SizeF(96f, 96f);
			base.Name = "RegistrationPanel";
			base.SidePanel.ResumeLayout(false);
			base.BodyPanel.ResumeLayout(false);
			_registrationPanel.ResumeLayout(false);
			base.ResumeLayout(false);
		}

		protected internal override void LoadPanel(SecureLicenseContext context)
		{
			base.LoadPanel(context);
			RegistrationLimit registrationLimit = base.Limit as RegistrationLimit;
			if (registrationLimit.LogoResource != null)
			{
				base.Logo = Toolbox.GetImage(registrationLimit.LogoResource, context);
			}
			_registerTitle.Text = SR.GetString(_registerTitle.Text, base._superForm.Context.SupportInfo.Product);
			if (registrationLimit.IsMandatory)
			{
				_registrationNotice.Text = SR.GetString("UI_RegistrationMandatoryNotice", base._superForm.Context.SupportInfo.Product);
			}
			else
			{
				TrialLimit trialLimit = registrationLimit.License.Limits.FindLimitByType(typeof(TrialLimit), true) as TrialLimit;
				if (trialLimit != null)
				{
					if (trialLimit.CanContinueTrial(base._superForm.Context))
					{
						_registrationNotice.Text = SR.GetString("UI_RegistrationOrTrialNotice", base._superForm.Context.SupportInfo.Product);
					}
					else
					{
						_registrationNotice.Text = SR.GetString("UI_RegistrationMandatoryNotice", base._superForm.Context.SupportInfo.Product);
					}
				}
				else
				{
					_registrationNotice.Text = SR.GetString("UI_RegistrationNotice", base._superForm.Context.SupportInfo.Product);
				}
			}
			_regControlsPanel.AutoScroll = true;
			if (registrationLimit.PrivacyPolicyAddress != null)
			{
				_privacyPolicyLink.Url = Toolbox.ResolveUrl(registrationLimit.PrivacyPolicyAddress, base._superForm.Context).ToString();
				_privacyPolicyLink.Image = Images.WebLink_png;
			}
			else
			{
				_privacyPolicyLink.Visible = false;
			}
			_activateOption.Visible = registrationLimit.ShowActivationOption;
			LicenseValuesDictionary registrationInfo = base._superForm.Context.RequestInfo.RegistrationInfo;
			_name = new TextBox();
			_name.Text = ((StringDictionary)registrationInfo)["name"];
			AddRegistrationField(_name, "name", "UI_Name", registrationLimit.RequiredFields.Contains("Name"));
			_organization = new TextBox();
			_organization.Text = ((StringDictionary)registrationInfo)["organization"];
			AddRegistrationField(_organization, "organization", "UI_Organization", registrationLimit.RequiredFields.Contains("Organization"));
			_serialNumber = new LicenseCodeTextBox();
			_serialNumber.Mask = registrationLimit.SerialNumberMask;
			_serialNumber.CharacterSet = registrationLimit.License.SerialNumberInfo.CharacterSet;
			_serialNumber.Font = base.SuperForm.FixedFont;
			_serialNumber.Text = registrationLimit.License.SerialNumber;
			if (registrationLimit.License.SerialNumber == null)
			{
				_serialNumber.Text = ((StringDictionary)registrationInfo)["serialnumber"];
			}
			_serialNumber.HideMaskOnLeave = true;
			AddRegistrationField(_serialNumber, "serialNumber", "UI_SerialNumber", registrationLimit.RequiredFields.Contains("SerialNumber"));
			foreach (CustomRegistrationField item in (IEnumerable)registrationLimit.CustomFields)
			{
				AddCustomField(item);
			}
		}

		protected internal override void InitializePanel()
		{
			RegistrationLimit registrationLimit = base.Limit as RegistrationLimit;
			if (registrationLimit.IsMandatory)
			{
				Button button = base.AddBottomButton("UI_Cancel", 30);
				button.Click += _cancel_Click;
				base._superForm.CancelButton = button;
				_register = base.AddBottomButton("UI_Register");
				_register.Click += _register_Click;
				base._superForm.AcceptButton = _register;
			}
			else if (registrationLimit.License.IsTrial && base._superForm.Context.Items["ManualRegistration"] == null)
			{
				Button button = base.AddBottomButton("UI_Try", 30);
				button.Click += _registerLater_Click;
				base._superForm.CancelButton = button;
				_register = base.AddBottomButton("UI_Register");
				_register.Click += _register_Click;
				base._superForm.AcceptButton = _register;
			}
			else
			{
				bool flag = false;
				foreach (SecureLicense item in (IEnumerable)registrationLimit.License.LicenseFile.Licenses)
				{
					if (!item.IsStillEncrypted && item.CanUnlockBySerial && item != registrationLimit.License)
					{
						flag = true;
					}
				}
				if (flag)
				{
					Button button = base.AddBottomButton("UI_Cancel", 30);
					button.Click += _cancel_Click;
					base._superForm.CancelButton = button;
				}
				_register = base.AddBottomButton("UI_Register", (!flag) ? 30 : 0);
				_register.Click += _register_Click;
				base._superForm.AcceptButton = _register;
				if (!flag)
				{
					Button button = base.AddBottomButton("UI_RegisterLater");
					button.Click += _registerLater_Click;
					base._superForm.CancelButton = button;
					button = base.AddBottomButton("UI_DontRegister");
					button.Click += _dontRegister_Click;
				}
			}
			if (registrationLimit.PurchaseUrl != null)
			{
				Button button = base.AddBottomButton("UI_BuyNow");
				button.Click += _buyNow_Click;
			}
			base.AddLicenseAgreementLink();
		}

		protected virtual void HandlePrivacyPolicy()
		{
			base.ShowPage("PRIVACYPOLICY", null);
		}

		protected virtual void HandleDontRegister()
		{
			if (((RegistrationLimit)base.Limit).DontRegister(base._superForm.Context))
			{
				base.Return(FormResult.Incomplete);
			}
		}

		protected virtual void HandleRegisterLater()
		{
			if (((RegistrationLimit)base.Limit).RegisterLater(base._superForm.Context))
			{
				base.Return(FormResult.Incomplete);
			}
		}

		protected virtual void HandleRegister()
		{
			if (ValidateForm())
			{
				LicenseValuesDictionary registrationInfo = GetRegistrationInfo();
				RegistrationLimit registrationLimit = base.Limit as RegistrationLimit;
				if (registrationLimit.FindTargetLicense(base.SuperForm.Context, _serialNumber.Text) == null)
				{
					base.ShowMessageBox("E_InvalidSerialOrFailure", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
				else if (!CheckUpgradeSerial(registrationInfo))
				{
					if (((RegistrationLimit)base.Limit).Servers.Count == 0)
					{
						_registerRequest = registrationLimit.RegisterAsync(base._superForm.Context, true, registrationInfo, HandleRegisterComplete);
						_register.Enabled = false;
						Cursor = Cursors.WaitCursor;
					}
					else
					{
						base.ShowPanel(new RegisterOnlinePanel(registrationLimit, registrationInfo), HandleRegisterOnline);
					}
				}
			}
		}

		private void HandleRegisterOnline(FormResult result)
		{
			switch (result)
			{
			case FormResult.Incomplete:
				base.ShowMessageBox("E_InvalidSerialOrFailure", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				break;
			case FormResult.Success:
			case FormResult.Failure:
			case FormResult.Retry:
				base.Return(result);
				break;
			}
			base.Invoke(new MethodInvoker(CompleteRegistration));
		}

		private void HandleUpgrade(FormResult result)
		{
			switch (result)
			{
			case FormResult.Retry:
				base.Return(FormResult.Retry);
				break;
			case FormResult.Success:
				HandleRegister();
				break;
			}
		}

		private void HandleRegisterComplete(object sender, EventArgs e)
		{
			switch (_registerRequest.GetResult())
			{
			case ValidationResult.Canceled:
				base.Invoke(new MethodInvoker(CompleteRegistration), null);
				base.ShowMessageBox("E_InvalidSerialOrFailure", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				break;
			case ValidationResult.Retry:
				base.Return(FormResult.Retry);
				break;
			default:
			{
				LicenseEventArgs licenseEventArgs = new LicenseEventArgs(base.Limit.License, base._superForm.Context, base._superForm.Context.LatestValidationRecord);
				SecureLicenseManager.OnErrorOccurred(licenseEventArgs);
				if (!licenseEventArgs.Handled)
				{
					base.ShowPanel(new ErrorReportPanel(null, null, SR.GetString("E_CouldNotRegister"), new ValidationRecordCollection(base._superForm.Context.LatestValidationRecord)), null);
				}
				else
				{
					base.Invoke(new MethodInvoker(CompleteRegistration));
				}
				break;
			}
			case ValidationResult.Valid:
				base.Return(FormResult.Success);
				break;
			}
		}

		private void CompleteRegistration()
		{
			_register.Enabled = true;
			Cursor = Cursors.Default;
		}

		protected virtual LicenseValuesDictionary GetRegistrationInfo()
		{
			LicenseValuesDictionary registrationInfo = base._superForm.Context.RequestInfo.RegistrationInfo;
			((StringDictionary)registrationInfo)["name"] = ResolveStringValue(_name.Text);
			((StringDictionary)registrationInfo)["organization"] = ResolveStringValue(_organization.Text);
			((StringDictionary)registrationInfo)["serialNumber"] = ResolveStringValue(_serialNumber.Text);
			foreach (Control control in _regControlsPanel.Controls)
			{
				CustomRegistrationField customRegistrationField = control.Tag as CustomRegistrationField;
				if (customRegistrationField != null)
				{
					switch (customRegistrationField.FieldType)
					{
					case CustomFieldType.Custom:
						((ICustomFormControl)control).AddValues(customRegistrationField.Name, registrationInfo);
						break;
					case CustomFieldType.Checkbox:
						((StringDictionary)registrationInfo)[customRegistrationField.Name] = ((CheckBox)control).Checked.ToString();
						break;
					case CustomFieldType.Text:
					case CustomFieldType.MultiLine:
					case CustomFieldType.Password:
						((StringDictionary)registrationInfo)[customRegistrationField.Name] = ResolveStringValue(((TextBox)control).Text);
						break;
					}
				}
			}
			RegistrationLimit registrationLimit = base.Limit as RegistrationLimit;
			if (registrationLimit.ShowActivationOption && _activateOption.Checked)
			{
				((StringDictionary)registrationInfo)["_autoactivate"] = "true";
			}
			return registrationInfo;
		}

		private string ResolveStringValue(string text)
		{
			if (text == null)
			{
				return null;
			}
			text = text.Trim();
			if (text.Length == 0)
			{
				return null;
			}
			return text;
		}

		protected virtual bool CheckUpgradeSerial(LicenseValuesDictionary reginfo)
		{
			SecureLicense secureLicense = ((RegistrationLimit)base.Limit).FindTargetLicense(base._superForm.Context, _serialNumber.Text);
			if (secureLicense != null)
			{
				Limit[] array = secureLicense.Limits.FindLimitsByType(typeof(UpgradeLimit), true);
				if (array != null && array.Length > 0)
				{
					string text = ((StringDictionary)reginfo)["upgradeSerialNumber"];
					if (text == null)
					{
						Limit[] array2 = array;
						for (int i = 0; i < array2.Length; i++)
						{
							UpgradeLimit upgradeLimit = (UpgradeLimit)array2[i];
							text = upgradeLimit.GetUpgradeSerialFromContext(base._superForm.Context);
							if (text != null && text.Length > 0)
							{
								break;
							}
						}
					}
					base._superForm.Context.Items["TargetLicense"] = secureLicense;
					if (text != null && text.Length != 0)
					{
						((StringDictionary)reginfo)["upgradeSerialNumber"] = text;
						goto IL_00ee;
					}
					base.ShowPage("UPGRADE", HandleUpgrade);
					return true;
				}
			}
			goto IL_00ee;
			IL_00ee:
			return false;
		}

		private void _cancel_Click(object sender, EventArgs e)
		{
			base.Return(FormResult.Failure);
		}

		private void _buyNow_Click(object sender, EventArgs e)
		{
			HandleBuyNow();
		}

		private void _regControlsPanel_Scroll(object sender, ScrollEventArgs e)
		{
			_regControlsPanel.Invalidate(false);
		}

		private void _privacyPolicyLink_LinkClicked(object sender, EventArgs e)
		{
			HandlePrivacyPolicy();
		}

		private void _dontRegister_Click(object sender, EventArgs e)
		{
			HandleDontRegister();
		}

		private void _registerLater_Click(object sender, EventArgs e)
		{
			HandleRegisterLater();
		}

		private void _register_Click(object sender, EventArgs e)
		{
			HandleRegister();
		}

		protected internal override void PanelShown(bool safeToChange)
		{
			base.PanelShown(safeToChange);
			base.SelectNextControl(_regControlsPanel, true, true, true, true);
			if (safeToChange)
			{
				PopulatePotentialSerialNumber();
			}
		}

		private void PopulatePotentialSerialNumber()
		{
			if (_serialNumber != null && _serialNumber.Text.Length == 0)
			{
				string potentialSerialFromClipboard = base.SuperForm.Context.GetPotentialSerialFromClipboard();
				if (potentialSerialFromClipboard != null)
				{
					try
					{
						_serialNumber.Text = potentialSerialFromClipboard;
					}
					catch (ArgumentOutOfRangeException)
					{
					}
				}
			}
		}

		protected internal override void FormActivated()
		{
			base.FormActivated();
			PopulatePotentialSerialNumber();
		}

		protected void AddRegistrationField(Control ctrl, string field, string displayName, bool required)
		{
			if (displayName != null)
			{
				ThemeLabel themeLabel = new ThemeLabel();
				themeLabel.Text = SR.GetString(displayName);
				themeLabel.AutoSize = true;
				themeLabel.Location = new Point(0, _nextFieldTop);
				themeLabel.BackColor = Color.Transparent;
				themeLabel.Font = base._superForm.BoldFont;
				ThemeLabel themeLabel2 = themeLabel;
				_regControlsPanel.Controls.Add(themeLabel2);
				_nextFieldTop += themeLabel2.Height + 3;
				if (required)
				{
					BufferedPictureBox bufferedPictureBox = new BufferedPictureBox();
					bufferedPictureBox.Image = Images.Required_png;
					bufferedPictureBox.SetBounds(themeLabel2.Right + 2, (themeLabel2.Height - 8) / 2 + themeLabel2.Top, 8, 8);
					bufferedPictureBox.BackColor = Color.Transparent;
					_regControlsPanel.Controls.Add(bufferedPictureBox);
				}
			}
			ctrl.SetBounds(0, _nextFieldTop, _regControlsPanel.Width, ctrl.Height);
			ctrl.Anchor = (AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right);
			ctrl.Name = field;
			if (ctrl.Font == Font)
			{
				ctrl.Font = base.SuperForm.FieldFont;
			}
			_regControlsPanel.Controls.Add(ctrl);
			_nextFieldTop += ctrl.Height + 2;
			_registrationPanel.Height = Math.Min(_nextFieldTop + 32, 235);
			_activateOption.Top = _registrationPanel.Bottom + 4;
		}

		protected void AddCustomField(CustomRegistrationField field)
		{
			Control control = null;
			switch (field.FieldType)
			{
			case CustomFieldType.Custom:
			{
				Type type = (field.CustomTypeName == null || field.CustomTypeName.Length == 0) ? null : TypeHelper.FindType(field.CustomTypeName, false);
				if (type == null)
				{
					throw new SecureLicenseException("E_CannotCreateCustomControl", field.Name);
				}
				ICustomFormControl customFormControl = Activator.CreateInstance(type) as ICustomFormControl;
				if (customFormControl == null)
				{
					throw new SecureLicenseException("E_CannotCreateCustomControl", field.Name);
				}
				control = (customFormControl as Control);
				if (control == null)
				{
					throw new SecureLicenseException("E_CannotCreateCustomControl", field.Name);
				}
				control.Font = base._superForm.MediumFont;
				customFormControl.InitializeFields(field.Name, base._superForm.Context.RequestInfo.RegistrationInfo);
				break;
			}
			case CustomFieldType.Text:
			{
				TextBox textBox3 = new TextBox();
				textBox3.Font = base._superForm.FieldFont;
				control = textBox3;
				control.Text = ((StringDictionary)base._superForm.Context.RequestInfo.RegistrationInfo)[field.Name];
				break;
			}
			case CustomFieldType.MultiLine:
			{
				TextBox textBox2 = new TextBox();
				textBox2.Font = base._superForm.FieldFont;
				textBox2.Multiline = true;
				textBox2.Height = textBox2.Font.Height * 4;
				textBox2.AcceptsReturn = true;
				textBox2.ScrollBars = ScrollBars.Vertical;
				control = textBox2;
				control.Text = ((StringDictionary)base._superForm.Context.RequestInfo.RegistrationInfo)[field.Name];
				break;
			}
			case CustomFieldType.Checkbox:
			{
				CheckBox checkBox = new CheckBox();
				checkBox.Text = field.DisplayName;
				checkBox.Font = base._superForm.MediumFont;
				checkBox.BackColor = Color.Transparent;
				checkBox.Tag = field;
				checkBox.Checked = (((StringDictionary)base._superForm.Context.RequestInfo.RegistrationInfo)[field.Name] == "true");
				AddRegistrationField(checkBox, field.Name, null, ((RegistrationLimit)base.Limit).RequiredFields.Contains(field.Name));
				return;
			}
			case CustomFieldType.Header:
			{
				ShadowLabel shadowLabel = new ShadowLabel();
				shadowLabel.Text = field.DisplayName;
				shadowLabel.Font = base._superForm.HeaderFont;
				shadowLabel.Height = 24;
				shadowLabel.TextAlign = ContentAlignment.BottomLeft;
				AddRegistrationField(shadowLabel, field.Name, null, false);
				return;
			}
			case CustomFieldType.Password:
			{
				TextBox textBox = new TextBox();
				textBox.PasswordChar = '●';
				textBox.Font = base._superForm.FieldFont;
				control = textBox;
				control.Text = ((StringDictionary)base._superForm.Context.RequestInfo.RegistrationInfo)[field.Name];
				break;
			}
			}
			control.Tag = field;
			AddRegistrationField(control, field.Name, field.DisplayName, ((RegistrationLimit)base.Limit).RequiredFields.Contains(field.Name));
		}

		protected internal override void UpdateFromTheme()
		{
			base.UpdateFromTheme();
			_registerTitle.Font = base.SuperForm.TitleFont;
		}

		protected virtual bool ValidateForm()
		{
			RegistrationLimit registrationLimit = base.Limit as RegistrationLimit;
			if (registrationLimit.RequiredFields.Contains("name") && _name.Text.Trim().Length == 0)
			{
				base.ShowMessageBox(SR.GetString("UI_FieldRequiredNotice", SR.GetString("UI_Name")), null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				_name.Select();
				return false;
			}
			if (registrationLimit.RequiredFields.Contains("organization") && _organization.Text.Trim().Length == 0)
			{
				base.ShowMessageBox(SR.GetString("UI_FieldRequiredNotice", SR.GetString("UI_Organization")), null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				_organization.Select();
				return false;
			}
			if (registrationLimit.RequiredFields.Contains("serialNumber") && _serialNumber.Text.Trim().Length == 0)
			{
				base.ShowMessageBox(SR.GetString("UI_FieldRequiredNotice", SR.GetString("UI_SerialNumber")), null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				_serialNumber.Select();
				return false;
			}
			foreach (Control control in _regControlsPanel.Controls)
			{
				CustomRegistrationField customRegistrationField = control.Tag as CustomRegistrationField;
				if (customRegistrationField != null && registrationLimit.RequiredFields.Contains(customRegistrationField.Name))
				{
					bool flag = true;
					switch (customRegistrationField.FieldType)
					{
					case CustomFieldType.Custom:
					{
						ICustomFormControl customFormControl = control as ICustomFormControl;
						flag = customFormControl.ValidateField(customRegistrationField.Name);
						break;
					}
					case CustomFieldType.Checkbox:
						flag = ((CheckBox)control).Checked;
						break;
					case CustomFieldType.Text:
					case CustomFieldType.MultiLine:
					case CustomFieldType.Password:
						flag = (((TextBox)control).Text.Trim().Length > 0);
						break;
					}
					if (flag)
					{
						continue;
					}
					base.ShowMessageBox(SR.GetString("UI_FieldRequiredNotice", SR.GetString(customRegistrationField.DisplayName)), null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
					control.Select();
					return false;
				}
			}
			return true;
		}
	}
}
