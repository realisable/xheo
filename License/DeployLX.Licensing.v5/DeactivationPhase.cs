namespace DeployLX.Licensing.v5
{
	public enum DeactivationPhase
	{
		CheckPermission,
		Commit
	}
}
