using System;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public abstract class DesigntimeOrRuntimeLimit : Limit
	{
		private StringCollection _hostNames;

		private bool _dotNetCommandIsHost = true;

		public StringCollection HostNames
		{
			get
			{
				return _hostNames;
			}
		}

		public bool DotNetCommandIsHost
		{
			get
			{
				return _dotNetCommandIsHost;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_dotNetCommandIsHost != value)
				{
					_dotNetCommandIsHost = value;
					base.OnChanged("DotNetCommandIsHost");
				}
			}
		}

		protected DesigntimeOrRuntimeLimit()
		{
			_hostNames = new StringCollection();
			_hostNames.Changed += _hostNames_Changed;
		}

		private void _hostNames_Changed(object sender, CollectionEventArgs e)
		{
			base.OnCollectionChanged(sender, "HostNames", e);
		}

		protected bool IsHostedInDesigner(SecureLicenseContext context)
		{
			if (context.UsageMode == LicenseUsageMode.Designtime)
			{
				return true;
			}
			if (_hostNames.Count > 0)
			{
				Assembly entryAssembly = Assembly.GetEntryAssembly();
				if (entryAssembly != null)
				{
					string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(entryAssembly.Location);
					if (fileNameWithoutExtension != null)
					{
						if (_dotNetCommandIsHost)
						{
							if (string.Compare(fileNameWithoutExtension, "csc", true) == 0)
							{
								return true;
							}
							if (string.Compare(fileNameWithoutExtension, "al", true) == 0)
							{
								return true;
							}
							if (string.Compare(fileNameWithoutExtension, "vbc", true) == 0)
							{
								return true;
							}
							if (string.Compare(fileNameWithoutExtension, "lc", true) == 0)
							{
								return true;
							}
							if (string.Compare(fileNameWithoutExtension, "ilasm", true) == 0)
							{
								return true;
							}
							if (string.Compare(fileNameWithoutExtension, "jsc", true) == 0)
							{
								return true;
							}
						}
						foreach (string item in (IEnumerable)_hostNames)
						{
							if (string.Compare(item, fileNameWithoutExtension, true) == 0)
							{
								return true;
							}
							if (item.Length > 4 && string.Compare(item, item.Length - 4, ".exe", 0, 4, true) == 0 && string.Compare(item, 0, fileNameWithoutExtension, 0, fileNameWithoutExtension.Length, true) == 0)
							{
								return true;
							}
						}
					}
				}
			}
			return false;
		}

		protected override bool ReadFromXml(XmlReader reader)
		{
			_dotNetCommandIsHost = (reader.GetAttribute("dotNetCommandIsHost") != "false");
			_hostNames.Clear();
			if (reader.IsEmptyElement)
			{
				reader.Read();
				return true;
			}
			reader.Read();
			while (!reader.EOF)
			{
				if (reader.IsStartElement())
				{
					switch (reader.Name)
					{
					case "Limit":
						if (base.ReadChildLimit(reader))
						{
							break;
						}
						return false;
					case "Host":
						if (_hostNames.ReadFromXml(reader, null, "Host", "name", false))
						{
							break;
						}
						return false;
					default:
						reader.Skip();
						break;
					}
					continue;
				}
				reader.Read();
				break;
			}
			return true;
		}

		protected override bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			if (!_dotNetCommandIsHost)
			{
				writer.WriteAttributeString("dotNetCommandIsHost", "false");
			}
			_hostNames.WriteToXml(writer, null, "Host", "name");
			return true;
		}

		protected internal override void MakeReadOnly()
		{
			base.MakeReadOnly();
			_hostNames.MakeReadOnly();
		}
	}
}
