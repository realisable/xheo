using System;
using System.Collections;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public sealed class SecureLicenseCollection : WithEventsCollection
	{
		internal LicenseFile _licenseFile;

		public LicenseFile LicenseFile
		{
			get
			{
				return _licenseFile;
			}
		}

		public SecureLicense this[int index]
		{
			get
			{
				return base.List[index] as SecureLicense;
			}
			set
			{
				base.List[index] = value;
			}
		}

		public SecureLicense this[string licenseId]
		{
			get
			{
				int num = IndexOf(licenseId);
				if (num == -1)
				{
					return null;
				}
				return this[num];
			}
			set
			{
				if (value.LicenseId != licenseId)
				{
					throw new SecureLicenseException("E_InvalidIdForLicense");
				}
				int num = IndexOf(licenseId);
				if (num == -1)
				{
					Add(value);
				}
				else
				{
					this[num] = value;
				}
			}
		}

		public SecureLicenseCollection()
		{
		}

		internal SecureLicenseCollection(LicenseFile licenseFile)
		{
			_licenseFile = licenseFile;
		}

		protected override void OnAdded(CollectionEventArgs e)
		{
			base.OnAdded(e);
			SecureLicense secureLicense = e.Element as SecureLicense;
			if (secureLicense != null)
			{
				secureLicense._licenseFile = _licenseFile;
			}
		}

		public int Add(SecureLicense license)
		{
			return base.List.Add(license);
		}

		public void AddRange(SecureLicense[] licenses)
		{
			if (licenses != null)
			{
				foreach (SecureLicense license in licenses)
				{
					Add(license);
				}
			}
		}

		public void AddRange(SecureLicenseCollection licenses)
		{
			if (licenses != null)
			{
				foreach (SecureLicense item in (IEnumerable)licenses)
				{
					Add(item);
				}
			}
		}

		public void Insert(int index, SecureLicense license)
		{
			base.List.Insert(index, license);
		}

		public void Remove(SecureLicense license)
		{
			base.List.Remove(license);
		}

		public void Remove(string licenseId)
		{
			int num = IndexOf(licenseId);
			if (num != -1)
			{
				RemoveAt(num);
			}
		}

		public void CopyTo(SecureLicense[] array, int index)
		{
			base.List.CopyTo(array, index);
		}

		public int IndexOf(SecureLicense license)
		{
			return base.List.IndexOf(license);
		}

		public int IndexOf(string licenseId)
		{
			int num = 0;
			while (true)
			{
				if (num < Count)
				{
					if (this[num].LicenseId == licenseId)
					{
						break;
					}
					num++;
					continue;
				}
				return -1;
			}
			return num;
		}

		public bool Contains(SecureLicense license)
		{
			return base.List.Contains(license);
		}

		public SecureLicense[] ToArray()
		{
			if (Count == 0)
			{
				return null;
			}
			SecureLicense[] array = new SecureLicense[Count];
			CopyTo(array, 0);
			return array;
		}
	}
}
