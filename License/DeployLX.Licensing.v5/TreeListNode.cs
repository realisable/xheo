using System.Drawing;

namespace DeployLX.Licensing.v5
{
	internal sealed class TreeListNode
	{
		internal Rectangle _virtualBounds = Rectangle.Empty;

		private string _text;

		private TreeListNodeCollection _nodes;

		private bool _isExpanded;

		internal TreeList _treeList;

		internal TreeListNode _parentNode;

		private Bitmap _image;

		public string Text
		{
			get
			{
				return _text;
			}
			set
			{
				_text = value;
			}
		}

		public TreeListNodeCollection Nodes
		{
			get
			{
				return _nodes;
			}
		}

		public bool IsExpanded
		{
			get
			{
				return _isExpanded;
			}
			set
			{
				_isExpanded = value;
			}
		}

		public TreeList TreeList
		{
			get
			{
				if (_treeList == null && _parentNode != null)
				{
					_treeList = _parentNode.TreeList;
				}
				return _treeList;
			}
		}

		public TreeListNode ParentNode
		{
			get
			{
				return _parentNode;
			}
		}

		public Bitmap Image
		{
			get
			{
				return _image;
			}
			set
			{
				_image = value;
			}
		}

		public TreeListNode()
		{
			_nodes = new TreeListNodeCollection();
			_nodes.Changed += _nodes_Changed;
		}

		public TreeListNode(string text)
			: this()
		{
			_text = text;
		}

		private void _nodes_Changed(object sender, CollectionEventArgs e)
		{
			TreeListNode treeListNode = e.Element as TreeListNode;
			if (treeListNode != null)
			{
				treeListNode._parentNode = this;
				treeListNode._treeList = TreeList;
			}
		}

		public SizeF MeasureItem(Graphics g, float maxWidth)
		{
			if (TreeList == null)
			{
				return SizeF.Empty;
			}
			if (_image != null)
			{
				maxWidth -= (float)(_image.Width + 2);
			}
			using (StringFormat stringFormat = new StringFormat(StringFormat.GenericTypographic))
			{
				stringFormat.FormatFlags &= ~StringFormatFlags.NoWrap;
				SizeF result = g.MeasureString((_text == null) ? "" : _text, TreeList.Font, new SizeF(maxWidth, 3.40282347E+38f), stringFormat);
				if (_image != null && result.Height < (float)_image.Height)
				{
					result.Height = (float)_image.Height;
				}
				return result;
			}
		}

		public SizeF PaintItem(Graphics g, RectangleF bounds)
		{
			if (TreeList == null)
			{
				return SizeF.Empty;
			}
			string text = _text;
			if (text == null)
			{
				text = "";
			}
			SizeF result = MeasureItem(g, bounds.Width);
			int num = 0;
			if (_image != null)
			{
				g.DrawImage(_image, bounds.X + (float)num, bounds.Y - 1f);
				num += _image.Width + 2;
			}
			using (StringFormat stringFormat = new StringFormat(StringFormat.GenericTypographic))
			{
				stringFormat.FormatFlags &= ~StringFormatFlags.NoWrap;
				g.DrawString(text, TreeList.Font, SystemBrushes.WindowText, new RectangleF(bounds.X + (float)num, bounds.Y, result.Width, 2.14748365E+09f), stringFormat);
				return result;
			}
		}
	}
}
