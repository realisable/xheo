using System;
using System.Reflection;
using System.Security.Cryptography;

namespace DeployLX.Licensing.v5
{
	internal class FipsRijndael : Rijndael
	{
		private static readonly RNGCryptoServiceProvider _rng = new RNGCryptoServiceProvider();

		public override ICryptoTransform CreateEncryptor(byte[] rgbKey, byte[] rgbIV)
		{
			return NewEncryptor(rgbKey, base.ModeValue, rgbIV, base.FeedbackSizeValue, 0);
		}

		public override ICryptoTransform CreateDecryptor(byte[] rgbKey, byte[] rgbIV)
		{
			return NewEncryptor(rgbKey, base.ModeValue, rgbIV, base.FeedbackSizeValue, 1);
		}

		public override void GenerateKey()
		{
			base.KeyValue = new byte[base.KeySizeValue / 8];
			_rng.GetBytes(base.KeyValue);
		}

		public override void GenerateIV()
		{
			base.IVValue = new byte[base.BlockSizeValue / 8];
			_rng.GetBytes(base.IVValue);
		}

		private ICryptoTransform NewEncryptor(byte[] rgbKey, CipherMode mode, byte[] rgbIV, int feedbackSize, int encryptMode)
		{
			if (rgbKey == null)
			{
				rgbKey = new byte[base.KeySizeValue / 8];
				_rng.GetBytes(rgbKey);
			}
			if (mode != CipherMode.ECB && rgbIV == null)
			{
				rgbIV = new byte[base.BlockSizeValue / 8];
				_rng.GetBytes(rgbIV);
			}
			Type typeFromHandle = typeof(RijndaelManagedTransform);
			ConstructorInfo[] constructors = typeFromHandle.GetConstructors(BindingFlags.Instance | BindingFlags.NonPublic);
			ConstructorInfo[] array = constructors;
			int num = 0;
			ConstructorInfo constructorInfo;
			while (true)
			{
				if (num < array.Length)
				{
					constructorInfo = array[num];
					ParameterInfo[] parameters = constructorInfo.GetParameters();
					if (parameters != null && parameters.Length == 7 && parameters[0].ParameterType == typeof(byte[]) && parameters[1].ParameterType == typeof(CipherMode) && parameters[2].ParameterType == typeof(byte[]) && parameters[3].ParameterType == typeof(int) && parameters[4].ParameterType == typeof(int) && parameters[5].ParameterType == typeof(PaddingMode) && !(parameters[6].ParameterType.Name != "RijndaelManagedTransformMode"))
					{
						break;
					}
					num++;
					continue;
				}
				throw new NotSupportedException();
			}
			return constructorInfo.Invoke(new object[7]
			{
				rgbKey,
				mode,
				rgbIV,
				base.BlockSizeValue,
				feedbackSize,
				base.PaddingValue,
				encryptMode
			}) as ICryptoTransform;
		}
	}
}
