using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.IO.IsolatedStorage;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Web.UI;
//using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{

    public sealed class SecureLicenseManager : LicenseProvider
    {
        private struct NotificationEntry
        {
            public SecureLicenseContext Context;

            public Delegate Target;

            public object[] Args;

            public NotificationEntry(SecureLicenseContext context, Delegate target, params object[] args)
            {
                Context = context;
                Target = target;
                Args = args;
            }
        }

        private delegate SecureLicense ValidationMethod(SecureLicenseContext context, object[] args);

        private class ValidateStaHelper : StaHelper
        {
            private object _instance;

            private Type _licenseType;

            private LicenseValidationRequestInfo _info;

            private LicenseContext _licenseContext;

            private StackTrace _stackTrace;

            private object[] _args;

            private ValidationMethod _method;

            private Exception _exception;

            private SecureLicense _license;

            public Exception Exception
            {
                get
                {
                    return _exception;
                }
            }

            public SecureLicense License
            {
                get
                {
                    return _license;
                }
            }

            public ValidateStaHelper(object instance, Type type, LicenseValidationRequestInfo requestInfo, LicenseContext licenseContext, StackTrace stackTrace, ValidationMethod method, object[] args)
            {
                _instance = instance;
                _licenseType = type;
                _info = requestInfo;
                _licenseContext = licenseContext;
                _stackTrace = stackTrace;
                _args = args;
                _method = method;
                base.DontRetryWorkOnFailed = true;
            }

            protected override void Work()
            {
                try
                {
                    _license = ValidateExecutor(_instance, _licenseType, _info, _licenseContext, _stackTrace, _method, _args);
                }
                catch (Exception exception)
                {
                    Exception ex = _exception = exception;
                }
            }
        }

        private delegate void ResetCacheForTypeDelegate(Type type);

        private delegate void ResetCacheForLicenseDelegate(SecureLicense license);

        private static Hashtable _cachedLicenses;

        private static AutoResetEvent _sharedEvent;

        private static Dictionary<string, List<SecureLicense>> _suggestedLicenses;

        private static Queue _delayedNotifications;

        public static readonly object SyncRoot;

        public static readonly object AsyncLock;

        private static bool _shouldPerformFinalCleanup;

        private static Hashtable _savedLicenses;

        public static bool IsListeningForDelayed
        {
            get
            {
                if (SecureLicenseManager.DelayedValidationFailed == null)
                {
                    return false;
                }
                Delegate[] invocationList = SecureLicenseManager.DelayedValidationFailed.GetInvocationList();
                int num = 0;
                bool result;
                while (true)
                {
                    if (num < invocationList.Length)
                    {
                        Delegate @delegate = invocationList[num];
                        if (@delegate.Target == null)
                        {
                            if (@delegate.Method == null)
                            {
                                goto IL_0055;
                            }
                            result = true;
                            break;
                        }
                        object target = @delegate.Target;
                        WeakReference weakReference = target as WeakReference;
                        if (weakReference != null)
                        {
                            if (!weakReference.IsAlive)
                            {
                                goto IL_0055;
                            }
                            target = weakReference.Target;
                        }
                        if (target == null)
                        {
                            goto IL_0055;
                        }
                        return true;
                    }
                    return false;
                    IL_0055:
                    num++;
                }
                return result;
            }
        }

        public static SecureLicenseContext CurrentContext
        {
            get
            {
                return SecureLicenseContext._current;
            }
        }

        public static bool ShouldPerformFinalCleanup
        {
            get
            {
                return _shouldPerformFinalCleanup;
            }
        }

        public static event LicenseEventHandler DelayedValidationFailed;

        public static event LicenseEventHandler DelayedValidationComplete;

        public static event LicenseEventHandler ErrorOccurred;

        public static event ValidationHelpEventHandler HelpRequested;

        public static void OnDelayedValidationFailed(SecureLicenseContext context, LicenseEventArgs e)
        {
            if (SecureLicenseManager.DelayedValidationFailed != null)
            {
                if (context != null)
                {
                    EnqueueNotification(new NotificationEntry(context, SecureLicenseManager.DelayedValidationFailed, null, e));
                }
                else
                {
                    SecureLicenseManager.DelayedValidationFailed(null, e);
                }
            }
        }

        private static void EnqueueNotification(NotificationEntry e)
        {
            EnsureDelayNotificationPump();
            if (_delayedNotifications.Count > 1024)
            {
                throw new SecureLicenseException("E_ValidationOverflow");
            }
            _delayedNotifications.Enqueue(e);
            _sharedEvent.Set();
        }

        private static void EnsureDelayNotificationPump()
        {
            if (_delayedNotifications == null)
            {
                lock (SyncRoot)
                {
                    if (_delayedNotifications == null)
                    {
                        _delayedNotifications = new Queue();
                        Thread thread = new Thread(DntPump);
                        thread.IsBackground = true;
                        thread.Start();
                    }
                }
            }
        }

        internal static void OnDelayedValidationComplete(SecureLicenseContext context, LicenseEventArgs e)
        {
            if (SecureLicenseManager.DelayedValidationComplete != null)
            {
                if (context != null)
                {
                    EnqueueNotification(new NotificationEntry(context, SecureLicenseManager.DelayedValidationComplete, null, e));
                }
                else
                {
                    SecureLicenseManager.DelayedValidationComplete(null, e);
                }
            }
        }

        internal static void OnErrorOccurred(LicenseEventArgs e)
        {
            if (SecureLicenseManager.ErrorOccurred != null)
            {
                SecureLicenseManager.ErrorOccurred(null, e);
            }
        }

        private static void DntPump()
        {
            while (true)
            {
                _sharedEvent.WaitOne(-1, false);
                while (_delayedNotifications.Count > 0)
                {
                    NotificationEntry notificationEntry = (NotificationEntry)_delayedNotifications.Dequeue();
                    if ((object)notificationEntry.Target != null && notificationEntry.Target.Target != null)
                    {
                        try
                        {
                            notificationEntry.Target.DynamicInvoke(notificationEntry.Args);
                        }
                        catch (TargetInvocationException)
                        {
                        }
                    }
                }
            }
        }

        internal static void OnHelpRequested(ValidationHelpEventArgs e)
        {
            if (SecureLicenseManager.HelpRequested != null)
            {
                SecureLicenseManager.HelpRequested(null, e);
            }
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public SecureLicenseManager()
        {
        }

        static SecureLicenseManager()
        {
            _cachedLicenses = new Hashtable();
            _sharedEvent = new AutoResetEvent(false);
            _suggestedLicenses = new Dictionary<string, List<SecureLicense>>();
            SyncRoot = new object();
            AsyncLock = new object();
            _savedLicenses = new Hashtable();
            AppDomain.CurrentDomain.ProcessExit += CurrentDomain_ProcessExit;
            AppDomain.CurrentDomain.DomainUnload += CurrentDomain_ProcessExit;
            //Application.ApplicationExit += CurrentDomain_ProcessExit;
            EnsureDelayNotificationPump();
        }

        private static void CurrentDomain_ProcessExit(object sender, EventArgs e)
        {
            _shouldPerformFinalCleanup = true;
            ResetCache();
        }


        public static SecureLicense Validate(object instance, Type type, LicenseValidationRequestInfo requestInfo)
        {
            return ValidateInternal(instance, type, requestInfo, null, new StackTrace(1, false));
        }


        private static SecureLicense ValidateInternal(object instance, Type type, LicenseValidationRequestInfo requestInfo, LicenseContext licenseContext, StackTrace stackTrace)
        {
            return ValidateExecutor(instance, type, requestInfo, licenseContext, stackTrace, ValidateMethodExecutor, null);
        }

        private static SecureLicense ValidateMethodExecutor(SecureLicenseContext context, object[] args)
        {
            SecureLicense secureLicense = null;
            ArrayList arrayList = new ArrayList();
            if (!context.RequestInfo.DisableCache)
            {
                secureLicense = GetCachedLicense(context);
            }
            if (secureLicense == null && !context.StopLooking)
            {
                if (context.RequestInfo.LicenseFile != null)
                {
                    secureLicense = GrantLicense(context, TryLicenseFile(context, context.RequestInfo.LicenseFile.Licenses, false, arrayList));
                    if (secureLicense == null && arrayList.Count > 0)
                    {
                        secureLicense = GrantLicense(context, TryLicenseFile(context, arrayList, true, null));
                    }
                }
                if (secureLicense == null && !context.StopLooking)
                {
                    if (context.RequestInfo.OnlyCheckManualLicenseFile)
                    {
                        context.ReportError("E_NoLicensesFound", null, null, ErrorSeverity.Low, SR.GetString("M_CurrentAssembly"));
                    }
                    else
                    {
                        if (secureLicense == null)
                        {
                            secureLicense = GrantLicense(context, GetSuggestedLicense(context, arrayList));
                        }
                        if (secureLicense == null)
                        {
                            secureLicense = GrantLicense(context, FindSavedLicense(context, arrayList));
                        }
                        if (secureLicense == null)
                        {
                            StringCollection stringCollection = FindPossibleLicenseFolders(context);
                            foreach (string item in (IEnumerable)stringCollection)
                            {
                                StringCollection stringCollection2 = null;
                                try
                                {
                                    stringCollection2 = FindLicenseFiles(item);
                                }
                                catch (Exception exception)
                                {
                                    context.ReportError("E_InvalidSearchPath", item, exception, ErrorSeverity.NotSet, item);
                                    continue;
                                }
                                if (stringCollection2.Count == 0)
                                {
                                    context.ReportError("E_NoLicensesFound", item, null, ErrorSeverity.NotSet, item);
                                    continue;
                                }
                                foreach (string item2 in (IEnumerable)stringCollection2)
                                {
                                    LicenseFile licenseFile = null;
                                    try
                                    {
                                        licenseFile = new LicenseFile(item2);
                                        secureLicense = GrantLicense(context, TryLicenseFile(context, licenseFile.Licenses, false, arrayList));
                                        if (secureLicense != null)
                                        {
                                            goto IL_02b3;
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        bool flag = false;
                                        bool flag2 = false;
                                        Exception innerException = ex.InnerException;
                                        while (true)
                                        {
                                            if (innerException == null)
                                            {
                                                break;
                                            }
                                            SecureLicenseException ex2 = innerException as SecureLicenseException;
                                            if (ex2 != null)
                                            {
                                                if (ex2.ErrorId == "E_InvalidLicenseFile")
                                                {
                                                    flag = true;
                                                }
                                                else if (ex2.ErrorId == "E_InvalidVersion")
                                                {
                                                    flag2 = true;
                                                }
                                            }
                                            innerException = innerException.InnerException;
                                        }
                                        if (flag2)
                                        {
                                            context.ReportError("E_OldVersion", item2, null, ErrorSeverity.Low, item2);
                                        }
                                        else if (flag)
                                        {
                                            context.ReportError("E_ErrorLoadingLicenseFile", item2, ex, ErrorSeverity.Low, item2);
                                        }
                                        else
                                        {
                                            context.ReportError("E_ErrorLoadingLicenseFile", item2, ex, ErrorSeverity.NotSet, item2);
                                        }
                                        if (licenseFile != null)
                                        {
                                            licenseFile.Dispose();
                                        }
                                    }
                                }
                                goto IL_02b3;
                                IL_02b3:
                                if (secureLicense != null)
                                {
                                    break;
                                }
                            }
                        }
                        if (secureLicense == null)
                        {
                            try
                            {
                                LicenseFile embeddedLicense = GetEmbeddedLicense(context.LicensedType);
                                if (embeddedLicense != null)
                                {
                                    secureLicense = GrantLicense(context, TryLicenseFile(context, embeddedLicense.Licenses, false, arrayList));
                                }
                                else
                                {
                                    context.ReportError("E_NoLicensesFound", null, null, ErrorSeverity.Low, SR.GetString("M_CurrentAssembly"));
                                }
                            }
                            catch (Exception exception2)
                            {
                                context.ReportError("E_NoLicensesFound", null, exception2, ErrorSeverity.High, SR.GetString("M_CurrentAssembly"));
                            }
                        }
                        if (secureLicense == null)
                        {
                            secureLicense = GrantLicense(context, TryLicenseFile(context, arrayList, true, null));
                        }
                        if (secureLicense != null)
                        {
                            return secureLicense;
                        }
                    }
                    return null;
                }
                return secureLicense;
            }
            return secureLicense;
        }

        private static SecureLicense ValidateExecutor(object instance, Type type, LicenseValidationRequestInfo requestInfo, LicenseContext licenseContext, StackTrace stackTrace, ValidationMethod method, object[] args)
        {
            if (instance == null && type == null)
            {
                throw new ArgumentNullException("instance");
            }
            if (type == null)
            {
                type = instance.GetType();
                if ((instance is Page || instance is System.Web.UI.Control) && Attribute.GetCustomAttribute(type.Assembly, typeof(GeneratedCodeAttribute)) != null)
                {
                    type = type.BaseType;
                }
            }
            if (Thread.CurrentThread.ApartmentState != 0 && !SafeToolbox.IsServiceRequest && !SafeToolbox.IsWebRequest)
            {
                ValidateStaHelper validateStaHelper = new ValidateStaHelper(instance, type, requestInfo, licenseContext, stackTrace, method, args);
                validateStaHelper.GoAndWait();
                if (validateStaHelper.Exception != null)
                {
                    throw validateStaHelper.Exception;
                }
                return validateStaHelper.License;
            }
            lock (SyncRoot)
            {
                SecureLicense secureLicense = null;
                new ArrayList();
                SecureLicenseContext current = null;
                SecureLicenseContext resolveContext = null;
                SecureLicenseContext secureLicenseContext = null;
                CultureInfo cultureInfo = null;
                try
                {
                    if (requestInfo != null)
                    {
                        requestInfo.MakeReadOnly();
                    }
                    cultureInfo = Toolbox.SelectWebCulture();
                    secureLicenseContext = ((licenseContext as SecureLicenseContext) ?? new SecureLicenseContext(licenseContext ?? LicenseManager.CurrentContext, type, instance, requestInfo, stackTrace));
                    current = SecureLicenseContext._current;
                    SecureLicenseContext._current = secureLicenseContext;
                    resolveContext = SecureLicenseContext.ResolveContext;
                    SecureLicenseContext.ResolveContext = secureLicenseContext;
                    secureLicenseContext.ShowHelpButton = (SecureLicenseManager.HelpRequested != null);
                    MessageBoxEx.DefaultSupportEmailAddress = secureLicenseContext.SupportInfo.Email;
                    secureLicenseContext.CheckForDeveloperMode();
                    secureLicense = method(secureLicenseContext, args);
                }
                catch (Exception exception)
                {
                    secureLicense = null;
                    if (secureLicenseContext == null)
                    {
                        throw;
                    }
                    SupportInfo supportInfo = secureLicenseContext.RequestInfo.SupportInfo;
                    secureLicenseContext.ReportError("E_UnexpectedValidate", null, exception, ErrorSeverity.High);
                }
                finally
                {
                    if (secureLicenseContext != null)
                    {
                        if (secureLicense == null)
                        {
                            if (secureLicenseContext.StopLooking)
                            {
                                secureLicenseContext.ReportError("E_StoppedLooking", null, null, ErrorSeverity.Low);
                            }
                            try
                            {
                                secureLicenseContext.ReportFailuresToServer();
                            }
                            catch
                            {
                            }
                            if (!secureLicenseContext.CanSkipFailureReport && secureLicenseContext.RequestInfo.ShowFinalErrorReport && secureLicenseContext.CanShowWindowsForm)
                            {
                                try
                                {
                                    secureLicenseContext.ShowErrors("#UI_LicenseNotFoundTitle", "#UI_LicenseNotFoundSubTitle", SR.GetString("E_NoLicense", secureLicenseContext.SupportInfo.Product, secureLicenseContext.SupportInfo.Company), secureLicenseContext.ValidationRecords);
                                }
                                catch
                                {
                                }
                            }
                        }
                        if (secureLicenseContext.RequestInfo.DeveloperMode && !secureLicenseContext.RequestInfo.DontShowDeveloperModeWarning)
                        {
                            if (SafeToolbox.IsWebRequest)
                            {
                                if (secureLicenseContext.GetWebPage() != null)
                                {
                                    secureLicenseContext.RenderHtml("<div style=\"background-color:yellow;color:black;padding: 5px; font: bold 10pt Tahoma\">" + SR.GetString("M_UsingDeveloperMode") + "</div>", "DLX_DeveloperMode", null, HtmlLocation.Top, false);
                                }
                            }
                            else if (Toolbox.IsConsoleRequest)
                            {
                                ConsoleColor foregroundColor = Console.ForegroundColor;
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.WriteLine(SR.GetString("M_UsingDeveloperMode"));
                                Console.ForegroundColor = foregroundColor;
                            }
                            else if (!SafeToolbox.IsServiceRequest && !Debugger.IsAttached)
                            {
                                MessageBoxEx.Show(SR.GetString("M_UsingDeveloperMode"), null);
                            }
                        }
                        try
                        {
                            if (requestInfo != null && requestInfo.Logger != null)
                            {
                                requestInfo.Logger(secureLicenseContext, Toolbox.MakeDiagnosticReportString(null, secureLicenseContext.ValidationRecords, secureLicenseContext));
                            }
                        }
                        catch
                        {
                        }
                        try
                        {
                            secureLicenseContext.Close();
                        }
                        catch
                        {
                        }
                    }
                    SecureLicenseContext._current = current;
                    SecureLicenseContext.ResolveContext = resolveContext;
                    if (cultureInfo != null)
                    {
                        try
                        {
                            Toolbox.SelectCulture(cultureInfo);
                        }
                        catch
                        {
                        }
                    }
                    if (secureLicense == null && secureLicenseContext != null && !secureLicenseContext._dontThrowOnFailure)
                    {
                        throw new NoLicenseException(secureLicenseContext);
                    }
                }
                return secureLicense;
            }
        }

        public static SecureLicense ShowForm(ISuperFormLimit limit, string pageId, object instance, Type licensedType, LicenseValidationRequestInfo info, SecureLicenseContext context)
        {
            if (limit == null)
            {
                throw new ArgumentNullException("limit");
            }
            return ValidateExecutor(instance, licensedType, info, context, new StackTrace(1, false), ShowFormExecutor, new object[2]
            {
                limit,
                pageId
            });
        }

        private static SecureLicense ShowFormExecutor(SecureLicenseContext context, object[] args)
        {
            ISuperFormLimit superFormLimit = args[0] as ISuperFormLimit;
            string text = args[1] as string;
            if (text == null)
            {
                text = "DEFAULT";
            }
            SecureLicense license = ((Limit)superFormLimit).License;
            foreach (SecureLicense item in (IEnumerable)license.LicenseFile.Licenses)
            {
                context.EnsureLicenseState(item);
            }
            context.RequestInfo.ShowFinalErrorReport = false;
            context._dontThrowOnFailure = true;
            context.SetValidatingLicense(license);
            if (context.RequestInfo.RegistrationInfo.Count == 0)
            {
                foreach (DictionaryEntry item2 in license.RegistrationInfo)
                {
                    if (!context.RequestInfo.RegistrationInfo.ContainsKey(item2.Key as string))
                    {
                        ((StringDictionary)context.RequestInfo.RegistrationInfo)[item2.Key as string] = (item2.Value as string);
                    }
                }
            }
            switch (context.ShowForm(superFormLimit, text))
            {
                case FormResult.Retry:
                    return DoRetryWithGrant(context);
                default:
                    return null;
                case FormResult.Success:
                    return GrantLicense(context, ((Limit)superFormLimit).License);
            }
        }

        public static bool Deactivate(SecureLicense license, object instance, Type type, LicenseValidationRequestInfo requestInfo, out string[] deactivationCodes)
        {
            List<string> list = new List<string>();
            license = ValidateExecutor(instance, type, requestInfo, null, new StackTrace(1, false), DeactivationExecutor, new object[2]
            {
                license,
                list
            });
            deactivationCodes = list.ToArray();
            return license != null;
        }

        public static bool Deactivate(SecureLicense license, object instance, Type type, LicenseValidationRequestInfo requestInfo)
        {
            List<string> list = new List<string>();
            license = ValidateExecutor(instance, type, requestInfo, null, new StackTrace(1, false), DeactivationExecutor, new object[2]
            {
                license,
                list
            });
            return license != null;
        }

        private static SecureLicense DeactivationExecutor(SecureLicenseContext context, object[] args)
        {
            context._dontThrowOnFailure = true;
            context.RequestInfo.ShowFinalErrorReport = false;
            SecureLicense secureLicense = args[0] as SecureLicense;
            List<string> list = args[1] as List<string>;
            Limit[] array = secureLicense.Limits.FindLimitsByType(typeof(ActivationLimit), true);
            if (array != null && array.Length != 0)
            {
                bool flag = false;
                Limit[] array2 = array;
                for (int i = 0; i < array2.Length; i++)
                {
                    ActivationLimit activationLimit = (ActivationLimit)array2[i];
                    flag |= (activationLimit.HasActivated && activationLimit.CanDeactivate);
                }
                if (!flag)
                {
                    return null;
                }
                context.SetValidatingLicense(secureLicense);
                if (context.CanShowWindowsForm)
                {
                    Limit[] array3 = array;
                    for (int j = 0; j < array3.Length; j++)
                    {
                        ActivationLimit formLimit = (ActivationLimit)array3[j];

                        FormResult formResult = context.ShowForm(formLimit, "DEACTIVATE");

                        if (formResult != FormResult.Success && formResult != FormResult.Closed)
                        {
                            return null;
                        }
                    }
                }
                else
                {
                    DeactivationPhase phase = DeactivationPhase.CheckPermission;
                    for (int k = 0; k < 2; k++)
                    {
                        Limit[] array4 = array;
                        for (int l = 0; l < array4.Length; l++)
                        {
                            ActivationLimit activationLimit2 = (ActivationLimit)array4[l];

                            if (activationLimit2.HasActivated && activationLimit2.CanDeactivate && activationLimit2.Deactivate(context, true, phase) != ValidationResult.Valid)
                            {
                                return null;
                            }
                        }
                        phase = DeactivationPhase.Commit;
                    }
                }

                secureLicense.ClearSerialNumbersAndActivationData(false);
                secureLicense.LicenseFile.Delete(true, true);

                ResetCacheForLicense(secureLicense);
                list.AddRange(context._deactivationCodes.Values);
                return secureLicense;
            }
            return null;
        }

        private static SecureLicense GrantLicense(SecureLicenseContext context, SecureLicense license)
        {
            string text = null;
            string text2 = null;
            if (license == null)
            {
                return null;
            }
            context.WriteDiagnostic("Validated license {0} from {1}", license, license.LicenseFile.Location);
            if (PrepareValidLicense(context, license) != ValidationResult.Valid)
            {
                return null;
            }
            text = ((context.Instance == null || context.Instance.GetType() == context.LicensedType) ? null : TypeHelper.GetNonVersionedAssemblyName(context.Instance.GetType().Assembly));
            text2 = ((license.Classes.Contains("*") || license.Classes.Count == 0) ? (text + TypeHelper.GetNonVersionedAssemblyName(context.LicensedType.Assembly) + ".*") : (text + context.LicensedType.FullName));
            if (!context.RequestInfo.DisableCache)
            {
                license._isCached = true;
                SecureLicense secureLicense = _cachedLicenses[text2] as SecureLicense;
                if (secureLicense != null && secureLicense != license && secureLicense.LicenseFile != license.LicenseFile)
                {
                    secureLicense.Dispose();
                }
                if (secureLicense != license)
                {
                    _cachedLicenses[text2] = license;
                    license.Limits.AddRef();
                }
            }
            if (context.UsageMode == LicenseUsageMode.Designtime)
            {
                context.SetSavedLicenseKey(context.LicensedType, license.ToXmlString());
            }
            license.Limits.AddRef();
            context.EnsureLicenseState(license.LicenseFile);
            return license;
        }

        private static ValidationResult PrepareValidLicense(SecureLicenseContext context, SecureLicense license)
        {
            context.SetValidatingLicense(license);
            try
            {
                ValidationRecord record = new ValidationRecord("E_CouldNotGrant", license, null, ErrorSeverity.High);
                if (!license.IsReadOnly)
                {
                    ((IChange)license).MakeReadOnly();
                }
                context.SetValidLicense(license);
                license.Limits.ResetValidation();
                if (license.CanUnlockBySerial && !license.TrySerialNumberQuick(context, license.SerialNumber))
                {
                    return ValidationResult.Invalid;
                }
                ValidationResult validationResult = license.Limits.Granted(context);
                if (validationResult != ValidationResult.Valid)
                {
                    context.ReportError(record);
                    return validationResult;
                }
                license.Flush();
                license.SaveOnValid(context, false);
                context.StartDelayedValidation();
                return ValidationResult.Valid;
            }
            finally
            {
                context.SetValidatingLicense(null);
            }
        }

        private static StringCollection FindPossibleLicenseFolders(SecureLicenseContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            object instance = context.Instance;
            Type licensedType = context.LicensedType;
            StringCollection stringCollection = new StringCollection();

            AddFolder(stringCollection, Config.SharedFolder);
            return stringCollection;

            if (context.RequestInfo.SearchPaths != null && context.RequestInfo.SearchPaths.Length > 0)
            {
                string[] searchPaths = context.RequestInfo.SearchPaths;
                foreach (string folder in searchPaths)
                {
                    AddFolder(stringCollection, folder);
                }
            }
            if (AppDomain.CurrentDomain.SetupInformation.LicenseFile != null)
            {
                AddFolder(stringCollection, Path.GetDirectoryName(AppDomain.CurrentDomain.SetupInformation.LicenseFile));
            }
            object obj = null;
            string text = null;
            string text2 = null;
            string text3 = null;
            try
            {
                object service = context.GetService(Type.GetType("System.Web.UI.Design.IWebApplication,System.Design"));
                if (service != null)
                {
                    object obj2 = service.GetType().InvokeMember("System.Web.UI.Design.IWebApplication.RootProjectItem", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetProperty, null, service, null);
                    if (obj2 != null)
                    {
                        string text4 = obj2.GetType().InvokeMember("System.Web.UI.Design.IProjectItem.get_PhysicalPath", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.InvokeMethod, null, obj2, null) as string;
                        AddFolder(stringCollection, text4);
                        AddFolder(stringCollection, Path.Combine(text4, "Bin"));
                    }
                }
                object obj3 = context.GetService(typeof(ITypeResolutionService));
                if (obj3 != null)
                {
                    if (obj3.GetType().Name.EndsWith("AggregateTypeResolutionService"))
                    {
                        obj3 = obj3.GetType().InvokeMember("_baseResolver", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetField, null, obj3, new object[0]);
                    }
                    try
                    {
                        obj = obj3.GetType().InvokeMember("Project", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetProperty, null, obj3, new object[0]);
                    }
                    catch
                    {
                    }
                    if (obj != null)
                    {
                        text = (obj.GetType().InvokeMember("FullName", BindingFlags.Instance | BindingFlags.Public | BindingFlags.GetProperty, null, obj, new object[0]) as string);
                        object obj5 = obj.GetType().InvokeMember("ConfigurationManager", BindingFlags.Instance | BindingFlags.Public | BindingFlags.GetProperty, null, obj, new object[0]);
                        if (obj5 != null)
                        {
                            object obj6 = obj5.GetType().InvokeMember("ActiveConfiguration", BindingFlags.Instance | BindingFlags.Public | BindingFlags.GetProperty, null, obj5, new object[0]);
                            if (obj6 != null)
                            {
                                object obj7 = obj6.GetType().InvokeMember("Properties", BindingFlags.Instance | BindingFlags.Public | BindingFlags.GetProperty, null, obj6, new object[0]);
                                if (obj7 != null)
                                {
                                    object obj8 = obj7.GetType().InvokeMember("Item", BindingFlags.Instance | BindingFlags.Public | BindingFlags.InvokeMethod, null, obj7, new object[1]
                                    {
                                        "OutputPath"
                                    });
                                    if (obj8 != null)
                                    {
                                        text2 = (obj8.GetType().InvokeMember("Value", BindingFlags.Instance | BindingFlags.Public | BindingFlags.GetProperty, null, obj8, new object[0]) as string);
                                    }
                                    obj8 = obj7.GetType().InvokeMember("Item", BindingFlags.Instance | BindingFlags.Public | BindingFlags.InvokeMethod, null, obj7, new object[1]
                                    {
                                        "StartWorkingDirectory"
                                    });
                                    if (obj8 != null)
                                    {
                                        text3 = (obj8.GetType().InvokeMember("Value", BindingFlags.Instance | BindingFlags.Public | BindingFlags.GetProperty, null, obj8, new object[0]) as string);
                                    }
                                }
                            }
                        }
                    }
                }
                if (text != null)
                {
                    AddFolder(stringCollection, Path.GetDirectoryName(text));
                    AddFolder(stringCollection, Path.Combine(Path.GetDirectoryName(text), "bin\\"));
                    if (text2 != null && text2.Length > 0)
                    {
                        AddFolder(stringCollection, Path.Combine(Path.GetDirectoryName(text), text2));
                    }
                    if (text3 != null && text3.Length > 0)
                    {
                        AddFolder(stringCollection, Path.Combine(Path.GetDirectoryName(text), text3));
                    }
                }
            }
            catch (Exception)
            {
            }
            if (context != null && context.UsageMode == LicenseUsageMode.Designtime)
            {
                ITypeResolutionService typeResolutionService = context.GetService(typeof(ITypeResolutionService)) as ITypeResolutionService;
                if (typeResolutionService != null)
                {
                    AddFolder(stringCollection, Path.GetDirectoryName(typeResolutionService.GetPathOfAssembly(licensedType.Assembly.GetName())));
                    if (Assembly.GetEntryAssembly() != null)
                    {
                        AddFolder(stringCollection, Path.GetDirectoryName(typeResolutionService.GetPathOfAssembly(Assembly.GetEntryAssembly().GetName())));
                    }
                    if (Assembly.GetCallingAssembly() != null)
                    {
                        AddFolder(stringCollection, Path.GetDirectoryName(typeResolutionService.GetPathOfAssembly(Assembly.GetCallingAssembly().GetName())));
                    }
                }
            }
            if (licensedType != null && !(licensedType.Assembly is AssemblyBuilder) && !(licensedType.Assembly.GetType().Name == "InternalAssemblyBuilder"))
            {
                AddFolder(stringCollection, Path.GetDirectoryName(ResolveCodePath(licensedType.Assembly)));
            }
            if (Assembly.GetEntryAssembly() != null)
            {
                AddFolder(stringCollection, Path.GetDirectoryName(ResolveCodePath(Assembly.GetEntryAssembly())));
            }
            AppDomain currentDomain = AppDomain.CurrentDomain;
            AddFolder(stringCollection, currentDomain.BaseDirectory);
            AddFolder(stringCollection, currentDomain.RelativeSearchPath);
            AddFolder(stringCollection, currentDomain.SetupInformation.ApplicationBase);
            if (currentDomain.SetupInformation.PrivateBinPath != null)
            {
                string[] array = currentDomain.SetupInformation.PrivateBinPath.Split(';');
                foreach (string text5 in array)
                {
                    AddFolder(stringCollection, text5.Trim());
                }
            }
            if (currentDomain.SetupInformation.PrivateBinPathProbe != null)
            {
                string[] array2 = currentDomain.SetupInformation.PrivateBinPathProbe.Split(';');
                foreach (string text6 in array2)
                {
                    AddFolder(stringCollection, text6.Trim());
                }
            }
            AddFolder(stringCollection, Config.SharedFolder);
            return stringCollection;
        }

        private static string ResolveCodePath(Assembly asm)
        {
            Uri uri = new Uri(asm.CodeBase);
            string text = uri.LocalPath;
            if (uri.Fragment != null)
            {
                text += Uri.UnescapeDataString(uri.Fragment);
            }
            return Path.GetFullPath(text);
        }

        private static void AddFolder(StringCollection folders, string folder)
        {
            if (folder != null && folder.Trim().Length != 0 && folder.IndexOf('*') == -1)
            {
                try
                {
                    string tempPath = Path.GetTempPath();
                    if (string.Compare(folder, 0, tempPath, 0, tempPath.Length, true) != 0)
                    {
                        string text = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, folder);
                        if (folder.StartsWith("file:"))
                        {
                            int num = folder.IndexOf("/");
                            if (num == -1)
                            {
                                num = folder.IndexOf("\\");
                            }
                            if (folder.Length > 1 && (folder[num + 1] == '/' || folder[num + 1] == '\\'))
                            {
                                num++;
                            }
                            text = folder.Substring(num + 1);
                        }
                        if (text.IndexOf(Path.AltDirectorySeparatorChar) > -1)
                        {
                            text = text.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
                        }
                        if (text.EndsWith(new string(Path.DirectorySeparatorChar, 1)))
                        {
                            text = text.Substring(0, text.Length - 1);
                        }
                        if (Directory.Exists(text))
                        {
                            text = Path.GetFullPath(text);
                            foreach (string item in (IEnumerable)folders)
                            {
                                if (string.Compare(item, text, true, CultureInfo.InvariantCulture) == 0)
                                {
                                    return;
                                }
                            }
                            folders.Add(text);
                        }
                    }
                }
                catch
                {
                }
            }
        }

        private static StringCollection FindLicenseFiles(string folder)
        {
            StringCollection stringCollection = new StringCollection();
            stringCollection.AddRange(Directory.GetFiles(folder, "*.LIC"));
            for (int i = 0; i < stringCollection.Count; i++)
            {
                string path = stringCollection[i];
                string text = LicenseFile.MakeSharedName(path);
                if (File.Exists(text))
                {
                    stringCollection[i] = text;
                }
            }
            try
            {
                if (!SafeToolbox.IsFipsEnabled)
                {
                    using (IsolatedStorageFile isolatedStorageFile = LicenseFile.GetIsfStore())
                    {
                        string searchPattern = Path.Combine(LicenseFile.IsfGetDirectoryName(Path.GetDirectoryName(folder)), "*.LIC");
                        if (LicenseFile.IsfDirectoryExists(Path.GetDirectoryName(folder)))
                        {
                            string[] fileNames = isolatedStorageFile.GetFileNames(searchPattern);
                            foreach (string path2 in fileNames)
                            {
                                string text2 = Path.Combine(folder, path2);
                                if (stringCollection.Contains(text2))
                                {
                                    stringCollection.Remove(text2);
                                }
                                stringCollection.Insert(0, "isolated:" + text2);
                            }
                        }
                    }
                }
            }
            catch
            {
            }
            for (int k = 0; k < stringCollection.Count; k++)
            {
                string text3 = stringCollection[k].ToLower(CultureInfo.InvariantCulture);
                if (!text3.EndsWith(".lic") && !text3.EndsWith(".lic.xml"))
                {
                    stringCollection.RemoveAt(k);
                    k--;
                }
            }
            return stringCollection;
        }

        private static SecureLicense TryLicenseFile(SecureLicenseContext context, ICollection licenses, bool allowDelayed, ArrayList delayLicenses)
        {
            if (context.StopLooking)
            {
                return null;
            }
            SecureLicenseCollection secureLicenseCollection = licenses as SecureLicenseCollection;
            ArrayList arrayList = new ArrayList();
            context.SetValidatingLicense(null);
            StringCollection stringCollection = context.Items["TriedLicenseFiles"] as StringCollection;
            if (stringCollection == null)
            {
                stringCollection = new StringCollection();
                context.Items["TriedLicenseFiles"] = stringCollection;
            }
            if (secureLicenseCollection != null)
            {
                context.WriteDiagnostic("Trying license file {0}", secureLicenseCollection._licenseFile._location);
                if (stringCollection.Contains(secureLicenseCollection._licenseFile._location))
                {
                    return null;
                }
                stringCollection.Add(secureLicenseCollection._licenseFile._location);
                context.DisposeIfNotUsed(secureLicenseCollection.LicenseFile);
                string originalLocation = secureLicenseCollection.LicenseFile.OriginalLocation;
                if (originalLocation != null)
                {
                    try
                    {
                        LicenseFile licenseFile = null;
                        if (!File.Exists(originalLocation))
                        {
                            if (!originalLocation.EndsWith(".ldat"))
                            {
                                return null;
                            }
                            licenseFile = GetEmbeddedLicense(context.LicensedType);

                            //Realisable
                            //if (licenseFile != null && string.Compare(licenseFile.Location, originalLocation, true) == 0)
                            if (licenseFile != null) // We don't care about the location really.
                            {
                                goto IL_010f;
                            }
                            return null;
                        }
                        licenseFile = new LicenseFile(originalLocation, false);
                        goto IL_010f;
                        IL_010f:
                        if (!(licenseFile.Id != secureLicenseCollection.LicenseFile.Id) && licenseFile.ReleaseVersion <= secureLicenseCollection.LicenseFile.ReleaseVersion)
                        {
                            licenseFile.Dispose();
                        }
                        else
                        {
                            context.ReportError("E_OriginalUpdated", licenseFile, null, ErrorSeverity.Low, originalLocation);
                            secureLicenseCollection = licenseFile.Licenses;
                            if (stringCollection.Contains(secureLicenseCollection._licenseFile._location))
                            {
                                return null;
                            }
                            stringCollection.Add(secureLicenseCollection._licenseFile._location);
                        }
                    }
                    catch (Exception exception)
                    {
                        context.ReportError(null, secureLicenseCollection, exception, ErrorSeverity.High);
                        return null;
                    }
                }
                licenses = secureLicenseCollection.LicenseFile.GetLicensesForComponent(context.LicensedType.FullName);
                if (licenses.Count == 0)
                {
                    context.ReportError("E_NoComponentInLicense", secureLicenseCollection, null, ErrorSeverity.Low, context.LicensedType.FullName, secureLicenseCollection.LicenseFile.DisplayLocation);
                    return null;
                }
            }
            foreach (SecureLicense license2 in licenses)
            {
                if (context.StopLooking)
                {
                    return null;
                }
                if (!license2.IsTemplate)
                {
                    context.SetValidatingLicense(null);
                    switch (context.VerifySignature(license2))
                    {
                        case VerifySignatureResult.Invalid:
                            context.ReportError("E_InvalidSignature", license2, null, ErrorSeverity.High, license2.LicenseId, license2.LicenseFile.DisplayLocation);
                            break;
                        default:
                            context.ReportError("E_DifferentProduct", license2, null, ErrorSeverity.Low, context.LicensedType.FullName, license2.LicenseFile.DisplayLocation);
                            break;
                        case VerifySignatureResult.Valid:
                            {
                                if (!context.EnsureLicenseState(license2))
                                {
                                    break;
                                }
                                if (license2.IsTrial && (Config.DisableTrials || context.RequestInfo.DisableTrials))
                                {
                                    context.ReportError("E_SkipTrial", license2, null, ErrorSeverity.Low, license2, license2.LicenseFile.DisplayLocation);
                                    break;
                                }
                                if (license2.CheckNeedsGui(context) && !allowDelayed)
                                {
                                    goto IL_0347;
                                }
                                if (license2.IsTrial)
                                {
                                    goto IL_0347;
                                }
                                goto IL_0378;
                            }
                            IL_0378:
                            if (license2.IsTrial && allowDelayed && !license2.IsUnlocked)
                            {
                                arrayList.Add(license2);
                            }
                            else
                            {
                                SecureLicense secureLicense2 = TryLicense(license2, context, true);
                                if (secureLicense2 != null)
                                {
                                    return secureLicense2;
                                }
                            }
                            break;
                            IL_0347:
                            if (delayLicenses != null && !license2.IsUnlocked)
                            {
                                if (allowDelayed && license2.IsTrial)
                                {
                                    arrayList.Add(license2);
                                }
                                else if (delayLicenses != null)
                                {
                                    delayLicenses.Add(license2);
                                }
                                break;
                            }
                            goto IL_0378;
                    }
                }
            }
            if (allowDelayed && arrayList.Count > 0)
            {
                foreach (SecureLicense item in arrayList)
                {
                    if (context.StopLooking)
                    {
                        break;
                    }
                    SecureLicense secureLicense3 = TryLicense(item, context, true);
                    if (secureLicense3 != null)
                    {
                        return secureLicense3;
                    }
                }
            }
            context.SetValidatingLicense(null);
            return null;
        }

        private static SecureLicense TryLicense(SecureLicense license, SecureLicenseContext context, bool canSkipSigCheck)
        {
            bool flag = false;
            SecureLicense secureLicense = null;
            try
            {
                if (context.HaveCheckedLicense(license))
                {
                    return null;
                }
                if (license.IsTemplate)
                {
                    return null;
                }
                context.SetValidatingLicense(license);
                bool flag2 = context.CheckAddresses();
                if (!canSkipSigCheck)
                {
                    switch (context.VerifySignature(license))
                    {
                        case VerifySignatureResult.Invalid:
                            context.ReportError("E_InvalidSignature", license, null, ErrorSeverity.High, license.LicenseId, license.LicenseFile.DisplayLocation);
                            goto IL_00b3;
                        default:
                            context.ReportError("E_DifferentProduct", license, null, ErrorSeverity.Low, context.LicensedType.FullName, license.LicenseFile.DisplayLocation);
                            goto IL_00b3;
                        case VerifySignatureResult.Valid:
                            break;
                            IL_00b3:
                            return null;
                    }
                }
                if (context.RequestInfo.LicenseFilter != null && !context.RequestInfo.LicenseFilter(license, context))
                {
                    return null;
                }
                flag = true;
                context.RequestInfo.RegistrationInfo.Push();
                ValidationResult validationResult = license.Validate(context);
                if (validationResult == ValidationResult.Valid && flag2)
                {
                    secureLicense = license;
                    return license;
                }
                if (context._retryFile == null && validationResult != ValidationResult.Retry)
                {
                    goto end_IL_0004;
                }
                return DoRetry(context);
                end_IL_0004:;
            }
            catch (Exception ex)
            {
                context.WriteDiagnostic(ex.ToString());
                context.ReportError("E_UnexpectedValidate", license, ex, ErrorSeverity.High);
            }
            finally
            {
                context.SetValidatingLicense(null);
                if (flag)
                {
                    context.RequestInfo.RegistrationInfo.Pop();
                }
                if (!context.HasFormBeenShown || (license.HardFailureIfFormShown && secureLicense == null))
                {
                    Limit[] array = license.Limits.FindLimitsByType(typeof(ISuperFormLimit), true);
                    if (array != null)
                    {
                        Limit[] array2 = array;
                        int num = 0;
                        while (num < array2.Length)
                        {
                            ISuperFormLimit superFormLimit = (ISuperFormLimit)array2[num];
                            if (!superFormLimit.FormShown)
                            {
                                num++;
                                continue;
                            }
                            context.HasFormBeenShown = true;
                            if (!license.HardFailureIfFormShown)
                            {
                                break;
                            }
                            context.StopLooking = true;
                            break;
                        }
                    }
                }
            }
            return null;
        }

        private static SecureLicense DoRetry(SecureLicenseContext context)
        {
            context.Retry++;
            if (context.Retry > 6)
            {
                throw new SecureLicenseException("E_RetriedTooManyTimes");
            }
            context.StopLooking = false;
            context.Items.Remove("TriedLicenseFiles");
            if (context._retryLicense == null && context._retryFile == null)
            {
                return TryLicense(context.ValidatingLicense, context, false);
            }
            if (context._retryLicense != null)
            {
                SecureLicense retryLicense = context._retryLicense;
                context._retryLicense = null;
                if (retryLicense.CanUnlockBySerial)
                {
                    bool shouldGetNewSerialNumber = context.RequestInfo.ShouldGetNewSerialNumber;
                    try
                    {
                        context.RequestInfo.ShouldGetNewSerialNumber = false;
                        return TryLicense(retryLicense, context, false);
                    }
                    finally
                    {
                        context.RequestInfo.ShouldGetNewSerialNumber = shouldGetNewSerialNumber;
                    }
                }
                return TryLicense(retryLicense, context, false);
            }
            ArrayList arrayList = new ArrayList();
            LicenseFile retryFile = context._retryFile;
            SecureLicense secureLicense = TryLicenseFile(context, retryFile.Licenses, false, arrayList);
            if (secureLicense == null)
            {
                secureLicense = TryLicenseFile(context, arrayList, true, null);
            }
            if (secureLicense != null)
            {
                secureLicense.LicenseFile.SaveOnValid(context, true);
            }
            return secureLicense;
        }

        internal static SecureLicense DoRetryWithGrant(SecureLicenseContext context)
        {
            SecureLicense license = DoRetry(context);
            return GrantLicense(context, license);
        }

        public static LicenseFile GetEmbeddedLicense(Type type)
        {
            string text;
            using (Stream stream = Toolbox.GetEmbeddedResource(type, "runtime.lic", out text))
            {
                if (stream != null)
                {
                    LicenseFile licenseFile = new LicenseFile();
                    licenseFile.Load(stream);
                    licenseFile.SetAsEmbedded();
                    string text2 = null;
                    if (LicenseManager.CurrentContext != null)
                    {
                        ITypeResolutionService typeResolutionService = LicenseManager.CurrentContext.GetService(typeof(ITypeResolutionService)) as ITypeResolutionService;
                        if (typeResolutionService != null)
                        {
                            string pathOfAssembly = typeResolutionService.GetPathOfAssembly(type.Assembly.GetName());
                            if (pathOfAssembly != null)
                            {
                                text2 = Path.Combine(pathOfAssembly, type.Assembly.GetName().Name + ".ldat");
                            }
                        }
                    }
                    if (text2 == null)
                    {
                        text2 = ResolveCodePath(type.Assembly) + ".ldat";
                    }
                    licenseFile._location = text2;
                    string path = LicenseFile.MakeSharedName(text2);
                    if (File.Exists(text2) || LicenseFile.IsfFileExists(text2) || File.Exists(path))
                    {
                        LicenseFile licenseFile2 = new LicenseFile(text2, true);
                        licenseFile2.SetAsEmbedded();
                        if (licenseFile.ReleaseVersion <= licenseFile2.ReleaseVersion)
                        {
                            return licenseFile2;
                        }
                    }
                    return licenseFile;
                }
            }
            return null;
        }

        private static SecureLicense FindSavedLicense(SecureLicenseContext context, ArrayList delayLicenses)
        {
            if (context == null)
            {
                return null;
            }
            Check.NotNull(delayLicenses, "delayLicenses");
            StackTrace stackTrace = context.StackTrace;
            Assembly assembly = null;
            SecureLicense secureLicense = null;
            try
            {
                int num = 0;
                while (true)
                {
                    if (num < stackTrace.FrameCount)
                    {
                        StackFrame frame = stackTrace.GetFrame(num);
                        MethodBase method = frame.GetMethod();
                        if (method != null)
                        {
                            Type declaringType = method.DeclaringType;
                            if (declaringType == null)
                            {
                                break;
                            }
                            if (declaringType.Assembly == null)
                            {
                                break;
                            }
                            assembly = method.DeclaringType.Assembly;
                            if (assembly != typeof(SecureLicense).Assembly && assembly != typeof(object).Assembly)
                            {
                                if (assembly is AssemblyBuilder || assembly.GetType().Name == "InternalAssemblyBuilder")
                                {
                                    return null;
                                }
                                string fullName = assembly.FullName;
                                if (!fullName.StartsWith("System.") && !fullName.StartsWith("mscorlib") && !fullName.StartsWith("System,") && assembly != context.LicensedType.Assembly)
                                {
                                    goto IL_0107;
                                }
                            }
                            num++;
                            continue;
                        }
                        return null;
                    }
                    goto IL_0107;
                    IL_0107:
                    if (assembly == null)
                    {
                        return null;
                    }
                    Hashtable hashtable = null;
                    string text = Uri.UnescapeDataString(assembly.CodeBase.Substring(assembly.CodeBase.LastIndexOf('/') + 1));
                    object obj = _savedLicenses[assembly.CodeBase];
                    hashtable = (obj as Hashtable);
                    if (hashtable == null)
                    {
                        if (obj != null)
                        {
                            return null;
                        }
                        lock (SyncRoot)
                        {
                            if (hashtable == null)
                            {
                                using (Stream stream = assembly.GetManifestResourceStream(text + ".licenses"))
                                {
                                    if (stream == null)
                                    {
                                        _savedLicenses[assembly.CodeBase] = new object();
                                        return null;
                                    }
                                    hashtable = DeserializeHashtable(stream, text.ToUpper(CultureInfo.InvariantCulture));
                                    _savedLicenses[assembly.CodeBase] = (hashtable ?? new object());
                                }
                                if (hashtable == null)
                                {
                                    return null;
                                }
                            }
                        }
                    }
                    string text2 = hashtable[context.LicensedType.AssemblyQualifiedName] as string;
                    if (text2 == null)
                    {
                        text2 = FindVersionCompatibleLicense(hashtable, context.LicensedType.AssemblyQualifiedName);
                    }
                    if (text2 == null)
                    {
                        return null;
                    }
                    LicenseFile licenseFile = new LicenseFile();
                    licenseFile.FromXmlString(text2);
                    licenseFile._location = ResolveCodePath(assembly) + ".licenses." + SharedToolbox.GetHashCode(context.LicensedType.AssemblyQualifiedName).ToString("X", CultureInfo.InvariantCulture);
                    if (!File.Exists(licenseFile.Location) && !LicenseFile.IsfFileExists(licenseFile.Location))
                    {
                        licenseFile.SetAsEmbedded();
                    }
                    else
                    {
                        licenseFile.Load(licenseFile.Location, true);
                    }
                    secureLicense = TryLicenseFile(context, licenseFile.Licenses, false, delayLicenses);
                    return secureLicense;
                }
                return null;
            }
            finally
            {
                if (secureLicense == null)
                {
                    context.ReportError("E_NoCompiledLicenses", null, null, ErrorSeverity.Low);
                }
            }
        }

        private static string FindVersionCompatibleLicense(Hashtable strings, string key)
        {
            key = MakeVersionStrippedKey(key);
            foreach (string key2 in strings.Keys)
            {
                if (string.Compare(MakeVersionStrippedKey(key2), key, true, CultureInfo.InvariantCulture) == 0)
                {
                    return strings[key2] as string;
                }
            }
            return null;
        }

        private static string MakeVersionStrippedKey(string key)
        {
            int num = key.IndexOf("Version=");
            if (num == -1)
            {
                return key;
            }
            int num2 = key.IndexOf(',', num);
            if (num2 == -1)
            {
                return key.Substring(0, num - 2);
            }
            return key.Remove(num - 2, num2 - num + 2);
        }

        private static Hashtable DeserializeHashtable(Stream o, string cryptoKey)
        {
            IFormatter formatter = new BinaryFormatter();
            object obj = formatter.Deserialize(o);
            object[] array = obj as object[];
            if (array != null && array[0] is string && (string)array[0] == cryptoKey)
            {
                return array[1] as Hashtable;
            }
            return null;
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public static string MakeSystemReport()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(SR.GetString("M_SystemInfo"));
            stringBuilder.Append(Environment.NewLine);
            stringBuilder.Append("--------------------------------------------------");
            stringBuilder.Append(Environment.NewLine);
            stringBuilder.Append(Environment.NewLine);
            stringBuilder.Append(Toolbox.MakeSystemInfoString());
            stringBuilder.Append(Environment.NewLine);
            stringBuilder.Append(Environment.NewLine);
            stringBuilder.Append(SR.GetString("M_Assemblies"));
            stringBuilder.Append(Environment.NewLine);
            stringBuilder.Append("--------------------------------------------------");
            stringBuilder.Append(Environment.NewLine);
            stringBuilder.Append(Environment.NewLine);
            stringBuilder.Append(Toolbox.MakeAssemblyListString());
            return stringBuilder.ToString();
        }

        public static void ResetCache()
        {
            if (!Monitor.TryEnter(SyncRoot))
            {
                Action action = () => ResetCacheInternal();
                action.BeginInvoke(null, null);
            }
            else
            {
                Monitor.Exit(SyncRoot);
                ResetCacheInternal();
            }
        }

        private static void ResetCacheInternal()
        {
            ArrayList arrayList = new ArrayList();
            lock (SyncRoot)
            {
                arrayList.AddRange(_cachedLicenses);
                _cachedLicenses.Clear();
            }
            FinalizeUncachedLicenses(arrayList);
        }

        public static void ResetCacheForType(Type type)
        {
            if (!Monitor.TryEnter(SyncRoot))
            {
                ResetCacheForTypeDelegate resetCacheForTypeDelegate = ResetCacheForTypeInternal;
                resetCacheForTypeDelegate.BeginInvoke(type, null, null);
            }
            else
            {
                Monitor.Exit(SyncRoot);
                ResetCacheForTypeInternal(type);
            }
        }

        private static void ResetCacheForTypeInternal(Type type)
        {
            if (type != null)
            {
                string nonVersionedAssemblyName = TypeHelper.GetNonVersionedAssemblyName(type.Assembly);
                string value = nonVersionedAssemblyName + type.FullName;
                string value2 = nonVersionedAssemblyName + nonVersionedAssemblyName + ".*";
                string fullName = type.FullName;
                ArrayList arrayList = new ArrayList();
                lock (SyncRoot)
                {
                    IDictionaryEnumerator enumerator = _cachedLicenses.GetEnumerator();
                    try
                    {
                        while (enumerator.MoveNext())
                        {
                            DictionaryEntry dictionaryEntry = (DictionaryEntry)enumerator.Current;
                            string text = dictionaryEntry.Key as string;
                            if (text.StartsWith(fullName) || text.StartsWith(value) || text.StartsWith(value2))
                            {
                                arrayList.Add(dictionaryEntry);
                            }
                        }
                    }
                    finally
                    {
                        IDisposable disposable = enumerator as IDisposable;
                        if (disposable != null)
                        {
                            disposable.Dispose();
                        }
                    }
                    foreach (DictionaryEntry item in arrayList)
                    {
                        _cachedLicenses.Remove(item.Key);
                    }
                }
                FinalizeUncachedLicenses(arrayList);
            }
        }

        private static void FinalizeUncachedLicenses(ArrayList removed)
        {
            lock (SyncRoot)
            {
                foreach (DictionaryEntry item in removed)
                {
                    SecureLicense secureLicense = item.Value as SecureLicense;
                    secureLicense.FlushSecureData(false);
                    secureLicense.ResetRuntimeState();
                    if (secureLicense._pendingContexts != null)
                    {
                        ArrayList arrayList = new ArrayList();
                        lock (SyncRoot)
                        {
                            arrayList.AddRange(secureLicense._pendingContexts);
                            secureLicense._pendingContexts.Clear();
                        }
                        foreach (SecureLicenseContext item2 in arrayList)
                        {
                            item2.StopDelayed(10000, new ValidationRecord("E_CacheReset", null, null, ErrorSeverity.Low), null);
                        }
                    }
                    secureLicense.Dispose();
                }
            }
        }

        public static void ResetCacheForLicense(SecureLicense license)
        {
            if (!Monitor.TryEnter(SyncRoot))
            {
                ResetCacheForLicenseDelegate resetCacheForLicenseDelegate = ResetCacheForLicenseInternal;
                resetCacheForLicenseDelegate.BeginInvoke(license, null, null);
            }
            else
            {
                Monitor.Exit(SyncRoot);
                ResetCacheForLicenseInternal(license);
            }
        }

        private static void ResetCacheForLicenseInternal(SecureLicense license)
        {
            ArrayList arrayList = new ArrayList();
            lock (SyncRoot)
            {
                IDictionaryEnumerator enumerator = _cachedLicenses.GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        DictionaryEntry dictionaryEntry = (DictionaryEntry)enumerator.Current;
                        SecureLicense secureLicense = dictionaryEntry.Value as SecureLicense;
                        if (secureLicense.SerialNumber == license.SerialNumber && secureLicense.LicenseFile.Location == license.LicenseFile.Location)
                        {
                            goto IL_007f;
                        }
                        if (secureLicense.LicenseId == license.LicenseId)
                        {
                            goto IL_007f;
                        }
                        continue;
                        IL_007f:
                        arrayList.Add(dictionaryEntry);
                    }
                }
                finally
                {
                    IDisposable disposable = enumerator as IDisposable;
                    if (disposable != null)
                    {
                        disposable.Dispose();
                    }
                }
                foreach (DictionaryEntry item in arrayList)
                {
                    _cachedLicenses.Remove(item.Key);
                }
            }
            FinalizeUncachedLicenses(arrayList);
        }

        private static SecureLicense GetCachedLicense(SecureLicenseContext context)
        {
            SecureLicense secureLicense = null;
            string text = null;
            string text2 = null;
            text = ((context.Instance == null || context.Instance.GetType() == context.LicensedType) ? null : TypeHelper.GetNonVersionedAssemblyName(context.Instance.GetType().Assembly));
            text2 = text + context.LicensedType.FullName;
            lock (SyncRoot)
            {
                secureLicense = (_cachedLicenses[text2] as SecureLicense);
                if (secureLicense == null)
                {
                    text2 = text + TypeHelper.GetNonVersionedAssemblyName(context.LicensedType.Assembly) + ".*";
                    secureLicense = (_cachedLicenses[text2] as SecureLicense);
                }
            }
            if (secureLicense != null && secureLicense.Signature != null && !secureLicense.IsDisposed)
            {
                try
                {
                    SecureLicense secureLicense2 = GrantLicense(context, secureLicense);
                    if (secureLicense2 == null)
                    {
                        secureLicense._isCached = false;
                        secureLicense.Dispose();
                        _cachedLicenses.Remove(text2);
                    }
                    return secureLicense2;
                }
                catch
                {
                    lock (SyncRoot)
                    {
                        secureLicense._isCached = false;
                        secureLicense.Dispose();
                        _cachedLicenses.Remove(text2);
                    }
                    return null;
                }
            }
            return null;
        }

        public static void SuggestLicense(Type type, SecureLicense license)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }
            if (license == null)
            {
                throw new ArgumentNullException("license");
            }
            string fullName = type.FullName;
            List<SecureLicense> list;
            if (!_suggestedLicenses.TryGetValue(fullName, out list))
            {
                lock (SyncRoot)
                {
                    if (!_suggestedLicenses.TryGetValue(fullName, out list))
                    {
                        list = new List<SecureLicense>();
                        _suggestedLicenses[fullName] = list;
                    }
                }
            }
            list.Insert(0, license);
        }

        private static SecureLicense GetSuggestedLicense(SecureLicenseContext context, ArrayList delayed)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }
            Type licensedType = context.LicensedType;
            string fullName = licensedType.FullName;
            List<SecureLicense> licenses;
            if (!_suggestedLicenses.TryGetValue(fullName, out licenses))
            {
                context.ReportError("E_NoLicensesFound", null, null, ErrorSeverity.Low, SR.GetString("M_SuggestionCache"));
                return null;
            }
            SecureLicense secureLicense = TryLicenseFile(context, licenses, true, delayed);
            if (secureLicense == null)
            {
                context.ReportError("E_NoLicensesFound", null, null, ErrorSeverity.Low, SR.GetString("M_SuggestionCache"));
            }
            return secureLicense;
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public override License GetLicense(LicenseContext context, Type type, object instance, bool allowExceptions)
        {
            return ValidateInternal(instance, type, null, context, new StackTrace(1, false));
        }

        public static bool RegisterTranslation(Assembly asm, string resourceName)
        {
            return SR.Resources.RegisterAssembly(asm, resourceName);
        }

        [Obsolete("No longer supported. See http://xbugz.com/issue/DLX-473", true)]
        public static void RegisterCodeProcessor(Assembly asm, CodeAlgorithm algorithm, object processor, string methodName, string characterSet)
        {
            throw new NotSupportedException("Custom algorithms are no longer supported: http://xbugz.com/issue/DLX-473");
        }
    }
}
