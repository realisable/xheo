using System;
using System.Collections;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public sealed class LicenseResourceCollection : WithEventsCollection
	{
		public LicenseResource this[int index]
		{
			get
			{
				return base.List[index] as LicenseResource;
			}
			set
			{
				base.List[index] = value;
			}
		}

		public byte[] GetBinary(string name)
		{
			int num = IndexOf(name);
			if (num == -1)
			{
				return null;
			}
			return ((LicenseResource)base.List[num]).Data;
		}

		public void SetBinary(string name, byte[] value)
		{
			int num = IndexOf(name);
			if (num == -1)
			{
				Add(name, value);
			}
			else
			{
				((LicenseResource)base.List[num]).Data = value;
			}
		}

		public int Add(LicenseResource resource)
		{
			return base.List.Add(resource);
		}

		public void Set(LicenseResource resource)
		{
			if (resource != null)
			{
				int num = IndexOf(resource.Name);
				if (num == -1)
				{
					Add(resource);
				}
				else
				{
					this[num] = resource;
				}
			}
		}

		public LicenseResource Add(string name, byte[] data)
		{
			LicenseResource licenseResource = new LicenseResource(name, data);
			base.List.Add(licenseResource);
			return licenseResource;
		}

		public void AddRange(LicenseResource[] resources)
		{
			if (resources != null)
			{
				foreach (LicenseResource resource in resources)
				{
					Add(resource);
				}
			}
		}

		public void AddRange(LicenseResourceCollection resources)
		{
			if (resources != null)
			{
				foreach (LicenseResource item in (IEnumerable)resources)
				{
					Add(item);
				}
			}
		}

		public void Insert(int index, LicenseResource resource)
		{
			base.List.Insert(index, resource);
		}

		public void Remove(LicenseResource resource)
		{
			base.List.Remove(resource);
		}

		public void CopyTo(LicenseResource[] array, int index)
		{
			base.List.CopyTo(array, index);
		}

		public int IndexOf(LicenseResource resource)
		{
			return base.List.IndexOf(resource);
		}

		public int IndexOf(string name)
		{
			int num = 0;
			while (true)
			{
				if (num < Count)
				{
					if (string.Compare(name, this[num].Name, true) == 0)
					{
						break;
					}
					num++;
					continue;
				}
				return -1;
			}
			return num;
		}

		public bool Contains(LicenseResource resource)
		{
			return base.List.Contains(resource);
		}

		public bool Contains(string name)
		{
			return IndexOf(name) != -1;
		}

		public void ReadFromXml(XmlReader reader)
		{
			Check.NotNull(reader, "reader");
			if (Count > 0)
			{
				Clear();
			}
			reader.MoveToContent();
			if (!(reader.Name != "Resources"))
			{
				reader.Read();
				while (true)
				{
					if (!reader.EOF)
					{
						if (!reader.IsStartElement())
						{
							break;
						}
						if (reader.Name == "Resource")
						{
							LicenseResource licenseResource = new LicenseResource();
							licenseResource.ReadFromXml(reader);
							Add(licenseResource);
						}
						else
						{
							reader.Skip();
						}
						continue;
					}
					return;
				}
				reader.Read();
			}
		}

		public void WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			Check.NotNull(writer, "writer");
			if (Count != 0)
			{
				writer.WriteStartElement("Resources");
				foreach (LicenseResource item in (IEnumerable)this)
				{
					item.WriteToXml(writer, signing);
				}
				writer.WriteEndElement();
			}
		}
	}
}
