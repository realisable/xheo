using System;

namespace DeployLX.Licensing.v5
{
	public sealed class UseMonitor
	{
		private UseLimit _useLimit;

		private int _useCount;

		public UseLimit UseLimit
		{
			get
			{
				return _useLimit;
			}
		}

		public int UseCount
		{
			get
			{
				return _useCount;
			}
		}

		public int UsesRemaining
		{
			get
			{
				return Math.Max(0, _useLimit.Uses - _useCount);
			}
		}

		public event EventHandler UseExpired;

		private void OnUseExpired(EventArgs e)
		{
			if (this.UseExpired != null)
			{
				this.UseExpired(this, e);
			}
		}

		internal UseMonitor(UseLimit limit)
		{
			_useLimit = limit;
			_useCount = limit.GetStoredUses();
		}

		public bool AddUse()
		{
			if (_useCount >= ((IExtendableLimit)_useLimit).ExtendableValue)
			{
				OnUseExpired(EventArgs.Empty);
				return false;
			}
			_useCount++;
			if (_useLimit.UseLimitType == UseLimitType.Cumulative)
			{
				_useLimit.SetStoredUses(_useCount);
			}
			return true;
		}

		public void RemoveUse()
		{
			if (!_useLimit.AllowUseRemove)
			{
				throw new SecureLicenseException("E_CannotReduceUsage");
			}
			_useCount--;
			if (_useCount < 0)
			{
				_useCount = 0;
			}
			if (_useLimit.UseLimitType == UseLimitType.Cumulative)
			{
				_useLimit.SetStoredUses(_useCount);
			}
		}

		public bool CheckLimitReached(int max)
		{
			return _useCount >= ((max == -1) ? ((IExtendableLimit)_useLimit).ExtendableValue : max);
		}

		internal void Connect(UseLimit limit)
		{
			_useLimit = limit;
			_useCount = limit.GetStoredUses();
		}

		public SecureLicense ShowExtensionForm(object instance, Type licensedType, LicenseValidationRequestInfo info)
		{
			return SecureLicenseManager.ShowForm(_useLimit, "EXTEND", instance, licensedType, info, null);
		}
	}
}
