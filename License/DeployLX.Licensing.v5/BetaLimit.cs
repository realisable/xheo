using System;
using System.Collections.Specialized;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	[LimitEditor("BetaLimitEditor", Category = "Super Form Limits", IconResource = "asmres://DeployLX.Licensing.DlxAddIn.v5/DeployLX.Licensing.DlxAddIn.v5.Resources.Limits.Beta.png")]
	public class BetaLimit : SuperFormLimit
	{
		public class PageIds
		{
			public const string Default = "DEFAULT";
		}

		private bool _shown;

		private string _updateUrl;

		private bool _browseUpdate;

		private string _message;

		private bool _shouldShowForm = true;

		public string UpdateUrl
		{
			get
			{
				return _updateUrl;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_updateUrl != value)
				{
					SharedToolbox.ValidateUrl(value, true);
					_updateUrl = value;
					base.OnChanged("UpdateUrl");
				}
			}
		}

		public bool BrowseUpdate
		{
			get
			{
				return _browseUpdate;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_browseUpdate != value)
				{
					_browseUpdate = value;
					base.OnChanged("BrowseUpdate");
				}
			}
		}

		public string Message
		{
			get
			{
				return _message;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_message != value)
				{
					_message = value;
					base.OnChanged("Message");
				}
			}
		}

		public bool ShouldShowForm
		{
			get
			{
				return _shouldShowForm;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_shouldShowForm != value)
				{
					_shouldShowForm = value;
					base.OnChanged("ShouldShowForm");
				}
			}
		}

		public override string Description
		{
			get
			{
				return SR.GetString("M_BetaLimitDescription");
			}
		}

		public override string Name
		{
			get
			{
				return "Beta";
			}
		}

		public override bool IsGui
		{
			get
			{
				if (_shouldShowForm)
				{
					return true;
				}
				return base.IsGui;
			}
		}

		public override ValidationResult Validate(SecureLicenseContext context)
		{
			if (CanContinueBeta(context))
			{
				return base.Validate(context);
			}
			if (base.ResolveShouldShowForm(context) && context.CanShowWindowsForm)
			{
				ShowForm(context, "DEFAULT");
				context.CanSkipFailureReport = true;
				_shown = true;
			}
			if (_message != null)
			{
				context.ReportError(_message, this, ErrorSeverity.Low);
			}
			else if (_updateUrl == null)
			{
				context.ReportError("E_BetaExpired", this, null, ErrorSeverity.Low);
			}
			else
			{
				context.ReportError("E_BetaExpiredUrl", this, null, ErrorSeverity.Low, _updateUrl);
			}
			return ValidationResult.Invalid;
		}

		protected override bool ResolveDefaultShouldShowForm(SecureLicenseContext context)
		{
			if (_shouldShowForm)
			{
				return !_shown;
			}
			return false;
		}

		protected override bool ReadFromXml(XmlReader reader)
		{
			_browseUpdate = (reader.GetAttribute("browseUpdate") == "true");
			_shouldShowForm = (reader.GetAttribute("shouldShowForm") == "true");
			_updateUrl = SafeToolbox.XmlDecode(reader.GetAttribute("updateUrl"));
			_message = reader.GetAttribute("message");
			if (!base.ReadFromXml(reader))
			{
				return false;
			}
			return base.ReadChildLimits(reader);
		}

		protected override bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			Check.NotNull(writer, "writer");
			if (_updateUrl != null)
			{
				writer.WriteAttributeString("updateUrl", SafeToolbox.XmlEncode(_updateUrl));
			}
			if (_browseUpdate)
			{
				writer.WriteAttributeString("browseUpdate", "true");
			}
			if (_shouldShowForm)
			{
				writer.WriteAttributeString("shouldShowForm", "true");
			}
			if (_message != null)
			{
				writer.WriteAttributeString("message", _message);
			}
			return base.WriteToXml(writer, signing);
		}

		protected override SuperFormPanel CreatePanel(string pageId, SecureLicenseContext context)
		{
			if (((StringDictionary)base.CustomForms)[pageId] == null && pageId == "DEFAULT")
			{
				if (_browseUpdate && _updateUrl != null)
				{
					return new InfoPanel(Toolbox.ResolveUrl(_updateUrl, context).ToString(), null);
				}
				string text = _message;
				if (text == null || text.Length == 0)
				{
					text = ((_updateUrl != null) ? SR.GetString("E_BetaExpiredUrl", _updateUrl) : SR.GetString("E_BetaExpired"));
				}
				return new InfoPanel(text, Toolbox.ResolveUrl(_updateUrl, context));
			}
			return base.CreatePanel(pageId, context);
		}

		public bool CanContinueBeta(SecureLicenseContext context)
		{
			PeekResult peekResult = base.Peek(context);
			if (peekResult != PeekResult.Invalid && peekResult != PeekResult.NeedsUser)
			{
				return true;
			}
			return false;
		}
	}
}
