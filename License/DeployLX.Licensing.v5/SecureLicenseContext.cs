using Microsoft.Win32;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.UI;
using System.Windows.Forms;
using System.Xml;
using Newtonsoft.Json;
using Realisable.Utils;
using Realisable.Utils.Net;

namespace DeployLX.Licensing.v5
{
    [ComVisible(false)]
    public sealed class SecureLicenseContext : LicenseContext, IDisposable
    {
        private sealed class HtmlRender
        {
            public System.Web.UI.Control _instance;

            public HtmlLocation _location;

            public string _html;

            public string _htmlId;

            public SecureLicense _validLicense;

            public SecureLicenseContext _context;

            public bool _exclusive;

            private bool _attached;

            public Page _page;

            public void control_PreRender(object sender, EventArgs e)
            {
                if (!_attached)
                {
                    _attached = true;
                    HttpContext httpContext = _context.GetHttpContext();
                    httpContext.Response.Write(_html);
                }
            }

            public void control_Init(object sender, EventArgs e)
            {
                if (_context._selectedLicense == _validLicense && !_attached)
                {
                    System.Web.UI.Control instance = _instance;
                    HtmlLocation htmlLocation = _location;
                    if (htmlLocation == HtmlLocation.Inline && (instance == null || instance is Page || instance.Parent == null))
                    {
                        htmlLocation = HtmlLocation.Bottom;
                    }
                    if (_exclusive)
                    {
                        HttpContext httpContext = _context.GetHttpContext();
                        if (!httpContext.Response.Buffer)
                        {
                            _context.ReportError("E_AspNetNotBuffered", null);
                            throw new NotSupportedException();
                        }
                        httpContext.Response.Clear();
                        httpContext.Response.Write("<html><body>");
                        httpContext.Response.Write(_html);
                        httpContext.Response.Write("</body></html>");
                        httpContext.Response.End();
                    }
                    switch (htmlLocation)
                    {
                        case HtmlLocation.Top:
                            _page.ClientScript.RegisterClientScriptBlock(typeof(Page), "XSLB:" + _htmlId, _html, false);
                            break;
                        case HtmlLocation.Bottom:
                            _page.ClientScript.RegisterStartupScript(typeof(Page), "XSLB:" + _htmlId, _html);
                            break;
                        case HtmlLocation.Inline:
                            if (instance != null && !(instance is Page) && instance.Parent != null)
                            {
                                instance.Parent.Controls.AddAt(instance.Parent.Controls.IndexOf(instance) + 1, new LiteralControl(_html));
                            }
                            else
                            {
                                _page.Controls.Add(new LiteralControl(_html));
                            }
                            break;
                    }
                    _attached = true;
                }
            }
        }

        private class PanelResultHandlerWrapper
        {
            private PanelResultHandler _handler;

            private ISuperFormLimit _limit;

            public PanelResultHandlerWrapper(ISuperFormLimit limit, PanelResultHandler handler)
            {
                _limit = limit;
                _handler = handler;
            }

            public void HandleResult(FormResult result)
            {
                if (result == FormResult.Success || ((Limit)_limit).License.HardFailureIfFormShown)
                {
                    _limit.FormShown = true;
                }
                if (_handler != null)
                {
                    _handler(result);
                }
            }
        }

        private sealed class DelayedValidationContext
        {
            public DelayedLimit _limit;

            public Thread _thread;

            public SecureLicenseContext _context;

            public void Go()
            {
                try
                {
                    if (_limit.DoDelayValidate(_context) != ValidationResult.Valid)
                    {
                        _context.ReportError("E_DelayedFailed", _limit, null, ErrorSeverity.Normal);
                    }
                }
                catch (Exception exception)
                {
                    _context.ReportError("E_DelayedFailed", _limit, exception, ErrorSeverity.High);
                }
                finally
                {
                    bool flag = false;
                    SecureLicense license = _limit.License;
                    lock (_context)
                    {
                        _context._delayedLimits.Remove(_limit.LimitId);
                        if (_context._delayedLimits.Count == 0)
                        {
                            if (!_context._hasDelayFailed)
                            {
                                _limit.License.SaveOnValid(_context, false);
                                SecureLicenseManager.OnDelayedValidationComplete(_context, new LicenseEventArgs(_context));
                            }
                            else
                            {
                                SecureLicenseManager.OnDelayedValidationFailed(_context, new LicenseEventArgs(_limit.License, _context, _context._validatingRecord));
                                _context.StopDelayed(10000, _context._latestValidationRecord, _limit);
                            }
                            flag = true;
                        }
                    }
                    if (flag)
                    {
                        lock (SecureLicenseManager.SyncRoot)
                        {
                            lock (_context)
                            {
                                license._pendingContexts.Remove(_context);
                                _context.Dispose();
                            }
                        }
                    }
                }
            }
        }

        private static readonly byte[] HeaderSignature = new byte[3]
        {
            88,
            76,
            75
        };

        private static readonly Version v3_0 = new Version(3, 0);

        public static readonly Version CurrentVersion = v3_0;

        private readonly LicenseContext _original;

        private readonly ArrayList _toDispose = new ArrayList();

        private SecureLicense _selectedLicense;

        internal SecureLicense _retryLicense;

        internal LicenseFile _retryFile;

        internal bool _dontThrowOnFailure;

        private ValidationRecord _validatingRecord;

        private Page _page;

        private HttpContext _httpContext;

        private readonly byte[] _keyId;

        private SuperFormTheme _lastTheme;

        private System.Windows.Forms.Control _uiControl;

        private readonly byte[] _signature;

        private readonly byte[] _license;

        private readonly byte[] _serial;

        private readonly byte[] _activation;

        private readonly byte[] _pke;

        private readonly byte[] _sessionKey;

        private string _contextId = Guid.NewGuid().ToString("N");

        private int _isWebRequest;

        private int _isServiceRequest;

        private int _isConsoleRequest;

        private Hashtable _items = new Hashtable();

        private bool _showHelpButton;

        private bool _canSkipFailureReport;

        private bool _hasFormBeenShown;

        private bool _hasAdminBeenPrompted;

        private bool _stopLooking;

        private object _instance;

        private Type _licensedType;

        private LicenseValidationRequestInfo _requestInfo;

        private SupportInfo _supportInfo;

        private string _assemblyName;

        private ValidationRecordCollection _validationRecords = new ValidationRecordCollection();

        private ValidationRecord _latestValidationRecord;

        private SecureLicense _validatingLicense;

        private SecureLicense _formResourceLicense;

        private bool _hasDelayFailed;

        private bool _shouldStopDelayed;

        private int _retry;

        [ThreadStatic]
        private static SecureLicenseContext _resolveContext;

        private StackTrace _stackTrace;

        internal static SecureLicenseContext _current;

        private static string _diagnosticPath;

        private static TextWriter _diagnostic;

        private static TextWriter _diagnosticLog;

        private Hashtable _htmlRenderers;

        private IntPtr[] ptrs = new IntPtr[10];

        private SuperForm _superForm;

        private Thread _reportThread;

        private Hashtable _delayedLimits;

        private ArrayList _asyncRequests = new ArrayList();

        private SecureLicenseCollection _checkedLicenses = new SecureLicenseCollection();

        private int _delayParsePeriod;

        private DateTime _lastFailed;

        private long _deactivationTicks;

        internal Dictionary<Limit, string> _deactivationCodes = new Dictionary<Limit, string>();

        public string ContextId
        {
            get
            {
                return _contextId;
            }
        }

        public override LicenseUsageMode UsageMode
        {
            get
            {
                return _original.UsageMode;
            }
        }

        public bool IsWebRequest
        {
            get
            {
                if (_isWebRequest == 0)
                {
                    _isWebRequest = (SafeToolbox.IsWebRequest ? 1 : 2);
                }
                return _isWebRequest == 1;
            }
        }

        public bool IsWebServiceRequest
        {
            get
            {
                return _instance is WebService;
            }
        }

        public bool IsServiceRequest
        {
            get
            {
                if (_isServiceRequest == 0)
                {
                    _isServiceRequest = (SafeToolbox.IsServiceRequest ? 1 : 2);
                }
                return _isServiceRequest == 1;
            }
        }

        public bool IsConsoleRequest
        {
            get
            {
                if (_isConsoleRequest == 0)
                {
                    _isConsoleRequest = (Toolbox.IsConsoleRequest ? 1 : 2);
                }
                return _isConsoleRequest == 1;
            }
        }

        public bool CanShowWindowsForm
        {
            get
            {
                if (!IsServiceRequest && !IsWebRequest)
                {
                    return !RequestInfo.DontShowForms;
                }
                return false;
            }
        }

        public Hashtable Items
        {
            get
            {
                return _items;
            }
        }

        internal bool ShowHelpButton
        {
            get
            {
                return _showHelpButton;
            }
            set
            {
                _showHelpButton = value;
            }
        }

        public bool CanSkipFailureReport
        {
            get
            {
                return _canSkipFailureReport;
            }
            set
            {
                _canSkipFailureReport = value;
            }
        }

        public bool HasFormBeenShown
        {
            get
            {
                return _hasFormBeenShown;
            }
            set
            {
                _hasFormBeenShown |= value;
            }
        }

        public bool HasAdminBeenPrompted
        {
            get
            {
                return _hasAdminBeenPrompted;
            }
            set
            {
                _hasAdminBeenPrompted |= value;
            }
        }

        public bool StopLooking
        {
            get
            {
                return _stopLooking;
            }
            set
            {
                _stopLooking = value;
            }
        }

        public object Instance
        {
            get
            {
                return _instance;
            }
        }

        public Type LicensedType
        {
            get
            {
                return _licensedType;
            }
        }

        public LicenseValidationRequestInfo RequestInfo
        {
            get
            {
                if (_requestInfo == null)
                {
                    _requestInfo = new LicenseValidationRequestInfo();
                    _requestInfo.MakeReadOnly();
                }
                return _requestInfo;
            }
        }

        public SupportInfo SupportInfo
        {
            get
            {
                if (_supportInfo == null)
                {
                    _supportInfo = RequestInfo.SupportInfo;
                    if (_supportInfo == null)
                    {
                        _supportInfo = LicenseHelpAttribute.GetSupportInfo(_licensedType);
                        RequestInfo.SupportInfo = _supportInfo;
                    }
                    else
                    {
                        SupportInfo supportInfo = LicenseHelpAttribute.GetSupportInfo(_licensedType);
                        _supportInfo.MergeInfo(supportInfo);
                    }
                }
                return _supportInfo;
            }
        }

        internal string AssemblyName
        {
            get
            {
                if (_assemblyName == null && _licensedType != null)
                {
                    _assemblyName = _licensedType.Assembly.GetName().Name;
                }
                return _assemblyName;
            }
        }

        public ValidationRecordCollection ValidationRecords
        {
            get
            {
                return _validationRecords;
            }
        }

        public ValidationRecord LatestValidationRecord
        {
            get
            {
                return _latestValidationRecord;
            }
        }

        public SecureLicense ValidatingLicense
        {
            get
            {
                return _validatingLicense;
            }
        }

        public SecureLicense FormResourceLicense
        {
            get
            {
                return _validatingLicense ?? _formResourceLicense;
            }
        }

        public bool IsDelayValidating
        {
            get
            {
                if (_delayedLimits != null)
                {
                    return _delayedLimits.Count > 0;
                }
                return false;
            }
        }

        public bool HasDelayFailed
        {
            get
            {
                return _hasDelayFailed;
            }
        }

        internal bool HasOutstandingAsyncRequests
        {
            get
            {
                if (_asyncRequests != null)
                {
                    return _asyncRequests.Count > 0;
                }
                return false;
            }
        }

        internal bool IsReportingFailure
        {
            get
            {
                if (_reportThread != null)
                {
                    return _reportThread.IsAlive;
                }
                return false;
            }
        }

        public bool ShouldStopDelayed
        {
            get
            {
                return _shouldStopDelayed;
            }
            set
            {
                if (!_shouldStopDelayed)
                {
                    _shouldStopDelayed = value;
                }
            }
        }

        public int Retry
        {
            get
            {
                return _retry;
            }
            set
            {
                _retry = value;
            }
        }

        public static SecureLicenseContext ResolveContext
        {
            get
            {
                return _resolveContext;
            }
            set
            {
                lock (SecureLicenseManager.SyncRoot)
                {
                    _resolveContext = value;
                }
            }
        }

        public DateTime CurrentDateAndTime
        {
            get
            {
                DateTime testDate = RequestInfo.TestDate;
                if (testDate == DateTime.MinValue)
                {
                    return DateTime.UtcNow;
                }
                if (testDate < DateTime.UtcNow)
                {
                    throw new SecureLicenseException("E_TestDateEarlierThanSystem");
                }
                return testDate;
            }
        }

        public bool IsSuperFormVisible
        {
            get
            {
                if (_superForm != null)
                {
                    return !_superForm.IsDisposed;
                }
                return false;
            }
        }

        public StackTrace StackTrace
        {
            get
            {
                return _stackTrace;
            }
        }

        internal static SecureLicenseContext Current
        {
            get
            {
                return _current;
            }
        }

        internal SecureLicenseContext(SecureLicense license, SecureLicenseContext template)
        {
            if (Check.CheckCalledByThisAssembly())
            {
                _original = LicenseManager.CurrentContext;
                if (template == null)
                {
                    template = SecureLicenseManager.CurrentContext;
                }
                if (template == null)
                {
                    _licensedType = typeof(SecureLicense);
                    _instance = license;
                }
                else
                {
                    _licensedType = template._licensedType;
                    _instance = template._instance;
                }
                _validatingLicense = license;
            }
        }

        internal SecureLicenseContext(LicenseContext original, Type licensedType, object instance, LicenseValidationRequestInfo requestInfo, StackTrace stackTrace)
        {
            Check.CheckCalledByThisAssembly();
            _original = original;
            _licensedType = licensedType;
            _instance = instance;
            _requestInfo = requestInfo;
            _stackTrace = stackTrace;
            if (_licensedType == null && instance != null)
            {
                _licensedType = instance.GetType();
            }
            if (_licensedType != null)
            {
                LicenseKeyAttribute licenseKeyAttribute = Attribute.GetCustomAttribute(_licensedType.Assembly, typeof(LicenseKeyAttribute)) as LicenseKeyAttribute;
                string text = (requestInfo == null) ? null : requestInfo.LicenseKey;
                if (text == null)
                {
                    if (licenseKeyAttribute == null)
                    {
                        ObfuscationAttribute obfuscationAttribute = Attribute.GetCustomAttribute(_licensedType.Assembly, typeof(ObfuscationAttribute)) as ObfuscationAttribute;
                        if (obfuscationAttribute != null)
                        {
                            text = obfuscationAttribute.Feature;
                        }
                    }
                    else
                    {
                        text = licenseKeyAttribute.Data;
                    }
                }
                if (text != null)
                {
                    byte[] array = SharedToolbox.Scramble(SharedToolbox.StringToByte(text, "7$,lsj*0mkyL._JV3b{Y;^HO~nSK8W@?[w`9F%RP(!qiC52DA&/4v:p)ZcU-6T|Med§GN=g'hoE}+]zQBft¶ax#rI1X"));
                    int i;
                    for (i = 0; i < HeaderSignature.Length; i++)
                    {
                        if (array[i] != HeaderSignature[i])
                        {
                            return;
                        }
                    }
                    int num = BitConverter.ToInt32(array, i);
                    i += 4;
                    Version v = new Version(num >> 16, num & 0xFFFF);
                    if (!(v != CurrentVersion))
                    {
                        byte[] array2 = new byte[128];
                        ReadData(array, array2, ref i);
                        byte[] array4 = default(byte[]);
                        using (SymmetricAlgorithm symmetricAlgorithm = SafeToolbox.GetAes(false))
                        {
                            byte[] array3 = new byte[16];
                            Buffer.BlockCopy(array2, 0, array3, 0, array3.Length);
                            symmetricAlgorithm.IV = array3;
                            array3 = new byte[32];
                            Buffer.BlockCopy(array2, symmetricAlgorithm.IV.Length, array3, 0, array3.Length);
                            symmetricAlgorithm.Key = array3;
                            array3 = new byte[32];
                            ReadData(array, array3, ref i);
                            array4 = symmetricAlgorithm.CreateDecryptor().TransformFinalBlock(array3, 0, array3.Length);
                        }
                        byte[] array5 = new byte[32];
                        Buffer.BlockCopy(Images.ProtectedSmall_png_, 128, array5, 0, array5.Length);
                        byte[] array6 = new byte[BitConverter.ToInt32(array, i)];
                        i += 4;
                        ReadData(array, array6, ref i);
                        byte[] array7 = null;
                        using (MemoryStream stream = new MemoryStream(Images.BigError_png_))
                        {
                            using (Bitmap bitmap = Image.FromStream(stream) as Bitmap)
                            {
                                BitmapData bitmapData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
                                byte[] array8 = new byte[bitmapData.Stride * bitmapData.Height];
                                Marshal.Copy(bitmapData.Scan0, array8, 0, array8.Length);
                                byte[] array9 = new byte[2];
                                int dx = ReadArray(array9, array8, 0);
                                array7 = new byte[BitConverter.ToInt16(array9, 0)];
                                ReadArray(array7, array8, dx);
                                bitmap.UnlockBits(bitmapData);
                            }
                        }
                        RSACryptoServiceProvider provider = GetProvider(array7, false);
                        try
                        {
                            if (!VerifyHash(provider, array6, array2))
                            {
                                Array.Clear(array6, 0, array6.Length);
                                Array.Clear(array7, 0, array7.Length);
                                throw new SecureLicenseException("E_InvalidPublicKey");
                            }
                        }
                        finally
                        {
                            try
                            {
                                ((IDisposable)provider).Dispose();
                            }
                            catch (Exception exception)
                            {
                                if (_requestInfo != null && _requestInfo.DeveloperMode)
                                {
                                    ReportError("E_UnexpectedValidate", this, exception, ErrorSeverity.High);
                                }
                            }
                        }
                        using (SymmetricAlgorithm symmetricAlgorithm2 = SafeToolbox.GetAes(false))
                        {
                            symmetricAlgorithm2.IV = array4;
                            symmetricAlgorithm2.Key = array5;
                            Array.Clear(array4, 0, array4.Length);
                            Array.Clear(array5, 0, array5.Length);
                            array6 = symmetricAlgorithm2.CreateDecryptor().TransformFinalBlock(array6, 0, array6.Length);
                        }
                        byte[] array10 = new byte[BitConverter.ToInt16(array6, 0)];
                        i = array10.Length + 2;
                        DESCryptoServiceProvider dESCryptoServiceProvider = new DESCryptoServiceProvider();
                        try
                        {
                            byte[] array11 = new byte[8];
                            ReadData(array6, array11, ref i);
                            dESCryptoServiceProvider.Key = array11;
                            array11 = new byte[8];
                            ReadData(array6, array11, ref i);
                            dESCryptoServiceProvider.IV = array11;
                            array10 = dESCryptoServiceProvider.CreateDecryptor().TransformFinalBlock(array6, 2, array10.Length);
                            Array.Clear(array, 0, array.Length);
                            Array.Clear(array6, 0, array6.Length);
                            Array.Reverse(array10, 0, array10.Length);
                            i = 32;
                            _keyId = new byte[16];
                            ReadData(array10, _keyId, ref i);
                            _license = new byte[32];
                            ReadData(array10, _license, ref i);
                            _serial = new byte[32];
                            ReadData(array10, _serial, ref i);
                            _activation = new byte[32];
                            ReadData(array10, _activation, ref i);
                            i++;
                            int num3 = array10[i++] << 3;
                            _signature = new byte[num3];
                            ReadData(array10, _signature, ref i);
                            Array.Clear(array10, 0, array10.Length);
                            RNGCryptoServiceProvider rNGCryptoServiceProvider = new RNGCryptoServiceProvider();
                            _sessionKey = new byte[32];
                            rNGCryptoServiceProvider.GetBytes(_sessionKey);
                            using (SymmetricAlgorithm symmetricAlgorithm3 = SafeToolbox.GetAes(false))
                            {
                                symmetricAlgorithm3.IV = new byte[16];
                                symmetricAlgorithm3.Key = _sessionKey;
                                byte[] license = _license;
                                _license = symmetricAlgorithm3.CreateEncryptor().TransformFinalBlock(_license, 0, _license.Length);
                                Array.Clear(license, 0, license.Length);
                                license = _serial;
                                _serial = symmetricAlgorithm3.CreateEncryptor().TransformFinalBlock(_serial, 0, _serial.Length);
                                Array.Clear(license, 0, license.Length);
                                license = _activation;
                                _activation = symmetricAlgorithm3.CreateEncryptor().TransformFinalBlock(_activation, 0, _activation.Length);
                                Array.Clear(license, 0, license.Length);
                                license = _signature;
                                _signature = symmetricAlgorithm3.CreateEncryptor().TransformFinalBlock(_signature, 0, _signature.Length);
                                Array.Clear(license, 0, license.Length);
                                _pke = symmetricAlgorithm3.CreateEncryptor().TransformFinalBlock(array7, 0, array7.Length);
                                Array.Clear(array7, 0, array7.Length);
                            }
                        }
                        finally
                        {
                            dESCryptoServiceProvider.Clear();
                        }
                        goto IL_06b4;
                    }
                    return;
                }
                throw new SecureLicenseException("E_MissingPublicKey", null, ErrorSeverity.High, _licensedType.Assembly);
            }
            goto IL_06b4;
            IL_06b4:
            if (CanShowWindowsForm)
            {
                try
                {
                    _uiControl = new System.Windows.Forms.Control();
                    _uiControl.CreateControl();
                }
                catch
                {
                }
            }
        }

        private static void ReadData(byte[] keys, byte[] dest, ref int index)
        {
            Buffer.BlockCopy(keys, index, dest, 0, dest.Length);
            index += dest.Length;
        }

        private static byte ReadByte(byte[] data, ref int dx)
        {
            byte b = 0;
            int num = 0;
            while (num < 8)
            {
                if (dx % 4 == 3)
                {
                    dx++;
                }
                b = (byte)(b | (byte)((byte)(data[dx] & 1) << num));
                num++;
                dx++;
            }
            return b;
        }

        private static int ReadArray(byte[] hidden, byte[] data, int dx)
        {
            for (int i = 0; i < hidden.Length; i++)
            {
                hidden[i] = ReadByte(data, ref dx);
            }
            return dx;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                _diagnostic.Write(Environment.NewLine);
                _diagnostic.Write(Environment.NewLine);
                _diagnostic.Write(Environment.NewLine);
                _diagnostic.WriteLine("Validation Result");
                _diagnostic.WriteLine("================================================================================");
                _diagnostic.WriteLine(Toolbox.MakeDiagnosticReportString(null, _validationRecords, this));
                //MessageBoxEx messageBoxEx = new MessageBoxEx("M_Diagnostics", string.Format("The diagnostic log has been written to {0}.", _diagnosticPath), "Diagnostics Saved");
                //messageBoxEx.SetIcon(MessageBoxIcon.Asterisk);
                //messageBoxEx.MinWidth = 300;
                //Button button = messageBoxEx.AddButton("Open Log", DialogResult.None);
                //button.Click += btn_Click;
                //messageBoxEx.AddButton("#UI_OK", DialogResult.OK, true, false);
                //messageBoxEx.ShowDialog();
                _checkedLicenses.Clear();
                if (_superForm != null && !_superForm.IsDisposed)
                {
                    _superForm.Dispose();
                }
                foreach (object item in _toDispose)
                {
                    SecureLicense secureLicense = item as SecureLicense;
                    if (secureLicense != null)
                    {
                        secureLicense.ResetRuntimeState();
                        if (secureLicense != _selectedLicense)
                        {
                            secureLicense.Dispose();
                        }
                    }
                    else
                    {
                        LicenseFile licenseFile = item as LicenseFile;
                        if (licenseFile != null && (_selectedLicense == null || _selectedLicense.LicenseFile != licenseFile))
                        {
                            foreach (SecureLicense item2 in (IEnumerable)licenseFile.Licenses)
                            {
                                item2.ResetRuntimeState();
                            }
                            licenseFile.Dispose();
                        }
                    }
                }
            }
        }

        private void btn_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start("notepad.exe", _diagnosticPath);
            }
            catch (Exception ex)
            {
                MessageBoxEx.ShowException(ex);
            }
        }

        ~SecureLicenseContext()
        {
            Dispose(false);
        }

        public void WriteDiagnostic(string message, params object[] args)
        {
#if DEBUG
            WriteDiagnosticToContext(message, args);
#endif
        }

        public string GetDiagnostics()
        {
#if DEBUG
			if (_diagnostic == null)
			{
				return "";
			}
			_diagnostic.Flush();
			if (_diagnosticLog != null)
			{
				_diagnosticLog.Flush();
			}
			return _diagnostic.ToString();
#else
            return string.Empty;
#endif
        }

        public static void WriteDiagnosticToContext(string message, params object[] args)
        {
#if DEBUG
			WriteDiagnosticToContext(true, false, message, args);
#endif
        }

        public static void WriteDiagnosticToContext(bool stackTrace, string message, params object[] args)
        {
#if DEBUG
			WriteDiagnosticToContext(true, stackTrace, message, args);
#endif
        }

        private static void WriteDiagnosticToContext(bool internalCall, bool stackTrace, string message, params object[] args)
        {
#if DEBUG
			if (_diagnostic == null)
			{
				_diagnostic = new StringWriter();
				WriteDiagnosticHeader(_diagnostic);
				try
				{
					_diagnosticPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory), "DeployLX Licensing Diagnostics.log");
					try
					{
						_diagnosticLog = new StreamWriter(_diagnosticPath);
					}
					catch (Exception ex)
					{
						WriteDiagnosticMessage(_diagnostic, "Could not create log on desktop, trying temp folder. {0}", ex.ToString());
						_diagnosticPath = Path.Combine(Path.GetTempPath(), string.Format("DeployLX Diagnostic Log {0}.log", DateTime.UtcNow.Ticks));
						_diagnosticLog = new StreamWriter(_diagnosticPath);
					}
					WriteDiagnosticMessage(_diagnostic, "Diagnostics logged to {0}", _diagnosticPath);
					WriteDiagnosticHeader(_diagnosticLog);
				}
				catch (Exception ex2)
				{
					WriteDiagnosticMessage(_diagnostic, "Could not create log in temp folder, in memory log only. {0}", ex2.ToString());
				}
			}
			StackTrace stackTrace2 = new StackTrace(2, true);
			string tab = "";
			if (!message.StartsWith("#"))
			{
				tab = new string(' ', stackTrace2.FrameCount * 2);
			}
			WriteDiagnosticMessage(_diagnostic, tab, message, args);
			if (_diagnosticLog != null)
			{
				WriteDiagnosticMessage(_diagnosticLog, tab, message, args);
			}
			if (stackTrace)
			{
				_diagnostic.WriteLine(SecureLicenseException.FormatStackTrace(stackTrace2, "Diagnostic Stack Trace"));
				if (_diagnosticLog != null)
				{
					_diagnosticLog.WriteLine(SecureLicenseException.FormatStackTrace(stackTrace2, "Diagnostic Stack Trace"));
				}
				_diagnostic.Flush();
				if (_diagnosticLog != null)
				{
					_diagnosticLog.Flush();
				}
			}
#endif
        }

        private static void WriteDiagnosticHeader(TextWriter writer)
        {
#if DEBUG
			writer.WriteLine("Validation log started @ {0}", DateTime.Now);
			writer.WriteLine("================================================================================\r\n");
			writer.WriteLine(Toolbox.MakeSystemInfoString());
			writer.WriteLine("================================================================================\r\n");
			writer.WriteLine(Toolbox.MakeAssemblyListString());
			writer.WriteLine("================================================================================\r\n");
#endif
        }

        private static void WriteDiagnosticMessage(TextWriter writer, string tab, string message, params object[] args)
        {
#if DEBUG
			writer.Write(DateTime.Now.ToString("HH:mm:ss: "));
			writer.Write(tab);
			writer.WriteLine(message, args);
			writer.Flush();
#endif
        }

        public override string GetSavedLicenseKey(Type type, Assembly resourceAssembly)
        {
            return _original.GetSavedLicenseKey(type, resourceAssembly);
        }

        public override void SetSavedLicenseKey(Type type, string key)
        {
            _original.SetSavedLicenseKey(type, key);
        }

        public override object GetService(Type serviceType)
        {
            return _original.GetService(serviceType);
        }

        internal void Close()
        {
            foreach (SecureLicense item in (IEnumerable)_checkedLicenses)
            {
                try
                {
                    if (EnsureLicenseState(item))
                    {
                        byte[] innerXmlAsString = GetInnerXmlAsString(item);
                        WriteDiagnostic(Encoding.UTF8.GetString(innerXmlAsString));
                    }
                }
                catch (Exception ex)
                {
                    WriteDiagnostic("Could not dump license: {0}: {1}", item.Type, ex.Message);
                }
            }
            if (!IsDelayValidating && !IsReportingFailure && !HasOutstandingAsyncRequests)
            {
                Dispose();
            }
            else
            {
                HideSuperForm();
            }
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public static SecureLicenseContext CreateResolveContext(SecureLicense license, SecureLicenseContext template)
        {
            SecureLicenseContext secureLicenseContext = new SecureLicenseContext(license, template);
            secureLicenseContext.ShowHelpButton = true;
            return secureLicenseContext;
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public static SecureLicenseContext CreateResolveContext(SecureLicense license)
        {
            return CreateResolveContext(license, null);
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public static SecureLicenseContext CreateSurrogate(object instance)
        {
            return CreateSurrogate(instance, null);
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public static SecureLicenseContext CreateSurrogate(object instance, string licenseKey)
        {
            Check.NotNull(instance, "instance");
            Type licensedType = (instance as Type) ?? instance.GetType();
            LicenseValidationRequestInfo licenseValidationRequestInfo = new LicenseValidationRequestInfo();
            licenseValidationRequestInfo.DeveloperMode = true;
            licenseValidationRequestInfo.DontShowDeveloperModeWarning = true;
            licenseValidationRequestInfo.LicenseKey = licenseKey;
            LicenseValidationRequestInfo requestInfo = licenseValidationRequestInfo;
            return new SecureLicenseContext(LicenseManager.CurrentContext, licensedType, (instance is Type) ? null : instance, requestInfo, new StackTrace(1, false));
        }

        public Page GetWebPage()
        {
            if (_page != null)
            {
                return _page;
            }
            if (!Toolbox.IisIsAvailable)
            {
                return null;
            }
            HttpContext httpContext = null;
            Page page = null;
            try
            {
                if (_instance is System.Web.UI.Control)
                {
                    page = (_instance as Page);
                    if (page != null)
                    {
                        return page;
                    }
                    page = ((System.Web.UI.Control)_instance).Page;
                    if (page != null)
                    {
                        return page;
                    }
                }
                httpContext = HttpContext.Current;
                if (httpContext != null)
                {
                    page = (httpContext.Handler as Page);
                }
                return page;
            }
            finally
            {
                _page = page;
            }
        }

        public bool RenderHtml(string html, string htmlId, System.Web.UI.Control control, HtmlLocation location, bool exclusive)
        {
            if (_htmlRenderers == null)
            {
                _htmlRenderers = new Hashtable();
            }
            Check.NotNull(html, "html");
            Check.NotNull(htmlId, "htmlId");
            HtmlRender htmlRender = _htmlRenderers[htmlId] as HtmlRender;
            if (htmlRender == null)
            {
                htmlRender = new HtmlRender();
                _htmlRenderers[htmlId] = htmlRender;
            }
            htmlRender._instance = control;
            htmlRender._html = html;
            htmlRender._htmlId = htmlId;
            htmlRender._location = location;
            htmlRender._context = this;
            htmlRender._validLicense = _validatingLicense;
            htmlRender._exclusive = exclusive;
            Page page = control as Page;
            if (page == null)
            {
                page = GetWebPage();
            }
            if (page == null)
            {
                ReportError("E_CouldNotGetPage", this, null, ErrorSeverity.Low);
                return false;
            }
            htmlRender._page = page;
            page.Init += htmlRender.control_Init;
            page.Load += htmlRender.control_Init;
            page.PreRender += htmlRender.control_PreRender;
            return true;
        }

        public System.Web.UI.Control FindWebControl()
        {
            System.Web.UI.Control control = _instance as System.Web.UI.Control;
            if (control == null)
            {
                Page webPage = GetWebPage();
                if (webPage != null)
                {
                    return webPage;
                }
            }
            return control;
        }

        public HttpContext GetHttpContext()
        {
            if (_httpContext != null)
            {
                return _httpContext;
            }
            if (!Toolbox.IisIsAvailable)
            {
                return null;
            }
            HttpContext httpContext = null;
            try
            {
                if (_instance is System.Web.UI.Control)
                {
                    Page page = _instance as Page;
                    int num = 0;
                    while (true)
                    {
                        if (num >= 2)
                        {
                            break;
                        }
                        if (page != null)
                        {
                            try
                            {
                                httpContext = (typeof(Page).GetProperty("Context", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetProperty).GetValue(page, null) as HttpContext);
                            }
                            catch
                            {
                            }
                            if (httpContext == null)
                            {
                                goto IL_006a;
                            }
                            return httpContext;
                        }
                        goto IL_006a;
                        IL_006a:
                        page = ((System.Web.UI.Control)_instance).Page;
                        num++;
                    }
                }
                else if (_instance is WebService)
                {
                    try
                    {
                        httpContext = (typeof(WebService).GetProperty("Context", BindingFlags.Instance | BindingFlags.Public | BindingFlags.GetProperty).GetValue(_instance, null) as HttpContext);
                    }
                    catch
                    {
                    }
                    if (httpContext != null)
                    {
                        return httpContext;
                    }
                }
                httpContext = HttpContext.Current;
                return httpContext;
            }
            finally
            {
                _httpContext = httpContext;
            }
        }

        internal void DisposeIfNotUsed(SecureLicense license)
        {
            if (!_toDispose.Contains(license))
            {
                _toDispose.Add(license);
            }
        }

        internal void DisposeIfNotUsed(LicenseFile LicenseFile)
        {
            if (!_toDispose.Contains(LicenseFile))
            {
                _toDispose.Add(LicenseFile);
            }
        }

        internal void SetValidLicense(SecureLicense license)
        {
            if (Check.CheckCalledByThisAssembly() && (license == null || !license.IsTemplate))
            {
                _selectedLicense = license;
                SetValidatingLicense(license);
            }
            else
            {
                _selectedLicense = null;
                _validatingLicense = null;
            }
        }

        internal void SetValidatingLicense(SecureLicense license)
        {
            if (Check.CheckCalledByThisAssembly() && (license == null || !license.IsTemplate))
            {
                if (license != null)
                {
                    WriteDiagnostic("Trying new license: {0}", license);
                }
                else if (_validatingLicense != null)
                {
                    WriteDiagnostic("\tDone validating license: {0}.", _validatingLicense);
                }
                _retryFile = null;
                _retryLicense = null;
                _validatingRecord = null;
                _validatingLicense = license;
                if (license != null)
                {
                    _checkedLicenses.Add(license);
                    EnsureLicenseState(license);
                    ((IChange)license).MakeReadOnly();
                    if (_lastTheme == null)
                    {
                        _lastTheme = license.LicenseFile.SuperFormTheme;
                    }
                }
            }
            else
            {
                _validatingLicense = null;
            }
        }

        internal bool CheckAddresses()
        {
            if (ptrs[0] == IntPtr.Zero)
            {
                ptrs[0] = SafeNativeMethods.GetModuleHandle("kernel32");
                ptrs[1] = SafeNativeMethods.GetModuleHandle("ntdll");
                if (ptrs[0] == IntPtr.Zero)
                {
                    return false;
                }
                IntPtr module = ptrs[0];
                ptrs[2] = SafeNativeMethods.GetProcAddress(module, "CreateFileW");
                ptrs[3] = SafeNativeMethods.GetProcAddress(module, "IsDebuggerAttached");
            }
            int num = 2;
            while (true)
            {
                if (num < ptrs.Length)
                {
                    if (ptrs[num] != IntPtr.Zero)
                    {
                        byte b = Marshal.ReadByte(ptrs[num]);
                        if (b == 204)
                        {
                            break;
                        }
                    }
                    num++;
                    continue;
                }
                return true;
            }
            return false;
        }

        public object CreateCustomPanelOrForm(ISuperFormLimit limit, string pageId)
        {
            IMultipleSuperFormLimit multipleSuperFormLimit = limit as IMultipleSuperFormLimit;
            string text = null;
            text = ((multipleSuperFormLimit == null) ? limit.CustomForm : ((StringDictionary)multipleSuperFormLimit.CustomForms)[pageId]);
            if (text == null)
            {
                return null;
            }
            Type type = TypeHelper.FindType(text, true);
            if (type == null)
            {
                return null;
            }
            if (!typeof(ICustomForm).IsAssignableFrom(type))
            {
                return Activator.CreateInstance(type, limit as Limit);
            }
            return Activator.CreateInstance(type, null);
        }

        public void ShowForm(ISuperFormLimit formLimit, string pageId, PanelResultHandler resultCallback, bool skipPopOnSuccess)
        {
            if (!CanShowWindowsForm)
            {
                if (resultCallback != null)
                {
                    resultCallback(FormResult.NotShown);
                }
            }
            else
            {
                if (pageId == null)
                {
                    pageId = "DEFAULT";
                }
                FormResult formResult = FormResult.NotShown;
                SuperFormPanel superFormPanel = null;
                ICustomForm customForm = null;
                Limit limit = formLimit as Limit;
                EnsurePeeked(limit);
                IMultipleSuperFormLimit multipleSuperFormLimit = limit as IMultipleSuperFormLimit;
                if (multipleSuperFormLimit != null)
                {
                    object obj = CreateCustomPanelOrForm(multipleSuperFormLimit, pageId);
                    superFormPanel = (obj as SuperFormPanel);
                    if (superFormPanel == null)
                    {
                        customForm = (obj as ICustomForm);
                    }
                }
                if (superFormPanel == null && customForm == null)
                {
                    if (formLimit.CustomForm != null && pageId == "DEFAULT")
                    {
                        Type type = TypeHelper.FindType(formLimit.CustomForm, true);
                        if (typeof(SuperFormPanel).IsAssignableFrom(type))
                        {
                            superFormPanel = (Activator.CreateInstance(type) as SuperFormPanel);
                        }
                        else
                        {
                            customForm = (Activator.CreateInstance(type) as ICustomForm);
                        }
                    }
                    else
                    {
                        superFormPanel = formLimit.CreatePanel(pageId, _superForm.Context);
                    }
                }
                if (superFormPanel == null)
                {
                    if (customForm != null)
                    {
                        _formResourceLicense = limit.License;
                        formResult = customForm.Show(_superForm.Context, limit, pageId);
                        if (resultCallback != null)
                        {
                            resultCallback(formResult);
                        }
                        return;
                    }
                    throw new SecureLicenseException("E_InvalidPageId", limit.Name, pageId);
                }
                SuperForm sf = InitializeSuperForm();
                ShowPanel(superFormPanel, skipPopOnSuccess, new PanelResultHandlerWrapper(formLimit, resultCallback).HandleResult);
                if (!sf.Visible)
                {
                    if (_uiControl != null && _uiControl.InvokeRequired)
                    {
                        _uiControl.Invoke((MethodInvoker)delegate
                        {
                            sf.ShowDialog();
                        });
                    }
                    else
                    {
                        sf.ShowDialog();
                    }
                }
            }
        }

        private void EnsurePeeked(Limit limit)
        {
            limit.Peek(this);
            foreach (Limit item in (IEnumerable)limit.Limits)
            {
                EnsurePeeked(item);
            }
        }

        internal void ShowPanel(SuperFormPanel panel, bool skipPopOnSuccess, PanelResultHandler resultCallback)
        {
            SuperForm superForm = InitializeSuperForm();
            if (panel.Limit != null)
            {
                _formResourceLicense = panel.Limit.License;
            }
            lock (this)
            {
                if (ValidatingLicense != null)
                {
                    superForm.Theme = FormResourceLicense.LicenseFile.SuperFormTheme;
                    _lastTheme = superForm.Theme;
                }
                if (superForm.ActivePanel != null)
                {
                    superForm.ActivePanel._callBack = resultCallback;
                    superForm.ActivePanel._skipPopOnSuccess = skipPopOnSuccess;
                }
                superForm.ShowPanel(panel, true);
            }
        }

        public FormResult ShowForm(ISuperFormLimit formLimit, string pageId)
        {
            if (formLimit == null)
            {
                throw new ArgumentNullException("formLimit");
            }
            if (!CanShowWindowsForm)
            {
                return FormResult.NotShown;
            }
            FormResult formResult = FormResult.NotShown;
            Limit limit = formLimit as Limit;
            try
            {
                SuperForm superForm = InitializeSuperForm();
                bool visible = superForm.Visible;
                FormResult callbackResult = FormResult.Unknown;
                ShowForm(formLimit, pageId, delegate (FormResult r)
                {
                    callbackResult = r;
                }, false);
                formResult = ((!visible) ? superForm.FormResult : superForm.GetResult());
                if (callbackResult != 0)
                {
                    formResult = callbackResult;
                }
                return formResult;
            }
            finally
            {
                if (formResult == FormResult.Success || limit.License.HardFailureIfFormShown)
                {
                    formLimit.FormShown = true;
                }
                switch (formResult)
                {
                    case FormResult.Retry:
                        break;
                    case FormResult.Incomplete:
                        ReportError("E_AbortedByUser", limit, pageId);
                        break;
                    case FormResult.NotShown:
                    case FormResult.Closed:
                        ReportError("E_FormClosed", limit, pageId);
                        break;
                }
            }
        }

        [Obsolete("Use ShowForm instead.", true)]
        public FormResult ShowLimit(ISuperFormLimit limit, string pageId)
        {
            return ShowForm(limit, pageId);
        }

        private SuperForm InitializeSuperForm()
        {
            lock (this)
            {
                if (_superForm != null)
                {
                    if (_superForm.IsDisposed)
                    {
                        CreateSuperForm();
                        return _superForm;
                    }
                }
                else
                {
                    CreateSuperForm();
                }
                return _superForm;
            }
        }

        private void CreateSuperForm()
        {
            if (_uiControl != null && _uiControl.InvokeRequired)
            {
                _uiControl.Invoke(new MethodInvoker(CreateSuperForm));
            }
            else
            {
                _superForm = new SuperForm(this);
                _superForm.CreateControl();
            }
        }

        public void ShowErrors(string title, string subTitle, string message, ValidationRecordCollection records)
        {
            if (CanShowWindowsForm)
            {
                SuperForm superForm = InitializeSuperForm();
                try
                {
                    superForm.Theme = _lastTheme;
                    superForm.ShowPanel(new ErrorReportPanel(title, subTitle, message, records), false);
                    superForm.ShowDialog();
                }
                catch (Exception exception)
                {
                    if (_requestInfo.DeveloperMode)
                    {
                        ReportError("E_UnexpectedValidate", this, exception, ErrorSeverity.High);
                    }
                }
            }
        }

        internal void HideSuperForm()
        {
            lock (SecureLicenseManager.SyncRoot)
            {
                lock (this)
                {
                    if (_superForm != null && !_superForm.IsDisposed)
                    {
                        _superForm.Dispose();
                        _superForm = null;
                    }
                }
            }
        }

        internal FormResult ShowPanel(SuperFormPanel panel)
        {
            if (!CanShowWindowsForm)
            {
                return FormResult.NotShown;
            }
            SuperForm superForm = InitializeSuperForm();
            superForm.Theme = _lastTheme;
            superForm.ShowPanel(panel, true);
            if (!superForm.Visible)
            {
                superForm.ShowDialog();
            }
            return superForm.GetResult();
        }

        public DialogResult ShowDialog(Form dialog)
        {
            if (!CanShowWindowsForm)
            {
                return DialogResult.None;
            }
            lock (this)
            {
                if (_superForm != null && !_superForm.IsDisposed && _superForm.Visible)
                {
                    return _superForm.ShowDialogWithEffects(dialog);
                }
                return dialog.ShowDialog();
            }
        }

        public ValidationResult ReportError(string errorIdOrMessage, object referenceObject, Exception exception, ErrorSeverity severity, params object[] args)
        {
            return ReportError(new ValidationRecord(errorIdOrMessage, referenceObject, exception, severity, args));
        }

        public ValidationResult ReportError(string errorIdOrMessage, object referenceObject, params object[] args)
        {
            return ReportError(errorIdOrMessage, referenceObject, null, ErrorSeverity.Normal, args);
        }

        public ValidationResult ReportError(ValidationRecord record)
        {
            MakeValidatingRecord(record);
            WriteDiagnosticToContext("#VALDATION RECORD: " + record.ToString());
            if (_validatingRecord == null)
            {
                _validationRecords.Add(record);
            }
            else
            {
                _validatingRecord.SubRecords.Add(record);
            }
            _latestValidationRecord = record;
            return ValidationResult.Invalid;
        }

        private void MakeValidatingRecord(ValidationRecord forRecord)
        {
            if (_validatingRecord == null)
            {
                SecureLicense secureLicense = (_validatingLicense == null) ? (forRecord.RelatedObject as SecureLicense) : _validatingLicense;
                if (secureLicense == null && forRecord.RelatedObject is Limit)
                {
                    secureLicense = ((Limit)forRecord.RelatedObject).License;
                }
                if (secureLicense != null)
                {
                    string text = secureLicense.LicenseFile.Location;
                    if (secureLicense.IsEmbedded)
                    {
                        text = _licensedType.Assembly.GetName().Name;
                    }
                    if (secureLicense._isCached && !IsDelayValidating)
                    {
                        _validatingRecord = new ValidationRecord("E_CouldNotValidateCached", secureLicense, null, ErrorSeverity.Low, secureLicense, text);
                    }
                    else
                    {
                        _validatingRecord = new ValidationRecord("E_CouldNotValidate", secureLicense, null, ErrorSeverity.Derived, secureLicense, text);
                    }
                    _validationRecords.Add(_validatingRecord);
                }
            }
        }

        public static ValidationResult ReportErrorOnCurrentContext(string errorIdOrMessage, object referenceObject, Exception exception, ErrorSeverity severity, bool devModeOnly, params object[] args)
        {
            SecureLicenseContext currentContext = SecureLicenseManager.CurrentContext;
            int num = 0;
            while (currentContext == null && num < 10)
            {
                currentContext = SecureLicenseManager.CurrentContext;
                Thread.Sleep(100);
            }
            if (currentContext != null && (!devModeOnly || currentContext.RequestInfo.DeveloperMode))
            {
                currentContext.ReportError(errorIdOrMessage, referenceObject, exception, severity, args);
            }
            return ValidationResult.Invalid;
        }

        public static ValidationResult ReportErrorOnCurrentContext(string errorIdOrMessage, object referenceObject, bool devModeOnly, params object[] args)
        {
            return ReportErrorOnCurrentContext(errorIdOrMessage, referenceObject, null, ErrorSeverity.Normal, devModeOnly, args);
        }

        public static ValidationResult ReportErrorOnCurrentContext(ValidationRecord record, bool devModeOnly)
        {
            lock (SecureLicenseManager.SyncRoot)
            {
                if (SecureLicenseManager.CurrentContext != null && (!devModeOnly || SecureLicenseManager.CurrentContext.RequestInfo.DeveloperMode))
                {
                    SecureLicenseManager.CurrentContext.ReportError(record);
                }
            }
            return ValidationResult.Invalid;
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public void ReportDelayedFailure()
        {
            _hasDelayFailed = true;
        }

        internal void ReportFailuresToServer()
        {
            if (SupportInfo.FailureReportUrl != null)
            {
                _reportThread = new Thread(ReportFailuresToServerInternal);
                _reportThread.Start();
            }
        }

        private void ReportFailuresToServerInternal()
        {
            try
            {
                string value = Toolbox.MakeDiagnosticReportString(SR.GetString("M_FailureReport"), _validationRecords, this);
                LicenseValuesDictionary licenseValuesDictionary = new LicenseValuesDictionary();
                ((StringDictionary)licenseValuesDictionary)["FAILUREREPORT"] = value;
                ((StringDictionary)licenseValuesDictionary)["Version"] = "1.0";
                CallServer("FAILUREREPORT", SupportInfo.FailureReportUrl, null, null, licenseValuesDictionary, true);
            }
            catch
            {
            }
            if (!IsDelayValidating && !HasOutstandingAsyncRequests)
            {
                if (_superForm != null && _superForm.Visible)
                {
                    return;
                }
                Dispose();
            }
        }

        public bool IsRegisteredForDelayedValidation(DelayedLimit limit)
        {
            if (limit == null)
            {
                throw new ArgumentNullException("limit");
            }
            if (_delayedLimits == null)
            {
                return false;
            }
            DelayedValidationContext delayedValidationContext = _delayedLimits[limit.LimitId] as DelayedValidationContext;
            return delayedValidationContext != null;
        }

        internal void RegisterForDelayedValidation(DelayedLimit limit)
        {
            Check.NotNull(limit, "limit");
            if (_delayedLimits == null)
            {
                _delayedLimits = new Hashtable();
            }
            DelayedValidationContext delayedValidationContext = _delayedLimits[limit.LimitId] as DelayedValidationContext;
            if (delayedValidationContext == null)
            {
                delayedValidationContext = new DelayedValidationContext();
                delayedValidationContext._limit = limit;
                delayedValidationContext._context = this;
                delayedValidationContext._thread = new Thread(delayedValidationContext.Go);
                delayedValidationContext._thread.IsBackground = true;
                delayedValidationContext._thread.SetApartmentState(ApartmentState.STA);
                _delayedLimits.Add(limit.LimitId, delayedValidationContext);
                if (limit.License._pendingContexts == null)
                {
                    limit.License._pendingContexts = new ArrayList();
                }
                lock (SecureLicenseManager.SyncRoot)
                {
                    if (!limit.License._pendingContexts.Contains(this))
                    {
                        limit.License._pendingContexts.Add(this);
                    }
                }
                return;
            }
            throw new SecureLicenseException("E_DelayAlreadyRegistered");
        }

        internal void StartDelayedValidation()
        {
            _hasDelayFailed = false;
            if (_delayedLimits != null)
            {
                lock (this)
                {
                    foreach (DelayedValidationContext value in _delayedLimits.Values)
                    {
                        value._thread.Start();
                    }
                }
            }
        }

        internal void StopDelayed(int timeout, ValidationRecord record, DelayedLimit limit)
        {
            if (Check.CheckCalledByThisAssembly())
            {
                Check.NotNull(record, "record");
                if (timeout < -1)
                {
                    throw new ArgumentOutOfRangeException("timeout", timeout, "Must be >= -1.");
                }
                if (_delayedLimits != null && _delayedLimits.Count > 0)
                {
                    lock (this)
                    {
                        _shouldStopDelayed = true;
                    }
                    while (true)
                    {
                        if (timeout != -1)
                        {
                            if (timeout <= 0)
                            {
                                break;
                            }
                            if (_delayedLimits.Count <= 0)
                            {
                                break;
                            }
                        }
                        if (Monitor.TryEnter(SecureLicenseManager.SyncRoot))
                        {
                            try
                            {
                                Monitor.PulseAll(SecureLicenseManager.SyncRoot);
                            }
                            finally
                            {
                                Monitor.Exit(SecureLicenseManager.SyncRoot);
                            }
                        }
                        Thread.Sleep(10);
                        if (timeout != -1)
                        {
                            timeout -= 10;
                            if (timeout <= 0)
                            {
                                break;
                            }
                        }
                    }
                    lock (this)
                    {
                        foreach (DelayedValidationContext value in _delayedLimits.Values)
                        {
                            if (value._limit != limit)
                            {
                                value._thread.Abort();
                            }
                        }
                    }
                    lock (this)
                    {
                        _delayedLimits.Clear();
                    }
                }
            }
        }

        internal void RegisterAsyncRequest(AsyncValidationRequest request)
        {
            _asyncRequests.Add(request);
        }

        internal void CompleteAsyncRequest(AsyncValidationRequest request)
        {
            _asyncRequests.Remove(request);
        }

        internal bool HaveCheckedLicense(SecureLicense license)
        {
            return _checkedLicenses.Contains(license);
        }

        internal void PopulateCheckedLicenses(NoLicenseException ex)
        {
            ex.CheckedLicenses.AddRange(_checkedLicenses);
            ex.CheckedLicenses.MakeReadOnly();
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public VerifySignatureResult VerifySignature(SecureLicense l)
        {
            return VerifySignature((object)l);
        }

        internal VerifySignatureResult VerifySignature(object lic)
        {
            try
            {
                SecureLicense secureLicense = lic as SecureLicense;
                if (secureLicense != null && secureLicense.Signature != null && secureLicense.Signature.Length != 0)
                {
                    new Guid(_keyId);
                    if (!secureLicense.Signature.EndsWith(Convert.ToBase64String(_keyId)))
                    {
                        return VerifySignatureResult.DifferentKeys;
                    }
                    byte[] innerXmlAsString = GetInnerXmlAsString(secureLicense);
                    byte[] signature = Convert.FromBase64String(secureLicense.Signature.Substring(0, secureLicense.Signature.Length - 24));
                    WriteDiagnosticToContext(false, "Validating License Signature: {0}", Encoding.UTF8.GetString(innerXmlAsString));
                    RSACryptoServiceProvider provider = GetProvider(_signature, true);
                    try
                    {
                        return VerifyHash(provider, innerXmlAsString, signature) ? VerifySignatureResult.Valid : VerifySignatureResult.Invalid;
                    }
                    finally
                    {
                        try
                        {
                            ((IDisposable)provider).Dispose();
                        }
                        catch (Exception exception)
                        {
                            if (_requestInfo != null && _requestInfo.DeveloperMode)
                            {
                                ReportError("E_UnexpectedValidate", this, exception, ErrorSeverity.High);
                            }
                        }
                    }
                }
                return VerifySignatureResult.Invalid;
            }
            catch (Exception exception2)
            {
                if (_requestInfo.DeveloperMode)
                {
                    ReportError("E_UnexpectedValidate", this, exception2, ErrorSeverity.High);
                }
                else
                {
                    ReportError("E_CannotValidateSignature", this, exception2, ErrorSeverity.High);
                }
                return VerifySignatureResult.Invalid;
            }
        }

        private RSACryptoServiceProvider GetProvider(byte[] key, bool encrypted)
        {
            try
            {
                if (encrypted)
                {
                    using (SymmetricAlgorithm symmetricAlgorithm = SafeToolbox.GetAes(false))
                    {
                        symmetricAlgorithm.IV = new byte[16];
                        symmetricAlgorithm.Key = _sessionKey;
                        key = symmetricAlgorithm.CreateDecryptor().TransformFinalBlock(key, 0, key.Length);
                    }
                }
                return SharedToolbox.GetProvider(key, this) as RSACryptoServiceProvider;
            }
            finally
            {
                if (encrypted)
                {
                    Array.Clear(key, 0, key.Length);
                }
            }
        }

        private static bool VerifyHash(RSACryptoServiceProvider rsa, byte[] data, byte[] signature)
        {
            using (SHA1CryptoServiceProvider sHA1CryptoServiceProvider = new SHA1CryptoServiceProvider())
            {
                byte[] rgbHash = sHA1CryptoServiceProvider.ComputeHash(data);
                return rsa.VerifyHash(rgbHash, (Environment.OSVersion.Version.Major >= 5) ? "SHA1" : "1.3.14.3.2.26", signature);
            }
        }

        public void VerifySalt(string salt, byte[] result)
        {
            byte[] array = new byte[result.Length - 1];
            Buffer.BlockCopy(result, 1, array, 0, array.Length);
            RSACryptoServiceProvider provider = GetProvider(_signature, true);
            try
            {
                if (!VerifyHash(provider, Encoding.UTF8.GetBytes(salt), array))
                {
                    throw new SecureLicenseException("E_InvalidResponseFromServer");
                }
            }
            finally
            {
                try
                {
                    ((IDisposable)provider).Dispose();
                }
                catch (Exception exception)
                {
                    if (_requestInfo != null && _requestInfo.DeveloperMode)
                    {
                        ReportError("E_UnexpectedValidate", this, exception, ErrorSeverity.High);
                    }
                }
            }
        }

        internal byte[] GetInnerXmlAsString(object lic)
        {
            SecureLicense secureLicense = lic as SecureLicense;
            if (secureLicense == null)
            {
                return null;
            }
            EnsureLicenseState(lic);
            using (MemoryStream memoryStream = new MemoryStream(1024))
            {
                XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, new UTF8Encoding(false));
                xmlTextWriter.WriteStartDocument();
                byte[] array = null;
                try
                {
                    using (SymmetricAlgorithm symmetricAlgorithm = SafeToolbox.GetAes(false))
                    {
                        symmetricAlgorithm.IV = new byte[16];
                        symmetricAlgorithm.Key = _sessionKey;
                        array = symmetricAlgorithm.CreateDecryptor().TransformFinalBlock(_license, 0, _license.Length);
                    }
                    secureLicense.WriteToXml(xmlTextWriter, LicenseSaveType.SignatureCheck, array);
                }
                finally
                {
                    Array.Clear(array, 0, array.Length);
                }
                xmlTextWriter.Close();
                memoryStream.Flush();
                return memoryStream.ToArray();
            }
        }

        internal bool EnsureLicenseState(object lic)
        {
            LicenseFile licenseFile = lic as LicenseFile;
            if (licenseFile != null)
            {
                bool flag = true;
                {
                    foreach (object item in (IEnumerable)licenseFile.Licenses)
                    {
                        flag &= EnsureLicenseState(item);
                    }
                    return flag;
                }
            }
            SecureLicense secureLicense = lic as SecureLicense;
            if (secureLicense != null && _sessionKey != null)
            {
                try
                {
                    if (Check.CheckCalledByThisAssembly() && secureLicense.IsStillEncrypted)
                    {
                        byte[] array = null;
                        try
                        {
                            using (SymmetricAlgorithm symmetricAlgorithm = SafeToolbox.GetAes(false))
                            {
                                symmetricAlgorithm.IV = new byte[16];
                                symmetricAlgorithm.Key = _sessionKey;
                                array = symmetricAlgorithm.CreateDecryptor().TransformFinalBlock(_license, 0, _license.Length);
                            }
                            secureLicense.ReloadWithKey(array);
                        }
                        finally
                        {
                            if (array != null)
                            {
                                Array.Clear(array, 0, array.Length);
                            }
                        }
                    }
                    return !secureLicense.IsStillEncrypted;
                }
                catch (Exception exception)
                {
                    if (_requestInfo != null && _requestInfo.DeveloperMode)
                    {
                        ReportError("E_UnexpectedValidate", this, exception, ErrorSeverity.High);
                    }
                    return false;
                }
            }
            return false;
        }

        internal void EnsureEncryption(object lic)
        {
            SecureLicense secureLicense = lic as SecureLicense;
            if (secureLicense != null && Check.CheckCalledByThisAssembly() && !secureLicense.IsStillEncrypted)
            {
                byte[] array = null;
                try
                {
                    using (SymmetricAlgorithm symmetricAlgorithm = SafeToolbox.GetAes(false))
                    {
                        symmetricAlgorithm.IV = new byte[16];
                        symmetricAlgorithm.Key = _sessionKey;
                        array = symmetricAlgorithm.CreateDecryptor().TransformFinalBlock(_license, 0, _license.Length);
                    }
                    secureLicense.ToXmlString(array);
                }
                finally
                {
                    if (array != null)
                    {
                        Array.Clear(array, 0, array.Length);
                    }
                }
            }
        }

        private byte[] ParseLicenseCodeData(string code, string serialNumber, SerialNumberInfo sni, params CodeAlgorithm[] supportedAlgorithms)
        {
            Check.NotNull(code, "code");
            Check.NotNull(sni, "sni");
            Thread.Sleep(_delayParsePeriod);
            code = code.Trim();
            if (code.Length < 4)
            {
                return null;
            }
            if (serialNumber != null)
            {
                serialNumber = serialNumber.Trim();
                serialNumber = ((serialNumber.Length != 0) ? serialNumber.ToUpper() : null);
            }
            StringBuilder stringBuilder = new StringBuilder(code);
            string prefix = sni.Prefix;
            if (prefix == null || prefix.IndexOf('-') == -1)
            {
                stringBuilder.Replace("-", "");
            }
            WriteDiagnostic("\tExpected prefix: {0}", prefix);
            if (prefix != null && string.Compare(prefix, 0, stringBuilder.ToString(), 0, prefix.Length, true) != 0)
            {
                WriteDiagnostic("\tBad prefix.");
                return null;
            }
            if (prefix != null)
            {
                stringBuilder.Remove(0, prefix.Length);
                if (prefix.IndexOf('-') > -1)
                {
                    stringBuilder.Replace("-", "");
                }
            }
            if (stringBuilder.Length < 3)
            {
                return null;
            }
            char[] array = stringBuilder.ToString().ToCharArray();
            Array.Reverse(array, 0, (int)char.ToLower(array[array.Length - 1]) % (array.Length - 2));
            stringBuilder.Length = 0;
            stringBuilder.Append(array);
            if (supportedAlgorithms != null && supportedAlgorithms.Length == 1 && supportedAlgorithms[0] == CodeAlgorithm.NotSet)
            {
                goto IL_014f;
            }
            if (supportedAlgorithms != null && supportedAlgorithms.Length == 0)
            {
                goto IL_014f;
            }
            goto IL_0152;
            IL_014f:
            supportedAlgorithms = null;
            goto IL_0152;
            IL_0152:
            CodeAlgorithm codeAlgorithm = CodeAlgorithm.NotSet;
            string text = null;
            if (supportedAlgorithms == null || supportedAlgorithms.Length == 0)
            {
                supportedAlgorithms = (Enum.GetValues(typeof(CodeAlgorithm)) as CodeAlgorithm[]);
            }
            CodeAlgorithm[] array2 = supportedAlgorithms;
            int num = 0;
            while (num < array2.Length)
            {
                CodeAlgorithm codeAlgorithm2 = array2[num];
                switch (codeAlgorithm2)
                {
                    case CodeAlgorithm.Simple:
                        text = "0123456789";
                        goto IL_01bb;
                    case CodeAlgorithm.SerialNumber:
                    case CodeAlgorithm.ActivationCode:
                    case CodeAlgorithm.Basic:
                    case CodeAlgorithm.Advanced:
                        text = sni.CharacterSet;
                        goto IL_01bb;
                    default:
                        {
                            num++;
                            continue;
                        }
                        IL_01bb:
                        if (text == null)
                        {
                            text = "U9VWT2FG3Q7RS0AC1DEYMNX6P8HJ4KL5";
                        }
                        if (text.Length >= (int)codeAlgorithm2 && stringBuilder[0] == text[(int)codeAlgorithm2])
                        {
                            break;
                        }
                        goto default;
                }
                codeAlgorithm = codeAlgorithm2;
                break;
            }
            WriteDiagnostic("\tDetermined Algorithm: {0}, {1}", codeAlgorithm, text);
            if (text == null)
            {
                return null;
            }
            stringBuilder.Remove(0, 1);
            if (stringBuilder.Length == 0)
            {
                return null;
            }
            char c = stringBuilder[stringBuilder.Length - 1];
            stringBuilder.Length--;
            if (stringBuilder.Length == 0)
            {
                return null;
            }
            byte[] array3 = SharedToolbox.StringToByte(stringBuilder.ToString(), text);
            if (text[SharedToolbox.Crc16(array3) % (text.Length - 1)] != c)
            {
                WriteDiagnostic("\tFailed CRC check");
                return null;
            }
            byte b = array3[0];
            for (int i = 1; i < array3.Length; i++)
            {
                array3[i] ^= b;
            }
            byte[] array4 = new byte[array3.Length - 1];
            Array.Copy(array3, 1, array4, 0, array4.Length);
            array3 = array4;
            if (prefix != null)
            {
                byte[] bytes = Encoding.UTF8.GetBytes(prefix);
                int num2 = 0;
                for (int j = 0; j < array3.Length; j++)
                {
                    array3[j] ^= bytes[num2++];
                    if (num2 == bytes.Length)
                    {
                        num2 = 0;
                    }
                }
            }
            if (serialNumber != null)
            {
                byte[] bytes2 = Encoding.UTF8.GetBytes(serialNumber);
                int num4 = 0;
                for (int k = 0; k < array3.Length; k++)
                {
                    array3[k] ^= bytes2[num4++];
                    if (num4 == bytes2.Length)
                    {
                        num4 = 0;
                    }
                }
            }
            try
            {
                switch (codeAlgorithm)
                {
                    case CodeAlgorithm.SerialNumber:
                        array3 = SerialNumberParseCode(array3, this);
                        break;
                    case CodeAlgorithm.ActivationCode:
                        array3 = ActivationParseCode(array3, this);
                        break;
                    case CodeAlgorithm.Simple:
                        array3 = SimpleParseCode(array3, this);
                        break;
                    case CodeAlgorithm.Basic:
                        array3 = BasicParseCode(array3, this);
                        break;
                    case CodeAlgorithm.Advanced:
                        array3 = AdvancedParseCode(array3, this);
                        break;
                }
            }
            catch (Exception ex)
            {
                WriteDiagnostic("\tError: {0}", ex.ToString());
                if (_requestInfo.DeveloperMode)
                {
                    ReportError("E_UnexpectedValidate", this, ex, ErrorSeverity.High);
                }
                return null;
            }
            byte b2 = (byte)SharedToolbox.Crc16(array3, 0, array3.Length - 1);
            if (array3[array3.Length - 1] != b2)
            {
                WriteDiagnostic("\tFailed CRC Check 2");
                return null;
            }
            byte[] array5 = new byte[array3.Length - 1];
            Array.Copy(array3, 0, array5, 0, array5.Length);
            WriteDiagnostic("\tParsed!");
            return array5;
        }

        private bool IncDelayParsePeriod()
        {
            if (_delayParsePeriod == 0)
            {
                _delayParsePeriod = 1;
            }
            else if (_delayParsePeriod < 60)
            {
                _delayParsePeriod++;
            }
            else if (_delayParsePeriod < 65535)
            {
                _delayParsePeriod += 100;
            }
            _lastFailed = DateTime.Now;
            return false;
        }

        private bool ClearDelayParsePeriod()
        {
            if ((DateTime.Now - _lastFailed).TotalMinutes > 2.0)
            {
                _delayParsePeriod = 0;
            }
            return true;
        }

        public bool CheckSerialNumber(string serialNumber, SerialNumberInfo sni, out int seed, out byte[] data)
        {
            data = null;
            seed = -1;
            WriteDiagnostic("Checking serial number: {0}", serialNumber);
            if (serialNumber == null)
            {
                return false;
            }
            byte[] array = ParseLicenseCodeData(serialNumber, null, sni, sni.Algorithm);
            if (array == null)
            {
                return IncDelayParsePeriod();
            }
            if (array.Length < 4)
            {
                return IncDelayParsePeriod();
            }
            int num = array[3];
            array[3] = 0;
            if (num != (SharedToolbox.Crc16(array, 0, 3) & 0xFF))
            {
                WriteDiagnostic("\tFailed serial number CRC");
                return IncDelayParsePeriod();
            }
            if (array.Length > 4)
            {
                data = new byte[array.Length - 4];
                Array.Copy(array, 4, data, 0, data.Length);
            }
            seed = BitConverter.ToInt32(array, 0);
            if (seed > 16777215)
            {
                WriteDiagnostic("\tSeed larger than max allowed.");
                return IncDelayParsePeriod();
            }
            if (seed >= sni.MinSeed && seed <= sni.MaxSeed)
            {
                if ((seed - sni.MinSeed) % sni.Step != 0)
                {
                    WriteDiagnostic("\tInvalid seed.");
                    return IncDelayParsePeriod();
                }
                _delayParsePeriod = 0;
                WriteDiagnostic("\tSuccess!");
                return ClearDelayParsePeriod();
            }
            WriteDiagnostic("\tSeed smaller than min allowed.");
            return IncDelayParsePeriod();
        }

        public bool CheckLicenseExtension(string code, IExtendableLimit limit, out int amount, out DateTime expires)
        {
            amount = 0;
            expires = DateTime.MinValue;
            Check.NotNull(code, "code");
            Check.NotNull(limit, "limit");
            Limit limit2 = limit as Limit;
            byte[] array = ParseLicenseCodeData(code, limit2.License.SerialNumber, limit2.License.SerialNumberInfo, limit.CodeAlgorithm);
            if (array == null)
            {
                return IncDelayParsePeriod();
            }
            if (array.Length != 12)
            {
                return IncDelayParsePeriod();
            }
            if (SharedToolbox.GetHashCode(((Limit)limit).LimitId) != BitConverter.ToInt32(array, 0))
            {
                return IncDelayParsePeriod();
            }
            amount = BitConverter.ToInt32(array, 4);
            int num = BitConverter.ToInt32(array, 8);
            DateTime dateTime = new DateTime(2000 + (num & 0x3FF), num >> 10 & 0xF, num >> 14);
            DateTime dateTime2 = dateTime.ToUniversalTime();
            expires = dateTime2.AddTicks(dateTime.Ticks - dateTime2.Ticks);
            return ClearDelayParsePeriod();
        }

        public bool CheckLicenseExtension(string code, IExtendableLimit limit, out int amount)
        {
            DateTime dateTime;
            if (!CheckLicenseExtension(code, limit, out amount, out dateTime))
            {
                ReportErrorOnCurrentContext("E_InvalidExtensionCode", limit, false);
                return false;
            }
            if (dateTime != DateTime.MinValue && dateTime < SecureLicenseManager.CurrentContext.CurrentDateAndTime)
            {
                ReportErrorOnCurrentContext("E_ExpiredExtensionCode", limit, false);
                amount = 0;
                return false;
            }
            return true;
        }

        public string MakeExtensionLimitCode(IExtendableLimit limit)
        {
            byte[] bytes = BitConverter.GetBytes(Math.Max(limit.ExtendableValue, limit.GetCurrentValue()));
            bytes[1] ^= 85;
            bytes[2] ^= 85;
            bytes[3] ^= 85;
            return SharedToolbox.ByteToString(bytes, limit.License.SerialNumberInfo.CharacterSet);
        }

        public bool CheckExpired(IExtendableLimit extendable)
        {
            int num;
            int num2;
            int num3;
            extendable.GetUseRange(this, out num, out num2, out num3);
            if (num3 >= num)
            {
                return num3 > num2;
            }
            return true;
        }

        public bool CheckActivationCode(string code, ActivationLimit limit, ActivationProfile profile, string newHash, out DateTime expires)
        {
            expires = DateTime.MinValue;
            Check.NotNull(code, "code");
            Check.NotNull(limit, "limit");
            Check.NotNull(profile, "profile");
            byte[] array = ParseLicenseCodeData(code, limit.License.SerialNumber, limit.License.SerialNumberInfo, limit.CodeAlgorithm);
            if (array == null)
            {
                array = ParseLicenseCodeData(code, null, limit.License.SerialNumberInfo, limit.CodeAlgorithm);
            }
            if (array != null && array.Length >= 6)
            {
                code = code.ToUpper();
                int referenceId = profile.ReferenceId;
                if (array[2] != (byte)(array[0] ^ 255 - referenceId))
                {
                    return IncDelayParsePeriod();
                }
                if (array[3] != (byte)(array[1] ^ 255 - referenceId))
                {
                    return IncDelayParsePeriod();
                }
                if (array[4] != (byte)referenceId)
                {
                    return IncDelayParsePeriod();
                }
                if (BitConverter.ToUInt16(array, 0) != (ushort)SharedToolbox.Crc16(Encoding.UTF8.GetBytes((newHash == null) ? profile.UnlockHash : newHash)))
                {
                    return IncDelayParsePeriod();
                }
                int num = BitConverter.ToInt32(array, 5);
                DateTime dateTime = new DateTime(2000 + (num & 0x3FF), num >> 10 & 0xF, num >> 14);
                DateTime dateTime2 = dateTime.ToUniversalTime();
                expires = dateTime2.AddTicks(dateTime.Ticks - dateTime2.Ticks);
                return ClearDelayParsePeriod();
            }
            return IncDelayParsePeriod();
        }

        public string MakeDeactivationConfirmationCode(ActivationLimit limit)
        {
            Check.NotNull(limit, "limit");
            int shortHashCode = SharedToolbox.GetShortHashCode(limit.License.SerialNumber);
            long deactivationTicks = _deactivationTicks;
            if (deactivationTicks == 0L)
            {
                _deactivationTicks = SecureLicenseManager.CurrentContext.CurrentDateAndTime.Ticks;
                deactivationTicks = _deactivationTicks;
            }
            int value = -1;
            if (limit.ActiveProfile != null)
            {
                value = SharedToolbox.GetShortHashCode(limit.ActiveProfile.Hash);
            }
            byte[] array = new byte[12];
            Array.Copy(BitConverter.GetBytes(shortHashCode), 0, array, 0, 2);
            Array.Copy(BitConverter.GetBytes(value), 0, array, 2, 2);
            Array.Copy(BitConverter.GetBytes(deactivationTicks), 0, array, 4, 8);
            using (SymmetricAlgorithm symmetricAlgorithm = SafeToolbox.GetAes(false))
            {
                symmetricAlgorithm.IV = new byte[16];
                using (SymmetricAlgorithm symmetricAlgorithm2 = SafeToolbox.GetAes(false))
                {
                    symmetricAlgorithm2.IV = new byte[16];
                    symmetricAlgorithm2.Key = _sessionKey;
                    symmetricAlgorithm.Key = symmetricAlgorithm2.CreateDecryptor().TransformFinalBlock(_activation, 0, _activation.Length);
                }
                array = symmetricAlgorithm.CreateEncryptor().TransformFinalBlock(array, 0, array.Length);
            }
            byte[] array2 = new byte[18]
            {
                63,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0
            };
            Array.Copy(array, 0, array2, 1, array.Length);
            Array.Copy(array, 0, array2, 17, 1);
            StringBuilder stringBuilder = new StringBuilder(SharedToolbox.ByteToString(array2, limit.License.SerialNumberInfo.CharacterSet, -1, 0, array2.Length, true));
            for (int i = 5; i < stringBuilder.Length; i += 6)
            {
                stringBuilder.Insert(i, '-');
            }
            if (limit.License.SerialNumberInfo.Prefix != null && limit.License.SerialNumberInfo.Prefix.IndexOf('-') == -1)
            {
                stringBuilder.Insert(0, '-');
            }
            stringBuilder.Insert(0, limit.License.SerialNumberInfo.Prefix);
            _deactivationCodes[limit] = stringBuilder.ToString();
            return stringBuilder.ToString();
        }

        public ServerResult CallServer(string command, string address, Limit referenceLimit, SecureLicense referenceLicense, LicenseValuesDictionary properties, bool noProxyAutoRetry)
        {
            Check.NotNull(command, "command");
            Check.NotNull(address, "address");
            if (RequestInfo.SimulateOffline)
            {
                ServerResult serverResult = new ServerResult(false, new SecureLicenseException("E_SimulateOffline"), null, false);
                serverResult.ConnectionError = true;
                return serverResult;
            }
            ServerResult serverResult2 = CallServerInternal(command, address, referenceLimit, referenceLicense, properties, noProxyAutoRetry);
            if (!serverResult2.Success)
            {
                IServerLimit serverLimit = referenceLimit as IServerLimit;
                if (serverLimit != null && serverLimit.ServerRetries > 0)
                {
                    for (int i = 0; i < serverLimit.ServerRetries; i++)
                    {
                        Thread.Sleep(1000);
                        ServerResult serverResult3 = CallServerInternal(command, address, referenceLimit, referenceLicense, properties, noProxyAutoRetry);
                        if (serverResult3.Success)
                        {
                            return serverResult3;
                        }
                    }
                    goto IL_00a6;
                }
                return serverResult2;
            }
            goto IL_00a6;
            IL_00a6:
            return serverResult2;
        }

        private ServerResult CallServerInternal(string command, string address, Limit referenceLimit, SecureLicense referenceLicense, LicenseValuesDictionary properties, bool noProxyAutoRetry)
        {
            Uri uri = Toolbox.ResolveUrl(address, this);
            try
            {
                IPAddress[] hostAddresses = Dns.GetHostAddresses(new Uri(address).Host);
                WriteDiagnostic("\tAddress resolved to: {0}", hostAddresses[0]);
            }
            catch
            {
            }
            //LicenseServerProxy licenseServerProxy = new LicenseServerProxy();
            if (properties == null)
            {
                properties = new LicenseValuesDictionary();
            }
            if (((StringDictionary)properties)["MachineProfile"] == null)
            {
                ((StringDictionary)properties)["MachineProfile"] = MachineProfile.Profile.GetComparableHash(false, null);
            }
            else
            {
                ((StringDictionary)properties)["MachineProfile.Absolute"] = MachineProfile.Profile.GetComparableHash(false, null);
            }
            ((StringDictionary)properties)["LicensedType"] = _licensedType.FullName;
            ((StringDictionary)properties)["LicensedAssembly"] = _licensedType.Assembly.FullName;
            ((StringDictionary)properties)["LicensedAssemblyName"] = _licensedType.Assembly.GetName().Name;
            ((StringDictionary)properties)["LicensedAssemblyVersion"] = _licensedType.Assembly.GetName().Version.ToString();
            if (RequestInfo.TestDate != DateTime.MinValue)
            {
                ((StringDictionary)properties)["LicenseTestDate"] = RequestInfo.TestDate.ToString("u");
            }
            AssemblyFileVersionAttribute assemblyFileVersionAttribute = Attribute.GetCustomAttribute(_licensedType.Assembly, typeof(AssemblyFileVersionAttribute)) as AssemblyFileVersionAttribute;
            if (assemblyFileVersionAttribute != null)
            {
                ((StringDictionary)properties)["LicensedAssemblyFileVersion"] = assemblyFileVersionAttribute.Version;
            }
            ((StringDictionary)properties)["IsServiceRequest"] = IsServiceRequest.ToString();
            ((StringDictionary)properties)["IsWebRequest"] = IsWebRequest.ToString();
            foreach (DictionaryEntry additionalServerProperty in RequestInfo.AdditionalServerProperties)
            {
                ((StringDictionary)properties)["Server." + additionalServerProperty.Key] = (additionalServerProperty.Value as string);
            }

            UriBuilder bldr = new UriBuilder(uri)
            {
                Query = "code=D9VdGbGuGcGPcrpjhYQ0ciiKD2NXhhh/7yNJZYpJrYt17VmWFJLgcw==&clientId=default"
            };

            ResettableByteHttpRequest request = new ResettableByteHttpRequest(bldr.Uri.ToString())
            {
                AllowAutoRedirect = true,
                Timeout = Config.ServerConnectTimeout,
                Method = "POST"
            };

            //licenseServerProxy.Url = uri.ToString();
            //licenseServerProxy.Proxy = Config.Proxy;
            //licenseServerProxy.AllowAutoRedirect = true;
            //licenseServerProxy.Timeout = Config.ServerConnectTimeout;
            //ServicePoint servicePoint = ServicePointManager.FindServicePoint(licenseServerProxy.Url, licenseServerProxy.Proxy);
            //servicePoint.Expect100Continue = false;
            string text = Guid.NewGuid().ToString("N");
            if (RequestInfo.DeveloperMode)
            {
                byte[] inArray = SharedToolbox.Scramble(Encoding.UTF8.GetBytes(text));
                ((StringDictionary)properties)["DeveloperMode"] = Convert.ToBase64String(inArray);
                if (request.Timeout == 120000)
                {
                    request.Timeout = 600000;
                }
            }
            bool flag = true;
            bool flag2 = false;
            Exception exception = null;
            LicenseFile licenseFile = null;
            SecureLicense secureLicense = null;
            SecureLicense secureLicense2 = referenceLicense ?? ((referenceLimit == null) ? _validatingLicense : referenceLimit.License);
            if (referenceLicense != null && referenceLimit != null && referenceLicense.Limits.FindLimitById(referenceLimit.LimitId) != referenceLimit)
            {
                throw new SecureLicenseException("E_ReferenceLimitMustBeInReferenceLicense");
            }
            while (true)
            {
                try
                {
                    byte[] licenseXml = null;
                    if (secureLicense2 != null)
                    {
                        foreach (SecureLicense item in (IEnumerable)secureLicense2.LicenseFile.Licenses)
                        {
                            EnsureEncryption(item);
                        }
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            secureLicense2.LicenseFile.Save(memoryStream);
                            licenseXml = memoryStream.ToArray();
                        }
                    }
                    DelayedLimit delayedLimit = referenceLimit as DelayedLimit;
                    byte[] array = null;
                    if (delayedLimit != null && delayedLimit.DelayValidation && !_requestInfo.NoDelayed)
                    {
                        //IAsyncResult asyncResult = licenseServerProxy.BeginDispatch(command, licenseXml, (secureLicense2 == null) ? null : secureLicense2.LicenseId, (referenceLimit == null) ? null : referenceLimit.LimitId, (secureLicense2 == null) ? null : secureLicense2.KeyName, Thread.CurrentThread.CurrentUICulture.Name, properties.ToXmlString(), text, null, null);
                        //WaitHandle[] waitHandles = new WaitHandle[1]
                        //{
                        //    asyncResult.AsyncWaitHandle
                        //};
                        //while (!asyncResult.IsCompleted)
                        //{
                        //    WaitHandle.WaitAny(waitHandles, 100, false);
                        //    if (_shouldStopDelayed)
                        //    {
                        //        return new ServerResult(false, null, null, false);
                        //    }
                        //}
                        //array = licenseServerProxy.EndDispatch(asyncResult);
                    }
                    else
                    {
                        LicenseHttpRequest licenseRequest = new LicenseHttpRequest
                        {
                            Command = command,
                            LicenseXml = licenseXml,
                            LicenseId = (secureLicense2 == null) ? null : secureLicense2.LicenseId,
                            LimitId = (referenceLimit == null) ? null : referenceLimit.LimitId,
                            Key = (secureLicense2 == null) ? null : secureLicense2.KeyName,
                            CurrentCulture = Thread.CurrentThread.CurrentUICulture.ToString(),
                            Properties = properties.ToXmlString(),
                            Salt = text
                        };
                        request.WriteContentToRequest(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(licenseRequest)));

                        array = request.GetResponse().Value;
                    }
                    WriteDiagnostic("Completed server request. Result = {0}", array);
                    if (array != null && array.Length != 0)
                    {
                        if (array[0] == 255)
                        {
                            WriteDiagnostic("Verifying salt.");
                            VerifySalt(text, array);
                        }
                        else
                        {
                            WriteDiagnostic("Parsing returned license.");
                            using (MemoryStream stream = new MemoryStream(array))
                            {
                                licenseFile = new LicenseFile();
                                licenseFile.Load(stream);
                                DisposeIfNotUsed(licenseFile);
                            }
                            if (licenseFile.SaveTo == SaveLocations.OriginalLocation)
                            {
                                licenseFile._location = Path.Combine(Config.SharedFolder, Path.GetFileName(secureLicense2.LicenseFile.Location));
                            }
                            if (licenseFile._location == null)
                            {
                                if (secureLicense2.LicenseFile.Location != null)
                                {
                                    string text2 = secureLicense2.LicenseFile.Location;
                                    if (text2.StartsWith("isolated:"))
                                    {
                                        text2 = text2.Substring(9);
                                    }
                                    licenseFile._location = Path.Combine(Path.GetDirectoryName(text2), secureLicense2.LicenseId + ".lic");
                                }
                                else
                                {
                                    licenseFile._location = SafeToolbox.GetFullPath(secureLicense2.LicenseId + ".lic");
                                }
                            }
                            if (flag = (licenseFile.Licenses.Count > 0))
                            {
                                if (((StringDictionary)licenseFile.MetaValues)["__NEWLICENSE"] == "true")
                                {
                                    flag2 = true;
                                    licenseFile.MetaValues.Remove("__NEWLICENSE");
                                }
                                else if (!(licenseFile.Id != secureLicense2.LicenseFile.Id) && licenseFile.Licenses[secureLicense2.LicenseId] != null)
                                {
                                    secureLicense = licenseFile.Licenses[secureLicense2.LicenseId];
                                    bool flag3 = false;
                                    bool flag4 = false;
                                    string strA = (referenceLimit == null) ? "" : ("__MODIFIEDATSERVER." + referenceLimit.LimitId);
                                    foreach (DictionaryEntry metaValue in secureLicense.MetaValues)
                                    {
                                        string text3 = metaValue.Key as string;
                                        if (text3 != null && string.Compare(text3, 0, "__MODIFIEDATSERVER", 0, 16, true) == 0)
                                        {
                                            flag3 = true;
                                            if (string.Compare(strA, text3, true) == 0)
                                            {
                                                flag4 = true;
                                                continue;
                                            }
                                            flag2 = true;
                                            break;
                                        }
                                    }
                                    flag2 |= (flag3 && !flag4);
                                    WriteDiagnostic("Modified license. retry = {0}, modified = {1}, found = {2}", flag2, flag3, flag4);
                                }
                                else
                                {
                                    flag2 = true;
                                }
                                if (secureLicense != null)
                                {
                                    secureLicense.MetaValues.RemoveWithPrefix("__MODIFIEDATSERVER");
                                }
                            }
                        }
                    }
                    else
                    {
                        flag = false;
                    }
                    if (!flag)
                    {
                        exception = new SecureLicenseException("E_InvalidResponseFromServer");
                    }
                }
                catch (ThreadAbortException)
                {
                    flag = false;
                }
                //This condition doesnt happen.
                catch (SoapException ex2)
                {
                    //if (HandleProxyError(ex2, noProxyAutoRetry))//, licenseServerProxy))
                    //{
                    //    continue;
                    //}
                    exception = ((!RequestInfo.DeveloperMode) ? new SecureLicenseException(ValidationRecord.PreprocessException(ex2, this)) : new SecureLicenseException(ValidationRecord.PreprocessException(ex2, this), ex2));
                    flag = false;
                }
                catch (WebException ex3)
                {
                    //if (HandleProxyError(ex3, noProxyAutoRetry))//, licenseServerProxy))
                    //{
                    //    continue;
                    //}
                    exception = ((!RequestInfo.DeveloperMode) ? new SecureLicenseException(ValidationRecord.PreprocessException(ex3, this)) : new SecureLicenseException(ValidationRecord.PreprocessException(ex3, this), ex3));

                    //exception = ((!RequestInfo.DeveloperMode) ? new SecureLicenseException(ex3.Message, ex3, ErrorSeverity.Normal) : new SecureLicenseException(ValidationRecord.PreprocessException(ex3, this), ex3, ErrorSeverity.Normal));
                    flag = false;
                }
                catch (Exception ex4)
                {
                    //if (HandleProxyError(ex4, noProxyAutoRetry))//, licenseServerProxy))
                    //{
                    //    continue;
                    //}
                    exception = ex4;
                    flag = false;
                }
                break;
            }
            if (flag2)
            {
                if (licenseFile != null)
                {
                    licenseFile.SaveOnValid(this, true);
                }
                RetryWith(licenseFile, (secureLicense == null) ? null : secureLicense.LicenseId);
            }
            if (secureLicense != null && ((StringDictionary)secureLicense.MetaValues)["__SUCCESSMESSAGE"] != null)
            {
                string text4 = ((StringDictionary)secureLicense.MetaValues)["__SUCCESSMESSAGE"];
                secureLicense.MetaValues.Remove("__SUCCESSMESSAGE");
                try
                {
                    if (IsWebRequest)
                    {
                        Page webPage = GetWebPage();
                        if (webPage != null)
                        {
                            webPage.RegisterClientScriptBlock(secureLicense.LicenseId + ".Message", text4);
                        }
                    }
                    else if (!IsServiceRequest)
                    {
                        MessageBoxEx dialog = new MessageBoxEx(null, text4, "#UI_ValidationNoticeTitle", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1, MessageBoxExOptions.SupportButtons, null);
                        ShowDialog(dialog);
                    }
                }
                catch (Exception)
                {
                }
            }
            if (!flag2 && licenseFile != null)
            {
                foreach (SecureLicense item2 in (IEnumerable)licenseFile.Licenses)
                {
                    EnsureLicenseState(item2);
                }
            }
            return new ServerResult(flag, exception, secureLicense, flag2);
        }

        private bool HandleProxyError(Exception ex, bool noProxyAutoRetry)//, LicenseServerProxy server)
        {
            if (!CanShowWindowsForm)
            {
                return false;
            }
            if (!noProxyAutoRetry && Toolbox.IsProxyException(ex, this))
            {
                try
                {
                    //using (ProxyForm dialog = new ProxyForm(new Uri(server.Url), "#UI_ProblemContectingToServerProxy"))
                    //{
                    //    if (ShowDialog(dialog) == DialogResult.OK)
                    //    {
                    //        server.Proxy = Config.Proxy;
                    //        return true;
                    //    }
                    //}
                }
                catch (Exception ex2)
                {
                    MessageBoxEx.ShowException(ex2);
                }
            }
            return false;
        }

        public ValidationResult RetryWith(LicenseFile licenseFile, string licenseId)
        {
            Check.NotNull(licenseFile, "license");
            ReportError("E_ModifiedOrUnlocked", _validatingLicense, null, ErrorSeverity.Low);
            _retryFile = licenseFile;
            if (licenseId != null)
            {
                _retryLicense = licenseFile.Licenses[licenseId];
            }
            else
            {
                _retryLicense = null;
            }
            foreach (SecureLicense item in (IEnumerable)licenseFile.Licenses)
            {
                _checkedLicenses.Remove(item);
                if (_retryLicense == null)
                {
                    Limit[] array = item.Limits.FindLimitsByType(typeof(ISuperFormLimit), true, false);
                    for (int i = 0; i < array.Length; i++)
                    {
                        ISuperFormLimit superFormLimit = (ISuperFormLimit)array[i];
                        superFormLimit.FormShown = false;
                    }
                }
            }
            _retryFile.SaveOnValid(this, false);
            return ValidationResult.Retry;
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public ValidationResult TestValidate(SecureLicense license)
        {
            if (license == null)
            {
                throw new ArgumentNullException("license");
            }
            if (license.LicenseFile == null)
            {
                LicenseFile licenseFile = new LicenseFile();
                licenseFile.Licenses.Add(license);
            }
            if (license.Signature == null)
            {
                license.Signature = "****TESTING****WILLNOTVALIDATE****";
            }
            SetValidatingLicense(license);
            _current = this;
            ValidationResult validationResult = license.Validate(this);
            if (validationResult != ValidationResult.Valid)
            {
                return validationResult;
            }
            return license.Limits.Granted(this);
        }

        public string GetPotentialSerialFromClipboard()
        {
            try
            {
                foreach (string potentialClipboardLine in GetPotentialClipboardLines())
                {
                    foreach (SecureLicense item in (IEnumerable)ValidatingLicense.LicenseFile.Licenses)
                    {
                        int num;
                        byte[] array;
                        if (!item.IsStillEncrypted && item.CanUnlockBySerial && CheckSerialNumber(potentialClipboardLine, item.SerialNumberInfo, out num, out array))
                        {
                            return potentialClipboardLine;
                        }
                    }
                }
                return null;
            }
            catch (CryptographicException)
            {
                return null;
            }
        }

        public string GetPotentialActivationFromClipboard(ActivationProfile profile, string hash)
        {
            if (profile == null)
            {
                throw new ArgumentNullException("profile");
            }
            if (hash != null && hash.Length != 0)
            {
                try
                {
                    foreach (string potentialClipboardLine in GetPotentialClipboardLines())
                    {
                        DateTime dateTime;
                        if (CheckActivationCode(potentialClipboardLine, profile.Limit, profile, hash, out dateTime))
                        {
                            return potentialClipboardLine;
                        }
                    }
                    return null;
                }
                catch (CryptographicException)
                {
                    return null;
                }
            }
            throw new ArgumentNullException("hash");
        }

        public string GetPotentialExtensionFromClipboard(IExtendableLimit limit)
        {
            if (limit == null)
            {
                throw new ArgumentNullException("limit");
            }
            try
            {
                foreach (string potentialClipboardLine in GetPotentialClipboardLines())
                {
                    int num;
                    if (CheckLicenseExtension(potentialClipboardLine, limit, out num))
                    {
                        return potentialClipboardLine;
                    }
                }
                return null;
            }
            catch (CryptographicException)
            {
                return null;
            }
        }

        private List<string> GetPotentialClipboardLines()
        {
            List<string> list = new List<string>();
            try
            {
                if (ValidatingLicense == null)
                {
                    return list;
                }
                List<string> list2 = new List<string>();
                foreach (SecureLicense item in (IEnumerable)ValidatingLicense.LicenseFile.Licenses)
                {
                    if (!item.IsStillEncrypted && item.SerialNumberInfo.Prefix != null)
                    {
                        list2.Add(item.SerialNumberInfo.Prefix);
                    }
                }
                if (list2.Count == 0)
                {
                    return list;
                }
                string text = ClipboardToolbox.GetClipboard("Text") as string;
                if (text != null && text.Length != 0)
                {
                    string[] array = text.Split(new char[2]
                    {
                        '\r',
                        '\n'
                    }, StringSplitOptions.RemoveEmptyEntries);
                    string[] array2 = array;
                    foreach (string text2 in array2)
                    {
                        string text3 = text2.Trim();
                        foreach (string item2 in list2)
                        {
                            int num = text3.IndexOf(item2);
                            if (num == -1)
                            {
                                continue;
                            }
                            int num2 = text3.IndexOfAny(new char[4]
                            {
                                ' ',
                                '\t',
                                '\'',
                                '"'
                            }, num);
                            if (num2 != -1)
                            {
                                text3 = text3.Substring(num, num2 - num);
                            }
                            else if (num > 0)
                            {
                                text3 = text3.Substring(num);
                            }
                            list.Add(text3);
                            break;
                        }
                    }
                    return list;
                }
                return list;
            }
            catch (ExternalException)
            {
                return list;
            }
        }

        internal void CheckForDeveloperMode()
        {
            byte[] array = null;
            byte[] array2 = null;
            try
            {
                using (new Wow64Guard())
                {
                    RegistryKey registryKey;
                    string text = default(string);
                    using (registryKey = Registry.CurrentUser.OpenSubKey("Software\\XHEO INC\\DeployLX\\v4.0", false))
                    {
                        if (registryKey == null || registryKey.GetValue("DeveloperMode") == null)
                        {
                            registryKey = Registry.LocalMachine.OpenSubKey("Software\\XHEO INC\\DeployLX\\v4.0", false);
                        }
                        if (registryKey != null)
                        {
                            text = (registryKey.GetValue("DeveloperMode") as string);
                            if (text != null)
                            {
                                goto end_IL_001e;
                            }
                        }
                        return;
                        end_IL_001e:;
                    }
                    using (SymmetricAlgorithm symmetricAlgorithm = SafeToolbox.GetAes(false))
                    {
                        symmetricAlgorithm.IV = new byte[16];
                        symmetricAlgorithm.Key = _sessionKey;
                        array = symmetricAlgorithm.CreateDecryptor().TransformFinalBlock(_license, 0, _license.Length);
                    }
                    using (SymmetricAlgorithm symmetricAlgorithm2 = SafeToolbox.GetAes(false))
                    {
                        symmetricAlgorithm2.IV = new byte[16];
                        symmetricAlgorithm2.Key = array;
                        byte[] array3 = SharedToolbox.StringToByte(text, "U9VWT2FG3Q7RS0AC1DEYMNX6P8HJ4KL5");
                        array2 = symmetricAlgorithm2.CreateDecryptor().TransformFinalBlock(array3, 0, array3.Length);
                    }
                    if (array2 != null && array2.Length == 8)
                    {
                        DateTime t = new DateTime(BitConverter.ToInt64(array2, 0), DateTimeKind.Utc);
                        if (t > DateTime.UtcNow)
                        {
                            RequestInfo.EnableDeveloperMode();
                        }
                    }
                }
            }
            catch
            {
            }
            finally
            {
                if (array != null)
                {
                    Array.Clear(array, 0, array.Length);
                }
            }
        }

        private byte[] SimpleParseCode(byte[] code, SecureLicenseContext context)
        {
            Array.Reverse(code);
            Array.Reverse(code, 0, (int)code[code.Length - 1] % (code.Length - 2));
            return code;
        }

        private byte[] BasicParseCode(byte[] data, SecureLicenseContext context)
        {
            int num = 0;
            byte[] array = context._license;
            using (SymmetricAlgorithm symmetricAlgorithm = SafeToolbox.GetAes(false))
            {
                symmetricAlgorithm.IV = new byte[16];
                symmetricAlgorithm.Key = _sessionKey;
                array = symmetricAlgorithm.CreateDecryptor().TransformFinalBlock(_license, 0, _license.Length);
            }
            int num2 = 1;
            for (int i = 0; i < num2; i++)
            {
                for (int j = 0; j < data.Length; j++)
                {
                    data[j] ^= array[num++];
                    if (num == array.Length)
                    {
                        num = 0;
                    }
                }
                if (array.Length % data.Length == 0)
                {
                    num++;
                }
            }
            Array.Clear(array, 0, array.Length);
            return data;
        }

        private byte[] AdvancedParseCode(byte[] code, SecureLicenseContext context)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (SymmetricAlgorithm symmetricAlgorithm2 = SafeToolbox.GetAes(false))
                {
                    using (SymmetricAlgorithm symmetricAlgorithm = SafeToolbox.GetAes(false))
                    {
                        symmetricAlgorithm.IV = new byte[16];
                        symmetricAlgorithm.Key = _sessionKey;
                        symmetricAlgorithm2.Key = symmetricAlgorithm.CreateDecryptor().TransformFinalBlock(_license, 0, _license.Length);
                    }
                    symmetricAlgorithm2.IV = new byte[16];
                    using (CryptoStream cryptoStream = new CryptoStream(memoryStream, symmetricAlgorithm2.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(code, 0, code.Length);
                        cryptoStream.Flush();
                        cryptoStream.FlushFinalBlock();
                        memoryStream.Flush();
                        symmetricAlgorithm2.Clear();
                        cryptoStream.Clear();
                    }
                    return memoryStream.ToArray();
                }
            }
        }

        private byte[] SerialNumberParseCode(byte[] code, SecureLicenseContext context)
        {
            byte[] array = default(byte[]);
            using (SymmetricAlgorithm symmetricAlgorithm = SafeToolbox.GetAes(false))
            {
                symmetricAlgorithm.IV = new byte[16];
                symmetricAlgorithm.Key = _sessionKey;
                array = symmetricAlgorithm.CreateDecryptor().TransformFinalBlock(_serial, 0, _serial.Length);
            }
            for (int i = 0; i < code.Length; i++)
            {
                code[i] ^= array[i % (array.Length - 1)];
            }
            Array.Clear(array, 0, array.Length);
            byte[] array2 = new byte[2];
            byte[] array3 = new byte[code.Length - 3];
            Array.Copy(code, 1, array3, 0, array3.Length);
            int num = 0;
            array2[1] = code[0];
            array2[0] = code[code.Length - 1];
            num = code[code.Length - 2];
            if (num == 255)
            {
                num = array2.Length - 1;
            }
            if (num >= 0 && num <= array2.Length)
            {
                for (int j = 0; j < 10; j++)
                {
                    if (num == -1)
                    {
                        num = array2.Length - 1;
                    }
                    for (int num2 = array3.Length - 1; num2 >= 0; num2--)
                    {
                        array3[num2] ^= array2[num--];
                        if (num == -1)
                        {
                            num = array2.Length - 1;
                        }
                    }
                    if ((code.Length - 3) % array2.Length == 0)
                    {
                        num--;
                    }
                }
                return array3;
            }
            return code;
        }

        private byte[] ActivationParseCode(byte[] code, SecureLicenseContext context)
        {
            using (SymmetricAlgorithm symmetricAlgorithm2 = SafeToolbox.GetAes(false))
            {
                using (SymmetricAlgorithm symmetricAlgorithm = SafeToolbox.GetAes(false))
                {
                    symmetricAlgorithm.IV = new byte[16];
                    symmetricAlgorithm.Key = _sessionKey;
                    symmetricAlgorithm2.Key = symmetricAlgorithm.CreateDecryptor().TransformFinalBlock(_activation, 0, _activation.Length);
                }
                symmetricAlgorithm2.IV = new byte[16];
                return symmetricAlgorithm2.CreateDecryptor().TransformFinalBlock(code, 0, code.Length);
            }
        }
    }
}
