using System;
using System.Collections;
using System.IO;
using System.Text;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	[LimitEditor("TimeLimitEditor", Category = "Extendable Limits", MiniType = "TimeLimitEditorControl", IconResource = "asmres://DeployLX.Licensing.DlxAddIn.v5/DeployLX.Licensing.DlxAddIn.v5.Resources.Limits.Time.png")]
	public class TimeLimit : ExtendableLimit
	{
		private DateTime _started = DateTime.MinValue;

		internal TimeSpan _offset = TimeSpan.Zero;

		[NonSerialized]
		private TimeMonitor _monitor;

		public static readonly DateTime AbsoluteBase = SafeToolbox.FastParseSortableDate("2006-01-01T00:00:00");

		private TimeLimitType _timeLimitType = TimeLimitType.FromFirstUse;

		private string _timeKey;

		private DateTime _lastChecked = DateTime.MinValue;

		private int _tryCount;

		public bool HasStarted
		{
			get
			{
				return _started != DateTime.MinValue;
			}
		}

		public override string Description
		{
			get
			{
				return SR.GetString("M_TimeLimitDescription");
			}
		}

		public override string Name
		{
			get
			{
				return "Time";
			}
		}

		public TimeLimitType TimeLimitType
		{
			get
			{
				return _timeLimitType;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_timeLimitType != value)
				{
					_timeLimitType = value;
					base.OnChanged("TimeLimitType");
				}
			}
		}

		public int Time
		{
			get
			{
				return ((IExtendableLimit)this).ExtendableValue;
			}
			set
			{
				base.AssertNotReadOnly();
				if (value < 0)
				{
					throw new ArgumentOutOfRangeException("Time", value, SR.GetString("E_PropertyAtLeast", "Time", 0));
				}
				if (((IExtendableLimit)this).ExtendableValue != value)
				{
					((IExtendableLimit)this).ExtendableValue = value;
					base.OnChanged("Time");
				}
			}
		}

		[Obsolete("Use MaxTime64 instead.")]
		public int MaxTime
		{
			get
			{
				long maxTime = MaxTime64;
				if (maxTime > 2147483647L)
				{
					throw new SecureLicenseException("E_DateTooFarInFutureUseMaxTime64");
				}
				return (int)maxTime;
			}
		}

		public long MaxTime64
		{
			get
			{
				long num = ((IExtendableLimit)this).ExtendableValue;
				if (_timeLimitType == TimeLimitType.SpecificDate)
				{
					if (Time == 0)
					{
						num = 0L;
					}
					else
					{
						DateTime d = AbsoluteBase.AddHours((double)Time);
						DateTime d2 = (DateTime)GetTime();
						num = (long)(d - d2).TotalSeconds;
						if (num < 0L)
						{
							num = 0L;
						}
					}
				}
				return num;
			}
		}

		public int TimeInDays
		{
			get
			{
				if (_timeLimitType == TimeLimitType.SpecificDate)
				{
					return (int)Math.Floor((double)Time / 1440.0);
				}
				return (int)Math.Floor((double)Time / 86400.0);
			}
			set
			{
				Time = value * 86400;
			}
		}

		public DateTime AbsoluteDate
		{
			get
			{
				if (_timeLimitType != TimeLimitType.SpecificDate)
				{
					throw new SecureLicenseException("E_TimeNotAbsolute");
				}
				return AbsoluteBase.AddHours((double)Time);
			}
			set
			{
				_timeLimitType = TimeLimitType.SpecificDate;
				Time = (int)(value.ToUniversalTime() - AbsoluteBase).TotalHours;
			}
		}

		public override string SummaryText
		{
			get
			{
				string arg = (_timeLimitType != TimeLimitType.SpecificDate) ? SafeToolbox.FormatTime(Time, Time, true) : AbsoluteDate.ToString("MMMM d, yyyy");
				return string.Format("{0}, {1}", arg, _timeLimitType);
			}
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
					if (base.License != null && _lastChecked != DateTime.MinValue)
					{
						GetRunningTime();
					}
					if (_monitor != null)
					{
						_monitor.Stop();
						_monitor = null;
					}
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public TimeMonitor GetTimeMonitor()
		{
			if (_monitor == null)
			{
				_monitor = (base.License.RuntimeState[base.LimitId + ".Monitor"] as TimeMonitor);
				if (_monitor == null)
				{
					_monitor = new TimeMonitor(this);
					base.License.RuntimeState[base.LimitId + ".Monitor"] = _monitor;
					SecureLicenseContext current = SecureLicenseContext.Current;
					if (current != null)
					{
						Peek(current);
					}
				}
				else
				{
					_monitor.Connect(this);
				}
			}
			return _monitor;
		}

		protected override bool ReadFromXml(XmlReader reader)
		{
			if (!base.ReadFromXml(reader))
			{
				return false;
			}
			string attribute = reader.GetAttribute("timeLimitType");
			if (attribute == null)
			{
				return false;
			}
			_timeLimitType = (TimeLimitType)SafeToolbox.FastParseInt32(attribute);
			if (reader.IsEmptyElement)
			{
				reader.Read();
				return true;
			}
			reader.Read();
			base.Servers.VerifyValues = false;
			while (!reader.EOF)
			{
				if (reader.IsStartElement())
				{
					switch (reader.Name)
					{
					case "Limit":
						if (base.ReadChildLimit(reader))
						{
							break;
						}
						return false;
					case "Server":
						attribute = reader.GetAttribute("address");
						if (attribute != null)
						{
							base.Servers.Add(SafeToolbox.XmlDecode(attribute));
							reader.Read();
							break;
						}
						return false;
					default:
						reader.Skip();
						break;
					}
					continue;
				}
				reader.Read();
				break;
			}
			base.Servers.VerifyValues = false;
			return true;
		}

		protected override bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			if (!base.WriteToXml(writer, signing))
			{
				return false;
			}
			int timeLimitType = (int)_timeLimitType;
			writer.WriteAttributeString("timeLimitType", timeLimitType.ToString());
			foreach (string item in (IEnumerable)base.Servers)
			{
				writer.WriteStartElement("Server");
				writer.WriteAttributeString("address", SafeToolbox.XmlEncode(item));
				writer.WriteEndElement();
			}
			return true;
		}

		public override ValidationResult Validate(SecureLicenseContext context)
		{
			base.ExtendWithRequestExtensionCodes(context);
			base.License.RuntimeState[base.UniqueId + ".DCC"] = (context.SupportInfo.DontCheckClock || base.License.Limits[typeof(TimeServerLimit)] != null);
			if (context.RequestInfo.TestDate != DateTime.MinValue)
			{
				_offset = context.CurrentDateAndTime - DateTime.UtcNow;
			}
			if (!IsTimeValid(context, false))
			{
				ValidationResult validationResult = base.TryExtend(context);
				switch (validationResult)
				{
				default:
					context.ReportError("E_TimeExpired", this);
					goto case ValidationResult.Retry;
				case ValidationResult.Retry:
					return validationResult;
				case ValidationResult.Valid:
					break;
				}
				if (!IsTimeValid(context, false))
				{
					return context.ReportError("E_TimeExpired", this);
				}
			}
			return base.Validate(context);
		}

		public override ValidationResult Granted(SecureLicenseContext context)
		{
			if (!IsTimeValid(context, true))
			{
				return ValidationResult.Invalid;
			}
			return base.Granted(context);
		}

		private void GetTimeKey()
		{
			if (_timeKey == null)
			{
				_timeKey = "RunningTime-" + (int)_timeLimitType;
			}
		}

		protected bool IsTimeValid(SecureLicenseContext context, bool reportError)
		{
			if (context.RequestInfo.TestDate != DateTime.MinValue)
			{
				_offset = context.CurrentDateAndTime - DateTime.UtcNow;
			}
			if (_started == DateTime.MinValue)
			{
				_started = context.CurrentDateAndTime;
			}
			long maxTime = MaxTime64;
			DateTime dateTime = (DateTime)GetTime();
			context.WriteDiagnostic("IsTimeValid maxTime = {0}, now = {1}", maxTime, dateTime);
			if (_timeLimitType == TimeLimitType.SpecificDate)
			{
				context.WriteDiagnostic("Specific Date, maxTime > 0 {0}", maxTime > 0L);
				if (maxTime > 0L)
				{
					return true;
				}
				context.ReportError("E_TimeExpired", this);
				return false;
			}
			long runningTime = GetRunningTime();
			context.WriteDiagnostic("runningTime = {0}, runningTime >= maxTime ({1}) {2} ", runningTime, maxTime, runningTime >= maxTime);
			if (runningTime >= maxTime)
			{
				if (reportError)
				{
					context.ReportError("E_TimeExpired", this);
				}
				return false;
			}
			return true;
		}

		protected override int GetCurrentValue()
		{
			return (int)Math.Min(GetRunningTime(), 2147483647L);
		}

		public override void Reset(SecureLicenseContext context, int value)
		{
			if (!Check.CalledByType(typeof(ResetLimit)) && !Check.CalledByThisType())
			{
				return;
			}
			base.Reset(context, value);
			base.SetPersistentData(_timeKey, null);
			_started = context.CurrentDateAndTime;
			_lastChecked = context.CurrentDateAndTime;
			GetTimeKey();
			base.SetPersistentData(_timeKey, null);
			GetTimeMonitor().Connect(this);
			GetRunningTime();
		}

		internal long GetRunningTime()
		{
			if (_started == DateTime.MinValue)
			{
				throw new SecureLicenseException("E_TimeNotValidated");
			}
			GetTimeKey();
			lock (SecureLicenseManager.AsyncLock)
			{
				if (_lastChecked == DateTime.MinValue)
				{
					_lastChecked = _started;
				}
				if (!CheckLastRun())
				{
					SecureLicenseContext.ReportErrorOnCurrentContext("E_InvalidClock", this, null, ErrorSeverity.High, true);
					return MaxTime64;
				}
				UpdateLastRun();
				DateTime dateTime = (DateTime)GetTime();
				if (dateTime == DateTime.MaxValue)
				{
					return MaxTime64;
				}
				SecureLicenseContext.WriteDiagnosticToContext("TimeLimitType = {0}", _timeLimitType);
				switch (_timeLimitType)
				{
				case TimeLimitType.AtATime:
					return (long)(dateTime - _started).TotalSeconds;
				case TimeLimitType.Cumulative:
				{
					object persistentData2 = base.GetPersistentData(_timeKey);
					long num = (long)Math.Ceiling((dateTime - _lastChecked).TotalMilliseconds);
					_lastChecked = (DateTime)GetTime();
					if (persistentData2 != null)
					{
						num += (long)persistentData2;
					}
					base.SetPersistentData(_timeKey, num);
					return (long)Math.Floor((double)num / 1000.0);
				}
				case TimeLimitType.FromFirstUse:
				case TimeLimitType.SpecificDate:
				{
					object persistentData = base.GetPersistentData(_timeKey);
					SecureLicenseContext.WriteDiagnosticToContext("GetRunningTime -> GetPersistentData = {0}", persistentData);
					DateTime dateTime2;
					if (persistentData == null)
					{
						dateTime2 = _started;
						base.SetPersistentData(_timeKey, dateTime2.ToFileTimeUtc());
					}
					else
					{
						dateTime2 = DateTime.FromFileTimeUtc((long)persistentData);
					}
					SecureLicenseContext.WriteDiagnosticToContext("startDate = {0}, now = {1}", dateTime2, dateTime);
					if (dateTime2 > dateTime)
					{
						SecureLicenseContext resolveContext = SecureLicenseContext.ResolveContext;
						if (resolveContext != null && resolveContext.RequestInfo != null && resolveContext.RequestInfo.DeveloperMode)
						{
							return 0L;
						}
						return MaxTime64;
					}
					return (long)(dateTime - dateTime2).TotalSeconds;
				}
				}
			}
			return 2147483647L;
		}

		private object GetTime()
		{
			DateTime dateTime = DateTime.UtcNow;
			if (_offset != TimeSpan.Zero)
			{
				dateTime += _offset;
			}
			DateTime dateTime2 = DateTime.MinValue;
			DateTime dateTime3 = DateTime.MinValue;
			if (_tryCount % 5 == 0)
			{
				_tryCount++;
				try
				{
					string[] logicalDrives = Environment.GetLogicalDrives();
					string[] array = logicalDrives;
					int num = 0;
					while (true)
					{
						if (num >= array.Length)
						{
							break;
						}
						string text = array[num];
						if (SafeNativeMethods.GetDriveType(text) == 3)
						{
							string path = Path.Combine(text, "page" + "file" + "." + "sys");
							if (File.Exists(path))
							{
								try
								{
									using (File.OpenWrite(path))
									{
									}
								}
								catch
								{
									DateTime lastWriteTimeUtc = File.GetLastWriteTimeUtc(path);
									if (lastWriteTimeUtc > dateTime2)
									{
										dateTime2 = lastWriteTimeUtc;
									}
								}
							}
						}
						num++;
					}
				}
				catch
				{
				}
				try
				{
					StringBuilder stringBuilder = new StringBuilder(256);
					if (SafeNativeMethods.SHGetFolderPath(IntPtr.Zero, 40, IntPtr.Zero, 0, stringBuilder) == 0)
					{
						stringBuilder.Append('\\');
						stringBuilder.Append("NT");
						stringBuilder.Append("user");
						stringBuilder.Append(".");
						stringBuilder.Append("dat");
						string path = stringBuilder.ToString();
						try
						{
							using (File.OpenWrite(path))
							{
							}
						}
						catch
						{
							dateTime3 = File.GetLastWriteTimeUtc(path);
						}
					}
				}
				catch
				{
				}
				if (dateTime2 < dateTime3)
				{
					dateTime2 = dateTime3;
				}
				if (dateTime < dateTime2 && (dateTime2 - dateTime).TotalHours > 2.0)
				{
					SecureLicenseContext.ReportErrorOnCurrentContext("E_InvalidClock", this, new SecureLicenseException("Clock has been modified."), ErrorSeverity.High, true);
					base.License.Signature = null;
				}
			}
			return dateTime;
		}

		private bool CheckLastRun()
		{
			object obj = base.License.RuntimeState[base.UniqueId + ".DCC"];
			if (obj != null && (bool)obj)
			{
				return true;
			}
			object persistentData = base.GetPersistentData("LastRun" + base.License.SerialNumber, null, PersistentDataLocationType.SharedOrUser);
			if (persistentData != null)
			{
				DateTime d = DateTime.FromFileTimeUtc((long)persistentData);
				TimeSpan timeSpan = d - DateTime.UtcNow;
				long num = (MaxTime64 <= 86400L) ? (MaxTime64 / 20L) : 7200L;
				if (timeSpan.TotalSeconds > (double)num)
				{
					return false;
				}
			}
			return true;
		}

		private void UpdateLastRun()
		{
			object obj = base.License.RuntimeState[base.UniqueId + ".DCC"];
			if (obj != null && (bool)obj)
			{
				return;
			}
			base.SetPersistentData("LastRun" + base.License.SerialNumber, DateTime.UtcNow.ToFileTimeUtc(), PersistentDataLocationType.SharedOrUser);
		}

		protected override bool CheckLimitReached(int maxAmount)
		{
			if (_timeLimitType == TimeLimitType.SpecificDate)
			{
				return false;
			}
			return base.CheckLimitReached(maxAmount);
		}

		public override PeekResult Peek(SecureLicenseContext context)
		{
			if (IsTimeValid(context, false))
			{
				return PeekResult.Valid;
			}
			if (!base.CanExtend)
			{
				return PeekResult.Invalid;
			}
			return PeekResult.NeedsUser;
		}

		public override string GetUseDescription(SecureLicenseContext context)
		{
			long maxTime = MaxTime64;
			long runningTime = GetRunningTime();
			if (_timeLimitType == TimeLimitType.SpecificDate)
			{
				return SR.GetString("M_TimeRemaining", SafeToolbox.FormatTime(maxTime, maxTime, false), SafeToolbox.FormatTime(maxTime + runningTime, maxTime + runningTime, true));
			}
			return SR.GetString("M_TimeRemaining", SafeToolbox.FormatTime(Math.Max(0L, maxTime - runningTime), maxTime, false), SafeToolbox.FormatTime(maxTime, maxTime, true));
		}

		public override void GetUseRange(SecureLicenseContext context, out int min, out int max, out int current)
		{
			long val = 0L;
			long num = MaxTime64;
			long runningTime = GetRunningTime();
			long val2;
			if (IsTimeValid(context, false))
			{
				if (_timeLimitType == TimeLimitType.SpecificDate)
				{
					num += runningTime;
				}
				val2 = Math.Min(runningTime, num);
			}
			else
			{
				val2 = num;
			}
			current = (int)Math.Min(val2, 2147483647L);
			max = (int)Math.Min(num, 2147483647L);
			min = (int)Math.Min(val, 2147483647L);
		}
	}
}
