using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	public class SplashPanel : SuperFormPanel
	{
		private Timer _continueTimer;

		private int _continueDelay;

		private Button _continue;

		private IContainer components;

		public SplashPanel(SplashLimit limit)
			: base(limit)
		{
			InitializeComponent();
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			base.SuspendLayout();
			base.Name = "SplashPanel";
			base.DockPadding.All = 0;
			base.ResumeLayout(false);
		}

		protected internal override void LoadPanel(SecureLicenseContext context)
		{
			base.LoadPanel(context);
			SplashLimit splashLimit = base.Limit as SplashLimit;
			Image image = Toolbox.GetImage(splashLimit.SplashResource, context);
			if (image != null)
			{
				BufferedPictureBox bufferedPictureBox = new BufferedPictureBox();
				bufferedPictureBox.SetBounds(0, 0, base.Width, base.Height);
				bufferedPictureBox.Image = image;
				bufferedPictureBox.BackColor = Color.Transparent;
				bufferedPictureBox.SizeMode = PictureBoxSizeMode.CenterImage;
				base.Controls.Add(bufferedPictureBox);
			}
			else
			{
				ShadowLabel shadowLabel = new ShadowLabel();
				shadowLabel.TextAlign = ContentAlignment.MiddleCenter;
				shadowLabel.Text = SR.GetString("E_CouldNotGetResource", splashLimit.SplashResource);
				shadowLabel.Dock = DockStyle.Fill;
				shadowLabel.BackColor = Color.Transparent;
				base.Controls.Add(shadowLabel);
			}
		}

		protected internal override void InitializePanel()
		{
			_continue = base.AddBottomButton("#UI_Continue");
			_continue.Click += _continue_Click;
			base._superForm.AcceptButton = _continue;
			base._superForm.CancelButton = _continue;
			SplashLimit splashLimit = base.Limit as SplashLimit;
			if (splashLimit.ContinueDelay > 0)
			{
				if (_continueTimer == null)
				{
					_continueTimer = new Timer();
					_continueTimer.Tick += _continueTimer_Tick;
				}
				_continueTimer.Interval = 1000;
				_continueDelay = splashLimit.ContinueDelay;
				_continueTimer.Start();
				_continue.Enabled = false;
				base.CanClose = false;
			}
		}

		private void _continue_Click(object sender, EventArgs e)
		{
			base.Return(FormResult.Success);
		}

		private void _continueTimer_Tick(object sender, EventArgs e)
		{
			_continueDelay--;
			if (_continueDelay > 0)
			{
				_continue.Text = string.Format("{0} ({1})", SR.GetString("UI_Continue"), _continueDelay);
			}
			else
			{
				_continueTimer.Stop();
				_continue.Enabled = true;
				_continue.Text = SR.GetString("UI_Continue");
				base.CanClose = true;
			}
		}
	}
}
