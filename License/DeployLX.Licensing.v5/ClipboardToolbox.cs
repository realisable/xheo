﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
    class ClipboardToolbox
    {
		public static void SetClipboard(string format, object data)
		{
			SetClipboardHelper setClipboardHelper = new SetClipboardHelper(format, data);
			setClipboardHelper.Go();
		}

		public static object GetClipboard(string format)
		{
			Check.NotNull(format, "format");
			GetClipboardHelper getClipboardHelper = new GetClipboardHelper(format);
			getClipboardHelper.GoAndWait();
			return getClipboardHelper._data;
		}

		private class SetClipboardHelper : StaHelper
		{
			private string _format;

			private object _data;

			public SetClipboardHelper(string format, object data)
			{
				_format = format;
				_data = data;
			}

			protected override void Work()
			{
                IDataObject data = new DataObject(_format, _data);
                Clipboard.SetDataObject(data, true);
            }
		}

		private class GetClipboardHelper : StaHelper
		{
			private string _format;

			public object _data;

			public GetClipboardHelper(string format)
			{
				_format = format;
			}

			protected override void Work()
			{
                IDataObject dataObject = Clipboard.GetDataObject();
                if (dataObject != null)
                {
                    _data = dataObject.GetData(_format, true);
                }
            }
		}
	}
}
