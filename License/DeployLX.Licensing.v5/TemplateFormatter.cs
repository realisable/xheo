using System;
using System.Text;

namespace DeployLX.Licensing.v5
{
	public sealed class TemplateFormatter
	{
		private TemplateFormatter()
		{
		}

		public static string Format(string template, string[] variableNames, string[] values)
		{
			if (template == null)
			{
				return null;
			}
			if (template.IndexOf('_') > -1)
			{
				template = SR.GetString(template);
			}
			StringBuilder stringBuilder = new StringBuilder(template);
			string text = stringBuilder.ToString();
			int num = text.IndexOf('{');
			while (num > -1)
			{
				if (num < text.Length - 1 && text[num + 1] == '{')
				{
					stringBuilder.Remove(num, 1);
					text = stringBuilder.ToString();
					num = text.IndexOf('{', num + 1);
					continue;
				}
				int num2 = text.IndexOf('}', num);
				if (num2 == -1)
				{
					break;
				}
				while (num2 < text.Length - 1 && text[num2 + 1] == '}')
				{
					stringBuilder.Remove(num2, 1);
					text = stringBuilder.ToString();
					num2 = text.IndexOf('}', num2);
				}
				if (num2 == -1)
				{
					break;
				}
				if (num2 > num + 1)
				{
					string text2 = "";
					bool flag = false;
					if (variableNames != null && variableNames.Length > 0)
					{
						int num3 = 0;
						while (num3 < variableNames.Length)
						{
							if (variableNames[num3] == null || variableNames[num3].Length != num2 - num - 1 || string.Compare(text, num + 1, variableNames[num3], 0, num2 - num - 1, true) != 0)
							{
								num3++;
								continue;
							}
							flag = true;
							text2 = values[num3];
							if ((text2 == null || text2 == variableNames[num3]) && variableNames[num3].StartsWith("UI_"))
							{
								text2 = SR.GetString(variableNames[num3]);
							}
							if (text2 != null)
							{
								break;
							}
							text2 = "";
							break;
						}
					}
					if (flag)
					{
						stringBuilder.Remove(num, num2 - num + 1);
						stringBuilder.Insert(num, text2);
						num2 = num + text2.Length;
						text = stringBuilder.ToString();
					}
					else
					{
						num2 = num + 1;
					}
				}
				if (num2 >= text.Length)
				{
					break;
				}
				num = text.IndexOf('{', num2);
			}
			return text;
		}

		public static string RemoveSection(string output, string section)
		{
			if (output != null && section != null)
			{
				int num = output.IndexOf(string.Format("<!-- {0} START -->", section));
				if (num == -1)
				{
					return output;
				}
				string text = string.Format("<!-- {0} END -->", section);
				int num2 = output.IndexOf(text, num);
				if (num2 == -1)
				{
					return output.Substring(0, num);
				}
				return output.Substring(0, num) + output.Substring(num2 + text.Length + 1);
			}
			return output;
		}

		public static string CreateInstanceId(SecureLicenseContext context)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}
			object obj = context.Items["__TemplateInstanceId"];
			int num;
			if (obj == null)
			{
				obj = 1;
				num = 1;
			}
			else
			{
				num = (int)obj;
				obj = num + 1;
			}
			context.Items["__TemplateInstanceId"] = obj;
			return num.ToString();
		}
	}
}
