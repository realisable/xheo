namespace DeployLX.Licensing.v5
{
	public interface IPurchaseLimit
	{
		string PurchaseUrl
		{
			get;
			set;
		}
	}
}
