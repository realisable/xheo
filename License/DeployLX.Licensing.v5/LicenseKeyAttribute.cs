using System;

namespace DeployLX.Licensing.v5
{
	[AttributeUsage(AttributeTargets.Assembly, AllowMultiple = false)]
	public sealed class LicenseKeyAttribute : Attribute
	{
		private string _data;

		public string Data
		{
			get
			{
				return _data;
			}
		}

		public LicenseKeyAttribute(string data)
		{
			_data = data;
		}
	}
}
