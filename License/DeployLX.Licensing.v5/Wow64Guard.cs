using System;
using System.Threading;
using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	internal class Wow64Guard : IDisposable
	{
		private IntPtr _oldValue;

		public Wow64Guard()
		{
			_oldValue = IntPtr.Zero;
			if (SafeNativeMethods.IsOsWin64)
			{
				Disable();
			}
		}

		public void Dispose()
		{
			if (SafeNativeMethods.IsOsWin64)
			{
				Restore();
			}
		}

		private void Disable()
		{
			SafeNativeMethods.Wow64DisableWow64FsRedirection(ref _oldValue);
		}

		private void Restore()
		{
			SafeNativeMethods.Wow64RevertWow64FsRedirection(_oldValue);
		}

		public static void ExecuteGuarded(MethodInvoker action)
		{
			Exception ex = null;
			Thread thread = new Thread((ThreadStart)delegate
			{
				try
				{
					action();
				}
				catch (Exception ex2)
				{
					Exception ex3 = ex = ex2;
				}
			});
			thread.Start();
			if (ex == null)
			{
				return;
			}
			throw ex;
		}
	}
}
