namespace DeployLX.Licensing.v5
{
	public enum VirtualMachineLevel
	{
		Unknown,
		Error,
		Physical,
		Virtual,
		HyperVirtual
	}
}
