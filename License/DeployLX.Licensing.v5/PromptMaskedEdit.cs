using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Text;
using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public class PromptMaskedEdit : Control
	{
		internal delegate bool IsValidCharDelegate(ref char c, CharacterCasing casing);

		private struct MaskPosition
		{
			public char Char;

			public bool IsLiteral;

			public CharacterCasing Casing;

			public IsValidCharDelegate CheckMethod;

			public MaskPosition(char c, bool isLiteral, CharacterCasing casing)
			{
				Char = c;
				IsLiteral = isLiteral;
				Casing = casing;
				CheckMethod = null;
			}

			public MaskPosition(char c, bool isLiteral, CharacterCasing casing, IsValidCharDelegate checkMethod)
			{
				this = new MaskPosition(c, isLiteral, casing);
				CheckMethod = checkMethod;
			}
		}

		private sealed class EditTextBox : TextBox
		{
			private PromptMaskedEdit _edit;

			private bool _isOverwrite;

			private bool _handleKeyPress = true;

			private int _caret;

			private string _undo;

			private int _undoStart;

			private int _undoLength;

			private bool _direction = true;

			public EditTextBox(PromptMaskedEdit edit)
			{
				_edit = edit;
			}

			private bool PlaceChar(char c, int start, int length, bool overwrite)
			{
				_caret = start;
				if (start < _edit._parsedMask.Length)
				{
					_edit._output.Length = 0;
					_edit._output.Append(Text);
					if (length > 0)
					{
						int end = start + length - 1;
						if (!_edit.Replace(_edit._output, c.ToString(), start, end, out _caret))
						{
							return false;
						}
					}
					else if (overwrite)
					{
						if (!_edit.Replace(_edit._output, c.ToString(), start, -1, out _caret))
						{
							return false;
						}
					}
					else if (!_edit.Insert(_edit._output, c.ToString(), start, out _caret))
					{
						return false;
					}
					if (_caret + 1 == _edit._output.Length)
					{
						_caret++;
						while (_caret < _edit._parsedMask.Length && _edit._parsedMask[_caret].IsLiteral)
						{
							_edit._output.Append(_edit._parsedMask[_caret++].Char);
						}
					}
					if (!_direction)
					{
						SetOutputToText(_edit._output.ToString(), false);
					}
					else
					{
						Text = _edit._output.ToString();
					}
					_direction = true;
					return true;
				}
				return false;
			}

			private bool Delete(Keys keyCode, int start, int length, bool makeUndo)
			{
				_caret = start;
				_edit._output.Length = 0;
				_edit._output.Append(Text);
				int num;
				if (length == 0)
				{
					if (keyCode == Keys.Back)
					{
						if (start == 0)
						{
							return true;
						}
						num = _edit.FindEditPosition(_edit._output, 0, start - 1, false, FindType.Edit);
						if (num == -1)
						{
							length += start + 1;
							start = 0;
						}
						else
						{
							start = num;
							if (start == 0)
							{
								length = 2;
							}
						}
					}
					else if (start + length == _edit._parsedMask.Length)
					{
						return true;
					}
				}
				int end = (length > 0) ? (start + length - 1) : start;
				if (_edit.Remove(_edit._output, start, end, out num, false))
				{
					if (_edit._output.ToString() != Text)
					{
						if (!_direction && !makeUndo)
						{
							Text = _edit._output.ToString();
						}
						else
						{
							SetOutputToText(_edit._output.ToString(), false);
							_direction = false;
						}
						_caret = start;
					}
					else if (length > 0)
					{
						_caret = num;
					}
					else
					{
						switch (keyCode)
						{
						case Keys.Delete:
							_caret = _edit.FindEditPosition(_edit._output, start, true);
							break;
						case Keys.Back:
							_caret = _edit.FindEditPosition(_edit._output, 0, start, false, FindType.Edit);
							break;
						}
					}
					if (_caret == -1)
					{
						_caret = _edit.FindEditPosition(_edit._output, start, true);
					}
					if (_caret != -1)
					{
						int num2 = _edit.FindEditPosition(_edit._output, _caret, true);
						if (num2 == -1)
						{
							base.Select(_caret, 0);
						}
						else
						{
							base.Select(num2, 0);
						}
					}
					_edit.InvalidateControl();
					return true;
				}
				return false;
			}

			internal bool PasteInternal(string text)
			{
				int selectionStart = base.SelectionStart;
				int selectionLength = SelectionLength;
				try
				{
					if (text != null)
					{
						text = text.Trim();
					}
					if (text != null && text.Length != 0)
					{
						_edit._output.Length = 0;
						_edit._output.Append(Text);
						int num;
						if (!_edit.Replace(_edit._output, text, selectionStart, selectionStart + selectionLength, out num))
						{
							return false;
						}
						SetOutputToText(_edit._output.ToString(), false);
						Text = _edit._output.ToString();
						base.SelectionStart = Math.Max(0, _edit.FindEditPosition(_edit._output, num + 1, true));
						SelectionLength = 0;
					}
					else if (!Delete(Keys.Delete, selectionStart, selectionLength, true))
					{
						return false;
					}
					_edit.InvalidateControl();
				}
				finally
				{
					_direction = true;
				}
				return true;
			}

			protected override void OnKeyDown(KeyEventArgs e)
			{
				base.OnKeyDown(e);
				if (_edit._mask != null && _edit._mask.Length != 0)
				{
					Keys keys = e.KeyCode;
					if (keys == Keys.Return || keys == Keys.Escape)
					{
						_handleKeyPress = false;
					}
					if (keys == Keys.Insert && e.Modifiers == Keys.None)
					{
						_isOverwrite = !_isOverwrite;
					}
					else
					{
						if (e.Control && char.IsLetter((char)keys))
						{
							if (keys != Keys.H)
							{
								_handleKeyPress = false;
								return;
							}
							keys = Keys.Back;
						}
						if (keys != Keys.Delete && keys != Keys.Back)
						{
							return;
						}
						if (!base.ReadOnly)
						{
							int num = base.SelectionStart;
							int num2 = SelectionLength;
							switch (e.Modifiers)
							{
							case Keys.Control:
								if (num2 == 0)
								{
									if (keys == Keys.Delete)
									{
										num2 = _edit._parsedMask.Length - num;
									}
									else
									{
										num2 = ((num == _edit._parsedMask.Length) ? num : (num + 1));
										num = 0;
									}
								}
								break;
							case Keys.Shift:
								if (keys == Keys.Delete)
								{
									keys = Keys.Back;
								}
								break;
							}
							if (!_handleKeyPress)
							{
								_handleKeyPress = true;
							}
							if (!Delete(keys, num, num2, false))
							{
								PlayBeep();
							}
							e.SuppressKeyPress = true;
						}
					}
				}
			}

			protected override void OnKeyPress(KeyPressEventArgs e)
			{
				base.OnKeyPress(e);
				if (_edit._mask != null && _edit._mask.Length != 0)
				{
					if (!_handleKeyPress)
					{
						_handleKeyPress = true;
					}
					else if (!base.ReadOnly)
					{
						int selectionStart = base.SelectionStart;
						int selectionLength = SelectionLength;
						if (PlaceChar(e.KeyChar, selectionStart, selectionLength, _isOverwrite))
						{
							base.SelectionStart = _caret + 1;
							if (selectionLength > 0)
							{
								SelectionLength = 0;
							}
						}
						else
						{
							PlayBeep();
						}
						e.Handled = true;
						_edit.InvalidateControl();
					}
				}
			}

			protected override bool IsInputKey(Keys keyData)
			{
				if ((keyData & Keys.KeyCode) == Keys.Return)
				{
					return false;
				}
				return base.IsInputKey(keyData);
			}

			protected override void OnTextChanged(EventArgs e)
			{
				base.OnTextChanged(e);
				_edit.OnTextChanged(e);
			}

			protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
			{
				bool result;
				if (!(result = base.ProcessCmdKey(ref msg, keyData)) && keyData == (Keys)131137)
				{
					base.SelectAll();
					result = true;
				}
				return result;
			}

			protected override void OnGotFocus(EventArgs e)
			{
				base.OnGotFocus(e);
				UndoCheckpoint();
				_edit.InvalidateControl();
			}

			protected override void OnLostFocus(EventArgs e)
			{
				base.OnLostFocus(e);
				_edit.InvalidateControl();
			}

			protected override void WndProc(ref Message m)
			{
				if (_edit._mask != null)
				{
					switch (m.Msg)
					{
					case 183:
					case 197:
						return;
					case 768:
						if (!base.ReadOnly && WmCopy())
						{
							if (!WmClear())
							{
								PlayBeep();
							}
						}
						else
						{
							PlayBeep();
						}
						return;
					case 769:
						WmCopy();
						return;
					case 770:
						if (!WmPaste())
						{
							PlayBeep();
						}
						return;
					case 771:
						if (!WmClear())
						{
							PlayBeep();
						}
						return;
					case 199:
					case 772:
						SetOutputToText(_undo, true);
						return;
					}
				}
				base.WndProc(ref m);
			}

			private bool WmClear()
			{
				if (!base.ReadOnly)
				{
					int selectionStart = base.SelectionStart;
					int selectionLength = SelectionLength;
					return Delete(Keys.Delete, selectionStart, selectionLength, false);
				}
				return false;
			}

			private bool WmCopy()
			{
				string selectedText = SelectedText;
				if (selectedText.Length == 0)
				{
					ClipboardToolbox.SetClipboard(DataFormats.Text, null);
				}
				else
				{
					ClipboardToolbox.SetClipboard(DataFormats.Text, selectedText);
				}
				return true;
			}

			private bool WmPaste()
			{
				if (!base.ReadOnly)
				{
					string text = ClipboardToolbox.GetClipboard(DataFormats.Text) as string;
					if (text != null && PasteInternal(text))
					{
						_undoLength = ((text != null) ? text.Length : 0);
						return true;
					}
				}
				return false;
			}

			private void PlayBeep()
			{
				if (_edit._beepOnError)
				{
					SafeNativeMethods.MessageBeep(-1);
				}
			}

			internal void ClearUndoList()
			{
				_undo = null;
			}

			private void SetOutputToText(string newOutput, bool select)
			{
				int undoStart = _undoStart;
				int undoLength = _undoLength;
				UndoCheckpoint();
				Text = newOutput;
				if (select)
				{
					base.SelectionStart = undoStart;
					SelectionLength = undoLength;
				}
				_edit.InvalidateControl();
			}

			private void UndoCheckpoint()
			{
				_undo = Text;
				_undoStart = base.SelectionStart;
				_undoLength = SelectionLength;
			}
		}

		private enum FindType
		{
			Literal,
			Edit,
			Available,
			Assigned
		}

		private Bitmap _back;

		private IntPtr _backBrush;

		private StringBuilder _output;

		private EditTextBox _textBox;

		private Color _backFadeColor;

		private string _prompt;

		private Font _promptFont;

		private Color _promptColor;

		private string _mask;

		private string _displayMask;

		private MaskPosition[] _parsedMask;

		private char _defaultPromptChar = 'X';

		private Color _maskColor;

		private bool _hideMaskOnleave;

		private bool _allowSpace;

		private bool _beepOnError = true;

		private static IsValidCharDelegate AnyLetterOrDigitDelegate = PromptMaskedEdit.AnyLetterOrDigit;

		private static IsValidCharDelegate AnyCharacterDelegate = PromptMaskedEdit.AnyCharacter;

		private static IsValidCharDelegate AnyDigitDelegate = PromptMaskedEdit.AnyDigit;

		private static IsValidCharDelegate AnyDigitOrNumberCharacterDelegate = PromptMaskedEdit.AnyDigitOrNumberCharacter;

		public override Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				if (value == Color.Empty)
				{
					value = SystemColors.Window;
				}
				base.BackColor = value;
			}
		}

		public override Color ForeColor
		{
			get
			{
				return base.ForeColor;
			}
			set
			{
				if (value == Color.Empty)
				{
					value = SystemColors.ControlText;
				}
				base.ForeColor = value;
				_textBox.ForeColor = value;
			}
		}

		[NotifyParentProperty(true)]
		[Category("Appearance")]
		[Description("The BackFadeColor value.")]
		public Color BackFadeColor
		{
			get
			{
				return _backFadeColor;
			}
			set
			{
				_backFadeColor = value;
				InvalidateControl();
			}
		}

		[Category("Appearance")]
		[NotifyParentProperty(true)]
		[Description("The BorderStyle value.")]
		public BorderStyle BorderStyle
		{
			get
			{
				return _textBox.BorderStyle;
			}
			set
			{
				_textBox.BorderStyle = value;
				InvalidateControl();
			}
		}

		protected override CreateParams CreateParams
		{
			get
			{
				CreateParams createParams = base.CreateParams;
				createParams.ClassName = null;
				return createParams;
			}
		}

		public override Font Font
		{
			get
			{
				return base.Font;
			}
			set
			{
				base.Font = value;
				_textBox.Font = value;
			}
		}

		public override string Text
		{
			get
			{
				return _textBox.Text;
			}
			set
			{
				if (value == null)
				{
					value = "";
				}
				if (_mask != null && _mask.Length != 0)
				{
					_output.Length = 0;
					_output.Append(value);
					if (value == null)
					{
						value = "";
					}
					int num;
					if (!IsValidString(_output, value, 0, out num))
					{
						throw new ArgumentOutOfRangeException("Text", value, null);
					}
					SetText(_output, value, 0);
					_textBox.Text = _output.ToString();
					_textBox.SelectionLength = 0;
					_textBox.SelectionStart = _textBox.Text.Length;
					InvalidateControl();
					_textBox.ClearUndoList();
				}
				else
				{
					_textBox.Text = value;
				}
			}
		}

		[Category("Appearance")]
		public string Prompt
		{
			get
			{
				return _prompt;
			}
			set
			{
				_prompt = value;
				InvalidateControl();
			}
		}

		[Category("Appearance")]
		public Font PromptFont
		{
			get
			{
				Font promptFont = _promptFont;
				if (promptFont != null)
				{
					return promptFont;
				}
				Font font = (base.Parent != null) ? base.Parent.Font : null;
				if (font != null)
				{
					return font;
				}
				if (Site != null)
				{
					AmbientProperties ambientProperties = Site.GetService(typeof(AmbientProperties)) as AmbientProperties;
					if (ambientProperties != null && ambientProperties.Font != null)
					{
						return ambientProperties.Font;
					}
				}
				return Font;
			}
			set
			{
				_promptFont = value;
				InvalidateControl();
			}
		}

		[Category("Appearance")]
		[Description("The PromptColor value.")]
		public Color PromptColor
		{
			get
			{
				return _promptColor;
			}
			set
			{
				_promptColor = value;
				InvalidateControl();
			}
		}

		[Category("Appearance")]
		public string Mask
		{
			get
			{
				return _mask;
			}
			set
			{
				if (value != null && value.Trim().Length == 0)
				{
					value = null;
				}
				_mask = value;
				if (value != null && ParseMask(value))
				{
					if (_output == null)
					{
						_output = new StringBuilder(_parsedMask.Length);
					}
				}
				else
				{
					_parsedMask = null;
					_displayMask = null;
					_mask = null;
				}
				InvalidateControl();
			}
		}

		[Browsable(false)]
		public int MaskLength
		{
			get
			{
				if (_parsedMask != null)
				{
					return _parsedMask.Length;
				}
				return 2147483647;
			}
		}

		[DefaultValue('X')]
		[Category("Appearance")]
		[Description("The character to display to indicate a value is needed.")]
		[NotifyParentProperty(true)]
		public char DefaultPromptCharacter
		{
			get
			{
				return _defaultPromptChar;
			}
			set
			{
				_defaultPromptChar = value;
				if (_mask != null)
				{
					ParseMask(_mask);
				}
				InvalidateControl();
			}
		}

		[Description("The MaskColor value.")]
		[Category("Appearance")]
		public Color MaskColor
		{
			get
			{
				return _maskColor;
			}
			set
			{
				_maskColor = value;
				InvalidateControl();
			}
		}

		[DefaultValue(false)]
		[Category("Behavior")]
		public bool HideMaskOnLeave
		{
			get
			{
				return _hideMaskOnleave;
			}
			set
			{
				_hideMaskOnleave = value;
				InvalidateControl();
			}
		}

		[Browsable(false)]
		public int SelectionStart
		{
			get
			{
				return _textBox.SelectionStart;
			}
			set
			{
				_textBox.SelectionStart = value;
			}
		}

		[Browsable(false)]
		public int SelectionLength
		{
			get
			{
				return _textBox.SelectionLength;
			}
			set
			{
				_textBox.SelectionLength = value;
			}
		}

		[Browsable(false)]
		public string SelectedText
		{
			get
			{
				return _textBox.SelectedText;
			}
			set
			{
				if (_mask != null && _mask.Length != 0)
				{
					_textBox.PasteInternal(value);
				}
				else
				{
					_textBox.SelectedText = value;
				}
				_textBox.ClearUndo();
				_textBox.ClearUndoList();
			}
		}

		[DefaultValue(false)]
		[Category("Behavior")]
		public bool ReadOnly
		{
			get
			{
				return _textBox.ReadOnly;
			}
			set
			{
				_textBox.ReadOnly = value;
				InvalidateControl();
			}
		}

		[DefaultValue(false)]
		[Description("Indicates if space characters are always accepted as valid characters.")]
		[Category("Behavior")]
		public bool AllowSpace
		{
			get
			{
				return _allowSpace;
			}
			set
			{
				_allowSpace = value;
			}
		}

		[Description("Indicates if an audible alert should be played when an invalid operation is attempted.")]
		[Category("Behavior")]
		[DefaultValue(true)]
		public bool BeepOnError
		{
			get
			{
				return _beepOnError;
			}
			set
			{
				_beepOnError = value;
			}
		}

		[Category("Behavior")]
		public char PasswordChar
		{
			get
			{
				return _textBox.PasswordChar;
			}
			set
			{
				_textBox.PasswordChar = value;
				InvalidateControl();
			}
		}

		[DefaultValue(false)]
		[Category("Behavior")]
		public bool UseSystemPasswordChar
		{
			get
			{
				return _textBox.UseSystemPasswordChar;
			}
			set
			{
				_textBox.UseSystemPasswordChar = value;
			}
		}

		[Category("Behavior")]
		[DefaultValue(CharacterCasing.Normal)]
		public CharacterCasing CharacterCasing
		{
			get
			{
				return _textBox.CharacterCasing;
			}
			set
			{
				_textBox.CharacterCasing = value;
			}
		}

		public PromptMaskedEdit()
		{
			_textBox = new EditTextBox(this);
			_textBox.Dock = DockStyle.Fill;
			base.Height = _textBox.PreferredHeight;
			AttachTextBoxEvents();
			base.Controls.Add(_textBox);
			base.SetStyle(ControlStyles.ResizeRedraw | ControlStyles.SupportsTransparentBackColor | ControlStyles.AllPaintingInWmPaint, true);
			BackColor = SystemColors.Window;
		}

		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
			if (_back != null)
			{
				_back.Dispose();
			}
			SafeNativeMethods.DeleteObject(_backBrush);
		}

		private new void ResetBackColor()
		{
			BackColor = SystemColors.Window;
			_textBox.BackColor = Color.Empty;
		}

		private bool ShouldSerializeBackColor()
		{
			return BackColor != SystemColors.Window;
		}

		private new void ResetForeColor()
		{
			ForeColor = SystemColors.ControlText;
			_textBox.ForeColor = Color.Empty;
		}

		private bool ShouldSerializeForeColor()
		{
			return ForeColor != SystemColors.ControlText;
		}

		private bool ShouldSerializeBackFadeColor()
		{
			return _backFadeColor != Color.Empty;
		}

		private void ResetBackFadeColor()
		{
			_backFadeColor = Color.Empty;
			InvalidateControl();
		}

		private bool ShouldSerializeBorderStyle()
		{
			return _textBox.BorderStyle != BorderStyle.Fixed3D;
		}

		private void ResetBorderStyle()
		{
			_textBox.BorderStyle = BorderStyle.Fixed3D;
			InvalidateControl();
		}

		private void ResetPromptFont()
		{
			_promptFont = null;
			InvalidateControl();
		}

		private bool ShouldSerializePromptFont()
		{
			return _promptFont != null;
		}

		private bool ShouldSerializePromptColor()
		{
			return _promptColor != Color.Empty;
		}

		private void ResetPromptColor()
		{
			_promptColor = Color.Empty;
			InvalidateControl();
		}

		private bool ShouldSerializeMaskColor()
		{
			return _maskColor != Color.Empty;
		}

		private void ResetMaskColor()
		{
			_maskColor = Color.Empty;
			InvalidateControl();
		}

		private bool ShouldSerializeSelectionStart()
		{
			return false;
		}

		private bool ShouldSerializeSelectionLength()
		{
			return false;
		}

		private bool ShouldSerializeSelectedText()
		{
			return false;
		}

		private void ResetPasswordChar()
		{
			_textBox.PasswordChar = '\0';
			InvalidateControl();
		}

		private bool ShouldSerializePasswordChar()
		{
			return _textBox.PasswordChar != '\0';
		}

		protected override void WndProc(ref Message m)
		{
			switch (m.Msg)
			{
			case 307:
				if (_backBrush != IntPtr.Zero)
				{
					SafeNativeMethods.SelectObject(m.WParam, _backBrush);
					SafeNativeMethods.SetBkMode(m.WParam, 1);
					m.Result = _backBrush;
					break;
				}
				goto default;
			default:
				base.WndProc(ref m);
				break;
			case 7:
				_textBox.Focus();
				m.Result = (IntPtr)1;
				break;
			}
		}

		protected override void OnResize(EventArgs e)
		{
			base.OnResize(e);
			if (_back != null)
			{
				_back.Dispose();
			}
			SafeNativeMethods.DeleteObject(_backBrush);
			_back = null;
		}

		protected override void OnGotFocus(EventArgs e)
		{
			_textBox.Focus();
		}

		protected override void SetBoundsCore(int x, int y, int width, int height, BoundsSpecified specified)
		{
			height = _textBox.PreferredHeight;
			base.SetBoundsCore(x, y, width, height, specified);
		}

		public override string ToString()
		{
			return _textBox.ToString();
		}

		protected override void OnPaintBackground(PaintEventArgs pevent)
		{
			if (base.Width > 0 && base.Height > 0)
			{
				if (_back == null)
				{
					_back = new Bitmap(base.Width, base.Height, PixelFormat.Format32bppArgb);
				}
				int num = 0;
				if (_textBox.BorderStyle == BorderStyle.Fixed3D)
				{
					num = 0;
				}
				else if (_textBox.BorderStyle == BorderStyle.FixedSingle)
				{
					num = 1;
				}
				string text = _textBox.Text;
				using (Graphics graphics = Graphics.FromImage(_back))
				{
					StringFormat stringFormat = new StringFormat(StringFormat.GenericTypographic);
					stringFormat.Trimming = StringTrimming.None;
					stringFormat.FormatFlags = (StringFormatFlags.MeasureTrailingSpaces | StringFormatFlags.NoWrap | StringFormatFlags.NoClip);
					PaintEventArgs pevent2 = new PaintEventArgs(graphics, base.ClientRectangle);
					base.OnPaintBackground(pevent2);
					if (BackFadeColor != Color.Transparent && BackFadeColor != Color.Empty)
					{
						using (LinearGradientBrush brush = new LinearGradientBrush(base.ClientRectangle, Color.Transparent, BackFadeColor, -90f, true))
						{
							graphics.FillRectangle(brush, base.ClientRectangle);
						}
					}
					if (_prompt != null && _prompt.Length > 0 && text.Length == 0 && !_textBox.Focused)
					{
						Color color = _promptColor;
						if (color == Color.Empty)
						{
							color = Color.FromArgb(128, ForeColor);
						}
						using (Brush brush2 = new SolidBrush(color))
						{
							graphics.DrawString(_prompt, PromptFont, brush2, (float)(num - 1), (float)(num + 1));
						}
					}
					else if (_mask != null && PasswordChar == '\0' && (_textBox.Focused || !_hideMaskOnleave) && text.Length < _mask.Length)
					{
						IntPtr intPtr = Font.ToHfont();
						IntPtr hdc = graphics.GetHdc();
						if (intPtr != new IntPtr(-1) && hdc != new IntPtr(-1))
						{
							int num2 = 1;
							int num3 = 1;
							switch (BorderStyle)
							{
							case BorderStyle.None:
								num2 = 0;
								num3 = 0;
								break;
							case BorderStyle.FixedSingle:
								num3++;
								num2++;
								break;
							}
							IntPtr intPtr2 = SafeNativeMethods.CreatePatternBrush(_back.GetHbitmap());
							if (intPtr2 != new IntPtr(-1))
							{
								byte[] array = new byte[16];
								Array.Copy(BitConverter.GetBytes(base.Width), 0, array, 8, 4);
								Array.Copy(BitConverter.GetBytes(base.Height), 0, array, 12, 4);
								SafeNativeMethods.FillRect(hdc, array, intPtr2);
								SafeNativeMethods.DeleteObject(intPtr2);
							}
							Color left = _maskColor;
							if (left == Color.Empty)
							{
								Color backColor = BackColor;
								Color foreColor = ForeColor;
								left = Color.FromArgb((backColor.R + backColor.R + backColor.R + backColor.R + foreColor.R) / 5, (backColor.B + backColor.B + backColor.B + backColor.B + foreColor.B) / 5, (backColor.G + backColor.G + backColor.G + backColor.G + foreColor.G) / 5);
							}
							SafeNativeMethods.SetTextColor(hdc, left.B << 16 | left.G << 8 | left.R);
							SafeNativeMethods.SelectObject(hdc, intPtr);
							SafeNativeMethods.SetBkMode(hdc, 1);
							byte[] array2 = new byte[8];
							SafeNativeMethods.GetTextExtentPoint32(hdc, text, text.Length, array2);
							int num4 = BitConverter.ToInt32(array2, 0);
							SafeNativeMethods.TextOut(hdc, num4 + num2, num3, _displayMask.Substring(text.Length), _displayMask.Length - text.Length);
							SafeNativeMethods.DeleteObject(intPtr);
							graphics.ReleaseHdc(hdc);
						}
					}
				}
				if (_backBrush != IntPtr.Zero)
				{
					SafeNativeMethods.DeleteObject(_backBrush);
				}
				_backBrush = SafeNativeMethods.CreatePatternBrush(_back.GetHbitmap());
				pevent.Graphics.DrawImage(_back, 0, 0);
			}
		}

		private bool Insert(StringBuilder output, string text, int position, out int lastPosition)
		{
			if (text == null)
			{
				throw new ArgumentNullException("text");
			}
			lastPosition = position;
			if (position >= 0 && position < _parsedMask.Length)
			{
				if (text.Length == 0)
				{
					return true;
				}
				if (!IsValidString(output, text, position, out lastPosition))
				{
					return false;
				}
				int num = FindEditPosition(output, position, true);
				bool flag = FindEditPosition(output, num, lastPosition, true, FindType.Assigned) != -1;
				int num2 = FindEditPosition(output, 0, output.Length, false, FindType.Assigned);
				if (flag && lastPosition == _parsedMask.Length - 1)
				{
					lastPosition = _parsedMask.Length;
					return false;
				}
				int num3 = FindEditPosition(output, lastPosition + 1, true);
				if (flag)
				{
					while (true)
					{
						if (num3 != -1)
						{
							if (output.Length > num3)
							{
								char c = output[num];
								if (IsValidChar(ref c, num3))
								{
									goto IL_00be;
								}
								lastPosition = num3;
								return false;
							}
							goto IL_00be;
						}
						lastPosition = _parsedMask.Length;
						return false;
						IL_00be:
						if (num == num2)
						{
							break;
						}
						num = FindEditPosition(output, num + 1, true);
						num3 = FindEditPosition(output, num3 + 1, true);
					}
				}
				if (flag)
				{
					while (num >= position)
					{
						if (!_parsedMask[num].IsLiteral && output.Length > num)
						{
							SetChar(output, output[num], num3);
						}
						num3 = FindEditPosition(output, 0, num3 - 1, false, FindType.Edit);
						num = FindEditPosition(output, 0, num - 1, false, FindType.Edit);
					}
				}
				SetText(output, text, position);
				return true;
			}
			return false;
		}

		private bool Replace(StringBuilder output, string text, int start, int end, out int lastPosition)
		{
			if (output == null)
			{
				throw new ArgumentNullException("output");
			}
			if (text == null)
			{
				throw new ArgumentNullException("text");
			}
			if (end == -1)
			{
				if (!IsValidString(output, text, start, out lastPosition))
				{
					return false;
				}
				SetText(output, text, start);
				return true;
			}
			lastPosition = start;
			if (end > _parsedMask.Length)
			{
				lastPosition = end;
				return false;
			}
			if (start >= 0 && start <= end)
			{
				if (text.Length == 0)
				{
					return Remove(output, start, end, out lastPosition, false);
				}
				if (!IsValidString(output, text, start, out lastPosition))
				{
					return false;
				}
				if (lastPosition < end)
				{
					int num;
					if (!Remove(output, lastPosition + 1, end, out num, false))
					{
						lastPosition = num;
						return false;
					}
				}
				else if (lastPosition > end)
				{
					int num2 = FindEditPosition(output, 0, output.Length, false, FindType.Assigned);
					int num3 = lastPosition + 1;
					int num4 = end + 1;
					while (true)
					{
						if (num2 == -1)
						{
							break;
						}
						if (output.Length <= num4)
						{
							break;
						}
						num4 = FindEditPosition(output, num4, true);
						num3 = FindEditPosition(output, num3, true);
						if (num3 == -1)
						{
							break;
						}
						char c = output[num4];
						if (IsValidChar(ref c, num3))
						{
							if (num4 == num2)
							{
								break;
							}
							continue;
						}
						lastPosition = num3;
						return false;
					}
					while (num2 > -1 && num3 > lastPosition)
					{
						if (output.Length > num4)
						{
							SetChar(output, output[num4], num3);
						}
						num4 = FindEditPosition(output, 0, num4 - 1, false, FindType.Edit);
						num3 = FindEditPosition(output, 0, num3 - 1, false, FindType.Edit);
					}
				}
				SetText(output, text, start);
				return true;
			}
			return false;
		}

		private bool Remove(StringBuilder output, int start, int end, out int lastPosition, bool testOnly)
		{
			if (output == null)
			{
				throw new ArgumentNullException("output");
			}
			lastPosition = start;
			if (end > _parsedMask.Length)
			{
				lastPosition = end;
				return false;
			}
			if (start >= 0 && start <= end)
			{
				int num = FindEditPosition(output, 0, output.Length, false, FindType.Assigned);
				int num2 = FindEditPosition(output, start, end, true, FindType.Edit);
				if (num2 != -1 && num2 < num)
				{
					if (end <= num)
					{
						int num3 = FindEditPosition(output, end + 1, true);
						int num4 = num3;
						start = num2;
						while (num3 != -1)
						{
							if (output.Length > num3)
							{
								char c = output[num3];
								if (IsValidChar(ref c, num2))
								{
									goto IL_0093;
								}
								lastPosition = num2;
								return false;
							}
							goto IL_0093;
							IL_0093:
							if (num3 == num)
							{
								break;
							}
							num3 = FindEditPosition(output, num3 + 1, true);
							num2 = FindEditPosition(output, num2 + 1, true);
						}
						if (testOnly)
						{
							return true;
						}
						num3 = num4;
						num2 = start;
						while (num3 >= 0 && output.Length > num3)
						{
							SetChar(output, output[num3], num2);
							if (num3 == num)
							{
								break;
							}
							num3 = FindEditPosition(output, num3 + 1, true);
							num2 = FindEditPosition(output, num2 + 1, true);
						}
						if (end != num)
						{
							start = num2 + 1;
						}
						output.Length = Math.Min(num, start);
						lastPosition = output.Length;
					}
					if (start <= end)
					{
						output.Length = Math.Min(output.Length, start);
					}
					return true;
				}
				if (output.Length > start)
				{
					output.Remove(start, Math.Min(end, output.Length - start));
					SetText(output, output.ToString(start, output.Length - start), start);
					lastPosition = FindEditPosition(output, end, true);
				}
				return true;
			}
			return false;
		}

		private void SetText(StringBuilder output, string text, int position)
		{
			foreach (char c in text)
			{
				char value = c;
				if (!IsValidChar(ref value, position))
				{
					int num = FindEditPosition(output, position, true);
					while (position < num)
					{
						if (output.Length > position)
						{
							output[position] = _parsedMask[position++].Char;
						}
						else
						{
							output.Append(_parsedMask[position++].Char);
						}
					}
					IsValidChar(ref value, position);
				}
				if (output.Length > position)
				{
					output[position] = value;
				}
				else
				{
					output.Append(value);
				}
				position++;
			}
		}

		private void SetChar(StringBuilder output, char c, int position)
		{
			if (output.Length > position)
			{
				IsValidChar(ref c, position);
				output[position] = c;
			}
			else
			{
				while (output.Length < position)
				{
					if (_parsedMask[output.Length].IsLiteral)
					{
						output.Append(_parsedMask[output.Length].Char);
					}
					else
					{
						output.Append(' ');
					}
				}
				IsValidChar(ref c, position);
				output.Append(c);
			}
		}

		private int FindEditPosition(StringBuilder output, int start, int end, bool direction, FindType findType)
		{
			if (start != -1 && end != -1)
			{
				if (end >= _parsedMask.Length)
				{
					end = _parsedMask.Length - 1;
				}
				if (start > end)
				{
					return -1;
				}
				int num = direction ? start : end;
				do
				{
					switch (findType)
					{
					case FindType.Assigned:
						if (!_parsedMask[num].IsLiteral && ((output == null) ? _textBox.Text.Length : output.Length) > num)
						{
							return num;
						}
						break;
					case FindType.Available:
						if (_parsedMask[num].IsLiteral)
						{
							break;
						}
						if (((output == null) ? _textBox.Text.Length : output.Length) > num)
						{
							break;
						}
						return num;
					case FindType.Edit:
						if (_parsedMask[num].IsLiteral)
						{
							break;
						}
						return num;
					case FindType.Literal:
						if (!_parsedMask[num].IsLiteral)
						{
							break;
						}
						return num;
					}
					num = ((!direction) ? (num - 1) : (num + 1));
				}
				while (num >= start && num <= end);
				return -1;
			}
			return -1;
		}

		public int FindEditPosition(StringBuilder output, int start, bool direction)
		{
			return FindEditPosition(output, start, _parsedMask.Length, direction, FindType.Edit);
		}

		private bool ParseMask(string characterMask)
		{
			if (characterMask != null && characterMask.Length != 0)
			{
				ArrayList arrayList = new ArrayList();
				StringBuilder stringBuilder = new StringBuilder(characterMask.Length);
				char value = _defaultPromptChar;
				for (int i = 0; i < characterMask.Length; i++)
				{
					char c = characterMask[i];
					bool flag = true;
					switch (c)
					{
					case '&':
						i++;
						if (i < characterMask.Length)
						{
							value = characterMask[i];
						}
						flag = false;
						break;
					case '#':
						arrayList.Add(new MaskPosition(c, false, CharacterCasing.Normal, AnyDigitDelegate));
						break;
					case '.':
						arrayList.Add(new MaskPosition(c, false, CharacterCasing.Normal, AnyDigitOrNumberCharacterDelegate));
						break;
					case '*':
						arrayList.Add(new MaskPosition(c, false, CharacterCasing.Normal, AnyCharacterDelegate));
						break;
					case 'X':
						arrayList.Add(new MaskPosition(c, false, CharacterCasing.Upper, AnyLetterOrDigitDelegate));
						break;
					case '?':
						arrayList.Add(new MaskPosition(c, false, CharacterCasing.Upper, AnyCharacterDelegate));
						break;
					default:
					{
						IsValidCharDelegate isValidCharDelegate = ParseMaskCharacter(c, i);
						if (isValidCharDelegate != null)
						{
							arrayList.Add(new MaskPosition(c, false, char.IsUpper(c) ? CharacterCasing.Upper : CharacterCasing.Normal, isValidCharDelegate));
						}
						else
						{
							arrayList.Add(new MaskPosition(c, true, CharacterCasing.Normal));
							stringBuilder.Append(c);
							flag = false;
						}
						break;
					}
					case 'x':
						arrayList.Add(new MaskPosition(c, false, CharacterCasing.Normal, AnyLetterOrDigitDelegate));
						break;
					case '\\':
						i++;
						if (i < characterMask.Length)
						{
							arrayList.Add(new MaskPosition(characterMask[i], true, CharacterCasing.Normal));
							stringBuilder.Append(characterMask[i]);
						}
						flag = false;
						break;
					}
					if (flag)
					{
						stringBuilder.Append(value);
					}
				}
				_parsedMask = (arrayList.ToArray(typeof(MaskPosition)) as MaskPosition[]);
				_displayMask = stringBuilder.ToString();
				return true;
			}
			return false;
		}

		private static bool AnyLetterOrDigit(ref char c, CharacterCasing casing)
		{
			if (!char.IsLetterOrDigit(c))
			{
				return false;
			}
			switch (casing)
			{
			case CharacterCasing.Upper:
				c = char.ToUpper(c);
				break;
			case CharacterCasing.Lower:
				c = char.ToLower(c);
				break;
			}
			return true;
		}

		private static bool AnyCharacter(ref char c, CharacterCasing casing)
		{
			switch (casing)
			{
			case CharacterCasing.Upper:
				c = char.ToUpper(c);
				break;
			case CharacterCasing.Lower:
				c = char.ToLower(c);
				break;
			}
			return true;
		}

		private static bool AnyDigit(ref char c, CharacterCasing casing)
		{
			return char.IsDigit(c);
		}

		private static bool AnyDigitOrNumberCharacter(ref char c, CharacterCasing casing)
		{
			if (char.IsDigit(c))
			{
				return true;
			}
			switch (c)
			{
			default:
				return false;
			case '+':
			case '-':
			case '.':
				return true;
			}
		}

		protected bool IsValidChar(ref char value, int position)
		{
			if (position >= _parsedMask.Length)
			{
				return false;
			}
			MaskPosition maskPosition = _parsedMask[position];
			if (maskPosition.IsLiteral)
			{
				if (char.IsUpper(maskPosition.Char))
				{
					char c = char.ToUpper(value);
					if (c == maskPosition.Char)
					{
						value = c;
						return true;
					}
				}
				else
				{
					char c = char.ToLower(value);
					if (c == maskPosition.Char)
					{
						value = c;
						return true;
					}
				}
				return false;
			}
			if (_allowSpace && value == ' ')
			{
				return true;
			}
			if (maskPosition.CheckMethod != null)
			{
				return maskPosition.CheckMethod(ref value, maskPosition.Casing);
			}
			return maskPosition.Char == value;
		}

		protected bool IsLiteral(int position)
		{
			if (position >= _parsedMask.Length)
			{
				return false;
			}
			return _parsedMask[position].IsLiteral;
		}

		public bool IsValidString(StringBuilder output, string text, int position, out int lastPosition)
		{
			lastPosition = position;
			if (text != null && text.Length > 0)
			{
				int num = 0;
				while (num < text.Length)
				{
					char c = text[num];
					if (lastPosition >= _parsedMask.Length)
					{
						return false;
					}
					char c2 = c;
					if (IsLiteral(lastPosition) && !IsValidChar(ref c2, lastPosition))
					{
						if (output == null)
						{
							output = new StringBuilder(position);
							output.Append(' ', position);
							output.Append(text);
						}
						lastPosition = FindEditPosition(output, lastPosition, true);
						if (lastPosition != -1)
						{
							goto IL_0080;
						}
						lastPosition = _parsedMask.Length;
						return false;
					}
					goto IL_0080;
					IL_0080:
					if (IsValidChar(ref c2, lastPosition))
					{
						lastPosition++;
						num++;
						continue;
					}
					return false;
				}
				lastPosition--;
			}
			return true;
		}

		internal virtual IsValidCharDelegate ParseMaskCharacter(char c, int position)
		{
			return null;
		}

		private void AttachTextBoxEvents()
		{
			_textBox.Resize += _textBox_Resize;
		}

		private void _textBox_Resize(object sender, EventArgs e)
		{
			base.Size = _textBox.Size;
			InvalidateControl();
		}

		private void InvalidateControl()
		{
			base.Invalidate();
			_textBox.Invalidate();
		}
	}
}
