using System;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	[LimitEditor("TimeServerLimitEditor", IconResource = "asmres://DeployLX.Licensing.DlxAddIn.v5/DeployLX.Licensing.DlxAddIn.v5.Resources.Limits.TimeServer.png")]
	public class TimeServerLimit : Limit
	{
		private int _allowedDifference = 1200;

		private StringCollection _addresses = new StringCollection();

		private static readonly string[] _ntpServers = new string[14]
		{
			"time-a.nist.gov",
			"time-b.nist.gov",
			"time-a.timefreq.bldrdoc.gov",
			"time-b.timefreq.bldrdoc.gov",
			"time-c.timefreq.bldrdoc.gov",
			"utcnist.colorado.edu",
			"time.nist.gov",
			"time-nw.nist.gov",
			"nist1.datum.com",
			"nist1.dc.glassey.com",
			"nist1.ny.glassey.com",
			"nist1.sj.glassey.com",
			"nist1.aol-ca.trutime.com",
			"nist1.aol-va.truetime.com"
		};

		public override string Description
		{
			get
			{
				return SR.GetString("M_TimeServerLimitDescription");
			}
		}

		public override string Name
		{
			get
			{
				return "Time Server";
			}
		}

		public int AllowedDifference
		{
			get
			{
				return _allowedDifference;
			}
			set
			{
				base.AssertNotReadOnly();
				Check.GreaterThanEqual(value, 5, "AllowedDifference");
				if (_allowedDifference != value)
				{
					_allowedDifference = value;
					base.OnChanged("AllowedDifference");
				}
			}
		}

		public StringCollection Addresses
		{
			get
			{
				return _addresses;
			}
		}

		public override ValidationResult Validate(SecureLicenseContext context)
		{
			if (!IsValidTimeDifference(context, true))
			{
				return ValidationResult.Invalid;
			}
			return base.Validate(context);
		}

		protected virtual bool IsValidTimeDifference(SecureLicenseContext context, bool reportErrors)
		{
			if (context.RequestInfo.SimulateOffline)
			{
				if (reportErrors)
				{
					context.ReportError("E_SimulateOffline", this);
				}
				return false;
			}
			if (GetTimeDifference(context) > _allowedDifference)
			{
				if (reportErrors)
				{
					context.ReportError("E_ClockWrong", this);
				}
				return false;
			}
			return true;
		}

		protected int GetTimeDifference(SecureLicenseContext context)
		{
			object obj;
			if (_addresses.Count != 0)
			{
				IEnumerable addresses = _addresses;
				obj = addresses;
			}
			else
			{
				obj = _ntpServers;
			}
			IEnumerable enumerable = (IEnumerable)obj;
			byte[] array = new byte[52]
			{
				27,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0
			};
			bool flag = false;
			foreach (string item in enumerable)
			{
				UdpClient udpClient = new UdpClient();
				try
				{
					IPHostEntry iPHostEntry = Dns.Resolve(item);
					IPEndPoint endPoint = new IPEndPoint(iPHostEntry.AddressList[0], 123);
					udpClient.Connect(endPoint);
					Array.Clear(array, 0, array.Length);
					array[0] = 27;
					DateTime currentDateAndTime = context.CurrentDateAndTime;
					FormatDate(array, 40, currentDateAndTime);
					udpClient.Send(array, array.Length);
					array = udpClient.Receive(ref endPoint);
					udpClient.Close();
					DateTime currentDateAndTime2 = context.CurrentDateAndTime;
					double num = ParseMilliseconds(array, 40);
					double num2 = ParseMilliseconds(array, 32);
					double num3 = ParseMilliseconds(array, 24);
					double num4 = num - num3 - ((currentDateAndTime2 - currentDateAndTime).TotalMilliseconds - (num - num2)) / 2.0;
					return (int)Math.Abs(Math.Floor(num4 / 1000.0));
				}
				catch (Exception ex)
				{
					if (!flag && Toolbox.IsProxyException(ex, context))
					{
						flag = true;
					}
					else
					{
						context.ReportError("E_UnexpectedErrorFromServer", this, ex, ErrorSeverity.High, item);
					}
				}
				finally
				{
					udpClient.Close();
				}
			}
			return 2147483647;
		}

		private static void FormatDate(byte[] data, int offset, DateTime date)
		{
			double totalMilliseconds = (date - new DateTime(1900, 1, 1)).TotalMilliseconds;
			ulong num = (ulong)(totalMilliseconds / 1000.0);
			ulong num2 = (ulong)(totalMilliseconds % 1000.0 * 4294967296.0 / 1000.0);
			data[offset + 3] = (byte)(num & 0xFF);
			data[offset + 2] = (byte)(num >> 8 & 0xFF);
			data[offset + 1] = (byte)(num >> 16 & 0xFF);
			data[offset] = (byte)(num >> 24 & 0xFF);
			data[offset + 7] = (byte)(num2 & 0xFF);
			data[offset + 6] = (byte)(num2 >> 8 & 0xFF);
			data[offset + 5] = (byte)(num2 >> 16 & 0xFF);
			data[offset + 4] = (byte)(num2 >> 24 & 0xFF);
		}

		private static double ParseMilliseconds(byte[] data, int offset)
		{
			ulong num = (uint)(data[offset] << 24 | data[offset + 1] << 16 | data[offset + 2] << 8 | data[offset + 3]);
			ulong num2 = (uint)(data[offset + 4] << 24 | data[offset + 5] << 16 | data[offset + 6] << 8 | data[offset + 7]);
			return (double)(num * 1000L + num2 * 1000L / 4294967296uL);
		}

		public override PeekResult Peek(SecureLicenseContext context)
		{
			return PeekResult.Unknown;
		}

		protected override bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			if (_allowedDifference != 1200)
			{
				writer.WriteAttributeString("allowedDifference", _allowedDifference.ToString());
			}
			foreach (string item in (IEnumerable)_addresses)
			{
				writer.WriteStartElement("Server");
				writer.WriteAttributeString("address", SafeToolbox.XmlEncode(item));
				writer.WriteEndElement();
			}
			return true;
		}

		protected override bool ReadFromXml(XmlReader reader)
		{
			_addresses.Clear();
			string attribute = reader.GetAttribute("allowedDifference");
			if (attribute != null)
			{
				_allowedDifference = SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_allowedDifference = 1200;
			}
			if (reader.IsEmptyElement)
			{
				reader.Read();
				return true;
			}
			reader.Read();
			while (!reader.EOF)
			{
				if (reader.IsStartElement())
				{
					switch (reader.Name)
					{
					case "Limit":
						if (base.ReadChildLimit(reader))
						{
							break;
						}
						return false;
					case "Server":
						attribute = reader.GetAttribute("address");
						if (attribute != null)
						{
							_addresses.Add(SafeToolbox.XmlDecode(attribute));
							reader.Read();
							break;
						}
						return false;
					default:
						reader.Skip();
						break;
					}
					continue;
				}
				reader.Read();
				break;
			}
			return true;
		}
	}
}
