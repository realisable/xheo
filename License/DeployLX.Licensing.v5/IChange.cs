namespace DeployLX.Licensing.v5
{
	public interface IChange
	{
		event ChangeEventHandler Changed;

		void MakeReadOnly();
	}
}
