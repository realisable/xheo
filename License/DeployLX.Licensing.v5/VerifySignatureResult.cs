namespace DeployLX.Licensing.v5
{
	public enum VerifySignatureResult
	{
		Invalid,
		DifferentKeys,
		Valid
	}
}
