namespace DeployLX.Licensing.v5
{
	public static class SRC
	{
		public const string E_CouldNotInitializeFromSerialNumber = "E_CouldNotInitializeFromSerialNumber";

		public const string E_InvalidCharacterSet = "E_InvalidCharacterSet";

		public const string E_InvalidSerialNumber = "E_InvalidSerialNumber";

		public const string E_MissingSerialNumber = "E_MissingSerialNumber";

		public const string E_MissingSerialNumberUpgrade = "E_MissingSerialNumberUpgrade";

		public const string E_Only8FlagsAllowed = "E_Only8FlagsAllowed";

		public const string E_ProcessorAlreadyRegistered = "E_ProcessorAlreadyRegistered";

		public const string E_ProcessorMethodNotFound = "E_ProcessorMethodNotFound";

		public const string E_RequestingNewSerialNumber = "E_RequestingNewSerialNumber";

		public const string E_SerialNumberMismatch = "E_SerialNumberMismatch";

		public const string E_AdminRequiredFirstRun = "E_AdminRequiredFirstRun";

		public const string E_AspNetNotBuffered = "E_AspNetNotBuffered";

		public const string E_CacheReset = "E_CacheReset";

		public const string E_CouldNotValidate = "E_CouldNotValidate";

		public const string E_CouldNotValidateCached = "E_CouldNotValidateCached";

		public const string E_DelayedFailed = "E_DelayedFailed";

		public const string E_ErrorLoadingLicenseFile = "E_ErrorLoadingLicenseFile";

		public const string E_InvalidPublicKey = "E_InvalidPublicKey";

		public const string E_InvalidSearchPath = "E_InvalidSearchPath";

		public const string E_InvalidSignature = "E_InvalidSignature";

		public const string E_CannotValidateSignature = "E_CannotValidateSignature";

		public const string E_MissingPublicKey = "E_MissingPublicKey";

		public const string E_NoComponentInLicense = "E_NoComponentInLicense";

		public const string E_NoLicense = "E_NoLicense";

		public const string E_NoLicensesFound = "E_NoLicensesFound";

		public const string E_NoLicensesFoundSummary = "E_NoLicensesFoundSummary";

		public const string E_NoCompiledLicenses = "E_NoCompiledLicenses";

		public const string E_DifferentProduct = "E_DifferentProduct";

		public const string E_OriginalUpdated = "E_OriginalUpdated";

		public const string E_RetriedTooManyTimes = "E_RetriedTooManyTimes";

		public const string E_SkipTrial = "E_SkipTrial";

		public const string E_UnexpectedValidate = "E_UnexpectedValidate";

		public const string E_ValidationOverflow = "E_ValidationOverflow";

		public const string E_WebImpersonationNotAllowed = "E_WebImpersonationNotAllowed";

		public const string E_StoppedLooking = "E_StoppedLooking";

		public const string E_CouldNotGrant = "E_CouldNotGrant";

		public const string E_RSADenied = "E_RSADenied";

		public const string E_CouldNotAcquireSecureStorage = "E_CouldNotAcquireSecureStorage";

		public const string E_LimitRequiresVersion = "E_LimitRequiresVersion";

		public const string E_LimitFeatureRequiresVersion = "E_LimitFeatureRequiresVersion";

		public const string E_AbortedByUser = "E_AbortedByUser";

		public const string E_FormClosed = "E_FormClosed";

		public const string E_InvalidIdForLicense = "E_InvalidIdForLicense";

		public const string E_InvalidRegistryKey = "E_InvalidRegistryKey";

		public const string E_InvalidUrl = "E_InvalidUrl";

		public const string E_LicenseReadOnly = "E_LicenseReadOnly";

		public const string E_PropertyAtLeast = "E_PropertyAtLeast";

		public const string E_PropertyCannotBeNull = "E_PropertyCannotBeNull";

		public const string E_PropertyNotSet = "E_PropertyNotSet";

		public const string E_ReadOnlyObject = "E_ReadOnlyObject";

		public const string E_ReadOnlyProperty = "E_ReadOnlyProperty";

		public const string E_RequiresDeveloperMode = "E_RequiresDeveloperMode";

		public const string E_UnbalancedUpdate = "E_UnbalancedUpdate";

		public const string E_UnknownVersionValue = "E_UnknownVersionValue";

		public const string E_CouldNotCreateEmail = "E_CouldNotCreateEmail";

		public const string E_LimitDoesNotBelongToLicense = "E_LimitDoesNotBelongToLicense";

		public const string E_SecureStorageTampered = "E_SecureStorageTampered";

		public const string E_UnknownValidationState = "E_UnknownValidationState";

		public const string E_FloatingNotSupported = "E_FloatingNotSupported";

		public const string E_InvalidResponseFromServer = "E_InvalidResponseFromServer";

		public const string E_ReferenceLimitMustBeInReferenceLicense = "E_ReferenceLimitMustBeInReferenceLicense";

		public const string E_UnexpectedErrorFromServer = "E_UnexpectedErrorFromServer";

		public const string E_SimulateOffline = "E_SimulateOffline";

		public const string E_CannotChangeAssociatedLicense = "E_CannotChangeAssociatedLicense";

		public const string E_DelayAlreadyRegistered = "E_DelayAlreadyRegistered";

		public const string E_DelayedWithoutListeners = "E_DelayedWithoutListeners";

		public const string E_InvalidCustomGuiType = "E_InvalidCustomGuiType";

		public const string E_LimitDoesNotExist = "E_LimitDoesNotExist";

		public const string E_3xCustomLimit = "E_3xCustomLimit";

		public const string E_PersistentDataMustBePrimitive = "E_PersistentDataMustBePrimitive";

		public const string E_TemplateVariableValueMismatch = "E_TemplateVariableValueMismatch";

		public const string E_InvalidPageId = "E_InvalidPageId";

		public const string E_CannotSaveEncryptedWithoutKey = "E_CannotSaveEncryptedWithoutKey";

		public const string E_CannotSaveLicenseInvalid = "E_CannotSaveLicenseInvalid";

		public const string E_CouldNotLoadLicenseFile = "E_CouldNotLoadLicenseFile";

		public const string E_InvalidLicenseFile = "E_InvalidLicenseFile";

		public const string E_InvalidVersion = "E_InvalidVersion";

		public const string E_LicenseStillEncrypted = "E_LicenseStillEncrypted";

		public const string E_LimitValueMismatch = "E_LimitValueMismatch";

		public const string E_MissingXmlAttribute = "E_MissingXmlAttribute";

		public const string E_OldVersion = "E_OldVersion";

		public const string E_CouldNotCompleteSave = "E_CouldNotCompleteSave";

		public const string E_ISNotFipsCompliant = "E_ISNotFipsCompliant";

		public const string E_CannotOverwriteValidatedLicense = "E_CannotOverwriteValidatedLicense";

		public const string E_CannotSaveDisposedLicense = "E_CannotSaveDisposedLicense";

		public const string E_ConfigurationCannotUseAnd = "E_ConfigurationCannotUseAnd";

		public const string E_ConfigurationUnexpected = "E_ConfigurationUnexpected";

		public const string E_InvalidConfigurationTag = "E_InvalidConfigurationTag";

		public const string E_InvalidConfigurationValue = "E_InvalidConfigurationValue";

		public const string E_MissingConfigurationAttribute = "E_MissingConfigurationAttribute";

		public const string E_UnknownConfigurationAttribute = "E_UnknownConfigurationAttribute";

		public const string M_VersionLimitDescription = "M_VersionLimitDescription";

		public const string E_VersionMismatch = "E_VersionMismatch";

		public const string E_VersionMismatchRange = "E_VersionMismatchRange";

		public const string M_BetaLimitDescription = "M_BetaLimitDescription";

		public const string E_BetaExpired = "E_BetaExpired";

		public const string E_BetaExpiredUrl = "E_BetaExpiredUrl";

		public const string M_FeatureLimitDescription = "M_FeatureLimitDescription";

		public const string E_FeatureNotEnabled = "E_FeatureNotEnabled";

		public const string E_DontAddFeatureNames = "E_DontAddFeatureNames";

		public const string E_FeatureNameCannotHaveSemiColons = "E_FeatureNameCannotHaveSemiColons";

		public const string M_FeatureFilterLimitDescription = "M_FeatureFilterLimitDescription";

		public const string M_DesigntimeLimitDescription = "M_DesigntimeLimitDescription";

		public const string E_DesigntimeOnly = "E_DesigntimeOnly";

		public const string M_RuntimeLimitDescription = "M_RuntimeLimitDescription";

		public const string E_RuntimeOnly = "E_RuntimeOnly";

		public const string M_OrLimitDescription = "M_OrLimitDescription";

		public const string E_NoValidChildLimits = "E_NoValidChildLimits";

		public const string M_RemoteDesktopLimitDescription = "M_RemoteDesktopLimitDescription";

		public const string E_CannotAccessWithRemoteDesktop = "E_CannotAccessWithRemoteDesktop";

		public const string M_ApplicationTypeLimitDescription = "M_ApplicationTypeLimitDescription";

		public const string M_TimeServerLimitDescription = "M_TimeServerLimitDescription";

		public const string E_ClockWrong = "E_ClockWrong";

		public const string M_TimeLimitDescription = "M_TimeLimitDescription";

		public const string M_TimeRemaining = "M_TimeRemaining";

		public const string E_TimeExpired = "E_TimeExpired";

		public const string E_TimeNotAbsolute = "E_TimeNotAbsolute";

		public const string E_InvalidClock = "E_InvalidClock";

		public const string E_TestDateEarlierThanSystem = "E_TestDateEarlierThanSystem";

		public const string E_TimeNotValidated = "E_TimeNotValidated";

		public const string E_DateTooFarInFutureUseMaxTime64 = "E_DateTooFarInFutureUseMaxTime64";

		public const string E_CannotReduceUsage = "E_CannotReduceUsage";

		public const string E_UseLimitReached = "E_UseLimitReached";

		public const string M_UseLimitDescription = "M_UseLimitDescription";

		public const string M_UsesRemaining = "M_UsesRemaining";

		public const string M_TrialLimitDescription = "M_TrialLimitDescription";

		public const string E_TrialExpired = "E_TrialExpired";

		public const string E_DidNotAcceptTrial = "E_DidNotAcceptTrial";

		public const string M_OSLimitDescription = "M_OSLimitDescription";

		public const string E_OSNotAllowed = "E_OSNotAllowed";

		public const string E_CannotExtendAtServer = "E_CannotExtendAtServer";

		public const string E_InvalidExtensionCode = "E_InvalidExtensionCode";

		public const string E_ExpiredExtensionCode = "E_ExpiredExtensionCode";

		public const string UI_Extend = "UI_Extend";

		public const string UI_ExtendingOnlineNotice = "UI_ExtendingOnlineNotice";

		public const string UI_ExtendLabel = "UI_ExtendLabel";

		public const string UI_ExtendOnline = "UI_ExtendOnline";

		public const string UI_Renew = "UI_Renew";

		public const string UI_RenewOnline = "UI_RenewOnline";

		public const string UI_ExtendTitle = "UI_ExtendTitle";

		public const string UI_ExtensionExpired = "UI_ExtensionExpired";

		public const string UI_ExtensionPrompt = "UI_ExtensionPrompt";

		public const string UI_ExtensionPromptOnlineOnly = "UI_ExtensionPromptOnlineOnly";

		public const string UI_ExtensionPromptCodeOnly = "UI_ExtensionPromptCodeOnly";

		public const string UI_ExtensionSuccess = "UI_ExtensionSuccess";

		public const string UI_ExtensionOnlineErrors = "UI_ExtensionOnlineErrors";

		public const string UI_InvalidExtensionCode = "UI_InvalidExtensionCode";

		public const string UI_InvalidExtensionCodeTitle = "UI_InvalidExtensionCodeTitle";

		public const string UI_LimitCode = "UI_LimitCode";

		public const string UI_MissingExtensionCode = "UI_MissingExtensionCode";

		public const string UI_RenewLabel = "UI_RenewLabel";

		public const string UI_SubscriptionExpired = "UI_SubscriptionExpired";

		public const string UI_SubscriptionPrompt = "UI_SubscriptionPrompt";

		public const string UI_SubscriptionPromptCodeOnly = "UI_SubscriptionPromptCodeOnly";

		public const string UI_SubscriptionPromptOnlineOnly = "UI_SubscriptionPromptOnlineOnly";

		public const string UI_RenewingOnlineNotice = "UI_RenewingOnlineNotice";

		public const string UI_RenewalOnlineErrors = "UI_RenewalOnlineErrors";

		public const string UI_SubscriptionPurchase = "UI_SubscriptionPurchase";

		public const string UI_SubscriptionTitle = "UI_SubscriptionTitle";

		public const string M_CopyExtension = "M_CopyExtension";

		public const string M_DomainLimitDescription = "M_DomainLimitDescription";

		public const string E_InvalidDomain = "E_InvalidDomain";

		public const string M_InvalidLimitDescription = "M_InvalidLimitDescription";

		public const string E_LicenseMarkedInvalid = "E_LicenseMarkedInvalid";

		public const string M_DefaultInvalidLicenseMessage = "M_DefaultInvalidLicenseMessage";

		public const string M_IPLimitDescription = "M_IPLimitDescription";

		public const string E_InvalidIPRange = "E_InvalidIPRange";

		public const string E_InvalidMachineIP = "E_InvalidMachineIP";

		public const string E_InvalidRequestIP = "E_InvalidRequestIP";

		public const string E_IPMustBeSameFamily = "E_IPMustBeSameFamily";

		public const string M_ApplicationLimitDescription = "M_ApplicationLimitDescription";

		public const string E_CouldNotCreateApplicationMonitor = "E_CouldNotCreateApplicationMonitor";

		public const string E_TooManyApplications = "E_TooManyApplications";

		public const string M_ResetLimitDescription = "M_ResetLimitDescription";

		public const string E_TargetResetLimitDoesNotExist = "E_TargetResetLimitDoesNotExist";

		public const string E_TargetLimitIsNotResettable = "E_TargetLimitIsNotResettable";

		public const string M_StateLimitDescription = "M_StateLimitDescription";

		public const string M_StateSkipped = "M_StateSkipped";

		public const string E_StateParentNotLimit = "E_StateParentNotLimit";

		public const string E_LimitDoesNotSupportMultipleStates = "E_LimitDoesNotSupportMultipleStates";

		public const string E_MissingStates = "E_MissingStates";

		public const string E_UnkownState = "E_UnkownState";

		public const string M_SessionsLimitDescription = "M_SessionsLimitDescription";

		public const string E_MissionContextOrSession = "E_MissionContextOrSession";

		public const string E_SessionTimeoutTooShort = "E_SessionTimeoutTooShort";

		public const string E_TooManySessions = "E_TooManySessions";

		public const string M_ServiceLimitDescription = "M_ServiceLimitDescription";

		public const string E_CannotUseOnServerOrService = "E_CannotUseOnServerOrService";

		public const string E_MustUseOnServerOrService = "E_MustUseOnServerOrService";

		public const string E_ServerOrServiceMismatch = "E_ServerOrServiceMismatch";

		public const string M_ActivationLimitDescription = "M_ActivationLimitDescription";

		public const string M_CopyActivation = "M_CopyActivation";

		public const string M_ActivationRequest = "M_ActivationRequest";

		public const string M_ProfileAny = "M_ProfileAny";

		public const string M_ProfileDesktop = "M_ProfileDesktop";

		public const string M_ProfileLaptop = "M_ProfileLaptop";

		public const string M_ProfilePhysical = "M_ProfilePhysical";

		public const string M_ProfileVirtual = "M_ProfileVirtual";

		public const string M_ProfileNone = "M_ProfileNone";

		public const string E_ActivationCodeInvalid = "E_ActivationCodeInvalid";

		public const string E_AdditionalProfileDataTooLong = "E_AdditionalProfileDataTooLong";

		public const string E_CannotActivateByKey = "E_CannotActivateByKey";

		public const string E_CouldNotActivateAtServer = "E_CouldNotActivateAtServer";

		public const string E_CannotDeactivate = "E_CannotDeactivate";

		public const string E_CouldNotDeactivateAtServer = "E_CouldNotDeactivateAtServer";

		public const string E_CouldNotDeactivate = "E_CouldNotDeactivate";

		public const string E_HardwareListInconsistent = "E_HardwareListInconsistent";

		public const string E_MachineNotActivated = "E_MachineNotActivated";

		public const string E_MachineMismatch = "E_MachineMismatch";

		public const string E_MachineMismatchCheck = "E_MachineMismatchCheck";

		public const string E_FileMovedAndInvalidateOnMove = "E_FileMovedAndInvalidateOnMove";

		public const string E_ProfileWasDeactivated = "E_ProfileWasDeactivated";

		public const string E_InvalidUnlockCodeForProfile = "E_InvalidUnlockCodeForProfile";

		public const string E_NoServersDefined = "E_NoServersDefined";

		public const string E_TooMuchHardware = "E_TooMuchHardware";

		public const string E_TooManyHardwareVariations = "E_TooManyHardwareVariations";

		public const string E_GracePeriodDefinedByState = "E_GracePeriodDefinedByState";

		public const string UI_Activate = "UI_Activate";

		public const string UI_ActivateLater = "UI_ActivateLater";

		public const string UI_ActivateManually = "UI_ActivateManually";

		public const string UI_ActivateManuallyNotice = "UI_ActivateManuallyNotice";

		public const string UI_ActivateManuallyTitle = "UI_ActivateManuallyTitle";

		public const string UI_ActivateOnline = "UI_ActivateOnline";

		public const string UI_ActivateOnlineTitle = "UI_ActivateOnlineTitle";

		public const string UI_ActivateTitle = "UI_ActivateTitle";

		public const string UI_ActivatingOnlineNotice = "UI_ActivatingOnlineNotice";

		public const string UI_ActivationCode = "UI_ActivationCode";

		public const string UI_MachineNickname = "UI_MachineNickname";

		public const string UI_ActivationOnlineErrors = "UI_ActivationOnlineErrors";

		public const string UI_ActivationSuccess = "UI_ActivationSuccess";

		public const string UI_ActivationNotice = "UI_ActivationNotice";

		public const string UI_MachineCode = "UI_MachineCode";

		public const string UI_ReactivationNotice = "UI_ReactivationNotice";

		public const string UI_SelectActivationProfile = "UI_SelectActivationProfile";

		public const string UI_TellMeMoreActivation = "UI_TellMeMoreActivation";

		public const string UI_WebActivationHeaderNotice = "UI_WebActivationHeaderNotice";

		public const string UI_EnterNewSerialActivation = "UI_EnterNewSerialActivation";

		public const string UI_ActivationNicknameNote = "UI_ActivationNicknameNote";

		public const string UI_ActivationMachineWarning = "UI_ActivationMachineWarning";

		public const string UI_Deactivate = "UI_Deactivate";

		public const string UI_DeactivateTitle = "UI_DeactivateTitle";

		public const string UI_DeactivatingNotice = "UI_DeactivatingNotice";

		public const string UI_DeactivationCompleteNotice = "UI_DeactivationCompleteNotice";

		public const string UI_DeactivationConfirmation = "UI_DeactivationConfirmation";

		public const string UI_DeactivationFailedNotice = "UI_DeactivationFailedNotice";

		public const string UI_DeactivationMachineWarning = "UI_DeactivationMachineWarning";

		public const string UI_DeactivationNotice = "UI_DeactivationNotice";

		public const string UI_DeactivationServerNotice = "UI_DeactivationServerNotice";

		public const string UI_PossibleErrorsDeactivation = "UI_PossibleErrorsDeactivation";

		public const string UI_AboutActivationTitle = "UI_AboutActivationTitle";

		public const string UI_AboutActivationSummary = "UI_AboutActivationSummary";

		public const string UI_AboutActivationRequiredTitle = "UI_AboutActivationRequiredTitle";

		public const string UI_AboutActivationRequiredSummary = "UI_AboutActivationRequiredSummary";

		public const string UI_AboutActivationRequiredSimpleSummary = "UI_AboutActivationRequiredSimpleSummary";

		public const string UI_AboutActivationTelephoneEmailOnline = "UI_AboutActivationTelephoneEmailOnline";

		public const string UI_AboutActivationTelephoneOnline = "UI_AboutActivationTelephoneOnline";

		public const string UI_AboutActivationEmailOnline = "UI_AboutActivationEmailOnline";

		public const string UI_AboutActivationOnline = "UI_AboutActivationOnline";

		public const string UI_AboutActivationTelephoneEmail = "UI_AboutActivationTelephoneEmail";

		public const string UI_AboutActivationTelephone = "UI_AboutActivationTelephone";

		public const string UI_AboutActivationEmail = "UI_AboutActivationEmail";

		public const string UI_AboutActivationImmediately = "UI_AboutActivationImmediately";

		public const string UI_AboutActivationToActivate = "UI_AboutActivationToActivate";

		public const string UI_AboutActivationPrivacy = "UI_AboutActivationPrivacy";

		public const string UI_AboutActivationReinstall = "UI_AboutActivationReinstall";

		public const string UI_AboutActivationHardware = "UI_AboutActivationHardware";

		public const string UI_AboutActivationHardwareMajor = "UI_AboutActivationHardwareMajor";

		public const string UI_AboutActivationDeactivate = "UI_AboutActivationDeactivate";

		public const string M_PublisherLimitDescription = "M_PublisherLimitDescription";

		public const string E_InvalidPublisher = "E_InvalidPublisher";

		public const string E_PublisherWithToken = "E_PublisherWithToken";

		public const string M_HtmlBrandedLimitDescription = "M_HtmlBrandedLimitDescription";

		public const string E_CouldNotGetPage = "E_CouldNotGetPage";

		public const string M_LicenseServerLimitDescription = "M_LicenseServerLimitDescription";

		public const string E_CannotValidateAtServer = "E_CannotValidateAtServer";

		public const string E_ModifiedOrUnlocked = "E_ModifiedOrUnlocked";

		public const string E_CannotRenewAtServer = "E_CannotRenewAtServer";

		public const string M_ScriptLimitDescription = "M_ScriptLimitDescription";

		public const string E_InvalidScriptLanguage = "E_InvalidScriptLanguage";

		public const string E_CouldNotCompileScript = "E_CouldNotCompileScript";

		public const string E_InvalidInvokeSignature = "E_InvalidInvokeSignature";

		public const string E_MustDefineCustomGui = "E_MustDefineCustomGui";

		public const string M_UpgradeLimitDescription = "M_UpgradeLimitDescription";

		public const string E_NoUpgradeSerialNumber = "E_NoUpgradeSerialNumber";

		public const string M_CompileLimitDescription = "M_CompileLimitDescription";

		public const string E_LicxRequired = "E_LicxRequired";

		public const string M_RegistrationLimitDescription = "M_RegistrationLimitDescription";

		public const string E_NotRegistered = "E_NotRegistered";

		public const string E_CannotCreateCustomControl = "E_CannotCreateCustomControl";

		public const string E_CouldNotRegister = "E_CouldNotRegister";

		public const string E_CouldNotRegisterWithServer = "E_CouldNotRegisterWithServer";

		public const string E_InvalidSerialOrFailure = "E_InvalidSerialOrFailure";

		public const string UI_RegistrationTitle = "UI_RegistrationTitle";

		public const string UI_RegistrationNotice = "UI_RegistrationNotice";

		public const string UI_RegistrationMandatoryNotice = "UI_RegistrationMandatoryNotice";

		public const string UI_RegistrationOrTrialNotice = "UI_RegistrationOrTrialNotice";

		public const string UI_RegistrationServerNotice = "UI_RegistrationServerNotice";

		public const string UI_RegistrationServerRequiredNotice = "UI_RegistrationServerRequiredNotice";

		public const string UI_RegisterLater = "UI_RegisterLater";

		public const string UI_DontRegister = "UI_DontRegister";

		public const string UI_FieldRequiredNotice = "UI_FieldRequiredNotice";

		public const string UI_ActivateAutoOnline = "UI_ActivateAutoOnline";

		public const string UI_UpgradeTitle = "UI_UpgradeTitle";

		public const string UI_UpgradeNotice = "UI_UpgradeNotice";

		public const string UI_PreviousSerialNumber = "UI_PreviousSerialNumber";

		public const string UI_RegistrationSuccess = "UI_RegistrationSuccess";

		public const string UI_RegisteringOnlineNotice = "UI_RegisteringOnlineNotice";

		public const string UI_RegistrationOnlineErrors = "UI_RegistrationOnlineErrors";

		public const string M_SplashLimitDescription = "M_SplashLimitDescription";

		public const string E_CouldNotGetResource = "E_CouldNotGetResource";

		public const string M_TypeLimitDescription = "M_TypeLimitDescription";

		public const string E_TypeNotEnabled = "E_TypeNotEnabled";

		public const string M_OnlineLimitDescription = "M_OnlineLimitDescription";

		public const string M_VirtualMachineLimitDescription = "M_VirtualMachineLimitDescription";

		public const string UI_OK = "UI_OK";

		public const string UI_Abort = "UI_Abort";

		public const string UI_Cancel = "UI_Cancel";

		public const string UI_Close = "UI_Close";

		public const string UI_Details = "UI_Details";

		public const string UI_DetailsCaption = "UI_DetailsCaption";

		public const string UI_ErrorCaption = "UI_ErrorCaption";

		public const string UI_Ignore = "UI_Ignore";

		public const string UI_No = "UI_No";

		public const string UI_Retry = "UI_Retry";

		public const string UI_Yes = "UI_Yes";

		public const string UI_Stop = "UI_Stop";

		public const string UI_GoBack = "UI_GoBack";

		public const string UI_MessageBoxExceptionMessage = "UI_MessageBoxExceptionMessage";

		public const string UI_CopyrightWarning = "UI_CopyrightWarning";

		public const string UI_Version = "UI_Version";

		public const string UI_Username = "UI_Username";

		public const string UI_Password = "UI_Password";

		public const string UI_Credentials = "UI_Credentials";

		public const string UI_Authorize = "UI_Authorize";

		public const string UI_CopyToEmail = "UI_CopyToEmail";

		public const string UI_CopyToClipboard = "UI_CopyToClipboard";

		public const string UI_LicenseAgreement = "UI_LicenseAgreement";

		public const string UI_BuyNow = "UI_BuyNow";

		public const string UI_Try = "UI_Try";

		public const string UI_MoreInfo = "UI_MoreInfo";

		public const string UI_Register = "UI_Register";

		public const string UI_MachineProfile = "UI_MachineProfile";

		public const string UI_SerialNumber = "UI_SerialNumber";

		public const string UI_ValidationNoticeTitle = "UI_ValidationNoticeTitle";

		public const string UI_FieldRequired = "UI_FieldRequired";

		public const string UI_Name = "UI_Name";

		public const string UI_Organization = "UI_Organization";

		public const string UI_PrivacyPolicy = "UI_PrivacyPolicy";

		public const string UI_EnterExtensionCode = "UI_EnterExtensionCode";

		public const string UI_ErrorsConnectingToServer = "UI_ErrorsConnectingToServer";

		public const string UI_DetailsCopied = "UI_DetailsCopied";

		public const string UI_DetailsCopiedTitle = "UI_DetailsCopiedTitle";

		public const string UI_SelectLicenseFile = "UI_SelectLicenseFile";

		public const string UI_LicenseFiles = "UI_LicenseFiles";

		public const string UI_LicenseFileExists = "UI_LicenseFileExists";

		public const string UI_SupportInfo = "UI_SupportInfo";

		public const string UI_EmailSupport = "UI_EmailSupport";

		public const string UI_VisitSupportWebsite = "UI_VisitSupportWebsite";

		public const string UI_ProxyFormTitle = "UI_ProxyFormTitle";

		public const string UI_ProxyServerAddress = "UI_ProxyServerAddress";

		public const string UI_ProxyHelpNote = "UI_ProxyHelpNote";

		public const string UI_ProxyNotice = "UI_ProxyNotice";

		public const string UI_AdminFormTitle = "UI_AdminFormTitle";

		public const string UI_AdminRequiredNotice = "UI_AdminRequiredNotice";

		public const string UI_AdminVistaNotice = "UI_AdminVistaNotice";

		public const string UI_AdminVistaNotice11 = "UI_AdminVistaNotice11";

		public const string UI_Admin2000Notice11 = "UI_Admin2000Notice11";

		public const string UI_CouldNotAuthorizeLicense = "UI_CouldNotAuthorizeLicense";

		public const string UI_PleaseEnterValidCredentials = "UI_PleaseEnterValidCredentials";

		public const string UI_UserIsNotAdmin = "UI_UserIsNotAdmin";

		public const string UI_InvalidCredentials = "UI_InvalidCredentials";

		public const string UI_LicenseNotFoundTitle = "UI_LicenseNotFoundTitle";

		public const string UI_LicenseNotFoundSubTitle = "UI_LicenseNotFoundSubTitle";

		public const string UI_ProblemContectingToServerProxy = "UI_ProblemContectingToServerProxy";

		public const string UI_ErrorReport = "UI_ErrorReport";

		public const string UI_CopyDetails = "UI_CopyDetails";

		public const string UI_ShowDetails = "UI_ShowDetails";

		public const string UI_DetailsReport = "UI_DetailsReport";

		public const string UI_Continue = "UI_Continue";

		public const string UI_DetailsTab = "UI_DetailsTab";

		public const string UI_AssembliesTab = "UI_AssembliesTab";

		public const string UI_SystemInfoTab = "UI_SystemInfoTab";

		public const string UI_NextSteps = "UI_NextSteps";

		public const string UI_CopyDetailsToClipboard = "UI_CopyDetailsToClipboard";

		public const string UI_GenerateDetailedEmailReport = "UI_GenerateDetailedEmailReport";

		public const string M_NoLicenseWebsite = "M_NoLicenseWebsite";

		public const string M_NoLicenseEmail = "M_NoLicenseEmail";

		public const string M_NoLicensePhone = "M_NoLicensePhone";

		public const string M_NoLicenseReason = "M_NoLicenseReason";

		public const string M_NoLicenseSummary = "M_NoLicenseSummary";

		public const string M_DynamicAssembly = "M_DynamicAssembly";

		public const string M_Platform = "M_Platform";

		public const string M_EntryAssembly = "M_EntryAssembly";

		public const string M_BaseDirectory = "M_BaseDirectory";

		public const string M_OSVersion = "M_OSVersion";

		public const string M_CLRVersion = "M_CLRVersion";

		public const string M_ProcessBitness = "M_ProcessBitness";

		public const string M_VirtualMachine = "M_VirtualMachine";

		public const string M_Culture = "M_Culture";

		public const string M_Encoding = "M_Encoding";

		public const string M_IISAvailable = "M_IISAvailable";

		public const string M_Timestamp = "M_Timestamp";

		public const string M_LicenseFailureDetails = "M_LicenseFailureDetails";

		public const string M_SystemInfo = "M_SystemInfo";

		public const string M_Assemblies = "M_Assemblies";

		public const string M_StackTrace = "M_StackTrace";

		public const string M_DetailsHeader = "M_DetailsHeader";

		public const string M_FailureReport = "M_FailureReport";

		public const string M_MachineCode = "M_MachineCode";

		public const string M_Seconds = "M_Seconds";

		public const string M_Minutes = "M_Minutes";

		public const string M_Hours = "M_Hours";

		public const string M_Days = "M_Days";

		public const string M_Second = "M_Second";

		public const string M_Minute = "M_Minute";

		public const string M_Hour = "M_Hour";

		public const string M_Day = "M_Day";

		public const string M_And = "M_And";

		public const string M_Or = "M_Or";

		public const string M_TheManufacturer = "M_TheManufacturer";

		public const string M_NA = "M_NA";

		public const string M_CurrentAssembly = "M_CurrentAssembly";

		public const string M_SuggestionCache = "M_SuggestionCache";

		public const string M_ExceptionPrompt = "M_ExceptionPrompt";

		public const string M_UsingDeveloperMode = "M_UsingDeveloperMode";

		public const string M_RestoredSecureStorage = "M_RestoredSecureStorage";
	}
}
