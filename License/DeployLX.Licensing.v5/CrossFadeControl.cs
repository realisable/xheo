using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Threading;
using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	internal sealed class CrossFadeControl : Control
	{
		private delegate void FadeDelegate(Bitmap fromBitmap, Bitmap toBitmap, int milliseconds);

		private bool _dontAutoHide;

		private IDisposable _boost;

		private float[][] _identity = new float[5][]
		{
			new float[5]
			{
				1f,
				0f,
				0f,
				0f,
				0f
			},
			new float[5]
			{
				0f,
				1f,
				0f,
				0f,
				0f
			},
			new float[5]
			{
				0f,
				0f,
				1f,
				0f,
				0f
			},
			new float[5]
			{
				0f,
				0f,
				0f,
				1f,
				0f
			},
			new float[5]
			{
				0f,
				0f,
				0f,
				0f,
				1f
			}
		};

		private Bitmap _from;

		private Bitmap _to;

		private float _percent;

		private float _faderStep;

		private System.Windows.Forms.Timer _timer;

		private ColorMatrix _fadeMatrix;

		private ImageAttributes _imageAttributes;

		private Rectangle _renderRectangle;

		private Bitmap _fadeBitmap;

		public bool IsFading
		{
			get
			{
				return _timer.Enabled;
			}
		}

		public event EventHandler Finished;

		private void OnFinished(EventArgs e)
		{
			if (this.Finished != null)
			{
				this.Finished(this, e);
			}
		}

		public CrossFadeControl()
		{
			base.Visible = false;
			base.SetStyle(ControlStyles.UserPaint | ControlStyles.Opaque | ControlStyles.AllPaintingInWmPaint, true);
			_timer = new System.Windows.Forms.Timer();
			_timer.Tick += _timer_Tick;
			_fadeMatrix = new ColorMatrix(_identity);
			_imageAttributes = new ImageAttributes();
		}

		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
			ReleaseBitmaps();
			if (_imageAttributes != null)
			{
				_imageAttributes.Dispose();
				_imageAttributes = null;
			}
			if (_fadeBitmap != null)
			{
				_fadeBitmap.Dispose();
				_fadeBitmap = null;
			}
			_timer.Stop();
			_timer.Dispose();
		}

		private void ReleaseBitmaps()
		{
			if (_from != null)
			{
				_from.Dispose();
			}
			if (_to != null)
			{
				_to.Dispose();
			}
			_from = null;
			_to = null;
		}

		public void Fade(Bitmap from, Bitmap to, int milliseconds, bool dontAutoHide)
		{
			Check.NotNull(from, "from");
			if (base.InvokeRequired)
			{
				base.BeginInvoke(new FadeDelegate(Fade), from, to, milliseconds);
			}
			else
			{
				if (_from != null && from != _from && to != _from)
				{
					_from.Dispose();
				}
				if (_to != null && _to != to && _to != from)
				{
					_to.Dispose();
				}
				_from = from;
				_to = to;
				_percent = 1f;
				if (_to != null)
				{
					_boost = Toolbox.ThreadBoost(ThreadPriority.Highest);
					_renderRectangle = new Rectangle(0, 0, from.Width, from.Height);
					int num = milliseconds / 50;
					if (num <= 2)
					{
						_timer.Interval = 5;
						_faderStep = 5f / (float)milliseconds;
					}
					else
					{
						_faderStep = 0.02f;
						_timer.Interval = num;
					}
					_timer.Start();
				}
				else if (_from == null)
				{
					base.Hide();
					OnFinished(EventArgs.Empty);
				}
				_dontAutoHide = dontAutoHide;
				base.Invalidate();
				base.Show();
				base.BringToFront();
			}
		}

		public void Fade(Bitmap fromBitmap, Bitmap toBitmap, int milliseconds)
		{
			Fade(fromBitmap, toBitmap, milliseconds, false);
		}

		public void Stop()
		{
			if (base.InvokeRequired)
			{
				base.BeginInvoke(new MethodInvoker(Stop));
			}
			else
			{
				_boost.Dispose();
				_timer.Stop();
				ReleaseBitmaps();
				if (!_dontAutoHide)
				{
					base.Hide();
					OnFinished(EventArgs.Empty);
				}
				_dontAutoHide = false;
			}
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			if (_from != null)
			{
				if (_to == null)
				{
					e.Graphics.DrawImage(_from, 0, 0);
				}
				else
				{
					if (_fadeBitmap == null)
					{
						_fadeBitmap = new Bitmap(base.Width, base.Height, PixelFormat.Format24bppRgb);
					}
					using (Graphics graphics = Graphics.FromImage(_fadeBitmap))
					{
						graphics.DrawImage(_to, _renderRectangle, 0, 0, _to.Width, _to.Height, GraphicsUnit.Pixel);
						_fadeMatrix[3, 3] = _percent;
						_imageAttributes.ClearColorMatrix();
						_imageAttributes.SetColorMatrix(_fadeMatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
						graphics.DrawImage(_from, _renderRectangle, 0, 0, _to.Width, _to.Height, GraphicsUnit.Pixel, _imageAttributes);
					}
					e.Graphics.DrawImage(_fadeBitmap, 0, 0);
				}
			}
			else if (_fadeBitmap != null)
			{
				e.Graphics.DrawImage(_fadeBitmap, 0, 0);
			}
			else
			{
				base.OnPaint(e);
			}
		}

		protected override void OnResize(EventArgs e)
		{
			base.OnResize(e);
			if (_fadeBitmap != null)
			{
				_fadeBitmap.Dispose();
			}
			_fadeBitmap = null;
		}

		private void _timer_Tick(object sender, EventArgs e)
		{
			if (base.InvokeRequired)
			{
				base.BeginInvoke(new EventHandler(_timer_Tick), sender, e);
			}
			else
			{
				_percent -= _faderStep;
				if (_percent > 0f)
				{
					base.Invalidate(false);
				}
				else if (_percent < 0f)
				{
					Stop();
				}
			}
		}
	}
}
