using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Web;

namespace DeployLX.Licensing.v5
{
	public class DefaultLicenseLogger
	{
		private static DefaultLicenseLogger _instance;

		public static DefaultLicenseLogger Instance
		{
			get
			{
				return _instance ?? (_instance = new DefaultLicenseLogger());
			}
		}

		public string LogPath
		{
			get;
			set;
		}

		public DefaultLicenseLogger()
		{
			if (SafeToolbox.IsWebRequest)
			{
				LogPath = HttpContext.Current.Request.MapPath("~/App_Data/DeployLX");
			}
			else
			{
				LogPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "XHEO INC\\DeployLX\\Logs");
			}
		}

		public void Log(SecureLicenseContext context, string diagnosticReport)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}
			if (diagnosticReport == null)
			{
				throw new ArgumentNullException("diagnosticReport");
			}
			if (LogPath != null)
			{
				string currentLogFile = GetCurrentLogFile(context);
				for (int i = 0; i < 3; i++)
				{
					try
					{
						using (StreamWriter streamWriter = new StreamWriter(currentLogFile, true, new UTF8Encoding(false)))
						{
							streamWriter.WriteLine(diagnosticReport);
							streamWriter.WriteLine();
							streamWriter.WriteLine("######################################################################");
							streamWriter.WriteLine();
						}
						return;
					}
					catch (IOException)
					{
						Thread.Sleep(3000);
					}
				}
			}
		}

		private string GetSafeName(string name)
		{
			StringBuilder stringBuilder = new StringBuilder(name);
			List<char> list = new List<char>(Path.GetInvalidPathChars());
			for (int i = 0; i < stringBuilder.Length; i++)
			{
				if (list.Contains(stringBuilder[i]))
				{
					list[i] = '_';
				}
			}
			return stringBuilder.ToString();
		}

		private string GetCurrentLogFile(SecureLicenseContext context)
		{
			FileInfo fileInfo = null;
			string text = LogPath;
			if (context.RequestInfo.SupportInfo.Company != null)
			{
				text = Path.Combine(text, GetSafeName(context.RequestInfo.SupportInfo.Company));
			}
			if (context.RequestInfo.SupportInfo.Product != null)
			{
				text = Path.Combine(text, GetSafeName(context.RequestInfo.SupportInfo.Product));
			}
			if (text == LogPath)
			{
				text = Path.Combine(text, Assembly.GetEntryAssembly().GetName().Name);
			}
			if (!Directory.Exists(text))
			{
				Directory.CreateDirectory(text);
				return Path.Combine(text, "License.log");
			}
			List<FileInfo> list = new List<FileInfo>();
			string[] files = Directory.GetFiles(text, "License*.log");
			foreach (string fileName in files)
			{
				FileInfo fileInfo2 = new FileInfo(fileName);
				list.Add(fileInfo2);
				if (fileInfo == null || fileInfo.LastWriteTimeUtc < fileInfo2.LastWriteTimeUtc)
				{
					fileInfo = fileInfo2;
				}
			}
			if (fileInfo == null)
			{
				return Path.Combine(text, "License.log");
			}
			if (fileInfo.Length > 1048576L)
			{
				int num = 1;
				string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(fileInfo.FullName);
				int num2 = fileNameWithoutExtension.IndexOf('-');
				if (num2 != -1)
				{
					num = ((!int.TryParse(fileNameWithoutExtension.Substring(num2 + 1), out num)) ? 1 : (num + 1));
				}
				string text2 = Path.Combine(text, "License-" + num + ".log");
				while (File.Exists(text2))
				{
					num++;
					text2 = Path.Combine(text, "License-" + num + ".log");
				}
				{
					foreach (FileInfo item in list)
					{
						if (item != fileInfo)
						{
							item.Delete();
						}
					}
					return text2;
				}
			}
			return fileInfo.FullName;
		}
	}
}
