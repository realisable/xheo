using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Security;
using System.Security.Permissions;
using System.Text;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public sealed class NoLicenseException : LicenseException, ISerializable
	{
		private readonly ValidationRecordCollection _validationRecords = new ValidationRecordCollection();

		private readonly SecureLicenseCollection _checkedLicenses;

		public ValidationRecordCollection ValidationRecords
		{
			get
			{
				return _validationRecords;
			}
		}

		public SecureLicenseCollection CheckedLicenses
		{
			get
			{
				return _checkedLicenses;
			}
		}

		internal NoLicenseException(SecureLicenseContext context)
			: base(context.LicensedType, context.Instance, MakeReportString(context))
		{
			_validationRecords.AddRange(context.ValidationRecords);
			_checkedLicenses = new SecureLicenseCollection();
			context.PopulateCheckedLicenses(this);
		}

		private NoLicenseException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
			if (info != null)
			{
				_validationRecords = (info.GetValue("records", typeof(ValidationRecordCollection)) as ValidationRecordCollection);
			}
		}

		public string GetSummaryErrorMessage()
		{
			return _validationRecords.GetSummaryErrorMessage(SR.GetString("E_NoLicensesFoundSummary"));
		}

		private static string MakeReportString(SecureLicenseContext context)
		{
			SupportInfo supportInfo = context.SupportInfo;
			StringBuilder stringBuilder = new StringBuilder();
			if (supportInfo.Website != null)
			{
				stringBuilder.Append(SR.GetString("M_NoLicenseWebsite", supportInfo.Website));
			}
			if (supportInfo.Email != null)
			{
				if (stringBuilder.Length > 0)
				{
					stringBuilder.Append(", ");
				}
				stringBuilder.Append(SR.GetString("M_NoLicenseEmail", supportInfo.Email));
			}
			if (supportInfo.Phone != null)
			{
				if (stringBuilder.Length > 0)
				{
					stringBuilder.Append(", ");
				}
				stringBuilder.Append(SR.GetString("M_NoLicensePhone", supportInfo.Phone));
			}
			if (stringBuilder.Length > 0)
			{
				stringBuilder.Insert(0, " (");
				stringBuilder.Append(')');
			}
			stringBuilder.Insert(0, SR.GetString("E_NoLicense", supportInfo.Product, supportInfo.Company));
			stringBuilder.Append(Environment.NewLine);
			stringBuilder.Append(Environment.NewLine);
			stringBuilder.Append(SR.GetString("M_NoLicenseSummary"));
			stringBuilder.Append(Environment.NewLine);
			stringBuilder.Append(Environment.NewLine);
			stringBuilder.Append(context.ValidationRecords.GetSummaryErrorMessage(SR.GetString("E_NoLicensesFoundSummary")));
			stringBuilder.Append(Environment.NewLine);
			stringBuilder.Append(Environment.NewLine);
			stringBuilder.Append(SR.GetString("M_NoLicenseReason"));
			stringBuilder.Append(Environment.NewLine);
			stringBuilder.Append(Environment.NewLine);
			stringBuilder.Append(context.ValidationRecords.ToString());
			if (context.RequestInfo.DeveloperMode)
			{
				stringBuilder.Append(Toolbox.MakeDiagnosticReportString(null, null, context));
			}
			stringBuilder.AppendFormat("\r\n****************************************************\r\nDIAGNOSTICS: {0}\r\n****************************************************\r\n", Environment.MachineName);
			stringBuilder.Append(context.GetDiagnostics());
			stringBuilder.AppendFormat("\r\n****************************************************\r\nEND DIAGNOSTICS: {0}\r\n****************************************************\r\n", Environment.MachineName);
			return stringBuilder.ToString();
		}

		[SecurityCritical]
		[SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			if (info != null)
			{
				info.AddValue("records", _validationRecords);
			}
		}
	}
}
