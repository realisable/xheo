using System;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public sealed class SerialNumberInfo : IChange
	{
		public const int MaximumPossibleSeed = 16777215;

		public const string DefaultCharacterSet = "U9VWT2FG3Q7RS0AC1DEYMNX6P8HJ4KL5";

		private bool _isReadOnly;

		private ChangeEventHandler _changed;

		private object _owner;

		private SecureLicense _license;

		private string _prefix;

		private string _serialNumber;

		private bool _serialExternal;

		internal int _seed;

		private int _minSeed;

		private int _maxSeed = 16777215;

		private int _step = 1;

		private CodeAlgorithm _algorithm = CodeAlgorithm.SerialNumber;

		private string _characterSet;

		private StringCollection _flagNames = new StringCollection();

		public object Owner
		{
			get
			{
				return _owner;
			}
		}

		public SecureLicense License
		{
			get
			{
				return _license;
			}
		}

		public string Prefix
		{
			get
			{
				return _prefix;
			}
			set
			{
				AssertNotReadOnly();
				if (_prefix != value)
				{
					_prefix = value;
					OnChanged("Prefix");
				}
			}
		}

		public string SerialNumber
		{
			get
			{
				return _serialNumber;
			}
			set
			{
				if (_license != null && !_license.CanUnlockBySerial)
				{
					AssertNotReadOnly();
					if (_serialNumber != value)
					{
						_serialNumber = value;
						OnChanged("SerialNumber");
					}
				}
				else
				{
					SecureLicenseContext.WriteDiagnosticToContext("Saving serial number '{0}' to license with prefix '{1}'.", value, _prefix);
					if (value != _serialNumber)
					{
						if (!(value == _prefix) && value != null && value.Length != 0)
						{
							if (_prefix != null && !value.Replace("-", "").StartsWith(_prefix.Replace("-", ""), StringComparison.InvariantCultureIgnoreCase))
							{
								throw new SecureLicenseException("E_SerialNumberMismatch", _prefix);
							}
							_serialNumber = value;
						}
						else
						{
							_serialNumber = null;
						}
						OnChanged("SerialNumber");
						_serialExternal = false;
					}
				}
			}
		}

		public bool SerialExternal
		{
			get
			{
				return _serialExternal;
			}
			set
			{
				if (_license != null && !_license.CanUnlockBySerial)
				{
					AssertNotReadOnly();
				}
				if (_serialExternal != value)
				{
					_serialExternal = value;
					OnChanged("SerialExternal");
				}
			}
		}

		public int Seed
		{
			get
			{
				return _seed;
			}
		}

		public int MinSeed
		{
			get
			{
				return _minSeed;
			}
			set
			{
				AssertNotReadOnly();
				if (_minSeed != value)
				{
					_minSeed = value;
					OnChanged("MinSeed");
				}
			}
		}

		public int MaxSeed
		{
			get
			{
				return _maxSeed;
			}
			set
			{
				AssertNotReadOnly();
				Check.LessThanEqual(value, 16777215, "MaxSeed");
				if (_maxSeed != value)
				{
					_maxSeed = value;
					OnChanged("MaxSeed");
				}
			}
		}

		public int Step
		{
			get
			{
				return _step;
			}
			set
			{
				AssertNotReadOnly();
				if (_step != value)
				{
					_step = value;
					OnChanged("Step");
				}
			}
		}

		public CodeAlgorithm Algorithm
		{
			get
			{
				if (_algorithm != 0)
				{
					return _algorithm;
				}
				return CodeAlgorithm.SerialNumber;
			}
			set
			{
				AssertNotReadOnly();
				if (_algorithm != value)
				{
					_algorithm = value;
					OnChanged("Algorithm");
				}
			}
		}

		public string CharacterSet
		{
			get
			{
				if (_characterSet != null)
				{
					return _characterSet;
				}
				return "U9VWT2FG3Q7RS0AC1DEYMNX6P8HJ4KL5";
			}
			set
			{
				AssertNotReadOnly();
				if (value != null && value.Length == 0)
				{
					value = null;
				}
				if (_characterSet != value)
				{
					if (value == "U9VWT2FG3Q7RS0AC1DEYMNX6P8HJ4KL5")
					{
						value = null;
					}
					if (value != null)
					{
						for (int i = 0; i < value.Length; i++)
						{
							char c = value[i];
							if (!char.IsDigit(c) && !char.IsLetter(c))
							{
								throw new SecureLicenseException("E_InvalidCharacterSet");
							}
							if (char.IsLetter(c) && !char.IsUpper(c))
							{
								throw new SecureLicenseException("E_InvalidCharacterSet");
							}
							for (int j = 0; j < value.Length; j++)
							{
								if (j != i && value[j] == c)
								{
									throw new SecureLicenseException("E_InvalidCharacterSet");
								}
							}
						}
					}
					_characterSet = value;
					OnChanged("CharacterSet");
				}
			}
		}

		public StringCollection FlagNames
		{
			get
			{
				return _flagNames;
			}
		}

		event ChangeEventHandler IChange.Changed
		{
			add
			{
				lock (this)
				{
					_changed = (ChangeEventHandler)Delegate.Combine(_changed, value);
				}
			}
			remove
			{
				lock (this)
				{
					_changed = (ChangeEventHandler)Delegate.Remove(_changed, value);
				}
			}
		}

		private void OnChanged(string property)
		{
			if (_changed != null)
			{
				ChangeEventArgs e = new ChangeEventArgs(property, this);
				_changed(this, e);
			}
		}

		private void OnChanged(ChangeEventArgs e)
		{
			if (_changed != null)
			{
				_changed(this, e);
			}
		}

		public SerialNumberInfo(object owner)
		{
			_owner = owner;
			_license = (owner as SecureLicense);
			_flagNames.Changed += _flagNames_Changed;
		}

		private void _flagNames_Changed(object sender, CollectionEventArgs e)
		{
			if (_flagNames.Count > 8)
			{
				throw new SecureLicenseException("E_Only8FlagsAllowed");
			}
			OnChanged(new ChangeEventArgs("FlagNames", this, e.Element, e.Action));
		}

		void IChange.MakeReadOnly()
		{
			_isReadOnly = true;
		}

		private void AssertNotReadOnly()
		{
			if (_license != null)
			{
				_license.AssertNotReadOnly();
				return;
			}
			if (!_isReadOnly)
			{
				return;
			}
			throw new SecureLicenseException("E_LicenseReadOnly");
		}

		internal bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			writer.WriteStartElement("SerialNumber");
			if (((_license != null && !_license.CanUnlockBySerial) || signing == LicenseSaveType.Normal) && _serialNumber != null && _serialNumber.Length > 0)
			{
				writer.WriteAttributeString("value", _serialNumber);
			}
			if (signing == LicenseSaveType.Normal && _serialExternal)
			{
				writer.WriteAttributeString("serialExternal", "true");
			}
			if (_prefix != null && _prefix.Length > 0)
			{
				writer.WriteAttributeString("prefix", _prefix);
			}
			if (_characterSet != null)
			{
				writer.WriteAttributeString("characterSet", _characterSet);
			}
			if (_step != 1)
			{
				writer.WriteAttributeString("step", _step.ToString());
			}
			if (_minSeed != 0)
			{
				writer.WriteAttributeString("min", _minSeed.ToString());
			}
			if (_maxSeed != 16777215)
			{
				writer.WriteAttributeString("max", _maxSeed.ToString());
			}
			if (_algorithm != 0 && _algorithm != CodeAlgorithm.SerialNumber)
			{
				int algorithm = (int)_algorithm;
				writer.WriteAttributeString("algorithm", algorithm.ToString());
			}
			for (int i = 0; i < _flagNames.Count; i++)
			{
				writer.WriteAttributeString("flag" + i.ToString(), _flagNames[i]);
			}
			writer.WriteEndElement();
			return true;
		}

		internal bool ReadFromXml(XmlReader reader)
		{
			Clear();
			_serialNumber = reader.GetAttribute("value");
			_serialExternal = (reader.GetAttribute("serialExternal") == "true");
			_prefix = reader.GetAttribute("prefix");
			_characterSet = reader.GetAttribute("characterSet");
			string attribute;
			for (int i = 0; i < 8; i++)
			{
				attribute = reader.GetAttribute("flag" + i.ToString());
				if (attribute == null)
				{
					break;
				}
				_flagNames.Add(attribute);
			}
			attribute = reader.GetAttribute("step");
			if (attribute != null)
			{
				_step = SafeToolbox.FastParseInt32(attribute);
			}
			attribute = reader.GetAttribute("min");
			if (attribute != null)
			{
				_minSeed = SafeToolbox.FastParseInt32(attribute);
			}
			if (_minSeed < 0)
			{
				return false;
			}
			attribute = reader.GetAttribute("max");
			if (attribute != null)
			{
				_maxSeed = SafeToolbox.FastParseInt32(attribute);
			}
			if (_maxSeed > 16777215)
			{
				return false;
			}
			attribute = reader.GetAttribute("algorithm");
			if (attribute != null)
			{
				_algorithm = (CodeAlgorithm)SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_algorithm = CodeAlgorithm.NotSet;
			}
			reader.Skip();
			return true;
		}

		internal void Clear()
		{
			AssertNotReadOnly();
			_serialNumber = null;
			_prefix = null;
			_flagNames.Clear();
			_step = 1;
			_minSeed = 0;
			_maxSeed = 16777215;
		}
	}
}
