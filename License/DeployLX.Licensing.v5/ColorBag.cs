using System;
using System.Collections;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public sealed class ColorBag : Bag
	{
		public Color this[string name]
		{
			get
			{
				object value = GetValue(name);
				if (value == null)
				{
					return Color.Empty;
				}
				return (Color)value;
			}
			set
			{
				if (string.Compare(name, "Base", true) == 0)
				{
					SetBaseColor(value, false);
					base.SetValue(name, value);
				}
				else if (value == Color.Empty)
				{
					base.SetValue(name, null);
				}
				else
				{
					base.SetValue(name, value);
				}
			}
		}

		public Color this[string name, params string[] inherit]
		{
			get
			{
				object value = GetValue(name, inherit);
				if (value == null)
				{
					return Color.Empty;
				}
				return (Color)value;
			}
		}

		public ColorBag()
			: base("Color")
		{
		}

		internal override bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			if (!ShouldSerialize())
			{
				return true;
			}
			ColorBag colorBag = new ColorBag();
			colorBag.SetBaseColor(this["Base"], true);
			writer.WriteStartElement("Colors");
			if (this["Base"] != SuperFormTheme.DefaultBaseColor)
			{
				writer.WriteAttributeString("base", ImageEffects.ToRGBHex(this["Base"]));
			}
			IDictionaryEnumerator enumerator = base.Dictionary.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					DictionaryEntry dictionaryEntry = (DictionaryEntry)enumerator.Current;
					Color color = (Color)dictionaryEntry.Value;
					string text = dictionaryEntry.Key as string;
					if (!(colorBag[text] == color))
					{
						writer.WriteAttributeString(text, ImageEffects.ToRGBHex(color));
					}
				}
			}
			finally
			{
				IDisposable disposable = enumerator as IDisposable;
				if (disposable != null)
				{
					disposable.Dispose();
				}
			}
			writer.WriteEndElement();
			return true;
		}

		internal override bool ShouldSerialize()
		{
			if (this["Base"] == Color.Empty)
			{
				this["Base"] = SuperFormTheme.DefaultBaseColor;
			}
			if (this["Base"] != SuperFormTheme.DefaultBaseColor)
			{
				return true;
			}
			ColorBag colorBag = new ColorBag();
			colorBag.SetBaseColor(this["Base"], true);
			IDictionaryEnumerator enumerator = base.Dictionary.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					DictionaryEntry dictionaryEntry = (DictionaryEntry)enumerator.Current;
					Color right = (Color)dictionaryEntry.Value;
					string name = dictionaryEntry.Key as string;
					if (!(colorBag[name] == right))
					{
						return true;
					}
				}
			}
			finally
			{
				IDisposable disposable = enumerator as IDisposable;
				if (disposable != null)
				{
					disposable.Dispose();
				}
			}
			return false;
		}

		internal override bool ReadFromXml(XmlReader reader)
		{
			Check.NotNull(reader, "reader");
			base.AssertNotReadOnly();
			base.Dictionary.Clear();
			if (reader.Name != "Colors")
			{
				return false;
			}
			if (reader.MoveToFirstAttribute())
			{
				do
				{
					base.Dictionary[reader.Name] = ImageEffects.FromRGBHex(reader.Value);
				}
				while (reader.MoveToNextAttribute());
			}
			reader.Read();
			return true;
		}

		public void SetBaseColor(Color color, bool overwriteExisting)
		{
			if (!overwriteExisting)
			{
				ColorBag colorBag = new ColorBag();
				colorBag.SetBaseColor(this["Base"], true);
				base.BeginUpdate();
				IDictionaryEnumerator enumerator = colorBag.Dictionary.GetEnumerator();
				try
				{
					while (enumerator.MoveNext())
					{
						DictionaryEntry dictionaryEntry = (DictionaryEntry)enumerator.Current;
						if (string.Compare(dictionaryEntry.Key as string, "Base", true) != 0 && this[dictionaryEntry.Key as string] == colorBag[dictionaryEntry.Key as string])
						{
							this[dictionaryEntry.Key as string] = Color.Empty;
						}
					}
				}
				finally
				{
					IDisposable disposable = enumerator as IDisposable;
					if (disposable != null)
					{
						disposable.Dispose();
					}
				}
				base.EndUpdate();
			}
			base.SetValue("Base", color);
			if (overwriteExisting || this["BaseLight"] == Color.Empty)
			{
				this["BaseLight"] = ImageEffects.CreateAdjustedColor(color, 32);
			}
			if (overwriteExisting || this["BaseLightLight"] == Color.Empty)
			{
				this["BaseLightLight"] = ImageEffects.CreateAdjustedColor(color, 64);
			}
			if (overwriteExisting || this["BaseHigh"] == Color.Empty)
			{
				this["BaseHigh"] = ImageEffects.CreateDodgeColor(color, 32);
			}
			if (overwriteExisting || this["BaseHighHigh"] == Color.Empty)
			{
				this["BaseHighHigh"] = ImageEffects.CreateDodgeColor(color, 64);
			}
			if (overwriteExisting || this["BaseHighHighHigh"] == Color.Empty)
			{
				this["BaseHighHighHigh"] = ImageEffects.CreateDodgeColor(color, 160);
			}
			if (overwriteExisting || this["BaseDark"] == Color.Empty)
			{
				this["BaseDark"] = ControlPaint.Dark(color, 0.05f);
			}
			if (overwriteExisting || this["BaseDarkDark"] == Color.Empty)
			{
				this["BaseDarkDark"] = ControlPaint.Dark(color, 0.3f);
			}
			if (overwriteExisting || this["BaseHighlight"] == Color.Empty)
			{
				this["BaseHighlight"] = ImageEffects.CreateAdjustedColor(color, 192);
			}
			if (overwriteExisting || this["Text"] == Color.Empty)
			{
				this["Text"] = Color.White;
			}
			if (overwriteExisting || this["GlassPanelText"] == Color.Empty)
			{
				this["GlassPanelText"] = Color.Black;
			}
			if (overwriteExisting || this["GlassPanelHighlightText"] == Color.Empty)
			{
				this["GlassPanelHighlightText"] = color;
			}
			if (overwriteExisting || this["FooterText"] == Color.Empty)
			{
				this["FooterText"] = Color.Black;
			}
			if (overwriteExisting || this["Footer"] == Color.Empty)
			{
				this["Footer"] = Color.FromArgb(254, 254, 254);
			}
			if (overwriteExisting || this["FooterFade"] == Color.Empty)
			{
				this["FooterFade"] = Color.FromArgb(240, 240, 240);
			}
			if (overwriteExisting || this["WebPanel"] == Color.Empty)
			{
				this["WebPanel"] = Color.FromArgb(240, 240, 240);
			}
			if (overwriteExisting || this["WebSide"] == Color.Empty)
			{
				this["WebSide"] = color;
			}
			if (overwriteExisting || this["WebText"] == Color.Empty)
			{
				this["WebText"] = Color.Black;
			}
			if (overwriteExisting || this["WebSideText"] == Color.Empty)
			{
				this["WebSideText"] = this["Text"];
			}
			if (this["Panel"] != Color.Empty)
			{
				if (!overwriteExisting && !(this["PanelFade"] == Color.Empty))
				{
					return;
				}
				this["PanelFade"] = ImageEffects.CreateAdjustedColor(this["Panel"], 32);
			}
		}
	}
}
