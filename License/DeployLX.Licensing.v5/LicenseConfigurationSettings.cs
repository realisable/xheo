using System.Collections;
using System.Collections.Specialized;
using System.Net;

namespace DeployLX.Licensing.v5
{
	internal sealed class LicenseConfigurationSettings
	{
		public bool DisableTrials;

		public IWebProxy Proxy;

		public NameValueCollection SerialNumbers;

		public int ServerConnectTimeout;

		public string SharedFolder;

		public LicenseConfigurationSettings()
		{
			SerialNumbers = new NameValueCollection(new CaseInsensitiveHashCodeProvider(), new CaseInsensitiveComparer());
			ServerConnectTimeout = 120000;
		}
	}
}
