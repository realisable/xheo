using System;
using System.IO;

namespace DeployLX.Licensing.v5
{
	internal sealed class LicResProtocol : ICustomProtocol
	{
		public string Scheme
		{
			get
			{
				return "licres";
			}
		}

		public bool Navigate
		{
			get
			{
				return true;
			}
		}

		public bool CanProcessUri(Uri uri)
		{
			Check.NotNull(uri, "uri");
			return true;
		}

		public Stream GetResourceStream(Uri uri, SecureLicenseContext context)
		{
			Check.NotNull(uri, "uri");
			if (context == null)
			{
				context = SecureLicenseContext.ResolveContext;
			}
			byte[] binary;
			if (context != null && context.FormResourceLicense != null)
			{
				SecureLicense formResourceLicense = context.FormResourceLicense;
				int i;
				for (i = 0; uri.LocalPath.Length > i && (uri.LocalPath[i] == '/' || uri.LocalPath[i] == '\\'); i++)
				{
				}
				string name = uri.LocalPath.Substring(i);
				if (string.Compare(uri.Host, "license", true) == 0)
				{
					binary = formResourceLicense.Resources.GetBinary(name);
					goto IL_00c1;
				}
				if (formResourceLicense.LicenseFile != null && string.Compare(uri.Host, "licensefile", true) == 0)
				{
					binary = formResourceLicense.LicenseFile.Resources.GetBinary(name);
					goto IL_00c1;
				}
				return null;
			}
			return null;
			IL_00c1:
			if (binary != null)
			{
				return new MemoryStream(binary);
			}
			return null;
		}

		public Stream GetResourceStream(Uri uri)
		{
			return GetResourceStream(uri, SecureLicenseManager.CurrentContext);
		}
	}
}
