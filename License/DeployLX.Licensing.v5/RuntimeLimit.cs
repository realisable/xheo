using System;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	[LimitEditor("DesignOrRuntimeLimitEditor", IconResource = "asmres://DeployLX.Licensing.DlxAddIn.v5/DeployLX.Licensing.DlxAddIn.v5.Resources.Limits.Runtime.png")]
	public class RuntimeLimit : DesigntimeOrRuntimeLimit
	{
		public override string Description
		{
			get
			{
				return SR.GetString("M_RuntimeLimitDescription");
			}
		}

		public override string Name
		{
			get
			{
				return "Runtime";
			}
		}

		public override ValidationResult Validate(SecureLicenseContext context)
		{
			if (!base.IsHostedInDesigner(context))
			{
				return base.Validate(context);
			}
			return context.ReportError("E_RuntimeOnly", this, null, ErrorSeverity.Low);
		}

		public override PeekResult Peek(SecureLicenseContext context)
		{
			if (base.IsHostedInDesigner(context))
			{
				return PeekResult.Invalid;
			}
			return base.Peek(context);
		}
	}
}
