using System;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	public class DoDeactivationPanel : SuperFormPanel
	{
		private Timer _tickTimer;

		private ShadowLabel _deactivatingNotice;

		private ProgressBar _progressing;

		private SkinnedPanel _completePanel;

		private ShadowLabel _completeNotice;

		private BufferedPictureBox _completeIcon;

		private SkinnedButton _details;

		private DeactivationPhase _phase;

		private AsyncValidationRequest _request;

		public DoDeactivationPanel(ActivationLimit activationLimit)
			: base(activationLimit)
		{
			if (activationLimit == null)
			{
				throw new ArgumentNullException("activationLimit");
			}
			InitializeComponent();
			try
			{
				_progressing.Style = ProgressBarStyle.Marquee;
			}
			catch
			{
				_tickTimer = new Timer();
				_tickTimer.Interval = 500;
				_tickTimer.Tick += _tickTimer_Tick;
			}
		}

		private void InitializeComponent()
		{
			_deactivatingNotice = new ShadowLabel();
			_progressing = new ProgressBar();
			_completePanel = new SkinnedPanel();
			_details = new SkinnedButton();
			_completeNotice = new ShadowLabel();
			_completeIcon = new BufferedPictureBox();
			_completePanel.SuspendLayout();
			((ISupportInitialize)_completeIcon).BeginInit();
			base.SuspendLayout();
			_deactivatingNotice.AutoSize = true;
			_deactivatingNotice.Location = new Point(115, 133);
			_deactivatingNotice.Name = "_deactivatingNotice";
			_deactivatingNotice.Size = new Size(131, 15);
			_deactivatingNotice.TabIndex = 6;
			_deactivatingNotice.Text = "#UI_DeactivatingNotice";
			_deactivatingNotice.ThemeFont = ThemeFont.Medium;
			_progressing.Location = new Point(113, 159);
			_progressing.Maximum = 10;
			_progressing.Name = "_progressing";
			_progressing.Size = new Size(465, 23);
			_progressing.TabIndex = 5;
			_completePanel.BackColor = Color.Transparent;
			_completePanel.BackgroundImageLayout = ImageLayout.None;
			_completePanel.Controls.Add(_details);
			_completePanel.Controls.Add(_completeNotice);
			_completePanel.Controls.Add(_completeIcon);
			_completePanel.Location = new Point(98, 120);
			_completePanel.Name = "_completePanel";
			_completePanel.PaintFakeBackground = true;
			_completePanel.Size = new Size(488, 165);
			_completePanel.TabIndex = 9;
			_completePanel.Visible = false;
			_details.Location = new Point(375, 122);
			_details.Name = "_details";
			_details.ShouldDrawFocus = false;
			_details.Size = new Size(95, 26);
			_details.TabIndex = 5;
			_details.Text = "&Details...";
			_details.UseVisualStyleBackColor = false;
			_details.Visible = false;
			_details.Click += _details_Click;
			_completeNotice.Location = new Point(70, 15);
			_completeNotice.Name = "_completeNotice";
			_completeNotice.Size = new Size(400, 78);
			_completeNotice.TabIndex = 1;
			_completeNotice.Text = "#UI_DeactivationCompleteNotice";
			_completeNotice.ThemeColor = new string[1]
			{
				"GlassPanelText"
			};
			_completeNotice.ThemeFont = ThemeFont.Medium;
			_completeIcon.BackColor = Color.Transparent;
			_completeIcon.BackgroundImageLayout = ImageLayout.None;
			_completeIcon.Location = new Point(15, 15);
			_completeIcon.Name = "_completeIcon";
			_completeIcon.OffsetBackgroundImage = false;
			_completeIcon.Size = new Size(48, 48);
			_completeIcon.TabIndex = 0;
			_completeIcon.TabStop = false;
			base.AutoScaleDimensions = new SizeF(96f, 96f);
			base.Controls.Add(_completePanel);
			base.Controls.Add(_deactivatingNotice);
			base.Controls.Add(_progressing);
			base.Name = "DoDeactivationPanel";
			_completePanel.ResumeLayout(false);
			((ISupportInitialize)_completeIcon).EndInit();
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		protected internal override void LoadPanel(SecureLicenseContext context)
		{
			base.LoadPanel(context);
			_completeIcon.Image = Images.BigInfo_png;
		}

		protected internal override void InitializePanel()
		{
			if (base._firstInitialization && _tickTimer != null)
			{
				_tickTimer.Enabled = true;
			}
		}

		protected internal override void PanelShown(bool safeToChange)
		{
			base.PanelShown(safeToChange);
			if (base._firstInitialization && safeToChange)
			{
				ActivationLimit activationLimit = base.Limit as ActivationLimit;
				if (activationLimit.HasActivated && activationLimit.CanDeactivate)
				{
					lock (this)
					{
						_phase = DeactivationPhase.CheckPermission;
						_request = activationLimit.DeactivateAsync(base._superForm.Context, true, DeactivationPhase.CheckPermission, HandleDeactivate);
					}
				}
				else
				{
					base.Return(FormResult.Failure);
				}
			}
		}

		private void AddConfirmationCode(ActivationLimit limit)
		{
			foreach (Control control in _completePanel.Controls)
			{
				if (control.Tag as Limit == limit)
				{
					return;
				}
			}
			int y = _completeNotice.Bottom + 8;
			string text = base._superForm.Context.MakeDeactivationConfirmationCode(limit);
			Label label = new Label();
			label.Font = new Font("Lucida Console", 12f, FontStyle.Regular);
			label.ForeColor = base.GetThemeColor(Color.Black, "GlassPanelHighlightText", "GlassPanelText");
			label.SetBounds(16, y, 433, 33);
			label.BackColor = Color.Transparent;
			label.Text = text;
			label.TextAlign = ContentAlignment.MiddleCenter;
			label.Tag = limit;
			y = label.Bottom + 8;
			_completePanel.Controls.Add(label);
			_completePanel.Height += 29;
		}

		private void HandleDeactivate(object sender, EventArgs e)
		{
			AsyncValidationRequest asyncValidationRequest = sender as AsyncValidationRequest;
			if (asyncValidationRequest.GetResult() != ValidationResult.Valid)
			{
				base.Invoke(new MethodInvoker(ShowFailed));
			}
			else if (_phase == DeactivationPhase.CheckPermission)
			{
				_phase = DeactivationPhase.Commit;
				_request = ((ActivationLimit)base.Limit).DeactivateAsync(base._superForm.Context, true, _phase, HandleDeactivate);
			}
			else
			{
				base.Invoke(new MethodInvoker(ShowOK));
			}
		}

		private void _tickTimer_Tick(object sender, EventArgs e)
		{
			_progressing.Increment(1);
			if (_progressing.Value >= _progressing.Maximum)
			{
				_progressing.Value = _progressing.Minimum;
			}
		}

		private void ShowFailed()
		{
			_completeNotice.Text = SR.GetString("UI_DeactivationFailedNotice");
			_completeIcon.Image = Images.BigError7_png;
			_completePanel.Top = (base.Parent.Height - _completePanel.Height) / 2;
			_details.Top = _completePanel.Height - _details.Height - 24;
			base.ShowControlsWithEffects(TriBoolValue.Default, _completePanel, _progressing, _deactivatingNotice, _details);
			base.ClearBottomControls();
			Button button = base.AddBottomButton("UI_OK");
			button.Click += _ok_Click;
		}

		private void ShowOK()
		{
			_completePanel.Top = (base.Parent.Height - _completePanel.Height) / 2;
			AddConfirmationCode(base.Limit as ActivationLimit);
			base.ShowControlsWithEffects(TriBoolValue.Default, _completePanel, _progressing, _deactivatingNotice);
			base.ClearBottomControls();
			Button button = base.AddBottomButton("UI_OK");
			button.Click += _ok_Click;
			base.SuperForm.AcceptButton = button;
			button.Select();
			Button button2 = base.AddBottomButton("UI_CopyToClipboard");
			button2.Click += btn_Click;
		}

		private void btn_Click(object sender, EventArgs e)
		{
			try
			{
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.AppendLine(base._superForm.Context.MakeDeactivationConfirmationCode(base.Limit as ActivationLimit));
				ClipboardToolbox.SetClipboard(DataFormats.Text, stringBuilder.ToString());
				base.ShowMessageBox("UI_DetailsCopied", "UI_DetailsCopiedTitle", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
			}
			catch (Exception ex)
			{
				MessageBoxEx.ShowException(ex);
			}
		}

		private void _ok_Click(object sender, EventArgs e)
		{
			base.Return(FormResult.Success);
		}

		private void _details_Click(object sender, EventArgs e)
		{
			LicenseEventArgs licenseEventArgs = new LicenseEventArgs(base._superForm.Context.ValidatingLicense, base._superForm.Context, base._superForm.Context.LatestValidationRecord);
			SecureLicenseManager.OnErrorOccurred(licenseEventArgs);
			if (!licenseEventArgs.Handled)
			{
				base.ShowPanel(new ErrorReportPanel(null, null, _completeNotice.Text, new ValidationRecordCollection(base._superForm.Context.LatestValidationRecord), true), HandleDetails);
			}
		}

		private void HandleDetails(FormResult result)
		{
			base.Return((FormResult)524290);
		}
	}
}
