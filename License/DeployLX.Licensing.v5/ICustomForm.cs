namespace DeployLX.Licensing.v5
{
	public interface ICustomForm
	{
		FormResult Show(SecureLicenseContext context, Limit limit, string pageId);
	}
}
