using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	[LimitEditor("OnlineLimitEditor", IconResource = "asmres://DeployLX.Licensing.DlxAddIn.v5/DeployLX.Licensing.DlxAddIn.v5.Resources.Limits.Online.png", Category = "State and Filters")]
	public class OnlineLimit : Limit, IServerLimit
	{
		public class ValidationStateIds
		{
			public const string Disconnected = "Disconnected";

			public const string Online = "Online";
		}

		private static readonly List<string> _validationStates;

		private readonly UriCollection _servers = new UriCollection();

		private int _serverRetries;

		private string _state;

		public override string Description
		{
			get
			{
				return SR.GetString("M_OnlineLimitDescription");
			}
		}

		public override string Name
		{
			get
			{
				return "Online";
			}
		}

		public int ServerRetries
		{
			get
			{
				return _serverRetries;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_serverRetries != value)
				{
					_serverRetries = value;
					base.OnChanged("ServerRetries");
				}
			}
		}

		public UriCollection Servers
		{
			get
			{
				return _servers;
			}
		}

		public override string State
		{
			get
			{
				return _state ?? "Online";
			}
		}

		public override IList<string> ValidationStates
		{
			get
			{
				return _validationStates;
			}
		}

		static OnlineLimit()
		{
			_validationStates = new List<string>();
			_validationStates.Add("Online");
			_validationStates.Add("Disconnected");
		}

		public OnlineLimit()
		{
			_servers.Changed += OnServersChanged;
		}

		protected virtual bool IsOnline(SecureLicenseContext context)
		{
			if (context.RequestInfo.SimulateOffline)
			{
				return false;
			}
			if (!NetworkInterface.GetIsNetworkAvailable())
			{
				return false;
			}
			if (_servers.Count == 0)
			{
				return true;
			}
			for (int i = 0; i <= _serverRetries; i++)
			{
				foreach (string item in (IEnumerable)_servers)
				{
					if (TestServer(new Uri(item), context))
					{
						return true;
					}
				}
			}
			return false;
		}

		protected internal override void MakeReadOnly()
		{
			_servers.MakeReadOnly();
			base.MakeReadOnly();
		}

		private void OnServersChanged(object sender, CollectionEventArgs e)
		{
			base.OnCollectionChanged(sender, "Servers", e);
		}

		public override ValidationResult Validate(SecureLicenseContext context)
		{
			_state = (IsOnline(context) ? "Online" : "Disconnected");
			return base.Validate(context);
		}

		public override ValidationResult Granted(SecureLicenseContext context)
		{
			_state = (IsOnline(context) ? "Online" : "Disconnected");
			return base.Granted(context);
		}

		protected override bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			if (signing == LicenseSaveType.Signing)
			{
				base.AssertMinimumVersion(SecureLicense.v4_0);
			}
			if (_serverRetries > 0)
			{
				writer.WriteAttributeString("serverRetries", _serverRetries.ToString());
			}
			foreach (string item in (IEnumerable)_servers)
			{
				writer.WriteStartElement("Server");
				writer.WriteAttributeString("address", SafeToolbox.XmlEncode(item));
				writer.WriteEndElement();
			}
			return true;
		}

		protected override bool ReadFromXml(XmlReader reader)
		{
			string attribute = reader.GetAttribute("serverRetries");
			_serverRetries = ((attribute != null) ? SafeToolbox.FastParseInt32(attribute) : 0);
			_servers.Clear();
			if (reader.IsEmptyElement)
			{
				reader.Read();
				return true;
			}
			_servers.VerifyValues = false;
			reader.Read();
			while (!reader.EOF)
			{
				if (reader.IsStartElement())
				{
					switch (reader.Name)
					{
					case "Limit":
						if (base.ReadChildLimit(reader))
						{
							break;
						}
						return false;
					case "Server":
						attribute = reader.GetAttribute("address");
						if (attribute != null)
						{
							_servers.Add(SafeToolbox.XmlDecode(attribute));
							reader.Read();
							break;
						}
						return false;
					default:
						reader.Skip();
						break;
					}
					continue;
				}
				reader.Read();
				break;
			}
			_servers.VerifyValues = true;
			return true;
		}

		private bool TestServer(Uri uri, SecureLicenseContext context)
		{
			try
			{
				HttpWebRequest httpWebRequest = WebRequest.Create(uri) as HttpWebRequest;
				httpWebRequest.Method = "GET";
				httpWebRequest.Proxy = Config.Proxy;
				ServicePoint servicePoint = ServicePointManager.FindServicePoint(uri, httpWebRequest.Proxy);
				servicePoint.Expect100Continue = false;
				httpWebRequest.UserAgent = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;)";
				using (httpWebRequest.GetResponse())
				{
					return true;
				}
			}
			catch (Exception ex)
			{
				if (Toolbox.IsProxyException(ex, context))
				{
					return true;
				}
				context.ReportError("E_UnexpectedErrorFromServer", this, ex, uri);
			}
			return false;
		}
	}
}
