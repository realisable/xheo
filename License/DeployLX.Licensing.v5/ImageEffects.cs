using System;
using System.Collections.Specialized;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	internal sealed class ImageEffects
	{
		private static HybridDictionary _gaussianMatrices;

		private static float[][] _identityMatrix;

		private static readonly byte[] ClipTable;

		private ImageEffects()
		{
		}

		static ImageEffects()
		{
			_gaussianMatrices = new HybridDictionary(64);
			_identityMatrix = new float[5][]
			{
				new float[5]
				{
					1f,
					0f,
					0f,
					0f,
					0f
				},
				new float[5]
				{
					0f,
					1f,
					0f,
					0f,
					0f
				},
				new float[5]
				{
					0f,
					0f,
					1f,
					0f,
					0f
				},
				new float[5]
				{
					0f,
					0f,
					0f,
					1f,
					0f
				},
				new float[5]
				{
					0f,
					0f,
					0f,
					0f,
					1f
				}
			};
			ClipTable = new byte[768];
			for (int i = 0; i < 768; i++)
			{
				int num = i - 255;
				ClipTable[i] = (byte)((num >= 0) ? ((num > 255) ? 255 : num) : 0);
			}
		}

		public static Bitmap Clone(Bitmap source, Rectangle bounds, PixelFormat format)
		{
			if (bounds.X == 0 && bounds.Y == 0 && bounds.Width == source.Width && bounds.Height == source.Height)
			{
				return source.Clone() as Bitmap;
			}
			Bitmap bitmap = new Bitmap(bounds.Width, bounds.Height, format);
			try
			{
				using (Graphics graphics = Graphics.FromImage(bitmap))
				{
					graphics.DrawImage(source, new Rectangle(0, 0, bounds.Width, bounds.Height), bounds, GraphicsUnit.Pixel);
					return bitmap;
				}
			}
			catch
			{
				if (bitmap != null)
				{
					bitmap.Dispose();
				}
				return null;
			}
		}

		public static Bitmap MakeTransparent(Image bmp, int opacity)
		{
			Bitmap bitmap = new Bitmap(bmp.Width, bmp.Height, PixelFormat.Format32bppArgb);
			ColorMatrix colorMatrix = new ColorMatrix(_identityMatrix);
			using (ImageAttributes imageAttributes = new ImageAttributes())
			{
				colorMatrix[3, 3] = (float)opacity / 255f;
				imageAttributes.ClearColorMatrix();
				imageAttributes.SetColorMatrix(colorMatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
				using (Graphics graphics = Graphics.FromImage(bitmap))
				{
					graphics.DrawImage(bmp, new Rectangle(0, 0, bitmap.Width, bitmap.Height), 0, 0, bitmap.Width, bitmap.Height, GraphicsUnit.Pixel, imageAttributes);
					return bitmap;
				}
			}
		}

		public unsafe static Bitmap FillImageColor(Bitmap image, Color color, bool nonDestructive)
		{
			Bitmap bitmap = null;
			BitmapData bitmapData = null;
			BitmapData bitmapData2 = null;
			try
			{
				if (nonDestructive)
				{
					bitmap = new Bitmap(image.Width, image.Height, PixelFormat.Format32bppArgb);
					bitmapData = image.LockBits(new Rectangle(0, 0, image.Width, image.Height), ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
					bitmapData2 = bitmap.LockBits(new Rectangle(0, 0, image.Width, image.Height), ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
				}
				else
				{
					bitmapData = image.LockBits(new Rectangle(0, 0, image.Width, image.Height), ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
					bitmapData2 = bitmapData;
				}
				int* ptr = (int*)bitmapData.Scan0.ToPointer();
				int* ptr2 = (int*)bitmapData2.Scan0.ToPointer();
				int num = bitmapData.Stride >> 2;
				int num2 = num - image.Width;
				int num3 = color.R << 16 | color.G << 8 | color.B;
				int num4 = (color.A == 255) ? (-16777216) : (color.A << 24);
				int width = image.Width;
				int height = image.Height;
				for (int i = 0; i < height; i++)
				{
					for (int j = 0; j < width; j++)
					{
						*ptr2 = ((*ptr & num4) | num3);
						ptr++;
						ptr2++;
					}
					ptr += num2;
					ptr2 += num2;
				}
				return bitmap;
			}
			catch (Exception)
			{
				if (bitmap != null)
				{
					if (bitmapData2 != null)
					{
						bitmap.UnlockBits(bitmapData2);
						bitmapData2 = null;
					}
					if (nonDestructive)
					{
						bitmap.Dispose();
					}
				}
				throw;
			}
			finally
			{
				if (bitmapData != null)
				{
					image.UnlockBits(bitmapData);
				}
				if (bitmapData2 != null && bitmap != null)
				{
					bitmap.UnlockBits(bitmapData2);
				}
			}
		}

		public unsafe static Bitmap Convolution(Bitmap image, ConvolutionMatrix matrix)
		{
			Bitmap bitmap = null;
			Bitmap bitmap2 = null;
			BitmapData bitmapData = null;
			BitmapData bitmapData2 = null;
			try
			{
				bitmap = new Bitmap(image.Width, image.Height, PixelFormat.Format32bppArgb);
				bitmapData = image.LockBits(new Rectangle(0, 0, image.Width, image.Height), ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
				bitmapData2 = bitmap.LockBits(new Rectangle(0, 0, image.Width, image.Height), ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
				int dimX = matrix.DimX;
				int dimY = matrix.DimY;
				int num = dimX >> 1;
				int num2 = dimY >> 1;
				int num3 = dimY;
				int num4 = dimX;
				int num5 = num2;
				int num6 = num;
				int num7 = 0;
				int num8 = 0;
				int height = image.Height;
				int width = image.Width;
				int num9 = bitmapData.Stride - (image.Width << 2);
				int num10 = num9 >> 2;
				int stride = bitmapData.Stride;
				int[,] array = PreCalculatePixelOffsets(matrix, bitmapData.Stride);
				int[,] array2 = null;
				int factor = matrix.Factor;
				int offset = matrix.Offset;
				if (matrix.IsIsotropic)
				{
					array2 = PreCalculatePixelOffsetsCompliment(matrix, bitmapData.Stride);
				}
				int[][] array3 = new int[dimY][];
				for (int i = 0; i < dimY; i++)
				{
					int[] array4 = new int[dimX];
					for (int j = 0; j < dimX; j++)
					{
						array4[j] = matrix.Matrix[j, i];
					}
					array3[i] = array4;
				}
				int[][] array5 = new int[dimY][];
				for (int k = 0; k < dimY; k++)
				{
					int[] array6 = new int[dimX];
					for (int l = 0; l < dimX; l++)
					{
						array6[l] = array[l, k];
					}
					array5[k] = array6;
				}
				try
				{
					fixed (byte* ptr4 = &ClipTable[255])
					{
						IntPtr scan = bitmapData.Scan0;
						byte* ptr = (byte*)scan.ToPointer();
						scan = bitmapData2.Scan0;
						byte* ptr2 = (byte*)scan.ToPointer();
						int* ptr3 = (int*)ptr;
						for (int m = 0; m < height; m++)
						{
							int num11 = m - num2;
							for (int n = 0; n < width; n++)
							{
								int num12 = 0;
								int num13 = 0;
								int num14 = 0;
								int num15 = 0;
								int num16 = factor;
								int num17 = factor;
								for (num8 = 0; num8 < dimY; num8++)
								{
									int[] array7 = array3[num8];
									int num18 = num11 + num8;
									if (num18 >= 0 && num18 < height)
									{
										int[] array8 = array5[num8];
										int num19 = n - num;
										for (num7 = 0; num7 < dimX; num7++)
										{
											int num20 = array8[num7];
											int num21 = array7[num7];
											int num22 = num19 + num7;
											if (num22 >= 0 && num22 < width)
											{
												if (ptr3[num20 >> 2] == 0)
												{
													num16 -= num21;
												}
												else
												{
													num14 += ptr[num20] * num21;
													num13 += ptr[num20 + 1] * num21;
													num12 += ptr[num20 + 2] * num21;
													num15 += ptr[num20 + 3] * num21;
												}
											}
											else
											{
												num16 -= num21;
												num17 -= num21;
											}
										}
									}
									else
									{
										for (num7 = 0; num7 < dimX; num7++)
										{
											int num23 = array7[num7];
											num16 -= num23;
											num17 -= num23;
										}
									}
								}
								if (num16 < 1)
								{
									num16 = 1;
								}
								if (num17 < 1)
								{
									num17 = 1;
								}
								num14 = num14 / num16 + offset;
								num13 = num13 / num16 + offset;
								num12 = num12 / num16 + offset;
								num15 = num15 / num17 + offset;
								*ptr2 = ptr4[num14];
								ptr2[1] = ptr4[num13];
								ptr2[2] = ptr4[num12];
								ptr2[3] = ptr4[num15];
								ptr += 4;
								ptr2 += 4;
								ptr3++;
							}
							ptr3 += num10;
							ptr += num9;
							ptr2 += num9;
						}
						if (matrix.IsIsotropic)
						{
							image.UnlockBits(bitmapData);
							bitmap2 = bitmap;
							bitmap = new Bitmap(image.Width, image.Height, PixelFormat.Format32bppArgb);
							bitmapData = bitmapData2;
							bitmapData2 = bitmap.LockBits(new Rectangle(0, 0, image.Width, image.Height), ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
							scan = bitmapData.Scan0;
							ptr = (byte*)scan.ToPointer();
							scan = bitmapData2.Scan0;
							ptr2 = (byte*)scan.ToPointer();
							ptr3 = (int*)ptr;
							array5 = new int[num3][];
							for (int num24 = 0; num24 < num3; num24++)
							{
								int[] array9 = new int[num4];
								for (int num25 = 0; num25 < num4; num25++)
								{
									array9[num25] = array2[num24, num25];
								}
								array5[num24] = array9;
							}
							array3 = new int[dimX][];
							for (int num26 = 0; num26 < num3; num26++)
							{
								int[] array10 = new int[num4];
								for (int num27 = 0; num27 < num4; num27++)
								{
									array10[num27] = matrix.Matrix[num27, num26];
								}
								array3[num26] = array10;
							}
							for (int num28 = 0; num28 < height; num28++)
							{
								int num29 = num28 - num6;
								for (int num30 = 0; num30 < width; num30++)
								{
									int num31 = 0;
									int num32 = 0;
									int num33 = 0;
									int num34 = 0;
									int num35 = factor;
									int num36 = factor;
									int num37 = num30 - num5;
									for (num7 = 0; num7 < num3; num7++)
									{
										int[] array11 = array3[num7];
										int num38 = num37 + num7;
										if (num38 >= 0 && num38 < width)
										{
											int[] array12 = array5[num7];
											for (num8 = 0; num8 < num4; num8++)
											{
												int num39 = array12[num8];
												int num40 = array11[num8];
												int num41 = num29 + num8;
												if (num41 >= 0 && num41 < height)
												{
													if (ptr3[num39 >> 2] == 0)
													{
														num35 -= num40;
													}
													else
													{
														num33 += ptr[num39] * num40;
														num32 += ptr[num39 + 1] * num40;
														num31 += ptr[num39 + 2] * num40;
														num34 += ptr[num39 + 3] * num40;
													}
												}
												else
												{
													num35 -= num40;
													num36 -= num40;
												}
											}
										}
										else
										{
											for (num8 = 0; num8 < num4; num8++)
											{
												int num42 = array11[num8];
												num35 -= num42;
												num36 -= num42;
											}
										}
									}
									if (num35 < 1)
									{
										num35 = 1;
									}
									if (num36 < 1)
									{
										num36 = 1;
									}
									num33 = num33 / num35 + offset;
									num32 = num32 / num35 + offset;
									num31 = num31 / num35 + offset;
									num34 = num34 / num36 + offset;
									*ptr2 = ptr4[num33];
									ptr2[1] = ptr4[num32];
									ptr2[2] = ptr4[num31];
									ptr2[3] = ptr4[num34];
									ptr += 4;
									ptr2 += 4;
									ptr3++;
								}
								ptr3 += num10;
								ptr += num9;
								ptr2 += num9;
							}
							return bitmap;
						}
						return bitmap;
					}
				}
				finally
				{
				}
			}
			catch (Exception)
			{
				if (bitmap != null)
				{
					if (bitmapData2 != null)
					{
						bitmap.UnlockBits(bitmapData2);
						bitmapData2 = null;
					}
					bitmap.Dispose();
				}
				throw;
			}
			finally
			{
				if (bitmapData != null)
				{
					if (bitmap2 == null)
					{
						image.UnlockBits(bitmapData);
					}
					else
					{
						bitmap2.UnlockBits(bitmapData);
						bitmap2.Dispose();
					}
				}
				if (bitmapData2 != null)
				{
					bitmap.UnlockBits(bitmapData2);
				}
			}
		}

		private static byte Clamp(int src, int offset)
		{
			uint num = (uint)(src + offset);
			if (num > 255)
			{
				return byte.MaxValue;
			}
			return (byte)num;
		}

		private static int[,] PreCalculatePixelOffsets(ConvolutionMatrix matrix, int scanline)
		{
			int dimX = matrix.DimX;
			int dimY = matrix.DimY;
			int[,] array = new int[dimX, dimY];
			int num = dimX >> 1;
			int num2 = dimY >> 1;
			for (int i = 0; i < dimX; i++)
			{
				for (int j = 0; j < dimY; j++)
				{
					array[i, j] = (j - num2) * scanline + (i - num << 2);
				}
			}
			return array;
		}

		private static int[,] PreCalculatePixelOffsetsCompliment(ConvolutionMatrix matrix, int scanline)
		{
			int dimY = matrix.DimY;
			int dimX = matrix.DimX;
			int[,] array = new int[dimY, dimX];
			int num = dimY >> 1;
			int num2 = dimX >> 1;
			for (int i = 0; i < dimY; i++)
			{
				for (int j = 0; j < dimX; j++)
				{
					array[i, j] = (j - num2) * scanline + (i - num << 2);
				}
			}
			return array;
		}

		public static ConvolutionMatrix CreateGaussianMatrix(float amount)
		{
			object obj = _gaussianMatrices[amount];
			if (obj != null)
			{
				return (ConvolutionMatrix)obj;
			}
			lock (typeof(ImageEffects))
			{
				obj = _gaussianMatrices[amount];
				if (obj == null)
				{
					int num = 0;
					int num2 = (int)Math.Ceiling((double)amount);
					if ((num2 & 1) == 0)
					{
						num2++;
					}
					int[,] array = new int[num2, 1];
					double num3 = (double)(2f * amount);
					double num4 = 1.0 / (num3 * 3.1415926535897931) * 10000.0;
					int num5 = num2 >> 1;
					for (int i = 1; i <= num5; i++)
					{
						int num6 = (int)Math.Round(num4 * Math.Pow(2.7182818284590451, (double)(-(i * i)) / num3));
						array[i + num5, 0] = num6;
						array[num5 - i, 0] = num6;
						num += num6;
						num += num6;
					}
					int num7 = (int)Math.Round(num4 * Math.Pow(2.7182818284590451, 0.0));
					array[num5, 0] = num7;
					num += num7;
					if (num == 0)
					{
						num = 1;
					}
					int num8 = 0;
					while (num8 < num2)
					{
						if (array[num8, 0] == 0)
						{
							num8++;
							continue;
						}
						if (num8 == 0)
						{
							break;
						}
						int[,] array2 = new int[num2 - num8 - num8, 1];
						for (int j = 0; j < num2 - num8 - num8; j++)
						{
							int[,] array3 = array2;
							int num9 = j;
							int num10 = array[j + num8, 0];
							array3[num9, 0] = num10;
						}
						array = array2;
						break;
					}
					obj = new ConvolutionMatrix(array, num, 0, true);
					_gaussianMatrices[amount] = obj;
				}
			}
			return (ConvolutionMatrix)obj;
		}

		public static void DrawStringWithShadow(Graphics g, string text, Font font, Color color, Rectangle layoutRect, DropShadow shadow, TextFormatFlags format)
		{
			if (text != null && text.Length != 0)
			{
				if (shadow != null && shadow.Visible)
				{
					int num = shadow.Size / 2;
					if ((num & 1) == 0)
					{
						num++;
					}
					int size = shadow.Size;
					using (Bitmap bitmap = new Bitmap(layoutRect.Width + size, layoutRect.Height + size + size, PixelFormat.Format32bppArgb))
					{
						bitmap.MakeTransparent(Color.White);
						using (Graphics graphics = Graphics.FromImage(bitmap))
						{
							graphics.TextRenderingHint = g.TextRenderingHint;
							graphics.SmoothingMode = g.SmoothingMode;
							graphics.CompositingMode = g.CompositingMode;
							graphics.CompositingQuality = g.CompositingQuality;
							TextRenderer.DrawText(graphics, text, font, new Rectangle(num, num, layoutRect.Width, layoutRect.Height), shadow.Color, format);
						}
						using (Bitmap bitmap2 = Convolution(bitmap, CreateGaussianMatrix((float)shadow.Size)))
						{
							ColorMatrix colorMatrix = new ColorMatrix(_identityMatrix);
							using (ImageAttributes imageAttributes = new ImageAttributes())
							{
								colorMatrix[3, 3] = (float)shadow.Opacity / 255f;
								imageAttributes.ClearColorMatrix();
								imageAttributes.SetColorMatrix(colorMatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
								g.DrawImage(bitmap2, new Rectangle(layoutRect.X + shadow.OffsetX - num, layoutRect.Y + shadow.OffsetY - num, bitmap2.Width, bitmap2.Height), 0, 0, bitmap2.Width, bitmap2.Height, GraphicsUnit.Pixel, imageAttributes);
							}
						}
					}
				}
				TextRenderer.DrawText(g, text, font, layoutRect, color, format);
			}
		}

		public static Color CreateAdjustedColor(Color baseColor, int brightness)
		{
			int num = baseColor.R + brightness * 1000 / 1000;
			int num2 = baseColor.G + brightness * 1000 / 1000;
			int num3 = baseColor.B + brightness * 1000 / 1000;
			return Color.FromArgb(baseColor.A, (num > 255) ? 255 : ((num >= 0) ? num : 0), (num2 > 255) ? 255 : ((num2 >= 0) ? num2 : 0), (num3 > 255) ? 255 : ((num3 >= 0) ? num3 : 0));
		}

		public static Color CreateDodgeColor(Color baseColor, int amount)
		{
			int r = baseColor.R;
			int g = baseColor.G;
			int b = baseColor.B;
			int red = BoostDodge(r, amount);
			int green = BoostDodge(g, amount);
			int blue = BoostDodge(b, amount);
			return Color.FromArgb(baseColor.A, red, green, blue);
		}

		private static byte BoostDodge(int original, int amount)
		{
			int num = original * 255 / (255 - amount);
			if (num > 255)
			{
				return byte.MaxValue;
			}
			return (byte)num;
		}

		public static Color CreateBurnColor(Color baseColor, int amount)
		{
			int r = baseColor.R;
			int g = baseColor.G;
			int b = baseColor.B;
			amount = 255 - amount;
			int red = BoostBurn(r, amount);
			int green = BoostBurn(g, amount);
			int blue = BoostBurn(b, amount);
			return Color.FromArgb(baseColor.A, red, green, blue);
		}

		private static byte BoostBurn(int original, int amount)
		{
			int num = 255 - 255 * (255 - original) / amount;
			if (num < 0)
			{
				return 0;
			}
			return (byte)num;
		}

		public static void DrawImageWithShadow(Graphics g, Image image, DropShadow shadow, Rectangle bounds)
		{
			if (image != null)
			{
				if (shadow != null && shadow.Visible)
				{
					using (Bitmap image2 = new Bitmap(image.Width + shadow.Size * 2, image.Height + shadow.Size * 2, PixelFormat.Format32bppArgb))
					{
						using (Graphics graphics = Graphics.FromImage(image2))
						{
							graphics.DrawImage(image, shadow.Size, shadow.Size);
						}
						FillImageColor(image2, shadow.Color, false);
						ColorMatrix colorMatrix = new ColorMatrix(_identityMatrix);
						using (ImageAttributes imageAttributes = new ImageAttributes())
						{
							colorMatrix[3, 3] = (float)shadow.Opacity / 255f;
							imageAttributes.ClearColorMatrix();
							imageAttributes.SetColorMatrix(colorMatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
							using (Bitmap bitmap = Convolution(image2, CreateGaussianMatrix((float)shadow.Size)))
							{
								g.DrawImage(bitmap, new Rectangle(bounds.X - shadow.Size + shadow.OffsetX, bounds.Y - shadow.Size + shadow.OffsetY, bitmap.Width, bitmap.Height), 0, 0, bitmap.Width, bitmap.Height, GraphicsUnit.Pixel, imageAttributes);
							}
						}
					}
				}
				g.DrawImage(image, bounds.X, bounds.Y);
			}
		}

		public static TextFormatFlags BuildTextFormatFlags(ContentAlignment align, bool noWrap)
		{
			TextFormatFlags textFormatFlags = TextFormatFlags.NoClipping;
			textFormatFlags = ((!noWrap) ? (textFormatFlags | TextFormatFlags.WordBreak) : (textFormatFlags | TextFormatFlags.SingleLine));
			switch (align)
			{
			case ContentAlignment.MiddleCenter:
				textFormatFlags |= (TextFormatFlags.HorizontalCenter | TextFormatFlags.VerticalCenter);
				break;
			case ContentAlignment.MiddleLeft:
				textFormatFlags |= TextFormatFlags.VerticalCenter;
				break;
			case ContentAlignment.TopLeft:
				textFormatFlags = textFormatFlags;
				break;
			case ContentAlignment.TopCenter:
				textFormatFlags |= TextFormatFlags.HorizontalCenter;
				break;
			case ContentAlignment.TopRight:
				textFormatFlags |= TextFormatFlags.Right;
				break;
			case ContentAlignment.BottomLeft:
				textFormatFlags |= TextFormatFlags.Bottom;
				break;
			case ContentAlignment.MiddleRight:
				textFormatFlags |= (TextFormatFlags.Right | TextFormatFlags.VerticalCenter);
				break;
			case ContentAlignment.BottomRight:
				textFormatFlags |= (TextFormatFlags.Bottom | TextFormatFlags.Right);
				break;
			case ContentAlignment.BottomCenter:
				textFormatFlags |= (TextFormatFlags.Bottom | TextFormatFlags.HorizontalCenter);
				break;
			}
			return textFormatFlags;
		}

		public static string ToRGBHex(Color color)
		{
			if (color.IsKnownColor)
			{
				return color.Name;
			}
			if (color.A == 255)
			{
				return string.Format("#{0:X2}{1:X2}{2:X2}", color.R, color.G, color.B);
			}
			return string.Format("#{0:X2}{1:X2}{2:X2}{3:X2}", color.R, color.G, color.B, color.A);
		}

		public static Color FromRGBHex(string hex)
		{
			if (hex == null)
			{
				return Color.Empty;
			}
			if (hex.StartsWith("#"))
			{
				int red = int.Parse(hex.Substring(1, 2), NumberStyles.HexNumber);
				int green = int.Parse(hex.Substring(3, 2), NumberStyles.HexNumber);
				int blue = int.Parse(hex.Substring(5, 2), NumberStyles.HexNumber);
				int alpha = 255;
				if (hex.Length == 9)
				{
					alpha = int.Parse(hex.Substring(7, 2), NumberStyles.HexNumber);
				}
				return Color.FromArgb(alpha, red, green, blue);
			}
			return Color.FromName(hex);
		}
	}
}
