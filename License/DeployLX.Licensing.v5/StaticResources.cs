using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Reflection.Emit;
using System.Resources;
using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	public sealed class StaticResources
	{
		private Hashtable _managers = new Hashtable();

		private readonly List<Assembly> _registeredTranslations = new List<Assembly>();

		private string _resourceName;

		public string ResourceName
		{
			get
			{
				return _resourceName;
			}
		}

		public StaticResources()
		{
		}

		public StaticResources(string resourceName)
		{
			_resourceName = resourceName;
		}

		public string GetString(string name, bool inherit, params object[] args)
		{
			if (name == null)
			{
				return null;
			}
			Check.NotNull(_resourceName, "this.ResourceName");
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (name.Length > 0 && name[0] == '#')
			{
				name = name.Substring(1);
			}
			SecureLicenseContext currentContext = SecureLicenseManager.CurrentContext;
			if (currentContext != null && currentContext.StackTrace != null)
			{
				string text = TryStackTrace(name, currentContext.StackTrace, inherit, args);
				if (text != name)
				{
					return text;
				}
			}
			StackTrace trace = new StackTrace(1, false);
			return TryStackTrace(name, trace, inherit, args);
		}

		private string TryStackTrace(string name, StackTrace trace, bool inherit, object[] args)
		{
			StackFrame[] frames = trace.GetFrames();
			int num = (!inherit) ? 1 : trace.FrameCount;
			Assembly assemblyFromStack = GetAssemblyFromStack(frames, 0);
			string assemblyName = GetAssemblyName(assemblyFromStack);
			ArrayList arrayList = new ArrayList();
			int num2 = num - 1;
			string text;
			while (true)
			{
				if (num2 >= 0)
				{
					Assembly assembly = null;
					assembly = GetAssemblyFromStack(frames, num2);
					if (assembly != null && !arrayList.Contains(assembly.GetHashCode()))
					{
						arrayList.Add(assembly.GetHashCode());
						string fullName = assembly.FullName;
						if (!(assembly is AssemblyBuilder) && !(assembly.GetType().Name == "InternalAssemblyBuilder") && !fullName.StartsWith("mscorlib") && !fullName.StartsWith("System") && !fullName.StartsWith("Microsoft.") && !fullName.StartsWith("vshost"))
						{
							text = TryAssembly(name, assemblyFromStack, assembly, assemblyName, args);
							if (text != null)
							{
								break;
							}
						}
					}
					num2--;
					continue;
				}
				foreach (Assembly registeredTranslation in _registeredTranslations)
				{
					text = TryAssembly(name, assemblyFromStack, registeredTranslation, assemblyName, args);
					if (text != null)
					{
						return text;
					}
				}
				if (name != null && args != null && args.Length > 0)
				{
					return string.Format(name, args);
				}
				return name;
			}
			return text;
		}

		private string TryAssembly(string name, Assembly rootAsm, Assembly asm, string rootName, object[] args)
		{
			object obj = _managers[asm.GetHashCode()];
			ResourceManager resourceManager;
			if (obj == null)
			{
				resourceManager = FindResourceManager(_resourceName, asm);
				if (resourceManager == null && rootAsm != null && rootAsm != asm)
				{
					resourceManager = FindResourceManager(rootName, asm);
				}
				if (resourceManager == null)
				{
					resourceManager = FindResourceManager(GetAssemblyName(asm), asm);
				}
				if (resourceManager == null)
				{
					_managers[asm.GetHashCode()] = new object();
					return null;
				}
			}
			else
			{
				resourceManager = (obj as ResourceManager);
				if (resourceManager == null)
				{
					return null;
				}
			}
			string @string;
			try
			{
				@string = resourceManager.GetString(name);
			}
			catch (FileNotFoundException)
			{
				return null;
			}
			catch (MissingManifestResourceException)
			{
				return null;
			}
			catch (Exception)
			{
				return null;
			}
			if (!(@string == name) && @string != null)
			{
				if (args != null && args.Length != 0)
				{
					return string.Format(@string, args);
				}
				return @string;
			}
			return null;
		}

		private static string GetAssemblyName(Assembly asm)
		{
			string fullName = asm.FullName;
			fullName = fullName.Substring(0, fullName.IndexOf(',')).Trim();
			return fullName.Replace(' ', '_');
		}

		private static Assembly GetAssemblyFromStack(StackFrame[] frames, int index)
		{
			Assembly result = null;
			try
			{
				StackFrame stackFrame = frames[index];
				MethodBase method = stackFrame.GetMethod();
				if (method != null && method.DeclaringType != null)
				{
					result = method.DeclaringType.Assembly;
					return result;
				}
				return null;
			}
			catch
			{
				return result;
			}
		}

		private static Assembly GetFirstAssemblyNotMe(StackFrame[] frames, ref int index)
		{
			Assembly assembly = null;
			Assembly executingAssembly = Assembly.GetExecutingAssembly();
			while (true)
			{
				if (index < frames.Length)
				{
					assembly = GetAssemblyFromStack(frames, index);
					if (assembly != null && assembly != executingAssembly)
					{
						break;
					}
					index++;
					continue;
				}
				return null;
			}
			return assembly;
		}

		private ResourceManager FindResourceManager(string resourceName, Assembly asm)
		{
			ResourceManager resourceManager = null;
			string[] manifestResourceNames = asm.GetManifestResourceNames();
			foreach (string text in manifestResourceNames)
			{
				if (text.Length - 10 >= resourceName.Length && string.Compare(text, text.Length - resourceName.Length - 10, resourceName, 0, resourceName.Length, true) == 0)
				{
					string baseName = text.Substring(0, text.Length - 10);
					resourceManager = new ResourceManager(baseName, asm);
					_managers[asm.GetHashCode()] = resourceManager;
					break;
				}
			}
			return resourceManager;
		}

		public void LocalizeObject(object root, bool inherit, bool includeChildren, params object[] args)
		{
			ArrayList stack = new ArrayList();
			LocalizeObjectInternal(root, inherit, includeChildren, stack, null, args);
		}

		private void LocalizeObjectInternal(object root, bool inherit, bool includeChildren, ArrayList stack, string childType, object[] args)
		{
			if (root != null && !stack.Contains(root))
			{
				stack.Add(root);
				Type type = root.GetType();
				MemberInfo[] members = type.GetMembers(BindingFlags.Instance | BindingFlags.Public | BindingFlags.GetField | BindingFlags.GetProperty);
				object[] index = new object[0];
				MemberInfo[] array = members;
				foreach (MemberInfo memberInfo in array)
				{
					PropertyInfo propertyInfo = memberInfo as PropertyInfo;
					if (propertyInfo != null)
					{
						if (propertyInfo.PropertyType == typeof(string) && propertyInfo.CanWrite && propertyInfo.CanRead)
						{
							string text = propertyInfo.GetValue(root, index) as string;
							if (text != null && text.Length > 1 && text[0] == '#')
							{
								string text2 = text.Substring(1);
								text = GetString(text2, inherit, args);
								if (!(text == text2))
								{
									propertyInfo.SetValue(root, text, index);
								}
							}
						}
					}
					else
					{
						FieldInfo fieldInfo = memberInfo as FieldInfo;
						if (fieldInfo != null && fieldInfo.FieldType == typeof(string))
						{
							string text3 = fieldInfo.GetValue(root) as string;
							if (text3 != null && text3.Length > 1 && text3[0] == '#')
							{
								string text4 = text3.Substring(1);
								text3 = GetString(text4, inherit, args);
								if (!(text3 == text4))
								{
									fieldInfo.SetValue(root, text3);
								}
							}
						}
					}
				}
				if (includeChildren)
				{
					MemberInfo[] array2 = members;
					int num = 0;
					while (true)
					{
						if (num >= array2.Length)
						{
							break;
						}
						MemberInfo memberInfo2 = array2[num];
						PropertyInfo propertyInfo2 = memberInfo2 as PropertyInfo;
						if (propertyInfo2 != null && propertyInfo2.CanRead)
						{
							if (propertyInfo2.PropertyType.IsValueType || propertyInfo2.PropertyType == typeof(string))
							{
								goto IL_0227;
							}
							try
							{
								object value = propertyInfo2.GetValue(root, index);
								LocalizeChild(inherit, stack, childType, args, value);
							}
							catch
							{
							}
						}
						FieldInfo fieldInfo2 = memberInfo2 as FieldInfo;
						if (fieldInfo2 != null && !fieldInfo2.FieldType.IsValueType && fieldInfo2.FieldType != typeof(string))
						{
							try
							{
								object value2 = fieldInfo2.GetValue(root);
								LocalizeChild(inherit, stack, childType, args, value2);
							}
							catch
							{
							}
						}
						goto IL_0227;
						IL_0227:
						num++;
					}
				}
			}
		}

		private void LocalizeChild(bool inherit, ArrayList stack, string childType, object[] args, object child)
		{
			if (child is IEnumerable)
			{
				foreach (object item in (IEnumerable)child)
				{
					if (child != null && (childType == null || IsOfType(child.GetType(), childType)))
					{
						LocalizeObjectInternal(item, inherit, true, stack, childType, args);
					}
				}
			}
			else if (child != null)
			{
				if (childType != null && !IsOfType(child.GetType(), childType))
				{
					return;
				}
				LocalizeObjectInternal(child, inherit, true, stack, childType, args);
			}
		}

		private static bool IsOfType(Type type, string stringType)
		{
			while (type != null)
			{
				if (type.Name == stringType)
				{
					return true;
				}
				type = type.BaseType;
				if (type == typeof(object))
				{
					break;
				}
			}
			return false;
		}

		public void LocalizeControl(Control root, bool inherit, bool includeChildren, params object[] args)
		{
			ArrayList stack = new ArrayList();
			LocalizeControl(root, inherit, includeChildren, args, stack);
		}

		private void LocalizeControl(Control root, bool inherit, bool includeChildren, object[] args, ArrayList stack)
		{
			if (root != null && !stack.Contains(root))
			{
				if (IsAutoName(root.Text))
				{
					root.Text = GetString(root.Text.Substring(1), inherit, args);
				}
				LicenseCodeTextBox licenseCodeTextBox = root as LicenseCodeTextBox;
				if (licenseCodeTextBox != null && IsAutoName(licenseCodeTextBox.Prompt))
				{
					licenseCodeTextBox.Prompt = GetString(licenseCodeTextBox.Prompt.Substring(1), inherit, args);
				}
				if (includeChildren)
				{
					stack.Add(root);
					foreach (Control control in root.Controls)
					{
						LocalizeControl(control, inherit, true, args, stack);
					}
					ListView listView = root as ListView;
					if (listView != null)
					{
						foreach (ColumnHeader column in listView.Columns)
						{
							column.Text = GetString(column.Text, inherit, args);
						}
					}
				}
			}
		}

		public static bool IsAutoName(string name)
		{
			if (name != null && name.Length > 1 && name[0] == '#')
			{
				return true;
			}
			return false;
		}

		public bool RegisterAssembly(Assembly asm, string resourceName)
		{
			ResourceManager resourceManager = FindResourceManager(resourceName ?? ResourceName, asm);
			if (resourceManager != null)
			{
				_managers[asm.GetHashCode()] = resourceManager;
				_registeredTranslations.Add(asm);
				return true;
			}
			return false;
		}
	}
}
