using System;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	public sealed class OSRecord : IChange
	{
		private bool _isReadOnly;

		private Version _version;

		private string _servicePack;

		private OSProduct _product;

		private OSEditions _edition;

		private static OSRecord _thisMachine;

		public Version Version
		{
			get
			{
				return _version;
			}
			set
			{
				AssertNotReadOnly();
				if (_version != value)
				{
					_version = value;
					OnChanged("Version");
				}
			}
		}

		public string ServicePack
		{
			get
			{
				return _servicePack;
			}
			set
			{
				AssertNotReadOnly();
				if (_servicePack != value)
				{
					_servicePack = value;
					OnChanged("ServicePack");
				}
			}
		}

		public OSProduct Product
		{
			get
			{
				return _product;
			}
			set
			{
				AssertNotReadOnly();
				if (_product != value)
				{
					_product = value;
					OnChanged("Product");
				}
			}
		}

		public OSEditions Edition
		{
			get
			{
				return _edition;
			}
			set
			{
				AssertNotReadOnly();
				if (_edition != value)
				{
					_edition = value;
					OnChanged("Edition");
				}
			}
		}

		public static OSRecord ThisMachine
		{
			get
			{
				if (_thisMachine == null)
				{
					lock (SecureLicenseManager.SyncRoot)
					{
						if (_thisMachine == null)
						{
							_thisMachine = GetCurrentOS();
						}
					}
				}
				return _thisMachine;
			}
		}

		public event ChangeEventHandler Changed;

		private void OnChanged(string property)
		{
			if (this.Changed != null)
			{
				this.Changed(this, new ChangeEventArgs(property, this));
			}
		}

		private static OSRecord GetCurrentOS()
		{
			OSRecord oSRecord = new OSRecord();
			oSRecord._version = Environment.OSVersion.Version;
			switch (Environment.OSVersion.Platform)
			{
			case PlatformID.Win32S:
				oSRecord._product = OSProduct.Windows32s;
				break;
			case PlatformID.Win32Windows:
				oSRecord._product = OSProduct.Windows9x;
				break;
			case PlatformID.Win32NT:
				if (oSRecord._version.Major > 4)
				{
					byte[] array = new byte[284]
					{
						28,
						1,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0,
						0
					};
					GCHandle gCHandle = GCHandle.Alloc(array, GCHandleType.Pinned);
					try
					{
						if (!SafeNativeMethods.GetVersionEx(gCHandle.AddrOfPinnedObject()))
						{
							array[0] = 20;
							array[1] = 1;
							SafeNativeMethods.GetVersionEx(gCHandle.AddrOfPinnedObject());
						}
					}
					finally
					{
						gCHandle.Free();
					}
					int major = BitConverter.ToInt32(array, 4);
					int minor = BitConverter.ToInt32(array, 8);
					int build = BitConverter.ToInt32(array, 12);
					oSRecord._version = new Version(major, minor, build);
					switch (array[282])
					{
					case 1:
						oSRecord._edition |= OSEditions.Workstation;
						break;
					case 2:
						oSRecord._edition |= OSEditions.DomainController;
						break;
					case 3:
						oSRecord._edition |= OSEditions.Server;
						break;
					}
					int num = BitConverter.ToInt16(array, 280);
					if ((num & 0x200) != 0)
					{
						oSRecord._edition |= OSEditions.Home;
					}
					else if ((oSRecord._edition & (OSEditions.Server | OSEditions.DomainController)) == OSEditions.NotSet)
					{
						oSRecord._edition |= OSEditions.Professional;
					}
					int count = 256;
					int num2 = 20;
					while (num2 < 276)
					{
						if (array[num2] != 0)
						{
							num2 += 2;
							continue;
						}
						count = num2 - 20;
						break;
					}
					oSRecord._servicePack = Encoding.Unicode.GetString(array, 20, count);
					if (SafeNativeMethods.GetSystemMetrics(87) != 0)
					{
						oSRecord._edition |= OSEditions.MediaCenter;
					}
					if (SafeNativeMethods.GetSystemMetrics(86) != 0)
					{
						oSRecord._edition |= OSEditions.TabletPC;
					}
				}
				switch (oSRecord._version.Major)
				{
				case 3:
				case 4:
					oSRecord._product = OSProduct.WindowsNT;
					break;
				case 5:
					switch (oSRecord.Version.Minor)
					{
					case 0:
						oSRecord._product = OSProduct.Windows2000;
						break;
					case 1:
						oSRecord._product = OSProduct.WindowsXP;
						break;
					case 2:
						if ((oSRecord._edition & OSEditions.Workstation) != 0)
						{
							oSRecord._product = OSProduct.WindowsXP;
						}
						else
						{
							oSRecord._product = OSProduct.Windows2003;
						}
						break;
					}
					break;
				case 6:
					switch (oSRecord.Version.Minor)
					{
					default:
						if ((oSRecord._edition & OSEditions.Workstation) != 0)
						{
							oSRecord._product = OSProduct.WindowsVista;
						}
						else
						{
							oSRecord._product = OSProduct.Windows2008;
						}
						break;
					case 1:
						oSRecord._product = OSProduct.Windows7;
						break;
					case 2:
						if ((oSRecord._edition & OSEditions.Workstation) != 0)
						{
							oSRecord._product = OSProduct.Windows8;
						}
						else
						{
							oSRecord._product = OSProduct.Windows2012;
						}
						break;
					}
					break;
				}
				break;
			case PlatformID.WinCE:
				oSRecord._product = OSProduct.WindowsCE;
				break;
			}
			if (IntPtr.Size == 8)
			{
				oSRecord._edition |= OSEditions.SixtyFourBit;
			}
			else if (Environment.GetEnvironmentVariable("PROCESSOR_ARCHITEW6432") != null)
			{
				oSRecord._edition |= OSEditions.SixtyFourBit;
			}
			else
			{
				oSRecord._edition |= OSEditions.ThirtyTwoBit;
			}
			return oSRecord;
		}

		public bool IsMatch(OSRecord record)
		{
			if (_product != 0 && record._product != 0 && _product != record._product)
			{
				return false;
			}
			if (_edition != 0 && record._edition != 0)
			{
				foreach (int value in Enum.GetValues(typeof(OSEditions)))
				{
					OSEditions oSEditions = (OSEditions)((int)record._edition & value);
					OSEditions oSEditions2 = (OSEditions)((int)_edition & value);
					if (oSEditions != 0 && oSEditions2 == OSEditions.NotSet)
					{
						return false;
					}
				}
			}
			if (_version != (Version)null && record._version != (Version)null)
			{
				if (_version.Major != record._version.Major)
				{
					return false;
				}
				if (record._version.Minor > -1 && _version.Minor != record._version.Minor)
				{
					return false;
				}
				if (record._version.Build > -1 && _version.Build != record._version.Build)
				{
					return false;
				}
			}
			if (record._servicePack != null && (_servicePack == null || string.Compare(_servicePack, record._servicePack, true) != 0))
			{
				return false;
			}
			return true;
		}

		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder(200);
			switch (_product)
			{
			case OSProduct.WindowsCE:
				stringBuilder.Append("Windows CE");
				break;
			case OSProduct.NotSet:
				stringBuilder.Append("Unknown OS");
				break;
			case OSProduct.Windows9x:
				stringBuilder.Append("Windows 9x");
				break;
			case OSProduct.WindowsNT:
				stringBuilder.Append("Windows NT");
				break;
			case OSProduct.Windows32s:
				stringBuilder.Append("Windows 3.x");
				break;
			case OSProduct.WindowsVista:
				stringBuilder.Append("Windows Vista");
				break;
			case OSProduct.Windows2008:
				stringBuilder.Append("Windows Server 2008");
				break;
			case OSProduct.Windows2000:
				stringBuilder.Append("Windows 2000");
				break;
			case OSProduct.WindowsXP:
				stringBuilder.Append("Windows XP");
				break;
			case OSProduct.Windows2003:
				stringBuilder.Append("Windows 2003");
				break;
			default:
				stringBuilder.Append("ERROR BAD OS");
				break;
			case OSProduct.Unknown:
				stringBuilder.Append("UNIX");
				break;
			case OSProduct.Windows8:
				stringBuilder.Append("Windows 8");
				break;
			case OSProduct.Windows2012:
				stringBuilder.Append("Windows Server 2012");
				break;
			case OSProduct.Windows7:
				stringBuilder.Append("Windows 7");
				break;
			}
			if (_product < OSProduct.WindowsVista)
			{
				if ((_edition & OSEditions.Home) != 0)
				{
					stringBuilder.Append(" Home");
				}
				if ((_edition & OSEditions.Professional) != 0)
				{
					stringBuilder.Append(" Professional");
				}
				if ((_edition & (OSEditions.Server | OSEditions.DomainController)) != 0)
				{
					stringBuilder.Append(" Server");
				}
				if ((_edition & OSEditions.TabletPC) != 0)
				{
					stringBuilder.Append(" Tablet PC");
				}
				if ((_edition & OSEditions.MediaCenter) != 0)
				{
					stringBuilder.Append(" Media Center");
				}
			}
			if ((_edition & (OSEditions.Server | OSEditions.DomainController)) != 0)
			{
				stringBuilder.Append(" Server");
			}
			if ((_edition & OSEditions.ThirtyTwoBit) != 0)
			{
				stringBuilder.Append(" 32-bit");
			}
			if ((_edition & OSEditions.SixtyFourBit) != 0)
			{
				stringBuilder.Append(" 64-bit");
			}
			if (_servicePack != null && _servicePack.Length > 0)
			{
				stringBuilder.Append(' ');
				stringBuilder.Append(_servicePack);
			}
			if (_version != (Version)null && _version.Major > 0)
			{
				stringBuilder.Append(" [");
				stringBuilder.Append(_version.Major);
				if (_version.Minor > -1)
				{
					stringBuilder.Append('.');
					stringBuilder.Append(_version.Minor);
					if (_version.Build > -1)
					{
						stringBuilder.Append(" Build ");
						stringBuilder.Append(_version.Build);
					}
				}
				stringBuilder.Append(']');
			}
			return stringBuilder.ToString();
		}

		public bool WriteToXml(XmlWriter writer)
		{
			writer.WriteStartElement("OSRecord");
			if (_product != 0)
			{
				int product = (int)_product;
				writer.WriteAttributeString("product", product.ToString());
			}
			if (_edition != 0)
			{
				int edition = (int)_edition;
				writer.WriteAttributeString("edition", edition.ToString());
			}
			if (_version != (Version)null && _version.Major > 0)
			{
				writer.WriteAttributeString("version", _version.ToString());
			}
			if (_servicePack != null && _servicePack.Length > 0)
			{
				writer.WriteAttributeString("servicePack", _servicePack);
			}
			writer.WriteEndElement();
			return true;
		}

		public bool ReadFromXml(XmlReader reader)
		{
			_product = OSProduct.NotSet;
			_edition = OSEditions.NotSet;
			_version = null;
			string attribute = reader.GetAttribute("product");
			if (attribute != null)
			{
				_product = (OSProduct)SafeToolbox.FastParseInt32(attribute);
			}
			attribute = reader.GetAttribute("edition");
			if (attribute != null)
			{
				_edition = (OSEditions)SafeToolbox.FastParseInt32(attribute);
			}
			attribute = reader.GetAttribute("version");
			if (attribute != null)
			{
				_version = new Version(attribute);
			}
			_servicePack = reader.GetAttribute("servicePack");
			reader.Read();
			return true;
		}

		void IChange.MakeReadOnly()
		{
			_isReadOnly = true;
		}

		private void AssertNotReadOnly()
		{
			if (!_isReadOnly)
			{
				return;
			}
			throw new SecureLicenseException("E_ReadOnlyObject", "OSRecord");
		}
	}
}
