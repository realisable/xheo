using System;
using System.ComponentModel;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public sealed class IPRangeCollection : WithEventsCollection
	{
		public IPRange this[int index]
		{
			get
			{
				return base.List[index] as IPRange;
			}
			set
			{
				base.List[index] = value;
			}
		}

		protected override void OnChanged(CollectionEventArgs e)
		{
			IPRange iPRange = e.Element as IPRange;
			if (e.Action == CollectionChangeAction.Add)
			{
				if (iPRange._collection != null)
				{
					throw new NotSupportedException();
				}
				iPRange._collection = this;
			}
			else if (e.Action == CollectionChangeAction.Remove)
			{
				iPRange._collection = null;
			}
			base.OnChanged(e);
		}

		public void Add(IPRange range)
		{
			base.List.Add(range);
		}

		public void Insert(int index, IPRange range)
		{
			base.List.Insert(index, range);
		}

		public void Remove(IPRange range)
		{
			base.List.Remove(range);
		}

		public void CopyTo(IPRange[] array, int index)
		{
			base.List.CopyTo(array, index);
		}

		public int IndexOf(IPRange range)
		{
			return base.List.IndexOf(range);
		}

		public bool Contains(IPRange range)
		{
			return base.List.Contains(range);
		}
	}
}
