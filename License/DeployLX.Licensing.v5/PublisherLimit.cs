using System;
using System.Collections;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	[LimitEditor("PublisherLimitEditor", IconResource = "asmres://DeployLX.Licensing.DlxAddIn.v5/DeployLX.Licensing.DlxAddIn.v5.Resources.Limits.Publisher.png")]
	public class PublisherLimit : Limit
	{
		private string _publisherName;

		private string _publicKeyToken;

		private bool _forceStrongNameVerification = true;

		private bool _checkOnce = true;

		public override string Description
		{
			get
			{
				return SR.GetString("M_PublisherLimitDescription");
			}
		}

		public override string Name
		{
			get
			{
				return "Publisher";
			}
		}

		public string PublisherName
		{
			get
			{
				return _publisherName;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_publisherName != value)
				{
					_publisherName = value;
					base.OnChanged("PublisherName");
				}
			}
		}

		public string PublicKeyToken
		{
			get
			{
				return _publicKeyToken;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_publicKeyToken != value)
				{
					_publicKeyToken = value;
					base.OnChanged("PublicKeyToken");
				}
			}
		}

		public bool ForceStrongNameVerification
		{
			get
			{
				return _forceStrongNameVerification;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_forceStrongNameVerification != value)
				{
					_forceStrongNameVerification = value;
					base.OnChanged("ForceStrongNameVerification");
				}
			}
		}

		public bool CheckOnce
		{
			get
			{
				return _checkOnce;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_checkOnce != value)
				{
					_checkOnce = value;
					base.OnChanged("CheckOnce");
				}
			}
		}

		protected bool IsPublisherValid(SecureLicenseContext context)
		{
			string key = base.LimitId + _publicKeyToken;
			object obj = context.Items[key];
			if (obj != null)
			{
				return (int)obj == 1;
			}
			StackTrace stackTrace = context.StackTrace;
			ArrayList arrayList = new ArrayList(stackTrace.FrameCount);
			int num = 0;
			while (true)
			{
				if (num < stackTrace.FrameCount)
				{
					StackFrame frame = stackTrace.GetFrame(num);
					MethodBase method = frame.GetMethod();
					if (method != null)
					{
						Type declaringType = method.DeclaringType;
						if (declaringType != null && declaringType.Assembly != null && declaringType.Assembly != typeof(SecureLicense).Assembly && declaringType.Assembly != typeof(object).Assembly && !arrayList.Contains(declaringType.Assembly.FullName))
						{
							arrayList.Add(declaringType.Assembly.FullName);
							Assembly assembly = method.DeclaringType.Assembly;
							string fullName = assembly.FullName;
							string text = null;
							int num2 = fullName.IndexOf("PublicKeyToken=");
							int num3 = 0;
							if (num2 > -1)
							{
								num2 += 15;
								num3 = fullName.IndexOf(',', num2);
								text = ((num3 <= -1) ? fullName.Substring(num2) : fullName.Substring(num2, num3 - num2));
								if (string.Compare(_publicKeyToken, text, false, CultureInfo.InvariantCulture) == 0)
								{
									if (!_forceStrongNameVerification)
									{
										break;
									}
									if (assembly.Location == null)
									{
										break;
									}
									if (Check.Crc(assembly.Location))
									{
										break;
									}
								}
							}
						}
					}
					num++;
					continue;
				}
				context.Items[key] = 0;
				return false;
			}
			context.Items[key] = 1;
			return true;
		}

		public override ValidationResult Validate(SecureLicenseContext context)
		{
			if (!IsPublisherValid(context))
			{
				string text = _publisherName;
				if (text == null)
				{
					text = SR.GetString("E_PublisherWithToken", _publicKeyToken);
				}
				return context.ReportError("E_InvalidPublisher", this, text);
			}
			return base.Validate(context);
		}

		public override ValidationResult Granted(SecureLicenseContext context)
		{
			if (!_checkOnce && !IsPublisherValid(context))
			{
				string text = _publisherName;
				if (text == null)
				{
					text = SR.GetString("E_PublisherWithToken", _publicKeyToken);
				}
				return context.ReportError("E_InvalidPublisher", this, text);
			}
			return base.Granted(context);
		}

		public override PeekResult Peek(SecureLicenseContext context)
		{
			if (!IsPublisherValid(context))
			{
				return PeekResult.Invalid;
			}
			return base.Peek(context);
		}

		protected override bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			if (_publicKeyToken == null)
			{
				return false;
			}
			writer.WriteAttributeString("token", _publicKeyToken);
			if (_publisherName != null)
			{
				writer.WriteAttributeString("publisher", _publisherName);
			}
			if (!_checkOnce)
			{
				writer.WriteAttributeString("checkOnce", "false");
			}
			if (!_forceStrongNameVerification)
			{
				writer.WriteAttributeString("forceVerification", "false");
			}
			return true;
		}

		protected override bool ReadFromXml(XmlReader reader)
		{
			_publicKeyToken = reader.GetAttribute("token");
			if (_publicKeyToken == null)
			{
				return false;
			}
			_publisherName = reader.GetAttribute("publisher");
			_forceStrongNameVerification = (reader.GetAttribute("forceVerification") != "false");
			_checkOnce = (reader.GetAttribute("checkOnce") != "false");
			return base.ReadChildLimits(reader);
		}
	}
}
