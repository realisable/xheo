using System;
using System.Runtime.InteropServices;
using System.Security;

namespace DeployLX.Licensing.v5
{
	[SuppressUnmanagedCodeSecurity]
	internal sealed class Native
	{
		private Native()
		{
		}

		private static void MakeMeRun(byte[] code)
		{
			int num;
			if (!SafeNativeMethods.VirtualProtect(code, new IntPtr(code.Length), 64, out num))
			{
				Marshal.ThrowExceptionForHR(Marshal.GetHRForLastWin32Error());
			}
		}

		[SecuritySafeCritical]
		public static bool Cpuid(byte[] result)
		{
			byte[] array = SafeNativeMethods.IsWin64 ? _cpuid64Bytes.bytes : _cpuid32Bytes.bytes;
			MakeMeRun(array);
			return CallWindowProcW(array, IntPtr.Zero, 0, result, new IntPtr(result.Length)) != IntPtr.Zero;
		}

		[DllImport("user32", CharSet = CharSet.Unicode, ExactSpelling = true, SetLastError = true)]
		private static extern IntPtr CallWindowProcW([In] byte[] bytes, IntPtr hWnd, int msg, [In] [Out] byte[] wParam, IntPtr lParam);
	}
}
