namespace DeployLX.Licensing.v5
{
	public enum OSProduct
	{
		NotSet,
		Windows32s = 48,
		Windows9x = 0x40,
		WindowsNT,
		Windows2000 = 80,
		WindowsXP,
		Windows2003,
		WindowsVista = 96,
		Windows2008,
		Windows7 = 112,
		Windows8 = 0x80,
		Windows2012,
		WindowsCE = 0x10,
		Unknown = 0x1000
	}
}
