using System;
using System.Collections;
using System.Collections.Specialized;
using System.Threading;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	[LimitEditor("RegistrationLimitEditor", Category = "Super Form Limits", IconResource = "asmres://DeployLX.Licensing.DlxAddIn.v5/DeployLX.Licensing.DlxAddIn.v5.Resources.Limits.Registration.png")]
	public class RegistrationLimit : SuperFormLimit, IPurchaseLimit, IServerLimit
	{
		public class PageIds
		{
			public const string Default = "DEFAULT";

			public const string Eula = "EULA";

			public const string PrivacyPolicy = "PRIVACYPOLICY";

			public const string Upgrade = "UPGRADE";
		}

		private bool _isMandatory;

		private int _reminderPeriod;

		private readonly CustomRegistrationFieldCollection _customFields;

		private string _followUpUrl;

		private string _privacyPolicyAddress;

		private string _purchaseUrl;

		private readonly UriCollection _servers = new UriCollection();

		private bool _canSkipServer;

		private int _serverRetries;

		private string _logoResource;

		private readonly StringCollection _requiredFields;

		private string _serialNumberMask;

		private bool _isRegistered;

		private DateTime _nextReminder = DateTime.MinValue;

		private bool _showActivationOption;

		private bool _autoShowForm = true;

		public bool IsMandatory
		{
			get
			{
				return _isMandatory;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_isMandatory != value)
				{
					_isMandatory = value;
					base.OnChanged("IsMandatory");
				}
			}
		}

		public int ReminderPeriod
		{
			get
			{
				return _reminderPeriod;
			}
			set
			{
				base.AssertNotReadOnly();
				Check.GreaterThanEqual(value, 0, "ReminderPeriod");
				if (_reminderPeriod != value)
				{
					_reminderPeriod = value;
					base.OnChanged("ReminderPeriod");
				}
			}
		}

		public CustomRegistrationFieldCollection CustomFields
		{
			get
			{
				return _customFields;
			}
		}

		public string FollowUpUrl
		{
			get
			{
				return _followUpUrl;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_followUpUrl != value)
				{
					SharedToolbox.ValidateUrl(value, true);
					_followUpUrl = value;
					base.OnChanged("FollowUpUrl");
				}
			}
		}

		public string PrivacyPolicyAddress
		{
			get
			{
				return _privacyPolicyAddress;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_privacyPolicyAddress != value)
				{
					SharedToolbox.ValidateUrl(value, true);
					_privacyPolicyAddress = value;
					base.OnChanged("PrivacyPolicyAddress");
				}
			}
		}

		public string PurchaseUrl
		{
			get
			{
				return _purchaseUrl;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_purchaseUrl != value)
				{
					SharedToolbox.ValidateUrl(value, true);
					_purchaseUrl = value;
					base.OnChanged("PurchaseUrl");
				}
			}
		}

		public UriCollection Servers
		{
			get
			{
				return _servers;
			}
		}

		public bool CanSkipServer
		{
			get
			{
				return _canSkipServer;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_canSkipServer != value)
				{
					_canSkipServer = value;
					base.OnChanged("CanSkipServer");
				}
			}
		}

		public int ServerRetries
		{
			get
			{
				return _serverRetries;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_serverRetries != value)
				{
					_serverRetries = value;
					base.OnChanged("ServerRetries");
				}
			}
		}

		public string LogoResource
		{
			get
			{
				return _logoResource;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_logoResource != value)
				{
					SharedToolbox.ValidateUrl(value, true);
					_logoResource = value;
					base.OnChanged("LogoResource");
				}
			}
		}

		public StringCollection RequiredFields
		{
			get
			{
				return _requiredFields;
			}
		}

		public string SerialNumberMask
		{
			get
			{
				return _serialNumberMask;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_serialNumberMask != value)
				{
					_serialNumberMask = value;
					base.OnChanged("SerialNumberMask");
				}
			}
		}

		public bool IsRegistered
		{
			get
			{
				return _isRegistered;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_isRegistered != value)
				{
					_isRegistered = value;
					base.OnChanged("IsRegistered");
				}
			}
		}

		public DateTime NextReminder
		{
			get
			{
				return _nextReminder;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_nextReminder != value)
				{
					_nextReminder = value;
					base.OnChanged("NextReminder");
				}
			}
		}

		public bool ShowActivationOption
		{
			get
			{
				return _showActivationOption;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_showActivationOption != value)
				{
					_showActivationOption = value;
					base.OnChanged("ShowActivationOption");
				}
			}
		}

		public override string Description
		{
			get
			{
				return SR.GetString("M_RegistrationLimitDescription");
			}
		}

		public override string Name
		{
			get
			{
				return "Registration";
			}
		}

		public override bool IsGui
		{
			get
			{
				if (_autoShowForm)
				{
					return true;
				}
				return base.IsGui;
			}
		}

		public bool AutoShowForm
		{
			get
			{
				return _autoShowForm;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_autoShowForm != value)
				{
					_autoShowForm = value;
					base.OnChanged("AutoShowForm");
				}
			}
		}

		public RegistrationLimit()
		{
			_customFields = new CustomRegistrationFieldCollection();
			_customFields.Changed += _customFields_Changed;
			_requiredFields = new StringCollection();
			_requiredFields.Changed += _requiredFields_Changed;
			_servers.Changed += _servers_Changed;
		}

		private void _requiredFields_Changed(object sender, CollectionEventArgs e)
		{
			base.OnCollectionChanged(sender, "RequiredFields", e);
		}

		private void _customFields_Changed(object sender, CollectionEventArgs e)
		{
			base.OnCollectionChanged(sender, "CustomFields", e);
		}

		private void _servers_Changed(object sender, CollectionEventArgs e)
		{
			base.OnCollectionChanged(sender, "Servers", e);
		}

		protected internal override void MakeReadOnly()
		{
			base.MakeReadOnly();
			_customFields.MakeReadOnly();
			_requiredFields.MakeReadOnly();
		}

		public override ValidationResult Validate(SecureLicenseContext context)
		{
			if (_isRegistered)
			{
				return base.Validate(context);
			}
			if (base.ResolveShouldShowForm(context) && context.CanShowWindowsForm)
			{
				FormResult formResult = ShowRegistrationForm(context);
				if (formResult == FormResult.Retry)
				{
					return ValidationResult.Retry;
				}
			}
			if (_isMandatory && !_isRegistered)
			{
				return context.ReportError("E_NotRegistered", this);
			}
			return base.Validate(context);
		}

		protected override bool ResolveDefaultShouldShowForm(SecureLicenseContext context)
		{
			if (_isMandatory)
			{
				return true;
			}
			if (!base.FormShown && !(_nextReminder > context.CurrentDateAndTime))
			{
				return _autoShowForm;
			}
			return false;
		}

		public override PeekResult Peek(SecureLicenseContext context)
		{
			if (!_isRegistered && _autoShowForm)
			{
				return PeekResult.NeedsUser;
			}
			return base.Peek(context);
		}

		protected override bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			if (signing == LicenseSaveType.Normal && _isRegistered)
			{
				writer.WriteAttributeString("isRegistered", "true");
			}
			if (_isMandatory)
			{
				writer.WriteAttributeString("isMandatory", "true");
			}
			if (_showActivationOption)
			{
				writer.WriteAttributeString("showActivationOption", "true");
			}
			if (_canSkipServer)
			{
				writer.WriteAttributeString("canSkipServer", "true");
			}
			if (_serverRetries > 0)
			{
				writer.WriteAttributeString("serverRetries", _serverRetries.ToString());
			}
			if (!_autoShowForm)
			{
				writer.WriteAttributeString("autoShowForm", "false");
			}
			if (_reminderPeriod > 0)
			{
				writer.WriteAttributeString("reminderPeriod", _reminderPeriod.ToString());
			}
			if (signing == LicenseSaveType.Normal && _nextReminder != DateTime.MinValue)
			{
				writer.WriteAttributeString("nextReminder", SafeToolbox.FormatSortableDate(_nextReminder));
			}
			if (_serialNumberMask != null)
			{
				writer.WriteAttributeString("serialNumberMask", _serialNumberMask);
			}
			if (_purchaseUrl != null)
			{
				writer.WriteAttributeString("purchaseUrl", SafeToolbox.XmlEncode(_purchaseUrl));
			}
			if (_followUpUrl != null)
			{
				writer.WriteAttributeString("followUpUrl", SafeToolbox.XmlEncode(_followUpUrl));
			}
			if (_privacyPolicyAddress != null)
			{
				writer.WriteAttributeString("privacyPolicyAddress", SafeToolbox.XmlEncode(_privacyPolicyAddress));
			}
			if (_logoResource != null)
			{
				writer.WriteAttributeString("logoResource", SafeToolbox.XmlEncode(_logoResource));
			}
			if (!base.WriteToXml(writer, signing))
			{
				return false;
			}
			if (_requiredFields.Count > 0)
			{
				_requiredFields.WriteToXml(writer, null, "Required", "field");
			}
			foreach (CustomRegistrationField item in (IEnumerable)_customFields)
			{
				writer.WriteStartElement("Custom");
				if (item.Name == null)
				{
					return false;
				}
				writer.WriteAttributeString("field", item.Name);
				if (item.DisplayName != null)
				{
					writer.WriteAttributeString("displayName", item.DisplayName);
				}
				writer.WriteAttributeString("type", ((int)item.FieldType).ToString());
				if (item.CustomTypeName != null && item.FieldType == CustomFieldType.Custom)
				{
					writer.WriteAttributeString("customType", item.CustomTypeName);
				}
				writer.WriteEndElement();
			}
			foreach (string item2 in (IEnumerable)_servers)
			{
				writer.WriteStartElement("Server");
				writer.WriteAttributeString("address", SafeToolbox.XmlEncode(item2));
				writer.WriteEndElement();
			}
			return true;
		}

		protected override bool ReadFromXml(XmlReader reader)
		{
			_servers.Clear();
			_canSkipServer = (reader.GetAttribute("canSkipServer") == "true");
			string attribute = reader.GetAttribute("serverRetries");
			if (attribute != null)
			{
				_serverRetries = SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_serverRetries = 0;
			}
			_customFields.Clear();
			_requiredFields.Clear();
			_isMandatory = (reader.GetAttribute("isMandatory") == "true");
			_autoShowForm = (reader.GetAttribute("autoShowForm") != "false");
			_isRegistered = (reader.GetAttribute("isRegistered") == "true");
			_serialNumberMask = reader.GetAttribute("serialNumberMask");
			_showActivationOption = (reader.GetAttribute("showActivationOption") == "true");
			attribute = reader.GetAttribute("reminderPeriod");
			if (attribute != null)
			{
				_reminderPeriod = SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_reminderPeriod = 0;
			}
			attribute = reader.GetAttribute("nextReminder");
			if (attribute != null)
			{
				_nextReminder = SafeToolbox.FastParseSortableDate(attribute);
			}
			else
			{
				_nextReminder = DateTime.MinValue;
			}
			_purchaseUrl = SafeToolbox.XmlDecode(reader.GetAttribute("purchaseUrl"));
			_followUpUrl = SafeToolbox.XmlDecode(reader.GetAttribute("followUpUrl"));
			_privacyPolicyAddress = SafeToolbox.XmlDecode(reader.GetAttribute("privacyPolicyAddress"));
			_logoResource = SafeToolbox.XmlDecode(reader.GetAttribute("logoResource"));
			if (!base.ReadFromXml(reader))
			{
				return false;
			}
			if (reader.IsEmptyElement)
			{
				reader.Read();
				return true;
			}
			_servers.VerifyValues = false;
			reader.Read();
			while (!reader.EOF)
			{
				if (reader.IsStartElement())
				{
					switch (reader.Name)
					{
					case "Limit":
						if (base.ReadChildLimit(reader))
						{
							break;
						}
						return false;
					case "Required":
						_requiredFields.ReadFromXml(reader, null, "Required", "field", false);
						break;
					case "Custom":
					{
						string attribute2 = reader.GetAttribute("field");
						if (attribute2 != null)
						{
							string attribute3 = reader.GetAttribute("displayName");
							attribute = reader.GetAttribute("type");
							if (attribute != null)
							{
								string attribute4 = reader.GetAttribute("customType");
								_customFields.Add(new CustomRegistrationField(attribute2, attribute3, (CustomFieldType)SafeToolbox.FastParseInt32(attribute), attribute4));
								reader.Read();
								break;
							}
							return false;
						}
						return false;
					}
					case "Server":
						attribute = reader.GetAttribute("address");
						if (attribute != null)
						{
							_servers.Add(SafeToolbox.XmlDecode(attribute));
							reader.Read();
							break;
						}
						return false;
					default:
						reader.Skip();
						break;
					}
					continue;
				}
				reader.Read();
				break;
			}
			_servers.VerifyValues = true;
			return true;
		}

		[Obsolete("Use SecureLicense.ShowRegistrationForm")]
		public FormResult ShowRegistrationForm(SecureLicenseContext context)
		{
			FormResult formResult = ShowForm(context, "DEFAULT");
			FormResult formResult2 = formResult;
			if (formResult2 == FormResult.Success)
			{
				base.FormShown = true;
			}
			else if (!_isMandatory)
			{
				base.FormShown = true;
			}
			return formResult;
		}

		public virtual bool RegisterLater(SecureLicenseContext context)
		{
			if (_isMandatory)
			{
				return false;
			}
			if (_reminderPeriod > 0)
			{
				_nextReminder = context.CurrentDateAndTime.AddDays((double)_reminderPeriod);
				base.OnChanged("NextReminder");
			}
			return true;
		}

		public virtual bool DontRegister(SecureLicenseContext context)
		{
			if (_isMandatory)
			{
				return false;
			}
			_isRegistered = true;
			base.OnChanged("IsRegistered");
			return true;
		}

		public ValidationResult Register(SecureLicenseContext context, bool reportError, LicenseValuesDictionary registrationInfo)
		{
			return RegisterInternal(context, reportError, new object[1]
			{
				registrationInfo
			});
		}

		private ValidationResult RegisterInternal(SecureLicenseContext context, bool reportError, object[] args)
		{
			try
			{
				bool flag = false;
				SecureLicense secureLicense = base.License;
				Limit referenceLimit = this;
				LicenseValuesDictionary licenseValuesDictionary = args[0] as LicenseValuesDictionary;
				ValidationRecord validationRecord = new ValidationRecord("E_CouldNotRegister", this, null, ErrorSeverity.Normal);
				foreach (SecureLicense item in (IEnumerable)base.License.LicenseFile.Licenses)
				{
					if (item != base.License && item.CanUnlockBySerial)
					{
						flag = true;
						break;
					}
				}
				if (flag)
				{
					referenceLimit = null;
					string serialNumber = ((StringDictionary)licenseValuesDictionary)["serialNumber"];
					secureLicense = FindTargetLicense(context, serialNumber);
					if (secureLicense != null && secureLicense.Unlock(context, serialNumber))
					{
						goto IL_00ba;
					}
					return ValidationResult.Canceled;
				}
				goto IL_00ba;
				IL_00ba:
				foreach (DictionaryEntry item2 in licenseValuesDictionary)
				{
					((StringDictionary)secureLicense.RegistrationInfo)[item2.Key as string] = (item2.Value as string);
				}
				if (_servers.Count <= 0 || secureLicense.GetFlag("DisableLicenseServerCheck"))
				{
					if (((StringDictionary)licenseValuesDictionary)["_autoactivate"] == "true" && Toolbox.CheckInternetConnection(null))
					{
						ActivationLimit activationLimit = secureLicense.Limits.FindLimitByType(typeof(ActivationLimit), true) as ActivationLimit;
						if (activationLimit != null && activationLimit.Servers.Count > 0)
						{
							activationLimit.ActivateAtServer(context, true, null);
						}
					}
					if (secureLicense != base.License)
					{
						UpgradeLimit upgradeLimit = FindTargetUpgrade(context, secureLicense, ((StringDictionary)licenseValuesDictionary)["upgradeSerialNumber"]);
						if (upgradeLimit != null && !upgradeLimit.Unlock(context, ((StringDictionary)licenseValuesDictionary)["upgradeSerialNumber"]))
						{
							return ValidationResult.Canceled;
						}
					}
					if (!flag)
					{
						_isRegistered = true;
					}
					base.OnChanged("IsRegistered");
					if (_followUpUrl != null)
					{
						SharedToolbox.OpenUrl(Toolbox.ResolveUrl(_followUpUrl, SecureLicenseContext.CreateResolveContext(secureLicense, context)).ToString(), true);
					}
					return (secureLicense == base.License) ? ValidationResult.Valid : context.RetryWith(secureLicense.LicenseFile, secureLicense.LicenseId);
				}
				ServerResult serverResult = null;
				foreach (string item3 in (IEnumerable)_servers)
				{
					serverResult = context.CallServer("REGISTER", item3, referenceLimit, secureLicense, null, false);
					if (serverResult.Retry)
					{
						return ValidationResult.Retry;
					}
					if (!serverResult.Success)
					{
						validationRecord.SubRecords.Add("E_CouldNotRegisterWithServer", this, serverResult.Exception, (_canSkipServer && serverResult.ConnectionError) ? ErrorSeverity.Low : ErrorSeverity.High);
						continue;
					}
					if (((StringDictionary)licenseValuesDictionary)["_autoactivate"] == "true" && Toolbox.CheckInternetConnection(null))
					{
						ActivationLimit activationLimit2 = secureLicense.Limits.FindLimitByType(typeof(ActivationLimit), true) as ActivationLimit;
						if (activationLimit2 != null && activationLimit2.Servers.Count > 0)
						{
							activationLimit2.ActivateAtServer(context, true, null);
						}
					}
					if (secureLicense != base.License)
					{
						UpgradeLimit upgradeLimit2 = FindTargetUpgrade(context, secureLicense, ((StringDictionary)licenseValuesDictionary)["upgradeSerialNumber"]);
						if (upgradeLimit2 != null && !upgradeLimit2.Unlock(context, ((StringDictionary)licenseValuesDictionary)["upgradeSerialNumber"]))
						{
							return ValidationResult.Canceled;
						}
					}
					if (!flag)
					{
						_isRegistered = true;
					}
					base.OnChanged("IsRegistered");
					if (_followUpUrl == null)
					{
						return (secureLicense == base.License) ? ValidationResult.Valid : context.RetryWith(secureLicense.LicenseFile, secureLicense.LicenseId);
					}
					SharedToolbox.OpenUrl(Toolbox.ResolveUrl(_followUpUrl, SecureLicenseContext.CreateResolveContext(secureLicense, context)).ToString(), true);
					break;
				}
				if (reportError)
				{
					context.ReportError(validationRecord);
				}
				if (serverResult.ConnectionError && _canSkipServer)
				{
					if (_followUpUrl != null)
					{
						SharedToolbox.OpenUrl(Toolbox.ResolveUrl(_followUpUrl, context).ToString(), true);
					}
					return (secureLicense == base.License) ? ValidationResult.Valid : context.RetryWith(secureLicense.LicenseFile, secureLicense.LicenseId);
				}
				return ValidationResult.Invalid;
			}
			catch (ThreadAbortException)
			{
				_isMandatory = false;
				return ValidationResult.Canceled;
			}
		}

		public AsyncValidationRequest RegisterAsync(SecureLicenseContext context, bool reportError, LicenseValuesDictionary registrationInfo, EventHandler completeHandler)
		{
			return new AsyncValidationRequest(RegisterInternal, context, reportError, new object[1]
			{
				registrationInfo
			}, completeHandler);
		}

		public SecureLicense FindTargetLicense(SecureLicenseContext context, string serialNumber)
		{
			Check.NotNull(context, "context");
			Check.NotNull(serialNumber, "serialNumber");
			bool flag = false;
			foreach (SecureLicense item in (IEnumerable)base.License.LicenseFile.Licenses)
			{
				if (item.CanUnlockBySerial && item != base.License && item.CanLicenseComponent(context.LicensedType.FullName))
				{
					flag = true;
					if (!item.TrySerialNumberQuick(context, serialNumber))
					{
						continue;
					}
					return item;
				}
			}
			if (flag)
			{
				context.ReportError("E_InvalidSerialNumber", this, serialNumber);
				return null;
			}
			return base.License;
		}

		public UpgradeLimit FindTargetUpgrade(SecureLicenseContext context, SecureLicense license, string serialNumber)
		{
			Check.NotNull(context, "context");
			Check.NotNull(license, "license");
			if (serialNumber == null)
			{
				return null;
			}
			Limit[] array = license.Limits.FindLimitsByType(typeof(UpgradeLimit), true);
			Limit[] array2 = array;
			int num = 0;
			UpgradeLimit upgradeLimit;
			while (true)
			{
				if (num < array2.Length)
				{
					upgradeLimit = (UpgradeLimit)array2[num];
					int num2;
					byte[] array3;
					if (context.CheckSerialNumber(serialNumber, upgradeLimit.SerialNumberInfo, out num2, out array3))
					{
						break;
					}
					num++;
					continue;
				}
				if (license.CanUnlockBySerial && array.Length > 0)
				{
					context.ReportError("E_InvalidSerialNumber", license, serialNumber);
				}
				return null;
			}
			return upgradeLimit;
		}

		protected override SuperFormPanel CreatePanel(string pageId, SecureLicenseContext context)
		{
			if (((StringDictionary)base.CustomForms)[pageId] == null)
			{
				switch (pageId)
				{
				case "PRIVACYPOLICY":
					return new InfoPanel(Toolbox.ResolveUrl(PrivacyPolicyAddress, context).ToString(), null);
				case "UPGRADE":
					return new UpgradePanel(context.Items["TargetLicense"] as SecureLicense, this);
				case null:
				case "DEFAULT":
					return new RegistrationPanel(this);
				}
			}
			return base.CreatePanel(pageId, context);
		}
	}
}
