using System;
using System.ComponentModel;

namespace DeployLX.Licensing.v5
{
	public interface IExtendableLimit : IPurchaseLimit, IServerLimit
	{
		bool CanExtendByCode
		{
			get;
		}

		SecureLicense License
		{
			get;
		}

		bool CanExtend
		{
			get;
			set;
		}

		bool UseExtensionForm
		{
			get;
			set;
		}

		string CodeMask
		{
			get;
			set;
		}

		CodeAlgorithm CodeAlgorithm
		{
			get;
			set;
		}

		bool IsSubscription
		{
			get;
			set;
		}

		bool AutoExtend
		{
			get;
			set;
		}

		int ExtensionAmount
		{
			get;
			set;
		}

		int ExtendableValue
		{
			get;
			set;
		}

		int MaxExtendableValue
		{
			get;
		}

		int MinExtendableValue
		{
			get;
		}

		bool InitializeFromSerialNumber(SecureLicenseContext context, string serialNumber, int index);

		bool ExtendByCode(SecureLicenseContext context, string code);

		ValidationResult ExtendAtServer(SecureLicenseContext context, bool reportError);

		AsyncValidationRequest ExtendAtServerAsync(SecureLicenseContext context, bool reportError, EventHandler completedHandler);

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		void UpdateAtServer(LicenseValuesDictionary values, int extensionAmount);

		string GetUseDescription(SecureLicenseContext context);

		void GetUseRange(SecureLicenseContext context, out int min, out int max, out int current);

		int GetCurrentValue();
	}
}
