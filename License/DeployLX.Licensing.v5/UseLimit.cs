using System;
using System.Collections;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	[LimitEditor("UseLimitEditor", Category = "Extendable Limits", MiniType = "UseLimitEditorControl", IconResource = "asmres://DeployLX.Licensing.DlxAddIn.v5/DeployLX.Licensing.DlxAddIn.v5.Resources.Limits.Use.png")]
	public class UseLimit : ExtendableLimit
	{
		[NonSerialized]
		private UseMonitor _monitor;

		private UseLimitType _useLimitType = UseLimitType.Cumulative;

		private bool _autoUseOnValidate = true;

		private bool _autoUseOncePerSession;

		private bool _allowUseRemove;

		public override string Description
		{
			get
			{
				return SR.GetString("M_UseLimitDescription");
			}
		}

		public override string Name
		{
			get
			{
				return "Use";
			}
		}

		public UseLimitType UseLimitType
		{
			get
			{
				return _useLimitType;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_useLimitType != value)
				{
					_useLimitType = value;
					base.OnChanged("UseLimitType");
				}
			}
		}

		public int Uses
		{
			get
			{
				return ((IExtendableLimit)this).ExtendableValue;
			}
			set
			{
				base.AssertNotReadOnly();
				if (value < 0)
				{
					throw new ArgumentOutOfRangeException("Uses", value, SR.GetString("E_PropertyAtLeast", "Uses", 0));
				}
				if (((IExtendableLimit)this).ExtendableValue != value)
				{
					((IExtendableLimit)this).ExtendableValue = value;
					base.OnChanged("Uses");
				}
			}
		}

		public bool AutoUseOnValidate
		{
			get
			{
				return _autoUseOnValidate;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_autoUseOnValidate != value)
				{
					_autoUseOnValidate = value;
					base.OnChanged("AutoUseOnValidate");
				}
			}
		}

		public bool AutoUseOncePerSession
		{
			get
			{
				return _autoUseOncePerSession;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_autoUseOncePerSession != value)
				{
					_autoUseOncePerSession = value;
					base.OnChanged("AutoUseOncePerSession");
				}
			}
		}

		public bool AllowUseRemove
		{
			get
			{
				return _allowUseRemove;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_allowUseRemove != value)
				{
					_allowUseRemove = value;
					base.OnChanged("AllowUseRemove");
				}
			}
		}

		public override string SummaryText
		{
			get
			{
				return string.Format("{0}, {1}", Uses, _useLimitType);
			}
		}

		public UseMonitor GetUseMonitor()
		{
			if (_monitor == null && _monitor == null)
			{
				_monitor = (base.License.RuntimeState[base.LimitId + ".Monitor"] as UseMonitor);
				if (_monitor == null)
				{
					_monitor = new UseMonitor(this);
					base.License.RuntimeState[base.LimitId + ".Monitor"] = _monitor;
					SecureLicenseContext current = SecureLicenseContext.Current;
					if (current != null)
					{
						Peek(current);
					}
				}
				else
				{
					_monitor.Connect(this);
				}
			}
			return _monitor;
		}

		protected override bool ReadFromXml(XmlReader reader)
		{
			if (!base.ReadFromXml(reader))
			{
				return false;
			}
			string attribute = reader.GetAttribute("useLimitType");
			if (attribute == null)
			{
				return false;
			}
			_useLimitType = (UseLimitType)SafeToolbox.FastParseInt32(attribute);
			_autoUseOnValidate = (reader.GetAttribute("autoUseOnValidate") != "false");
			_autoUseOncePerSession = (reader.GetAttribute("autoUseOncePerSession") == "true");
			_allowUseRemove = (reader.GetAttribute("allowUseRemove") == "true");
			if (reader.IsEmptyElement)
			{
				reader.Read();
				return true;
			}
			base.Servers.VerifyValues = false;
			reader.Read();
			while (!reader.EOF)
			{
				if (reader.IsStartElement())
				{
					switch (reader.Name)
					{
					case "Limit":
						if (base.ReadChildLimit(reader))
						{
							break;
						}
						return false;
					case "Server":
						attribute = reader.GetAttribute("address");
						if (attribute != null)
						{
							base.Servers.Add(SafeToolbox.XmlDecode(attribute));
							reader.Read();
							break;
						}
						return false;
					default:
						reader.Skip();
						break;
					}
					continue;
				}
				reader.Read();
				break;
			}
			base.Servers.VerifyValues = true;
			return true;
		}

		protected override bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			if (!base.WriteToXml(writer, signing))
			{
				return false;
			}
			int useLimitType = (int)_useLimitType;
			writer.WriteAttributeString("useLimitType", useLimitType.ToString());
			if (!_autoUseOnValidate)
			{
				writer.WriteAttributeString("autoUseOnValidate", "false");
			}
			if (_autoUseOncePerSession)
			{
				writer.WriteAttributeString("autoUseOncePerSession", "true");
			}
			if (_allowUseRemove)
			{
				writer.WriteAttributeString("allowUseRemove", "true");
			}
			foreach (string item in (IEnumerable)base.Servers)
			{
				writer.WriteStartElement("Server");
				writer.WriteAttributeString("address", SafeToolbox.XmlEncode(item));
				writer.WriteEndElement();
			}
			return true;
		}

		public override ValidationResult Validate(SecureLicenseContext context)
		{
			base.ExtendWithRequestExtensionCodes(context);
			Check.NotNull(context, "context");
			GetUseMonitor();
			if (!IsCurrentUseValid())
			{
				ValidationResult validationResult = base.TryExtend(context);
				switch (validationResult)
				{
				case ValidationResult.Invalid:
					return context.ReportError("E_UseLimitReached", this);
				case ValidationResult.Canceled:
				case ValidationResult.Retry:
					return validationResult;
				}
			}
			return base.Validate(context);
		}

		public override ValidationResult Granted(SecureLicenseContext context)
		{
			Check.NotNull(context, "context");
			GetUseMonitor();
			if (!IsCurrentUseValid())
			{
				ValidationResult validationResult = base.TryExtend(context);
				switch (validationResult)
				{
				case ValidationResult.Invalid:
					return context.ReportError("E_UseLimitReached", this);
				case ValidationResult.Canceled:
				case ValidationResult.Retry:
					return validationResult;
				}
			}
			if (_autoUseOnValidate && (!_autoUseOncePerSession || base.License.RuntimeState[base.UniqueId + ".AutoUse"] == null) && !_monitor.AddUse())
			{
				ValidationResult validationResult2 = base.TryExtend(context);
				switch (validationResult2)
				{
				case ValidationResult.Invalid:
					return context.ReportError("E_UseLimitReached", this);
				case ValidationResult.Canceled:
				case ValidationResult.Retry:
					return validationResult2;
				}
				base.License.RuntimeState[base.UniqueId + ".AutoUse"] = this;
			}
			return base.Granted(context);
		}

		private bool IsCurrentUseValid()
		{
			if (!_monitor.CheckLimitReached(-1))
			{
				return true;
			}
			if (_autoUseOnValidate)
			{
				return false;
			}
			return Uses == 0;
		}

		public override PeekResult Peek(SecureLicenseContext context)
		{
			GetUseMonitor();
			if (!_monitor.CheckLimitReached(-1))
			{
				return PeekResult.Valid;
			}
			if (base.UseExtensionForm && base.CanExtend)
			{
				return PeekResult.NeedsUser;
			}
			return PeekResult.Invalid;
		}

		protected override int GetCurrentValue()
		{
			return GetUseMonitor().UseCount;
		}

		protected override bool CheckLimitReached(int maxAmount)
		{
			if (GetUseMonitor().CheckLimitReached(maxAmount))
			{
				return true;
			}
			return false;
		}

		internal int GetStoredUses()
		{
			if (!Check.CalledByType(typeof(UseMonitor)) && !Check.CalledByThisType())
			{
				return ((IExtendableLimit)this).MaxExtendableValue;
			}
			if (_useLimitType == UseLimitType.Cumulative)
			{
				object persistentData = base.GetPersistentData("UseCount");
				if (persistentData != null)
				{
					return (int)persistentData;
				}
			}
			return 0;
		}

		internal void SetStoredUses(int uses)
		{
			if (!Check.CalledByType(typeof(UseMonitor)))
			{
				uses = Math.Max(uses, ((IExtendableLimit)this).MaxExtendableValue);
			}
			if (_useLimitType == UseLimitType.Cumulative)
			{
				base.SetPersistentData("UseCount", uses);
			}
		}

		public override string GetUseDescription(SecureLicenseContext context)
		{
			GetCurrentValue();
			int minExtendableValue = ((IExtendableLimit)this).MinExtendableValue;
			int extendableValue = ((IExtendableLimit)this).ExtendableValue;
			return SR.GetString("M_UsesRemaining", Math.Max(0, extendableValue - GetUseMonitor().UseCount), extendableValue - minExtendableValue);
		}

		public override void Reset(SecureLicenseContext context, int value)
		{
			if (!Check.CalledByType(typeof(ResetLimit)) && !Check.CalledByThisType())
			{
				return;
			}
			base.Reset(context, value);
			base.SetPersistentData("UseCount", null);
			GetUseMonitor().Connect(this);
		}
	}
}
