using System;
using System.Collections;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	[LimitEditor("DomainNameLimitEditor", Category = "Web Limits", IconResource = "asmres://DeployLX.Licensing.DlxAddIn.v5/DeployLX.Licensing.DlxAddIn.v5.Resources.Limits.Domain.png")]
	public class DomainNameLimit : Limit
	{
		private StringCollection _domains = new StringCollection();

		public StringCollection Domains
		{
			get
			{
				return _domains;
			}
		}

		public override string Description
		{
			get
			{
				return SR.GetString("M_DomainLimitDescription");
			}
		}

		public override string Name
		{
			get
			{
				return "Domain Name";
			}
		}

		public DomainNameLimit()
		{
			_domains.Changed += _domains_Changed;
		}

		private void _domains_Changed(object sender, CollectionEventArgs e)
		{
			base.OnCollectionChanged(sender, "Domains", e);
		}

		protected override bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			_domains.WriteToXml(writer, null, "Domain", "name");
			return true;
		}

		protected override bool ReadFromXml(XmlReader reader)
		{
			_domains.Clear();
			if (reader.IsEmptyElement)
			{
				reader.Read();
				return true;
			}
			reader.Read();
			while (!reader.EOF)
			{
				if (reader.IsStartElement())
				{
					switch (reader.Name)
					{
					case "Limit":
						if (base.ReadChildLimit(reader))
						{
							break;
						}
						return false;
					case "Domain":
						_domains.ReadFromXml(reader, null, "Domain", "name", false);
						break;
					default:
						reader.Skip();
						break;
					}
					continue;
				}
				reader.Read();
				break;
			}
			return true;
		}

		public override ValidationResult Validate(SecureLicenseContext context)
		{
			if (IsDomainValid(context))
			{
				return base.Validate(context);
			}
			context.ReportError("E_InvalidDomain", this, HttpContext.Current.Request.Url.Host);
			return ValidationResult.Invalid;
		}

		public override ValidationResult Granted(SecureLicenseContext context)
		{
			if (IsDomainValid(context))
			{
				return base.Granted(context);
			}
			context.ReportError("E_InvalidDomain", this, HttpContext.Current.Request.Url.Host);
			return ValidationResult.Invalid;
		}

		public bool IsDomainValid(SecureLicenseContext context)
		{
			if (!SafeToolbox.IsWebRequest)
			{
				return true;
			}
			string host = HttpContext.Current.Request.Url.Host;
			foreach (string item in (IEnumerable)Domains)
			{
				if (CompareDomain(host, item))
				{
					return true;
				}
			}
			return false;
		}

		protected static bool CompareDomain(string name, string match)
		{
			if (string.Compare(name, match, true, CultureInfo.InvariantCulture) == 0)
			{
				return true;
			}
			string text = null;
			text = ((match[0] != '#') ? ('^' + match.Replace("*", "([-_\\d\\w]*?)").Replace("?", "([-_\\d\\w]??)") + '$') : match.Substring(1));
			return Regex.IsMatch(name, text, RegexOptions.IgnoreCase);
		}

		protected internal override void MakeReadOnly()
		{
			base.MakeReadOnly();
			_domains.MakeReadOnly();
		}
	}
}
