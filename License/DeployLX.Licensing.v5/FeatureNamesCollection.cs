using System;
using System.ComponentModel;
using System.Text;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public class FeatureNamesCollection : StringCollection
	{
		private readonly string[] _defaultNames;

		private int _maxFlags;

		private bool _skipAssert;

		public string this[FeatureFlags flag]
		{
			get
			{
				return base[MapFlagIndex(flag)];
			}
			set
			{
				_skipAssert = true;
				try
				{
					int num = MapFlagIndex(flag);
					base[num] = (value ?? _defaultNames[num]);
				}
				finally
				{
					_skipAssert = false;
				}
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public new string this[int index]
		{
			get
			{
				return base[index];
			}
			set
			{
				_skipAssert = true;
				try
				{
					base[index] = value;
				}
				finally
				{
					_skipAssert = false;
				}
			}
		}

		private int MapFlagIndex(FeatureFlags flag)
		{
			int num = -1;
			for (int num2 = (int)flag; num2 != 0; num2 >>= 1)
			{
				num++;
			}
			if (num > _maxFlags)
			{
				throw new ArgumentOutOfRangeException("flag");
			}
			return num;
		}

		public FeatureNamesCollection()
			: this(-1, null)
		{
		}

		public FeatureNamesCollection(int maxFlags, string featurePrefix)
		{
			if (maxFlags == -1)
			{
				maxFlags = 31;
			}
			if (maxFlags > 31)
			{
				throw new ArgumentOutOfRangeException("maxFlags");
			}
			_maxFlags = maxFlags;
			if (featurePrefix == null)
			{
				featurePrefix = "Feature";
			}
			_defaultNames = new string[maxFlags];
			for (int i = 0; i < maxFlags; i++)
			{
				_defaultNames[i] = featurePrefix + i;
			}
			base.AddRange(_defaultNames);
		}

		protected override void OnChanging(CollectionEventArgs e)
		{
			if (!_skipAssert && e.Action != CollectionChangeAction.Refresh)
			{
				throw new SecureLicenseException("E_DontAddFeatureNames", e.Element);
			}
			base.OnChanging(e);
		}

		public int GetLastNonDefaultIndex()
		{
			int num = _maxFlags - 1;
			while (true)
			{
				if (num >= 0)
				{
					if (this[num] != _defaultNames[num])
					{
						break;
					}
					num--;
					continue;
				}
				return -1;
			}
			return num;
		}

		public void Reset()
		{
			_skipAssert = true;
			try
			{
				for (int i = 0; i < _defaultNames.Length; i++)
				{
					this[i] = _defaultNames[i];
				}
			}
			finally
			{
				_skipAssert = false;
			}
		}

		public override void WriteToXml(XmlWriter writer, string collectionName, string itemName, string attributeName, int index, int count)
		{
			if (writer == null)
			{
				throw new ArgumentNullException("writer");
			}
			if (collectionName == null)
			{
				collectionName = "Features";
			}
			if (itemName == null)
			{
				itemName = "Feature";
			}
			if (attributeName == null)
			{
				attributeName = "name";
			}
			if (count == -1)
			{
				count = GetLastNonDefaultIndex() + 1;
			}
			if (count != -1)
			{
				writer.WriteStartElement(collectionName);
				int num = index;
				while (num < count)
				{
					string text = base[num];
					if (text.IndexOf(';') <= -1)
					{
						writer.WriteStartElement(itemName);
						writer.WriteAttributeString(attributeName, base[num]);
						writer.WriteEndElement();
						num++;
						continue;
					}
					throw new SecureLicenseException("E_FeatureNameCannotHaveSemiColons");
				}
				writer.WriteEndElement();
			}
		}

		public override bool ReadFromXml(XmlReader reader, string collectionName, string itemName, string attributeName, bool clear)
		{
			if (reader == null)
			{
				throw new ArgumentNullException("reader");
			}
			if (collectionName == null)
			{
				collectionName = "Features";
			}
			if (itemName == null)
			{
				itemName = "Feature";
			}
			if (attributeName == null)
			{
				attributeName = "name";
			}
			if (clear)
			{
				Reset();
			}
			int num = 0;
			reader.MoveToContent();
			if (reader.IsEmptyElement)
			{
				Reset();
				reader.Read();
				return true;
			}
			_skipAssert = true;
			try
			{
				reader.Read();
				while (!reader.EOF)
				{
					reader.MoveToContent();
					if (reader.IsStartElement())
					{
						if (reader.Name == itemName)
						{
							string attribute = reader.GetAttribute(attributeName);
							if (attribute != null)
							{
								base[num++] = attribute;
								reader.Skip();
								continue;
							}
							return false;
						}
						reader.Skip();
						continue;
					}
					reader.Read();
					break;
				}
			}
			finally
			{
				_skipAssert = false;
			}
			return true;
		}

		public string MakeFeatureList(FeatureFlags flags)
		{
			return MakeFeatureList(flags, null);
		}

		public string MakeFeatureList(FeatureFlags flags, string separator)
		{
			return MakeFeatureList(flags, separator, null);
		}

		public string MakeFeatureList(FeatureFlags flags, string separator, string suffix)
		{
			if (separator == null)
			{
				separator = "; ";
			}
			StringBuilder stringBuilder = new StringBuilder();
			int num = (int)flags;
			int num2 = 0;
			while (num2 < _maxFlags)
			{
				if ((num & 1) == 1)
				{
					if (stringBuilder.Length > 0)
					{
						stringBuilder.Append(separator);
					}
					stringBuilder.Append(base[num2]);
					stringBuilder.Append(suffix);
				}
				num2++;
				num >>= 1;
			}
			return stringBuilder.ToString();
		}

		public FeatureFlags ParseFeatureList(string list)
		{
			return ParseFeatureList(list, null);
		}

		public FeatureFlags ParseFeatureList(string list, string separator)
		{
			FeatureFlags featureFlags = FeatureFlags.None;
			if (list != null && list.Length != 0)
			{
				if (separator == null)
				{
					separator = "; ";
				}
				string[] array = list.Split(new string[1]
				{
					separator
				}, StringSplitOptions.RemoveEmptyEntries);
				string[] array2 = array;
				foreach (string text in array2)
				{
					int num = base.IndexOf(text.Trim());
					if (num != -1)
					{
						featureFlags = (FeatureFlags)((int)featureFlags | 1 << num);
					}
				}
				return featureFlags;
			}
			return featureFlags;
		}
	}
}
