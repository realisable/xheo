using System;
using System.Collections;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public sealed class OSRecordCollection : WithEventsCollection
	{
		public OSRecord this[int index]
		{
			get
			{
				return base.List[index] as OSRecord;
			}
			set
			{
				base.List[index] = value;
			}
		}

		public int Add(OSRecord record)
		{
			return base.List.Add(record);
		}

		public void AddRange(OSRecord[] records)
		{
			if (records != null)
			{
				foreach (OSRecord record in records)
				{
					Add(record);
				}
			}
		}

		public void AddRange(OSRecordCollection records)
		{
			if (records != null)
			{
				foreach (OSRecord item in (IEnumerable)records)
				{
					Add(item);
				}
			}
		}

		public void Insert(int index, OSRecord record)
		{
			base.List.Insert(index, record);
		}

		public void Remove(OSRecord record)
		{
			base.List.Remove(record);
		}

		public void CopyTo(OSRecord[] array, int index)
		{
			base.List.CopyTo(array, index);
		}

		public int IndexOf(OSRecord record)
		{
			return base.List.IndexOf(record);
		}

		public bool Contains(OSRecord record)
		{
			return base.List.Contains(record);
		}
	}
}
