namespace DeployLX.Licensing.v5
{
	public enum LocationState
	{
		Unknown,
		Altered,
		Unaltered
	}
}
