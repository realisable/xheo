using System;
using System.Security.Principal;

namespace DeployLX.Licensing.v5
{
	internal sealed class ImpHandle : IDisposable
	{
		public WindowsImpersonationContext Imp;

		public bool CalledElevation;

		public bool IsValid
		{
			get
			{
				if (Imp == null)
				{
					return CalledElevation;
				}
				return true;
			}
		}

		public void Dispose()
		{
			if (Imp != null)
			{
				Imp.Undo();
			}
			GC.SuppressFinalize(this);
		}
	}
}
