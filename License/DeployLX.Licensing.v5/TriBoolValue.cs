using System;

namespace DeployLX.Licensing.v5
{
	[CLSCompliant(true)]
	public enum TriBoolValue
	{
		Default = 2,
		Yes = 1,
		No = 0
	}
}
