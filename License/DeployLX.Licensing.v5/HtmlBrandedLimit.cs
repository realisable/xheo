using System;
using System.Web;
using System.Web.UI;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	[LimitEditor("HtmlLimitEditor", Category = "Web Limits", IconResource = "asmres://DeployLX.Licensing.DlxAddIn.v5/DeployLX.Licensing.DlxAddIn.v5.Resources.Limits.Html.png")]
	public class HtmlBrandedLimit : Limit
	{
		public static readonly string[] TemplateVariables = new string[19]
		{
			"LimitId",
			"LicenseId",
			"SerialNumber",
			"Url",
			"MachineCode",
			"SupportPhone",
			"SupportEmail",
			"SupportWebsite",
			"WebPanelColor",
			"WebTextColor",
			"WebLinkTextColor",
			"WebSideColor",
			"WebSideTextColor",
			"WebSideLinkTextColor",
			"WebSideSplitterFadeColor",
			"WebSupportTitleTextColor",
			"WebHeaderColor",
			"WebHeaderTextColor",
			"InstanceId"
		};

		private string _html;

		private HtmlLocation _htmlLocation = HtmlLocation.Inline;

		public string Html
		{
			get
			{
				return _html;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_html != value)
				{
					_html = value;
					base.OnChanged("Html");
				}
			}
		}

		public override string Description
		{
			get
			{
				return SR.GetString("M_HtmlBrandedLimitDescription");
			}
		}

		public override string Name
		{
			get
			{
				return "Html Branded";
			}
		}

		public HtmlLocation HtmlLocation
		{
			get
			{
				return _htmlLocation;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_htmlLocation != value)
				{
					_htmlLocation = value;
					base.OnChanged("HtmlLocation");
				}
			}
		}

		public override ValidationResult Granted(SecureLicenseContext context)
		{
			if (SafeToolbox.IsWebRequest)
			{
				Control control = context.FindWebControl();
				if (control == null)
				{
					if (!context.IsWebServiceRequest)
					{
						return context.ReportError("E_CouldNotGetPage", this, null, ErrorSeverity.Low);
					}
				}
				else if (!AttachAspNetForm(context))
				{
					return ValidationResult.Invalid;
				}
			}
			return base.Validate(context);
		}

		protected bool AttachAspNetForm(SecureLicenseContext context)
		{
			Control control = context.FindWebControl();
			string html = _html;
			HttpContext httpContext = context.GetHttpContext();
			string text = (httpContext == null) ? null : httpContext.Request.Url.ToString();
			int num = text.IndexOf("__activate");
			if (num > -1)
			{
				int num2 = text.IndexOf('&');
				if (text[num - 1] == '&' || text[num - 1] == '?')
				{
					num--;
				}
				text = ((num2 != -1) ? text.Remove(num, num2 - num) : text.Substring(0, num));
			}
			text = ((text.IndexOf('?') != -1) ? (text + "&__activate=" + base.LimitId) : (text + "?__activate=" + base.LimitId));
			string[] values = MakeTemplateVariables(context, text, false);
			html = TemplateFormatter.Format(html, TemplateVariables, values);
			return context.RenderHtml(html, base.LimitId, control, _htmlLocation, false);
		}

		public string[] MakeTemplateVariables(SecureLicenseContext context, string url, bool sample)
		{
			string text = sample ? "http://www.company.com" : context.SupportInfo.Website;
			if (text != null)
			{
				int num = text.IndexOf("://");
				if (num == -1)
				{
					text = "http://" + text;
				}
				text = Toolbox.ResolveUrl(text, context).ToString();
			}
			SuperFormTheme superFormTheme = base.License.LicenseFile.SuperFormTheme;
			if (superFormTheme == null)
			{
				superFormTheme = new SuperFormTheme(SuperFormTheme.DefaultBaseColor);
			}
			return new string[19]
			{
				base.LimitId,
				base.License.LicenseId,
				base.License.SerialNumber,
				url,
				MachineProfile.Profile.Hash,
				sample ? "(123) 456-7890" : context.SupportInfo.Phone,
				sample ? "support@company.com" : context.SupportInfo.Email,
				text,
				ImageEffects.ToRGBHex(superFormTheme.Colors["buttonface", new string[5]
				{
					"WebPanel",
					"WebBase",
					"Panel",
					"Back",
					"Base"
				}]),
				ImageEffects.ToRGBHex(superFormTheme.Colors["button", new string[2]
				{
					"WebText",
					"Text"
				}]),
				ImageEffects.ToRGBHex(superFormTheme.Colors["blue", new string[4]
				{
					"WebLinkText",
					"WebText",
					"LinkText",
					"Text"
				}]),
				ImageEffects.ToRGBHex(superFormTheme.Colors["buttonface", new string[6]
				{
					"WebSide",
					"WebPanel",
					"WebBase",
					"Panel",
					"Back",
					"Base"
				}]),
				ImageEffects.ToRGBHex(superFormTheme.Colors["buttonface", new string[3]
				{
					"WebSideText",
					"WebText",
					"Text"
				}]),
				ImageEffects.ToRGBHex(superFormTheme.Colors["buttonface", new string[4]
				{
					"WebSideLinkText",
					"WebLinkText",
					"LinkText",
					"Text"
				}]),
				ImageEffects.ToRGBHex(superFormTheme.Colors["white", new string[3]
				{
					"WebSideSplitterFade",
					"SideSplitterFade",
					"BaseHighlight"
				}]),
				ImageEffects.ToRGBHex(superFormTheme.Colors["button", new string[4]
				{
					"WebSupportTitleText",
					"SupportTitleText",
					"HeaderText",
					"Text"
				}]),
				ImageEffects.ToRGBHex(superFormTheme.Colors["button", new string[6]
				{
					"WebHeaderColor",
					"WebPanel",
					"WebBase",
					"Panel",
					"Back",
					"Base"
				}]),
				ImageEffects.ToRGBHex(superFormTheme.Colors["button", new string[3]
				{
					"WebHeaderTextColor",
					"WebText",
					"Text"
				}]),
				TemplateFormatter.CreateInstanceId(context)
			};
		}

		protected override bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			if (_htmlLocation != HtmlLocation.Inline)
			{
				int htmlLocation = (int)_htmlLocation;
				writer.WriteAttributeString("location", htmlLocation.ToString());
			}
			if (_html != null)
			{
				writer.WriteStartElement("Html");
				writer.WriteCData(_html);
				writer.WriteEndElement();
			}
			return true;
		}

		protected override bool ReadFromXml(XmlReader reader)
		{
			_html = null;
			string attribute = reader.GetAttribute("location");
			if (attribute != null)
			{
				_htmlLocation = (HtmlLocation)SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_htmlLocation = HtmlLocation.Inline;
			}
			if (reader.IsEmptyElement)
			{
				reader.Read();
				return true;
			}
			reader.Read();
			while (!reader.EOF)
			{
				if (reader.IsStartElement())
				{
					switch (reader.Name)
					{
					case "Limit":
						if (base.ReadChildLimit(reader))
						{
							break;
						}
						return false;
					case "Html":
						reader.Read();
						_html = reader.ReadString();
						reader.Read();
						break;
					default:
						reader.Skip();
						break;
					}
					continue;
				}
				reader.Read();
				break;
			}
			return true;
		}
	}
}
