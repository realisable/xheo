using System;

namespace DeployLX.Licensing.v5
{
	[Flags]
	public enum SaveLocations
	{
		NotSet = 0x0,
		OriginalLocation = 0x1,
		SharedFolder = 0x2
	}
}
