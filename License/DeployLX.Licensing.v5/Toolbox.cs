using Microsoft.Win32;
using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Cryptography;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using System.Web;
using System.Windows.Forms;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[EditorBrowsable(EditorBrowsableState.Never)]
	public static class Toolbox
	{
		private delegate Bitmap GetControlAsBitmapDelegate(Control ctrl);

		private sealed class BoostHandle : IDisposable
		{
			private ThreadPriority _originalPriority;

			public BoostHandle(ThreadPriority priority)
			{
				_originalPriority = Thread.CurrentThread.Priority;
				Thread.CurrentThread.Priority = priority;
			}

			public void Dispose()
			{
				Thread.CurrentThread.Priority = _originalPriority;
			}
		}

		private static Type _aesType;

		//[ThreadStatic]
		//private static int _threadIsWeb;


		private static int _iisIsAvailable;

		private static int _isTerminalServices;

		private static int _isFipsEnabled;

		private static readonly bool _isMediumTrust;

		private static MethodInfo _setStyleMethod;

		private static MethodInfo _getStyleMethod;

		private static readonly ControlStyles _bufferStyles;

		private static int _canGetControlAsBitmap;


		public static bool IsConsoleRequest
		{
			get
			{
				try
				{
					int cursorSize = Console.CursorSize;
					return cursorSize > 0;
				}
				catch (IOException)
				{
					return false;
				}
			}
		}

		public static bool IisIsAvailable
		{
			get
			{
				return _iisIsAvailable == 1;
			}
		}

		public static bool IsTerminalServices
		{
			get
			{
				if (_isTerminalServices == -1)
				{
					lock (SecureLicenseManager.SyncRoot)
					{
						if (_isTerminalServices == -1)
						{
							if ((Environment.OSVersion.Platform & PlatformID.WinCE) != 0)
							{
								_isTerminalServices = ((SafeNativeMethods.GetSystemMetrics(4096) != 0) ? 1 : 0);
							}
							else
							{
								_isTerminalServices = 0;
							}
						}
					}
				}
				return _isTerminalServices == 1;
			}
		}

		

		public static bool IsMediumTrust
		{
			get
			{
				return _isMediumTrust;
			}
		}

		static Toolbox()
		{
			
			_iisIsAvailable = -1;
			_isTerminalServices = -1;
			_isMediumTrust = CheckIsMediumTrust();
			_bufferStyles = ((Environment.Version.Major >= 2) ? (ControlStyles.DoubleBuffer | ControlStyles.OptimizedDoubleBuffer) : ControlStyles.DoubleBuffer);
			try
			{
				InitLib();
			}
			catch
			{
				_iisIsAvailable = 0;
			}
			try
			{
				Assembly assembly = Assembly.Load("System.Core, Version=3.5.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089");
				_aesType = assembly.GetType("System.Security.Cryptography.AesCryptoServiceProvider");
			}
			catch (FileNotFoundException)
			{
			}
			catch (BadImageFormatException)
			{
			}
			if (_aesType == null)
			{
				_aesType = (SafeToolbox.IsFipsEnabled ? typeof(FipsRijndael) : typeof(RijndaelManaged));
			}
		}

		internal static void InitLib()
		{
			_iisIsAvailable = 0;
			try
			{
				try
				{
					if (HttpContext.Current != null)
					{
						_iisIsAvailable = 1;
						return;
					}
				}
				catch
				{
				}
				string text = RuntimeEnvironment.GetRuntimeDirectory() + "aspnet_isapi.dll";
				if (File.Exists(text))
				{
					IntPtr intPtr = SafeNativeMethods.LoadLibraryExW(text, IntPtr.Zero, 2u);
					if (intPtr == IntPtr.Zero)
					{
						intPtr = SafeNativeMethods.LoadLibraryExW(text, IntPtr.Zero, 2u);
					}
					if (intPtr != IntPtr.Zero && SafeNativeMethods.FreeLibrary(intPtr) != 0)
					{
						_iisIsAvailable = 1;
					}
				}
			}
			catch
			{
			}
		}

		private static bool CheckIsMediumTrust()
		{
			try
			{
				SecurityPermission securityPermission = new SecurityPermission(SecurityPermissionFlag.SkipVerification);
				securityPermission.Demand();
				return false;
			}
			catch (SecurityException)
			{
				return true;
			}
		}

		public static bool CheckInternetConnection(Uri suggestedUrl)
		{
			if (suggestedUrl == (Uri)null)
			{
				suggestedUrl = new Uri("http://www.google.com");
			}
			try
			{
				TcpClient tcpClient = new TcpClient();
				tcpClient.Connect(suggestedUrl.Host, suggestedUrl.Port);
				tcpClient.Close();
				return true;
			}
			catch
			{
				return false;
			}
		}

		public static WebProxy MakeWebProxy(string address, string username, string password, string domain)
		{
			if (address != null && address.Length != 0)
			{
				CredentialCache credentialCache = new CredentialCache();
				NetworkCredential cred = new NetworkCredential(username, password, domain);
				if (string.Compare(address, 0, "http://", 0, 7, true) != 0 && string.Compare(address, 0, "https://", 0, 7, true) != 0 && address.IndexOf("://") == -1)
				{
					address = "http://" + address;
				}
				Uri uriPrefix = new Uri(address);
				credentialCache.Add(uriPrefix, "Basic", cred);
				credentialCache.Add(uriPrefix, "NTLM", cred);
				credentialCache.Add(uriPrefix, "Digest", cred);
				credentialCache.Add(uriPrefix, "Kerberos", cred);
				return new WebProxy(address, true, new string[0], credentialCache);
			}
			throw new ArgumentNullException("address");
		}

		public static void SplitUserName(string winUsername, out string username, out string domain)
		{
			Check.NotNull(winUsername, "winUsername");
			username = null;
			domain = null;
			int num = winUsername.IndexOf('\\');
			if (num > -1)
			{
				domain = winUsername.Substring(0, num);
				username = winUsername.Substring(num + 1);
			}
			else
			{
				num = winUsername.IndexOf('@');
				if (num > -1)
				{
					username = winUsername.Substring(0, num);
					domain = winUsername.Substring(num + 1);
				}
				else
				{
					username = winUsername;
				}
			}
		}

		public static CultureInfo SelectCulture(CultureInfo culture)
		{
			Check.NotNull(culture, "culture");
			if (Thread.CurrentThread.CurrentUICulture == culture)
			{
				return culture;
			}
			CultureInfo currentUICulture = Thread.CurrentThread.CurrentUICulture;
			if (culture.IsNeutralCulture)
			{
				CultureInfo[] cultures = CultureInfo.GetCultures(CultureTypes.SpecificCultures);
				int num = 0;
				while (num < cultures.Length)
				{
					CultureInfo cultureInfo = cultures[num];
					if (!cultureInfo.Name.StartsWith(culture.Name))
					{
						num++;
						continue;
					}
					culture = cultureInfo;
					break;
				}
				if (culture.IsNeutralCulture)
				{
					return currentUICulture;
				}
			}
			Thread.CurrentThread.CurrentUICulture = culture;
			return currentUICulture;
		}

		public static CultureInfo SelectWebCulture()
		{
			CultureInfo currentUICulture = Thread.CurrentThread.CurrentUICulture;
			if (!SafeToolbox.IsWebRequest)
			{
				return currentUICulture;
			}
			try
			{
				HttpContext current = HttpContext.Current;
				string[] userLanguages = current.Request.UserLanguages;
				if (userLanguages != null && userLanguages.Length > 0)
				{
					string[] array = userLanguages;
					int num = 0;
					while (true)
					{
						if (num >= array.Length)
						{
							break;
						}
						string text = array[num];
						try
						{
							int num2 = text.IndexOf(';');
							CultureInfo culture = new CultureInfo((num2 == -1) ? text : text.Substring(0, num2));
							culture = SelectCulture(culture);
							if (CultureInfo.CurrentUICulture != currentUICulture)
							{
								return culture;
							}
						}
						catch (ArgumentException)
						{
						}
						num++;
					}
				}
				return currentUICulture;
			}
			catch
			{
				return currentUICulture;
			}
		}

		internal static Stream GetEmbeddedResource(Type type, string name, bool ignoreCase, out string foundResourceName)
		{
			string text = (type == null) ? "" : type.Namespace;
			foundResourceName = null;
			if (text == null)
			{
				text = "";
			}
			while (true)
			{
				string text2 = (text.Length > 0) ? (text + '.' + name) : name;
				Stream manifestResourceStream = type.Assembly.GetManifestResourceStream(text2);
				if (manifestResourceStream == null)
				{
					if (text.Length == 0)
					{
						break;
					}
					int num = text.IndexOf('.');
					text = ((num <= -1) ? "" : text.Substring(0, num));
					continue;
				}
				foundResourceName = text2;
				return manifestResourceStream;
			}
			Stream stream = null;
			Stream stream2 = null;
			string text3 = Path.GetExtension(name);
			if (ignoreCase)
			{
				text3 = text3.ToLower(CultureInfo.InvariantCulture);
			}
			try
			{
				string value = ignoreCase ? name.ToLower(CultureInfo.InvariantCulture) : name;
				string[] manifestResourceNames = type.Assembly.GetManifestResourceNames();
				int num2 = 0;
				while (num2 < manifestResourceNames.Length)
				{
					string text4 = manifestResourceNames[num2];
					string text5 = ignoreCase ? text4.ToLower(CultureInfo.InvariantCulture) : text4;
					if (!text5.EndsWith(value))
					{
						if (text5.EndsWith(text3))
						{
							stream = type.Assembly.GetManifestResourceStream(text4);
							stream2 = stream;
							foundResourceName = text4;
						}
						num2++;
						continue;
					}
					stream2 = type.Assembly.GetManifestResourceStream(text4);
					foundResourceName = text4;
					return stream2;
				}
				return stream;
			}
			finally
			{
				if (stream != null && stream != stream2)
				{
					stream.Close();
				}
			}
		}

		internal static Stream GetEmbeddedResource(Type type, string name, out string foundResourceName)
		{
			return GetEmbeddedResource(type, name, false, out foundResourceName);
		}

		public static bool IsUriResource(string resource)
		{
			if (resource != null && resource.Length != 0)
			{
				if (string.Compare(resource, 0, Uri.UriSchemeHttp, 0, Uri.UriSchemeHttp.Length, true, CultureInfo.InvariantCulture) != 0 && string.Compare(resource, 0, Uri.UriSchemeHttps, 0, Uri.UriSchemeHttps.Length, true, CultureInfo.InvariantCulture) != 0 && string.Compare(resource, 0, Uri.UriSchemeFtp, 0, Uri.UriSchemeFtp.Length, true, CultureInfo.InvariantCulture) != 0 && string.Compare(resource, 0, Uri.UriSchemeFile, 0, Uri.UriSchemeFtp.Length, true, CultureInfo.InvariantCulture) != 0 && !IsResourceAddress(resource))
				{
					return false;
				}
				return true;
			}
			return false;
		}

		public static bool IsResourceAddress(string address)
		{
			if (address != null && address.Length != 0)
			{
				if (string.Compare(address, 0, "asmres://", 0, 9, true, CultureInfo.InvariantCulture) != 0 && string.Compare(address, 0, "licres://", 0, 9, true, CultureInfo.InvariantCulture) != 0 && string.Compare(address, 0, "html://", 0, 7, true, CultureInfo.InvariantCulture) != 0)
				{
					return false;
				}
				return true;
			}
			return false;
		}

		public static Image GetImage(string address, SecureLicenseContext context)
		{
			Stream stream = null;
			try
			{
				stream = GetResource(ref address, context);
				if (stream == null)
				{
					return null;
				}
				return GetImage(stream);
			}
			catch (Exception)
			{
				return null;
			}
			finally
			{
				if (stream != null)
				{
					stream.Close();
				}
			}
		}

		public static Image GetImage(Stream stream)
		{
			try
			{
				Image image = null;
				if (stream == null)
				{
					return null;
				}
				image = Image.FromStream(stream);
				if (image != null)
				{
					try
					{
						return new Bitmap(image, image.Size);
					}
					finally
					{
						image.Dispose();
					}
				}
				return null;
			}
			catch (Exception)
			{
				return null;
			}
		}

		public static Stream GetResource(ref string address, SecureLicenseContext context)
		{
			if (address != null && address.Length != 0)
			{
				Uri uri = null;
				try
				{
					if (context == null)
					{
						context = SecureLicenseContext.ResolveContext;
					}
					if (context != null && context.RequestInfo.SimulateOffline)
					{
						return null;
					}
					foreach (string item in (IEnumerable)UriCollection.FromList(address))
					{
						uri = null;
						try
						{
							uri = ResolveUrl(item, context);
							Uri uri2 = uri;
							string extension = Path.GetExtension(uri.AbsolutePath);
							UriBuilder uriBuilder = new UriBuilder(uri);
							CultureInfo cultureInfo = Thread.CurrentThread.CurrentUICulture;
							while (true)
							{
								if (cultureInfo == null)
								{
									break;
								}
								try
								{
									if (cultureInfo == CultureInfo.InvariantCulture)
									{
										return TryCulture(context, uri2);
									}
									uriBuilder.Path = Path.ChangeExtension(uri.AbsolutePath, cultureInfo.Name + extension);
									Stream stream = TryCulture(context, uriBuilder.Uri);
									if (stream != null)
									{
										return stream;
									}
								}
								catch (WebException)
								{
									if (cultureInfo != CultureInfo.InvariantCulture)
									{
										goto end_IL_00d9;
									}
									throw;
									end_IL_00d9:;
								}
								cultureInfo = cultureInfo.Parent;
							}
						}
						catch (Exception)
						{
						}
					}
				}
				finally
				{
					if (uri != (Uri)null)
					{
						address = uri.ToString();
					}
				}
				return null;
			}
			return null;
		}

		private static Stream TryCulture(SecureLicenseContext context, Uri uri)
		{
			if (string.Compare(uri.Scheme, "asmres", true) == 0)
			{
				AsmResProtocol asmResProtocol = new AsmResProtocol();
				return asmResProtocol.GetResourceStream(uri);
			}
			if (string.Compare(uri.Scheme, "licres", true) == 0)
			{
				if (context == null)
				{
					return null;
				}
				LicResProtocol licResProtocol = new LicResProtocol();
				return licResProtocol.GetResourceStream(uri, context);
			}
			WebRequest webRequest = WebRequest.Create(uri);
			webRequest.Timeout = 5000;
			WebResponse response = webRequest.GetResponse();
			return response.GetResponseStream();
		}

		public static Stream GetResource(string address)
		{
			return GetResource(ref address, null);
		}

		public static byte[] GetResourceData(string address)
		{
			using (Stream stream = GetResource(address))
			{
				if (stream == null)
				{
					return null;
				}
				byte[] array = null;
				try
				{
					array = new byte[stream.Length];
				}
				catch
				{
				}
				if (array == null)
				{
					byte[] array2 = new byte[1024];
					using (MemoryStream memoryStream = new MemoryStream())
					{
						for (int num = stream.Read(array2, 0, array2.Length); num > 0; num = stream.Read(array2, 0, array2.Length))
						{
							memoryStream.Write(array2, 0, num);
							if (num < array2.Length)
							{
								break;
							}
						}
						return memoryStream.ToArray();
					}
				}
				stream.Read(array, 0, array.Length);
				return array;
			}
		}

		public static Uri ResolveUrl(string address, SecureLicenseContext context)
		{
			if (context == null)
			{
				context = SecureLicenseContext.ResolveContext;
			}
			return SharedToolbox.ResolveUrl(address, (context == null) ? null : context.SupportInfo, (context == null) ? null : context.ValidatingLicense, (context == null) ? null : context.LicensedType);
		}

		public static Bitmap GetControlAsBitmap(Control ctrl)
		{
			if (ctrl == null)
			{
				return null;
			}
			Form form = ctrl.FindForm();
			if (!ctrl.IsDisposed && (form == null || !form.IsDisposed))
			{
				if (ctrl.InvokeRequired)
				{
					return ctrl.Invoke(new GetControlAsBitmapDelegate(GetControlAsBitmap), ctrl) as Bitmap;
				}
				using (ThreadBoost(ThreadPriority.Highest))
				{
					if (!ctrl.IsHandleCreated)
					{
						ctrl.CreateControl();
					}
					Bitmap bitmap = new Bitmap(ctrl.Width, ctrl.Height, PixelFormat.Format32bppRgb);
					using (Graphics graphics = Graphics.FromImage(bitmap))
					{
						IntPtr hdc = graphics.GetHdc();
						ArrayList arrayList = new ArrayList();
						try
						{
							DisableBuffering(ctrl, arrayList);
							SafeNativeMethods.SendMessage(ctrl.Handle, 791u, hdc, (IntPtr)23);
							return bitmap;
						}
						finally
						{
							graphics.ReleaseHdc(hdc);
							foreach (Control item in arrayList)
							{
								_setStyleMethod.Invoke(item, new object[2]
								{
									_bufferStyles,
									true
								});
							}
						}
					}
				}
			}
			return null;
		}

		private static void DisableBuffering(Control ctl, ArrayList controls)
		{
			if (!controls.Contains(ctl))
			{
				if ((bool)_getStyleMethod.Invoke(ctl, new object[1]
				{
					_bufferStyles
				}))
				{
					controls.Add(ctl);
					_setStyleMethod.Invoke(ctl, new object[2]
					{
						_bufferStyles,
						false
					});
				}
				if (ctl.HasChildren)
				{
					foreach (Control control in ctl.Controls)
					{
						DisableBuffering(control, controls);
					}
				}
			}
		}

		public static bool CanGetControlAsBitmap()
		{
			if (_canGetControlAsBitmap == 0)
			{
				try
				{
					Control obj = new Control();
					_getStyleMethod = typeof(Control).GetMethod("GetStyle", BindingFlags.Instance | BindingFlags.NonPublic);
					_setStyleMethod = typeof(Control).GetMethod("SetStyle", BindingFlags.Instance | BindingFlags.NonPublic);
					if (_getStyleMethod != null && _setStyleMethod != null)
					{
						_setStyleMethod.Invoke(obj, new object[2]
						{
							ControlStyles.DoubleBuffer,
							true
						});
					}
					_canGetControlAsBitmap = 1;
				}
				catch
				{
					_canGetControlAsBitmap = -1;
				}
			}
			return _canGetControlAsBitmap == 1;
		}

		

		

		public static string MakeSystemInfoString()
		{
			StringBuilder stringBuilder = new StringBuilder(256);
			try
			{
				stringBuilder.Append(SR.GetString("M_Timestamp"));
				stringBuilder.Append(": ");
				stringBuilder.Append(SafeToolbox.FormatSortableDate(DateTime.UtcNow));
				stringBuilder.Append(" / ");
				stringBuilder.Append(SafeToolbox.FormatSortableDate(DateTime.Now));
				stringBuilder.Append(Environment.NewLine);
				Assembly entryAssembly = Assembly.GetEntryAssembly();
				stringBuilder.Append(SR.GetString("M_EntryAssembly"));
				stringBuilder.Append(": ");
				try
				{
					stringBuilder.Append((entryAssembly == null) ? "N/A" : entryAssembly.FullName);
				}
				catch (Exception ex)
				{
					stringBuilder.Append(ex.Message);
				}
				stringBuilder.Append(Environment.NewLine);
				stringBuilder.Append(SR.GetString("M_BaseDirectory"));
				stringBuilder.Append(": ");
				stringBuilder.Append(AppDomain.CurrentDomain.BaseDirectory);
				stringBuilder.Append(Environment.NewLine);
				stringBuilder.Append(SR.GetString("M_Platform"));
				stringBuilder.Append(": ");
				stringBuilder.Append(OSRecord.ThisMachine.ToString());
				stringBuilder.Append(Environment.NewLine);
				stringBuilder.Append(SR.GetString("M_OSVersion"));
				stringBuilder.Append(": ");
				stringBuilder.Append(Environment.OSVersion.Version);
				stringBuilder.Append(Environment.NewLine);
				stringBuilder.Append(SR.GetString("M_CLRVersion"));
				stringBuilder.Append(": ");
				stringBuilder.Append(Environment.Version);
				stringBuilder.Append(Environment.NewLine);
				stringBuilder.Append(SR.GetString("M_ProcessBitness"));
				stringBuilder.Append(": ");
				stringBuilder.Append(SafeNativeMethods.IsWin64 ? "64-bit" : "32-bit");
				stringBuilder.Append(Environment.NewLine);
				stringBuilder.Append(SR.GetString("M_VirtualMachine"));
				stringBuilder.Append(": ");
				stringBuilder.Append((VirtualMachineLevel)Dossier.Compile());
				stringBuilder.Append(Environment.NewLine);
				stringBuilder.Append(SR.GetString("M_Culture"));
				stringBuilder.Append(": ");
				stringBuilder.Append(CultureInfo.CurrentCulture);
				stringBuilder.Append(" / ");
				stringBuilder.Append(CultureInfo.CurrentUICulture);
				stringBuilder.Append(Environment.NewLine);
				stringBuilder.Append(SR.GetString("M_Encoding"));
				stringBuilder.Append(": ");
				stringBuilder.Append(Encoding.Default.EncodingName);
				stringBuilder.Append(Environment.NewLine);
				stringBuilder.Append(SR.GetString("M_IISAvailable"));
				stringBuilder.Append(": ");
				stringBuilder.Append(IisIsAvailable);
				stringBuilder.Append(Environment.NewLine);
				stringBuilder.Append(SR.GetString("M_MachineCode"));
				stringBuilder.Append(": ");
				stringBuilder.Append(MachineProfile.Profile.Hash);
			}
			catch (Exception ex2)
			{
				stringBuilder.Append(ex2.Message);
			}
			return stringBuilder.ToString();
		}

		public static string MakeAssemblyListString()
		{
			StringBuilder stringBuilder = new StringBuilder(512);
			Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
			Array.Sort(assemblies, new AssemblyComparer());
			Assembly[] array = assemblies;
			int num = 0;
			while (true)
			{
				if (num >= array.Length)
				{
					break;
				}
				Assembly assembly = array[num];
				if (stringBuilder.Length > 0)
				{
					stringBuilder.Append(Environment.NewLine);
					stringBuilder.Append(Environment.NewLine);
				}
				try
				{
					AssemblyName name = assembly.GetName();
					stringBuilder.Append(name.Name);
					if (!name.Name.StartsWith("mscorlib") && !name.Name.StartsWith("System") && !name.Name.StartsWith("Microsoft.") && !name.Name.StartsWith("vshost"))
					{
						stringBuilder.Append(" *");
					}
					stringBuilder.Append(Environment.NewLine);
					ParsedVersion assemblyVersion = TypeHelper.GetAssemblyVersion(assembly);
					stringBuilder.Append('\t');
					stringBuilder.AppendFormat(CultureInfo.InvariantCulture, "{0} / {1}", name.Version, assemblyVersion.ToString("d \\f\\o\\r .NET f x"));
					stringBuilder.Append(Environment.NewLine);
					stringBuilder.Append('\t');
					if (!(assembly is AssemblyBuilder) && assembly.GetType().Name != "InternalAssemblyBuilder")
					{
						if (string.Compare("file://", 0, assembly.CodeBase, 0, 7, true, CultureInfo.InvariantCulture) == 0)
						{
							stringBuilder.Append(assembly.Location);
						}
						else
						{
							stringBuilder.Append(assembly.CodeBase);
						}
					}
					else
					{
						stringBuilder.Append(SR.GetString("M_DynamicAssembly"));
					}
				}
				catch (Exception ex)
				{
					stringBuilder.Append(ex.Message);
				}
				num++;
			}
			return stringBuilder.ToString();
		}

		public static string MakeDiagnosticReportString(string message, ValidationRecordCollection records, SecureLicenseContext context)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(SR.GetString("M_LicenseFailureDetails"));
			stringBuilder.Append(Environment.NewLine);
			stringBuilder.Append("--------------------------------------------------");
			stringBuilder.Append(Environment.NewLine);
			stringBuilder.Append(DateTime.UtcNow.ToString());
			stringBuilder.Append(Environment.NewLine);
			if (context != null)
			{
				if (context.RequestInfo.TestDate != DateTime.MinValue)
				{
					stringBuilder.Append('\t');
					stringBuilder.Append(context.CurrentDateAndTime.ToString());
					stringBuilder.Append(Environment.NewLine);
				}
				stringBuilder.Append(context.LicensedType.ToString());
				stringBuilder.Append(Environment.NewLine);
			}
			stringBuilder.Append("--------------------------------------------------");
			stringBuilder.Append(Environment.NewLine);
			stringBuilder.Append(Environment.NewLine);
			stringBuilder.Append(message);
			stringBuilder.Append(Environment.NewLine);
			stringBuilder.Append(Environment.NewLine);
			if (records != null && records.Count > 0)
			{
				stringBuilder.Append(SR.GetString("M_DetailsHeader"));
				stringBuilder.Append(Environment.NewLine);
				stringBuilder.Append(Environment.NewLine);
				stringBuilder.Append(records.ToString());
			}
			stringBuilder.Append(Environment.NewLine);
			stringBuilder.Append(Environment.NewLine);
			if (context != null)
			{
				if (context.SupportInfo.IncludeSystemInfo)
				{
					stringBuilder.Append(SR.GetString("M_SystemInfo"));
					stringBuilder.Append(Environment.NewLine);
					stringBuilder.Append("--------------------------------------------------");
					stringBuilder.Append(Environment.NewLine);
					stringBuilder.Append(Environment.NewLine);
					stringBuilder.Append(MakeSystemInfoString());
					stringBuilder.Append(Environment.NewLine);
					stringBuilder.Append(Environment.NewLine);
				}
				if (context.SupportInfo.IncludeAssemblies)
				{
					stringBuilder.Append(SR.GetString("M_Assemblies"));
					stringBuilder.Append(Environment.NewLine);
					stringBuilder.Append("--------------------------------------------------");
					stringBuilder.Append(Environment.NewLine);
					stringBuilder.Append(Environment.NewLine);
					stringBuilder.Append(MakeAssemblyListString());
				}
				if (context.StackTrace != null)
				{
					stringBuilder.AppendLine();
					stringBuilder.AppendLine();
					stringBuilder.AppendLine();
					stringBuilder.AppendLine();
					stringBuilder.AppendLine(SR.GetString("M_StackTrace"));
					stringBuilder.AppendLine("--------------------------------------------------");
					stringBuilder.AppendLine(context.StackTrace.ToString());
					stringBuilder.AppendLine();
				}
			}
			if (context != null)
			{
				stringBuilder.AppendFormat("\r\n****************************************************\r\nDIAGNOSTICS: {0}\r\n****************************************************\r\n", Environment.MachineName);
				stringBuilder.Append(context.GetDiagnostics());
				stringBuilder.AppendFormat("\r\n****************************************************\r\nEND DIAGNOSTICS: {0}\r\n****************************************************\r\n", Environment.MachineName);
			}
			return stringBuilder.ToString();
		}

		public static bool IsProxyException(Exception ex, SecureLicenseContext context)
		{
			if (ex != null)
			{
				WebException ex2 = ex as WebException;
				if (ex2 != null)
				{
					WebExceptionStatus status = ex2.Status;
					if (status != WebExceptionStatus.ProxyNameResolutionFailure && status != WebExceptionStatus.RequestProhibitedByProxy)
					{
						goto IL_0020;
					}
					return true;
				}
				goto IL_0020;
			}
			goto IL_0061;
			IL_0061:
			return false;
			IL_0020:
			if (ex.Message != null)
			{
				string text = ValidationRecord.PreprocessException(ex, context).ToLower();
				if (text.IndexOf("proxy") <= -1 && text.IndexOf("isa server") <= -1 && text.IndexOf("407") <= -1)
				{
					goto IL_0061;
				}
				return true;
			}
			goto IL_0061;
		}

		public static IDisposable ThreadBoost(ThreadPriority priority)
		{
			return new BoostHandle(priority);
		}

		public static AssemblyName GetAssemblyName(Assembly asm)
		{
			return new AssemblyName(asm.FullName);
		}

		public static Version GetAssemblyVersion(Assembly asm)
		{
			return GetAssemblyName(asm).Version;
		}

		public static bool CopyXmlPersistable(IXmlPersistable from, IXmlPersistable to, LicenseSaveType signing)
		{
			using (MemoryStream memoryStream = new MemoryStream())
			{
				using (XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, new UTF8Encoding(false)))
				{
					if (from.WriteToXml(xmlTextWriter, signing))
					{
						xmlTextWriter.Flush();
						memoryStream.Seek(0L, SeekOrigin.Begin);
						using (XmlTextReader reader = new XmlTextReader(memoryStream))
						{
							return to.ReadFromXml(reader);
						}
					}
					return false;
				}
			}
		}
	}
}
