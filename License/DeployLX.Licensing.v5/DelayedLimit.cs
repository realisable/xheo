using System;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public abstract class DelayedLimit : Limit
	{
		private bool _delayValidation;

		private bool _skipDelayedIfPending;

		public bool DelayValidation
		{
			get
			{
				return _delayValidation;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_delayValidation != value)
				{
					_delayValidation = value;
					base.OnChanged("DelayValidation");
				}
			}
		}

		public bool SkipDelayedIfPending
		{
			get
			{
				return _skipDelayedIfPending;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_skipDelayedIfPending != value)
				{
					_skipDelayedIfPending = value;
					base.OnChanged("SkipDelayedIfPending");
				}
			}
		}

		protected override bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			if (_delayValidation)
			{
				writer.WriteAttributeString("delayValidation", "true");
			}
			if (_skipDelayedIfPending)
			{
				writer.WriteAttributeString("skipDelayedIfPending", "true");
			}
			return true;
		}

		protected override bool ReadFromXml(XmlReader reader)
		{
			_delayValidation = (reader.GetAttribute("delayValidation") == "true");
			_skipDelayedIfPending = (reader.GetAttribute("skipDelayedIfPending") == "true");
			return true;
		}

		public override ValidationResult Validate(SecureLicenseContext context)
		{
			if (_delayValidation && !context.RequestInfo.NoDelayed)
			{
				if (!SecureLicenseManager.IsListeningForDelayed && (ValidationStates == null || ValidationStates.Count == 0))
				{
					return context.ReportError("E_DelayedWithoutListeners", this);
				}
				return ValidationResult.Valid;
			}
			return DoDelayValidate(context);
		}

		internal ValidationResult DoDelayValidate(SecureLicenseContext context)
		{
			context.Items[base.LimitId + ".Validated"] = new object();
			ValidationResult validationResult = DelayValidate(context);
			if (validationResult != ValidationResult.Valid)
			{
				context.ReportDelayedFailure();
				return validationResult;
			}
			validationResult = base.Validate(context);
			if (validationResult != ValidationResult.Valid)
			{
				context.ReportDelayedFailure();
				return validationResult;
			}
			validationResult = DelayGranted(context);
			if (validationResult != ValidationResult.Valid)
			{
				context.ReportDelayedFailure();
				return validationResult;
			}
			return base.Granted(context);
		}

		public override ValidationResult Granted(SecureLicenseContext context)
		{
			if (context.Items[base.LimitId + ".Validated"] == null)
			{
				context.Items[base.LimitId + ".Validated"] = new object();
				if (_delayValidation && !context.RequestInfo.NoDelayed)
				{
					if (_skipDelayedIfPending && base.License._pendingContexts != null && base.License._pendingContexts.Count != 0)
					{
						return ValidationResult.Valid;
					}
					context.RegisterForDelayedValidation(this);
					return ValidationResult.Valid;
				}
				ValidationResult validationResult = DelayGranted(context);
				if (validationResult != ValidationResult.Valid)
				{
					return validationResult;
				}
				return base.Granted(context);
			}
			return ValidationResult.Valid;
		}

		protected virtual ValidationResult DelayGranted(SecureLicenseContext context)
		{
			return ValidationResult.Valid;
		}

		protected internal abstract ValidationResult DelayValidate(SecureLicenseContext context);
	}
}
