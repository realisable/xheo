using System;
using System.Collections;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	internal sealed class TreeList : Control
	{
		private static byte[] collapse_png = new byte[273]
		{
			137,
			80,
			78,
			71,
			13,
			10,
			26,
			10,
			0,
			0,
			0,
			13,
			73,
			72,
			68,
			82,
			0,
			0,
			0,
			9,
			0,
			0,
			0,
			9,
			8,
			6,
			0,
			0,
			0,
			224,
			145,
			6,
			16,
			0,
			0,
			0,
			4,
			103,
			65,
			77,
			65,
			0,
			0,
			175,
			200,
			55,
			5,
			138,
			233,
			0,
			0,
			0,
			25,
			116,
			69,
			88,
			116,
			83,
			111,
			102,
			116,
			119,
			97,
			114,
			101,
			0,
			65,
			100,
			111,
			98,
			101,
			32,
			73,
			109,
			97,
			103,
			101,
			82,
			101,
			97,
			100,
			121,
			113,
			201,
			101,
			60,
			0,
			0,
			0,
			163,
			73,
			68,
			65,
			84,
			120,
			218,
			124,
			144,
			61,
			14,
			194,
			48,
			12,
			133,
			95,
			84,
			36,
			134,
			12,
			92,
			151,
			141,
			28,
			131,
			19,
			112,
			3,
			110,
			193,
			196,
			202,
			192,
			198,
			82,
			68,
			74,
			169,
			250,
			147,
			196,
			49,
			78,
			16,
			205,
			80,
			129,
			37,
			59,
			137,
			223,
			103,
			229,
			201,
			106,
			187,
			63,
			238,
			0,
			24,
			252,
			14,
			3,
			129,
			248,
			95,
			36,
			125,
			149,
			80,
			162,
			0,
			165,
			202,
			40,
			115,
			174,
			240,
			110,
			202,
			239,
			12,
			121,
			239,
			190,
			242,
			12,
			121,
			55,
			160,
			123,
			218,
			2,
			197,
			24,
			160,
			245,
			102,
			97,
			230,
			122,
			57,
			23,
			136,
			136,
			208,
			52,
			119,
			57,
			61,
			56,
			178,
			12,
			17,
			166,
			161,
			199,
			216,
			191,
			10,
			212,
			218,
			26,
			163,
			52,
			131,
			124,
			203,
			28,
			37,
			57,
			223,
			41,
			248,
			2,
			61,
			234,
			27,
			186,
			214,
			34,
			56,
			129,
			102,
			95,
			140,
			170,
			74,
			178,
			206,
			144,
			57,
			156,
			122,
			217,
			211,
			26,
			159,
			92,
			238,
			233,
			45,
			192,
			0,
			14,
			209,
			121,
			133,
			126,
			46,
			175,
			151,
			0,
			0,
			0,
			0,
			73,
			69,
			78,
			68,
			174,
			66,
			96,
			130
		};

		private static byte[] expand_png = new byte[301]
		{
			137,
			80,
			78,
			71,
			13,
			10,
			26,
			10,
			0,
			0,
			0,
			13,
			73,
			72,
			68,
			82,
			0,
			0,
			0,
			9,
			0,
			0,
			0,
			9,
			8,
			6,
			0,
			0,
			0,
			224,
			145,
			6,
			16,
			0,
			0,
			0,
			4,
			103,
			65,
			77,
			65,
			0,
			0,
			175,
			200,
			55,
			5,
			138,
			233,
			0,
			0,
			0,
			25,
			116,
			69,
			88,
			116,
			83,
			111,
			102,
			116,
			119,
			97,
			114,
			101,
			0,
			65,
			100,
			111,
			98,
			101,
			32,
			73,
			109,
			97,
			103,
			101,
			82,
			101,
			97,
			100,
			121,
			113,
			201,
			101,
			60,
			0,
			0,
			0,
			191,
			73,
			68,
			65,
			84,
			120,
			218,
			124,
			144,
			201,
			14,
			130,
			64,
			12,
			134,
			127,
			68,
			99,
			88,
			18,
			77,
			136,
			7,
			223,
			212,
			155,
			60,
			134,
			79,
			160,
			79,
			96,
			226,
			213,
			187,
			47,
			97,
			72,
			220,
			32,
			130,
			72,
			88,
			102,
			1,
			75,
			33,
			225,
			96,
			180,
			73,
			151,
			105,
			191,
			233,
			76,
			107,
			172,
			54,
			251,
			53,
			0,
			31,
			191,
			197,
			7,
			65,
			205,
			63,
			105,
			235,
			227,
			22,
			213,
			90,
			193,
			48,
			186,
			107,
			166,
			57,
			129,
			82,
			146,
			162,
			6,
			82,
			84,
			156,
			99,
			72,
			74,
			209,
			119,
			110,
			216,
			10,
			42,
			74,
			81,
			32,
			123,
			197,
			124,
			30,
			181,
			166,
			174,
			21,
			44,
			203,
			33,
			117,
			57,
			105,
			219,
			46,
			102,
			243,
			5,
			193,
			229,
			208,
			73,
			107,
			141,
			36,
			137,
			200,
			75,
			120,
			222,
			18,
			143,
			123,
			128,
			170,
			200,
			81,
			230,
			239,
			1,
			74,
			227,
			16,
			37,
			37,
			85,
			byte.MaxValue,
			108,
			116,
			11,
			56,
			214,
			252,
			183,
			30,
			122,
			134,
			23,
			100,
			105,
			12,
			37,
			4,
			142,
			135,
			29,
			194,
			235,
			25,
			52,
			24,
			13,
			209,
			150,
			29,
			134,
			252,
			237,
			41,
			167,
			61,
			77,
			209,
			233,
			247,
			158,
			62,
			2,
			12,
			0,
			17,
			177,
			120,
			207,
			122,
			180,
			230,
			50,
			0,
			0,
			0,
			0,
			73,
			69,
			78,
			68,
			174,
			66,
			96,
			130
		};

		private TreeListNodeCollection _nodes;

		private Bitmap _expand;

		private Bitmap _collapse;

		private VScrollBar _sb;

		private int _wheelDelta;

		public TreeListNodeCollection Nodes
		{
			get
			{
				return _nodes;
			}
		}

		public TreeList()
		{
			_nodes = new TreeListNodeCollection();
			_nodes.Changed += _nodes_Changed;
			base.SetStyle(ControlStyles.UserPaint | ControlStyles.ResizeRedraw | ControlStyles.Selectable | ControlStyles.SupportsTransparentBackColor | ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer, true);
			BackColor = SystemColors.Window;
			BackgroundImageLayout = ImageLayout.None;
			_sb = new VScrollBar();
			_sb.Visible = false;
			_sb.Dock = DockStyle.Right;
			_sb.ValueChanged += _sb_ValueChanged;
			base.Controls.Add(_sb);
		}

		private void _nodes_Changed(object sender, CollectionEventArgs e)
		{
			TreeListNode treeListNode = e.Element as TreeListNode;
			if (treeListNode != null)
			{
				treeListNode._treeList = this;
			}
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			if (_expand == null)
			{
				using (MemoryStream stream = new MemoryStream(expand_png))
				{
					_expand = (Image.FromStream(stream) as Bitmap);
				}
				using (MemoryStream stream2 = new MemoryStream(collapse_png))
				{
					_collapse = (Image.FromStream(stream2) as Bitmap);
				}
			}
			Graphics graphics = e.Graphics;
			SizeF lineHeight = graphics.MeasureString("EMy", Font);
			float num = 0f;
			if (lineHeight.Height < 18f)
			{
				num = (float)Math.Floor((double)((18f - lineHeight.Height) / 2f));
				lineHeight.Height = 18f;
			}
			float ecOffset = (float)Math.Floor((double)((lineHeight.Height - (float)_expand.Height) / 2f)) - num;
			using (SolidBrush brush = new SolidBrush(BackColor))
			{
				graphics.FillRectangle(brush, e.ClipRectangle);
			}
			RectangleF rectangleF = new RectangleF((float)(_expand.Width + 4), num, (float)(base.Width - _expand.Width - 4), (float)base.Height);
			if (_sb.Visible)
			{
				rectangleF.Width -= (float)(_sb.Width + 2);
			}
			DrawNodes(graphics, lineHeight, ecOffset, ref rectangleF, _nodes);
			if (rectangleF.Y < (float)base.Height)
			{
				if (_sb.Visible && _sb.Value > 0)
				{
					_sb.Value = 0;
					_sb.Visible = false;
					base.Invalidate();
				}
				else
				{
					ShowScrollBars(false);
				}
			}
			if (_sb != null && _sb.Visible)
			{
				_sb.SmallChange = (int)lineHeight.Height;
				_sb.LargeChange = base.Height;
				_sb.Maximum = (int)rectangleF.Y;
			}
		}

		private void DrawNodes(Graphics g, SizeF lineHeight, float ecOffset, ref RectangleF bounds, TreeListNodeCollection nodes)
		{
			foreach (TreeListNode item in (IEnumerable)nodes)
			{
				RectangleF bounds2 = new RectangleF(bounds.X, bounds.Y - (float)_sb.Value, bounds.Width, bounds.Height);
				bool num = bounds2.Y <= (float)base.Height && bounds2.Y > 0f;
				bool flag = num;
				SizeF sizeF = num ? item.PaintItem(g, bounds2) : item.MeasureItem(g, bounds.Width);
				if (!flag && bounds2.Y + sizeF.Height > 0f)
				{
					flag = true;
					sizeF = item.PaintItem(g, bounds2);
				}
				item._virtualBounds = new Rectangle((int)bounds.X - _expand.Width - 4, (int)bounds.Y, (int)sizeF.Width + _expand.Width + 4, (int)sizeF.Height);
				if (item.Nodes.Count > 0)
				{
					if (flag)
					{
						g.DrawImage(item.IsExpanded ? _collapse : _expand, bounds2.X - (float)_expand.Width - 2f, bounds2.Y + ecOffset);
					}
					bounds.Y += Math.Max((float)Math.Ceiling((double)sizeF.Height), lineHeight.Height) + 4f;
					if (item.IsExpanded)
					{
						bounds.X += 16f;
						bounds.Width -= 16f;
						DrawNodes(g, lineHeight, ecOffset, ref bounds, item.Nodes);
						bounds.X -= 16f;
						bounds.Width += 16f;
					}
				}
				else
				{
					bounds.Y += Math.Max((float)Math.Ceiling((double)sizeF.Height), lineHeight.Height) + 4f;
				}
				if (bounds.Y >= (float)base.Height && (_sb == null || !_sb.Visible))
				{
					ShowScrollBars(true);
					break;
				}
			}
		}

		private void ShowScrollBars(bool show)
		{
			if (!show && _sb == null)
			{
				return;
			}
			if (_sb.Visible != show)
			{
				_sb.Visible = show;
				base.Invalidate();
			}
		}

		private void _sb_ValueChanged(object sender, EventArgs e)
		{
			base.Invalidate();
		}

		protected override void OnClick(EventArgs e)
		{
			base.OnClick(e);
			Point point = Cursor.Position;
			point = base.PointToClient(point);
			point.Y += _sb.Value;
			TreeListNode treeListNode = NodeFromPoint(point, _nodes);
			if (treeListNode != null && treeListNode.Nodes.Count > 0)
			{
				treeListNode.IsExpanded = !treeListNode.IsExpanded;
				base.Invalidate();
			}
		}

		private TreeListNode NodeFromPoint(Point pos, TreeListNodeCollection nodes)
		{
			foreach (TreeListNode item in (IEnumerable)nodes)
			{
				if (item._virtualBounds.Contains(pos))
				{
					return item;
				}
				if (item.IsExpanded)
				{
					TreeListNode treeListNode2 = NodeFromPoint(pos, item.Nodes);
					if (treeListNode2 == null)
					{
						continue;
					}
					return treeListNode2;
				}
			}
			return null;
		}

		protected override void OnMouseWheel(MouseEventArgs e)
		{
			if (_sb.Visible)
			{
				_wheelDelta += e.Delta;
				while (Math.Abs(_wheelDelta) >= 120)
				{
					if (_wheelDelta > 0)
					{
						_wheelDelta -= 120;
						_sb.Value = Math.Max(0, _sb.Value - _sb.SmallChange);
					}
					else
					{
						_wheelDelta += 120;
						_sb.Value = Math.Min(_sb.Maximum - _sb.LargeChange + 1, _sb.Value + _sb.SmallChange);
					}
				}
				if (e is HandledMouseEventArgs)
				{
					((HandledMouseEventArgs)e).Handled = true;
				}
			}
			base.OnMouseWheel(e);
		}
	}
}
