using System.Xml;

namespace DeployLX.Licensing.v5
{
	[LimitEditor("FeatureFilterLimitEditor", IconResource = "asmres://DeployLX.Licensing.DlxAddIn.v5/DeployLX.Licensing.DlxAddIn.v5.Resources.Limits.Filter.png", Category = "State and Filters")]
	public class FeatureFilterLimit : Limit
	{
		private bool _inverse;

		private FeatureFlags _featureFlags;

		private SerialNumberFlags _serialNumberFlags;

		private FeatureLimit _featureLimit;

		private string _featureLimitId;

		public override string Description
		{
			get
			{
				return SR.GetString("M_FeatureFilterLimitDescription");
			}
		}

		public override string Name
		{
			get
			{
				return "Feature Filter";
			}
		}

		public bool Inverse
		{
			get
			{
				return _inverse;
			}
			set
			{
				if (_inverse != value)
				{
					base.AssertNotReadOnly();
					_inverse = value;
					base.OnChanged("Inverse");
				}
			}
		}

		public FeatureFlags FeatureFlags
		{
			get
			{
				return _featureFlags;
			}
			set
			{
				if (_featureFlags != value)
				{
					base.AssertNotReadOnly();
					_featureFlags = value;
					base.OnChanged("FeatureFlags");
				}
			}
		}

		public SerialNumberFlags SerialNumberFlags
		{
			get
			{
				return _serialNumberFlags;
			}
			set
			{
				if (_serialNumberFlags != value)
				{
					base.AssertNotReadOnly();
					_serialNumberFlags = value;
					base.OnChanged("SerialNumberFlags");
				}
			}
		}

		public FeatureLimit FeatureLimit
		{
			get
			{
				if (_featureLimit == null && _featureLimitId != null && base.License != null)
				{
					_featureLimit = (base.License.Limits[_featureLimitId] as FeatureLimit);
				}
				return _featureLimit;
			}
			set
			{
				if (_featureLimit != value)
				{
					base.AssertNotReadOnly();
					_featureLimit = value;
					_featureLimitId = ((value == null) ? null : value.LimitId);
					base.OnChanged("FeatureLimit");
					base.OnChanged("FeatureLimitId");
				}
			}
		}

		public override string SummaryText
		{
			get
			{
				int num = 0;
				int num2 = (int)FeatureFlags;
				string text = "";
				int num3 = 0;
				while (num2 > 0)
				{
					if ((num2 & 1) == 1)
					{
						num++;
						if (FeatureLimit != null)
						{
							text = FeatureLimit.Names[num3];
						}
					}
					num3++;
					num2 >>= 1;
				}
				num3 = 0;
				for (num2 = (int)SerialNumberFlags; num2 > 0; num2 >>= 1)
				{
					if ((num2 & 1) == 1)
					{
						num++;
						if (base.License.SerialNumberInfo.FlagNames.Count > num3)
						{
							text = base.License.SerialNumberInfo.FlagNames[num3];
						}
					}
					num3++;
				}
				if (num == 1 && text != "")
				{
					return text;
				}
				return string.Format("{0} flags", num);
			}
		}

		public string FeatureLimitId
		{
			get
			{
				return _featureLimitId;
			}
			set
			{
				if (!(_featureLimitId == value))
				{
					base.AssertNotReadOnly();
					_featureLimitId = value;
					_featureLimit = null;
					base.OnChanged("FeatureLimitId");
				}
			}
		}

		protected override bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			if (signing == LicenseSaveType.Signing)
			{
				base.AssertMinimumVersion(SecureLicense.v4_0);
			}
			if (_serialNumberFlags != 0)
			{
				int serialNumberFlags = (int)_serialNumberFlags;
				writer.WriteAttributeString("serialNumberFlags", serialNumberFlags.ToString());
			}
			if (_featureLimitId != null)
			{
				writer.WriteAttributeString("featureLimitId", _featureLimitId);
			}
			if (_featureFlags != 0)
			{
				int featureFlags = (int)_featureFlags;
				writer.WriteAttributeString("featureFlags", featureFlags.ToString());
			}
			if (_inverse)
			{
				writer.WriteAttributeString("inverse", "true");
			}
			return true;
		}

		protected override bool ReadFromXml(XmlReader reader)
		{
			string attribute = reader.GetAttribute("serialNumberFlags");
			_serialNumberFlags = (SerialNumberFlags)((attribute != null) ? SafeToolbox.FastParseInt32(attribute) : 0);
			_featureLimit = null;
			_featureLimitId = reader.GetAttribute("featureLimitId");
			attribute = reader.GetAttribute("featureFlags");
			_featureFlags = (FeatureFlags)((attribute != null) ? SafeToolbox.FastParseInt32(attribute) : 0);
			_inverse = (reader.GetAttribute("inverse") == "true");
			return base.ReadChildLimits(reader);
		}

		protected bool AreFlagsEnabled(SecureLicenseContext context, FeatureLimit limit)
		{
			if (FeatureFlags != 0)
			{
				if (limit == null)
				{
					return false;
				}
				if (!limit.AreFeaturesEnabled(FeatureFlags, false))
				{
					return false;
				}
			}
			if (SerialNumberFlags != 0)
			{
				if (base.License == null)
				{
					return false;
				}
				if (!base.License.GetFlag(SerialNumberFlags))
				{
					return false;
				}
			}
			if (SerialNumberFlags == SerialNumberFlags.None)
			{
				return FeatureFlags != FeatureFlags.None;
			}
			return true;
		}

		public override ValidationResult Validate(SecureLicenseContext context)
		{
			bool flag = AreFlagsEnabled(context, FeatureLimit);
			if (_inverse)
			{
				flag = !flag;
			}
			if (!flag)
			{
				return ValidationResult.Valid;
			}
			return base.Validate(context);
		}

		public override ValidationResult Granted(SecureLicenseContext context)
		{
			bool flag = AreFlagsEnabled(context, FeatureLimit);
			if (_inverse)
			{
				flag = !flag;
			}
			if (!flag)
			{
				return ValidationResult.Valid;
			}
			return base.Granted(context);
		}
	}
}
