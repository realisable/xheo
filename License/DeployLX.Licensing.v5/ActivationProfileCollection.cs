using System;
using System.Collections;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public sealed class ActivationProfileCollection : WithEventsCollection
	{
		public ActivationProfile this[int index]
		{
			get
			{
				return base.List[index] as ActivationProfile;
			}
			set
			{
				base.List[index] = value;
			}
		}

		public ActivationProfile this[string referenceId]
		{
			get
			{
				int num = SafeToolbox.FastParseInt32(referenceId);
				foreach (ActivationProfile item in (IEnumerable)this)
				{
					if (item.ReferenceId == num)
					{
						return item;
					}
				}
				return null;
			}
		}

		public int Add(ActivationProfile profile)
		{
			return base.List.Add(profile);
		}

		public void AddRange(ActivationProfile[] profiles)
		{
			if (profiles != null)
			{
				foreach (ActivationProfile profile in profiles)
				{
					Add(profile);
				}
			}
		}

		public void AddRange(ActivationProfileCollection profiles)
		{
			if (profiles != null)
			{
				foreach (ActivationProfile item in (IEnumerable)profiles)
				{
					Add(item);
				}
			}
		}

		public void Insert(int index, ActivationProfile profile)
		{
			base.List.Insert(index, profile);
		}

		public void Remove(ActivationProfile profile)
		{
			base.List.Remove(profile);
		}

		public void CopyTo(ActivationProfile[] array, int index)
		{
			base.List.CopyTo(array, index);
		}

		public int IndexOf(ActivationProfile profile)
		{
			return base.List.IndexOf(profile);
		}

		public bool Contains(ActivationProfile profile)
		{
			return base.List.Contains(profile);
		}
	}
}
