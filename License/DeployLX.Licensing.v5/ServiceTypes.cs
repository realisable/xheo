using System;

namespace DeployLX.Licensing.v5
{
	[Flags]
	public enum ServiceTypes
	{
		NotSet = 0x0,
		NotService = 0x1,
		WebServer = 0x2,
		XmlWebService = 0x4,
		NTService = 0x8,
		WindowsService = 0x8,
		Custom = 0x10,
		WebServerOrXmlService = 0x6,
		AnyServerOrService = 0xE
	}
}
