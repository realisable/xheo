using System;
using System.Collections.Generic;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	[LimitEditor("ApplicationLimitEditor", IconResource = "asmres://DeployLX.Licensing.DlxAddIn.v5/DeployLX.Licensing.DlxAddIn.v5.Resources.Limits.Application.png")]
	public class ApplicationLimit : Limit
	{
		private static readonly Dictionary<string, Semaphore> _semaphores = new Dictionary<string, Semaphore>();

		private readonly List<string> _cookies = new List<string>();

		private string _baseCookie;

		private int _allowedInstances = 1;

		public override string Description
		{
			get
			{
				return SR.GetString("M_ApplicationLimitDescription");
			}
		}

		public override string Name
		{
			get
			{
				return "Application";
			}
		}

		public virtual int AllowedInstances
		{
			get
			{
				return _allowedInstances;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_allowedInstances != value)
				{
					_allowedInstances = value;
					base.OnChanged("AllowedInstances");
				}
			}
		}

		public override string SummaryText
		{
			get
			{
				return AllowedInstances.ToString();
			}
		}

		public ApplicationLimit()
		{
			AppDomain.CurrentDomain.ProcessExit += CurrentDomain_ProcessExit;
		}

		private void CurrentDomain_ProcessExit(object sender, EventArgs e)
		{
			SecureLicenseContext.WriteDiagnosticToContext("Process exiting, releasing semaphores.");
			foreach (Semaphore value in _semaphores.Values)
			{
				SecureLicenseContext.WriteDiagnosticToContext("Releasing Semaphore: {0}", value.SafeWaitHandle);
				value.Release();
			}
			_semaphores.Clear();
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
					SecureLicenseContext.WriteDiagnosticToContext("Disposing");
					foreach (string cooky in _cookies)
					{
						Semaphore semaphore;
						if (_semaphores.TryGetValue(cooky, out semaphore))
						{
							SecureLicenseContext.WriteDiagnosticToContext("Releasing Semaphore: {0}: {1}", cooky, semaphore.SafeWaitHandle);
							semaphore.Release();
							_semaphores.Remove(cooky);
						}
					}
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public override ValidationResult Validate(SecureLicenseContext context)
		{
			if (IsApplicationValid(context, true))
			{
				return base.Validate(context);
			}
			return ValidationResult.Invalid;
		}

		protected bool IsApplicationValid(SecureLicenseContext context, bool reportError)
		{
			if (_baseCookie == null)
			{
				_baseCookie = string.Format("Global\\{0}.{1}.{2}", base.License.LicenseId, base.LimitId, base.License.SerialNumber);
			}
			string text = (!SafeToolbox.IsWebRequest) ? _baseCookie : (_baseCookie + '.' + HttpContext.Current.Request.ServerVariables["INSTANCE_META_PATH"]);
			SecureLicenseContext.WriteDiagnosticToContext("Using key {0}", text);
			Semaphore semaphore;
			if (!_semaphores.TryGetValue(text, out semaphore))
			{
				SecureLicenseContext.WriteDiagnosticToContext("Semaphore not located yet. Creating or opening new one.");
				try
				{
					semaphore = Semaphore.OpenExisting(text, SemaphoreRights.Modify | SemaphoreRights.Synchronize);
					SecureLicenseContext.WriteDiagnosticToContext("Opened an existing semaphore.");
				}
				catch (WaitHandleCannotBeOpenedException)
				{
				}
				catch (UnauthorizedAccessException exception)
				{
					context.ReportError("E_CouldNotCreateApplicationMonitor", this, exception, ErrorSeverity.High);
					return false;
				}
				catch (Exception exception2)
				{
					context.ReportError("E_CouldNotCreateApplicationMonitor", this, exception2, ErrorSeverity.High);
					return false;
				}
				if (semaphore == null)
				{
					try
					{
						SecureLicenseContext.WriteDiagnosticToContext("Creating a new semaphore.");
						SemaphoreSecurity semaphoreSecurity = new SemaphoreSecurity();
						SecurityIdentifier identity = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
						semaphoreSecurity.SetAccessRule(new SemaphoreAccessRule(identity, SemaphoreRights.Modify | SemaphoreRights.Synchronize, AccessControlType.Allow));
						bool flag;
						semaphore = new Semaphore(AllowedInstances, AllowedInstances, text, out flag, semaphoreSecurity);
					}
					catch (Exception exception3)
					{
						context.ReportError("E_CouldNotCreateApplicationMonitor", this, exception3, ErrorSeverity.High);
						return false;
					}
				}
			}
			else
			{
				SecureLicenseContext.WriteDiagnosticToContext("Semaphore already located.");
			}
			if (!_cookies.Contains(text))
			{
				SecureLicenseContext.WriteDiagnosticToContext("Checking sempahore count.");
				if (semaphore.WaitOne(0, false))
				{
					SecureLicenseContext.WriteDiagnosticToContext("Got cookie.");
					_cookies.Add(text);
					_semaphores[text] = semaphore;
					goto IL_0201;
				}
				if (reportError)
				{
					context.ReportError("E_TooManyApplications", this);
				}
				return false;
			}
			SecureLicenseContext.WriteDiagnosticToContext("Already got cookie for semaphore.");
			goto IL_0201;
			IL_0201:
			return true;
		}

		protected override bool ReadFromXml(XmlReader reader)
		{
			string attribute = reader.GetAttribute("allowedInstances");
			if (attribute == null)
			{
				_allowedInstances = 1;
			}
			else
			{
				_allowedInstances = SafeToolbox.FastParseInt32(attribute);
			}
			return base.ReadChildLimits(reader);
		}

		protected override bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			if (_allowedInstances != 1)
			{
				writer.WriteAttributeString("allowedInstances", _allowedInstances.ToString());
			}
			return true;
		}
	}
}
