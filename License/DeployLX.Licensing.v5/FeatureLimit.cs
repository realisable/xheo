using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	[LimitEditor("FeatureLimitEditor", Category = "Extendable Limits", MiniType = "FeatureLimitEditorControl", IconResource = "asmres://DeployLX.Licensing.DlxAddIn.v5/DeployLX.Licensing.DlxAddIn.v5.Resources.Limits.Feature.png")]
	public class FeatureLimit : ExtendableLimit
	{
		private string _state;

		private List<string> _validationStates;

		private FeatureNamesCollection _names;

		public override string Description
		{
			get
			{
				return SR.GetString("M_FeatureLimitDescription");
			}
		}

		public override string Name
		{
			get
			{
				return "Feature";
			}
		}

		public FeatureNamesCollection Names
		{
			get
			{
				return _names;
			}
		}

		public FeatureFlags Features
		{
			get
            {
                //Realisable - Due to the ExtendableLimit changes for the UseLimit issue obtain the Features directly from the Persistent data.
                object persistentData = base.GetPersistentData("ExtendableValue", 0, PersistentDataLocationType.SharedOrUser);
                if (persistentData != null)
                {
                    return (FeatureFlags)Convert.ToInt32(persistentData);
                }
                else
                {
                    //The original logic.
                    return (FeatureFlags)((IExtendableLimit)this).ExtendableValue;
                }
            }
			set
			{
				((IExtendableLimit)this).ExtendableValue = (int)value;
			}
		}

		public override IList<string> ValidationStates
		{
			get
			{
				if (_validationStates == null)
				{
					_validationStates = new List<string>();
					foreach (string item in (IEnumerable)_names)
					{
						_validationStates.Add(item + " Enabled");
					}
				}
				return _validationStates;
			}
		}

		public override string State
		{
			get
			{
				if (_state == null)
				{
					_state = _names.MakeFeatureList(Features, "; ", " Enabled");
				}
				return _state;
			}
		}

		public override string SummaryText
		{
			get
			{
				int lastNonDefaultIndex = _names.GetLastNonDefaultIndex();
				if (lastNonDefaultIndex == -1)
				{
					return null;
				}
				FeatureFlags featureFlags = FeatureFlags.None;
				int num = 1;
				for (int i = 0; i <= lastNonDefaultIndex; i++)
				{
					featureFlags = (FeatureFlags)((int)featureFlags | num);
					num <<= 1;
				}
				return _names.MakeFeatureList(featureFlags, ", ", null);
			}
		}

		public FeatureLimit()
		{
			_names = CreateNamesCollection();
			_names.Changed += OnNamesOnChanged;
			((IChange)this).Changed += OnChanged;
		}

		private void OnChanged(object sender, ChangeEventArgs e)
		{
			_state = null;
		}

		private void OnNamesOnChanged(object sender, CollectionEventArgs e)
		{
			base.OnCollectionChanged(this, "Names", e);
			_validationStates = null;
		}

		protected virtual FeatureNamesCollection CreateNamesCollection()
		{
			return new FeatureNamesCollection();
		}

		protected internal override void MakeReadOnly()
		{
			base.MakeReadOnly();
			_names.MakeReadOnly();
		}

		protected override bool ReadFromXml(XmlReader reader)
		{
			if (!base.ReadFromXml(reader))
			{
				return false;
			}
			_names.Reset();
			if (reader.IsEmptyElement)
			{
				reader.Read();
				return true;
			}
			base.Servers.VerifyValues = false;
			reader.Read();
			while (!reader.EOF)
			{
				if (reader.IsStartElement())
				{
					switch (reader.Name)
					{
					case "Limit":
						if (base.ReadChildLimit(reader))
						{
							break;
						}
						return false;
					case "Features":
						_names.ReadFromXml(reader, "Features", "Feature", "name", false);
						break;
					case "Server":
					{
						string attribute = reader.GetAttribute("address");
						if (attribute != null)
						{
							base.Servers.Add(SafeToolbox.XmlDecode(attribute));
							reader.Read();
							break;
						}
						return false;
					}
					default:
						reader.Skip();
						break;
					}
					continue;
				}
				reader.Read();
				break;
			}
			base.Servers.VerifyValues = true;
			return true;
		}

		protected override bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			if (signing == LicenseSaveType.Signing)
			{
				base.AssertMinimumVersion(SecureLicense.v4_0);
			}
			if (!base.WriteToXml(writer, signing))
			{
				return false;
			}
			foreach (string item in (IEnumerable)base.Servers)
			{
				writer.WriteStartElement("Server");
				writer.WriteAttributeString("address", SafeToolbox.XmlEncode(item));
				writer.WriteEndElement();
			}
			_names.WriteToXml(writer, "Features", "Feature", "name", 0, -1);
			return true;
		}

		public override ValidationResult Validate(SecureLicenseContext context)
		{
			base.ExtendWithRequestExtensionCodes(context);
			if (!IsFeatureValid(context, true))
			{
				return ValidationResult.Invalid;
			}
			return base.Validate(context);
		}

		public override ValidationResult Granted(SecureLicenseContext context)
		{
			if (!IsFeatureValid(context, true))
			{
				return ValidationResult.Invalid;
			}
			return base.Granted(context);
		}

		protected virtual bool IsFeatureValid(SecureLicenseContext context, bool reportError)
		{
			return true;
		}

		protected override bool CheckLimitReached(int maxAmount)
		{
			return false;
		}

		public override PeekResult Peek(SecureLicenseContext context)
		{
			if (IsFeatureValid(context, false))
			{
				return PeekResult.Valid;
			}
			if (!base.CanExtend)
			{
				return PeekResult.Invalid;
			}
			return PeekResult.NeedsUser;
		}

		public override string GetUseDescription(SecureLicenseContext context)
		{
			return null;
		}

		public override void GetUseRange(SecureLicenseContext context, out int min, out int max, out int current)
		{
			min = 0;
			max = 134217727;
			current = (IsFeatureValid(context, false) ? min : max);
		}

		public bool AreFeaturesEnabled(FeatureFlags features, bool onlyRequireSingleMatch)
		{
			if (features == FeatureFlags.None)
			{
				return false;
			}
			if (onlyRequireSingleMatch)
			{
				return (features & Features) == features;
			}
			return (features & Features) != FeatureFlags.None;
		}

		public bool AreFeaturesEnabled(string features, bool onlyRequireSingleMatch)
		{
			return AreFeaturesEnabled(_names.ParseFeatureList(features), onlyRequireSingleMatch);
		}

		public void EnableFeatures(FeatureFlags features, bool enabled)
		{
			if (enabled)
			{
				Features |= features;
			}
			else
			{
				Features &= ~features;
			}
		}

		public void EnableFeatures(FeatureFlags features)
		{
			EnableFeatures(features, true);
		}

		public void EnableFeatures(string features, bool enabled)
		{
			EnableFeatures(_names.ParseFeatureList(features), enabled);
		}

		public void EnableFeatures(string features)
		{
			EnableFeatures(features, true);
		}
	}
}
