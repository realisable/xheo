using System;
using System.Net;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public sealed class IPRange : IChange
	{
		internal IPRangeCollection _collection;

		private bool _isReadOnly;

		private IPAddress _first;

		private IPAddress _last;

		public IPAddress First
		{
			get
			{
				return _first;
			}
			set
			{
				AssertNotReadOnly();
				if (_collection != null && _collection.IsReadOnly)
				{
					throw new SecureLicenseException("E_LicenseReadOnly");
				}
				if (value != IPAddress.None && value != IPAddress.IPv6None)
				{
					if (_first != value)
					{
						_first = value;
						OnChanged(new ChangeEventArgs("First", this));
					}
					return;
				}
				throw new SecureLicenseException("E_InvalidIPRange");
			}
		}

		public IPAddress Last
		{
			get
			{
				return _last;
			}
			set
			{
				AssertNotReadOnly();
				if (value != IPAddress.None && value != IPAddress.IPv6None)
				{
					if (_last != value)
					{
						_last = value;
						OnChanged(new ChangeEventArgs("Last", this));
					}
					return;
				}
				throw new SecureLicenseException("E_InvalidIPRange");
			}
		}

		public event ChangeEventHandler Changed;

		private void OnChanged(ChangeEventArgs e)
		{
			if (this.Changed != null)
			{
				this.Changed(this, e);
			}
		}

		public IPRange()
		{
		}

		public IPRange(IPAddress first, IPAddress last)
		{
			Check.NotNull(first, "first");
			Check.NotNull(last, "last");
			if (first.AddressFamily != last.AddressFamily)
			{
				throw new SecureLicenseException("E_IPMustBeSameFamily");
			}
			if (first != IPAddress.None && first != IPAddress.IPv6None && last != IPAddress.None && last != IPAddress.IPv6None)
			{
				if (IPAddressLimit.CompareIPAddresses(first, last) > 0)
				{
					_first = last;
					_last = first;
				}
				else
				{
					_first = first;
					_last = last;
				}
				return;
			}
			throw new SecureLicenseException("E_InvalidIPRange");
		}

		public bool Contains(IPAddress address)
		{
			if (IPAddressLimit.CompareIPAddresses(address, _first) >= 0 && IPAddressLimit.CompareIPAddresses(address, _last) <= 0)
			{
				return true;
			}
			return false;
		}

		public override string ToString()
		{
			if (_first != null && _last != null)
			{
				return _first.ToString() + " - " + _last.ToString();
			}
			return base.ToString();
		}

		void IChange.MakeReadOnly()
		{
			_isReadOnly = true;
		}

		private void AssertNotReadOnly()
		{
			if (!_isReadOnly)
			{
				return;
			}
			throw new SecureLicenseException("E_ReadOnlyObject", "IPRange");
		}
	}
}
