using System;
using System.Collections;
using System.ComponentModel;

namespace DeployLX.Licensing.v5
{
	public class ChangeEventArgs : EventArgs
	{
		private string _name;

		private object _element;

		private object _collectionItem;

		private CollectionChangeAction _collectionAction;

		private Stack _bubbleStack = new Stack();

		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}

		public object Element
		{
			get
			{
				return _element;
			}
			set
			{
				_element = value;
			}
		}

		public object CollectionItem
		{
			get
			{
				return _collectionItem;
			}
			set
			{
				_collectionItem = value;
			}
		}

		public CollectionChangeAction CollectionAction
		{
			get
			{
				return _collectionAction;
			}
			set
			{
				_collectionAction = value;
			}
		}

		public Stack BubbleStack
		{
			get
			{
				return _bubbleStack;
			}
		}

		public ChangeEventArgs(string name, object element)
		{
			_name = name;
			_element = element;
		}

		public ChangeEventArgs(string name, object element, object item, CollectionChangeAction action)
		{
			_name = name;
			_element = element;
			_collectionItem = item;
			_collectionAction = action;
		}
	}
}
