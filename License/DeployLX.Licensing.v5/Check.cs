using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security;

namespace DeployLX.Licensing.v5
{
	[EditorBrowsable(EditorBrowsableState.Never)]
	public class Check
	{
		private static ArrayList _validAsmSns = new ArrayList();

		private Check()
		{
		}

		public static void NotNull(object value, string name, string details)
		{
			if (value != null)
			{
				return;
			}
			throw new ArgumentNullException(name, details);
		}

		public static void NotNull(object value, string name)
		{
			NotNull(value, name, null);
		}

		public static void NotNullOrEmpty(string value, string name, string details)
		{
			if (value != null && value.Length != 0)
			{
				return;
			}
			throw new ArgumentNullException(name, details);
		}

		public static void NotNullOrEmpty(string value, string name)
		{
			NotNullOrEmpty(value, name, null);
		}

		public static void GreaterThan(int value, int minValue, string name)
		{
			if (value > minValue)
			{
				return;
			}
			throw new ArgumentOutOfRangeException(name, value, null);
		}

		public static void GreaterThanEqual(int value, int minValue, string name)
		{
			if (value >= minValue)
			{
				return;
			}
			throw new ArgumentOutOfRangeException(name, value, null);
		}

		public static void LessThan(int value, int maxValue, string name)
		{
			if (value < maxValue)
			{
				return;
			}
			throw new ArgumentOutOfRangeException(name, value, null);
		}

		public static void LessThanEqual(int value, int maxValue, string name)
		{
			if (value <= maxValue)
			{
				return;
			}
			throw new ArgumentOutOfRangeException(name, value, null);
		}

		public static bool CalledByType(Type type)
		{
			StackTrace trace = new StackTrace(1, false);
			MethodBase methodBase;
			MethodBase firstNonOverloadedFrame = GetFirstNonOverloadedFrame(trace, out methodBase);
			if (methodBase != null && methodBase.DeclaringType != null && firstNonOverloadedFrame != null && firstNonOverloadedFrame.DeclaringType != null)
			{
				return type.IsAssignableFrom(firstNonOverloadedFrame.DeclaringType);
			}
			return false;
		}

		public static bool CalledByThisType()
		{
			StackTrace trace = new StackTrace(1, false);
			MethodBase methodBase;
			MethodBase firstNonOverloadedFrame = GetFirstNonOverloadedFrame(trace, out methodBase);
			if (methodBase != null && methodBase.DeclaringType != null && firstNonOverloadedFrame != null && firstNonOverloadedFrame.DeclaringType != null)
			{
				return methodBase.DeclaringType.IsAssignableFrom(firstNonOverloadedFrame.DeclaringType);
			}
			return false;
		}

		public static bool CalledByAssembly(Assembly asm)
		{
			if (Toolbox.IsMediumTrust)
			{
				return true;
			}
			StackTrace trace = new StackTrace(1, false);
			MethodBase methodBase;
			MethodBase firstNonOverloadedFrame = GetFirstNonOverloadedFrame(trace, out methodBase);
			if (methodBase != null && methodBase.DeclaringType != null && firstNonOverloadedFrame != null && firstNonOverloadedFrame.DeclaringType != null)
			{
				if (asm != firstNonOverloadedFrame.ReflectedType.Assembly)
				{
					return false;
				}
				if (_validAsmSns.Contains(asm.GetHashCode()))
				{
					return true;
				}
				AssemblyName assemblyName = Toolbox.GetAssemblyName(asm);
				byte[] publicKeyToken = assemblyName.GetPublicKeyToken();
				if (publicKeyToken != null && publicKeyToken.Length != 0 && !Crc(asm.Location))
				{
					return false;
				}
				_validAsmSns.Add(asm.GetHashCode());
				return true;
			}
			return false;
		}

		public static bool CheckCalledByThisAssembly()
		{
			return CalledByAssembly(Assembly.GetCallingAssembly());
		}

		private static MethodBase GetFirstNonOverloadedFrame(StackTrace trace, out MethodBase root)
		{
			int num = 0;
			root = null;
			StackFrame stackFrame = null;
			while (true)
			{
				if (num < trace.FrameCount)
				{
					stackFrame = trace.GetFrame(num);
					if (stackFrame != null)
					{
						root = stackFrame.GetMethod();
						if (root == null)
						{
							break;
						}
						if (root.DeclaringType == null)
						{
							break;
						}
						if (root.DeclaringType == typeof(Check))
						{
							root = null;
							num++;
							continue;
						}
						goto IL_0056;
					}
					return null;
				}
				goto IL_0056;
				IL_0056:
				if (root == null)
				{
					return null;
				}
				num++;
				while (true)
				{
					if (num < trace.FrameCount)
					{
						StackFrame frame = trace.GetFrame(num);
						if (frame != null)
						{
							MethodBase method = frame.GetMethod();
							if (method == null)
							{
								break;
							}
							if (method.DeclaringType == null)
							{
								break;
							}
							if (root.DeclaringType == method.DeclaringType && !(root.Name != method.Name))
							{
								if (root.IsConstructor)
								{
									if (method.IsConstructor)
									{
										goto IL_0138;
									}
									return method;
								}
								if (!method.IsConstructor)
								{
									MethodInfo methodInfo = root as MethodInfo;
									MethodInfo methodInfo2 = method as MethodInfo;
									ParameterInfo[] parameters = methodInfo.GetParameters();
									ParameterInfo[] parameters2 = methodInfo2.GetParameters();
									if (parameters == null && parameters2 == null)
									{
										return method;
									}
									if (parameters2 != null)
									{
										if (parameters == null)
										{
											return method;
										}
										for (int i = 0; i < parameters2.Length && i < parameters.Length; i++)
										{
											if (parameters[i].ParameterType != parameters2[i].ParameterType)
											{
												return method;
											}
										}
									}
									goto IL_0138;
								}
								return method;
							}
							return method;
						}
						return null;
					}
					return null;
					IL_0138:
					num++;
				}
				return null;
			}
			return null;
		}

		[SecuritySafeCritical]
		public static bool Crc(string path)
		{
			int num;
			if (Environment.Version.Major <= 2)
			{
				StrongNameSignatureVerification(path, 1, out num);
			}
			else
			{
				StrongNameSignatureVerification_1(path, 1, out num);
			}
			return num == 1;
		}

		[DllImport("mscoree", CharSet = CharSet.Unicode)]
		private static extern int StrongNameSignatureVerification(string path, [In] int inFlags, out int outFlags);

		[DllImport("clr", CharSet = CharSet.Unicode, EntryPoint = "StrongNameSignatureVerification")]
		private static extern int StrongNameSignatureVerification_1(string path, [In] int inFlags, out int outFlags);
	}
}
