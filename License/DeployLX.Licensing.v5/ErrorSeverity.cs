using System;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public enum ErrorSeverity
	{
		NotSet,
		Low,
		Normal,
		High,
		Derived = -1
	}
}
