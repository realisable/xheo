using System;
using System.ComponentModel;
using System.Threading;

namespace DeployLX.Licensing.v5
{
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	public sealed class AsyncValidationRequest
	{
		private bool _canceled;

		private readonly EventHandler _completedHandler;

		private readonly Thread _asyncThread;

		private readonly SecureLicenseContext _context;

		private readonly bool _reportError;

		private readonly AsyncValidationMethod _validationMethod;

		private readonly object[] _args;

		private ValidationResult _result;

		private bool _isComplete;

		private Exception _exception;

		public bool IsComplete
		{
			get
			{
				return _isComplete;
			}
		}

		public Exception Exception
		{
			get
			{
				return _exception;
			}
		}

		public AsyncValidationRequest(AsyncValidationMethod validationMethod, SecureLicenseContext context, bool reportError, object[] args, EventHandler completedHandler)
		{
			context.RegisterAsyncRequest(this);
			_completedHandler = completedHandler;
			_context = context;
			_reportError = reportError;
			_validationMethod = validationMethod;
			_args = args;
			_asyncThread = new Thread(Go);
			_asyncThread.Start();
		}

		private void Go()
		{
			try
			{
				_result = _validationMethod(_context, _reportError, _args);
				_isComplete = true;
			}
			catch (Exception ex)
			{
				if (_reportError)
				{
					if (ex is ThreadAbortException)
					{
						if (!_canceled)
						{
							_context.ReportError("E_AbortedByUser", null, ex, ErrorSeverity.Low);
						}
					}
					else
					{
						_context.ReportError("E_UnexpectedValidate", null, ex, ErrorSeverity.High);
					}
				}
				_exception = ex;
				_canceled = true;
				_isComplete = true;
			}
			finally
			{
				if (_completedHandler != null)
				{
					_completedHandler(this, EventArgs.Empty);
				}
				_context.CompleteAsyncRequest(this);
			}
		}

		public void Cancel()
		{
			_canceled = true;
			_asyncThread.Abort();
			try
			{
				_isComplete = true;
				if (_completedHandler != null)
				{
					_completedHandler(this, EventArgs.Empty);
				}
			}
			catch (Exception exception)
			{
				if (_reportError)
				{
					_context.ReportError("E_UnexpectedValidate", null, exception, ErrorSeverity.High);
				}
			}
		}

		public ValidationResult GetResult()
		{
			if (_canceled)
			{
				return ValidationResult.Canceled;
			}
			if (!_isComplete)
			{
				_asyncThread.Join();
			}
			return _result;
		}
	}
}
