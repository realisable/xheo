using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Text;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	[LimitEditor("SuperFormLimitEditor", Category = "Super Form Limits")]
	public abstract class SuperFormLimit : Limit, ISuperFormLimit, IMultipleSuperFormLimit
	{
		public class CommonPageIds
		{
			public const string Default = "DEFAULT";

			public const string Eula = "EULA";
		}

		private readonly LicenseValuesDictionary _customForms;

		[Obsolete("Use CustomForms[ CommonPageIds.Default ] instead")]
		public string CustomForm
		{
			get
			{
				return ((StringDictionary)_customForms)["DEFAULT"];
			}
			set
			{
				base.AssertNotReadOnly();
				if (((StringDictionary)_customForms)["DEFAULT"] != value)
				{
					((StringDictionary)_customForms)["DEFAULT"] = value;
				}
			}
		}

		public LicenseValuesDictionary CustomForms
		{
			get
			{
				return _customForms;
			}
		}

		[Browsable(false)]
		public bool FormShown
		{
			get
			{
				if (base.License == null)
				{
					return false;
				}
				object obj = base.License.RuntimeState[base.UniqueId + ".FormShown"];
				if (obj != null)
				{
					return (bool)obj;
				}
				return false;
			}
			set
			{
				if (base.License != null)
				{
					base.License.RuntimeState[base.UniqueId + ".FormShown"] = value;
				}
			}
		}

		protected SuperFormLimit()
		{
			_customForms = new LicenseValuesDictionary();
			_customForms.Changed += _customForms_Changed;
		}

		private void _customForms_Changed(object sender, CollectionEventArgs e)
		{
			base.OnCollectionChanged(sender, "CustomForms", e);
			if (e.Element != null && ((DictionaryEntry)e.Element).Key as string== "DEFAULT")
			{
				base.OnChanged("CustomForm");
			}
		}

		protected override bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			if (base.License != null && base.License.Version <= SecureLicense.v4_0 && CustomForm != null)
			{
				writer.WriteAttributeString("customForm", CustomForm);
			}
			if (base.License == null || base.License.Version >= SecureLicense.v4_0)
			{
				if (CustomForm != null && CustomForms.Count > 1)
				{
					goto IL_0081;
				}
				if (CustomForms.Count > 0)
				{
					goto IL_0081;
				}
			}
			goto IL_010a;
			IL_010a:
			return true;
			IL_0081:
			StringBuilder stringBuilder = new StringBuilder();
			string[] array = new string[_customForms.Keys.Count];
			_customForms.Keys.CopyTo(array, 0);
			Array.Sort(array);
			string[] array2 = array;
			foreach (string text in array2)
			{
				if (stringBuilder.Length > 0)
				{
					stringBuilder.Append('|');
				}
				stringBuilder.AppendFormat("{0}={1}", text, ((StringDictionary)_customForms)[text]);
			}
			writer.WriteAttributeString("additionalCustomForms", stringBuilder.ToString());
			goto IL_010a;
		}

		protected override bool ReadFromXml(XmlReader reader)
		{
			CustomForms.Clear();
			CustomForm = reader.GetAttribute("customForm");
			string attribute = reader.GetAttribute("additionalCustomForms");
			if (attribute != null)
			{
				string[] array = attribute.Split('|');
				string[] array2 = array;
				foreach (string text in array2)
				{
					if (text != null && text.Length != 0)
					{
						int num = text.IndexOf('=');
						if (num != -1)
						{
							string key = text.Substring(0, num);
							string value = text.Substring(num + 1);
							((StringDictionary)CustomForms)[key] = value;
						}
					}
				}
			}
			return true;
		}

		protected virtual FormResult ShowForm(SecureLicenseContext context, string pageId)
		{
			return context.ShowForm(this, pageId);
		}

		protected bool ResolveShouldShowForm(SecureLicenseContext context)
		{
			if (context.RequestInfo.ShowFormFilter != null)
			{
				return context.RequestInfo.ShowFormFilter(this, context, ResolveDefaultShouldShowForm(context));
			}
			return ResolveDefaultShouldShowForm(context);
		}

		protected virtual bool ResolveDefaultShouldShowForm(SecureLicenseContext context)
		{
			return true;
		}

		protected virtual SuperFormPanel CreatePanel(string pageId, SecureLicenseContext context)
		{
			SuperFormPanel superFormPanel = context.CreateCustomPanelOrForm(this, pageId) as SuperFormPanel;
			if (superFormPanel != null)
			{
				return superFormPanel;
			}
			switch (pageId)
			{
			case "EXTEND":
				return new ExtensionPanel(false, base.Limits.FindExtendableLimits(true));
			case "EULA":
				return new InfoPanel(Toolbox.ResolveUrl(base.License.EulaAddress, context).ToString(), null);
			default:
				return null;
			}
		}

		SuperFormPanel ISuperFormLimit.CreatePanel(string pageId, SecureLicenseContext context)
		{
			return CreatePanel(pageId, context);
		}

		protected internal override void MakeReadOnly()
		{
			base.MakeReadOnly();
			((IChange)_customForms).MakeReadOnly();
		}
	}
}
