using System;
using System.Threading;

namespace DeployLX.Licensing.v5
{
	public sealed class TimeMonitor
	{
		private Thread _monitorThread;

		private bool _stop;

		private TimeLimit _timeLimit;

		public TimeLimit TimeLimit
		{
			get
			{
				return _timeLimit;
			}
		}

		public TimeSpan TimeRemaining
		{
			get
			{
				if (_timeLimit.TimeLimitType == TimeLimitType.SpecificDate)
				{
					return TimeSpan.FromSeconds((double)_timeLimit.MaxTime64);
				}
				return TimeSpan.FromSeconds((double)Math.Max(0L, _timeLimit.MaxTime64 - _timeLimit.GetRunningTime()));
			}
		}

		public TimeSpan TotalTime
		{
			get
			{
				return TimeSpan.FromSeconds((double)_timeLimit.MaxTime64);
			}
		}

		public TimeSpan RunningTime
		{
			get
			{
				return TimeSpan.FromSeconds((double)_timeLimit.GetRunningTime());
			}
		}

		public event EventHandler TimeExpired;

		private void OnTimeExpired(EventArgs e)
		{
			if (this.TimeExpired != null)
			{
				this.TimeExpired(this, e);
			}
		}

		internal TimeMonitor(TimeLimit limit)
		{
			_timeLimit = limit;
			_monitorThread = new Thread(Monitor);
			_monitorThread.IsBackground = true;
			_monitorThread.Start();
			AppDomain.CurrentDomain.DomainUnload += CurrentDomain_DomainUnload;
			AppDomain.CurrentDomain.ProcessExit += CurrentDomain_ProcessExit;
		}

		private void CurrentDomain_ProcessExit(object sender, EventArgs e)
		{
			if (!_timeLimit.IsDisposed && _timeLimit.HasStarted)
			{
				_timeLimit.GetRunningTime();
			}
		}

		private void CurrentDomain_DomainUnload(object sender, EventArgs e)
		{
			if (!_timeLimit.IsDisposed)
			{
				_timeLimit.GetRunningTime();
			}
		}

		private void Monitor()
		{
			while (!_stop)
			{
				while (!_stop && !_timeLimit.HasStarted)
				{
					Thread.Sleep(1000);
				}
				if (!System.Threading.Monitor.TryEnter(_timeLimit, 60000))
				{
					OnTimeExpired(EventArgs.Empty);
				}
				else
				{
					try
					{
						long runningTime = _timeLimit.GetRunningTime();
						if (runningTime > _timeLimit.MaxTime64)
						{
							OnTimeExpired(EventArgs.Empty);
							return;
						}
					}
					finally
					{
						System.Threading.Monitor.Exit(_timeLimit);
					}
					if (_timeLimit.Time < 600)
					{
						Thread.Sleep(1000);
					}
					else
					{
						Thread.Sleep(60000);
					}
				}
			}
		}

		internal void Stop()
		{
			if (Check.CheckCalledByThisAssembly())
			{
				_stop = true;
			}
		}

		internal void Connect(TimeLimit timeLimit)
		{
			_timeLimit = timeLimit;
		}

		public SecureLicense ShowExtensionForm(object instance, Type licensedType, LicenseValidationRequestInfo info)
		{
			return SecureLicenseManager.ShowForm(_timeLimit, "EXTEND", instance, licensedType, info, null);
		}
	}
}
