using System;

namespace DeployLX.Licensing.v5
{
	public class LicenseEventArgs : EventArgs
	{
		private SecureLicense _license;

		private SecureLicenseContext _context;

		private ValidationRecord _validationRecord;

		private bool _handled;

		public SecureLicense License
		{
			get
			{
				return _license;
			}
		}

		public SecureLicenseContext Context
		{
			get
			{
				return _context;
			}
		}

		public ValidationRecord ValidationRecord
		{
			get
			{
				return _validationRecord;
			}
		}

		public bool Handled
		{
			get
			{
				return _handled;
			}
			set
			{
				_handled = value;
			}
		}

		public LicenseEventArgs(SecureLicense license, SecureLicenseContext context, ValidationRecord validationRecord)
		{
			_context = context;
			_license = license;
			_validationRecord = validationRecord;
		}

		public LicenseEventArgs(SecureLicenseContext context)
		{
			_context = context;
		}
	}
}
