using System;
using System.Collections;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public sealed class ImageBag : Bag
	{
		private readonly Hashtable _externalImages = new Hashtable();

		public Bitmap this[string name]
		{
			get
			{
				return GetValue(name) as Bitmap;
			}
			set
			{
				string valueUrl = GetValueUrl(name);
				if (valueUrl != null)
				{
					_externalImages.Remove(valueUrl);
				}
				base.SetValue(name, value);
			}
		}

		public Bitmap this[string name, params string[] inherit]
		{
			get
			{
				return GetValue(name, inherit) as Bitmap;
			}
		}

		public ImageBag()
			: base("Image")
		{
		}

		internal override bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			Check.NotNull(writer, "writer");
			if (!ShouldSerialize())
			{
				return true;
			}
			writer.WriteStartElement("Images");
			IDictionaryEnumerator enumerator = base.Dictionary.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					DictionaryEntry dictionaryEntry = (DictionaryEntry)enumerator.Current;
					Image image = dictionaryEntry.Value as Image;
					string text = dictionaryEntry.Value as string;
					string value = dictionaryEntry.Key as string;
					if (image != null || text != null)
					{
						writer.WriteStartElement("Image");
						writer.WriteAttributeString("name", value);
						if (text != null)
						{
							writer.WriteAttributeString("url", text);
						}
						else
						{
							using (MemoryStream memoryStream = new MemoryStream(image.Width * image.Height))
							{
								image.Save(memoryStream, ImageFormat.Png);
								memoryStream.Flush();
								string text2 = Convert.ToBase64String(memoryStream.ToArray());
								writer.WriteCData(text2);
							}
						}
						writer.WriteEndElement();
					}
				}
			}
			finally
			{
				IDisposable disposable = enumerator as IDisposable;
				if (disposable != null)
				{
					disposable.Dispose();
				}
			}
			writer.WriteEndElement();
			return true;
		}

		internal override bool ReadFromXml(XmlReader reader)
		{
			Check.NotNull(reader, "reader");
			base.AssertNotReadOnly();
			base.Dictionary.Clear();
			if (reader.Name != "Images")
			{
				return false;
			}
			if (reader.IsEmptyElement)
			{
				reader.Read();
				return true;
			}
			reader.Read();
			while (!reader.EOF)
			{
				reader.MoveToContent();
				if (reader.IsStartElement())
				{
					string name;
					if ((name = reader.Name) != null && name == "Image")
					{
						string attribute = reader.GetAttribute("name");
						if (attribute != null)
						{
							attribute = attribute.Trim();
							string attribute2 = reader.GetAttribute("url");
							if (attribute2 != null)
							{
								base.Dictionary.Add(attribute, attribute2);
								reader.Read();
								continue;
							}
							if (!reader.IsEmptyElement)
							{
								reader.Read();
								byte[] buffer = Convert.FromBase64String(reader.ReadString());
								reader.Read();
								using (MemoryStream stream = new MemoryStream(buffer))
								{
									Image value = Image.FromStream(stream);
									base.Dictionary.Add(attribute, value);
								}
								continue;
							}
							return false;
						}
						return false;
					}
					reader.Skip();
					continue;
				}
				reader.Read();
				break;
			}
			return true;
		}

		public void AddExternalImage(string name, string address)
		{
			base.SetValue(name, address);
		}

		protected override object GetValue(string name, params string[] inherit)
		{
			object valueInternal = GetValueInternal(base.Dictionary[name]);
			if (valueInternal != null)
			{
				return valueInternal;
			}
			if (inherit != null && inherit.Length > 0)
			{
				foreach (string key in inherit)
				{
					valueInternal = GetValueInternal(base.Dictionary[key]);
					if (valueInternal != null)
					{
						return valueInternal;
					}
				}
			}
			return null;
		}

		private object GetValueInternal(object value)
		{
			string text = value as string;
			if (text != null)
			{
				value = _externalImages[text];
				if (value == null)
				{
					value = Toolbox.GetImage(text, null);
					if (!text.StartsWith("licres://"))
					{
						if (value == null)
						{
							_externalImages[text] = new object();
						}
						else
						{
							_externalImages[text] = value;
						}
					}
				}
			}
			return value as Bitmap;
		}

		public string GetValueUrl(string name, params string[] inherit)
		{
			object obj = base.Dictionary[name];
			if (obj != null)
			{
				return obj as string;
			}
			if (inherit != null && inherit.Length > 0)
			{
				foreach (string key in inherit)
				{
					obj = base.Dictionary[key];
					if (obj != null)
					{
						return obj as string;
					}
				}
			}
			return null;
		}
	}
}
