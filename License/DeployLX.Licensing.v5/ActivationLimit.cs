using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Windows.Forms;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	[LimitEditor("ActivationLimitEditor", Category = "Super Form Limits", IconResource = "asmres://DeployLX.Licensing.DlxAddIn.v5/DeployLX.Licensing.DlxAddIn.v5.Resources.Limits.Activation.png")]
	public class ActivationLimit : SuperFormLimit, IServerLimit
	{
		public class PageIds
		{
			public const string Default = "DEFAULT";

			public const string ActivateManually = "ACTIVATEMANUALLY";

			public const string ActivateOnline = "ACTIVATEONLINE";

			public const string ActivateInfo = "ACTIVATEOINFO";

			public const string Deactivate = "DEACTIVATE";

			public const string Eula = "EULA";

			public const string Extend = "EXTEND";
		}

		public class ValidationStateIds
		{
			public const string Activated = "Activated";

			public const string Unactivated = "Unactivated";

			public const string UnactivatedExpired = "Unactivated Expired";
		}

		public const string DefaultAspNetHeaderTemplate = "<p class=\"dlxaHeader{InstanceId}\"><a href=\"#DoActivation\">{UI_WebActivationHeaderNotice}</a></p>";

		public const string DefaultAspNetTemplate = "<style type=\"text/css\">\r\n<!-- \r\n\t.dlxaTable{InstanceId} { border: solid 1px #4e5a74; background-color: {WebPanelColor}; }\r\n\t.dlxaHeader{InstanceId}{ font-family: sans-serif; font-size: 12px; font: messagebox; color: {WebTextColor}; }\r\n\t.dlxaHeader{InstanceId}{ background-color: {WebHeaderColor}; color: {WebHeaderTextColor}; padding: 5px; border: solid 1px #4e5a74; text-align: center; }\t\r\n\t.dlxaHeader{InstanceId} a { color: {WebHeaderTextColor}; }\r\n\t.dlxaError{InstanceId}{ background:red; color:white; padding:10px; white-space: pre-wrap; }\r\n\t.dlxaFixed{InstanceId} { font: normal 16px Consolas, 'Lucida Console', 'Courier New', monospace; }\r\n\t.dlxaTable{InstanceId} td { font-family: sans-serif; font-size: 12px; font: messagebox; color: {WebTextColor}; vertical-align: top; }\r\n\t.dlxaSide{InstanceId} { background-color: {WebSideColor}; background-image: url({WebSideImageUrl}); }\r\n\t.dlxaPanel{InstanceId} { background-image: {WebPanelImageUrl}; }\r\n\t.dlxaSide{InstanceId} td { color: {WebSideTextColor}; }\r\n\t.dlxaSide{InstanceId} a { color: {WebSideLinkTextColor} }\r\n\t.dlxaSideTitle{InstanceId}{ color: {WebSupportTitleTextColor}; font-size: 120%; }\r\n\t.dlxaSmall{InstanceId} { font-size: 85%; }\r\n\th1.dlxa{InstanceId} { font-size: 135%; line-height: 0px; }\r\n\th3.dlxa{InstanceId} { font-size: 120%; line-height: 0px; }\r\n-->\r\n</style>\r\n\r\n<p>\r\n<A name=\"DoActivation\"></a>\r\n<TABLE cellSpacing=\"0\" cellPadding=\"0\" class=\"dlxaTable{InstanceId}\">\r\n\t<TR>\r\n\t\t<TD width=\"170\" class=\"dlxaSide{InstanceId}\">\r\n\t\t\t<table cellSpacing=\"0\" cellpadding=\"8\">\r\n\t\t\t\t<tr><td align=\"center\">{WebIconUrl}</td></tr>\r\n\t\t\t\t<tr><td ><h3 class=\"dlxaSideTitle{InstanceId}\">{UI_SupportInfo}</h3></td></tr>\r\n\t\t\t\t<!-- SUPPORT PHONE START -->\r\n\t\t\t\t<tr><td>{SupportPhone}</td></tr>\r\n\t\t\t\t<!-- SUPPORT PHONE END -->\r\n\t\t\t\t<!-- SUPPORT EMAIL START -->\r\n\t\t\t\t<tr><td>\r\n\t\t\t\t\t<div><a href=\"mailto:{SupportEmail}\"><b>{UI_EmailSupport}</b></a></div>\r\n\t\t\t\t\t<div class=\"dlxaSmall{InstanceId}\">{SupportEmail}</div>\r\n\t\t\t\t</td></tr>\r\n\t\t\t\t<!-- SUPPORT EMAIL END -->\r\n\t\t\t\t<!-- SUPPORT WEB START -->\r\n\t\t\t\t<tr><td>\r\n\t\t\t\t\t<div><a href=\"{SupportWebsite}\" target=\"_blank\"><b>{UI_VisitSupportWebsite}</b></a></div>\r\n\t\t\t\t\t<div class=\"dlxaSmall{InstanceId}\">{SupportWebsite}</div>\r\n\t\t\t\t</td></tr>\r\n\t\t\t\t<!-- SUPPORT WEB END -->\r\n\t\t\t\t<tr><td><div class=\"dlxaSmall{InstanceId}\">{UI_RequirementNotice}</div></td></tr>\r\n\t\t\t</table>\r\n\t\t</TD>\r\n\t\t<td width=\"1\" nowrap bgcolor=\"{WebSideSplitterFadeColor}\"></td>\r\n\t\t<td  vAlign=\"top\" class=\"dlxaPanel{InstanceId}\">\r\n\t\t\t<div style=\"padding:8px\">\r\n\t\t\t\t<h1 class=\"dlxa{InstanceId}\">{UI_ActivateTitle}</h1>\r\n\t\t\t\t<!-- ERROR START -->\r\n\t\t\t\t<p class=\"dlxaError{InstanceId}\">{ActivationError}</p>\r\n\t\t\t\t<!-- ERROR END -->\r\n\t\t\t\t<p>{UI_ActivationNotice}</p>\r\n\t\t\t\t<p class=\"dlxaSmall{InstanceId}\">{UI_ActivationMachineWarning}</p>\r\n\t\t\t\t<!-- ACTIVATE ONLINE START -->\r\n\t\t\t\t<h3 class=\"dlxa{InstanceId}\">{UI_ActivateOnlineTitle}</h3>\r\n\t\t\t\t<p><a href=\"{Url}\" style=\"color:{WebLinkTextColor}\">{UI_ActivateOnline}</a></p>\r\n\t\t\t\t<!-- ACTIVATE ONLINE END -->\r\n\r\n\t\t\t\t<!-- ACTIVATE MANUALLY START -->\r\n\t\t\t\t<h3 class=\"dlxa{InstanceId}\">{UI_ActivateManuallyTitle}</b></h3>\r\n\t\t\t\t<p>{UI_ActivateManuallyNotice}</p>\r\n\t\t\t\r\n\t\t\t\t<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\r\n\t\t\t\t\t<tr><td><b>{UI_SerialNumber}</b></td></tr>\r\n\t\t\t\t\t<tr><td><div class=\"dlxaFixed{InstanceId}\">{SerialNumber}</div></td></tr>\r\n\t\t\t\t\t<tr><td>&nbsp;</td></tr>\r\n\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t<td><b>{UI_MachineCode}</b></td>\r\n\t\t\t\t\t\t<td width=\"40\" nowrap></td>\r\n\t\t\t\t\t\t<td><b>{UI_MachineProfile}</b></td>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t<td><div class=\"dlxaFixed{InstanceId}\">{MachineCode}</div></td>\r\n\t\t\t\t\t\t<td>&nbsp;</td>\r\n\t\t\t\t\t\t<td><select name=\"AC_{LimitId}_Machine\" id=\"AC_{LimitId}_Machine\">\r\n\t\t\t\t\t{MachineProfileSelectionOptions}\r\n\t\t\t\t</select></td>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t\t\r\n\t\t\t\t</table>\r\n\r\n\r\n\t\t\t\t<p>\r\n\t\t\t\t<div><b>Activation Code</b></div>\r\n\t\t\t\t<div><input type=\"text\" name=\"AC_{LimitId}_Code\" id=\"AC_{LimitId}_Code\" size=\"48\" class=\"dlxaFixed{InstanceId}\">\r\n\t\t\t\t<input type=\"button\" value=\"{UI_Activate}\" onClick=\"location.href='{Url},' + AC_{LimitId}_Code.value + ',' + AC_{LimitId}_Machine.value \"/>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"dlxaFixed{InstanceId}\">{UnlockCodeMask}</div>\r\n\t\t\t\t</p>\r\n\t\t\t\t<!-- ACTIVATE MANUALLY END -->\t\t\t\t\t\t\r\n\t\t\t</div>\r\n\t\t</td>\r\n\t</TR>\r\n</TABLE></p>";

		private bool _validMachine;

		private static readonly List<string> _validationStates;

		public static readonly string DefaultEmailTemplate;

		public static readonly string[] EmailTemplateVariables;

		public static readonly string[] AspNetFormTemplateVariables;

		private ShowFormOption _shouldShowForm = ShowFormOption.Always;

		private ActivationProfileCollection _profiles = new ActivationProfileCollection();

		private UriCollection _servers = new UriCollection();

		private int _serverRetries;

		private CodeAlgorithm _codeAlgorithm;

		private string _codeMask;

		private bool _autoActivate;

		private string _logoResource;

		private int _continueDelay;

		private bool _hideExtendButton;

		private bool _hideManualButton;

		private bool _canActivateByCode = true;

		private bool _canDeactivate;

		private bool _canDeactivateWithoutServer;

		private bool _dontValidateChildrenWhenActivated = true;

		private string _unlockEmailTemplate;

		private ActivationProfile _activeProfile;

		private string _aspNetFormTemplate;

		private string _aspNetHeaderTemplate;

		private string _state;

		public override string Description
		{
			get
			{
				return SR.GetString("M_ActivationLimitDescription");
			}
		}

		public override string Name
		{
			get
			{
				return "Activation";
			}
		}

		public ShowFormOption ShouldShowForm
		{
			get
			{
				return _shouldShowForm;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_shouldShowForm != value)
				{
					_shouldShowForm = value;
					base.OnChanged("ShouldShowForm");
				}
			}
		}

		public ActivationProfileCollection Profiles
		{
			get
			{
				return _profiles;
			}
		}

		public UriCollection Servers
		{
			get
			{
				return _servers;
			}
		}

		public int ServerRetries
		{
			get
			{
				return _serverRetries;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_serverRetries != value)
				{
					_serverRetries = value;
					base.OnChanged("ServerRetries");
				}
			}
		}

		public CodeAlgorithm CodeAlgorithm
		{
			get
			{
				if (_codeAlgorithm != 0)
				{
					return _codeAlgorithm;
				}
				return CodeAlgorithm.ActivationCode;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_codeAlgorithm != value)
				{
					_codeAlgorithm = value;
					base.OnChanged("CodeAlgorithm");
				}
			}
		}

		public string CodeMask
		{
			get
			{
				return _codeMask;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_codeMask != value)
				{
					_codeMask = value;
					base.OnChanged("CodeMask");
				}
			}
		}

		public bool AutoActivate
		{
			get
			{
				return _autoActivate;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_autoActivate != value)
				{
					_autoActivate = value;
					base.OnChanged("AutoActivate");
				}
			}
		}

		public bool HasActivated
		{
			get
			{
				foreach (ActivationProfile item in (IEnumerable)_profiles)
				{
					if (item.Hash != null || item.UnlockHash != null)
					{
						return true;
					}
				}
				return false;
			}
		}

		public string LogoResource
		{
			get
			{
				return _logoResource;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_logoResource != value)
				{
					SharedToolbox.ValidateUrl(value, true);
					_logoResource = value;
					base.OnChanged("LogoResource");
				}
			}
		}

		public int ContinueDelay
		{
			get
			{
				return _continueDelay;
			}
			set
			{
				base.AssertNotReadOnly();
				Check.GreaterThanEqual(value, 0, "ContinueDelay");
				if (_continueDelay != value)
				{
					_continueDelay = value;
					base.OnChanged("ContinueDelay");
				}
			}
		}

		public bool HideExtendButton
		{
			get
			{
				return _hideExtendButton;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_hideExtendButton != value)
				{
					_hideExtendButton = value;
					base.OnChanged("HideExtendButton");
				}
			}
		}

		public bool HideManualButton
		{
			get
			{
				return _hideManualButton;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_hideManualButton != value)
				{
					_hideManualButton = value;
					base.OnChanged("HideManualButton");
				}
			}
		}

		public bool CanActivateByCode
		{
			get
			{
				return _canActivateByCode;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_canActivateByCode != value)
				{
					_canActivateByCode = value;
					base.OnChanged("CanActivateByCode");
				}
			}
		}

		public bool CanDeactivate
		{
			get
			{
				return _canDeactivate;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_canDeactivate != value)
				{
					_canDeactivate = value;
					base.OnChanged("CanDeactivate");
				}
			}
		}

		public bool CanDeactivateWithoutServer
		{
			get
			{
				return _canDeactivateWithoutServer;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_canDeactivateWithoutServer != value)
				{
					_canDeactivateWithoutServer = value;
					base.OnChanged("CanDeactivateWithoutServer");
				}
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Use the child State limits to define limits to enforce in the Activated and Unactivated states.")]
		public bool DontValidateChildrenWhenActivated
		{
			get
			{
				return _dontValidateChildrenWhenActivated;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_dontValidateChildrenWhenActivated != value)
				{
					_dontValidateChildrenWhenActivated = value;
					base.OnChanged("DontValidateChildrenWhenActivated");
				}
			}
		}

		public string UnlockEmailTemplate
		{
			get
			{
				if (_unlockEmailTemplate != null)
				{
					return _unlockEmailTemplate;
				}
				return DefaultEmailTemplate;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_unlockEmailTemplate != value)
				{
					if (value == DefaultEmailTemplate)
					{
						value = null;
					}
					_unlockEmailTemplate = value;
					base.OnChanged("UnlockEmailTemplate");
				}
			}
		}

		public ActivationProfile ActiveProfile
		{
			get
			{
				return _activeProfile;
			}
		}

		public DateTime LastActivated
		{
			get
			{
				DateTime dateTime = DateTime.MinValue;
				foreach (ActivationProfile item in (IEnumerable)_profiles)
				{
					if (item.UnlockDate > dateTime)
					{
						dateTime = item.UnlockDate;
					}
				}
				return dateTime;
			}
		}

		public string AspNetFormTemplate
		{
			get
			{
				if (_aspNetFormTemplate != null)
				{
					return _aspNetFormTemplate;
				}
				return "<style type=\"text/css\">\r\n<!-- \r\n\t.dlxaTable{InstanceId} { border: solid 1px #4e5a74; background-color: {WebPanelColor}; }\r\n\t.dlxaHeader{InstanceId}{ font-family: sans-serif; font-size: 12px; font: messagebox; color: {WebTextColor}; }\r\n\t.dlxaHeader{InstanceId}{ background-color: {WebHeaderColor}; color: {WebHeaderTextColor}; padding: 5px; border: solid 1px #4e5a74; text-align: center; }\t\r\n\t.dlxaHeader{InstanceId} a { color: {WebHeaderTextColor}; }\r\n\t.dlxaError{InstanceId}{ background:red; color:white; padding:10px; white-space: pre-wrap; }\r\n\t.dlxaFixed{InstanceId} { font: normal 16px Consolas, 'Lucida Console', 'Courier New', monospace; }\r\n\t.dlxaTable{InstanceId} td { font-family: sans-serif; font-size: 12px; font: messagebox; color: {WebTextColor}; vertical-align: top; }\r\n\t.dlxaSide{InstanceId} { background-color: {WebSideColor}; background-image: url({WebSideImageUrl}); }\r\n\t.dlxaPanel{InstanceId} { background-image: {WebPanelImageUrl}; }\r\n\t.dlxaSide{InstanceId} td { color: {WebSideTextColor}; }\r\n\t.dlxaSide{InstanceId} a { color: {WebSideLinkTextColor} }\r\n\t.dlxaSideTitle{InstanceId}{ color: {WebSupportTitleTextColor}; font-size: 120%; }\r\n\t.dlxaSmall{InstanceId} { font-size: 85%; }\r\n\th1.dlxa{InstanceId} { font-size: 135%; line-height: 0px; }\r\n\th3.dlxa{InstanceId} { font-size: 120%; line-height: 0px; }\r\n-->\r\n</style>\r\n\r\n<p>\r\n<A name=\"DoActivation\"></a>\r\n<TABLE cellSpacing=\"0\" cellPadding=\"0\" class=\"dlxaTable{InstanceId}\">\r\n\t<TR>\r\n\t\t<TD width=\"170\" class=\"dlxaSide{InstanceId}\">\r\n\t\t\t<table cellSpacing=\"0\" cellpadding=\"8\">\r\n\t\t\t\t<tr><td align=\"center\">{WebIconUrl}</td></tr>\r\n\t\t\t\t<tr><td ><h3 class=\"dlxaSideTitle{InstanceId}\">{UI_SupportInfo}</h3></td></tr>\r\n\t\t\t\t<!-- SUPPORT PHONE START -->\r\n\t\t\t\t<tr><td>{SupportPhone}</td></tr>\r\n\t\t\t\t<!-- SUPPORT PHONE END -->\r\n\t\t\t\t<!-- SUPPORT EMAIL START -->\r\n\t\t\t\t<tr><td>\r\n\t\t\t\t\t<div><a href=\"mailto:{SupportEmail}\"><b>{UI_EmailSupport}</b></a></div>\r\n\t\t\t\t\t<div class=\"dlxaSmall{InstanceId}\">{SupportEmail}</div>\r\n\t\t\t\t</td></tr>\r\n\t\t\t\t<!-- SUPPORT EMAIL END -->\r\n\t\t\t\t<!-- SUPPORT WEB START -->\r\n\t\t\t\t<tr><td>\r\n\t\t\t\t\t<div><a href=\"{SupportWebsite}\" target=\"_blank\"><b>{UI_VisitSupportWebsite}</b></a></div>\r\n\t\t\t\t\t<div class=\"dlxaSmall{InstanceId}\">{SupportWebsite}</div>\r\n\t\t\t\t</td></tr>\r\n\t\t\t\t<!-- SUPPORT WEB END -->\r\n\t\t\t\t<tr><td><div class=\"dlxaSmall{InstanceId}\">{UI_RequirementNotice}</div></td></tr>\r\n\t\t\t</table>\r\n\t\t</TD>\r\n\t\t<td width=\"1\" nowrap bgcolor=\"{WebSideSplitterFadeColor}\"></td>\r\n\t\t<td  vAlign=\"top\" class=\"dlxaPanel{InstanceId}\">\r\n\t\t\t<div style=\"padding:8px\">\r\n\t\t\t\t<h1 class=\"dlxa{InstanceId}\">{UI_ActivateTitle}</h1>\r\n\t\t\t\t<!-- ERROR START -->\r\n\t\t\t\t<p class=\"dlxaError{InstanceId}\">{ActivationError}</p>\r\n\t\t\t\t<!-- ERROR END -->\r\n\t\t\t\t<p>{UI_ActivationNotice}</p>\r\n\t\t\t\t<p class=\"dlxaSmall{InstanceId}\">{UI_ActivationMachineWarning}</p>\r\n\t\t\t\t<!-- ACTIVATE ONLINE START -->\r\n\t\t\t\t<h3 class=\"dlxa{InstanceId}\">{UI_ActivateOnlineTitle}</h3>\r\n\t\t\t\t<p><a href=\"{Url}\" style=\"color:{WebLinkTextColor}\">{UI_ActivateOnline}</a></p>\r\n\t\t\t\t<!-- ACTIVATE ONLINE END -->\r\n\r\n\t\t\t\t<!-- ACTIVATE MANUALLY START -->\r\n\t\t\t\t<h3 class=\"dlxa{InstanceId}\">{UI_ActivateManuallyTitle}</b></h3>\r\n\t\t\t\t<p>{UI_ActivateManuallyNotice}</p>\r\n\t\t\t\r\n\t\t\t\t<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\r\n\t\t\t\t\t<tr><td><b>{UI_SerialNumber}</b></td></tr>\r\n\t\t\t\t\t<tr><td><div class=\"dlxaFixed{InstanceId}\">{SerialNumber}</div></td></tr>\r\n\t\t\t\t\t<tr><td>&nbsp;</td></tr>\r\n\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t<td><b>{UI_MachineCode}</b></td>\r\n\t\t\t\t\t\t<td width=\"40\" nowrap></td>\r\n\t\t\t\t\t\t<td><b>{UI_MachineProfile}</b></td>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t<td><div class=\"dlxaFixed{InstanceId}\">{MachineCode}</div></td>\r\n\t\t\t\t\t\t<td>&nbsp;</td>\r\n\t\t\t\t\t\t<td><select name=\"AC_{LimitId}_Machine\" id=\"AC_{LimitId}_Machine\">\r\n\t\t\t\t\t{MachineProfileSelectionOptions}\r\n\t\t\t\t</select></td>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t\t\r\n\t\t\t\t</table>\r\n\r\n\r\n\t\t\t\t<p>\r\n\t\t\t\t<div><b>Activation Code</b></div>\r\n\t\t\t\t<div><input type=\"text\" name=\"AC_{LimitId}_Code\" id=\"AC_{LimitId}_Code\" size=\"48\" class=\"dlxaFixed{InstanceId}\">\r\n\t\t\t\t<input type=\"button\" value=\"{UI_Activate}\" onClick=\"location.href='{Url},' + AC_{LimitId}_Code.value + ',' + AC_{LimitId}_Machine.value \"/>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"dlxaFixed{InstanceId}\">{UnlockCodeMask}</div>\r\n\t\t\t\t</p>\r\n\t\t\t\t<!-- ACTIVATE MANUALLY END -->\t\t\t\t\t\t\r\n\t\t\t</div>\r\n\t\t</td>\r\n\t</TR>\r\n</TABLE></p>";
			}
			set
			{
				base.AssertNotReadOnly();
				if (_aspNetFormTemplate != value)
				{
					if (value == "<style type=\"text/css\">\r\n<!-- \r\n\t.dlxaTable{InstanceId} { border: solid 1px #4e5a74; background-color: {WebPanelColor}; }\r\n\t.dlxaHeader{InstanceId}{ font-family: sans-serif; font-size: 12px; font: messagebox; color: {WebTextColor}; }\r\n\t.dlxaHeader{InstanceId}{ background-color: {WebHeaderColor}; color: {WebHeaderTextColor}; padding: 5px; border: solid 1px #4e5a74; text-align: center; }\t\r\n\t.dlxaHeader{InstanceId} a { color: {WebHeaderTextColor}; }\r\n\t.dlxaError{InstanceId}{ background:red; color:white; padding:10px; white-space: pre-wrap; }\r\n\t.dlxaFixed{InstanceId} { font: normal 16px Consolas, 'Lucida Console', 'Courier New', monospace; }\r\n\t.dlxaTable{InstanceId} td { font-family: sans-serif; font-size: 12px; font: messagebox; color: {WebTextColor}; vertical-align: top; }\r\n\t.dlxaSide{InstanceId} { background-color: {WebSideColor}; background-image: url({WebSideImageUrl}); }\r\n\t.dlxaPanel{InstanceId} { background-image: {WebPanelImageUrl}; }\r\n\t.dlxaSide{InstanceId} td { color: {WebSideTextColor}; }\r\n\t.dlxaSide{InstanceId} a { color: {WebSideLinkTextColor} }\r\n\t.dlxaSideTitle{InstanceId}{ color: {WebSupportTitleTextColor}; font-size: 120%; }\r\n\t.dlxaSmall{InstanceId} { font-size: 85%; }\r\n\th1.dlxa{InstanceId} { font-size: 135%; line-height: 0px; }\r\n\th3.dlxa{InstanceId} { font-size: 120%; line-height: 0px; }\r\n-->\r\n</style>\r\n\r\n<p>\r\n<A name=\"DoActivation\"></a>\r\n<TABLE cellSpacing=\"0\" cellPadding=\"0\" class=\"dlxaTable{InstanceId}\">\r\n\t<TR>\r\n\t\t<TD width=\"170\" class=\"dlxaSide{InstanceId}\">\r\n\t\t\t<table cellSpacing=\"0\" cellpadding=\"8\">\r\n\t\t\t\t<tr><td align=\"center\">{WebIconUrl}</td></tr>\r\n\t\t\t\t<tr><td ><h3 class=\"dlxaSideTitle{InstanceId}\">{UI_SupportInfo}</h3></td></tr>\r\n\t\t\t\t<!-- SUPPORT PHONE START -->\r\n\t\t\t\t<tr><td>{SupportPhone}</td></tr>\r\n\t\t\t\t<!-- SUPPORT PHONE END -->\r\n\t\t\t\t<!-- SUPPORT EMAIL START -->\r\n\t\t\t\t<tr><td>\r\n\t\t\t\t\t<div><a href=\"mailto:{SupportEmail}\"><b>{UI_EmailSupport}</b></a></div>\r\n\t\t\t\t\t<div class=\"dlxaSmall{InstanceId}\">{SupportEmail}</div>\r\n\t\t\t\t</td></tr>\r\n\t\t\t\t<!-- SUPPORT EMAIL END -->\r\n\t\t\t\t<!-- SUPPORT WEB START -->\r\n\t\t\t\t<tr><td>\r\n\t\t\t\t\t<div><a href=\"{SupportWebsite}\" target=\"_blank\"><b>{UI_VisitSupportWebsite}</b></a></div>\r\n\t\t\t\t\t<div class=\"dlxaSmall{InstanceId}\">{SupportWebsite}</div>\r\n\t\t\t\t</td></tr>\r\n\t\t\t\t<!-- SUPPORT WEB END -->\r\n\t\t\t\t<tr><td><div class=\"dlxaSmall{InstanceId}\">{UI_RequirementNotice}</div></td></tr>\r\n\t\t\t</table>\r\n\t\t</TD>\r\n\t\t<td width=\"1\" nowrap bgcolor=\"{WebSideSplitterFadeColor}\"></td>\r\n\t\t<td  vAlign=\"top\" class=\"dlxaPanel{InstanceId}\">\r\n\t\t\t<div style=\"padding:8px\">\r\n\t\t\t\t<h1 class=\"dlxa{InstanceId}\">{UI_ActivateTitle}</h1>\r\n\t\t\t\t<!-- ERROR START -->\r\n\t\t\t\t<p class=\"dlxaError{InstanceId}\">{ActivationError}</p>\r\n\t\t\t\t<!-- ERROR END -->\r\n\t\t\t\t<p>{UI_ActivationNotice}</p>\r\n\t\t\t\t<p class=\"dlxaSmall{InstanceId}\">{UI_ActivationMachineWarning}</p>\r\n\t\t\t\t<!-- ACTIVATE ONLINE START -->\r\n\t\t\t\t<h3 class=\"dlxa{InstanceId}\">{UI_ActivateOnlineTitle}</h3>\r\n\t\t\t\t<p><a href=\"{Url}\" style=\"color:{WebLinkTextColor}\">{UI_ActivateOnline}</a></p>\r\n\t\t\t\t<!-- ACTIVATE ONLINE END -->\r\n\r\n\t\t\t\t<!-- ACTIVATE MANUALLY START -->\r\n\t\t\t\t<h3 class=\"dlxa{InstanceId}\">{UI_ActivateManuallyTitle}</b></h3>\r\n\t\t\t\t<p>{UI_ActivateManuallyNotice}</p>\r\n\t\t\t\r\n\t\t\t\t<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\r\n\t\t\t\t\t<tr><td><b>{UI_SerialNumber}</b></td></tr>\r\n\t\t\t\t\t<tr><td><div class=\"dlxaFixed{InstanceId}\">{SerialNumber}</div></td></tr>\r\n\t\t\t\t\t<tr><td>&nbsp;</td></tr>\r\n\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t<td><b>{UI_MachineCode}</b></td>\r\n\t\t\t\t\t\t<td width=\"40\" nowrap></td>\r\n\t\t\t\t\t\t<td><b>{UI_MachineProfile}</b></td>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t<td><div class=\"dlxaFixed{InstanceId}\">{MachineCode}</div></td>\r\n\t\t\t\t\t\t<td>&nbsp;</td>\r\n\t\t\t\t\t\t<td><select name=\"AC_{LimitId}_Machine\" id=\"AC_{LimitId}_Machine\">\r\n\t\t\t\t\t{MachineProfileSelectionOptions}\r\n\t\t\t\t</select></td>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t\t\r\n\t\t\t\t</table>\r\n\r\n\r\n\t\t\t\t<p>\r\n\t\t\t\t<div><b>Activation Code</b></div>\r\n\t\t\t\t<div><input type=\"text\" name=\"AC_{LimitId}_Code\" id=\"AC_{LimitId}_Code\" size=\"48\" class=\"dlxaFixed{InstanceId}\">\r\n\t\t\t\t<input type=\"button\" value=\"{UI_Activate}\" onClick=\"location.href='{Url},' + AC_{LimitId}_Code.value + ',' + AC_{LimitId}_Machine.value \"/>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"dlxaFixed{InstanceId}\">{UnlockCodeMask}</div>\r\n\t\t\t\t</p>\r\n\t\t\t\t<!-- ACTIVATE MANUALLY END -->\t\t\t\t\t\t\r\n\t\t\t</div>\r\n\t\t</td>\r\n\t</TR>\r\n</TABLE></p>")
					{
						value = null;
					}
					_aspNetFormTemplate = value;
					base.OnChanged("AspNetFormTemplate");
				}
			}
		}

		public string AspNetHeaderTemplate
		{
			get
			{
				if (_aspNetHeaderTemplate != null)
				{
					return _aspNetHeaderTemplate;
				}
				return "<p class=\"dlxaHeader{InstanceId}\"><a href=\"#DoActivation\">{UI_WebActivationHeaderNotice}</a></p>";
			}
			set
			{
				base.AssertNotReadOnly();
				if (_aspNetHeaderTemplate != value)
				{
					if (value == "<p class=\"dlxaHeader{InstanceId}\"><a href=\"#DoActivation\">{UI_WebActivationHeaderNotice}</a></p>")
					{
						value = null;
					}
					_aspNetHeaderTemplate = value;
					base.OnChanged("AspNetHeaderTemplate");
				}
			}
		}

		public override IList<string> ValidationStates
		{
			get
			{
				return _validationStates;
			}
		}

		public override string State
		{
			get
			{
				return _state ?? "Unactivated";
			}
		}

		public ActivationLimit()
		{
			_profiles.Changed += _profiles_Changed;
			_profiles.Add(new ActivationProfile());
			_servers.Changed += _servers_Changed;
		}

		private void _servers_Changed(object sender, CollectionEventArgs e)
		{
			base.OnCollectionChanged(sender, "Servers", e);
		}

		private void _profiles_Changed(object sender, CollectionEventArgs e)
		{
			if (e.Action == CollectionChangeAction.Add)
			{
				ActivationProfile activationProfile = e.Element as ActivationProfile;
				if (activationProfile._referenceId == 0)
				{
					int num = 0;
					foreach (ActivationProfile item in (IEnumerable)_profiles)
					{
						if (item._referenceId >= num)
						{
							num = item._referenceId + 1;
						}
					}
					activationProfile._referenceId = num;
				}
				activationProfile._limit = this;
			}
			base.OnCollectionChanged(sender, "Profiles", e);
			if (base.License != null)
			{
				base.License.AttachChangeBubbler(e);
			}
		}

		static ActivationLimit()
		{
			DefaultEmailTemplate = SR.GetString("M_CopyActivation");
			EmailTemplateVariables = new string[3]
			{
				"SerialNumber",
				"MachineCode",
				"ReferenceId"
			};
			AspNetFormTemplateVariables = new string[41]
			{
				"LimitId",
				"LicenseId",
				"UI_ActivateTitle",
				"UI_ActivationNotice",
				"UI_ActivateOnlineTitle",
				"Url",
				"UI_ActivateOnline",
				"UI_ActivateManuallyTitle",
				"UI_ActivateManuallyNotice",
				"UI_SerialNumber",
				"SerialNumber",
				"UI_MachineCode",
				"MachineCode",
				"UI_MachineProfile",
				"MachineProfileSelectionOptions",
				"UI_Activate",
				"UnlockCodeMask",
				"UI_RequirementNotice",
				"UI_ActivationMachineWarning",
				"ActivationError",
				"UI_SupportInfo",
				"SupportPhone",
				"UI_EmailSupport",
				"SupportEmail",
				"UI_VisitSupportWebsite",
				"SupportWebsite",
				"WebPanelColor",
				"WebTextColor",
				"WebLinkTextColor",
				"WebSideColor",
				"WebSideTextColor",
				"WebSideLinkTextColor",
				"WebSideSplitterFadeColor",
				"WebSupportTitleTextColor",
				"WebSideImageUrl",
				"WebPanelImageUrl",
				"WebIconUrl",
				"UI_WebActivationHeaderNotice",
				"WebHeaderColor",
				"WebHeaderTextColor",
				"InstanceId"
			};
			_validationStates = new List<string>();
			_validationStates.Add("Activated");
			_validationStates.Add("Unactivated");
			_validationStates.Add("Unactivated Expired");
		}

		public override ValidationResult Validate(SecureLicenseContext context)
		{
			try
			{
				_state = "Unactivated";
				Peek(context);

				if (IsMachineValid(context))
				{
					_state = "Activated";
					return HasStateLimits(_state) ? base.Validate(context) : ValidationResult.Valid;
				}
				ValidationResult validationResult = Activate(context);
				switch (validationResult)
				{
				case ValidationResult.Retry:
					base.FormShown = false;
					return validationResult;
				case ValidationResult.Valid:
					if (_validMachine)
					{
						_state = "Activated";
						return HasStateLimits(_state) ? base.Validate(context) : ValidationResult.Valid;
					}
					if (!SafeToolbox.IsWebRequest)
					{
						break;
					}
					if (context.Items[base.LimitId + ".AttachedToWeb"] != this)
					{
						break;
					}
					return ValidationResult.Valid;
				}
				if (base.HasChildLimits)
				{
					validationResult = base.Validate(context);
					if (validationResult != 0)
					{
						return validationResult;
					}
					_state = "Unactivated Expired";
					if (HasStateLimits(_state))
					{
						ReportUnactivated(context);
						return base.Validate(context);
					}
				}
				return ReportUnactivated(context);
			}
			finally
			{
				if (base.FormShown)
				{
					base.SetPersistentData("ShownOnce", true, PersistentDataLocationType.SharedOrUser);
				}
			}
		}

		private ValidationResult ReportUnactivated(SecureLicenseContext context)
		{
			if (HasActivated)
			{
				return context.ReportError("E_MachineMismatch", this);
			}
			return context.ReportError("E_MachineNotActivated", this);
		}

		public override ValidationResult Granted(SecureLicenseContext context)
		{
			_state = "Unactivated";
			if (!_validMachine && !IsMachineValid(context))
			{
				if (context.IsWebRequest && !context.IsWebServiceRequest)
				{
					switch (HandleAspNetActivationRequest(context))
					{
					case ValidationResult.Retry:
						return ValidationResult.Retry;
					case ValidationResult.Valid:
						_state = "Activated";
						if (!HasStateLimits(_state))
						{
							return ValidationResult.Valid;
						}
						return base.Granted(context);
					}
					if (base.ResolveShouldShowForm(context) && AttachAspNetForm(context))
					{
						if (context.Items[base.LimitId + ".AttachedToWeb"] == this)
						{
							return ValidationResult.Valid;
						}
						if (!HasStateLimits(_state))
						{
							return ValidationResult.Valid;
						}
						return base.Granted(context);
					}
				}
				ValidationResult validationResult = base.Granted(context);
				if (validationResult != 0)
				{
					return validationResult;
				}
				_state = "Unactivated Expired";
				if (HasStateLimits(_state))
				{
					ReportUnactivated(context);
					return base.Granted(context);
				}
				return ReportUnactivated(context);
			}
			_state = "Activated";
			if (!HasStateLimits(_state))
			{
				return ValidationResult.Valid;
			}
			return base.Granted(context);
		}

		protected override bool ResolveDefaultShouldShowForm(SecureLicenseContext context)
		{
			if (IsMachineValid(context))
			{
				return false;
			}
			if (context.RequestInfo.DontShowForms)
			{
				return false;
			}
			if (_shouldShowForm == ShowFormOption.Never)
			{
				return false;
			}
			if (_shouldShowForm == ShowFormOption.Always)
			{
				return true;
			}
			if (_shouldShowForm == ShowFormOption.FirstTimeAndExpires && !(bool)base.GetPersistentData("ShownOnce", false, PersistentDataLocationType.SharedOrUser))
			{
				return true;
			}
			if (CanDelayActivation(context))
			{
				return _shouldShowForm == ShowFormOption.BeforeExpires;
			}
			return _shouldShowForm == ShowFormOption.AfterExpires;
		}

		public override PeekResult Peek(SecureLicenseContext context)
		{
			if (!IsMachineValid(context))
			{
				if (!base.ResolveShouldShowForm(context))
				{
					if (!CanDelayActivation(context))
					{
						return PeekResult.Invalid;
					}
					return PeekResult.Valid;
				}
				return PeekResult.NeedsUser;
			}
			return base.Peek(context);
		}

		protected bool IsMachineValid(SecureLicenseContext context)
		{
			foreach (ActivationProfile item in (IEnumerable)_profiles)
			{
				if (!item.CheckProfile(context))
				{
					continue;
				}
				_validMachine = true;
				_activeProfile = item;
				return true;
			}

			if (!context.RequestInfo.ShouldGetNewSerialNumber && base.License.RuntimeState[base.LimitId + "ActivationCheck"] == null && !context.RequestInfo.DeveloperMode && context.RequestInfo.ShouldPersistCodes)
			{
				foreach (ActivationProfile item2 in (IEnumerable)_profiles)
				{
					string text = base.GetPersistentData("UnlockCode" + item2.ReferenceId.ToString() + base.License.LicenseFile.ReleaseVersion, null, PersistentDataLocationType.SharedOrUser) as string;
					string text2 = base.GetPersistentData("UnlockHash" + item2.ReferenceId.ToString() + base.License.LicenseFile.ReleaseVersion, null, PersistentDataLocationType.SharedOrUser) as string;
					if (text != null && text2 != null && item2.UnlockByCode(context, text, text2) && item2.CheckProfile(context))
					{
						_validMachine = true;
						_activeProfile = item2;
						return true;
					}
				}
				base.License.RuntimeState[base.LimitId + "ActivationCheck"] = new object();
			}
			return false;
		}

		public ValidationResult Activate(SecureLicenseContext context)
		{
			try
			{
				foreach (ActivationProfile item in (IEnumerable)_profiles)
				{
					if (item.Hash != null || item.UnlockHash != null)
					{
						item.CheckProfile(context, true);
					}
				}
				context.Items["Activating"] = "true";
				if (_autoActivate || (((StringDictionary)base.License.RegistrationInfo)["_autoactivate"] == "true" && _servers.Count > 0))
				{
					ValidationResult validationResult = ActivateAtServer(context, !base.ResolveShouldShowForm(context), null);
					switch (validationResult)
					{
					case ValidationResult.Valid:
						return ValidationResult.Valid;
					case ValidationResult.Retry:
						return validationResult;
					}
				}
				if (!context.IsServiceRequest && !context.IsWebServiceRequest)
				{
					if (context.IsWebRequest)
					{
						ValidationResult validationResult2 = HandleAspNetActivationRequest(context);
						if (validationResult2 != 0)
						{
							return validationResult2;
						}
						if (base.ResolveShouldShowForm(context) && AttachAspNetForm(context))
						{
							return ValidationResult.Valid;
						}
						if (CanDelayActivation(context))
						{
							return ValidationResult.Valid;
						}
					}
					else if (base.ResolveShouldShowForm(context) && context.CanShowWindowsForm)
					{
						switch (ShowForm(context, "DEFAULT"))
						{
						case FormResult.Retry:
							return ValidationResult.Retry;
						case FormResult.Success:
							if (IsMachineValid(context))
							{
								return ValidationResult.Valid;
							}
							if (!CanDelayActivation(context))
							{
								break;
							}
							return ValidationResult.Valid;
						}
					}
					else if (CanDelayActivation(context))
					{
						return ValidationResult.Valid;
					}
				}
			}
			finally
			{
				context.Items["Activating"] = null;
			}
			return ValidationResult.Invalid;
		}

		protected virtual ValidationResult HandleAspNetActivationRequest(SecureLicenseContext context)
		{
			HttpContext httpContext = context.GetHttpContext();
			string text = httpContext.Request.QueryString["__activate"];
			if (text != null && text.StartsWith(base.LimitId))
			{
				string[] array = text.Split(',');
				if (array.Length == 1)
				{
					return ActivateAtServer(context, true, null);
				}
				if (array.Length < 3)
				{
					return ValidationResult.Invalid;
				}
				string code = array[1];
				string s = array[2];
				ActivationProfile activationProfile = null;
				try
				{
					int num = int.Parse(s);
					foreach (ActivationProfile item in (IEnumerable)_profiles)
					{
						if (item.ReferenceId != num)
						{
							continue;
						}
						activationProfile = item;
						break;
					}
				}
				catch (Exception ex)
				{
					return context.ReportError(ValidationRecord.PreprocessException(ex), this);
				}
				if (activationProfile == null)
				{
					return ValidationResult.Invalid;
				}
				if (ActivateByCode(context, code, activationProfile))
				{
					return ValidationResult.Valid;
				}
				return context.ReportError("E_ActivationCodeInvalid", this);
			}
			return ValidationResult.Invalid;
		}

		public ValidationResult ActivateAtServer(SecureLicenseContext context, bool reportError, ActivationProfile profile)
		{
			return ActivateAtServerInternal(context, reportError, new object[1]
			{
				profile
			});
		}

		private ValidationResult ActivateAtServerInternal(SecureLicenseContext context, bool reportError, object[] args)
		{
			ValidationRecord validationRecord = new ValidationRecord("E_CouldNotActivateAtServer", this, null, ErrorSeverity.Normal);
			try
			{
				if (_servers.Count == 0)
				{
					validationRecord.SubRecords.Add("E_NoServersDefined", this, null, ErrorSeverity.Low);
					goto end_IL_0014;
				}
				ActivationProfile activationProfile = args[0] as ActivationProfile;
				if (activationProfile == null)
				{
					activationProfile = (context.Items["ActiveProfile"] as ActivationProfile);
				}
				if (activationProfile == null)
				{
					ActivationProfileCollection profilesForThisMachine = GetProfilesForThisMachine();
					if (profilesForThisMachine.Count > 0 && context.Items["ActiveProfile"] == null)
					{
						if (profilesForThisMachine.Count != 1 && context.CanShowWindowsForm)
						{
							ComboBox comboBox = new ComboBox();
							MessageBoxEx messageBoxEx = new MessageBoxEx(null, "UI_SelectActivationProfile", "UI_ValidationNoticeTitle", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1, MessageBoxExOptions.None, comboBox);
							Panel panel = new Panel();
							panel.Height = 10;
							messageBoxEx.AddCustomControl(panel);
							foreach (ActivationProfile item in (IEnumerable)profilesForThisMachine)
							{
								comboBox.Items.Add(item);
							}
							if (comboBox.Items.Count == 0)
							{
								comboBox.Items.Add(SR.GetString("M_ProfileNone"));
							}
							comboBox.SelectedIndex = 0;
							comboBox.DropDownStyle = ComboBoxStyle.DropDownList;
							messageBoxEx.AddCustomControl(comboBox);
							if (context.ShowDialog(messageBoxEx) == DialogResult.OK)
							{
								context.Items["ActiveProfile"] = comboBox.SelectedItem;
								activationProfile = (comboBox.SelectedItem as ActivationProfile);
								goto IL_01a3;
							}
							return ValidationResult.Canceled;
						}
						activationProfile = profilesForThisMachine[0];
					}
				}
				goto IL_01a3;
				IL_01a3:
				LicenseValuesDictionary licenseValuesDictionary = new LicenseValuesDictionary();
				((StringDictionary)licenseValuesDictionary)["ActiveProfile"] = ((activationProfile == null) ? null : activationProfile.ReferenceId.ToString());
				((StringDictionary)licenseValuesDictionary)["MachineName"] = (context.Items["MachineName"] as string);
				foreach (string item2 in (IEnumerable)_servers)
				{
					ServerResult serverResult = context.CallServer("ACTIVATE", item2, this, null, licenseValuesDictionary, false);
					if (!serverResult.Success)
					{
						SecureLicenseException ex = serverResult.Exception as SecureLicenseException;
						if (ex != null && !context.RequestInfo.DeveloperMode)
						{
							validationRecord.SubRecords.Add(ex.ErrorId, this, null, ErrorSeverity.High, item2);
						}
						else
						{
							validationRecord.SubRecords.Add("E_UnexpectedErrorFromServer", this, serverResult.Exception, ErrorSeverity.High, item2);
						}
						continue;
					}
					if (serverResult.Retry)
					{
						return ValidationResult.Retry;
					}
					ActivationLimit activationLimit = serverResult.CopyLicense.Limits.FindLimitById(base.LimitId) as ActivationLimit;
					if (activationLimit == null)
					{
						return context.RetryWith(serverResult.CopyLicense.LicenseFile, serverResult.CopyLicense.LicenseId);
					}
					ActivationProfile activationProfile2 = null;
					foreach (ActivationProfile item3 in (IEnumerable)activationLimit.Profiles)
					{
						if (activationProfile2 == null || activationProfile2.UnlockDate < item3.UnlockDate)
						{
							activationProfile2 = item3;
						}
					}
					ActivationProfile activationProfile4 = null;
					foreach (ActivationProfile item4 in (IEnumerable)_profiles)
					{
						if (item4._referenceId == activationProfile2._referenceId)
						{
							activationProfile4 = item4;
						}
					}
					if (activationProfile4 == null)
					{
						break;
					}
					lock (SecureLicenseManager.AsyncLock)
					{
						activationProfile4.UpdateFromServer(activationProfile2);
						base.License.Signature = serverResult.CopyLicense.Signature;
					}
					context.Items["Activating"] = "true";
					if (!IsMachineValid(context))
					{
						validationRecord.SubRecords.Add("E_MachineMismatch", this, null, ErrorSeverity.Normal);
						if (reportError)
						{
							context.ReportError(validationRecord);
						}
						return ValidationResult.Invalid;
					}
					if (context.VerifySignature(base.License) == VerifySignatureResult.Valid)
					{
						return ValidationResult.Valid;
					}
					return context.RetryWith(serverResult.CopyLicense.LicenseFile, serverResult.CopyLicense.LicenseId);
				}
				end_IL_0014:;
			}
			catch (ThreadAbortException exception)
			{
				validationRecord.SubRecords.Add("E_AbortedByUser", this, exception, ErrorSeverity.Low);
			}
			if (reportError)
			{
				context.ReportError(validationRecord);
			}
			return ValidationResult.Invalid;
		}

		public AsyncValidationRequest ActivateAtServerAsync(SecureLicenseContext context, bool reportError, ActivationProfile profile, EventHandler completedHandler)
		{
			return new AsyncValidationRequest(ActivateAtServerInternal, context, reportError, new object[1]
			{
				profile
			}, completedHandler);
		}

		public bool ActivateByCode(SecureLicenseContext context, string code, ActivationProfile profile)
		{
			if (!profile.UnlockByCode(context, code))
			{
				foreach (ActivationProfile item in (IEnumerable)_profiles)
				{
					if (item != profile && item.UnlockByCode(context, code))
					{
						return IsMachineValid(context);
					}
				}
				return false;
			}
			return true;
		}

		public bool CanDelayActivation(SecureLicenseContext context)
		{
			if (!base.HasChildLimits)
			{
				return false;
			}
			Limit[] array = GetStateLimits("Unactivated").FindLimitsByType(typeof(IExtendableLimit), true);
			if (array != null && array.Length != 0)
			{
				PeekResult peekResult = base.Peek(context);
				if (peekResult != PeekResult.Invalid && peekResult != PeekResult.NeedsUser)
				{
					return true;
				}
				return false;
			}
			return false;
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public ValidationResult Deactivate(SecureLicenseContext context, bool reportError, DeactivationPhase phase)
		{
			return DeactivateInternal(context, reportError, new object[1]
			{
				phase
			});
		}

		private ValidationResult DeactivateInternal(SecureLicenseContext context, bool reportError, object[] args)
		{
			if (!_canDeactivate)
			{
				if (reportError)
				{
					context.ReportError("E_CannotDeactivate", this, null, ErrorSeverity.High);
				}
				return ValidationResult.Invalid;
			}
			try
			{
				DeactivationPhase deactivationPhase = (DeactivationPhase)args[0];
				if (deactivationPhase == DeactivationPhase.Commit)
				{
					Invalidate();
					context.MakeDeactivationConfirmationCode(this);
				}
				if (_servers.Count > 0)
				{
					ValidationResult validationResult = DeactivateAtServer(context, reportError, deactivationPhase);
					if (validationResult != ValidationResult.Valid && deactivationPhase == DeactivationPhase.CheckPermission && (!_canDeactivateWithoutServer || validationResult != ValidationResult.Canceled))
					{
						return validationResult;
					}
				}
				if (deactivationPhase == DeactivationPhase.Commit)
				{
					base.License.Flush();
				}
			}
			catch (Exception exception)
			{
				if (reportError)
				{
					context.ReportError("E_CouldNotDeactivate", this, exception, ErrorSeverity.High);
				}
				return ValidationResult.Invalid;
			}
			return ValidationResult.Valid;
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public AsyncValidationRequest DeactivateAsync(SecureLicenseContext context, bool reportError, DeactivationPhase phase, EventHandler completedHandler)
		{
			return new AsyncValidationRequest(DeactivateInternal, context, reportError, new object[1]
			{
				phase
			}, completedHandler);
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public ValidationResult DeactivateAtServer(SecureLicenseContext context, bool reportError, DeactivationPhase phase)
		{
			return DeactivateAtServerInternal(context, reportError, new object[1]
			{
				phase
			});
		}

		private ValidationResult DeactivateAtServerInternal(SecureLicenseContext context, bool reportError, object[] args)
		{
			ValidationRecord validationRecord = new ValidationRecord("E_CouldNotDeactivateAtServer", this, null, ErrorSeverity.Normal);
			bool flag = true;
			try
			{
				if (!_canDeactivate)
				{
					validationRecord.SubRecords.Add("E_CannotDeactivate", this, null, ErrorSeverity.Low);
				}
				else if (_servers.Count == 0)
				{
					validationRecord.SubRecords.Add("E_NoServersDefined", this, null, ErrorSeverity.Low);
				}
				else
				{
					LicenseValuesDictionary licenseValuesDictionary = new LicenseValuesDictionary();
					((StringDictionary)licenseValuesDictionary)["ConfirmationCode"] = context.MakeDeactivationConfirmationCode(this);
					((StringDictionary)licenseValuesDictionary)["DeactivationPhase"] = ((DeactivationPhase)args[0]).ToString();
					foreach (string item in (IEnumerable)_servers)
					{
						ServerResult serverResult = context.CallServer("DEACTIVATE", item, this, null, licenseValuesDictionary, false);
						if (serverResult.Success)
						{
							return ValidationResult.Valid;
						}
						flag &= serverResult.ConnectionError;
						SecureLicenseException ex = serverResult.Exception as SecureLicenseException;
						if (ex != null && !context.RequestInfo.DeveloperMode)
						{
							validationRecord.SubRecords.Add(ex.ErrorId, this, serverResult.Exception, ErrorSeverity.High, item);
						}
						else
						{
							validationRecord.SubRecords.Add("E_UnexpectedErrorFromServer", this, serverResult.Exception, ErrorSeverity.High, item);
						}
					}
				}
			}
			catch (ThreadAbortException exception)
			{
				validationRecord.SubRecords.Add("E_AbortedByUser", this, exception, ErrorSeverity.Low);
			}
			if (reportError)
			{
				context.ReportError(validationRecord);
			}
			if (!flag)
			{
				return ValidationResult.Invalid;
			}
			return ValidationResult.Canceled;
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public AsyncValidationRequest DeactivateAtServerAsync(SecureLicenseContext context, bool reportError, DeactivationPhase phase, EventHandler completedHandler)
		{
			return new AsyncValidationRequest(DeactivateAtServerInternal, context, reportError, new object[1]
			{
				phase
			}, completedHandler);
		}

		internal string[] GetDeactivated()
		{
			if (Check.CheckCalledByThisAssembly())
			{
				return base.GetPersistentData("DEACTIVATED", PersistentDataLocationType.SharedOrUser) as string[];
			}
			ArrayList arrayList = new ArrayList();
			foreach (ActivationProfile item in (IEnumerable)_profiles)
			{
				if (item.ServerActivationId != null)
				{
					arrayList.Add("SC." + item.ServerActivationId);
				}
				if (item.UnlockCode != null)
				{
					arrayList.Add("UC." + item.UnlockCode);
				}
			}
			return arrayList.ToArray(typeof(string)) as string[];
		}

		public void Invalidate()
		{
			_validMachine = false;
			string[] array = base.GetPersistentData("DEACTIVATED", PersistentDataLocationType.SharedOrUser) as string[];
			StringCollection stringCollection = new StringCollection();
			if (array != null && array.Length > 0)
			{
				stringCollection.AddRange(array);
			}
			foreach (ActivationProfile item in (IEnumerable)_profiles)
			{
				base.SetPersistentData("UnlockCode" + item.ReferenceId.ToString() + base.License.LicenseFile.ReleaseVersion, null, PersistentDataLocationType.SharedOrUser);
				base.SetPersistentData("UnlockHash" + item.ReferenceId.ToString() + base.License.LicenseFile.ReleaseVersion, null, PersistentDataLocationType.SharedOrUser);
				if (item.UnlockCode != null)
				{
					stringCollection.Add("UC." + item.UnlockCode);
				}
				if (item.ServerActivationId != null)
				{
					stringCollection.Add("SC." + item.ServerActivationId);
				}
				item.Clear();
			}
			if (stringCollection.Count > 0)
			{
				array = new string[stringCollection.Count];
				stringCollection.CopyTo(array, 0);
				base.SetPersistentData("DEACTIVATED", array, PersistentDataLocationType.SharedOrUser);
			}
		}

		internal void NotifyUnlocked(SecureLicenseContext context, ActivationProfile profile)
		{
			if (Check.CheckCalledByThisAssembly() && !context.RequestInfo.DeveloperMode && context.RequestInfo.ShouldPersistCodes)
			{
				base.SetPersistentData("UnlockCode" + profile.ReferenceId.ToString() + base.License.LicenseFile.ReleaseVersion, profile.UnlockCode);
				base.SetPersistentData("UnlockHash" + profile.ReferenceId.ToString() + base.License.LicenseFile.ReleaseVersion, profile.UnlockHash);
			}
		}

		protected override bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			if (signing == LicenseSaveType.Signing)
			{
				MigrateGracePeriod();
			}
			if (_profiles.Count > 1)
			{
				ActivationProfile activationProfile = _profiles[0];
				int num = 1;
				while (num < _profiles.Count)
				{
					ActivationProfile activationProfile2 = _profiles[num];
					if (activationProfile2.Hardware.Count == activationProfile.Hardware.Count)
					{
						int num2 = 0;
						while (num2 < activationProfile.Hardware.Count)
						{
							if (activationProfile.Hardware[num2].Type == activationProfile2.Hardware[num2].Type)
							{
								num2++;
								continue;
							}
							throw new SecureLicenseException("E_HardwareListInconsistent");
						}
						num++;
						continue;
					}
					throw new SecureLicenseException("E_HardwareListInconsistent");
				}
			}
			if (base.License != null && base.License.Version <= SecureLicense.v5_0)
			{
				foreach (ActivationProfile item in (IEnumerable)_profiles)
				{
					if (item.MachineType > MachineType.Laptop)
					{
						base.AssertFeatureVersion(SecureLicense.v5_0, "virtual machines");
					}
				}
			}
			if (!_canActivateByCode)
			{
				writer.WriteAttributeString("canActivateByCode", "false");
			}
			if (_canDeactivate)
			{
				writer.WriteAttributeString("canDeactivate", "true");
			}
			if (_canDeactivateWithoutServer)
			{
				writer.WriteAttributeString("canDeactivateWithoutServer", "true");
			}
			if (_shouldShowForm != ShowFormOption.Always)
			{
				int shouldShowForm = (int)_shouldShowForm;
				writer.WriteAttributeString("shouldShowForm", shouldShowForm.ToString());
			}
			if (_hideExtendButton)
			{
				writer.WriteAttributeString("hideExtendButton", "true");
			}
			if (_hideManualButton)
			{
				writer.WriteAttributeString("hideManualButton", "true");
			}
			if (!_dontValidateChildrenWhenActivated)
			{
				writer.WriteAttributeString("dontValidateChildren", "false");
			}
			if (_autoActivate)
			{
				writer.WriteAttributeString("autoActivate", "true");
			}
			if (_serverRetries > 0)
			{
				writer.WriteAttributeString("serverRetries", _serverRetries.ToString());
			}
			if (_codeAlgorithm != CodeAlgorithm.ActivationCode && _codeAlgorithm != 0)
			{
				int codeAlgorithm = (int)_codeAlgorithm;
				writer.WriteAttributeString("algorithm", codeAlgorithm.ToString());
			}
			if (_codeMask != null)
			{
				writer.WriteAttributeString("codeMask", _codeMask);
			}
			if (_continueDelay != 0)
			{
				writer.WriteAttributeString("continueDelay", _continueDelay.ToString());
			}
			if (_logoResource != null)
			{
				writer.WriteAttributeString("logoResource", _logoResource.ToString());
			}
			if (!base.WriteToXml(writer, signing))
			{
				return false;
			}
			foreach (ActivationProfile item2 in (IEnumerable)_profiles)
			{
				item2.WriteToXml(writer, signing);
			}
			foreach (string item3 in (IEnumerable)_servers)
			{
				writer.WriteStartElement("Server");
				writer.WriteAttributeString("address", SafeToolbox.XmlEncode(item3));
				writer.WriteEndElement();
			}
			if (_unlockEmailTemplate != null)
			{
				writer.WriteStartElement("UnlockEmailTemplate");
				writer.WriteAttributeString("xml:space", "preserve");
				writer.WriteCData(_unlockEmailTemplate);
				writer.WriteEndElement();
			}
			if (_aspNetFormTemplate != null)
			{
				writer.WriteStartElement("AspNetFormTemplate");
				writer.WriteAttributeString("xml:space", "preserve");
				writer.WriteCData(_aspNetFormTemplate);
				writer.WriteEndElement();
			}
			if (_aspNetHeaderTemplate != null)
			{
				writer.WriteStartElement("AspNetHeaderFormTemplate");
				writer.WriteAttributeString("xml:space", "preserve");
				writer.WriteCData(_aspNetHeaderTemplate);
				writer.WriteEndElement();
			}
			return true;
		}

		private void MigrateGracePeriod()
		{
			if (base.License != null && base.License.Version == SecureLicense.v3_0)
			{
				return;
			}
			if (base.HasChildLimits && _dontValidateChildrenWhenActivated)
			{
				bool flag = false;
				foreach (Limit item in (IEnumerable)base.Limits)
				{
					if (!(item is StateLimit))
					{
						continue;
					}
					flag = true;
					break;
				}
				if (!flag)
				{
					StateLimit stateLimit = new StateLimit();
					stateLimit.States.Add("Unactivated");
					foreach (Limit item2 in (IEnumerable)base.Limits)
					{
						stateLimit.Limits.Add(item2);
					}
					base.Limits.Clear();
					base.Limits.Add(stateLimit);
				}
			}
		}

		protected override bool ReadFromXml(XmlReader reader)
		{
			_canActivateByCode = (reader.GetAttribute("canActivateByCode") != "false");
			_canDeactivate = (reader.GetAttribute("canDeactivate") == "true");
			_canDeactivateWithoutServer = (reader.GetAttribute("canDeactivateWithoutServer") == "true");
			_hideExtendButton = (reader.GetAttribute("hideExtendButton") == "true");
			_hideManualButton = (reader.GetAttribute("hideManualButton") == "true");
			if (base.License != null && base.License.Version == SecureLicense.v3_0)
			{
				_dontValidateChildrenWhenActivated = (reader.GetAttribute("dontValidateChildren") != "false");
			}
			else
			{
				_dontValidateChildrenWhenActivated = true;
			}
			_codeMask = reader.GetAttribute("codeMask");
			_unlockEmailTemplate = null;
			_profiles.Clear();
			_autoActivate = (reader.GetAttribute("autoActivate") == "true");
			_servers.Clear();
			string attribute = reader.GetAttribute("serverRetries");
			if (attribute != null)
			{
				_serverRetries = SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_serverRetries = 0;
			}
			attribute = reader.GetAttribute("algorithm");
			if (attribute != null)
			{
				_codeAlgorithm = (CodeAlgorithm)SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_codeAlgorithm = CodeAlgorithm.NotSet;
			}
			attribute = reader.GetAttribute("shouldShowForm");
			if (attribute != null)
			{
				_shouldShowForm = (ShowFormOption)SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_shouldShowForm = ShowFormOption.Always;
			}
			attribute = reader.GetAttribute("continueDelay");
			if (attribute != null)
			{
				_continueDelay = SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_continueDelay = 0;
			}
			_logoResource = SafeToolbox.XmlDecode(reader.GetAttribute("logoResource"));
			if (!base.ReadFromXml(reader))
			{
				return false;
			}
			if (!reader.IsEmptyElement)
			{
				_servers.VerifyValues = false;
				reader.Read();
				while (!reader.EOF)
				{
					if (reader.IsStartElement())
					{
						switch (reader.Name)
						{
						case "Profile":
						{
							ActivationProfile activationProfile = new ActivationProfile();
							if (activationProfile.ReadFromXml(reader))
							{
								_profiles.Add(activationProfile);
								break;
							}
							return false;
						}
						case "Server":
							attribute = reader.GetAttribute("address");
							if (attribute != null)
							{
								_servers.Add(SafeToolbox.XmlDecode(attribute));
								reader.Read();
								break;
							}
							return false;
						case "Limit":
							if (base.ReadChildLimit(reader))
							{
								break;
							}
							return false;
						case "UnlockEmailTemplate":
							reader.Read();
							_unlockEmailTemplate = reader.ReadString();
							reader.Read();
							break;
						case "AspNetFormTemplate":
							reader.Read();
							_aspNetFormTemplate = reader.ReadString();
							reader.Read();
							break;
						case "AspNetHeaderFormTemplate":
							reader.Read();
							_aspNetHeaderTemplate = reader.ReadString();
							reader.Read();
							break;
						default:
							reader.Skip();
							break;
						}
						continue;
					}
					reader.Read();
					break;
				}
				_servers.VerifyValues = true;
			}
			else
			{
				reader.Read();
			}
			if (_profiles.Count == 0)
			{
				_profiles.Add(new ActivationProfile());
			}
			return true;
		}

		protected internal override void MakeReadOnly()
		{
			base.MakeReadOnly();
			_profiles.MakeReadOnly();
			_servers.MakeReadOnly();
		}

		protected override SuperFormPanel CreatePanel(string pageId, SecureLicenseContext context)
		{
			if (((StringDictionary)base.CustomForms)[pageId] == null)
			{
				switch (pageId)
				{
				case "ACTIVATEMANUALLY":
					return new ActivateManuallyPanel(this);
				case "ACTIVATEONLINE":
					return new ActivateOnlinePanel(this);
				case "ACTIVATEOINFO":
					return new ActivationInfoPanel(this, true);
				case "EXTEND":
					return new ExtensionPanel(!CanDelayActivation(context), GetStateLimits("Unactivated").FindLimitsByType(typeof(IExtendableLimit), true));
				case "DEACTIVATE":
					return new DeactivationPanel(this);
				case "DEFAULT":
				case null:
					return new ActivationPanel(this);
				}
			}
			return base.CreatePanel(pageId, context);
		}

		public static string MakeRequiredText(ActivationLimit limit, SecureLicenseContext context)
		{
			StringBuilder stringBuilder = new StringBuilder();
			if (!limit.CanDelayActivation(context))
			{
				stringBuilder.Append(SR.GetString("UI_AboutActivationImmediately", context.SupportInfo.Product));
			}
			else
			{
				LimitCollection stateLimits = limit.GetStateLimits("Unactivated");
				string value = (stateLimits.FindLimitByType(typeof(OrLimit), true) == null) ? SR.GetString("M_Or") : SR.GetString("M_And");
				Limit[] array = stateLimits.FindExtendableLimits();
				for (int i = 0; i < array.Length; i++)
				{
					IExtendableLimit extendableLimit = (IExtendableLimit)array[i];
					if (stringBuilder.Length > 0)
					{
						stringBuilder.Append(' ');
						stringBuilder.Append(value);
						stringBuilder.Append(' ');
					}
					stringBuilder.Append(extendableLimit.GetUseDescription(context));
				}
				stringBuilder.Append(SR.GetString("UI_AboutActivationToActivate"));
			}
			return SR.GetString("UI_AboutActivationRequiredSummary", stringBuilder, context.SupportInfo.Product);
		}

		protected bool AttachAspNetForm(SecureLicenseContext context)
		{
			System.Web.UI.Control control = context.FindWebControl();
			if (control == null)
			{
				return false;
			}
			string template = (_aspNetFormTemplate == null) ? "<style type=\"text/css\">\r\n<!-- \r\n\t.dlxaTable{InstanceId} { border: solid 1px #4e5a74; background-color: {WebPanelColor}; }\r\n\t.dlxaHeader{InstanceId}{ font-family: sans-serif; font-size: 12px; font: messagebox; color: {WebTextColor}; }\r\n\t.dlxaHeader{InstanceId}{ background-color: {WebHeaderColor}; color: {WebHeaderTextColor}; padding: 5px; border: solid 1px #4e5a74; text-align: center; }\t\r\n\t.dlxaHeader{InstanceId} a { color: {WebHeaderTextColor}; }\r\n\t.dlxaError{InstanceId}{ background:red; color:white; padding:10px; white-space: pre-wrap; }\r\n\t.dlxaFixed{InstanceId} { font: normal 16px Consolas, 'Lucida Console', 'Courier New', monospace; }\r\n\t.dlxaTable{InstanceId} td { font-family: sans-serif; font-size: 12px; font: messagebox; color: {WebTextColor}; vertical-align: top; }\r\n\t.dlxaSide{InstanceId} { background-color: {WebSideColor}; background-image: url({WebSideImageUrl}); }\r\n\t.dlxaPanel{InstanceId} { background-image: {WebPanelImageUrl}; }\r\n\t.dlxaSide{InstanceId} td { color: {WebSideTextColor}; }\r\n\t.dlxaSide{InstanceId} a { color: {WebSideLinkTextColor} }\r\n\t.dlxaSideTitle{InstanceId}{ color: {WebSupportTitleTextColor}; font-size: 120%; }\r\n\t.dlxaSmall{InstanceId} { font-size: 85%; }\r\n\th1.dlxa{InstanceId} { font-size: 135%; line-height: 0px; }\r\n\th3.dlxa{InstanceId} { font-size: 120%; line-height: 0px; }\r\n-->\r\n</style>\r\n\r\n<p>\r\n<A name=\"DoActivation\"></a>\r\n<TABLE cellSpacing=\"0\" cellPadding=\"0\" class=\"dlxaTable{InstanceId}\">\r\n\t<TR>\r\n\t\t<TD width=\"170\" class=\"dlxaSide{InstanceId}\">\r\n\t\t\t<table cellSpacing=\"0\" cellpadding=\"8\">\r\n\t\t\t\t<tr><td align=\"center\">{WebIconUrl}</td></tr>\r\n\t\t\t\t<tr><td ><h3 class=\"dlxaSideTitle{InstanceId}\">{UI_SupportInfo}</h3></td></tr>\r\n\t\t\t\t<!-- SUPPORT PHONE START -->\r\n\t\t\t\t<tr><td>{SupportPhone}</td></tr>\r\n\t\t\t\t<!-- SUPPORT PHONE END -->\r\n\t\t\t\t<!-- SUPPORT EMAIL START -->\r\n\t\t\t\t<tr><td>\r\n\t\t\t\t\t<div><a href=\"mailto:{SupportEmail}\"><b>{UI_EmailSupport}</b></a></div>\r\n\t\t\t\t\t<div class=\"dlxaSmall{InstanceId}\">{SupportEmail}</div>\r\n\t\t\t\t</td></tr>\r\n\t\t\t\t<!-- SUPPORT EMAIL END -->\r\n\t\t\t\t<!-- SUPPORT WEB START -->\r\n\t\t\t\t<tr><td>\r\n\t\t\t\t\t<div><a href=\"{SupportWebsite}\" target=\"_blank\"><b>{UI_VisitSupportWebsite}</b></a></div>\r\n\t\t\t\t\t<div class=\"dlxaSmall{InstanceId}\">{SupportWebsite}</div>\r\n\t\t\t\t</td></tr>\r\n\t\t\t\t<!-- SUPPORT WEB END -->\r\n\t\t\t\t<tr><td><div class=\"dlxaSmall{InstanceId}\">{UI_RequirementNotice}</div></td></tr>\r\n\t\t\t</table>\r\n\t\t</TD>\r\n\t\t<td width=\"1\" nowrap bgcolor=\"{WebSideSplitterFadeColor}\"></td>\r\n\t\t<td  vAlign=\"top\" class=\"dlxaPanel{InstanceId}\">\r\n\t\t\t<div style=\"padding:8px\">\r\n\t\t\t\t<h1 class=\"dlxa{InstanceId}\">{UI_ActivateTitle}</h1>\r\n\t\t\t\t<!-- ERROR START -->\r\n\t\t\t\t<p class=\"dlxaError{InstanceId}\">{ActivationError}</p>\r\n\t\t\t\t<!-- ERROR END -->\r\n\t\t\t\t<p>{UI_ActivationNotice}</p>\r\n\t\t\t\t<p class=\"dlxaSmall{InstanceId}\">{UI_ActivationMachineWarning}</p>\r\n\t\t\t\t<!-- ACTIVATE ONLINE START -->\r\n\t\t\t\t<h3 class=\"dlxa{InstanceId}\">{UI_ActivateOnlineTitle}</h3>\r\n\t\t\t\t<p><a href=\"{Url}\" style=\"color:{WebLinkTextColor}\">{UI_ActivateOnline}</a></p>\r\n\t\t\t\t<!-- ACTIVATE ONLINE END -->\r\n\r\n\t\t\t\t<!-- ACTIVATE MANUALLY START -->\r\n\t\t\t\t<h3 class=\"dlxa{InstanceId}\">{UI_ActivateManuallyTitle}</b></h3>\r\n\t\t\t\t<p>{UI_ActivateManuallyNotice}</p>\r\n\t\t\t\r\n\t\t\t\t<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\r\n\t\t\t\t\t<tr><td><b>{UI_SerialNumber}</b></td></tr>\r\n\t\t\t\t\t<tr><td><div class=\"dlxaFixed{InstanceId}\">{SerialNumber}</div></td></tr>\r\n\t\t\t\t\t<tr><td>&nbsp;</td></tr>\r\n\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t<td><b>{UI_MachineCode}</b></td>\r\n\t\t\t\t\t\t<td width=\"40\" nowrap></td>\r\n\t\t\t\t\t\t<td><b>{UI_MachineProfile}</b></td>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t<td><div class=\"dlxaFixed{InstanceId}\">{MachineCode}</div></td>\r\n\t\t\t\t\t\t<td>&nbsp;</td>\r\n\t\t\t\t\t\t<td><select name=\"AC_{LimitId}_Machine\" id=\"AC_{LimitId}_Machine\">\r\n\t\t\t\t\t{MachineProfileSelectionOptions}\r\n\t\t\t\t</select></td>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t\t\r\n\t\t\t\t</table>\r\n\r\n\r\n\t\t\t\t<p>\r\n\t\t\t\t<div><b>Activation Code</b></div>\r\n\t\t\t\t<div><input type=\"text\" name=\"AC_{LimitId}_Code\" id=\"AC_{LimitId}_Code\" size=\"48\" class=\"dlxaFixed{InstanceId}\">\r\n\t\t\t\t<input type=\"button\" value=\"{UI_Activate}\" onClick=\"location.href='{Url},' + AC_{LimitId}_Code.value + ',' + AC_{LimitId}_Machine.value \"/>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"dlxaFixed{InstanceId}\">{UnlockCodeMask}</div>\r\n\t\t\t\t</p>\r\n\t\t\t\t<!-- ACTIVATE MANUALLY END -->\t\t\t\t\t\t\r\n\t\t\t</div>\r\n\t\t</td>\r\n\t</TR>\r\n</TABLE></p>" : _aspNetFormTemplate;
			string template2 = (_aspNetHeaderTemplate == null) ? "<p class=\"dlxaHeader{InstanceId}\"><a href=\"#DoActivation\">{UI_WebActivationHeaderNotice}</a></p>" : _aspNetHeaderTemplate;
			bool flag = !CanDelayActivation(context);
			HttpContext httpContext = context.GetHttpContext();
			string text = httpContext.Request.Url.ToString();
			string text2 = null;
			int num = text.IndexOf("__activate");
			if (num > -1)
			{
				int num2 = text.IndexOf('&');
				if (text[num - 1] == '&' || text[num - 1] == '?')
				{
					num--;
				}
				text = ((num2 != -1) ? text.Remove(num, num2 - num) : text.Substring(0, num));
				if (context.LatestValidationRecord != null)
				{
					text2 = context.LatestValidationRecord.ToString();
				}
			}
			text = ((text.IndexOf('?') != -1) ? (text + "&__activate=" + base.LimitId) : (text + "?__activate=" + base.LimitId));
			if (text2 != null)
			{
				text2 = text2.Replace("\r\n", "<br/><br/>");
			}
			string[] values = MakeTemplateVariables(context, text, text2, false);
			template = TemplateFormatter.Format(template, AspNetFormTemplateVariables, values);
			if (!_canActivateByCode || _hideManualButton)
			{
				template = TemplateFormatter.RemoveSection(template, "ACTIVATE MANUALLY");
			}
			if (_servers.Count == 0)
			{
				template = TemplateFormatter.RemoveSection(template, "ACTIVATE ONLINE");
			}
			if (context.SupportInfo.Website == null)
			{
				template = TemplateFormatter.RemoveSection(template, "SUPPORT WEB");
			}
			if (context.SupportInfo.Email == null)
			{
				template = TemplateFormatter.RemoveSection(template, "SUPPORT EMAIL");
			}
			if (context.SupportInfo.Phone == null)
			{
				template = TemplateFormatter.RemoveSection(template, "SUPPORT PHONE");
			}
			if (text2 == null)
			{
				template = TemplateFormatter.RemoveSection(template, "ERROR");
			}
			if (flag && !httpContext.Response.Buffer)
			{
				context.ReportError("E_AspNetNotBuffered", this);
				return false;
			}
			if (!flag)
			{
				template2 = TemplateFormatter.Format(template2, AspNetFormTemplateVariables, values);
				if (!context.RenderHtml(template2, base.LimitId + ".Header", control, HtmlLocation.Top, false))
				{
					return false;
				}
			}
			else
			{
				context.Items[base.LimitId + ".AttachedToWeb"] = this;
			}
			return context.RenderHtml(template, base.LimitId, control, HtmlLocation.Bottom, flag);
		}

		public string[] MakeTemplateVariables(SecureLicenseContext context, string url, string error, bool sample)
		{
			if (context == null)
			{
				context = SecureLicenseContext.CreateResolveContext(base.License);
			}
			StringBuilder stringBuilder = new StringBuilder();
			bool flag = false;
			foreach (ActivationProfile item in (IEnumerable)_profiles)
			{
				if (item.MachineType == MachineType.Any || item.MachineType == MachineProfile.MachineType)
				{
					if (item.IsEmpty && !flag)
					{
						flag = true;
						stringBuilder.AppendFormat("<option value=\"{0}\" selected> {1}", item.ReferenceId, item.ToString());
					}
					else
					{
						stringBuilder.AppendFormat("<option value=\"{0}\"> {1}", item.ReferenceId, item.ToString());
					}
				}
			}
			if (stringBuilder.Length == 0)
			{
				stringBuilder.AppendFormat("<option value=\"{0}\"> {1}", 0, SR.GetString("M_ProfileNone"));
			}
			string text = sample ? "http://www.company.com" : context.SupportInfo.Website;
			if (text != null)
			{
				int num = text.IndexOf("://");
				if (num == -1)
				{
					text = "http://" + text;
				}
				text = Toolbox.ResolveUrl(text, context).ToString();
			}
			SuperFormTheme superFormTheme = base.License.LicenseFile.SuperFormTheme;
			if (superFormTheme == null)
			{
				superFormTheme = new SuperFormTheme(SuperFormTheme.DefaultBaseColor);
			}
			string text2 = null;
			text2 = ((_logoResource == null || (!_logoResource.StartsWith("http://") && !_logoResource.StartsWith("https://"))) ? superFormTheme.Images.GetValueUrl("WebIcon") : _logoResource);
			if (text2 == null)
			{
				text2 = "http://www.deploylx.com/WebIcon.jpg";
			}
			if (text2 != null)
			{
				text2 = string.Format("<img src=\"{0}\" >", Toolbox.ResolveUrl(text2, context));
			}
			Uri uri = Toolbox.ResolveUrl(superFormTheme.Images.GetValueUrl("WebSide", "Side"), context);
			Uri uri2 = Toolbox.ResolveUrl(superFormTheme.Images.GetValueUrl("WebPanel", "WebBase", "Panel", "Base"), context);
			return new string[41]
			{
				base.LimitId,
				base.License.LicenseId,
				SR.GetString("UI_ActivateTitle", context.SupportInfo.Product),
				SR.GetString("UI_ActivationNotice", sample ? "The Software" : context.SupportInfo.Product),
				SR.GetString("UI_ActivateOnlineTitle"),
				url,
				SR.GetString("UI_ActivateOnline").Replace("&", ""),
				SR.GetString("UI_ActivateManuallyTitle"),
				SR.GetString("UI_ActivateManuallyNotice", sample ? "The Software" : context.SupportInfo.Product),
				SR.GetString("UI_SerialNumber"),
				base.License.SerialNumber,
				SR.GetString("UI_MachineCode"),
				_profiles[0].MakeMachineProfile().Hash,
				SR.GetString("UI_MachineProfile").Replace("&", ""),
				stringBuilder.ToString(),
				SR.GetString("UI_Activate").Replace("&", ""),
				(_codeMask == null) ? "" : _codeMask.Replace("\\", "").Replace('C', 'X'),
				sample ? "You have XXXX days to complete activation." : MakeRequiredText(this, context),
				SR.GetString("UI_ActivationMachineWarning"),
				error,
				SR.GetString("UI_SupportInfo"),
				sample ? "(123) 456-7890" : context.SupportInfo.Phone,
				SR.GetString("UI_EmailSupport"),
				sample ? "support@company.com" : context.SupportInfo.Email,
				SR.GetString("UI_VisitSupportWebsite"),
				text,
				ImageEffects.ToRGBHex(superFormTheme.Colors["WebPanel", new string[4]
				{
					"WebBase",
					"Panel",
					"Back",
					"Base"
				}]),
				ImageEffects.ToRGBHex(superFormTheme.Colors["WebText", new string[1]
				{
					"Text"
				}]),
				ImageEffects.ToRGBHex(superFormTheme.Colors["WebLinkText", new string[3]
				{
					"WebText",
					"LinkText",
					"Text"
				}]),
				ImageEffects.ToRGBHex(superFormTheme.Colors["WebSide", new string[5]
				{
					"WebPanel",
					"WebBase",
					"Panel",
					"Back",
					"Base"
				}]),
				ImageEffects.ToRGBHex(superFormTheme.Colors["WebSideText", new string[2]
				{
					"WebText",
					"Text"
				}]),
				ImageEffects.ToRGBHex(superFormTheme.Colors["WebSideLinkText", new string[3]
				{
					"WebLinkText",
					"LinkText",
					"Text"
				}]),
				ImageEffects.ToRGBHex(superFormTheme.Colors["WebSideSplitterFade", new string[2]
				{
					"SideSplitterFade",
					"BaseHighlight"
				}]),
				ImageEffects.ToRGBHex(superFormTheme.Colors["WebSupportTitleText", new string[3]
				{
					"SupportTitleText",
					"HeaderText",
					"Text"
				}]),
				(uri == (Uri)null) ? "http://www.deploylx.com/webback.jpg" : uri.ToString(),
				(uri2 == (Uri)null) ? null : uri2.ToString(),
				text2,
				SR.GetString("UI_WebActivationHeaderNotice", sample ? "The Product" : context.SupportInfo.Product),
				ImageEffects.ToRGBHex(superFormTheme.Colors["WebHeaderColor", new string[5]
				{
					"WebPanel",
					"WebBase",
					"Panel",
					"Back",
					"Base"
				}]),
				ImageEffects.ToRGBHex(superFormTheme.Colors["WebHeaderTextColor", new string[2]
				{
					"WebText",
					"Text"
				}]),
				TemplateFormatter.CreateInstanceId(context)
			};
		}

		public ActivationProfileCollection GetProfilesForThisMachine()
		{
			ActivationProfileCollection activationProfileCollection = new ActivationProfileCollection();
			ActivationProfile activationProfile = null;
			int num = 2147483647;
			foreach (ActivationProfile item in (IEnumerable)Profiles)
			{
				if (item.MachineType == MachineType.Any || item.MachineType == MachineProfile.MachineType)
				{
					activationProfileCollection.Add(item);
					if (activationProfile == null)
					{
						activationProfile = item;
						if (!activationProfile.IsEmpty)
						{
							num = MachineProfile.CompareHash(item.Hash, item.MakeMachineProfile(), false);
						}
					}
					else if (!item.IsEmpty)
					{
						int num2 = MachineProfile.CompareHash(item.Hash, item.MakeMachineProfile(), false);
						if (num2 < num)
						{
							num = num2;
							activationProfile = item;
						}
					}
				}
			}
			if (activationProfile != null)
			{
				activationProfileCollection.Remove(activationProfile);
				activationProfileCollection.Insert(0, activationProfile);
			}
			return activationProfileCollection;
		}

		public bool HasStateLimits(string state)
		{
			if (state != null && state.Length != 0)
			{
				if (!_validationStates.Contains(state))
				{
					throw new SecureLicenseException("E_UnknownValidationState", state, Name);
				}
				if (base.License.Version == SecureLicense.v3_0)
				{
					string a;
					if ((a = state) != null && a == "Activated")
					{
						if (!_dontValidateChildrenWhenActivated)
						{
							return base.Limits.Count > 0;
						}
						goto IL_00e8;
					}
					return base.Limits.Count > 0;
				}
				foreach (Limit item in (IEnumerable)base.Limits)
				{
					StateLimit stateLimit = item as StateLimit;
					if (stateLimit != null && stateLimit.States.Contains(state))
					{
						return true;
					}
				}
				goto IL_00e8;
			}
			throw new ArgumentNullException("state");
			IL_00e8:
			return false;
		}

		public LimitCollection GetStateLimits(string state)
		{
			if (state != null && state.Length != 0)
			{
				if (!_validationStates.Contains(state))
				{
					throw new SecureLicenseException("E_UnknownValidationState", state, Name);
				}
				LimitCollection limitCollection = new LimitCollection();
				if (base.License.Version == SecureLicense.v3_0)
				{
					string a;
					if ((a = state) != null && a == "Activated")
					{
						if (!_dontValidateChildrenWhenActivated)
						{
							AddLimits(limitCollection, base.Limits);
						}
					}
					else
					{
						AddLimits(limitCollection, base.Limits);
					}
				}
				else
				{
					AddLimits(limitCollection, base.Limits.FindStateLimits(null));
					AddLimits(limitCollection, base.Limits.FindStateLimits(state));
				}
				return limitCollection;
			}
			throw new ArgumentNullException("state");
		}

		private void AddLimits(LimitCollection limits, IEnumerable collection)
		{
			foreach (Limit item in collection)
			{
				limits.Add(item);
			}
		}
	}
}
