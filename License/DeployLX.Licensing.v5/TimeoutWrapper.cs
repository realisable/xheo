using System;

namespace DeployLX.Licensing.v5
{
	public sealed class TimeoutWrapper
	{
		private DateTime _started;

		private int _timeout;

		private bool _debug;

		public bool Expired
		{
			get
			{
				if (_timeout != -1)
				{
					return RemainingTime == 0;
				}
				return false;
			}
		}

		public int RemainingTime
		{
			get
			{
				if (_timeout == -1)
				{
					return -1;
				}
				DateTime now = DateTime.Now;
				TimeSpan timeSpan = now - _started;
				int num = _timeout - (int)timeSpan.TotalMilliseconds;
				if (num < 0)
				{
					return 0;
				}
				return num;
			}
		}

		public TimeSpan RemainingTimeSpan
		{
			get
			{
				return TimeSpan.FromMilliseconds((double)RemainingTime);
			}
		}

		public TimeoutWrapper(int timeout, bool debug)
		{
			_timeout = timeout;
			_started = DateTime.Now;
			_debug = debug;
		}

		public TimeoutWrapper(int timeout)
			: this(timeout, false)
		{
		}

		public TimeoutWrapper(TimeSpan timeout)
			: this((int)timeout.TotalMilliseconds, false)
		{
		}

		public void Reset()
		{
			_started = DateTime.Now;
		}
	}
}
