﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace DeployLX.Licensing.v5
{
    public class SafeToolbox
    {
		private static int _isFipsEnabled;

        [ThreadStatic]
		private static int _threadIsWeb;

		private static int _isServiceRequest = -1;

		public static bool IsFipsEnabled
		{
			get
			{
				if (_isFipsEnabled == 0)
				{
					try
					{
						new RijndaelManaged();
						_isFipsEnabled = -1;
					}
					catch (InvalidOperationException)
					{
						_isFipsEnabled = 1;
					}
				}
				return _isFipsEnabled == 1;
			}
		}

		public static SymmetricAlgorithm GetAes(bool forceFips)
		{
			return Aes.Create(); // assembly.GetType("System.Security.Cryptography.AesCryptoServiceProvider");

			//return Activator.CreateInstance(_aesType) as SymmetricAlgorithm;
		}

		public static int FastParseInt32(string value)
		{
			if (value == null)
			{
				return 0;
			}
			return FastParseInt32(value, 0, value.Length);
		}

		public static int FastParseInt32(string value, int start, int length)
		{
			if (value == null)
			{
				return 0;
			}
			int num = 0;
			int i = start;
			bool flag = false;
			if (value[0] == '-')
			{
				i++;
				flag = true;
			}
			for (int num2 = start + length; i < num2; i++)
			{
				num = num * 10 + (value[i] - 48);
			}
			if (!flag)
			{
				return num;
			}
			return -num;
		}

		public static string XmlDecode(string value)
		{
			if (value != null && value.Length != 0)
			{
				return SecurityElement.Escape(value);
			}
			return value;
		}

		public static RegistryKey GetRegistryKey(string key, bool writeAccess)
		{
			Check.NotNull(key, "key");
			int num = key.StartsWith("registry:") ? 9 : 0;
			if (num == 0 && key.Length < 5)
			{
				goto IL_0039;
			}
			if (num == 9 && key.Length < 14)
			{
				goto IL_0039;
			}
			RegistryKey registryKey = null;
			bool flag = false;
			if (string.Compare(key, num, "HKLM", 0, 4, true, CultureInfo.InvariantCulture) == 0)
			{
				registryKey = Registry.LocalMachine;
			}
			else if (string.Compare(key, num, "HKCU", 0, 4, true, CultureInfo.InvariantCulture) == 0)
			{
				registryKey = Registry.CurrentUser;
			}
			else if (string.Compare(key, num, "HKCR", 0, 4, true, CultureInfo.InvariantCulture) == 0)
			{
				registryKey = Registry.ClassesRoot;
			}
			else if (string.Compare(key, num, "HK*U", 0, 4, true, CultureInfo.InvariantCulture) == 0)
			{
				registryKey = Registry.CurrentUser;
				flag = true;
			}
			else if (string.Compare(key, num, "HK*M", 0, 4, true, CultureInfo.InvariantCulture) == 0)
			{
				registryKey = Registry.LocalMachine;
				flag = true;
			}
			if (registryKey == null)
			{
				throw new ArgumentException(SR.GetString("E_InvalidRegistryKey", key), "code");
			}
			string name = key.Substring(num + 5);
			RegistryKey registryKey2 = registryKey.OpenSubKey(name, writeAccess);
			if (flag && registryKey2 == null)
			{
				registryKey2 = ((registryKey != Registry.CurrentUser) ? Registry.CurrentUser.OpenSubKey(name, writeAccess) : Registry.LocalMachine.OpenSubKey(name, writeAccess));
			}
			return registryKey2;
			IL_0039:
			throw new ArgumentException(SR.GetString("E_InvalidRegistryKey", key), "code");
		}

		[ComVisible(false)]
		public static string GetFullPath(string path)
		{
			if (AppDomain.CurrentDomain.SetupInformation.ConfigurationFile == null)
			{
				return null;
			}
			return Path.Combine(Path.GetDirectoryName(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile), path);
		}

		public static void GrantEveryoneAccess(string path)
		{
			if (path != null && path.Length != 0)
			{
				bool flag = path.StartsWith("reg::");
				try
				{
					Thread thread = new Thread((ThreadStart)delegate
					{
						try
						{
							SafeNativeMethods.LoadLibraryExW("NTMARTA.DLL", IntPtr.Zero, 0u);
							Marshal.GetLastWin32Error();
						}
						catch
						{
						}
					});
					thread.Start();
					thread.Join();
				}
				catch
				{
				}
				using (new Wow64Guard())
				{
					if (flag)
					{
						path = ((!path.StartsWith("reg::HKEY_CURRENT_USER")) ? ((!path.StartsWith("reg::HKCU")) ? ((!path.StartsWith("reg::HKEY_LOCAL_MACHINE")) ? ((!path.StartsWith("reg::HKLM")) ? ((!path.StartsWith("reg::HKEY_CLASSES_ROOT")) ? ((!path.StartsWith("reg::HKCR")) ? ((!path.StartsWith("reg::HKEY_USERS")) ? ((!path.StartsWith("reg::HKU")) ? path.Substring(5) : ("USERS" + path.Substring(8))) : path.Substring(10)) : ("CLASSES_ROOT" + path.Substring(9))) : path.Substring(9)) : ("MACHINE" + path.Substring(9))) : ("MACHINE" + path.Substring(24))) : ("CURRENT_USER" + path.Substring(9))) : path.Substring(10));
					}
					else if (!File.Exists(path) && !Directory.Exists(path))
					{
						goto end_IL_0057;
					}
					IntPtr zero = IntPtr.Zero;
					IntPtr zero2 = IntPtr.Zero;
					IntPtr zero3 = IntPtr.Zero;
					if (path.IndexOf('\0') > -1)
					{
						path = path.Replace("\0", "");
					}
					try
					{
						int namedSecurityInfo;
						if ((namedSecurityInfo = SafeNativeMethods.GetNamedSecurityInfo(path, (!flag) ? 1 : 4, 4, IntPtr.Zero, IntPtr.Zero, out zero, IntPtr.Zero, out zero2)) == 0)
						{
							SafeNativeMethods.EXPLICIT_ACCESS eXPLICIT_ACCESS = default(SafeNativeMethods.EXPLICIT_ACCESS);
							eXPLICIT_ACCESS.AccessPermissions = 2097151;
							eXPLICIT_ACCESS.AccessMode = 1;
							eXPLICIT_ACCESS.Inheritance = 0;
							if (!SafeNativeMethods.ConvertStringSidToSid("S-1-1-0", ref eXPLICIT_ACCESS.Trustee.Name))
							{
								Marshal.ThrowExceptionForHR(Marshal.GetHRForLastWin32Error());
							}
							eXPLICIT_ACCESS.Trustee.MultipleTrustee = IntPtr.Zero;
							eXPLICIT_ACCESS.Trustee.TrusteeForm = 0;
							eXPLICIT_ACCESS.Trustee.TrusteeType = 5;
							while (SafeNativeMethods.DeleteAce(zero, 0))
							{
							}
							if ((namedSecurityInfo = SafeNativeMethods.SetEntriesInAcl(1, ref eXPLICIT_ACCESS, zero, out zero3)) == 0)
							{
								if ((namedSecurityInfo = SafeNativeMethods.SetNamedSecurityInfo(path, (!flag) ? 1 : 4, 536870916u, IntPtr.Zero, IntPtr.Zero, zero3, IntPtr.Zero)) != 0)
								{
									throw new Win32Exception(namedSecurityInfo);
								}
								goto end_IL_01b2;
							}
							throw new Win32Exception(namedSecurityInfo);
						}
						throw new Win32Exception(namedSecurityInfo);
						end_IL_01b2:;
					}
					finally
					{
						if (zero2 != IntPtr.Zero)
						{
							Marshal.FreeHGlobal(zero2);
						}
						if (zero3 != IntPtr.Zero)
						{
							Marshal.FreeHGlobal(zero3);
						}
					}
					end_IL_0057:;
				}
			}
		}

		public static string FormatTime(long seconds, long max, bool includeUnit)
		{
			string text = null;
			if (max > 86400L)
			{
				text = "M_Days";
				seconds = (int)Math.Ceiling((double)((float)seconds / 86400f));
			}
			else if (max > 3600L)
			{
				text = "M_Hours";
				seconds = (int)Math.Ceiling((double)((float)seconds / 3600f));
			}
			else if (max > 60L)
			{
				text = "M_Minutes";
				seconds = (int)Math.Ceiling((double)((float)seconds / 60f));
			}
			else
			{
				text = "M_Seconds";
			}
			if (includeUnit)
			{
				if (seconds == 1L)
				{
					text = text.Substring(0, text.Length - 1);
				}
				return string.Format("{0} {1}", seconds, SR.GetString(text));
			}
			return seconds.ToString();
		}

		public static string FormatSortableDate(DateTime date)
		{
			return string.Format("{0:0000}-{1:00}-{2:00}T{3:00}:{4:00}:{5:00}", date.Year, date.Month, date.Day, date.Hour, date.Minute, date.Second);
		}

		public static DateTime FastParseSortableDate(string value)
		{
			Check.NotNull(value, "value");
			if (value.Length != 19)
			{
				throw new ArgumentException("Invalid date", "value");
			}
			return new DateTime(SafeToolbox.FastParseInt32(value, 0, 4), SafeToolbox.FastParseInt32(value, 5, 2), SafeToolbox.FastParseInt32(value, 8, 2), SafeToolbox.FastParseInt32(value, 11, 2), SafeToolbox.FastParseInt32(value, 14, 2), SafeToolbox.FastParseInt32(value, 17, 2), DateTimeKind.Utc);
		}

		public static string MakeEnumList(object value, string separator)
		{
			if (!(value is Enum))
			{
				throw new InvalidCastException("Must be enum.");
			}
			if (separator == null)
			{
				separator = ", ";
			}
			int num = 1;
			int num2 = (int)value;
			Type type = value.GetType();
			if (num2 == 0)
			{
				return Enum.GetName(type, value);
			}
			StringBuilder stringBuilder = new StringBuilder();
			while (num > 0)
			{
				int num3 = num & num2;
				if (num3 != 0)
				{
					if (stringBuilder.Length > 0)
					{
						stringBuilder.Append(separator);
					}
					stringBuilder.Append(Enum.GetName(type, num3));
				}
				num <<= 1;
			}
			return stringBuilder.ToString();
		}

		private static object _webUtilityType;

		public static string XmlEncode(string value)
		{
			if (value != null && value.Length != 0)
			{
				if (_webUtilityType == null)
				{
					try
					{
						_webUtilityType = (((object)Type.GetType("System.Net.WebUtility, System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089", false)) ?? ((object)"Missing"));
					}
					catch (FileLoadException)
					{
						_webUtilityType = "Missing";
					}
				}
				if (_webUtilityType as string == "Missing")
				{
					return SecurityElement.Escape(value);
				}
				using (StringWriter stringWriter = new StringWriter())
				{
					((Type)_webUtilityType).InvokeMember("HtmlEncode", BindingFlags.Static | BindingFlags.Public | BindingFlags.InvokeMethod, null, null, new object[2]
					{
						value,
						stringWriter
					});
					return stringWriter.ToString();
				}
			}
			return value;
		}


		//Realisable - This determines if running in Azure function. We could use other environment variables.
		public static bool IsAzureRequest
		{
			get
			{
				return System.Environment.GetEnvironmentVariable("APPSETTING_WEBSITE_SITE_NAME", EnvironmentVariableTarget.Process) != null;
			}
		}

		public static bool ShouldUseWebLogic
		{
			get
			{
				if (_threadIsWeb == 0)
				{
					try
					{
						if (HasContext())
						{
							_threadIsWeb = 1;
						}
					}
					catch
					{
					}
				}
				return _threadIsWeb == 1;
			}
		}

		public static bool IsWebRequest
		{
			get
			{
				if (ShouldUseWebLogic)
				{
					return true;
				}
				if (HttpContext.Current != null)
				{
					_threadIsWeb = 1;
					return true;
				}
				return false;
			}
		}

		public static bool IsServiceRequest
		{
			get
			{
				if (_isServiceRequest == -1)
				{
					_isServiceRequest = ((!Environment.UserInteractive && !IsWebRequest) ? 1 : 0);
				}
				return _isServiceRequest == 1;
			}
		}

		private static bool HasContext()
		{
			return HttpContext.Current != null;
		}
	}
}
