﻿using System;

namespace DeployLX.Licensing.v5
{
    public class LicenseHttpResponse
    {
        public byte[] LicenseXml { get; set; }
    }
}
