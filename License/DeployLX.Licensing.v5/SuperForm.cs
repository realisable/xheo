using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	public sealed class SuperForm : Form
	{
		private delegate void DisposeDelegate();

		private delegate void ShowPanelDelegate(SuperFormPanel panel, bool push);

		private delegate void ReturnDelegate(FormResult result);

		private delegate DialogResult ShowDialogWithEffectsDelegate(Form form);

		private delegate DialogResult MethodInvokerForm();

		private delegate void ShowControlsWithEffectsDelegate(TriBool show, params Control[] controls);

		private readonly Stack _themeStack = new Stack();

		private readonly BlankPanel _blankPanel;

		private readonly CrossFadeControl _fader;

		private SuperFormPanel _activePanel;

		private GradientPanel _supportPanel;

		private GradientPanel _supportLabelGradient;

		private ShadowLabel _supportLabel;

		private GradientPanel _headerPanel;

		private GradientPanel _headerPanelSplitter;

		private ShadowLabel _titleLabel;

		private ShadowLabel _subTitleLabel;

		private BufferedPanel _hostPanel;

		private BufferedPanel _clientPanel;

		private GradientPanel _bottomPanel;

		private GradientPanel _controlPanel;

		private SystemCursorPictureBox _logoImage;

		private SkinnedButton _helpIcon;

		private SizeF _scaleFactor;

		private System.Threading.Timer _themeTimer;

		private readonly SecureLicenseContext _context;

		private SuperFormTheme _theme = new SuperFormTheme(SuperFormTheme.DefaultBaseColor);

		private Font _mediumFont;

		private Font _fieldFont;

		private Font _fixedFont;

		private Font _titleFont;

		private Font _headerFont;

		private Font _boldFont;

		private DropShadow _lightShadow;

		private FormResult _formResult;

		private bool _activated;

		private SuperFormPanel _firstPanel;

		private bool _firstPush;

		private readonly ArrayList _hostedPanels = new ArrayList();

		private readonly Stack _panelStack = new Stack();

		private bool _noPushOnNext;

		private bool _closedByReturn;

		private readonly Stack _waitStack = new Stack();

		private FormResult _waitResult;

		private int _nextControlEdge = 8;

		private bool _headerSet;

		private bool _supportInitialized;

		private System.Windows.Forms.Timer _supportTimer;

		private bool _keepSupportVisible;

		private readonly List<Image> _scaledImages = new List<Image>();

		public SecureLicenseContext Context
		{
			get
			{
				return _context;
			}
		}

		public SuperFormTheme Theme
		{
			get
			{
				return _theme;
			}
			set
			{
				if (value == null)
				{
					value = new SuperFormTheme(SuperFormTheme.DefaultBaseColor);
				}
				if (_theme != value)
				{
					_theme.Changed -= _theme_Changed;
					_theme = value;
					_theme.Changed += _theme_Changed;
					UpdateFromTheme();
				}
			}
		}

		public Font MediumFont
		{
			get
			{
				if (_mediumFont == null)
				{
					_mediumFont = new Font(Font.FontFamily, Font.Size + 3f, Font.Style);
				}
				return _mediumFont;
			}
		}

		public Font FieldFont
		{
			get
			{
				if (_fieldFont == null)
				{
					_fieldFont = new Font(Font.FontFamily, Font.Size + 4f, Font.Style);
				}
				return _fieldFont;
			}
		}

		public Font FixedFont
		{
			get
			{
				if (_fixedFont == null)
				{
					_fixedFont = (GetInstalledFont(16f, FontStyle.Regular, "Consolas", "Lucida Console", "Courier New") ?? new Font(FontFamily.GenericMonospace, 16f, FontStyle.Regular));
				}
				return _fixedFont;
			}
		}

		public Font TitleFont
		{
			get
			{
				if (_titleFont == null)
				{
					_titleFont = GetInstalledFont(Font.Size + 6f, FontStyle.Bold, "Segoe UI Light", "Segoe UI", "Tahoma", Font.Name);
				}
				return _titleFont;
			}
		}

		public Font HeaderFont
		{
			get
			{
				if (_headerFont == null)
				{
					_headerFont = new Font(Font.FontFamily, Font.Size + 2f, Font.Style | FontStyle.Bold);
				}
				return _headerFont;
			}
		}

		public Font BoldFont
		{
			get
			{
				if (_boldFont == null)
				{
					_boldFont = new Font(Font, Font.Style | FontStyle.Bold);
				}
				return _boldFont;
			}
		}

		public DropShadow LightShadow
		{
			get
			{
				return _lightShadow ?? (_lightShadow = new DropShadow
				{
					Opacity = 19,
					OffsetX = 2,
					OffsetY = 2
				});
			}
		}

		protected override CreateParams CreateParams
		{
			get
			{
				CreateParams createParams = base.CreateParams;
				if (OSRecord.ThisMachine.Product == OSProduct.WindowsXP || OSRecord.ThisMachine.Product == OSProduct.Windows2003)
				{
					createParams.ClassStyle |= 131072;
				}
				return createParams;
			}
		}

		internal FormResult FormResult
		{
			get
			{
				return _formResult;
			}
		}

		public SuperFormPanel ActivePanel
		{
			get
			{
				return _activePanel;
			}
		}

		internal int LeftmostButtonEdge
		{
			get
			{
				if (_logoImage.Visible && _logoImage.Image != null)
				{
					return _logoImage.Right;
				}
				if (!_helpIcon.Visible)
				{
					return 6;
				}
				return _helpIcon.Right;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public SuperForm(SecureLicenseContext context)
		{
			_context = context;
			base.AutoScaleMode = AutoScaleMode.Dpi;
			Font = (GetInstalledFont(9f, FontStyle.Regular, "Segoe UI", "Tahoma") ?? Font);
			InitializeComponent();
			if (_scaleFactor == SizeF.Empty)
			{
				_scaleFactor = base.AutoScaleFactor;
			}
			base.AutoScaleMode = AutoScaleMode.Dpi;
			BackColor = SuperFormTheme.DefaultBaseColor;
			base.Icon = Images.Shield_ico;
			_fader = new CrossFadeControl();
			_fader.SetBounds(0, 0, _clientPanel.Width, _clientPanel.Height);
			_fader.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			_fader.Visible = false;
			_fader.Finished += _fader_Finished;
			base.Controls.Add(_fader);
			_blankPanel = new BlankPanel();
			_blankPanel._superForm = this;
			_activePanel = _blankPanel;
			_hostPanel.Controls.Add(_blankPanel);
			if (context != null && context.SupportInfo != null)
			{
				Text = context.SupportInfo.Product;
			}
			else
			{
				Text = null;
			}
			_theme.Changed += _theme_Changed;
			_helpIcon.Image = Images.Help_png;
			InitializeSupportPanel();
		}

		private void _theme_Changed(object sender, ChangeEventArgs e)
		{
			if (_themeTimer == null)
			{
				_themeTimer = new System.Threading.Timer(InvokeUpdateFromTheme, null, -1, -1);
			}
			_themeTimer.Change(100, -1);
		}

		private void InvokeUpdateFromTheme(object state)
		{
            UpdateFromTheme();
		}

		protected override void Dispose(bool disposing)
		{
			if (base.InvokeRequired)
			{
				if (!base.IsDisposed)
				{
                    
                    //DisposeDelegate d = ;

                    ////()

                    //// TODO: Work out issue with dispose delegate.
                    base.BeginInvoke(new DisposeDelegate(((Component)this).Dispose));
                }
            }
			else if (!base.IsDisposed)
			{
				if (!disposing)
				{
					base.Dispose(disposing);
				}
				foreach (IDisposable hostedPanel in _hostedPanels)
				{
					hostedPanel.Dispose();
				}
				if (_mediumFont != null)
				{
					_mediumFont.Dispose();
				}
				if (_headerFont != null)
				{
					_headerFont.Dispose();
				}
				if (_titleFont != null)
				{
					_titleFont.Dispose();
				}
				if (_fieldFont != null)
				{
					_fieldFont.Dispose();
				}
				if (_fixedFont != null)
				{
					_fixedFont.Dispose();
				}
				if (_boldFont != null)
				{
					_boldFont.Dispose();
				}
				foreach (Image scaledImage in _scaledImages)
				{
					scaledImage.Dispose();
				}
				base.DestroyHandle();
				base.Dispose(disposing);
			}
		}

		private void InitializeComponent()
		{
			_logoImage = new SystemCursorPictureBox();
			_clientPanel = new BufferedPanel();
			_supportPanel = new GradientPanel();
			_supportLabelGradient = new GradientPanel();
			_supportLabel = new ShadowLabel();
			_hostPanel = new BufferedPanel();
			_headerPanel = new GradientPanel();
			_subTitleLabel = new ShadowLabel();
			_headerPanelSplitter = new GradientPanel();
			_titleLabel = new ShadowLabel();
			_bottomPanel = new GradientPanel();
			_controlPanel = new GradientPanel();
			_helpIcon = new SkinnedButton();
			((ISupportInitialize)_logoImage).BeginInit();
			_clientPanel.SuspendLayout();
			_supportPanel.SuspendLayout();
			_supportLabelGradient.SuspendLayout();
			_headerPanel.SuspendLayout();
			_bottomPanel.SuspendLayout();
			_controlPanel.SuspendLayout();
			base.SuspendLayout();
			_logoImage.BackColor = Color.Transparent;
			_logoImage.Location = new Point(3, 3);
			_logoImage.Name = "_logoImage";
			_logoImage.Size = new Size(134, 27);
			_logoImage.TabIndex = 1;
			_logoImage.TabStop = false;
			_logoImage.Click += _logoImage_Click;
			_clientPanel.BackgroundImageLayout = ImageLayout.None;
			_clientPanel.Controls.Add(_supportPanel);
			_clientPanel.Controls.Add(_hostPanel);
			_clientPanel.Controls.Add(_headerPanel);
			_clientPanel.Controls.Add(_bottomPanel);
			_clientPanel.Dock = DockStyle.Fill;
			_clientPanel.Location = new Point(0, 0);
			_clientPanel.Name = "_clientPanel";
			_clientPanel.Size = new Size(684, 463);
			_clientPanel.TabIndex = 0;
			_supportPanel.Anchor = (AnchorStyles.Bottom | AnchorStyles.Left);
			_supportPanel.BackColor2 = Color.FromArgb(183, 213, 251);
			_supportPanel.BackgroundImageLayout = ImageLayout.None;
			_supportPanel.Controls.Add(_supportLabelGradient);
			_supportPanel.GradientAngle = 135f;
			_supportPanel.Location = new Point(0, 401);
			_supportPanel.Name = "_supportPanel";
			_supportPanel.Size = new Size(170, 19);
			_supportPanel.TabIndex = 5;
			_supportLabelGradient.BackColor2 = Color.FromArgb(55, 88, 148);
			_supportLabelGradient.BackgroundImageLayout = ImageLayout.None;
			_supportLabelGradient.Controls.Add(_supportLabel);
			_supportLabelGradient.GradientOffset = new Point(140, 0);
			_supportLabelGradient.GradientScale = new Point(25, 0);
			_supportLabelGradient.Location = new Point(0, 1);
			_supportLabelGradient.Name = "_supportLabelGradient";
			_supportLabelGradient.Size = new Size(170, 20);
			_supportLabelGradient.TabIndex = 1;
			_supportLabel.ForeColor = Color.White;
			_supportLabel.Location = new Point(7, 1);
			_supportLabel.Name = "_supportLabel";
			_supportLabel.Size = new Size(90, 15);
			_supportLabel.TabIndex = 1;
			_supportLabel.Text = "#UI_SupportInfo";
			_supportLabel.ThemeFont = ThemeFont.Bold;
			_hostPanel.BackgroundImageLayout = ImageLayout.None;
			_hostPanel.Dock = DockStyle.Fill;
			_hostPanel.Location = new Point(0, 59);
			_hostPanel.Name = "_hostPanel";
			_hostPanel.Size = new Size(684, 362);
			_hostPanel.TabIndex = 0;
			_headerPanel.BackColor = Color.FromArgb(1, 53, 103);
			_headerPanel.BackColor2 = Color.FromArgb(1, 36, 71);
			_headerPanel.BackgroundImageLayout = ImageLayout.None;
			_headerPanel.Controls.Add(_subTitleLabel);
			_headerPanel.Controls.Add(_headerPanelSplitter);
			_headerPanel.Controls.Add(_titleLabel);
			_headerPanel.Dock = DockStyle.Top;
			_headerPanel.GradientAngle = 90f;
			_headerPanel.GradientOffset = new Point(0, 10);
			_headerPanel.GradientStyle = GradientStyle.Reflected;
			_headerPanel.Location = new Point(0, 0);
			_headerPanel.Name = "_headerPanel";
			_headerPanel.Size = new Size(684, 59);
			_headerPanel.TabIndex = 1;
			_headerPanel.Visible = false;
			_subTitleLabel.ForeColor = Color.White;
			_subTitleLabel.Location = new Point(18, 29);
			_subTitleLabel.Name = "_subTitleLabel";
			_subTitleLabel.Size = new Size(660, 23);
			_subTitleLabel.TabIndex = 1;
			_headerPanelSplitter.BackColor = Color.FromArgb(64, 105, 174);
			_headerPanelSplitter.BackColor2 = Color.FromArgb(183, 213, 251);
			_headerPanelSplitter.BackgroundImageLayout = ImageLayout.None;
			_headerPanelSplitter.Dock = DockStyle.Bottom;
			_headerPanelSplitter.GradientStyle = GradientStyle.Reflected;
			_headerPanelSplitter.Location = new Point(0, 57);
			_headerPanelSplitter.Name = "_headerPanelSplitter";
			_headerPanelSplitter.Size = new Size(684, 2);
			_headerPanelSplitter.TabIndex = 0;
			_titleLabel.ForeColor = Color.White;
			_titleLabel.Location = new Point(9, 8);
			_titleLabel.Name = "_titleLabel";
			_titleLabel.Size = new Size(669, 30);
			_titleLabel.TabIndex = 0;
			_titleLabel.ThemeFont = ThemeFont.Header;
			_bottomPanel.BackColor = Color.White;
			_bottomPanel.BackColor2 = Color.FromArgb(204, 204, 220);
			_bottomPanel.BackgroundImageLayout = ImageLayout.None;
			_bottomPanel.Controls.Add(_controlPanel);
			_bottomPanel.Dock = DockStyle.Bottom;
			_bottomPanel.GradientAngle = 90f;
			_bottomPanel.Location = new Point(0, 421);
			_bottomPanel.Name = "_bottomPanel";
			_bottomPanel.Size = new Size(684, 42);
			_bottomPanel.TabIndex = 0;
			_controlPanel.Anchor = (AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right);
			_controlPanel.BackColor = Color.FromArgb(204, 204, 220);
			_controlPanel.BackColor2 = Color.White;
			_controlPanel.BackgroundImageLayout = ImageLayout.None;
			_controlPanel.Controls.Add(_helpIcon);
			_controlPanel.GradientAngle = -90f;
			_controlPanel.Location = new Point(0, 5);
			_controlPanel.Name = "_controlPanel";
			_controlPanel.Size = new Size(684, 32);
			_controlPanel.TabIndex = 0;
			_helpIcon.AcceptsFocus = false;
			_helpIcon.FlatStyle = FlatStyle.Flat;
			_helpIcon.Location = new Point(6, 4);
			_helpIcon.Name = "_helpIcon";
			_helpIcon.ShouldDrawFocus = false;
			_helpIcon.Size = new Size(24, 24);
			_helpIcon.TabIndex = 0;
			_helpIcon.UseVisualStyleBackColor = true;
			_helpIcon.Visible = false;
			_helpIcon.Click += _helpIcon_Click;
			base.AutoScaleDimensions = new SizeF(96f, 96f);
			base.AutoScaleMode = AutoScaleMode.Dpi;
			AutoSize = true;
			base.ClientSize = new Size(684, 463);
			base.Controls.Add(_clientPanel);
			base.FormBorderStyle = FormBorderStyle.FixedDialog;
			base.KeyPreview = true;
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "SuperForm";
			base.StartPosition = FormStartPosition.CenterScreen;
			base.KeyDown += SuperForm_KeyDown;
			base.KeyUp += SuperForm_KeyUp;
			((ISupportInitialize)_logoImage).EndInit();
			_clientPanel.ResumeLayout(false);
			_supportPanel.ResumeLayout(false);
			_supportLabelGradient.ResumeLayout(false);
			_headerPanel.ResumeLayout(false);
			_bottomPanel.ResumeLayout(false);
			_controlPanel.ResumeLayout(false);
			base.ResumeLayout(false);
		}

		internal static Font GetInstalledFont(float size, FontStyle style, params string[] families)
		{
			int num = 0;
			Font font;
			while (true)
			{
				if (num < families.Length)
				{
					string text = families[num];
					font = new Font(text, size, style);
					if (string.Compare(font.Name, text, StringComparison.OrdinalIgnoreCase) == 0)
					{
						break;
					}
					font.Dispose();
					num++;
					continue;
				}
				return null;
			}
			return font;
		}

		protected override void DestroyHandle()
		{
			_activated = false;
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
		}

		protected override void OnLayout(LayoutEventArgs levent)
		{
			base.OnLayout(levent);
		}

		protected override void OnActivated(EventArgs e)
		{
			base.OnActivated(e);
			if (!_activated)
			{
				_activated = true;
				base.Activate();
			}
			if (_activePanel != null)
			{
				_activePanel.FormActivated();
			}
		}

		protected override void OnClosing(CancelEventArgs e)
		{
			base.OnClosing(e);
			if (_activePanel != null && !_activePanel.CanClose)
			{
				e.Cancel = true;
			}
			else if (!_closedByReturn)
			{
				_panelStack.Clear();
				Return((FormResult)524294);
			}
		}

		protected override void SetVisibleCore(bool value)
		{
			if (_context.SupportInfo.UseFormEffects)
			{
				if (!value)
				{
					try
					{
						MethodInfo method = typeof(Control).GetMethod("SetState", BindingFlags.Instance | BindingFlags.NonPublic);
						method.Invoke(this, new object[2]
						{
							2,
							false
						});
						return;
					}
					catch (Exception ex)
					{
						MessageBoxEx.ShowException(ex);
					}
				}
				else
				{
					base.Activate();
					if (_firstPanel != null)
					{
						base.SetVisibleCore(value);
						ShowPanel(_firstPanel, _firstPush);
						base.StartPosition = FormStartPosition.Manual;
						return;
					}
				}
			}
			base.SetVisibleCore(value);
		}

		protected override void OnClosed(EventArgs e)
		{
			base.OnClosed(e);
			if (!_closedByReturn)
			{
				_panelStack.Clear();
				_formResult = FormResult.Closed;
			}
			ShowPanel(null, false);
		}

		private void _logoImage_Click(object sender, EventArgs e)
		{
			Uri uri = _logoImage.Tag as Uri;
			if (uri != (Uri)null)
			{
				SharedToolbox.OpenUrl(uri.ToString(), true);
			}
		}

		public void ShowPreviewPanel(SuperFormPanel panel)
		{
			ShowPanel(panel, false);
		}

		internal void ShowPanel(SuperFormPanel panel, bool push)
		{
			if (!base.Visible && (_context == null || _context.SupportInfo.UseFormEffects))
			{
				_firstPanel = panel;
				_firstPush = push;
			}
			else
			{
				_firstPanel = null;
				using (Toolbox.ThreadBoost(ThreadPriority.Highest))
				{
					Thread.CurrentThread.Priority = ThreadPriority.Highest;
					Bitmap bitmap = null;
					Bitmap bitmap2 = null;
					if (base.InvokeRequired)
					{
						base.Invoke(new ShowPanelDelegate(ShowPanel), panel, push);
					}
					else
					{
						if (_noPushOnNext)
						{
							push = false;
							_noPushOnNext = false;
						}
						bool num = _context.SupportInfo.UseFormEffects && !Toolbox.IsTerminalServices;
						bool flag = num;
						if (num)
						{
							bitmap = GetClientBitmap();
						}
						if (_controlPanel.ContainsFocus)
						{
							base.SelectNextControl(_controlPanel, true, true, true, true);
						}
						if (push && _hostPanel.Controls.Count > 0 && !(_hostPanel.Controls[0] is BlankPanel))
						{
							_panelStack.Push(_hostPanel.Controls[0]);
						}
						if (_hostPanel.ContainsFocus)
						{
							base.SelectNextControl(_hostPanel, true, true, true, true);
						}
						_headerSet = false;
						if (flag && bitmap != null)
						{
							_fader.Fade(bitmap, null, 0);
						}
						if (push)
						{
							_themeStack.Push(_theme);
							_theme.MakeReadOnly();
							if (_themeStack.Count > 1)
							{
								object[] array = _themeStack.ToArray();
								Theme = (array[array.Length - 1] as SuperFormTheme);
							}
						}
						else if (_themeStack.Count > 0)
						{
							SuperFormTheme superFormTheme = _themeStack.Pop() as SuperFormTheme;
							if (superFormTheme != _theme)
							{
								Theme = superFormTheme;
							}
						}
						base.AcceptButton = null;
						base.CancelButton = null;
						if (panel != null)
						{
							_activePanel = panel;
							ClearBottomControls();
							panel._superForm = this;
							if (!_hostedPanels.Contains(panel))
							{
								_hostedPanels.Add(panel);
								panel.LoadPanel(Context);
							}
							panel.Dock = DockStyle.Fill;
							panel.InitializePanel();
							foreach (Control control in _controlPanel.Controls)
							{
								if (control is Button)
								{
									((Button)control).DialogResult = DialogResult.None;
								}
							}
							UpdateFromTheme();
							SR.LocalizeControl(panel, true);
							KeepSupportVisible(panel.KeepSupportVisible);
							if (!panel.KeepSupportVisible)
							{
								HideSupportPanel();
							}
						}
						if (_titleLabel.Text.Length == 0 && _subTitleLabel.Text.Length == 0)
						{
							_headerPanel.Visible = false;
						}
						else
						{
							_headerPanel.Visible = _headerSet;
						}
						if (panel != null)
						{
							if (flag && bitmap != null)
							{
								_hostPanel.Controls.Clear();
								_hostPanel.Controls.Add(panel);
								panel.PanelShown(false);
								if (panel._firstInitialization)
								{
									panel.Scale(_scaleFactor);
								}
								bitmap2 = GetClientBitmap();
								_fader.Fade(bitmap, bitmap2, 65);
							}
							else
							{
								_hostPanel.Controls.Clear();
								_hostPanel.Controls.Add(panel);
								panel.PanelShown(false);
								if (panel._firstInitialization)
								{
									panel.Scale(_scaleFactor);
								}
								panel.PanelShown(true);
								panel._firstInitialization = false;
							}
							_closedByReturn = false;
						}
						else
						{
							_noPushOnNext = true;
							_activePanel = null;
						}
					}
				}
			}
		}

		private void _fader_Finished(object sender, EventArgs e)
		{
			if (_activePanel != null)
			{
				_activePanel.PanelShown(true);
				_activePanel._firstInitialization = false;
			}
		}

		private Bitmap GetClientBitmap()
		{
			using (Toolbox.ThreadBoost(ThreadPriority.Highest))
			{
				Bitmap controlAsBitmap = Toolbox.GetControlAsBitmap(_clientPanel);
				if (controlAsBitmap == null)
				{
					return null;
				}
				if (_supportPanel.Visible)
				{
					using (Bitmap image = Toolbox.GetControlAsBitmap(_supportPanel))
					{
						using (Graphics graphics = Graphics.FromImage(controlAsBitmap))
						{
							graphics.DrawImage(image, _supportPanel.Location);
						}
					}
				}
				return controlAsBitmap;
			}
		}

		internal FormResult GetResult()
		{
			if (_panelStack.Count > 0)
			{
				SuperFormPanel superFormPanel = _panelStack.Peek() as SuperFormPanel;
				superFormPanel._callBack = HandleSubPanelResult;
				_waitStack.Push(superFormPanel);
				int count = _waitStack.Count;
				while (true)
				{
					if (_waitStack.Count != count)
					{
						break;
					}
					if (base.IsDisposed)
					{
						break;
					}
					if (_closedByReturn)
					{
						break;
					}
					Application.DoEvents();
					Thread.Sleep(10);
				}
				return _waitResult;
			}
			return _formResult;
		}

		private void HandleSubPanelResult(FormResult result)
		{
			_waitStack.Pop();
			_waitResult = result;
		}

		internal void Return(FormResult result)
		{
			if (base.InvokeRequired)
			{
				if (base.IsDisposed)
				{
					_closedByReturn = true;
					base.DialogResult = DialogResult.OK;
				}   
				else
				{
					base.Invoke(new ReturnDelegate(Return), result);
				}
			}
			else
			{
				bool flag = (result & FormResult.DontReshowPanel) != FormResult.Unknown;
				result &= (FormResult)(-524289);
				_formResult = result;
				if (_panelStack.Count > 0)
				{
					SuperFormPanel superFormPanel = _panelStack.Pop() as SuperFormPanel;
					if (superFormPanel._callBack != null)
					{
						PanelResultHandler callBack = superFormPanel._callBack;
						superFormPanel._callBack = null;
						object obj = null;
						if (_panelStack.Count > 0)
						{
							obj = _panelStack.Peek();
						}
						callBack(result);
						if ((obj != null || _panelStack.Count > 0) && _panelStack.Count > 0 && _panelStack.Peek() != obj)
						{
							_panelStack.Pop();
							_panelStack.Push(superFormPanel);
							return;
						}
					}
					if (result == FormResult.Retry)
					{
						ShowPanel(null, false);
						_closedByReturn = true;
						base.DialogResult = DialogResult.OK;
					}
					else
					{
						if (superFormPanel._skipPopOnSuccess)
						{
							flag |= (result == FormResult.Success);
						}
						if (!flag)
						{
							ShowPanel(superFormPanel, false);
						}
					}
				}
				else
				{
					ShowPanel(null, false);
					_closedByReturn = true;
					base.DialogResult = DialogResult.OK;
				}
			}
		}

		internal void AddBottomControl(Control ctrl, int buffer)
		{
			ShadowLabel shadowLabel = ctrl as ShadowLabel;
			if (shadowLabel != null)
			{
				shadowLabel.DropShadow.Visible = false;
			}
			_controlPanel.Controls.Add(ctrl);
			ctrl.Scale(_scaleFactor);
			ctrl.Left = _controlPanel.Right - _nextControlEdge - ctrl.Width;
			if (buffer == -1)
			{
				buffer = 8;
			}
			buffer = (int)Math.Round((double)((float)buffer * _scaleFactor.Width), 0);
			_nextControlEdge += buffer + ctrl.Width;
			if (ctrl.Top == 0)
			{
				ctrl.Top = (_controlPanel.Height - ctrl.Height) / 2;
			}
			if (ctrl.TabStop)
			{
				ctrl.BringToFront();
				ctrl.TabIndex = 0;
			}
			ctrl.Anchor = (AnchorStyles.Top | AnchorStyles.Right);
		}

		internal void ClearBottomControls()
		{
			_controlPanel.Controls.Clear();
			_controlPanel.Controls.Add(_logoImage);
			_helpIcon.Visible = _context.ShowHelpButton;
			if (_context.ShowHelpButton)
			{
				_controlPanel.Controls.Add(_helpIcon);
				_logoImage.Left = _helpIcon.Right + 8 + _logoImage.Top;
			}
			else
			{
				_logoImage.Left = _logoImage.Top;
			}
			_nextControlEdge = 8;
		}

		internal void SetHeader(string title, string subTitle)
		{
			_headerSet = true;
			if (base.Visible && _headerPanel.Visible != (title != null) && !_fader.IsFading && _activePanel != null && !_activePanel.FirstInitialization)
			{
				ShowControlsWithEffects(title != null, _headerPanel, _titleLabel, _subTitleLabel);
			}
			else
			{
				_headerPanel.Visible = (title != null);
			}
			_titleLabel.Text = SR.GetString(title);
			_subTitleLabel.Text = SR.GetString(subTitle);
			if (subTitle != null)
			{
				_titleLabel.Height = 18;
				_subTitleLabel.Visible = true;
			}
			else
			{
				_subTitleLabel.Visible = false;
				_titleLabel.Height = 30;
			}
		}

		internal DialogResult ShowDialogWithEffects(Form form)
		{
			Check.NotNull(form, "form");
			if (_context.SupportInfo.UseFormEffects && !Toolbox.IsTerminalServices)
			{
				if (base.InvokeRequired && !form.IsHandleCreated)
				{
					return (DialogResult)base.Invoke(new ShowDialogWithEffectsDelegate(ShowDialogWithEffects), form);
				}
				using (Bitmap fromBitmap = GetClientBitmap())
				{
					base.BeginInvoke(new MethodInvoker(BlurClient));
					DialogResult result = (!form.InvokeRequired) ? form.ShowDialog(base.InvokeRequired ? null : this) : ((DialogResult)form.Invoke(new MethodInvokerForm(form.ShowDialog)));
					_fader.Fade(fromBitmap, null, 0);
					if (_fader.InvokeRequired)
					{
						_fader.Invoke(new MethodInvoker(_fader.Update));
						_fader.Invoke(new MethodInvoker(_fader.Hide));
					}
					else
					{
						_fader.Update();
						_fader.Hide();
					}
					return result;
				}
			}
			if (form.InvokeRequired)
			{
				return (DialogResult)form.Invoke(new MethodInvokerForm(form.ShowDialog));
			}
			if (base.InvokeRequired)
			{
				if (form.IsHandleCreated)
				{
					return (DialogResult)base.Invoke(new ShowDialogWithEffectsDelegate(ShowDialogWithEffects), form);
				}
				return form.ShowDialog(this);
			}
			return form.ShowDialog(this);
		}

		internal void ShowControlsWithEffects(TriBool show, params Control[] controls)
		{
			if (base.InvokeRequired)
			{
				base.Invoke(new ShowControlsWithEffectsDelegate(ShowControlsWithEffects), show, controls);
			}
			else
			{
				bool flag = _context.SupportInfo.UseFormEffects && !Toolbox.IsTerminalServices;
				Bitmap bitmap = null;
				Bitmap bitmap2 = null;
				if (flag)
				{
					bitmap = GetClientBitmap();
					if (bitmap != null)
					{
						_fader.Fade(bitmap, null, 0);
					}
				}
				if (show == (TriBool)TriBoolValue.Default)
				{
					foreach (Control control in controls)
					{
						control.Visible = !control.Visible;
					}
				}
				else
				{
					bool visible = show.Resolve(true);
					foreach (Control control2 in controls)
					{
						control2.Visible = visible;
					}
				}
				if (flag)
				{
					bitmap2 = GetClientBitmap();
					_fader.Fade(bitmap, bitmap2, 100);
				}
			}
		}

		internal void BlurClient()
		{
			if (_context.SupportInfo.UseFormEffects)
			{
				Thread.Sleep(10);
				Application.DoEvents();
				using (Toolbox.ThreadBoost(ThreadPriority.Highest))
				{
					Bitmap clientBitmap = GetClientBitmap();
					Bitmap bitmap = ImageEffects.Convolution(clientBitmap, ImageEffects.CreateGaussianMatrix(6f));
					using (Graphics graphics = Graphics.FromImage(bitmap))
					{
						using (SolidBrush brush = new SolidBrush(Color.FromArgb(64, Theme.Colors["BaseDarkDark"])))
						{
							graphics.FillRectangle(brush, 0, 0, bitmap.Width, bitmap.Height);
						}
					}
					_fader.Fade(clientBitmap, bitmap, 50, true);
				}
			}
		}

		private void InitializeSupportPanel()
		{
			if (!_supportInitialized)
			{
				_supportInitialized = true;
				if (base.InvokeRequired)
				{
					base.Invoke(new MethodInvoker(InitializeSupportPanel));
				}
				else
				{
					bool flag = false;
					_supportPanel.Height += 8;
					if (_context.SupportInfo.Phone != null)
					{
						ShadowLabel shadowLabel = new ShadowLabel();
						shadowLabel.Text = _context.SupportInfo.Phone;
						if (shadowLabel.Text.Length < 20)
						{
							shadowLabel.Font = MediumFont;
							AddSupportControl(shadowLabel, null, 8);
						}
						else
						{
							AddSupportControl(shadowLabel, null, 0);
						}
						flag = true;
					}
					if (_context.SupportInfo.Email != null)
					{
						ShadowLabel shadowLabel2 = new ShadowLabel();
						shadowLabel2.Text = SR.GetString("UI_EmailSupport");
						shadowLabel2.Url = string.Format("mailto:{0}", _context.SupportInfo.Email);
						AddSupportControl(shadowLabel2, _context.SupportInfo.Email, 0);
						flag = true;
					}
					if (_context.SupportInfo.Website != null)
					{
						ShadowLabel shadowLabel3 = new ShadowLabel();
						string text = _context.SupportInfo.Website;
						shadowLabel3.Text = SR.GetString("UI_VisitSupportWebsite");
						shadowLabel3.Url = text;
						if (text.StartsWith("http://"))
						{
							text = text.Substring(7);
						}
						AddSupportControl(shadowLabel3, text, 0);
						flag = true;
					}
					if (flag)
					{
						SR.LocalizeControl(_supportPanel, true);
						_supportPanel.Top = _bottomPanel.Top - _supportPanel.Height;
						_supportPanel.Tag = _supportPanel.Bounds;
						HideSupportPanel();
						_supportPanel.MouseEnter += _supportPanel_MouseEnter;
						_supportLabel.MouseEnter += _supportPanel_MouseEnter;
						_supportLabelGradient.MouseEnter += _supportPanel_MouseEnter;
						_supportPanel.MouseLeave += _supportPanel_MouseLeave;
						_supportLabel.MouseLeave += _supportPanel_MouseLeave;
						_supportLabelGradient.MouseLeave += _supportPanel_MouseLeave;
						_supportTimer = new System.Windows.Forms.Timer();
						_supportTimer.Interval = 100;
						_supportTimer.Tick += _supportTimer_Tick;
					}
					else
					{
						_supportPanel.Visible = false;
					}
				}
			}
		}

		internal void KeepSupportVisible(bool keepVisible)
		{
			_keepSupportVisible = keepVisible;
			if (keepVisible)
			{
				ShowSupportPanel();
			}
		}

		private void AddSupportControl(Control ctl, string subText, int addPadding)
		{
			ctl.BackColor = Color.Transparent;
			ctl.Left = 8;
			ctl.Top = _supportPanel.Height;
			ctl.Width = _supportPanel.Width - 16;
			_supportPanel.Height += ctl.Height + addPadding;
			_supportPanel.Controls.Add(ctl);
			ctl.MouseEnter += _supportPanel_MouseEnter;
			ctl.MouseLeave += _supportPanel_MouseLeave;
			Label label = ctl as Label;
			if (label != null)
			{
				ShadowLabel shadowLabel = label as ShadowLabel;
				if (shadowLabel != null)
				{
					shadowLabel.LinkClicked += shadowLabel_LinkClicked;
					shadowLabel.DropShadow.Opacity = 32;
				}
			}
			if (subText != null)
			{
				Label label2 = new Label();
				label2.Text = subText;
				label2.Font = new Font(Font.FontFamily, 7.5f, FontStyle.Regular);
				label2.Left = 16;
				label2.Top = (int)((float)_supportPanel.Height - Font.Size + 3f);
				label2.Width = _supportPanel.Width - 24;
				label2.BackColor = Color.Transparent;
				label2.ForeColor = Color.White;
				_supportPanel.Height = label2.Bottom;
				_supportPanel.Controls.Add(label2);
				label2.BringToFront();
				label2.MouseEnter += _supportPanel_MouseEnter;
				label2.MouseLeave += _supportPanel_MouseLeave;
			}
		}

		private void HideSupportPanel()
		{
			_supportPanel.SetBounds(0, _bottomPanel.Top - _supportLabelGradient.Height - 1, _supportPanel.Width, _supportLabelGradient.Height + 1);
		}

		private void ShowSupportPanel()
		{
			if (_supportPanel.Tag != null)
			{
				Rectangle rectangle = (Rectangle)_supportPanel.Tag;
				_supportPanel.SetBounds(rectangle.X, _bottomPanel.Top - rectangle.Height, rectangle.Width, rectangle.Height);
				_supportTimer.Stop();
				_supportTimer.Interval = 100;
			}
		}

		private void _supportPanel_MouseEnter(object sender, EventArgs e)
		{
			ShowSupportPanel();
			_supportTimer.Stop();
		}

		private void _supportPanel_MouseLeave(object sender, EventArgs e)
		{
			if (!_keepSupportVisible)
			{
				_supportTimer.Start();
			}
		}

		private void _supportTimer_Tick(object sender, EventArgs e)
		{
			HideSupportPanel();
		}

		private void shadowLabel_LinkClicked(object sender, EventArgs e)
		{
			ShadowLabel shadowLabel = sender as ShadowLabel;
			SharedToolbox.OpenUrl(shadowLabel.Url, true);
		}

		private void UpdateFromTheme()
		{
			if (base.InvokeRequired)
			{
				base.Invoke(new MethodInvoker(UpdateFromTheme));
			}
			else
			{
				bool visible = true;
				if (_theme.Properties["UseTextDropShadows"] == "false")
				{
					visible = false;
				}
				Color color = _theme.Colors["TextShadowColor"];
				if (color == Color.Empty)
				{
					color = Color.Black;
				}
				base.Icon = _theme.FormIcon;
				_headerPanel.BackColor = _theme.Colors["Header", new string[1]
				{
					"BaseDark"
				}];
				_headerPanel.BackColor2 = _theme.Colors["HeaderFade", new string[1]
				{
					"BaseDarkDark"
				}];
				_supportLabel.Font = BoldFont;
				_supportLabel.DropShadow.Visible = visible;
				_supportLabel.DropShadow.Color = color;
				_titleLabel.Font = HeaderFont;
				_titleLabel.DropShadow.Visible = visible;
				_titleLabel.DropShadow.Color = color;
				_supportLabelGradient.BackColor = _theme.Colors["SupportLabel", new string[1]
				{
					"Base"
				}];
				_supportLabelGradient.BackColor2 = _theme.Colors["SupportLabelFade", new string[1]
				{
					"BaseDark"
				}];
				_supportPanel.BackColor = _theme.Colors["SupportPanel", new string[1]
				{
					"Base"
				}];
				_supportPanel.BackColor2 = _theme.Colors["SupportPanelFade", new string[1]
				{
					"BaseLightLight"
				}];
				_headerPanelSplitter.BackColor2 = _theme.Colors["HeaderSplitter", new string[1]
				{
					"BaseHighHighHigh"
				}];
				_headerPanelSplitter.BackColor = _theme.Colors["HeaderSplitterFade", new string[2]
				{
					"Header",
					"BaseDark"
				}];
				_titleLabel.ForeColor = _theme.Colors["TitleText", new string[2]
				{
					"HeaderText",
					"Text"
				}];
				_subTitleLabel.ForeColor = _theme.Colors["SubTitleText", new string[3]
				{
					"TitleText",
					"HeaderText",
					"Text"
				}];
				_subTitleLabel.DropShadow.Visible = visible;
				_subTitleLabel.DropShadow.Color = color;
				_supportLabel.ForeColor = _theme.Colors["SupportTitleText", new string[2]
				{
					"HeaderText",
					"Text"
				}];
				Color foreColor = _theme.Colors["SupportLabelText", new string[1]
				{
					"Text"
				}];
				foreach (Control control3 in _supportPanel.Controls)
				{
					if (control3 is Label && control3 != _supportLabel)
					{
						control3.ForeColor = foreColor;
						ShadowLabel shadowLabel = control3 as ShadowLabel;
						if (shadowLabel != null)
						{
							shadowLabel.DropShadow.Visible = visible;
							shadowLabel.DropShadow.Color = color;
						}
					}
				}
				BackColor = _theme.Colors["Panel", new string[1]
				{
					"Base"
				}];
				foreach (Control control4 in _controlPanel.Controls)
				{
					if (control4 is SkinnedButton)
					{
						((SkinnedButton)control4).Skin = _theme.ButtonSkin;
					}
					if (control4 is Label)
					{
						ThemeLabel themeLabel = control4 as ThemeLabel;
						if (themeLabel != null && themeLabel.ThemeColor != null)
						{
							control4.ForeColor = _theme.Colors["@", themeLabel.ThemeColor];
						}
						else if (themeLabel.Url == null)
						{
							control4.ForeColor = _theme.Colors["FooterText", new string[1]
							{
								"Text"
							}];
						}
						else
						{
							control4.ForeColor = _theme.Colors["FooterLinkText", new string[2]
							{
								"FooterText",
								"Text"
							}];
						}
						ShadowLabel shadowLabel2 = control4 as ShadowLabel;
						if (shadowLabel2 != null)
						{
							shadowLabel2.DropShadow.Visible = visible;
							shadowLabel2.DropShadow.Color = color;
						}
					}
				}
				_supportPanel.BackgroundImage = GetScaledImage(_theme.Images["Support"], _supportPanel.BackgroundImage);
				_supportLabelGradient.BackgroundImage = GetScaledImage(_theme.Images["SupportHeader"], _supportLabelGradient.BackgroundImage);
				Bitmap bitmap = _theme.Images["Header"];
				if (bitmap != null)
				{
					_headerPanelSplitter.Visible = false;
					_headerPanel.BackgroundImage = GetScaledImage(bitmap, _headerPanel.BackgroundImage);
				}
				else
				{
					_headerPanelSplitter.Visible = true;
				}
				Bitmap bitmap2 = _theme.Images["Footer"];
				if (bitmap2 != null)
				{
					_controlPanel.BackColor = Color.Transparent;
					_controlPanel.BackColor2 = Color.Empty;
					_bottomPanel.BackgroundImageLayout = ImageLayout.Stretch;
				}
				else
				{
					_bottomPanel.BackColor = _theme.Colors["Footer"];
					_bottomPanel.BackColor2 = _theme.Colors["FooterFade"];
					_controlPanel.BackColor = _bottomPanel.BackColor;
					_controlPanel.BackColor2 = _bottomPanel.BackColor2;
				}
				_bottomPanel.BackgroundImage = GetScaledImage(bitmap2, _bottomPanel.BackgroundImage);
				_logoImage.Image = GetScaledImage(_theme.Images["FooterLogo"], _logoImage.Image);
				if (_logoImage.Image != null)
				{
					_logoImage.Visible = true;
					if (_theme.Properties["companyurl"] != null)
					{
						_logoImage.Tag = Toolbox.ResolveUrl(_theme.Properties["companyurl"], _context);
					}
					else
					{
						_logoImage.Tag = null;
					}
					int num = Math.Min(27, _logoImage.Image.Height);
					int num2 = (_controlPanel.Height - num) / 2;
					_logoImage.SetBounds((_helpIcon.Visible ? (_helpIcon.Right + 8) : 0) + num2, num2, Math.Min(134, _logoImage.Image.Width), num);
				}
				else
				{
					_logoImage.Visible = false;
				}
				_logoImage.Cursor = ((_logoImage.Tag == null) ? Cursors.Default : Cursors.Hand);
				if (_activePanel != null)
				{
					_activePanel.OnThemeChanged(EventArgs.Empty);
				}
			}
		}

		internal Image GetScaledImage(Image src, Image current)
		{
			if (current == src)
			{
				return src;
			}
			if (!(_scaleFactor == SizeF.Empty) && (_scaleFactor.Width != 1f || _scaleFactor.Height != 1f))
			{
				if (current != null && _scaledImages.Contains(current))
				{
					_scaledImages.Remove(current);
					current.Dispose();
				}
				if (src == null)
				{
					return null;
				}
				Bitmap bitmap = new Bitmap((int)Math.Ceiling((double)((float)src.Width * _scaleFactor.Width)), (int)Math.Ceiling((double)((float)src.Height * _scaleFactor.Height)), PixelFormat.Format64bppPArgb);
				using (Graphics graphics = Graphics.FromImage(bitmap))
				{
					graphics.DrawImage(src, new RectangleF(0f, 0f, (float)bitmap.Width, (float)bitmap.Height), new RectangleF(0f, 0f, (float)src.Width, (float)src.Height), GraphicsUnit.Pixel);
				}
				_scaledImages.Add(bitmap);
				return bitmap;
			}
			return src;
		}

		private void SuperForm_KeyDown(object sender, KeyEventArgs e)
		{
			if (_activePanel != null)
			{
				_activePanel.HandleKeyDown(sender, e);
			}
			if (!e.Handled)
			{
				switch (e.KeyData)
				{
				case (Keys)131190:
					e.Handled = true;
					try
					{
						using (ProxyForm form = new ProxyForm(null, "#UI_ProxyNotice"))
						{
							ShowDialogWithEffects(form);
						}
					}
					catch (Exception ex2)
					{
						MessageBoxEx.ShowException(ex2);
					}
					break;
				case Keys.F8:
					e.Handled = true;
					try
					{
							ClipboardToolbox.SetClipboard(DataFormats.Text, Toolbox.MakeDiagnosticReportString("", _context.ValidationRecords, _context));
					}
					catch (Exception ex)
					{
						MessageBoxEx.ShowException(ex);
					}
					break;
				}
			}
		}

		private void SuperForm_KeyUp(object sender, KeyEventArgs e)
		{
		}

		private void _helpIcon_Click(object sender, EventArgs e)
		{
			if (_activePanel != null)
			{
				SecureLicenseManager.OnHelpRequested(new ValidationHelpEventArgs(_activePanel));
			}
		}
	}
}
