using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public sealed class LimitCollection : WithEventsCollection
	{
		private SecureLicense _license;

		[NonSerialized]
		private int _refCount;

		private readonly object _owner;

		public SecureLicense License
		{
			get
			{
				return _license;
			}
			set
			{
				Limit limit = _owner as Limit;
				if (limit != null && value != limit.License)
				{
					throw new SecureLicenseException("E_CannotChangeAssociatedLicense");
				}
				_license = value;
			}
		}

		public object Owner
		{
			get
			{
				return _owner;
			}
		}

		public Limit this[int index]
		{
			get
			{
				return base.List[index] as Limit;
			}
			set
			{
				base.List[index] = value;
			}
		}

		public Limit this[Type type]
		{
			get
			{
				return FindLimitByType(type, true);
			}
		}

		public Limit this[string limitId]
		{
			get
			{
				return FindLimitById(limitId);
			}
		}

		public LimitCollection(SecureLicense license, object owner)
		{
			_license = license;
			_owner = owner;
		}

		public LimitCollection()
		{
		}

		public void Add(Limit limit)
		{
			base.List.Add(limit);
		}

		public void Insert(int index, Limit limit)
		{
			base.List.Insert(index, limit);
		}

		public void Remove(Limit limit)
		{
			base.List.Remove(limit);
		}

		public void CopyTo(Limit[] array, int index)
		{
			base.List.CopyTo(array, index);
		}

		public int IndexOf(Limit limit)
		{
			return base.List.IndexOf(limit);
		}

		public bool Contains(Limit limit)
		{
			return base.List.Contains(limit);
		}

		public void DisposeLimits(bool disposing)
		{
			foreach (Limit item in (IEnumerable)this)
			{
				item.DisposeInternal(disposing);
			}
		}

		protected override void OnChanged(CollectionEventArgs e)
		{
			Limit limit = e.Element as Limit;
			if (e.Action == CollectionChangeAction.Add)
			{
				if (limit._parentCollection == null)
				{
					limit._parentCollection = this;
				}
				if (_license != null && limit.License != _license)
				{
					limit.License = _license;
				}
			}
			else if (e.Action == CollectionChangeAction.Remove && limit._parentCollection == this)
			{
				limit._parentCollection = null;
			}
			base.OnChanged(e);
		}

		internal ValidationResult Validate(SecureLicenseContext context)
		{
			foreach (Limit item in (IEnumerable)this)
			{
				context.WriteDiagnostic("Validating limit {0} [{1}]", item, item.LimitId);
				ValidationResult validationResult = item.Validate(context);
				item._wasValidated = true;
				context.WriteDiagnostic("\tLimit {0} [{1}] returned {2}", item, item.LimitId, validationResult);
				if (validationResult != ValidationResult.Valid)
				{
					return validationResult;
				}
			}
			return ValidationResult.Valid;
		}

		internal ValidationResult Granted(SecureLicenseContext context)
		{
			foreach (Limit item in (IEnumerable)this)
			{
				context.WriteDiagnostic("Granting limit {0} [{1}]", item, item.LimitId);
				ValidationResult validationResult = item.Granted(context);
				context.WriteDiagnostic("\tLimit {0} [{1}] returned {2}", item, item.LimitId, validationResult);
				if (validationResult != ValidationResult.Valid)
				{
					return validationResult;
				}
				item._wasValidated = true;
			}
			return ValidationResult.Valid;
		}

		internal void ResetValidation()
		{
			foreach (Limit item in (IEnumerable)this)
			{
				item._wasValidated = false;
				if (item.HasChildLimits)
				{
					item.Limits.ResetValidation();
				}
			}
		}

		internal PeekResult Peek(SecureLicenseContext context)
		{
			PeekResult peekResult = PeekResult.Valid;
			foreach (Limit item in (IEnumerable)this)
			{
				PeekResult peekResult2 = item.Peek(context);
				if (peekResult2 < peekResult)
				{
					peekResult = peekResult2;
					if (peekResult2 != 0)
					{
						continue;
					}
					return peekResult2;
				}
			}
			return peekResult;
		}

		internal void AddRef()
		{
			Interlocked.Increment(ref _refCount);
		}

		internal bool ReleaseRef()
		{
			int num = Interlocked.Decrement(ref _refCount);
			return num <= 0;
		}

		public Limit[] FindLimitsByType(Type limitType, bool includeDerived)
		{
			return FindLimitsByType(limitType, includeDerived, false);
		}

		public Limit[] FindLimitsByType(Type limitType, bool includeDerived, bool filterByState)
		{
			ArrayList arrayList = new ArrayList();
			AddLimitsByType(arrayList, this, limitType, includeDerived, filterByState);
			object obj = arrayList.ToArray(typeof(Limit));
			return obj as Limit[];
		}

		private static void AddLimitsByType(ArrayList list, LimitCollection limits, Type limitType, bool includeDerived, bool filterbyState)
		{
			foreach (Limit item in (IEnumerable)limits)
			{
				if (includeDerived)
				{
					if (limitType.IsAssignableFrom(item.GetType()))
					{
						list.Add(item);
					}
				}
				else if (item.GetType() == limitType)
				{
					list.Add(item);
				}
			}
			foreach (Limit item2 in (IEnumerable)limits)
			{
				if (item2.HasChildLimits)
				{
					if (filterbyState && item2.State != null)
					{
						AddLimitsByType(list, item2.Limits.GetStateLimits(item2.State), limitType, includeDerived, filterbyState);
					}
					else
					{
						AddLimitsByType(list, item2.Limits, limitType, includeDerived, filterbyState);
					}
				}
			}
		}

		public Limit FindLimitByType(Type limitType, bool includeDerived)
		{
			return FindLimitByType(limitType, includeDerived, false);
		}

		public Limit FindLimitByType(Type limitType, bool includeDerived, bool filterByState)
		{
			return FindLimitByType(limitType, includeDerived, filterByState, this);
		}

		private Limit FindLimitByType(Type limitType, bool includeDerived, bool filterByState, LimitCollection limits)
		{
			foreach (Limit item in (IEnumerable)limits)
			{
				if (includeDerived)
				{
					if (!limitType.IsAssignableFrom(item.GetType()))
					{
						continue;
					}
					return item;
				}
				if (limitType == item.GetType())
				{
					return item;
				}
			}
			foreach (Limit item2 in (IEnumerable)limits)
			{
				if (item2.HasChildLimits)
				{
					if (filterByState && item2.State != null)
					{
						Limit limit3 = FindLimitByType(limitType, includeDerived, filterByState, item2.Limits.GetStateLimits(item2.State));
						if (limit3 == null)
						{
							continue;
						}
						return limit3;
					}
					Limit limit4 = FindLimitByType(limitType, includeDerived, filterByState, item2.Limits);
					if (limit4 == null)
					{
						continue;
					}
					return limit4;
				}
			}
			return null;
		}

		public Limit FindLimitById(string limitId)
		{
			if (limitId != null && limitId.Length > 0 && limitId[0] == '#')
			{
				Limit limit = Limit.CreateLimitFromName(limitId.Substring(1));
				if (limit == null)
				{
					return null;
				}
				return FindLimitByType(limit.GetType(), true);
			}
			Limit limit2 = FindLimitById(limitId, this);
			if (limit2 != null)
			{
				return limit2;
			}
			IEnumerable<Limit> enumerable = Find((Limit x) => x.Nickname == limitId);
			IEnumerator<Limit> enumerator = enumerable.GetEnumerator();
			if (enumerator.MoveNext())
			{
				return enumerator.Current;
			}
			return null;
		}

		private static Limit FindLimitById(string limitId, LimitCollection limits)
		{
			foreach (Limit item in (IEnumerable)limits)
			{
				if (string.Compare(limitId, item.LimitId, true) == 0)
				{
					return item;
				}
			}
			foreach (Limit item2 in (IEnumerable)limits)
			{
				if (item2.HasChildLimits)
				{
					Limit limit3 = FindLimitById(limitId, item2.Limits);
					if (limit3 == null)
					{
						continue;
					}
					return limit3;
				}
			}
			return null;
		}

		public Limit FindLimitByOrdinal(int ordinal)
		{
			int num = 0;
			return FindLimitByOrdinal(ordinal, this, ref num);
		}

		private static Limit FindLimitByOrdinal(int ordinal, LimitCollection limits, ref int currentOrdinal)
		{
			foreach (Limit item in (IEnumerable)limits)
			{
				if (currentOrdinal == ordinal)
				{
					return item;
				}
				currentOrdinal++;
				if (item.HasChildLimits)
				{
					Limit limit2 = FindLimitByOrdinal(ordinal, item.Limits, ref currentOrdinal);
					if (limit2 == null)
					{
						continue;
					}
					return limit2;
				}
			}
			return null;
		}

		public int CalculateOrdinal(Limit limit)
		{
			Check.NotNull(limit, "limit");
			int num = 0;
			return CalculateOrdinal(limit, this, ref num);
		}

		private static int CalculateOrdinal(Limit searchLimit, LimitCollection limits, ref int currentOrdinal)
		{
			foreach (Limit item in (IEnumerable)limits)
			{
				if (item == searchLimit)
				{
					return currentOrdinal;
				}
				currentOrdinal++;
				if (item.HasChildLimits)
				{
					int num = CalculateOrdinal(item, item.Limits, ref currentOrdinal);
					if (num <= -1)
					{
						continue;
					}
					return num;
				}
			}
			return -1;
		}

		public IList<Limit> FindStateLimits(string state)
		{
			List<Limit> list = new List<Limit>();
			foreach (Limit item in (IEnumerable)this)
			{
				StateLimit stateLimit = item as StateLimit;
				if (stateLimit == null)
				{
					if (state == null)
					{
						list.Add(item);
					}
				}
				else if (stateLimit.States.Contains(state))
				{
					list.Add(stateLimit);
				}
			}
			return list;
		}

		public bool HasStateLimits(string state)
		{
			if (state != null && state.Length != 0)
			{
				Limit limit = Owner as Limit;
				if (limit != null)
				{
					if (limit.ValidationStates == null || limit.ValidationStates.Count == 0)
					{
						return false;
					}
					if (!limit.ValidationStates.Contains(state))
					{
						throw new SecureLicenseException("E_UnknownValidationState", state, limit.Name);
					}
				}
				foreach (Limit item in (IEnumerable)this)
				{
					StateLimit stateLimit = item as StateLimit;
					if (stateLimit != null && stateLimit.States.Contains(state))
					{
						return true;
					}
				}
				return false;
			}
			throw new ArgumentNullException("state");
		}

		private void AddLimits(LimitCollection limits, IEnumerable collection)
		{
			foreach (Limit item in collection)
			{
				limits.Add(item);
			}
		}

		public LimitCollection GetStateLimits(string state)
		{
			if (state != null && state.Length != 0)
			{
				Limit limit = Owner as Limit;
				if (limit != null)
				{
					if (limit.ValidationStates == null || limit.ValidationStates.Count == 0)
					{
						return this;
					}
					if (!limit.ValidationStates.Contains(state))
					{
						throw new SecureLicenseException("E_UnknownValidationState", state, limit.Name);
					}
				}
				LimitCollection limitCollection = new LimitCollection();
				AddLimits(limitCollection, FindStateLimits(null));
				AddLimits(limitCollection, FindStateLimits(state));
				return limitCollection;
			}
			throw new ArgumentNullException("state");
		}

		public Limit[] FindExtendableLimits(bool canExtendOnly)
		{
			if (Count == 0)
			{
				return new Limit[0];
			}
			ArrayList arrayList = new ArrayList();
			AddLimitsByType(arrayList, this, typeof(IExtendableLimit), true, false);
			if (arrayList.Count == 0)
			{
				return new Limit[0];
			}
			if (canExtendOnly)
			{
				int num = 0;
				while (num < arrayList.Count)
				{
					if (!((IExtendableLimit)arrayList[num]).CanExtend)
					{
						arrayList.RemoveAt(num);
					}
					else
					{
						num++;
					}
				}
			}
			return arrayList.ToArray(typeof(Limit)) as Limit[];
		}

		public Limit[] FindExtendableLimits()
		{
			return FindExtendableLimits(false);
		}

		public IEnumerable<Limit> Find(Predicate<Limit> predicate)
		{
			if (predicate != null)
			{
				foreach (Limit item in (IEnumerable)this)
				{
					if (predicate(item))
					{
						yield return item;
					}
					if (item.HasChildLimits)
					{
						foreach (Limit item2 in item.Limits.Find(predicate))
						{
							yield return item2;
						}
					}
				}
			}
		}
	}
}
