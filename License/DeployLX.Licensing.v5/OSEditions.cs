using System;

namespace DeployLX.Licensing.v5
{
	[Flags]
	public enum OSEditions
	{
		NotSet = 0x0,
		Workstation = 0x1,
		Server = 0x2,
		TabletPC = 0x4,
		MediaCenter = 0x8,
		DomainController = 0x10,
		ThirtyTwoBit = 0x20000,
		SixtyFourBit = 0x10000,
		Home = 0x100000,
		Professional = 0x200000
	}
}
