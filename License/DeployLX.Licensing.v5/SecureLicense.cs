using Microsoft.Win32;
using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.IO.IsolatedStorage;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security;
using System.Security.Cryptography;
using System.Security.Permissions;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web;
//using System.Windows.Forms;
using System.Xml;

namespace DeployLX.Licensing.v5
{
    [Serializable]
    [ComVisible(true)]
    public sealed class SecureLicense : License, ISerializable, IChange
    {
        private sealed class StateEntry
        {
            public Hashtable SessionState = new Hashtable();

            public DateTime LastUsed = DateTime.UtcNow;
        }

        private const int IvSize = 16;

        private string _encryptedSource;

        private bool _modified = true;

        private bool _needsSaveOnValid;

        internal ArrayList _pendingContexts;

        private byte[] _parsedSerialData;

        private bool _pendingSecureData;

        private bool _dontPersistToStorage;

        internal bool _isCached;

        [EditorBrowsable(EditorBrowsableState.Never)]
        public static readonly Version v3_0 = new Version(3, 0);

        [EditorBrowsable(EditorBrowsableState.Never)]
        public static readonly Version v4_0 = new Version(4, 0);

        [EditorBrowsable(EditorBrowsableState.Never)]
        public static readonly Version v5_0 = new Version(5, 0);

        [EditorBrowsable(EditorBrowsableState.Never)]
        public static readonly Version[] ReleaseVersions = new Version[3]
        {
            v3_0,
            v4_0,
            v5_0
        };

        public static readonly Version CurrentVersion = v5_0;

        private ChangeEventHandler _changed;

        private ChangeEventHandler _bubbleHandler;

        private EventHandler _domainEventHandler;

        private string _keyName;

        private string _signature;

        private Version _version = CurrentVersion;

        private string _comments;

        internal LicenseFile _licenseFile;

        private string _licenseId = Guid.NewGuid().ToString("N");

        private LimitCollection _limits;

        private StringCollection _classes;

        private LicenseValuesDictionary _values;

        private LicenseValuesDictionary _metaValues;

        private LicenseValuesDictionary _editorState = new LicenseValuesDictionary();

        private LicenseValuesDictionary _registrationInfo;

        private LicenseResourceCollection _resources;

        private string _eulaAddress;

        private string _type;

        private AdminRunRequirement _requireAdminOnFirstRun = AdminRunRequirement.ApplicationsAndServices;

        private bool _requireAdmin;

        private bool _isAdmin;

        private bool _demandAuthenticationForWeb = true;

        private bool _disallowImpersonationForWeb = true;

        private SecureStorageRequirement _secureStorageRequirement = SecureStorageRequirement.Required;

        private SerialNumberInfo _serialNumberInfo;

        private bool _canUnlockBySerial;

        private bool _hardFailureIfFormShown = true;

        private bool _isStillEncrypted;

        private bool _isTemplate;

        private StateEntry _entry;

        private static Hashtable _states = new Hashtable();

        private bool _isReadOnly;

        private long _wasReadOnly;

        private bool _wasUnlockedThisTime;

        private TimeLimit _trialTime;

        private bool _isEditing;

        private bool _isValidating;

        private static readonly byte[] _sigCheck = new byte[6]
        {
            60,
            82,
            111,
            111,
            116,
            62
        };

        private bool _reloading;

        private Thread _flushThread;

        private Hashtable _secureState;

        private static Hashtable _secureStates = new Hashtable();

        private Hashtable _machineSecureState;

        private Hashtable _userSecureState;

        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public string KeyName
        {
            get
            {
                object keyName;
                if (LicenseFile != null)
                {
                    keyName = LicenseFile.KeyName;
                    if (keyName == null)
                    {
                        return _keyName;
                    }
                }
                else
                {
                    keyName = _keyName;
                }
                return (string)keyName;
            }
        }

        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public string Signature
        {
            get
            {
                return _signature;
            }
            set
            {
                if (_signature != value)
                {
                    _signature = value;
                    OnChanged("Signature");
                    if (_signature != null)
                    {
                        OnChanged("IsNew");
                    }
                }
            }
        }

        public Version Version
        {
            get
            {
                return _version;
            }
            set
            {
                AssertNotReadOnly();
                if (!(value < v3_0) && !(value > CurrentVersion))
                {
                    if (_version != value)
                    {
                        _version = value;
                        OnChanged("Version");
                    }
                    return;
                }
                throw new SecureLicenseException("E_InvalidVersion", CurrentVersion);
            }
        }

        public string Comments
        {
            get
            {
                return _comments;
            }
            set
            {
                AssertNotReadOnly();
                if (_comments != value)
                {
                    _comments = value;
                    OnChanged("Comments");
                }
            }
        }

        [Browsable(false)]
        public LicenseFile LicenseFile
        {
            get
            {
                return _licenseFile;
            }
        }

        public string Username
        {
            get
            {
                return ((StringDictionary)RegistrationInfo)["Name"];
            }
            set
            {
                ((StringDictionary)RegistrationInfo)["Name"] = value;
            }
        }

        public string Organization
        {
            get
            {
                return ((StringDictionary)RegistrationInfo)["Organization"];
            }
            set
            {
                ((StringDictionary)RegistrationInfo)["Organization"] = value;
            }
        }

        public string LicenseId
        {
            get
            {
                return _licenseId;
            }
            set
            {
                AssertNotReadOnly();
                if (_licenseId != value)
                {
                    if (value == null)
                    {
                        throw new ArgumentNullException("LicenseId", SR.GetString("E_PropertyCannotBeNull", "LicenseId"));
                    }
                    _licenseId = value;
                    OnChanged("LicenseId");
                }
            }
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public override string LicenseKey
        {
            get
            {
                if (_licenseFile == null)
                {
                    return ToXmlString();
                }
                return _licenseFile.ToXmlString();
            }
        }

        public LimitCollection Limits
        {
            get
            {
                AssertNotEncrypted();
                return _limits;
            }
        }

        [Obsolete("Use FeatureLimit instead.")]
        public StringCollection Classes
        {
            get
            {
                return _classes;
            }
        }

        public LicenseValuesDictionary Values
        {
            get
            {
                AssertNotEncrypted();
                return _values;
            }
        }

        public LicenseValuesDictionary MetaValues
        {
            get
            {
                return _metaValues;
            }
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public LicenseValuesDictionary EditorState
        {
            get
            {
                return _editorState;
            }
        }

        public LicenseValuesDictionary RegistrationInfo
        {
            get
            {
                return _registrationInfo;
            }
        }

        public LicenseResourceCollection Resources
        {
            get
            {
                AssertNotEncrypted();
                return _resources;
            }
        }

        public string EulaAddress
        {
            get
            {
                return _eulaAddress;
            }
            set
            {
                AssertNotReadOnly();
                if (_eulaAddress != value)
                {
                    SharedToolbox.ValidateUrl(value, true);
                    _eulaAddress = value;
                    OnChanged("EulaAddress");
                }
            }
        }

        public string Type
        {
            get
            {
                AssertNotEncrypted();
                return _type;
            }
            set
            {
                AssertNotReadOnly();
                if (_type != value)
                {
                    _type = value;
                    OnChanged("Type");
                    OnChanged("Edition");
                }
            }
        }

        public string Edition
        {
            get
            {
                return Type;
            }
            set
            {
                Type = value;
            }
        }

        public AdminRunRequirement RequireAdminOnFirstRun
        {
            get
            {
                return _requireAdminOnFirstRun;
            }
            set
            {
                AssertNotReadOnly();
                if (_requireAdminOnFirstRun != value)
                {
                    _requireAdminOnFirstRun = value;
                    OnChanged("RequiredAdminOnFirstRun");
                }
                _requireAdminOnFirstRun = value;
            }
        }

        public bool DemandAuthenticationForWeb
        {
            get
            {
                return _demandAuthenticationForWeb;
            }
            set
            {
                AssertNotReadOnly();
                if (_demandAuthenticationForWeb != value)
                {
                    _demandAuthenticationForWeb = value;
                    OnChanged("DemandAuthenticationForWeb");
                }
            }
        }

        public bool DisallowImpersonationForWeb
        {
            get
            {
                return _disallowImpersonationForWeb;
            }
            set
            {
                AssertNotReadOnly();
                if (_disallowImpersonationForWeb != value)
                {
                    _disallowImpersonationForWeb = value;
                    OnChanged("DisallowImpersonationForWeb");
                }
            }
        }

        public SecureStorageRequirement SecureStorageRequirement
        {
            get
            {
                AssertNotEncrypted();
                return _secureStorageRequirement;
            }
            set
            {
                AssertNotReadOnly();
                if (_secureStorageRequirement != value)
                {
                    _secureStorageRequirement = value;
                    OnChanged("SecureStorageRequirement");
                }
            }
        }

        public SerialNumberInfo SerialNumberInfo
        {
            get
            {
                AssertNotEncrypted();
                return _serialNumberInfo;
            }
        }

        public string SerialNumber
        {
            get
            {
                AssertNotEncrypted();
                return _serialNumberInfo.SerialNumber;
            }
            set
            {
                _serialNumberInfo.SerialNumber = value;
            }
        }

        public bool CanUnlockBySerial
        {
            get
            {
                AssertNotEncrypted();
                return _canUnlockBySerial;
            }
            set
            {
                AssertNotReadOnly();
                if (_canUnlockBySerial != value)
                {
                    _canUnlockBySerial = value;
                    OnChanged("UnlockBySerial");
                }
            }
        }

        public bool HardFailureIfFormShown
        {
            get
            {
                return _hardFailureIfFormShown;
            }
            set
            {
                AssertNotReadOnly();
                if (_hardFailureIfFormShown != value)
                {
                    _hardFailureIfFormShown = value;
                    OnChanged("HardFailureIfFormShown");
                }
            }
        }

        public bool IsStillEncrypted
        {
            get
            {
                return _isStillEncrypted;
            }
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public bool IsTemplate
        {
            get
            {
                return _isTemplate;
            }
            set
            {
                AssertNotReadOnly();
                if (_isTemplate != value)
                {
                    _isTemplate = value;
                    OnChanged("IsTemplate");
                }
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Hashtable RuntimeState
        {
            get
            {
                if (_entry == null)
                {
                    StateEntry stateEntry = _states[_licenseId] as StateEntry;
                    if (stateEntry == null)
                    {
                        stateEntry = new StateEntry();
                        _states[_licenseId] = stateEntry;
                        if (_states.Count > 20)
                        {
                            int num = 60;
                            try
                            {
                                if (SafeToolbox.IsWebRequest && HttpContext.Current.Session.Timeout > num)
                                {
                                    num = HttpContext.Current.Session.Timeout;
                                }
                            }
                            catch
                            {
                            }
                            ArrayList arrayList = new ArrayList();
                            IDictionaryEnumerator enumerator = _states.GetEnumerator();
                            try
                            {
                                while (enumerator.MoveNext())
                                {
                                    DictionaryEntry dictionaryEntry = (DictionaryEntry)enumerator.Current;
                                    if (((StateEntry)dictionaryEntry.Value).LastUsed.AddMinutes((double)num) < DateTime.UtcNow)
                                    {
                                        arrayList.Add(dictionaryEntry.Key);
                                    }
                                }
                            }
                            finally
                            {
                                IDisposable disposable = enumerator as IDisposable;
                                if (disposable != null)
                                {
                                    disposable.Dispose();
                                }
                            }
                            foreach (object item in arrayList)
                            {
                                _states.Remove(item);
                            }
                        }
                    }
                    _entry = stateEntry;
                }
                _entry.LastUsed = DateTime.UtcNow;
                if (!Check.CalledByType(typeof(Limit)) && !Check.CalledByThisType())
                {
                    _signature = null;
                    return null;
                }
                return _entry.SessionState;
            }
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public bool IsEmbedded
        {
            get
            {
                if (_licenseFile != null)
                {
                    return _licenseFile.IsEmbedded;
                }
                return false;
            }
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public bool IsReadOnly
        {
            get
            {
                return _isReadOnly;
            }
        }

        public bool IsDisposed
        {
            get;
            private set;
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public bool IsNew
        {
            get
            {
                return _signature == null;
            }
        }

        public bool IsTrial
        {
            get
            {
                return Limits.FindLimitByType(typeof(TrialLimit), true) != null;
            }
        }

        public bool IsUpgrade
        {
            get
            {
                return Limits.FindLimitByType(typeof(UpgradeLimit), true) != null;
            }
        }

        public bool IsUnlocked
        {
            get
            {
                AssertNotEncrypted();
                if (CanUnlockBySerial && SerialNumber != null)
                {
                    return SerialNumber != SerialNumberInfo.Prefix;
                }
                return false;
            }
        }

        public bool WasUnlockedThisTime
        {
            get
            {
                if (IsUnlocked)
                {
                    return _wasUnlockedThisTime;
                }
                return false;
            }
        }

        public bool IsActivated
        {
            get
            {
                Limit[] array = Limits.FindLimitsByType(typeof(ActivationLimit), true);
                if (array != null && array.Length != 0)
                {
                    Limit[] array2 = array;
                    int num = 0;
                    while (true)
                    {
                        if (num < array2.Length)
                        {
                            ActivationLimit activationLimit = (ActivationLimit)array2[num];
                            if (activationLimit.HasActivated)
                            {
                                break;
                            }
                            num++;
                            continue;
                        }
                        return false;
                    }
                    return true;
                }
                return false;
            }
        }

        public bool IsActivation
        {
            get
            {
                return Limits[typeof(ActivationLimit)] != null;
            }
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool SignedWithTrial
        {
            get
            {
                if (_trialTime == null)
                {
                    Limit[] array = Limits.FindLimitsByType(typeof(TimeLimit), true);
                    foreach (Limit limit in array)
                    {
                        if (limit is TimeLimit && limit.LimitId.StartsWith("DeployLXAutoTrialTimeLimit"))
                        {
                            _trialTime = (limit as TimeLimit);
                        }
                    }
                }
                return _trialTime != null;
            }
        }

        public bool IsEditing
        {
            get
            {
                if (!_isEditing)
                {
                    return _isTemplate;
                }
                return true;
            }
            set
            {
                _isEditing = value;
            }
        }

        event ChangeEventHandler IChange.Changed
        {
            add
            {
                lock (this)
                {
                    _changed = (ChangeEventHandler)Delegate.Combine(_changed, value);
                }
            }
            remove
            {
                lock (this)
                {
                    _changed = (ChangeEventHandler)Delegate.Remove(_changed, value);
                }
            }
        }

        private void OnChanged(object sender, string property, object item, CollectionChangeAction action)
        {
            if (_changed != null)
            {
                ChangeEventArgs changeEventArgs = new ChangeEventArgs(property, this, item, action);
                changeEventArgs.BubbleStack.Push(sender);
                OnChanged(changeEventArgs);
            }
        }

        private void OnChanged(string property)
        {
            if (_changed != null)
            {
                ChangeEventArgs e = new ChangeEventArgs(property, this);
                OnChanged(e);
            }
        }

        private void OnChanged(ChangeEventArgs e)
        {
            if (!_reloading)
            {
                if (!_isValidating && (_pendingContexts == null || _pendingContexts.Count <= 0))
                {
                    switch (e.Name)
                    {
                        case "MetaValues":
                        case "RegistrationInfo":
                            _needsSaveOnValid = true;
                            break;
                        default:
                            _modified = true;
                            _encryptedSource = null;
                            break;
                        case "EditorState":
                        case "Signature":
                        case "IsNew":
                        case "KeyName":
                            break;
                    }
                }
                else
                {
                    _modified = true;
                    _encryptedSource = null;
                    _needsSaveOnValid = true;
                }
                if (_changed != null)
                {
                    _changed(this, e);
                }
            }
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public void ForceEncryptedReWrite()
        {
            _encryptedSource = null;
        }

        public SecureLicense()
        {
            InitLicense();
            _domainEventHandler = CurrentDomain_DomainUnload;
            AppDomain.CurrentDomain.DomainUnload += _domainEventHandler;
            AppDomain.CurrentDomain.ProcessExit += _domainEventHandler;
            //Application.ApplicationExit += _domainEventHandler;
        }

        private void InitLicense()
        {
            _limits = new LimitCollection(this, this);
            _limits.Changed += _limits_Changed;
            _values = new LicenseValuesDictionary();
            _values.Changed += _values_Changed;
            _metaValues = new LicenseValuesDictionary();
            _metaValues.Changed += _metaValues_Changed;
            _registrationInfo = new LicenseValuesDictionary();
            _registrationInfo.Changed += _registrationInfo_Changed;
            _classes = new StringCollection();
            _classes.Changed += _classes_Changed;
            _resources = new LicenseResourceCollection();
            _resources.Changed += _resources_Changed;
            _serialNumberInfo = new SerialNumberInfo(this);
            ((IChange)_serialNumberInfo).Changed += SerialNumberInfo_Changed;
        }

        private void SerialNumberInfo_Changed(object sender, ChangeEventArgs e)
        {
            e.BubbleStack.Push(sender);
            OnChanged(e);
        }

        private void _registrationInfo_Changed(object sender, CollectionEventArgs e)
        {
            OnChanged(sender, "RegistrationInfo", e.Element, e.Action);
        }

        private void _limits_Changed(object sender, CollectionEventArgs e)
        {
            AssertNotReadOnly();
            OnChanged(sender, "Limits", e.Element, e.Action);
            AttachChangeBubbler(e);
        }

        private void BubbleChanged(object sender, ChangeEventArgs e)
        {
            e.BubbleStack.Push(sender);
            OnChanged(e);
        }

        private void _metaValues_Changed(object sender, CollectionEventArgs e)
        {
            OnChanged(sender, "MetaValues", e.Element, e.Action);
        }

        private void _values_Changed(object sender, CollectionEventArgs e)
        {
            OnChanged(sender, "Values", e.Element, e.Action);
        }

        private void _classes_Changed(object sender, CollectionEventArgs e)
        {
            OnChanged(sender, "Classes", e.Element, e.Action);
        }

        private void _resources_Changed(object sender, CollectionEventArgs e)
        {
            AssertNotReadOnly();
            OnChanged(sender, "Resources", e.Element, e.Action);
            AttachChangeBubbler(e);
        }

        public override void Dispose()
        {
            if (!_pendingSecureData && !_needsSaveOnValid)
            {
                AppDomain.CurrentDomain.DomainUnload -= _domainEventHandler;
                AppDomain.CurrentDomain.ProcessExit -= _domainEventHandler;
                //Application.ApplicationExit -= _domainEventHandler;
            }
            else
            {
                FlushSecureData(false);
            }
            lock (SecureLicenseManager.SyncRoot)
            {
                bool flag;
                if (!(flag = _limits.ReleaseRef()) && SecureLicenseManager.ShouldPerformFinalCleanup)
                {
                    while (!flag)
                    {
                        flag = _limits.ReleaseRef();
                    }
                }
                if (flag)
                {
                    IsDisposed = true;
                    _isReadOnly = false;
                    _signature = null;
                    foreach (Limit item in (IEnumerable)_limits)
                    {
                        item.Dispose();
                    }
                    _limits.Clear();
                    GC.SuppressFinalize(this);
                }
            }
        }

        private void CurrentDomain_DomainUnload(object sender, EventArgs e)
        {
            AppDomain.CurrentDomain.DomainUnload -= _domainEventHandler;
            AppDomain.CurrentDomain.ProcessExit -= _domainEventHandler;
            //Application.ApplicationExit -= _domainEventHandler;
            lock (SecureLicenseManager.SyncRoot)
            {
                if (_flushThread != null && _flushThread.ThreadState == System.Threading.ThreadState.WaitSleepJoin)
                {
                    _flushThread.Abort();
                }
            }
            if (!_pendingSecureData && !_needsSaveOnValid)
            {
                return;
            }
            FlushSecureData(false);
        }

        internal ValidationResult Validate(SecureLicenseContext context)
        {
            AssertNotEncrypted();
            _isValidating = true;
            string text = null;
            bool serialExternal = SerialNumberInfo.SerialExternal;
            bool serialExternal2 = false;
            try
            {
                _limits.ResetValidation();
                using (ImpHandle impHandle = StartAdminBlock(context))
                {
                    if (impHandle == null)
                    {
                        SecureLicenseContext.WriteDiagnosticToContext("Could not acquire admin context.");
                        context.ReportError("E_AdminRequiredFirstRun", this);
                        return ValidationResult.Invalid;
                    }
                    if (_limits.License == null)
                    {
                        _limits.License = this;
                    }
                    text = _serialNumberInfo.SerialNumber;
                    ValidationResult validationResult;
                    if (_canUnlockBySerial)
                    {
                        if (context.RequestInfo.ShouldGetNewSerialNumber || _serialNumberInfo.SerialNumber == null || _serialNumberInfo.SerialExternal)
                        {
                            string text2 = null;
                            string[] serialNumbers = context.RequestInfo.SerialNumbers;
                            if (serialNumbers != null)
                            {
                                string[] array = serialNumbers;
                                foreach (string text3 in array)
                                {
                                    if (text3 != null && (_serialNumberInfo.SerialExternal || text3 != _serialNumberInfo.SerialNumber) && TrySerialNumber(context, text3) == ValidationResult.Valid)
                                    {
                                        text2 = text3;
                                        serialExternal2 = !context.RequestInfo.SaveExternalSerials;
                                        break;
                                    }
                                }
                            }
                            if (text2 == null)
                            {
                                serialNumbers = Config.GetSerialNumber(context.LicensedType.Assembly.GetName().Name);
                                if (serialNumbers == null && context.SupportInfo.Product != null)
                                {
                                    serialNumbers = Config.GetSerialNumber(context.SupportInfo.Product);
                                }
                                if (serialNumbers != null)
                                {
                                    string[] array2 = serialNumbers;
                                    foreach (string text4 in array2)
                                    {
                                        if (text4 != null && (_serialNumberInfo.SerialExternal || text4 != _serialNumberInfo.SerialNumber) && TrySerialNumber(context, text4) == ValidationResult.Valid)
                                        {
                                            text2 = text4;
                                            serialExternal2 = !context.RequestInfo.SaveExternalSerials;
                                            break;
                                        }
                                    }
                                }
                            }
                            if (text2 == null && !context.RequestInfo.ShouldGetNewSerialNumber && !context.RequestInfo.DeveloperMode && context.RequestInfo.ShouldPersistCodes)
                            {
                                string text5 = GetPersistentData(null, "SerialNumber" + LicenseFile.ReleaseVersion, PersistentDataLocationType.SharedOrUser) as string;
                                if (text5 != null && text5 != "EXTERNAL" && text5 != _serialNumberInfo.SerialNumber && TrySerialNumber(context, text5) == ValidationResult.Valid)
                                {
                                    text2 = text5;
                                }
                            }
                            if (text2 != null && text2.Length != 0)
                            {
                                if (_serialNumberInfo.SerialNumber != text2)
                                {
                                    _serialNumberInfo.SerialNumber = text2;
                                    _serialNumberInfo.SerialExternal = serialExternal2;
                                    _wasUnlockedThisTime = true;
                                }
                                goto IL_0351;
                            }
                            if (context.LatestValidationRecord != null && !(context.LatestValidationRecord.ErrorCode != "E_InvalidSerialNumber"))
                            {
                                return ValidationResult.Invalid;
                            }
                            return context.ReportError(context.RequestInfo.ShouldGetNewSerialNumber ? "E_RequestingNewSerialNumber" : "E_MissingSerialNumber", this, null, ErrorSeverity.Low);
                        }
                        if (_serialNumberInfo.SerialNumber == null)
                        {
                            return context.ReportError("E_MissingSerialNumber", this, null, ErrorSeverity.Low);
                        }
                        validationResult = TrySerialNumber(context, _serialNumberInfo.SerialNumber);
                        if (validationResult != ValidationResult.Valid)
                        {
                            return validationResult;
                        }
                    }
                    goto IL_0351;
                    IL_0351:
                    if (!CanLicenseComponent(context.LicensedType.FullName))
                    {
                        return context.ReportError("E_NoComponentInLicense", this, context.LicensedType);
                    }
                    validationResult = _limits.Validate(context);
                    if (_signature != null && _wasReadOnly != 0L)
                    {
                        if (_wasUnlockedThisTime && validationResult == ValidationResult.Valid)
                        {
                            string key = "SerialNumber" + LicenseFile.ReleaseVersion;
                            foreach (SecureLicense item in (IEnumerable)LicenseFile.Licenses)
                            {
                                if (context.EnsureLicenseState(item) && item != this)
                                {
                                    item.ClearSerialNumbersAndActivationData(false);
                                }
                            }
                            _modified = true;
                            if (!context.RequestInfo.DeveloperMode && context.RequestInfo.ShouldPersistCodes)
                            {
                                SetPersistentData(null, key, _serialNumberInfo.SerialExternal ? null : _serialNumberInfo.SerialNumber, PersistentDataLocationType.SharedOrUser);
                            }
                        }
                        else if (validationResult == ValidationResult.Invalid || validationResult == ValidationResult.Canceled)
                        {
                            _needsSaveOnValid = false;
                            if (_serialNumberInfo.SerialNumber != text)
                            {
                                _serialNumberInfo.SerialNumber = text;
                            }
                        }
                        _dontPersistToStorage |= context.RequestInfo.DontPersistToStorage;
                        return validationResult;
                    }
                    _needsSaveOnValid = false;
                    if (_serialNumberInfo.SerialNumber != text)
                    {
                        _serialNumberInfo.SerialNumber = text;
                        _serialNumberInfo.SerialExternal = serialExternal;
                    }
                    return ValidationResult.Invalid;
                }
            }
            finally
            {
                _isValidating = false;
            }
        }

        internal bool TrySerialNumberQuick(SecureLicenseContext context, string serialNumber)
        {
            int seed;
            byte[] parsedSerialData;
            if (!context.CheckSerialNumber(serialNumber, _serialNumberInfo, out seed, out parsedSerialData))
            {
                context.ReportError("Failed at CheckSerialNumber. _parsedSerialData = {0}, seed = {1}", this, _parsedSerialData, _serialNumberInfo._seed);
                return false;
            }
            _serialNumberInfo._seed = seed;
            _parsedSerialData = parsedSerialData;
            return true;
        }

        private ValidationResult TrySerialNumber(SecureLicenseContext context, string serialNumber)
        {
            if (!context.CheckSerialNumber(serialNumber, _serialNumberInfo, out _serialNumberInfo._seed, out _parsedSerialData))
            {
                context.ReportError("Failed at CheckSerialNumber. _parsedSerialData = {0}, seed = {1}", this, _parsedSerialData, _serialNumberInfo._seed);
                return context.ReportError("E_InvalidSerialNumber", this, serialNumber);
            }
            if (_serialNumberInfo._seed >= _serialNumberInfo.MinSeed && _serialNumberInfo._seed <= _serialNumberInfo.MaxSeed && (_serialNumberInfo._seed - _serialNumberInfo.MinSeed) % _serialNumberInfo.Step == 0)
            {
                string[] array = GetPersistentData(null, "INITIALIZEDSERIALS", PersistentDataLocationType.SharedOrUser) as string[];
                if (array != null)
                {
                    string[] array2 = array;
                    int num = 0;
                    while (num < array2.Length)
                    {
                        string strA = array2[num];
                        if (string.Compare(strA, serialNumber, true) != 0)
                        {
                            num++;
                            continue;
                        }
                        if (!(serialNumber != _serialNumberInfo.SerialNumber))
                        {
                            return ValidationResult.Valid;
                        }
                        _wasUnlockedThisTime = true;
                        break;
                    }
                }
                byte[] parsedSerialData = _parsedSerialData;
                if (parsedSerialData != null)
                {
                    for (int i = 1; i < parsedSerialData.Length; i += 4)
                    {
                        if (parsedSerialData[i] == 0 && parsedSerialData[i + 1] == 0 && parsedSerialData[i + 2] == 0 && parsedSerialData[i + 3] == 0)
                        {
                            continue;
                        }
                        Limit limit = Limits.FindLimitByOrdinal(parsedSerialData[i + 3] >> 3);
                        if (!(limit is IExtendableLimit))
                        {
                            return context.ReportError("E_CouldNotInitializeFromSerialNumber", this);
                        }
                        if (!((IExtendableLimit)limit).InitializeFromSerialNumber(context, serialNumber, i))
                        {
                            return context.ReportError("E_CouldNotInitializeFromSerialNumber", this);
                        }
                    }
                }
                if (array == null)
                {
                    SetPersistentData(null, "INITIALIZEDSERIALS", new string[1]
                    {
                        serialNumber
                    }, PersistentDataLocationType.SharedOrUser);
                }
                else
                {
                    string[] array3 = new string[array.Length + 1];
                    Array.Copy(array, array3, array.Length);
                    array3[array3.Length - 1] = serialNumber;
                    SetPersistentData(null, "INITIALIZEDSERIALS", array3, PersistentDataLocationType.SharedOrUser);
                }
                if (serialNumber != _serialNumberInfo.SerialNumber)
                {
                    _wasUnlockedThisTime = true;
                }
                return ValidationResult.Valid;
            }
            context.WriteDiagnostic("Invalid serial number - bad seed: {0} [{1}-{2}]", this, _serialNumberInfo._seed, _serialNumberInfo.MinSeed, _serialNumberInfo.MaxSeed);
            return context.ReportError("E_InvalidSerialNumber", this, serialNumber);
        }

        internal bool CheckNeedsGui(SecureLicenseContext context)
        {
            using (ImpHandle impHandle = StartAdminBlock(context))
            {
                if (impHandle == null)
                {
                    return true;
                }
                foreach (Limit item in (IEnumerable)_limits)
                {
                    if (item.IsGui)
                    {
                        PeekResult peekResult = item.Peek(context);
                        if (peekResult != 0 && peekResult != PeekResult.NeedsUser)
                        {
                            continue;
                        }
                        return true;
                    }
                }
            }
            return false;
        }

        private ImpHandle StartAdminBlock(SecureLicenseContext context)
        {
            ImpHandle impHandle = new ImpHandle();
            if (_requireAdminOnFirstRun != 0 && (_requireAdminOnFirstRun == AdminRunRequirement.All || !SafeToolbox.IsWebRequest))
            {
                _requireAdmin = true;
                WindowsIdentity current = WindowsIdentity.GetCurrent();
                WindowsPrincipal windowsPrincipal = new WindowsPrincipal(current);
                _isAdmin = windowsPrincipal.IsInRole(WindowsBuiltInRole.Administrator);
                context.WriteDiagnostic("_isAdmin = {0}", _isAdmin);
                if (GetMachineState(true) == null)
                {
                    context.WriteDiagnostic("No existing machine state.");
                    if (!context.IsWebRequest && !context.IsWebServiceRequest)
                    {
                        if (!context.IsServiceRequest && !context.HasAdminBeenPrompted)
                        {
                            context.HasAdminBeenPrompted = true;
                            if (AdminCredentialsForm.GetAdminContext(context, ref impHandle))
                            {
                                if (!impHandle.IsValid)
                                {
                                    context.CanSkipFailureReport = true;
                                }
                                _isAdmin = true;
                            }
                        }
                    }
                    else if (_demandAuthenticationForWeb)
                    {
                        HttpContext httpContext = context.GetHttpContext();
                        if (httpContext != null)
                        {
                            bool flag = false;
                            if (httpContext.User != null)
                            {
                                WindowsIdentity windowsIdentity = null;
                                try
                                {
                                    windowsIdentity = WindowsIdentity.GetCurrent();
                                    flag = (string.Compare(httpContext.User.Identity.Name, windowsIdentity.Name) == 0);
                                }
                                finally
                                {
                                    if (windowsIdentity != null)
                                    {
                                        ((IDisposable)windowsIdentity).Dispose();
                                    }
                                }
                            }
                            if (httpContext.User != null && httpContext.User is WindowsPrincipal)
                            {
                                windowsPrincipal = (httpContext.User as WindowsPrincipal);
                                if (windowsPrincipal.IsInRole(WindowsBuiltInRole.Administrator))
                                {
                                    current = (windowsPrincipal.Identity as WindowsIdentity);
                                    if (current != null)
                                    {
                                        impHandle.Imp = current.Impersonate();
                                    }
                                    _isAdmin = true;
                                }
                            }
                            if (impHandle.Imp == null)
                            {
                                httpContext.Response.Status = "401 " + SR.GetString("E_AdminRequiredFirstRun");
                                httpContext.Response.End();
                            }
                            else if (!_isAdmin && _disallowImpersonationForWeb && flag)
                            {
                                context.ReportError("E_WebImpersonationNotAllowed", this, null, ErrorSeverity.High);
                                httpContext.Response.Status = "403 " + SR.GetString("E_WebImpersonationNotAllowed");
                                httpContext.Response.End();
                            }
                        }
                    }
                    if (!impHandle.IsValid)
                    {
                        return null;
                    }
                    GetMachineState(true);
                    if (!Directory.Exists(Config.SharedFolder))
                    {
                        Directory.CreateDirectory(Config.SharedFolder);
                    }
                    SafeToolbox.GrantEveryoneAccess(Config.SharedFolder);
                }
            }
            return impHandle;
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public bool Unlock(SecureLicenseContext context, string serialNumber)
        {
            AssertNotEncrypted();
            if (!_canUnlockBySerial)
            {
                return false;
            }
            if (serialNumber != null && serialNumber.Trim().Length != 0)
            {
                if (serialNumber == _serialNumberInfo.SerialNumber)
                {
                    int num;
                    byte[] array;
                    return context.CheckSerialNumber(serialNumber, _serialNumberInfo, out num, out array);
                }
                if (TrySerialNumber(context, serialNumber) == ValidationResult.Valid)
                {
                    _serialNumberInfo.SerialNumber = serialNumber;
                    return true;
                }
                return false;
            }
            return false;
        }

        [EditorBrowsable(EditorBrowsableState.Advanced)]
        [Obsolete("There is no reason to call this method directly. It is only required when a license is deactivated and uninstalled from the machine")]
        public void ClearSerialNumbersAndActivationData(bool save)
        {
            _needsSaveOnValid = false;
            if (CanUnlockBySerial)
            {
                SetPersistentData(null, "SerialNumber" + LicenseFile.ReleaseVersion, null, PersistentDataLocationType.SharedOrUser);
                SerialNumber = null;
            }
            Limit[] array = Limits.FindLimitsByType(typeof(ActivationLimit), true);
            if (array != null)
            {
                Limit[] array2 = array;
                for (int i = 0; i < array2.Length; i++)
                {
                    ActivationLimit activationLimit = (ActivationLimit)array2[i];
                    activationLimit.Invalidate();
                }
            }
            if (LicenseFile.OriginalLocation != null && File.Exists(LicenseFile.OriginalLocation))
            {
                LicenseFile licenseFile = new LicenseFile(LicenseFile.OriginalLocation, false);
                SecureLicense secureLicense = licenseFile.Licenses[_licenseId];
                if (secureLicense != null)
                {
                    secureLicense.ClearSerialNumbersAndActivationData(save);
                }
            }
            if (save)
            {
                LicenseFile.Save(LicenseFile.Location, true, LicenseSaveType.Normal, true, true);
            }
        }

        internal void ReadFromXml(XmlReader reader, byte[] key)
        {
            Check.NotNull(reader, "reader");
            AssertNotReadOnly();
            try
            {
                reader.MoveToContent();
                string attribute = reader.GetAttribute("version");
                if (attribute == null)
                {
                    throw new SecureLicenseException("E_InvalidVersion");
                }
                _version = new Version(attribute);
                bool flag = false;
                Version[] releaseVersions = ReleaseVersions;
                foreach (Version v in releaseVersions)
                {
                    flag |= (v == _version);
                }
                if (!flag)
                {
                    if (_version.Major != 3)
                    {
                        throw new SecureLicenseException("E_InvalidVersion");
                    }
                    _version = v3_0;
                }
                _signature = reader.GetAttribute("signature");
                _keyName = reader.GetAttribute("keyName");
                _licenseId = reader.GetAttribute("licenseId");
                reader.Read();
                reader.MoveToContent();
                while (!reader.EOF)
                {
                    reader.MoveToContent();
                    if (reader.IsStartElement())
                    {
                        XmlReader readerFromEncrypted;
                        switch (reader.Name)
                        {
                            case "Data":
                                _encryptedSource = reader.ReadElementString();
                                _isStillEncrypted = true;
                                if (key != null)
                                {
                                    readerFromEncrypted = GetReaderFromEncrypted(key);
                                    if (readerFromEncrypted != null)
                                    {
                                        _isStillEncrypted = false;
                                        if (ReadPropertiesFromXml(readerFromEncrypted))
                                        {
                                            goto IL_01c6;
                                        }
                                        throw new SecureLicenseException("E_InvalidLicenseFile", null, ErrorSeverity.Normal);
                                    }
                                    goto IL_01c6;
                                }
                                reader.Read();
                                break;
                            case "Class":
                                attribute = reader.GetAttribute("name");
                                if (attribute != null && attribute.Length > 0)
                                {
                                    _classes.Add(attribute);
                                }
                                reader.Skip();
                                break;
                            case "MetaValues":
                                _metaValues.ReadFromXml(reader);
                                break;
                            case "EditorState":
                                _editorState.ReadFromXml(reader);
                                break;
                            case "RegistrationInfo":
                                _registrationInfo.ReadFromXml(reader);
                                break;
                            case "License":
                                _modified = false;
                                return;
                            default:
                                {
                                    _isStillEncrypted = false;
                                    _modified = false;
                                    if (ReadPropertiesFromXml(reader))
                                    {
                                        return;
                                    }
                                    throw new SecureLicenseException("E_InvalidLicenseFile", null, ErrorSeverity.Normal);
                                }
                                IL_01c6:
                                readerFromEncrypted.Close();
                                break;
                        }
                        continue;
                    }
                    reader.Read();
                    break;
                }
                _modified = false;
            }
            catch (Exception ex)
            {
                SecureLicenseException ex2 = ex as SecureLicenseException;
                if (ex2 != null && ex2.ErrorId == "E_InvalidLicenseFile")
                {
                    throw;
                }
                throw new SecureLicenseException("E_InvalidLicenseFile", ex, ErrorSeverity.Normal);
            }
        }

        private XmlReader GetReaderFromEncrypted(byte[] key)
        {
            if (key != null)
            {
                byte[] array = Convert.FromBase64String(_encryptedSource);
                SharedToolbox.Scramble(array);
                byte[] array2 = new byte[16];
                Buffer.BlockCopy(array, 1, array2, 0, 16);
                using (SymmetricAlgorithm symmetricAlgorithm = SafeToolbox.GetAes(false))
                {
                    symmetricAlgorithm.IV = array2;
                    symmetricAlgorithm.Key = key;
                    array = symmetricAlgorithm.CreateDecryptor().TransformFinalBlock(array, 17, array.Length - 16 - 1);
                }
                if (array.Length > _sigCheck.Length)
                {
                    for (int i = 0; i < _sigCheck.Length; i++)
                    {
                        if (array[i] != _sigCheck[i])
                        {
                            return null;
                        }
                    }
                }
                XmlReader xmlReader = new XmlTextReader(new MemoryStream(array, 0, array.Length, false, false));
                xmlReader.MoveToContent();
                xmlReader.Read();
                return xmlReader;
            }
            return null;
        }

        private bool ReadPropertiesFromXml(XmlReader reader)
        {
            _values.ClearForReload();
            _serialNumberInfo.Clear();
            _limits.Clear();
            _eulaAddress = null;
            _type = null;
            _comments = null;
            _requireAdminOnFirstRun = AdminRunRequirement.ApplicationsAndServices;
            _demandAuthenticationForWeb = true;
            _secureStorageRequirement = SecureStorageRequirement.Required;
            _isTemplate = false;
            _canUnlockBySerial = false;
            reader.MoveToContent();
            int depth = reader.Depth;
            while (!reader.EOF)
            {
                reader.MoveToContent();
                if (reader.IsStartElement())
                {
                    switch (reader.Name)
                    {
                        case "SerialNumber":
                            if (_serialNumberInfo.ReadFromXml(reader))
                            {
                                break;
                            }
                            return false;
                        case "Properties":
                            {
                                _eulaAddress = SafeToolbox.XmlDecode(reader.GetAttribute("eulaAddress"));
                                _type = reader.GetAttribute("type");
                                string attribute = reader.GetAttribute("requireAdmin");
                                if (attribute != null)
                                {
                                    _requireAdminOnFirstRun = (AdminRunRequirement)SafeToolbox.FastParseInt32(attribute);
                                }
                                _demandAuthenticationForWeb = (reader.GetAttribute("demandCredentials") != "false");
                                _disallowImpersonationForWeb = (reader.GetAttribute("disallowImpersonation") != "false");
                                attribute = reader.GetAttribute("secureStorageRequirement");
                                if (attribute != null)
                                {
                                    _secureStorageRequirement = (SecureStorageRequirement)SafeToolbox.FastParseInt32(attribute);
                                }
                                _canUnlockBySerial = (reader.GetAttribute("canUnlock") == "true");
                                _isTemplate = (reader.GetAttribute("isTemplate") == "true");
                                _hardFailureIfFormShown = (reader.GetAttribute("hardFailureIfFormShown") != "false");
                                reader.Skip();
                                break;
                            }
                        case "Class":
                            {
                                string attribute = reader.GetAttribute("name");
                                if (attribute != null && attribute.Length > 0)
                                {
                                    _classes.Add(attribute);
                                }
                                reader.Skip();
                                break;
                            }
                        case "Comments":
                            _comments = reader.ReadElementString();
                            break;
                        case "Values":
                            _values.ReadFromXml(reader);
                            break;
                        case "EditorState":
                            _editorState.ReadFromXml(reader);
                            break;
                        case "MetaValues":
                            _metaValues.ReadFromXml(reader);
                            break;
                        case "RegistrationInfo":
                            _registrationInfo.ReadFromXml(reader);
                            break;
                        case "Resources":
                            _resources.ReadFromXml(reader);
                            break;
                        case "Limit":
                            {
                                Limit limit = Limit.ReadLimit(reader, this);
                                if (limit != null)
                                {
                                    _limits.Add(limit);
                                    break;
                                }
                                return false;
                            }
                        default:
                            reader.Read();
                            break;
                    }
                }
                else
                {
                    reader.Read();
                    if (reader.Depth <= depth)
                    {
                        break;
                    }
                }
            }
            _modified = false;
            return true;
        }

        public bool ReloadWithKey(byte[] key)
        {
            return ReloadWithKeyInternal(key, false);
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ReloadWithKeyInternal(byte[] key, bool throwOnError)
        {
            if (key == null)
            {
                return !_isStillEncrypted;
            }
            if (_isStillEncrypted)
            {
                try
                {
                    if (_encryptedSource == null)
                    {
                        return false;
                    }
                    using (XmlReader xmlReader = GetReaderFromEncrypted(key))
                    {
                        if (xmlReader != null)
                        {
                            _reloading = true;
                            try
                            {
                                if (ReadPropertiesFromXml(xmlReader))
                                {
                                    _isStillEncrypted = false;
                                    return true;
                                }
                            }
                            finally
                            {
                                _reloading = false;
                            }
                            return false;
                        }
                    }
                }
                catch (CryptographicException)
                {
                    if (throwOnError)
                    {
                        throw;
                    }
                    return false;
                }
            }
            return true;
        }

        public bool WriteToXml(XmlWriter writer, LicenseSaveType signing, byte[] key)
        {
            if (IsDisposed)
            {
                throw new SecureLicenseException("E_CannotSaveDisposedLicense");
            }
            Check.NotNull(writer, "writer");
            if (signing != 0 && _isStillEncrypted && key != null)
            {
                ReloadWithKey(key);
            }
            writer.WriteStartElement("License");
            writer.WriteAttributeString("version", _version.ToString());
            if (signing == LicenseSaveType.Normal && _keyName != null && _keyName.Length > 0 && Version <= v3_0)
            {
                writer.WriteAttributeString("keyName", _keyName);
            }
            if (_licenseId == null)
            {
                _licenseId = Guid.NewGuid().ToString("N");
            }
            writer.WriteAttributeString("licenseId", _licenseId);
            if (signing == LicenseSaveType.Normal && _signature != null)
            {
                writer.WriteAttributeString("signature", _signature);
            }
            if (!_isStillEncrypted)
            {
                StringWriter stringWriter = null;
                XmlWriter xmlWriter;
                if (signing == LicenseSaveType.Normal && key != null)
                {
                    stringWriter = new StringWriter();
                    XmlTextWriter xmlTextWriter = new XmlTextWriter(stringWriter);
                    xmlTextWriter.Formatting = Formatting.Indented;
                    xmlTextWriter.Indentation = 1;
                    xmlTextWriter.IndentChar = '\t';
                    xmlWriter = xmlTextWriter;
                    xmlWriter.WriteStartElement("Root");
                }
                else
                {
                    xmlWriter = writer;
                }
                if (_encryptedSource != null && key == null)
                {
                    writer.WriteStartElement("Data");
                    writer.WriteString(_encryptedSource);
                    writer.WriteEndElement();
                }
                else
                {
                    xmlWriter.WriteStartElement("Properties");
                    if (_isTemplate)
                    {
                        xmlWriter.WriteAttributeString("isTemplate", "true");
                    }
                    if (_canUnlockBySerial)
                    {
                        xmlWriter.WriteAttributeString("canUnlock", "true");
                    }
                    if (_type != null && _type.Length > 0)
                    {
                        xmlWriter.WriteAttributeString("type", _type);
                    }
                    if (!_hardFailureIfFormShown)
                    {
                        xmlWriter.WriteAttributeString("hardFailureIfFormShown", "false");
                    }
                    if (_eulaAddress != null)
                    {
                        xmlWriter.WriteAttributeString("eulaAddress", SafeToolbox.XmlEncode(_eulaAddress));
                    }
                    if (_requireAdminOnFirstRun != AdminRunRequirement.ApplicationsAndServices)
                    {
                        XmlWriter xmlWriter2 = xmlWriter;
                        int requireAdminOnFirstRun = (int)_requireAdminOnFirstRun;
                        xmlWriter2.WriteAttributeString("requireAdmin", requireAdminOnFirstRun.ToString());
                    }
                    if (!_demandAuthenticationForWeb)
                    {
                        xmlWriter.WriteAttributeString("demandCredentials", "false");
                    }
                    if (!_disallowImpersonationForWeb)
                    {
                        xmlWriter.WriteAttributeString("disallowImpersonation", "false");
                    }
                    if (_secureStorageRequirement != SecureStorageRequirement.Required)
                    {
                        XmlWriter xmlWriter3 = xmlWriter;
                        int secureStorageRequirement = (int)_secureStorageRequirement;
                        xmlWriter3.WriteAttributeString("secureStorageRequirement", secureStorageRequirement.ToString());
                    }
                    xmlWriter.WriteEndElement();
                    _serialNumberInfo.WriteToXml(xmlWriter, signing);
                    foreach (Limit item in (IEnumerable)_limits)
                    {
                        if (!item.BaseWriteToXml(xmlWriter, signing))
                        {
                            return false;
                        }
                    }
                    _values.WriteToXml(xmlWriter, "Values");
                    if (_comments != null && _comments.Length > 0)
                    {
                        xmlWriter.WriteStartElement("Comments");
                        xmlWriter.WriteCData(_comments);
                        xmlWriter.WriteEndElement();
                    }
                    _resources.WriteToXml(xmlWriter, signing);
                    if (writer != xmlWriter)
                    {
                        xmlWriter.WriteEndElement();
                        if (_modified && key == null)
                        {
                            throw new SecureLicenseException("E_CannotSaveEncryptedWithoutKey");
                        }
                        string s = stringWriter.ToString();
                        if ((_modified || _encryptedSource == null) && key != null)
                        {
                            byte[] array = Encoding.UTF8.GetBytes(s);
                            using (SymmetricAlgorithm symmetricAlgorithm = SafeToolbox.GetAes(false))
                            {
                                symmetricAlgorithm.Key = key;
                                symmetricAlgorithm.GenerateIV();
                                using (MemoryStream memoryStream = new MemoryStream())
                                {
                                    array = symmetricAlgorithm.CreateEncryptor().TransformFinalBlock(array, 0, array.Length);
                                    byte[] iV = symmetricAlgorithm.IV;
                                    symmetricAlgorithm.Clear();
                                    memoryStream.WriteByte(byte.MaxValue);
                                    memoryStream.Write(iV, 0, iV.Length);
                                    memoryStream.Write(array, 0, array.Length);
                                    array = memoryStream.ToArray();
                                    SharedToolbox.Scramble(array);
                                }
                            }
                            string text = Convert.ToBase64String(array);
                            StringBuilder stringBuilder = new StringBuilder(text.Length);
                            for (int i = 0; i < text.Length; i += 80)
                            {
                                stringBuilder.Append("\r\n\t\t\t");
                                stringBuilder.Append(text, i, Math.Min(text.Length - i, 80));
                            }
                            stringBuilder.Append("\r\n\t\t");
                            _encryptedSource = stringBuilder.ToString();
                            _modified = false;
                        }
                        writer.WriteStartElement("Data");
                        writer.WriteString(_encryptedSource);
                        writer.WriteEndElement();
                    }
                }
            }
            else
            {
                writer.WriteStartElement("Data");
                writer.WriteString(_encryptedSource);
                writer.WriteEndElement();
            }
            foreach (string item2 in (IEnumerable)_classes)
            {
                writer.WriteStartElement("Class");
                writer.WriteAttributeString("name", item2);
                writer.WriteEndElement();
            }
            if (signing == LicenseSaveType.Normal)
            {
                _metaValues.WriteToXml(writer, "MetaValues");
                _registrationInfo.WriteToXml(writer, "RegistrationInfo");
                _editorState.WriteToXml(writer, "EditorState");
            }
            writer.WriteEndElement();
            if (_modified)
            {
                _encryptedSource = null;
            }
            if (signing != LicenseSaveType.SignatureCheck)
            {
                _modified = false;
            }
            return true;
        }

        public void FromXmlString(string xml)
        {
            XmlTextReader xmlTextReader = new XmlTextReader(xml, XmlNodeType.Element, null);
            ReadFromXml(xmlTextReader, null);
            xmlTextReader.Close();
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public string ToXmlString(byte[] key)
        {
            StringWriter stringWriter = new StringWriter();
            XmlTextWriter xmlTextWriter = new XmlTextWriter(stringWriter);
            xmlTextWriter.Formatting = Formatting.Indented;
            xmlTextWriter.IndentChar = '\t';
            xmlTextWriter.Indentation = 1;
            WriteToXml(xmlTextWriter, LicenseSaveType.Normal, key);
            xmlTextWriter.Close();
            return stringWriter.ToString();
        }

        public string ToXmlString()
        {
            return ToXmlString(null);
        }

        public override string ToString()
        {
            string text = (_type == null || _type.Trim().Length == 0) ? _licenseId : _type;
            if (_isStillEncrypted)
            {
                return string.Format("{0} (Still Encrypted)", text);
            }
            string text2 = (SerialNumber == null) ? SerialNumberInfo.Prefix : SerialNumber;
            if (text2 == null)
            {
                return text;
            }
            return string.Format("{0} ({1})", text, text2);
        }

        void IChange.MakeReadOnly()
        {
            _isReadOnly = true;
            _wasReadOnly = DateTime.Now.Ticks;
            _classes.MakeReadOnly();
            ((IChange)_values).MakeReadOnly();
            _resources.MakeReadOnly();
            ((IChange)_serialNumberInfo).MakeReadOnly();
            if (_licenseFile != null)
            {
                _licenseFile.MakeReadOnly();
            }
            _limits.MakeReadOnly();
            foreach (Limit item in (IEnumerable)_limits)
            {
                item.MakeReadOnly();
            }
        }

        internal void AssertNotReadOnly(bool allowEncrypted)
        {
            if (!_reloading)
            {
                if (_isReadOnly)
                {
                    throw new SecureLicenseException("E_LicenseReadOnly");
                }
                if (_isStillEncrypted && !allowEncrypted && !_reloading)
                {
                    throw new SecureLicenseException("E_LicenseStillEncrypted");
                }
                _modified = true;
            }
        }

        internal void AssertNotReadOnly()
        {
            AssertNotReadOnly(false);
        }

        private void AssertNotEncrypted()
        {
            if (!_isStillEncrypted)
            {
                return;
            }
            try
            {
                if (SecureLicenseManager.CurrentContext != null)
                {
                    SecureLicenseManager.CurrentContext.EnsureLicenseState(this);
                }
            }
            catch (Exception)
            {
            }
            if (!_isStillEncrypted)
            {
                return;
            }
            throw new SecureLicenseException("E_LicenseStillEncrypted");
        }

        public void Flush()
        {
            lock (SecureLicenseManager.AsyncLock)
            {
                if ((_pendingSecureData || _needsSaveOnValid) && _flushThread == null)
                {
                    _flushThread = new Thread(FlushSecure);
                    _flushThread.Start();
                }
            }
        }

        private void FlushSecure()
        {
            lock (SecureLicenseManager.SyncRoot)
            {
                FlushSecureData(false);
                _flushThread = null;
            }
        }

        internal void SaveOnValid(SecureLicenseContext context, bool force)
        {
            if (_modified || force || _needsSaveOnValid)
            {
                _licenseFile.SaveOnValid(context, true);
            }
            _needsSaveOnValid = false;
        }

        internal object GetPersistentData(Limit limit, string key, PersistentDataLocationType location)
        {
            //Realisable - New check to see if request is in Azure.
            if (SafeToolbox.IsAzureRequest)
            {
                return null;
            }

            Hashtable state;
            try
            {
                state = GetState(location);
            }
            catch (Exception innerException)
            {
                throw new SecureLicenseException("E_CouldNotAcquireSecureStorage", innerException, ErrorSeverity.High);
            }
            if (state == null)
            {
                switch (_secureStorageRequirement)
                {
                    case SecureStorageRequirement.NotRequired:
                        return null;
                    case SecureStorageRequirement.RequiredExceptWeb:
                        if (!SafeToolbox.IsWebRequest && !SafeToolbox.IsServiceRequest)
                        {
                            throw new SecureLicenseException("E_CouldNotAcquireSecureStorage");
                        }
                        goto default;
                    default:
                        return null;
                    case SecureStorageRequirement.Required:
                        throw new SecureLicenseException("E_CouldNotAcquireSecureStorage");
                }
            }
            string name = "SOFTWARE\\XHEO INC\\Licenses\\" + LicenseId;
            string name2 = key.GetHashCode().ToString("X");
            try
            {
                RegistryKey registryKey;
                using (registryKey = Registry.LocalMachine.OpenSubKey(name, false))
                {
                    if (registryKey == null)
                    {
                        registryKey = Registry.CurrentUser.OpenSubKey(name, false);
                    }
                    if (registryKey != null)
                    {
                        registryKey.GetValue(name2);
                    }
                }
            }
            catch
            {
            }
            if (limit == null)
            {
                return state[key];
            }
            return state[limit.LimitId + '.' + key];
        }

        private Hashtable GetMachineState(bool reload)
        {
            if (!Check.CheckCalledByThisAssembly())
            {
                SecureLicenseContext.WriteDiagnosticToContext("Not called by this assembly.");
                _signature = null;
                return null;
            }
            if (_machineSecureState == null)
            {
                string licenseId = _licenseId;
                object obj = _secureStates[licenseId];
                if (obj != null && !reload)
                {
                    _machineSecureState = (obj as Hashtable);
                }
                else
                {
                    SecureLicenseContext.WriteDiagnosticToContext(true, "Getting secure data, reload = {0}", reload);
                    _machineSecureState = GetSecureData("Seed" + "\0" + licenseId, _requireAdmin, false);
                    if (_machineSecureState == null)
                    {
                        SecureLicenseContext.WriteDiagnosticToContext("Could not acquire secure context.");
                        _secureStates[licenseId] = new object();
                    }
                    else
                    {
                        _secureStates[licenseId] = _machineSecureState;
                    }
                }
            }
            return _machineSecureState;
        }

        private Hashtable GetUserState()
        {
            if (!Check.CheckCalledByThisAssembly())
            {
                _signature = null;
                return null;
            }
            if (_userSecureState == null)
            {
                string licenseId = _licenseId;
                string key = "user::" + licenseId;
                object obj = _secureStates[key];
                if (obj == null)
                {
                    _userSecureState = GetSecureData("Seed" + "\0" + licenseId, false, true);
                    if (_userSecureState == null)
                    {
                        _secureStates[key] = new object();
                    }
                    else
                    {
                        _secureStates[key] = _userSecureState;
                    }
                }
                else
                {
                    _userSecureState = (obj as Hashtable);
                }
            }
            return _userSecureState;
        }

        private Hashtable GetState(PersistentDataLocationType location)
        {
            Hashtable hashtable = null;
            SecureLicenseContext.WriteDiagnosticToContext("Getting secure state for {0}", location);
            switch (location)
            {
                case PersistentDataLocationType.User:
                    hashtable = GetUserState();
                    break;
                case PersistentDataLocationType.Shared:
                    hashtable = GetMachineState(true);
                    break;
                case PersistentDataLocationType.SharedOrUser:
                    hashtable = _secureState;
                    if (hashtable == null)
                    {
                        hashtable = GetMachineState(true);
                        SecureLicenseContext.WriteDiagnosticToContext("Machine state is {0}", hashtable);
                        if (hashtable == null)
                        {
                            hashtable = GetUserState();
                            SecureLicenseContext.WriteDiagnosticToContext("Trying user state instead. Got {0}", hashtable);
                        }
                        else
                        {
                            SecureLicenseContext.WriteDiagnosticToContext("Got machine state.");
                        }
                        _secureState = hashtable;
                    }
                    else
                    {
                        SecureLicenseContext.WriteDiagnosticToContext("Using cached _secureState");
                    }
                    break;
            }
            return hashtable;
        }

        internal void FlushSecureData(bool requireAdmin)
        {
            if (_needsSaveOnValid)
            {
                SaveOnValid(null, true);
            }
            if (_pendingSecureData)
            {
                if (_machineSecureState != null)
                {
                    SaveSecureData("Seed" + "\0" + _licenseId, _machineSecureState, requireAdmin, false);
                }
                if (_userSecureState != null)
                {
                    SaveSecureData("Seed" + "\0" + _licenseId, _userSecureState, requireAdmin, true);
                }
                _pendingSecureData = false;
            }
        }

        private void SaveSecureData(string id, Hashtable hash, bool requireAdmin, bool user)
        {
            if (!_dontPersistToStorage)
            {
                try
                {
                    if (!SafeToolbox.IsWebRequest && hash["___69EFF3911DBE433EBD3361537853DD90"] == null)
                    {
                        using (new Wow64Guard())
                        {
                            SaveRegistrySecureData(id, user, requireAdmin, hash);
                        }
                    }
                    else
                    {
                        SaveIsolatedStorageSecureData(id, user, requireAdmin, hash);
                    }
                }
                catch (Exception ex)
                {
                    SecureLicenseContext.WriteDiagnosticToContext("Could not persist secure storage: {0}", ex.Message);
                }
            }
        }

        private void SaveIsolatedStorageSecureData(string id, bool user, bool requireAdmin, Hashtable hash)
        {
            IsolatedStorageFile isolatedStorage = GetIsolatedStorage(user);
            if (isolatedStorage != null)
            {
                using (IsolatedStorageFileStream isolatedStorageFileStream = new IsolatedStorageFileStream(GetSafeIsId(id), FileMode.Create, FileAccess.Write, isolatedStorage))
                {
                    byte[] array = EncryptSecureData(hash);
                    isolatedStorageFileStream.Write(array, 0, array.Length);
                    isolatedStorageFileStream.Flush();
                }
            }
        }

        private void SaveRegistrySecureData(string id, bool user, bool requireAdmin, Hashtable hash)
        {
            if (Check.CheckCalledByThisAssembly() && _wasReadOnly != 0L)
            {
                string value = "FBSGJNER\\Zvpebfbsg\\Pelcgbtencul\\EAT" + "\0";
                value = SharedToolbox.Rot13(value);
                try
                {
                    using (RegistryKey registryKey = Registry.CurrentUser.OpenSubKey(SharedToolbox.Rot13("Fbsgjner\\Zvpebfbsg\\Jvaqbjf\\PheeragIrefvba\\Rkcybere\\HfreNffvfg"), true))
                    {
                        string text = "{75048700-EF1F-11D0-9888-006097DEACF9}";
                        RegistryKey registryKey2;
                        using (registryKey2 = registryKey.OpenSubKey(text, true))
                        {
                            if (registryKey2 == null)
                            {
                                registryKey2 = registryKey.CreateSubKey(text);
                            }
                            RegistryKey registryKey3;
                            using (registryKey3 = registryKey2.OpenSubKey("Count", true))
                            {
                                if (registryKey3 == null)
                                {
                                    registryKey3 = registryKey2.CreateSubKey("Count");
                                }
                                string uAValueName = GetUAValueName();
                                uAValueName = SharedToolbox.Rot13(uAValueName);
                                object value2 = registryKey3.GetValue(uAValueName);
                                if (value2 == null)
                                {
                                    byte[] array = new byte[16]
                                    {
                                        1,
                                        0,
                                        0,
                                        0,
                                        0,
                                        6,
                                        0,
                                        0,
                                        0,
                                        0,
                                        0,
                                        0,
                                        0,
                                        0,
                                        0,
                                        0
                                    };
                                    Buffer.BlockCopy(BitConverter.GetBytes(DateTime.Now.Ticks), 0, array, 8, 8);
                                    registryKey3.SetValue(uAValueName, array, RegistryValueKind.Binary);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    SecureLicenseContext.WriteDiagnosticToContext("Could not create first run key: {0}", ex.Message);
                }
                IntPtr intPtr = OpenOrCreate(user ? RegistryHive.CurrentUser : RegistryHive.LocalMachine, value, requireAdmin);
                try
                {
                    if (intPtr != IntPtr.Zero)
                    {
                        if (_isAdmin)
                        {
                            IntPtr zero = IntPtr.Zero;
                            string ssd = string.Format("O:{0}G:S-1-3-1D:(A;;GA;;;S-1-1-0)", SafeNativeMethods.GetCurrentUserSid());
                            try
                            {
                                ulong num;
                                if (SafeNativeMethods.ConvertStringSecurityDescriptorToSecurityDescriptor(ssd, 1, out zero, out num))
                                {
                                    SafeNativeMethods.NtSetSecurityObject(intPtr, 4, zero);
                                }
                            }
                            finally
                            {
                                if (zero != IntPtr.Zero)
                                {
                                    Marshal.FreeHGlobal(zero);
                                }
                            }
                        }
                        using (SafeNativeMethods.USWrapper uSWrapper = new SafeNativeMethods.USWrapper(id))
                        {
                            byte[] array2 = EncryptSecureData(hash);
                            SafeNativeMethods.NtSetValueKey(intPtr, uSWrapper.GetPtr(), 0, 3, array2, array2.Length);
                        }
                    }
                }
                finally
                {
                    if (intPtr != IntPtr.Zero)
                    {
                        SafeNativeMethods.CloseHandle(intPtr);
                    }
                }
            }
        }

        private byte[] EncryptSecureData(Hashtable hash)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                binaryFormatter.Serialize(memoryStream, hash);
                memoryStream.Flush();
                byte[] array = memoryStream.ToArray();
                using (SymmetricAlgorithm symmetricAlgorithm = SafeToolbox.GetAes(false))
                {
                    symmetricAlgorithm.IV = new byte[16];
                    symmetricAlgorithm.Key = new byte[32];
                    Buffer.BlockCopy(Images.ProtectedSmall_png_, 0, symmetricAlgorithm.IV, 0, symmetricAlgorithm.IV.Length);
                    Buffer.BlockCopy(Images.ProtectedSmall_png_, symmetricAlgorithm.IV.Length, symmetricAlgorithm.Key, 0, symmetricAlgorithm.Key.Length);
                    return symmetricAlgorithm.CreateEncryptor().TransformFinalBlock(array, 0, array.Length);
                }
            }
        }

        private string GetUAValueName()
        {
            string text = "";
            if (Environment.OSVersion.Version.Major >= 6)
            {
                using (RegistryKey registryKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion", false))
                {
                    Version version = new Version(registryKey.GetValue("CurrentVersion") as string);
                    text = string.Format("{0}{1}{2:X}", 65 + version.Major, 97 + version.Minor, registryKey.GetValue("CurrentBuildNumber"));
                }
            }
            else
            {
                text = string.Format("{0}{1}{2:X}", 65 + Environment.OSVersion.Version.Major, 97 + Environment.OSVersion.Version.Minor, Environment.OSVersion.Version.Revision);
            }
            string text2;
            try
            {
                text2 = Environment.MachineName;
            }
            catch (InvalidOperationException)
            {
                text2 = "Windows";
            }
            return string.Format("UEME_RUNPATH:{0}\\{1}{2}\\{3}\\{4}", Environment.GetFolderPath(Environment.SpecialFolder.System), Environment.OSVersion.Platform, text, _licenseId, text2);
        }

        private Hashtable GetSecureData(string id, bool requireAdmin, bool user)
        {
            try
            {
                if (SafeToolbox.IsWebRequest)
                {
                    SecureLicenseContext.WriteDiagnosticToContext("Using ISF Storage");
                    Hashtable isolatedStorageSecureData = GetIsolatedStorageSecureData(user, requireAdmin, id);
                    if (isolatedStorageSecureData != null)
                    {
                        isolatedStorageSecureData["___69EFF3911DBE433EBD3361537853DD90"] = true;
                    }
                    return isolatedStorageSecureData;
                }
                using (new Wow64Guard())
                {
                    SecureLicenseContext.WriteDiagnosticToContext("Using Registry Storage");
                    return GetRegistrySecureData(user, requireAdmin, id);
                }
            }
            catch (Exception ex)
            {
                SecureLicenseContext.WriteDiagnosticToContext("Could not open secure storage: {0}", ex.Message);
                throw;
            }
        }

        private Hashtable GetIsolatedStorageSecureData(bool user, bool requireAdmin, string id)
        {
            IsolatedStorageFile isolatedStorage = GetIsolatedStorage(user);
            if (isolatedStorage == null)
            {
                return null;
            }
            string safeIsId = GetSafeIsId(id);
            string[] fileNames = isolatedStorage.GetFileNames(safeIsId);
            if (fileNames != null && fileNames.Length != 0)
            {
                using (IsolatedStorageFileStream isolatedStorageFileStream = new IsolatedStorageFileStream(safeIsId, FileMode.Open, FileAccess.Read, isolatedStorage))
                {
                    byte[] array = new byte[256];
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        for (int num = isolatedStorageFileStream.Read(array, 0, array.Length); num > 0; num = isolatedStorageFileStream.Read(array, 0, array.Length))
                        {
                            memoryStream.Write(array, 0, num);
                        }
                        memoryStream.Flush();
                        if (memoryStream.Length == 0L)
                        {
                            isolatedStorageFileStream.Close();
                            Hashtable hashtable = new Hashtable();
                            SaveSecureData(id, hashtable, requireAdmin, user);
                            return hashtable;
                        }
                        return DecryptSecureData(memoryStream.ToArray(), 0);
                    }
                }
            }
            Hashtable hashtable2 = new Hashtable();
            SaveSecureData(id, hashtable2, requireAdmin, user);
            return hashtable2;
        }

        private string GetSafeIsId(string id)
        {
            return id.Replace('\0', '_');
        }

        private IsolatedStorageFile GetIsolatedStorage(bool user)
        {
            IsolatedStorageFile isolatedStorageFile = null;
            try
            {
                if (user)
                {
                    SecureLicenseContext.WriteDiagnosticToContext("Opening user scoped isolated storage.");
                    isolatedStorageFile = IsolatedStorageFile.GetUserStoreForAssembly();
                }
                else
                {
                    SecureLicenseContext.WriteDiagnosticToContext("Opening administrator scoped isolated storage.");
                    isolatedStorageFile = IsolatedStorageFile.GetMachineStoreForAssembly();
                }
                isolatedStorageFile.GetDirectoryNames("*.test");
                return isolatedStorageFile;
            }
            catch (Exception ex)
            {
                SecureLicenseContext.WriteDiagnosticToContext("Could not open isolated storage {0}: {1}", ex.GetType(), ex.Message);
                return null;
            }
        }

        private Hashtable GetRegistrySecureData(bool user, bool requireAdmin, string id)
        {
            SecureLicenseContext.WriteDiagnosticToContext("GetRegistrySecureData( {0}, {1}, {2})", user, requireAdmin, id);
            string text = SharedToolbox.Rot13("FBSGJNER\\Zvpebfbsg\\Pelcgbtencul\\EAT" + "\0");
            SecureLicenseContext.WriteDiagnosticToContext("SDPath is {0}", text);
            IntPtr intPtr = OpenOrCreate(user ? RegistryHive.CurrentUser : RegistryHive.LocalMachine, text, requireAdmin);
            try
            {
                if (intPtr != IntPtr.Zero)
                {
                    SecureLicenseContext.WriteDiagnosticToContext("Got handle: {0:X}, opening {1}", intPtr, id);
                    byte[] array = null;
                    using (SafeNativeMethods.USWrapper uSWrapper = new SafeNativeMethods.USWrapper(id))
                    {
                        SecureLicenseContext.WriteDiagnosticToContext("Querying existing key {0}, {1}.", id, uSWrapper.GetPtr());
                        int num;
                        int num2 = SafeNativeMethods.NtQueryValueKey(intPtr, uSWrapper.GetPtr(), 2, null, 0, out num);
                        SecureLicenseContext.WriteDiagnosticToContext("QvNTK returned {0:X}", num2);
                        switch (num2)
                        {
                            case -1073741772:
                                {
                                    SecureLicenseContext.WriteDiagnosticToContext("Secure storage not found");
                                    try
                                    {
                                        using (RegistryKey registryKey = Registry.CurrentUser.OpenSubKey(SharedToolbox.Rot13("Fbsgjner\\Zvpebfbsg\\Jvaqbjf\\PheeragIrefvba\\Rkcybere\\HfreNffvfg")))
                                        {
                                            if (registryKey != null)
                                            {
                                                string name = SharedToolbox.Rot13("{PROSS5PQ-NPR2-4S4S-9178-9926S41749RN}\\Pbhag");
                                                RegistryKey registryKey2;
                                                using (registryKey2 = registryKey.OpenSubKey(name))
                                                {
                                                    if (registryKey2 != null)
                                                    {
                                                        string uAValueName = GetUAValueName();
                                                        uAValueName = SharedToolbox.Rot13(uAValueName);
                                                        object value = registryKey2.GetValue(uAValueName);
                                                        if (value != null)
                                                        {
                                                            SecureLicenseContext.WriteDiagnosticToContext("...First run marker found.");
                                                            bool flag = !user;
                                                            if (user)
                                                            {
                                                                flag = (GetRegistrySecureData(false, requireAdmin, id) == null);
                                                            }
                                                            if (flag)
                                                            {
                                                                SecureLicenseContext currentContext = SecureLicenseManager.CurrentContext;
                                                                if (currentContext != null)
                                                                {
                                                                    currentContext.ReportError("E_SecureStorageTampered", this);
                                                                    if (!currentContext.RequestInfo.DeveloperMode)
                                                                    {
                                                                        return null;
                                                                    }
                                                                    currentContext.ReportError(new ValidationRecord("M_RestoredSecureStorage", this, null, ErrorSeverity.Low));
                                                                    goto end_IL_015c;
                                                                }
                                                                return null;
                                                            }
                                                        }
                                                    }
                                                    end_IL_015c:;
                                                }
                                            }
                                        }
                                    }
                                    catch
                                    {
                                    }
                                    Hashtable hashtable = new Hashtable();
                                    SaveSecureData(id, hashtable, requireAdmin, user);
                                    return hashtable;
                                }
                            default:
                                return null;
                            case -1073741789:
                                break;
                        }
                        SecureLicenseContext.WriteDiagnosticToContext("Found key, needs buffer of  {0:X}", num);
                        array = new byte[num];
                        num2 = SafeNativeMethods.NtQueryValueKey(intPtr, uSWrapper.GetPtr(), 2, array, array.Length, out num);
                        SecureLicenseContext.WriteDiagnosticToContext("Second QvNTK returned {0:X}", num2);
                        if (num2 != 0)
                        {
                            SecureLicenseContext.WriteDiagnosticToContext("Could not get current key: {0:X}", num2);
                            return null;
                        }
                    }
                    return DecryptSecureData(array, 12);
                }
                SecureLicenseContext.WriteDiagnosticToContext("Could not get handle to secure data.");
            }
            catch (Exception ex)
            {
                SecureLicenseContext.WriteDiagnosticToContext("Exception when getting secure data: {0}", ex.ToString());
            }
            finally
            {
                if (intPtr != IntPtr.Zero)
                {
                    SafeNativeMethods.CloseHandle(intPtr);
                }
            }
            return null;
        }

        private Hashtable DecryptSecureData(byte[] data, int offset)
        {
            SecureLicenseContext.WriteDiagnosticToContext("Deserializing persisted data.");
            using (SymmetricAlgorithm symmetricAlgorithm = SafeToolbox.GetAes(false))
            {
                symmetricAlgorithm.IV = new byte[16];
                symmetricAlgorithm.Key = new byte[32];
                Buffer.BlockCopy(Images.ProtectedSmall_png_, 0, symmetricAlgorithm.IV, 0, symmetricAlgorithm.IV.Length);
                Buffer.BlockCopy(Images.ProtectedSmall_png_, symmetricAlgorithm.IV.Length, symmetricAlgorithm.Key, 0, symmetricAlgorithm.Key.Length);
                data = symmetricAlgorithm.CreateDecryptor().TransformFinalBlock(data, offset, data.Length - offset);
            }
            using (MemoryStream serializationStream = new MemoryStream(data, 0, data.Length, false, true))
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                object obj = binaryFormatter.Deserialize(serializationStream) as Hashtable;
                SecureLicenseContext.WriteDiagnosticToContext("result = {0}", obj);
                return obj as Hashtable;
            }
        }

        private IntPtr OpenOrCreate(RegistryHive hive, string path, bool requireAdmin)
        {
            int num = 0;
            using (SafeNativeMethods.OAWrapper oAWrapper = new SafeNativeMethods.OAWrapper())
            {
                string rootKey = GetRootKey(hive);
                SecureLicenseContext.WriteDiagnosticToContext("Secure data root path is [{0}]\\{1}", rootKey, path);
                IntPtr intPtr = IntPtr.Zero;
                IntPtr zero = IntPtr.Zero;
                uint num2 = (uint)((OSRecord.ThisMachine.Product > OSProduct.Windows2000) ? 256 : 0);
                SecureLicenseContext.WriteDiagnosticToContext("Wow64Flag: {0:X}", num2);
                oAWrapper.Initialize(rootKey + '\\' + path, 64u, intPtr, new IntPtr(-1));
                int num3;
                if (requireAdmin && !_isAdmin)
                {
                    num3 = SafeNativeMethods.NtOpenKey(out zero, 0xF003F | num2, oAWrapper.GetPtr());
                    SecureLicenseContext.WriteDiagnosticToContext("Call to OpNTK with full path returned {0:X}.", num3);
                }
                else
                {
                    num3 = SafeNativeMethods.NtCreateKey(ref zero, 0xF003F | num2, oAWrapper.GetPtr(), 0, IntPtr.Zero, 0, ref num);
                    SecureLicenseContext.WriteDiagnosticToContext("Call to CrNTK with full path returned {0:X}", num3);
                }
                if (num3 == 0)
                {
                    return zero;
                }
                string[] array = path.Split('\\');
                string text = rootKey + '\\' + array[0];
                SecureLicenseContext.WriteDiagnosticToContext("Key does not exist, walking path from {0}", text);
                oAWrapper.Initialize(text, 64u, IntPtr.Zero, IntPtr.Zero);
                num3 = SafeNativeMethods.NtCreateKey(ref intPtr, (uint)(-2147352551 | (int)num2), oAWrapper.GetPtr(), 0, IntPtr.Zero, 0, ref num);
                SecureLicenseContext.WriteDiagnosticToContext("Root call to CrNTK returned {0:X}", num3);
                switch (num3)
                {
                    case -1073741811:
                        return IntPtr.Zero;
                    case -1073741772:
                        if (requireAdmin)
                        {
                            break;
                        }
                        return IntPtr.Zero;
                }
                if (num3 < 0)
                {
                    Marshal.ThrowExceptionForHR(num3);
                }
                int num4 = 1;
                while (true)
                {
                    if (num4 < array.Length)
                    {
                        string text2 = array[num4];
                        SecureLicenseContext.WriteDiagnosticToContext("Secure data part {0:X}", text2);
                        oAWrapper.Initialize(text2, 64u, intPtr, new IntPtr(-1));
                        if (requireAdmin && !_isAdmin)
                        {
                            num3 = SafeNativeMethods.NtOpenKey(out zero, (uint)(((num4 == array.Length - 1) ? 983103 : 131097) | (int)num2), oAWrapper.GetPtr());
                            SecureLicenseContext.WriteDiagnosticToContext("OpNTK returned {0:X}", num3);
                        }
                        else
                        {
                            num3 = SafeNativeMethods.NtCreateKey(ref zero, (uint)(((num4 == array.Length - 1) ? 983103 : 131097) | (int)num2), oAWrapper.GetPtr(), 0, IntPtr.Zero, 0, ref num);
                            SecureLicenseContext.WriteDiagnosticToContext("CrNTK returned {0:X}", num3);
                        }
                        if (num3 < 0)
                        {
                            break;
                        }
                        SafeNativeMethods.CloseHandle(intPtr);
                        intPtr = zero;
                        num4++;
                        continue;
                    }
                    return zero;
                }
                SafeNativeMethods.CloseHandle(intPtr);
                return IntPtr.Zero;
            }
        }

        private static string GetRootKey(RegistryHive hive)
        {
            switch (hive)
            {
                default:
                    return null;
                case RegistryHive.CurrentUser:
                    return "\\Registry\\User\\" + SafeNativeMethods.GetCurrentUserSid();
                case RegistryHive.LocalMachine:
                    return "\\Registry\\Machine";
                case RegistryHive.Users:
                    return "\\Registry\\User";
            }
        }

        internal void SetPersistentData(Limit limit, string key, object value, PersistentDataLocationType location)
        {
            SecureLicenseContext.WriteDiagnosticToContext("SetPersistentData( limit = {0}, key = {1}, value = {2}, location = {3} )", limit, key, value, location);
            if (!Check.CheckCalledByThisAssembly())
            {
                SecureLicenseContext.WriteDiagnosticToContext("Not called by this assembly.");
            }
            else
            {
                if (location == PersistentDataLocationType.NotSet)
                {
                    location = PersistentDataLocationType.SharedOrUser;
                }
                Hashtable state;
                try
                {
                    SecureLicenseContext.WriteDiagnosticToContext("SetPersistentData GettingState");
                    state = GetState(location);
                    if (state != null)
                    {
                        goto end_IL_004d;
                    }
                    return;
                    end_IL_004d:;
                }
                catch (Exception innerException)
                {
                    throw new SecureLicenseException("E_CouldNotAcquireSecureStorage", innerException, ErrorSeverity.High);
                }
                string key2 = (limit == null) ? key : (limit.LimitId + '.' + key);
                object obj = state[key2];
                if (obj != value)
                {
                    state[key2] = value;
                    SecureLicenseContext current = SecureLicenseContext.Current;
                    if (!_dontPersistToStorage && (current == null || !current.RequestInfo.DeveloperMode || !current.RequestInfo.DontPersistToStorage))
                    {
                        _pendingSecureData = true;
                        Flush();
                    }
                }
                try
                {
                    string honeyAddress = GetHoneyAddress();
                    RegistryKey registryKey = null;
                    try
                    {
                        IntPtr intPtr = OpenOrCreate(RegistryHive.LocalMachine, honeyAddress, true);
                        if (intPtr == IntPtr.Zero)
                        {
                            intPtr = OpenOrCreate(RegistryHive.CurrentUser, honeyAddress, false);
                            if (intPtr != IntPtr.Zero)
                            {
                                SafeNativeMethods.CloseHandle(intPtr);
                                registryKey = Registry.CurrentUser.OpenSubKey(honeyAddress, true);
                            }
                        }
                        else
                        {
                            SafeNativeMethods.CloseHandle(intPtr);
                            registryKey = Registry.LocalMachine.OpenSubKey(honeyAddress, true);
                        }
                        if (registryKey != null)
                        {
                            string name = key.GetHashCode().ToString("X");
                            if (value == null)
                            {
                                registryKey.DeleteValue(name);
                            }
                            else
                            {
                                registryKey.SetValue(name, Convert.ToBase64String(Encoding.Unicode.GetBytes(value.ToString())));
                            }
                        }
                    }
                    finally
                    {
                        if (registryKey != null)
                        {
                            registryKey.Close();
                        }
                    }
                }
                catch
                {
                }
            }
        }

        private string GetHoneyAddress()
        {
            return SharedToolbox.Rot13("FBSGJNER\\KURB VAP\\Yvprafrf\\") + LicenseId;
        }

        internal void ResetRuntimeState()
        {
            Check.CalledByAssembly(typeof(SecureLicense).Assembly);
            _needsSaveOnValid = false;
            string[] array = new string[RuntimeState.Keys.Count];
            RuntimeState.Keys.CopyTo(array, 0);
            string[] array2 = array;
            foreach (string text in array2)
            {
                if (!text.StartsWith("#"))
                {
                    RuntimeState.Remove(text);
                }
            }
        }

        public TimeMonitor GetTimeMonitor(string limitId)
        {
            TimeLimit timeLimit = Limits[limitId] as TimeLimit;
            if (timeLimit == null)
            {
                return null;
            }
            return timeLimit.GetTimeMonitor();
        }

        public TimeMonitor GetTimeMonitor()
        {
            TimeLimit timeLimit = Limits.FindLimitByType(typeof(TimeLimit), true) as TimeLimit;
            if (timeLimit == null)
            {
                return null;
            }
            return timeLimit.GetTimeMonitor();
        }

        public UseMonitor GetUseMonitor(string limitId)
        {
            UseLimit useLimit = Limits[limitId] as UseLimit;
            if (useLimit == null)
            {
                return null;
            }
            return useLimit.GetUseMonitor();
        }

        public UseMonitor GetUseMonitor()
        {
            UseLimit useLimit = Limits.FindLimitByType(typeof(UseLimit), true, true) as UseLimit;
            if (useLimit == null)
            {
                return null;
            }
            return useLimit.GetUseMonitor();
        }

        public void AttachChangeBubbler(CollectionEventArgs e)
        {
            IChange change = e.Element as IChange;
            if (e.Action == CollectionChangeAction.Add)
            {
                if (_bubbleHandler == null)
                {
                    _bubbleHandler = BubbleChanged;
                }
                change.Changed += _bubbleHandler;
            }
            else if (e.Action == CollectionChangeAction.Remove)
            {
                change.Changed -= _bubbleHandler;
            }
        }

        public bool GetFlag(SerialNumberFlags flags)
        {
            AssertNotEncrypted();
            if (_serialNumberInfo.SerialNumber != null && _parsedSerialData != null)
            {
                return ((int)_parsedSerialData[0] & (int)flags) == (int)flags;
            }
            return false;
        }

        public bool GetFlag(string flag)
        {
            AssertNotEncrypted();
            int num = _serialNumberInfo.FlagNames.IndexOf(flag);
            if (num == -1)
            {
                return false;
            }
            return GetFlag((SerialNumberFlags)(1 << num));
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public static Stream GetResource(ref string address)
        {
            return Toolbox.GetResource(ref address, null);
        }

        public bool AreFeaturesEnabled(string features)
        {
            return AreFeaturesEnabled(features, false);
        }

        public bool AreFeaturesEnabled(string features, bool onlyRequireSingleMatch)
        {
            if (features != null && features.Length != 0)
            {
                Limit[] array = Limits.FindLimitsByType(typeof(FeatureLimit), true, true);
                if (array != null && array.Length != 0)
                {
                    if (array.Length == 1)
                    {
                        if (array[0].WasValidated)
                        {
                            return ((FeatureLimit)array[0]).AreFeaturesEnabled(features, onlyRequireSingleMatch);
                        }
                        return false;
                    }
                    if (onlyRequireSingleMatch)
                    {
                        Limit[] array2 = array;
                        int num = 0;
                        while (true)
                        {
                            if (num < array2.Length)
                            {
                                FeatureLimit featureLimit = (FeatureLimit)array2[num];
                                if (featureLimit.WasValidated && featureLimit.AreFeaturesEnabled(features, true))
                                {
                                    break;
                                }
                                num++;
                                continue;
                            }
                            return false;
                        }
                        return true;
                    }
                    string[] array3 = features.Split(new string[1]
                    {
                        ","
                    }, StringSplitOptions.RemoveEmptyEntries);
                    bool[] array4 = new bool[array3.Length];
                    for (int i = 0; i < array3.Length; i++)
                    {
                        Limit[] array5 = array;
                        for (int j = 0; j < array5.Length; j++)
                        {
                            FeatureLimit featureLimit2 = (FeatureLimit)array5[j];
                            if (featureLimit2.WasValidated && featureLimit2.AreFeaturesEnabled(array3[i], true))
                            {
                                array4[i] = true;
                                break;
                            }
                        }
                    }
                    bool[] array6 = array4;
                    int num2 = 0;
                    while (true)
                    {
                        if (num2 < array6.Length)
                        {
                            if (!array6[num2])
                            {
                                break;
                            }
                            num2++;
                            continue;
                        }
                        return true;
                    }
                    return false;
                }
                return false;
            }
            return false;
        }

        public SecureLicense ShowRegistrationForm(object instance, Type type, LicenseValidationRequestInfo info)
        {
            if (type == null && instance == null)
            {
                throw new ArgumentNullException("instance");
            }
            if (type == null)
            {
                type = instance.GetType();
            }
            SecureLicenseContext secureLicenseContext = null;
            try
            {
                secureLicenseContext = new SecureLicenseContext(LicenseManager.CurrentContext, type, instance, info, new StackTrace(1, false));
                foreach (SecureLicense item in (IEnumerable)LicenseFile.Licenses)
                {
                    secureLicenseContext.EnsureLicenseState(item);
                }
                RegistrationLimit regLimit = GetRegLimit();
                if (regLimit == null)
                {
                    return null;
                }
                if (info == null || info.RegistrationInfo.Count == 0)
                {
                    foreach (DictionaryEntry item2 in RegistrationInfo)
                    {
                        if (!secureLicenseContext.RequestInfo.RegistrationInfo.ContainsKey(item2.Key as string))
                        {
                            ((StringDictionary)secureLicenseContext.RequestInfo.RegistrationInfo)[item2.Key as string] = (item2.Value as string);
                        }
                    }
                    ((StringDictionary)secureLicenseContext.RequestInfo.RegistrationInfo)["upgradeserialnumber"] = null;
                }
                ((StringDictionary)secureLicenseContext.RequestInfo.RegistrationInfo)["suggestedupgradeserialnumber"] = SerialNumber;
                secureLicenseContext.Items["ManualRegistration"] = true;
                return SecureLicenseManager.ShowForm(regLimit, null, instance, type, info, secureLicenseContext);
            }
            finally
            {
                if (secureLicenseContext != null)
                {
                    secureLicenseContext.Close();
                }
            }
        }

        private RegistrationLimit GetRegLimit()
        {
            RegistrationLimit registrationLimit = Limits[typeof(RegistrationLimit)] as RegistrationLimit;
            if (registrationLimit == null)
            {
                if (CanUnlockBySerial)
                {
                    foreach (SecureLicense item in (IEnumerable)LicenseFile.Licenses)
                    {
                        if (!item.CanUnlockBySerial)
                        {
                            registrationLimit = (item.Limits[typeof(RegistrationLimit)] as RegistrationLimit);
                            if (registrationLimit != null)
                            {
                                break;
                            }
                        }
                    }
                }
                if (registrationLimit == null)
                {
                    return null;
                }
            }
            return registrationLimit;
        }

        public SecureLicense ShowForm(ISuperFormLimit limit, string pageId, object instance, Type licensedType, LicenseValidationRequestInfo info)
        {
            if (limit == null)
            {
                throw new ArgumentNullException("limit");
            }
            if (Limits.FindLimitById(((Limit)limit).LimitId) == null)
            {
                throw new SecureLicenseException("E_LimitDoesNotBelongToLicense");
            }
            return SecureLicenseManager.ShowForm(limit, pageId, instance, licensedType, info, null);
        }

        public SecureLicense ShowForm(Type limitType, string pageId, object instance, Type licensedType, LicenseValidationRequestInfo info)
        {
            if (limitType == null)
            {
                throw new ArgumentNullException("limitType");
            }
            ISuperFormLimit limit = Limits.FindLimitByType(limitType, true) as ISuperFormLimit;
            return ShowForm(limit, pageId, instance, licensedType, info);
        }

        internal bool CanLicenseComponent(string componentName)
        {
            if (_classes.Count != 0 && !_classes.Contains("*"))
            {
                return _classes.Contains(componentName);
            }
            return true;
        }

        [SecurityCritical]
        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("SecureLicense", ToXmlString());
        }

        private SecureLicense(SerializationInfo info, StreamingContext context)
        {
            _limits = new LimitCollection(this, this);
            FromXmlString(info.GetValue("SecureLicense", typeof(string)) as string);
        }
    }
}
