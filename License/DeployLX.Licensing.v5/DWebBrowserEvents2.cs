using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace DeployLX.Licensing.v5
{
	[ComImport]
	[InterfaceType(2)]
	[Guid("34A715A0-6587-11D0-924A-0020AFC7AC4D")]
	[TypeLibType(4112)]
	internal interface DWebBrowserEvents2
	{
		[MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall)]
		[DispId(102)]
		void StatusTextChange([In] [MarshalAs(UnmanagedType.BStr)] string Text);

		[MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall)]
		[DispId(108)]
		void ProgressChange([In] int Progress, [In] int ProgressMax);

		[MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall)]
		[DispId(105)]
		void CommandStateChange([In] int Command, [In] bool Enable);

		[MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall)]
		[DispId(106)]
		void DownloadBegin();

		[MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall)]
		[DispId(104)]
		void DownloadComplete();

		[MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall)]
		[DispId(113)]
		void TitleChange([In] [MarshalAs(UnmanagedType.BStr)] string Text);

		[MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall)]
		[DispId(112)]
		void PropertyChange([In] [MarshalAs(UnmanagedType.BStr)] string szProperty);

		[MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall)]
		[DispId(250)]
		void BeforeNavigate2([In] [MarshalAs(UnmanagedType.IDispatch)] object pDisp, [In] [MarshalAs(UnmanagedType.Struct)] ref object URL, [In] [MarshalAs(UnmanagedType.Struct)] ref object Flags, [In] [MarshalAs(UnmanagedType.Struct)] ref object TargetFrameName, [In] [MarshalAs(UnmanagedType.Struct)] ref object PostData, [In] [MarshalAs(UnmanagedType.Struct)] ref object Headers, [In] [Out] ref bool Cancel);

		[MethodImpl(MethodImplOptions.PreserveSig | MethodImplOptions.InternalCall)]
		[DispId(269)]
		void SetSecureLockIcon([In] int SecureLockIcon);
	}
}
