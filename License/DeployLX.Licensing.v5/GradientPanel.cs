using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public class GradientPanel : BufferedPanel
	{
		private Color _backColor2;

		private float _gradientAngle;

		private GradientStyle _gradientStyle;

		private Point _gradientOffset;

		private Point _scale = new Point(100, 100);

		private Image _image;

		private Point _imageOffset;

		[Category("Appearance")]
		public Color BackColor2
		{
			get
			{
				if (_backColor2 == Color.Empty)
				{
					return BackColor;
				}
				return _backColor2;
			}
			set
			{
				_backColor2 = value;
				base.Invalidate();
			}
		}

		[DefaultValue(0f)]
		[Category("Appearance")]
		[Description("The angle of the gradient.")]
		[NotifyParentProperty(true)]
		public float GradientAngle
		{
			get
			{
				return _gradientAngle;
			}
			set
			{
				_gradientAngle = value;
				base.Invalidate();
			}
		}

		[NotifyParentProperty(true)]
		[Description("Style of gradient to use.")]
		[Category("Appearance")]
		[DefaultValue(GradientStyle.Linear)]
		public GradientStyle GradientStyle
		{
			get
			{
				return _gradientStyle;
			}
			set
			{
				_gradientStyle = value;
				base.Invalidate();
			}
		}

		[NotifyParentProperty(true)]
		[Category("Appearance")]
		[Description("Offset of the starting point of the gradient.")]
		public Point GradientOffset
		{
			get
			{
				return _gradientOffset;
			}
			set
			{
				_gradientOffset = value;
				base.Invalidate();
			}
		}

		[Category("Appearance")]
		[NotifyParentProperty(true)]
		[Description("Amount to scale the gradient.")]
		public Point GradientScale
		{
			get
			{
				return _scale;
			}
			set
			{
				_scale = value;
				base.Invalidate();
			}
		}

		[Description("An image to display on the panel.")]
		[DefaultValue(null)]
		[NotifyParentProperty(true)]
		[Category("Appearance")]
		public Image Image
		{
			get
			{
				return _image;
			}
			set
			{
				_image = value;
				base.Invalidate();
			}
		}

		[NotifyParentProperty(true)]
		[Description("Offset of the image.")]
		[Category("Appearance")]
		public Point ImageOffset
		{
			get
			{
				return _imageOffset;
			}
			set
			{
				_imageOffset = value;
				base.Invalidate();
			}
		}

		public GradientPanel()
		{
			base.SetStyle(ControlStyles.ResizeRedraw | ControlStyles.SupportsTransparentBackColor, true);
		}

		private void ResetBackColor2()
		{
			_backColor2 = Color.Empty;
		}

		private bool ShouldSerializeBackColor2()
		{
			if (_backColor2 != Color.Empty)
			{
				return _backColor2 != BackColor;
			}
			return false;
		}

		private bool ShouldSerializeGradientOffset()
		{
			return _gradientOffset != Point.Empty;
		}

		private void ResetGradientOffset()
		{
			_gradientOffset = Point.Empty;
		}

		private bool ShouldSerializeGradientScale()
		{
			if (_scale.X == 100)
			{
				return _scale.Y != 100;
			}
			return true;
		}

		private void ResetGradientScale()
		{
			_scale = new Point(100, 100);
		}

		private bool ShouldSerializeImageOffset()
		{
			return _imageOffset != Point.Empty;
		}

		private void ResetImageOffset()
		{
			_imageOffset = Point.Empty;
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			if (BackgroundImage == null && ShouldSerializeBackColor2() && base.Width > 0 && base.Height > 0)
			{
				Rectangle clientRectangle = base.ClientRectangle;
				e.Graphics.SmoothingMode = SmoothingMode.HighSpeed;
				if (!_scale.IsEmpty || !_gradientOffset.IsEmpty || _gradientStyle == GradientStyle.Radial)
				{
					base.OnPaint(e);
				}
				if (!_scale.IsEmpty)
				{
					if (_scale.X != 0)
					{
						clientRectangle.Width = clientRectangle.Width * _scale.X / 100;
					}
					if (_scale.Y != 0)
					{
						clientRectangle.Height = clientRectangle.Height * _scale.Y / 100;
					}
				}
				if (!_gradientOffset.IsEmpty)
				{
					clientRectangle.X += _gradientOffset.X;
					clientRectangle.Y += _gradientOffset.Y;
				}
				switch (_gradientStyle)
				{
				case GradientStyle.Linear:
					using (LinearGradientBrush linearGradientBrush2 = new LinearGradientBrush(clientRectangle, BackColor, BackColor2, _gradientAngle, false))
					{
						linearGradientBrush2.WrapMode = WrapMode.TileFlipXY;
						e.Graphics.FillRectangle(linearGradientBrush2, clientRectangle);
					}
					break;
				case GradientStyle.Reflected:
					base.OnPaint(e);
					using (LinearGradientBrush linearGradientBrush = new LinearGradientBrush(clientRectangle, BackColor, BackColor2, _gradientAngle, false))
					{
						linearGradientBrush.WrapMode = WrapMode.TileFlipXY;
						ColorBlend colorBlend = new ColorBlend(3);
						colorBlend.Colors = new Color[3]
						{
							BackColor,
							BackColor2,
							BackColor
						};
						colorBlend.Positions = new float[3]
						{
							0f,
							0.5f,
							1f
						};
						linearGradientBrush.InterpolationColors = colorBlend;
						e.Graphics.FillRectangle(linearGradientBrush, clientRectangle);
					}
					break;
				case GradientStyle.Radial:
					using (GraphicsPath graphicsPath = new GraphicsPath())
					{
						graphicsPath.AddEllipse(clientRectangle);
						using (PathGradientBrush pathGradientBrush = new PathGradientBrush(graphicsPath))
						{
							pathGradientBrush.CenterColor = BackColor2;
							pathGradientBrush.SurroundColors = new Color[1]
							{
								BackColor
							};
							e.Graphics.FillPath(pathGradientBrush, graphicsPath);
						}
					}
					break;
				}
			}
			else
			{
				base.OnPaint(e);
			}
			if (_image != null)
			{
				e.Graphics.DrawImage(_image, _imageOffset);
			}
		}
	}
}
