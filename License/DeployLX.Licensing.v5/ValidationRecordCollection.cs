using System;
using System.Collections;
using System.Text;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public sealed class ValidationRecordCollection : WithEventsCollection
	{
		private delegate bool RecordFilter(ValidationRecord record);

		public ValidationRecord this[int index]
		{
			get
			{
				return base.List[index] as ValidationRecord;
			}
			set
			{
				base.List[index] = value;
			}
		}

		public ValidationRecordCollection()
		{
		}

		public ValidationRecordCollection(params ValidationRecord[] records)
		{
			AddRange(records);
		}

		public int Add(ValidationRecord record)
		{
			return base.List.Add(record);
		}

		public ValidationRecord Add(string errorCodeOrMessage, object relatedObject, Exception exception, ErrorSeverity severity, params object[] args)
		{
			ValidationRecord validationRecord = new ValidationRecord(errorCodeOrMessage, relatedObject, exception, severity, args);
			Add(validationRecord);
			return validationRecord;
		}

		public ValidationRecord Add(string errorCodeOrMessage, object relatedObject, params object[] args)
		{
			return Add(errorCodeOrMessage, relatedObject, null, ErrorSeverity.NotSet, args);
		}

		public ValidationRecord Add(string errorCodeOrMessage, params object[] args)
		{
			return Add(errorCodeOrMessage, null, null, ErrorSeverity.NotSet, args);
		}

		public ValidationRecord Add(string errorCodeOrMessage, object relatedObject, Exception exception, params object[] args)
		{
			return Add(errorCodeOrMessage, relatedObject, exception, ErrorSeverity.NotSet, args);
		}

		public void AddRange(ValidationRecord[] records)
		{
			if (records != null)
			{
				foreach (ValidationRecord record in records)
				{
					Add(record);
				}
			}
		}

		public void AddRange(ValidationRecordCollection records)
		{
			if (records != null)
			{
				foreach (ValidationRecord item in (IEnumerable)records)
				{
					Add(item);
				}
			}
		}

		public void Insert(int index, ValidationRecord record)
		{
			base.List.Insert(index, record);
		}

		public void Remove(ValidationRecord record)
		{
			base.List.Remove(record);
		}

		public void CopyTo(ValidationRecord[] array, int index)
		{
			base.List.CopyTo(array, index);
		}

		public int IndexOf(ValidationRecord record)
		{
			return base.List.IndexOf(record);
		}

		public bool Contains(ValidationRecord record)
		{
			return base.List.Contains(record);
		}

		public override string ToString()
		{
			return ToString(null);
		}

		public string ToString(SecureLicenseContext context)
		{
			if (context == null)
			{
				context = SecureLicenseManager.CurrentContext;
			}
			StringBuilder stringBuilder = new StringBuilder(128 * Count);
			for (int i = 0; i < Count; i++)
			{
				ValidationRecord validationRecord = this[i];
				validationRecord.AppendTo(stringBuilder, context, 0);
				if (i < Count - 1)
				{
					stringBuilder.Append(Environment.NewLine);
				}
			}
			return stringBuilder.ToString();
		}

		public string GetSummaryErrorMessage(string defaultMessage)
		{
			string text = FindRecord(ErrorSeverity.High, false, null);
			if (text != null)
			{
				return text;
			}
			text = FindRecord(ErrorSeverity.Normal, false, null);
			if (text != null)
			{
				return text;
			}
			text = FindRecord(ErrorSeverity.Low, true, delegate(ValidationRecord r)
			{
				if (r.ErrorCode != "E_DifferentProduct" && r.ErrorCode != "E_NoCompiledLicenses" && r.ErrorCode != "E_NoLicensesFound" && r.ErrorCode != "E_NoComponentInLicense")
				{
					return r.ErrorCode != "E_StoppedLooking";
				}
				return false;
			});
			if (text != null)
			{
				return text;
			}
			return defaultMessage;
		}

		private string FindRecord(ErrorSeverity severity, bool forward, RecordFilter filter)
		{
			if (filter == null)
			{
				filter = ((ValidationRecord r) => true);
			}
			int num = (!forward) ? (Count - 1) : 0;
			while (true)
			{
				if (!forward)
				{
					goto IL_0036;
				}
				if (num >= Count)
				{
					goto IL_0036;
				}
				goto IL_0043;
				IL_00ec:
				num += (forward ? 1 : (-1));
				continue;
				IL_0036:
				if (forward)
				{
					break;
				}
				if (num < 0)
				{
					break;
				}
				goto IL_0043;
				IL_0074:
				ValidationRecord validationRecord;
				bool flag;
				foreach (ValidationRecord item in (IEnumerable)validationRecord.SubRecords)
				{
					if (filter(item) && item.Severity == severity)
					{
						if (flag)
						{
							return validationRecord.Message + " " + item.Message;
						}
						return item.Message;
					}
				}
				goto IL_00ec;
				IL_0043:
				validationRecord = this[num];
				if (filter(validationRecord))
				{
					flag = false;
					if (validationRecord.Severity == severity)
					{
						flag = true;
						if (validationRecord.SubRecords.Count != 0)
						{
							goto IL_0074;
						}
						return validationRecord.Message;
					}
					goto IL_0074;
				}
				goto IL_00ec;
			}
			return null;
		}
	}
}
