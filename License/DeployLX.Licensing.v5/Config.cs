using Microsoft.Win32;
using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Reflection;
using System.Security;

namespace DeployLX.Licensing.v5
{
	public sealed class Config
	{
		internal const string CONFIG_SECTION = "licensing";

		internal const string REGISTRY_KEY = "Software\\XHEO INC\\DeployLX\\v4.0";

		private static LicenseConfigurationSettings _settings;

		private static string _sharedFolder;

		private static object _proxy;

		private static int _disableTrials = -1;

		private static LicenseConfigurationSettings Settings
		{
			get
			{
				if (_settings == null)
				{
					lock (typeof(Config))
					{
						_settings = (ConfigurationManager.GetSection("licensing") as LicenseConfigurationSettings);
						if (_settings == null)
						{
							_settings = new LicenseConfigurationSettings();
						}
					}
				}
				return _settings;
			}
		}

		public static string SharedFolder
		{
			get
			{
				if (_sharedFolder == null)
				{
					lock (typeof(Config))
					{
						if (_sharedFolder == null)
						{
							//Realisable - Remove and replace with redirection to \iman\config

							//string text = Settings.SharedFolder;
							//try
							//{
							//	if (text == null)
							//	{
							//		using (new Wow64Guard())
							//		{
							//			using (RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("Software\\XHEO INC\\DeployLX\\v4.0"))
							//			{
							//				if (registryKey != null)
							//				{
							//					text = (registryKey.GetValue("SharedLicenseFolder") as string);
							//				}
							//			}
							//		}
							//	}
							//	if (text == null)
							//	{
							//		using (new Wow64Guard())
							//		{
							//			using (RegistryKey registryKey2 = Registry.LocalMachine.OpenSubKey("Software\\XHEO INC\\DeployLX\\v4.0"))
							//			{
							//				if (registryKey2 != null)
							//				{
							//					text = (registryKey2.GetValue("SharedLicenseFolder") as string);
							//				}
							//			}
							//		}
							//	}
							//}
							//catch (SecurityException)
							//{
							//}
							//if (text == null)
							//{
							//	text = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "XHEO INC\\SharedLicenses");
							//}
							//text = (_sharedFolder = SafeToolbox.GetFullPath(text));

							string codeBase = Assembly.GetExecutingAssembly().CodeBase;

							UriBuilder uri = new UriBuilder(codeBase);
							string path = Uri.UnescapeDataString(uri.Path);
							path = Path.GetDirectoryName(path);

							String utilsAssemblyPath = Path.Combine(path, "Realisable.Utils.dll");

							if (!File.Exists(utilsAssemblyPath))
							{
								utilsAssemblyPath = Path.Combine(path, "bin\\Realisable.Utils.dll");
							}

							Assembly assembly = Assembly.LoadFrom(utilsAssemblyPath);
							Type configUtil = assembly.GetType("Realisable.Utils.ConfigurationUtil");

							MethodInfo info = configUtil.GetMethod("ConfigDirectory", BindingFlags.Static | BindingFlags.Public);
							_sharedFolder = info.Invoke(null, null).ToString();
						}
					}
				}
				return _sharedFolder;
			}
		}

		public static IWebProxy Proxy
		{
			get
			{
				if (_proxy == null)
				{
					lock (typeof(Config))
					{
						if (_proxy == null)
						{
							IWebProxy webProxy = null;
							webProxy = Settings.Proxy;
							if (webProxy == null)
							{
								RegistryKey registryKey = null;
								try
								{
									using (new Wow64Guard())
									{
										registryKey = Registry.CurrentUser.OpenSubKey("Software\\XHEO INC\\DeployLX\\v4.0");
										if (registryKey == null)
										{
											registryKey = Registry.LocalMachine.OpenSubKey("Software\\XHEO INC\\DeployLX\\v4.0");
										}
										if (registryKey != null)
										{
											string text = registryKey.GetValue("ProxyUsername") as string;
											string text2 = registryKey.GetValue("ProxyDomain") as string;
											string password = registryKey.GetValue("ProxyPassword") as string;
											string text3 = registryKey.GetValue("ProxyAddress") as string;
											if (text2 == null && text != null)
											{
												Toolbox.SplitUserName(text, out text, out text2);
											}
											if (text3 != null)
											{
												return Toolbox.MakeWebProxy(text3, text, password, text2);
											}
										}
									}
								}
								finally
								{
									if (registryKey != null)
									{
										registryKey.Close();
									}
								}
							}
							_proxy = (webProxy ?? new object());
						}
					}
				}
				return _proxy as IWebProxy;
			}
			set
			{
				_proxy = value;
			}
		}

		public static bool DisableTrials
		{
			get
			{
				if (_disableTrials == -1)
				{
					lock (typeof(Config))
					{
						if (_disableTrials == -1)
						{
							_disableTrials = (Settings.DisableTrials ? 1 : 0);
						}
					}
				}
				return _disableTrials == 1;
			}
		}

		public static int ServerConnectTimeout
		{
			get
			{
				return Settings.ServerConnectTimeout;
			}
		}

		private Config()
		{
		}

		internal static void SetProxy(IWebProxy proxy)
		{
			lock (typeof(Config))
			{
				_proxy = proxy;
			}
		}

		public static string[] GetSerialNumber(string applicationKey)
		{
			Check.NotNull(applicationKey, "applicationKey");
			if (Settings.SerialNumbers == null)
			{
				return null;
			}
			return Settings.SerialNumbers.GetValues(applicationKey);
		}
	}
}
