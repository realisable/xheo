namespace DeployLX.Licensing.v5
{
	public enum CustomFieldType
	{
		Text,
		MultiLine,
		Checkbox,
		Header = 4,
		Password,
		Custom = 0x10
	}
}
