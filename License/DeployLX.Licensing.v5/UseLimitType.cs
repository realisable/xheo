namespace DeployLX.Licensing.v5
{
	public enum UseLimitType
	{
		AtATime,
		Cumulative
	}
}
