using System;
using System.Collections;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	public class ExtensionPanel : SplitPanel
	{
		private readonly bool _autoExtensionRequest;

		private SkinnedPanel _extensionsPanel;

		private ShadowLabel _extensionNotice;

		private ShadowLabel _expiredNotice;

		private BufferedPanel _extensionsHost;

		private ThemeLabel _serialNumberLabel;

		private ThemeLabel _serialNumber;

		private SkinnedButton _purchase;

		private ShadowLabel _extensionTitle;

		private SkinnedButton _copyToClipboard;

		private Limit[] _limits;

		public Limit[] Limits
		{
			get
			{
				return _limits;
			}
		}

		public ExtensionPanel(bool autoExtensionRequest, params Limit[] limits)
			: base(null)
		{
			_autoExtensionRequest = autoExtensionRequest;
			InitializeComponent();
			_extensionsPanel.DockPadding.All = 15;
			ArrayList arrayList = new ArrayList();
			for (int i = 0; i < limits.Length; i++)
			{
				IExtendableLimit extendableLimit = (IExtendableLimit)limits[i];
				if (extendableLimit.CanExtend)
				{
					arrayList.Add(extendableLimit);
				}
			}
			_limits = (arrayList.ToArray(typeof(Limit)) as Limit[]);
		}

		private void InitializeComponent()
		{
			_extensionsPanel = new SkinnedPanel();
			_serialNumberLabel = new ThemeLabel();
			_serialNumber = new ThemeLabel();
			_extensionsHost = new BufferedPanel();
			_extensionNotice = new ShadowLabel();
			_extensionTitle = new ShadowLabel();
			_expiredNotice = new ShadowLabel();
			_copyToClipboard = new SkinnedButton();
			_purchase = new SkinnedButton();
			base.SidePanel.SuspendLayout();
			base.BodyPanel.SuspendLayout();
			_extensionsPanel.SuspendLayout();
			base.SuspendLayout();
			base.SidePanel.Controls.Add(_copyToClipboard);
			base.SidePanel.Controls.Add(_purchase);
			base.SidePanel.Location = new Point(0, 0);
			base.SidePanel.Size = new Size(170, 421);
			base.SidePanel.TabIndex = 0;
			base.SidePanel.Controls.SetChildIndex(_purchase, 0);
			base.SidePanel.Controls.SetChildIndex(_copyToClipboard, 0);
			base.BodyPanel.Controls.Add(_expiredNotice);
			base.BodyPanel.Controls.Add(_extensionNotice);
			base.BodyPanel.Controls.Add(_extensionTitle);
			base.BodyPanel.Controls.Add(_extensionsPanel);
			base.BodyPanel.Location = new Point(170, 0);
			base.BodyPanel.Size = new Size(514, 421);
			_extensionsPanel.BackColor = Color.Transparent;
			_extensionsPanel.BackgroundImageLayout = ImageLayout.None;
			_extensionsPanel.Controls.Add(_serialNumberLabel);
			_extensionsPanel.Controls.Add(_serialNumber);
			_extensionsPanel.Controls.Add(_extensionsHost);
			_extensionsPanel.Location = new Point(13, 176);
			_extensionsPanel.Name = "_extensionsPanel";
			_extensionsPanel.PaintFakeBackground = true;
			_extensionsPanel.Size = new Size(488, 226);
			_extensionsPanel.TabIndex = 1;
			_serialNumberLabel.Location = new Point(13, 13);
			_serialNumberLabel.Name = "_serialNumberLabel";
			_serialNumberLabel.Size = new Size(300, 20);
			_serialNumberLabel.TabIndex = 3;
			_serialNumberLabel.Text = "#UI_SerialNumber";
			_serialNumberLabel.ThemeFont = ThemeFont.Bold;
			_serialNumber.Location = new Point(28, 34);
			_serialNumber.Name = "_serialNumber";
			_serialNumber.Size = new Size(445, 33);
			_serialNumber.TabIndex = 2;
			_serialNumber.ThemeColor = new string[2]
			{
				"GlassPanelHighlightText",
				"GlassPanelText"
			};
			_extensionsHost.AutoScroll = true;
			_extensionsHost.BackColor = Color.Transparent;
			_extensionsHost.BackgroundImageLayout = ImageLayout.None;
			_extensionsHost.Location = new Point(15, 74);
			_extensionsHost.Name = "_extensionsHost";
			_extensionsHost.Size = new Size(458, 136);
			_extensionsHost.TabIndex = 0;
			_extensionNotice.Location = new Point(16, 111);
			_extensionNotice.Name = "_extensionNotice";
			_extensionNotice.Size = new Size(482, 58);
			_extensionNotice.TabIndex = 15;
			_extensionNotice.ThemeFont = ThemeFont.Medium;
			_extensionTitle.Location = new Point(14, 16);
			_extensionTitle.Name = "_extensionTitle";
			_extensionTitle.Size = new Size(482, 34);
			_extensionTitle.TabIndex = 14;
			_extensionTitle.ThemeColor = new string[2]
			{
				"PanelTitleText",
				"Text"
			};
			_expiredNotice.Location = new Point(16, 50);
			_expiredNotice.Name = "_expiredNotice";
			_expiredNotice.Size = new Size(482, 61);
			_expiredNotice.TabIndex = 15;
			_expiredNotice.ThemeFont = ThemeFont.Medium;
			_copyToClipboard.Location = new Point(20, 179);
			_copyToClipboard.Name = "_copyToClipboard";
			_copyToClipboard.ShouldDrawFocus = false;
			_copyToClipboard.Size = new Size(130, 25);
			_copyToClipboard.TabIndex = 3;
			_copyToClipboard.Text = "#UI_CopyToClipboard";
			_copyToClipboard.UseVisualStyleBackColor = true;
			_copyToClipboard.Click += _copyToClipboard_Click;
			_purchase.Location = new Point(20, 148);
			_purchase.Name = "_purchase";
			_purchase.ShouldDrawFocus = false;
			_purchase.Size = new Size(130, 25);
			_purchase.TabIndex = 4;
			_purchase.UseVisualStyleBackColor = true;
			_purchase.Click += _purchase_Click;
			base.AutoScaleDimensions = new SizeF(96f, 96f);
			KeepSupportVisible = true;
			base.Name = "ExtensionPanel";
			base.SidePanel.ResumeLayout(false);
			base.BodyPanel.ResumeLayout(false);
			_extensionsPanel.ResumeLayout(false);
			base.ResumeLayout(false);
		}

		protected override void HandleBuyNow()
		{
			SharedToolbox.OpenUrl(_purchase.Tag as string, true);
		}

		protected virtual void HandleExtendOnline()
		{
			base.ShowPanel(new ExtendOnlinePanel(_limits), HandleOnlinePanelResult);
		}

		private void HandleOnlinePanelResult(FormResult result)
		{
			switch (result)
			{
			case FormResult.Success:
				base.Return(FormResult.Success);
				break;
			case FormResult.Failure:
				base.Return(FormResult.Failure);
				break;
			}
		}

		protected virtual void HandleExtend()
		{
			bool flag = true;
			bool flag2 = false;
			foreach (Control control in _extensionsHost.Controls)
			{
				PromptMaskedEdit promptMaskedEdit = control as PromptMaskedEdit;
				if (promptMaskedEdit != null)
				{
					try
					{
						IExtendableLimit extendableLimit = promptMaskedEdit.Tag as IExtendableLimit;
						if (promptMaskedEdit.Text.Length != 0 && !(promptMaskedEdit.Text == extendableLimit.License.SerialNumberInfo.Prefix))
						{
							flag2 = true;
							if (!extendableLimit.ExtendByCode(base._superForm.Context, promptMaskedEdit.Text))
							{
								flag = false;
							}
						}
					}
					catch
					{
						flag = false;
					}
				}
			}
			if (flag && flag2)
			{
				base.Return(FormResult.Success);
			}
			else
			{
				string text;
				if (flag2)
				{
					text = SR.GetString("#UI_InvalidExtensionCode");
					if (base.SuperForm.Context.LatestValidationRecord != null)
					{
						text = text + "\r\n\r\nDetails: " + base.SuperForm.Context.LatestValidationRecord.Message;
					}
				}
				else
				{
					text = "#UI_MissingExtensionCode";
				}
				base.ShowMessageBox(text, "UI_InvalidExtensionCodeTitle", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		protected internal override void InitializePanel()
		{
			bool flag = true;
			bool flag2 = false;
			bool flag3 = false;
			string text = null;
			bool visible = false;
			Limit[] limits = _limits;
			for (int i = 0; i < limits.Length; i++)
			{
				IExtendableLimit extendableLimit = (IExtendableLimit)limits[i];
				if (extendableLimit.CanExtend)
				{
					if (base.SuperForm.Context.CheckExpired(extendableLimit))
					{
						visible = true;
					}
					if (!extendableLimit.IsSubscription)
					{
						flag = false;
					}
					if (extendableLimit.Servers.Count > 0 && extendableLimit.CanExtend)
					{
						flag2 = true;
					}
					if (extendableLimit.CanExtendByCode)
					{
						flag3 = true;
					}
					if (text == null && extendableLimit.PurchaseUrl != null)
					{
						text = Toolbox.ResolveUrl(extendableLimit.PurchaseUrl, base._superForm.Context).ToString();
					}
				}
			}
			_expiredNotice.Visible = visible;
			if (!_expiredNotice.Visible)
			{
				_extensionNotice.Top = _expiredNotice.Top;
				int bottom = _extensionsPanel.Bottom;
				_extensionsPanel.Top = _extensionNotice.Bottom + 10;
				_extensionsPanel.Height += bottom - _extensionsPanel.Bottom;
			}
			if (flag)
			{
				_extensionTitle.Text = SR.GetString("UI_SubscriptionTitle");
				_expiredNotice.Text = SR.GetString("UI_SubscriptionExpired", base._superForm.Context.SupportInfo.Product);
				if (flag2 && flag3)
				{
					_extensionNotice.Text = SR.GetString("UI_SubscriptionPrompt");
				}
				else if (flag2)
				{
					_extensionNotice.Text = SR.GetString("UI_SubscriptionPromptOnlineOnly");
				}
				else
				{
					_extensionNotice.Text = SR.GetString("UI_SubscriptionPromptCodeOnly");
				}
			}
			else
			{
				_extensionTitle.Text = SR.GetString("UI_ExtendTitle");
				_expiredNotice.Text = SR.GetString("UI_ExtensionExpired", base._superForm.Context.SupportInfo.Product);
				if (flag2 && flag3)
				{
					_extensionNotice.Text = SR.GetString("UI_ExtensionPrompt");
				}
				else if (flag2)
				{
					_extensionNotice.Text = SR.GetString("UI_ExtensionPromptOnlineOnly");
				}
				else
				{
					_extensionNotice.Text = SR.GetString("UI_ExtensionPromptCodeOnly");
				}
			}
			_serialNumber.Text = ((_limits[0].License.SerialNumber == null) ? SR.GetString("M_NA") : _limits[0].License.SerialNumber);
			if (text != null)
			{
				_purchase.Tag = text;
				_purchase.Text = (flag ? SR.GetString("UI_SubscriptionPurchase") : SR.GetString("UI_BuyNow"));
			}
			else
			{
				_purchase.Visible = false;
			}
			if (flag3)
			{
				Button button = base.AddBottomButton(flag ? "#UI_Renew" : "#UI_Extend");
				button.Click += _extend_Click;
				base._superForm.AcceptButton = button;
			}
			if (flag2)
			{
				Button button = base.AddBottomButton(flag ? "#UI_RenewOnline" : "#UI_ExtendOnline", 30);
				button.Click += _extendOnline_Click;
			}
			if (!_autoExtensionRequest)
			{
				Button button = base.AddBottomButton("#UI_GoBack");
				button.Image = Images.Back_png;
				button.TextImageRelation = TextImageRelation.ImageBeforeText;
				button.Click += _cancel_Click;
				base._superForm.CancelButton = button;
			}
			int num = 0;
			Limit[] limits2 = _limits;
			for (int j = 0; j < limits2.Length; j++)
			{
				IExtendableLimit extendableLimit2 = (IExtendableLimit)limits2[j];
				if (extendableLimit2.CanExtend)
				{
					ThemeLabel themeLabel = new ThemeLabel();
					themeLabel.ThemeColor = new string[3]
					{
						"GlassPanelHighlightText",
						"GlassPanelText",
						"Text"
					};
					themeLabel.AutoSize = true;
					themeLabel.Font = base.SuperForm.FixedFont;
					themeLabel.Text = base._superForm.Context.MakeExtensionLimitCode(extendableLimit2);
					themeLabel.Tag = extendableLimit2;
					ThemeLabel themeLabel2 = themeLabel;
					_extensionsHost.Controls.Add(themeLabel2);
					themeLabel2.Location = new Point(_extensionsHost.Width - themeLabel2.Width, num);
					themeLabel2.Anchor = (AnchorStyles.Top | AnchorStyles.Right);
					ThemeLabel themeLabel3 = new ThemeLabel();
					themeLabel3.Text = SR.GetString("UI_LimitCode");
					themeLabel3.AutoSize = true;
					themeLabel3.ThemeFont = ThemeFont.Bold;
					ThemeLabel themeLabel4 = themeLabel3;
					_extensionsHost.Controls.Add(themeLabel4);
					themeLabel4.Location = new Point(themeLabel2.Left - themeLabel4.Width - 8, num + 3);
					themeLabel2.Anchor = (AnchorStyles.Top | AnchorStyles.Right);
					ThemeLabel themeLabel5 = new ThemeLabel();
					themeLabel5.Text = string.Format("{0} {1} ({2})", flag ? SR.GetString("UI_RenewLabel") : SR.GetString("UI_ExtendLabel"), ((Limit)extendableLimit2).Name, extendableLimit2.GetUseDescription(base._superForm.Context));
					themeLabel5.ThemeFont = ThemeFont.Bold;
					themeLabel5.Location = new Point(0, num + 3);
					themeLabel5.AutoSize = true;
					ThemeLabel value = themeLabel5;
					_extensionsHost.Controls.Add(value);
					num += themeLabel2.Height + 3;
					LicenseCodeTextBox licenseCodeTextBox = new LicenseCodeTextBox();
					licenseCodeTextBox.Font = base.SuperForm.FixedFont;
					licenseCodeTextBox.Mask = extendableLimit2.CodeMask;
					licenseCodeTextBox.HideMaskOnLeave = true;
					licenseCodeTextBox.Tag = extendableLimit2;
					licenseCodeTextBox.BackColor = Color.Transparent;
					licenseCodeTextBox.CharacterSet = extendableLimit2.License.SerialNumberInfo.CharacterSet;
					licenseCodeTextBox.SetBounds(0, num, _extensionsHost.Width, licenseCodeTextBox.Height);
					licenseCodeTextBox.BackColor = SystemColors.Window;
					licenseCodeTextBox.Anchor = (AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right);
					_extensionsHost.Controls.Add(licenseCodeTextBox);
					num += licenseCodeTextBox.Height + 16;
				}
			}
			if (num + 88 < _extensionsPanel.Height)
			{
				_extensionsHost.SetBounds(15, 70, _extensionsHost.Width, num - 16);
				_extensionsPanel.Height = num + 72;
			}
			if (_extensionsPanel.Controls.Count > 1)
			{
				_extensionsPanel.Controls[1].Select();
			}
		}

		private void _extend_Click(object sender, EventArgs e)
		{
			HandleExtend();
		}

		private void _purchase_Click(object sender, EventArgs e)
		{
			HandleBuyNow();
		}

		private void _extendOnline_Click(object sender, EventArgs e)
		{
			HandleExtendOnline();
		}

		private void _cancel_Click(object sender, EventArgs e)
		{
			base.Return(FormResult.Incomplete);
		}

		private void _copyToClipboard_Click(object sender, EventArgs e)
		{
			StringBuilder stringBuilder = new StringBuilder();
			foreach (Control control in _extensionsHost.Controls)
			{
				ThemeLabel themeLabel = control as ThemeLabel;
				if (themeLabel != null && themeLabel.Tag is IExtendableLimit)
				{
					stringBuilder.AppendLine(TemplateFormatter.Format("M_CopyExtension", ExtendableLimit.EmailTemplateVariables, new string[2]
					{
						_serialNumber.Text,
						themeLabel.Text
					}));
				}
			}
			ClipboardToolbox.SetClipboard(DataFormats.Text, stringBuilder.ToString());
			base.ShowMessageBox("UI_DetailsCopied", "UI_DetailsCopiedTitle", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
		}

		protected internal override void UpdateFromTheme()
		{
			base.UpdateFromTheme();
			_extensionTitle.Font = base.SuperForm.TitleFont;
		}

		protected internal override void PanelShown(bool safeToChange)
		{
			base.PanelShown(safeToChange);
			if (safeToChange)
			{
				PopulatePotentialExtensionCode();
				foreach (Control control in _extensionsHost.Controls)
				{
					PromptMaskedEdit promptMaskedEdit = control as PromptMaskedEdit;
					if (promptMaskedEdit == null)
					{
						continue;
					}
					promptMaskedEdit.Focus();
					break;
				}
			}
		}

		private void PopulatePotentialExtensionCode()
		{
			if (_extensionsHost != null)
			{
				foreach (Control control in _extensionsHost.Controls)
				{
					PromptMaskedEdit promptMaskedEdit = control as PromptMaskedEdit;
					if (promptMaskedEdit != null && promptMaskedEdit.Text.Length <= 0)
					{
						IExtendableLimit limit = promptMaskedEdit.Tag as IExtendableLimit;
						try
						{
							promptMaskedEdit.Text = base.SuperForm.Context.GetPotentialExtensionFromClipboard(limit);
						}
						catch (ArgumentOutOfRangeException)
						{
						}
					}
				}
			}
		}

		protected internal override void FormActivated()
		{
			base.FormActivated();
			PopulatePotentialExtensionCode();
		}
	}
}
