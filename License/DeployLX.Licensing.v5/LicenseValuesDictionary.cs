using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;
using System.Security;
using System.Security.Permissions;
using System.Text;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public sealed class LicenseValuesDictionary : StringDictionary, ICollection, IEnumerable, ISerializable, IDictionary, IChange
	{
		private bool _readonly;

		private Stack _stack;

		[NonSerialized]
		private CollectionEventHandler _added;

		[NonSerialized]
		private CollectionEventHandler _removed;

		[NonSerialized]
		private CollectionEventHandler _changed;

		[NonSerialized]
		private ChangeEventHandler _ichanged;

		public override string this[string key]
		{
			get
			{
				return base[key];
			}
			set
			{
				AssertNotReadOnly();
				bool flag = ContainsKey(key);
				base[key] = value;
				CollectionEventArgs collectionEventArgs = new CollectionEventArgs(CollectionChangeAction.Add, new DictionaryEntry(key, value));
				if (!flag)
				{
					OnAdded(collectionEventArgs);
				}
				else
				{
					collectionEventArgs.Action = CollectionChangeAction.Refresh;
					OnChanged(collectionEventArgs);
				}
			}
		}

		bool IDictionary.IsFixedSize
		{
			get
			{
				return false;
			}
		}

		bool IDictionary.IsReadOnly
		{
			get
			{
				return false;
			}
		}

		ICollection IDictionary.Keys
		{
			get
			{
				return Keys;
			}
		}

		ICollection IDictionary.Values
		{
			get
			{
				return Values;
			}
		}

		object IDictionary.this[object key]
		{
			get
			{
				return ((StringDictionary)this)[key as string];
			}
			set
			{
				((StringDictionary)this)[key as string] = (value as string);
			}
		}

		public event CollectionEventHandler Added
		{
			add
			{
				lock (this)
				{
					_added = (CollectionEventHandler)Delegate.Combine(_added, value);
				}
			}
			remove
			{
				lock (this)
				{
					_added = (CollectionEventHandler)Delegate.Remove(_added, value);
				}
			}
		}

		public event CollectionEventHandler Removed
		{
			add
			{
				lock (this)
				{
					_removed = (CollectionEventHandler)Delegate.Combine(_removed, value);
				}
			}
			remove
			{
				lock (this)
				{
					_removed = (CollectionEventHandler)Delegate.Remove(_removed, value);
				}
			}
		}

		public event CollectionEventHandler Changed
		{
			add
			{
				lock (this)
				{
					_changed = (CollectionEventHandler)Delegate.Combine(_changed, value);
				}
			}
			remove
			{
				lock (this)
				{
					_changed = (CollectionEventHandler)Delegate.Remove(_changed, value);
				}
			}
		}

		event ChangeEventHandler IChange.Changed
		{
			add
			{
				lock (this)
				{
					_ichanged = (ChangeEventHandler)Delegate.Combine(_ichanged, value);
				}
			}
			remove
			{
				lock (this)
				{
					_ichanged = (ChangeEventHandler)Delegate.Remove(_ichanged, value);
				}
			}
		}

		private void OnAdded(CollectionEventArgs e)
		{
			if (e.Element != null)
			{
				if (_added != null)
				{
					_added(this, e);
				}
				OnChanged(e);
			}
		}

		private void OnRemoved(CollectionEventArgs e)
		{
			if (e.Element != null)
			{
				if (_removed != null)
				{
					_removed(this, e);
				}
				OnChanged(e);
			}
		}

		private void OnChanged(CollectionEventArgs e)
		{
			if (e.Element != null)
			{
				if (_changed != null)
				{
					_changed(this, e);
				}
				OnIChanged(e);
			}
		}

		private void OnIChanged(CollectionEventArgs e)
		{
			if (e.Element != null && _ichanged != null)
			{
				_ichanged(this, new ChangeEventArgs(null, this, e.Element, e.Action));
			}
		}

		public LicenseValuesDictionary()
		{
		}

		private void AssertNotReadOnly()
		{
			if (!_readonly)
			{
				return;
			}
			throw new SecureLicenseException("E_LicenseReadOnly");
		}

		public override void Add(string key, string value)
		{
			AssertNotReadOnly();
			base.Add(key, value);
			if (_added != null)
			{
				OnAdded(new CollectionEventArgs(CollectionChangeAction.Add, new DictionaryEntry(key, value)));
			}
		}

		public override void Clear()
		{
			AssertNotReadOnly();
			if (Count != 0)
			{
				ArrayList arrayList = new ArrayList();
				{
					IEnumerator enumerator = GetEnumerator();
					try
					{
						while (enumerator.MoveNext())
						{
							DictionaryEntry dictionaryEntry = (DictionaryEntry)enumerator.Current;
							arrayList.Add(dictionaryEntry);
						}
					}
					finally
					{
						IDisposable disposable = enumerator as IDisposable;
						if (disposable != null)
						{
							disposable.Dispose();
						}
					}
				}
				base.Clear();
				if (_removed == null && _changed == null)
				{
					return;
				}
				for (int i = 0; i < arrayList.Count; i++)
				{
					CollectionEventArgs e = new CollectionEventArgs(CollectionChangeAction.Remove, arrayList[i]);
					OnRemoved(e);
				}
			}
		}

		internal void ClearForReload()
		{
			base.Clear();
		}

		public override void Remove(string key)
		{
			AssertNotReadOnly();
			string value = ((StringDictionary)this)[key];
			base.Remove(key);
			if (_removed == null && _changed == null)
			{
				return;
			}
			OnRemoved(new CollectionEventArgs(CollectionChangeAction.Remove, new DictionaryEntry(key, value)));
		}

		public void RemoveWithPrefix(string prefix)
		{
			Check.NotNull(prefix, "prefix");
			ArrayList arrayList = new ArrayList();
			foreach (string key2 in Keys)
			{
				if (key2.Length >= prefix.Length && string.Compare(prefix, 0, key2, 0, prefix.Length, true) == 0)
				{
					arrayList.Add(key2);
				}
			}
			foreach (string item in arrayList)
			{
				Remove(item);
			}
		}

		void IChange.MakeReadOnly()
		{
			_readonly = true;
		}

		public void WriteToXml(XmlWriter writer, string collectionName)
		{
			Check.NotNull(writer, "writer");
			if (Count != 0)
			{
				Check.NotNull(writer, "writer");
				if (collectionName == null)
				{
					collectionName = "Values";
				}
				writer.WriteStartElement(collectionName);
				string[] array = new string[Keys.Count];
				Keys.CopyTo(array, 0);
				Array.Sort(array);
				string[] array2 = array;
				foreach (string text in array2)
				{
					writer.WriteStartElement("Value");
					writer.WriteAttributeString("name", text);
					writer.WriteCData(((StringDictionary)this)[text]);
					writer.WriteEndElement();
				}
				writer.WriteEndElement();
			}
		}

		public void ReadFromXml(XmlReader reader)
		{
			Check.NotNull(reader, "reader");
			if (Count > 0)
			{
				Clear();
			}
			reader.MoveToContent();
			string name = reader.Name;
			reader.Read();
			while (true)
			{
				string attribute;
				string text;
				if (!reader.EOF)
				{
					if (reader.IsStartElement())
					{
						attribute = reader.GetAttribute("name");
						text = reader.GetAttribute("value");
						if (attribute != null && attribute.Length > 0)
						{
							if (text == null)
							{
								reader.Read();
								text = reader.ReadString();
								reader.Read();
								if (text != null && text.Length != 0)
								{
									goto IL_0080;
								}
								continue;
							}
							goto IL_0080;
						}
						goto IL_0088;
					}
					if (reader.Name == name)
					{
						break;
					}
					reader.Read();
					continue;
				}
				return;
				IL_0080:
				Add(attribute, text);
				goto IL_0088;
				IL_0088:
				reader.Skip();
			}
			reader.Read();
		}

		public string ToXmlString()
		{
			using (MemoryStream memoryStream = new MemoryStream(512))
			{
				XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, new UTF8Encoding(false));
				xmlTextWriter.WriteStartDocument();
				xmlTextWriter.Indentation = 1;
				xmlTextWriter.IndentChar = '\t';
				xmlTextWriter.Formatting = Formatting.Indented;
				WriteToXml(xmlTextWriter, "Values");
				xmlTextWriter.Close();
				memoryStream.Flush();
				return Encoding.UTF8.GetString(memoryStream.ToArray());
			}
		}

		public void FromXmlString(string xml)
		{
			using (StringReader input = new StringReader(xml))
			{
				XmlTextReader xmlTextReader = new XmlTextReader(input);
				ReadFromXml(xmlTextReader);
				xmlTextReader.Close();
			}
		}

		internal void Push()
		{
			if (_stack == null)
			{
				_stack = new Stack();
			}
			DictionaryEntry[] array = new DictionaryEntry[Count];
			CopyTo(array, 0);
			_stack.Push(array);
		}

		internal void Pop()
		{
			if (_stack == null)
			{
				throw new InvalidOperationException();
			}
			DictionaryEntry[] array = _stack.Pop() as DictionaryEntry[];
			if (array == null)
			{
				throw new InvalidOperationException();
			}
			base.Clear();
			DictionaryEntry[] array2 = array;
			for (int i = 0; i < array2.Length; i++)
			{
				DictionaryEntry dictionaryEntry = array2[i];
				base.Add(dictionaryEntry.Key as string, dictionaryEntry.Value as string);
			}
		}

		void IDictionary.Add(object key, object value)
		{
			Add(key as string, value as string);
		}

		void IDictionary.Clear()
		{
			Clear();
		}

		bool IDictionary.Contains(object key)
		{
			return ContainsKey(key as string);
		}

		IDictionaryEnumerator IDictionary.GetEnumerator()
		{
			return GetEnumerator() as IDictionaryEnumerator;
		}

		void IDictionary.Remove(object key)
		{
			Remove(key as string);
		}

		[Obsolete("Declared only for serialization compatability, do not use.", true)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public void Add(string value)
		{
			throw new NotSupportedException();
		}

		[SecurityCritical]
		[SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("Collection", ToXmlString());
		}

		private LicenseValuesDictionary(SerializationInfo info, StreamingContext context)
		{
			FromXmlString(info.GetString("Collection"));
		}
	}
}
