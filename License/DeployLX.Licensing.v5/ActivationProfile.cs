using System;
using System.Collections;
using System.ComponentModel;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public sealed class ActivationProfile : IChange
	{
		private bool _isReadOnly;

		private bool _activatedThisTime;

		private int _tolerance = 3;

		private int _cumulativeTolerance = -1;

		private bool _allowCumulativeChanges;

		private MachineProfileEntryCollection _hardware = MachineProfile.GetDefaultProfile().HardwareList;

		private MachineType _machineType;

		private bool _invalidOnFileMoved;

		private string _hash;

		private string _unlockHash;

		private string _cumulativeHash;

		private string _unlockCode;

		private string _serverActivationId;

		private DateTime _unlockDate = DateTime.MinValue;

		internal int _referenceId;

		private string _name;

		private string _assignedName;

		internal ActivationLimit _limit;

		public int Tolerance
		{
			get
			{
				return _tolerance;
			}
			set
			{
				AssertNotReadOnly();
				if (_tolerance != value)
				{
					_tolerance = value;
					OnChanged(new ChangeEventArgs("Tolerance", this));
				}
			}
		}

		public int CumulativeTolerance
		{
			get
			{
				if (_cumulativeTolerance != -1)
				{
					return _cumulativeTolerance;
				}
				return _tolerance;
			}
			set
			{
				AssertNotReadOnly();
				if (_cumulativeTolerance != value)
				{
					_cumulativeTolerance = value;
					OnChanged(new ChangeEventArgs("CumulativeTolerance", this));
				}
			}
		}

		public bool AllowCumulativeChanges
		{
			get
			{
				return _allowCumulativeChanges;
			}
			set
			{
				AssertNotReadOnly();
				if (_allowCumulativeChanges != value)
				{
					_allowCumulativeChanges = value;
					OnChanged(new ChangeEventArgs("AllowCumulativeChanges", this));
				}
			}
		}

		public MachineProfileEntryCollection Hardware
		{
			get
			{
				return _hardware;
			}
		}

		public MachineType MachineType
		{
			get
			{
				return _machineType;
			}
			set
			{
				_machineType = value;
			}
		}

		public bool InvalidateOnFileMoved
		{
			get
			{
				return _invalidOnFileMoved;
			}
			set
			{
				AssertNotReadOnly();
				if (_invalidOnFileMoved != value)
				{
					_invalidOnFileMoved = value;
					OnChanged(new ChangeEventArgs("InvalidateOnFileMoved", this));
				}
			}
		}

		public string Hash
		{
			get
			{
				if (_unlockHash != null)
				{
					return _unlockHash;
				}
				return _hash;
			}
			set
			{
				AssertNotReadOnly();
				if (_hash != value)
				{
					if (value != null && value.Trim().Length == 0)
					{
						value = null;
					}
					_hash = value;
					OnChanged(new ChangeEventArgs("Hash", this));
					UnlockHash = null;
				}
			}
		}

		public string UnlockHash
		{
			get
			{
				return _unlockHash;
			}
			set
			{
				AssertNotReadOnly();
				if (_unlockHash != value)
				{
					if (value != null && value.Trim().Length == 0)
					{
						value = null;
					}
					_unlockHash = value;
					OnChanged(new ChangeEventArgs("UnlockHash", this));
				}
			}
		}

		public string CumulativeHash
		{
			get
			{
				if (_cumulativeHash != null)
				{
					return _cumulativeHash;
				}
				return Hash;
			}
			set
			{
				AssertNotReadOnly();
				if (_cumulativeHash != value)
				{
					_cumulativeHash = value;
					OnChanged(new ChangeEventArgs("CumulativeHash", this));
				}
			}
		}

		public string UnlockCode
		{
			get
			{
				return _unlockCode;
			}
			set
			{
				AssertNotReadOnly();
				if (_unlockCode != value)
				{
					if (value != null && value.Trim().Length == 0)
					{
						value = null;
					}
					_unlockCode = value;
					OnChanged(new ChangeEventArgs("UnlockCode", this));
				}
			}
		}

		public string ServerActivationId
		{
			get
			{
				return _serverActivationId;
			}
			set
			{
				AssertNotReadOnly();
				if (_serverActivationId != value)
				{
					_serverActivationId = value;
					OnChanged(new ChangeEventArgs("ServerActivationId", this));
				}
			}
		}

		public DateTime UnlockDate
		{
			get
			{
				return _unlockDate;
			}
			set
			{
				AssertNotReadOnly();
				if (_unlockDate != value)
				{
					_unlockDate = value;
					OnChanged(new ChangeEventArgs("UnlockDate", this));
				}
			}
		}

		public int ReferenceId
		{
			get
			{
				return _referenceId;
			}
		}

		public string Name
		{
			get
			{
				return _name ?? SR.GetString("M_Profile" + _machineType.ToString(), _referenceId);
			}
			set
			{
				AssertNotReadOnly();
				if (_name != value)
				{
					_name = value;
					OnChanged(new ChangeEventArgs("Name", this));
				}
			}
		}

		public string AssignedName
		{
			get
			{
				return _assignedName;
			}
			set
			{
				if (value != null && !(_assignedName == Name))
				{
					_assignedName = value.Trim();
				}
				else
				{
					_assignedName = null;
				}
			}
		}

		public ActivationLimit Limit
		{
			get
			{
				return _limit;
			}
		}

		public bool IsEmpty
		{
			get
			{
				if (_hash == null)
				{
					return _unlockHash == null;
				}
				return false;
			}
		}

		public event ChangeEventHandler Changed;

		private void OnChanged(ChangeEventArgs e)
		{
			if (this.Changed != null)
			{
				this.Changed(this, e);
			}
		}

		void IChange.MakeReadOnly()
		{
			_isReadOnly = true;
			_hardware.MakeReadOnly();
		}

		private void AssertNotReadOnly()
		{
			if (!_isReadOnly)
			{
				return;
			}
			throw new SecureLicenseException("E_ReadOnlyObject", "ActivationProfile");
		}

		public bool CheckProfile(SecureLicenseContext context, bool reportErrors)
		{
			if (_hash == null && _unlockHash == null)
			{
				return false;
			}

			if (_limit == null)
			{
				return false;
			}

			if (!CheckMachineType())
			{
				return false;
			}

			bool flag;
			if ((flag = (_limit.License.LicenseFile.LocationState != LocationState.Unaltered)) && _activatedThisTime)
			{
				flag = false;
			}

			if (flag && _invalidOnFileMoved)
			{
				if (reportErrors)
				{
					context.ReportError("E_FileMovedAndInvalidateOnMove", this, null, ErrorSeverity.Normal, this);
				}
				return false;
			}

			string[] array = null;
			if (_limit != null)
			{
				array = _limit.GetDeactivated();
			}
			if (_unlockCode != null)
			{
				if (array != null)
				{
					string[] array2 = array;
					foreach (string text in array2)
					{
						if (text.StartsWith("UC.") && string.Compare(text, 3, _unlockCode, 0, _unlockCode.Length, true) == 0)
						{
							if (reportErrors)
							{
								context.ReportError("E_ProfileWasDeactivated", this, null, ErrorSeverity.Low, this);
							}
							return false;
						}
					}
				}
				DateTime dateTime;
				if (!context.CheckActivationCode(_unlockCode, _limit, this, null, out dateTime))
				{
					if (reportErrors)
					{
						context.ReportError("E_InvalidUnlockCodeForProfile", this, null, this);
					}
					return false;
				}
				MachineProfile profile = new MachineProfile(_hardware);
				if (!CheckCumulativeProfile(flag, profile) && MachineProfile.CompareHash(_unlockHash, profile, flag) > _tolerance)
				{
					if (reportErrors)
					{
						context.ReportError("E_MachineMismatchCheck", this, null, this, _unlockHash);
					}
					return false;
				}
			}
			else
			{
				if (_serverActivationId == null)
				{
					return false;
				}
				if (array != null)
				{
					string serverActivationId = _serverActivationId;
					string[] array3 = array;
					foreach (string text2 in array3)
					{
						if (text2.StartsWith("SC.") && string.Compare(text2, 3, serverActivationId, 0, serverActivationId.Length, true) == 0)
						{
							if (reportErrors)
							{
								context.ReportError("E_ProfileWasDeactivated", this, null, ErrorSeverity.Low, this);
							}
							return false;
						}
					}
				}
				MachineProfile profile2 = new MachineProfile(_hardware);
				if (!CheckCumulativeProfile(flag, profile2) && MachineProfile.CompareHash(_hash, profile2, flag) > _tolerance)
				{
					if (reportErrors)
					{
						context.ReportError("E_MachineMismatchCheck", this, null, this, _hash);
					}
					return false;
				}
			}
			return true;
		}

		private bool CheckMachineType()
		{
			if (_machineType == MachineType.Any)
			{
				return true;
			}
			if (_machineType == MachineType.Virtual)
			{
				return MachineProfile.IsVirtualMachine;
			}
			if (_machineType == MachineType.Physical)
			{
				return !MachineProfile.IsVirtualMachine;
			}
			if (_machineType == MachineType.Laptop)
			{
				return MachineProfile.IsLaptop;
			}
			return _machineType == MachineType.Desktop;
		}

		private bool CheckCumulativeProfile(bool fileMoved, MachineProfile profile)
		{
			if (_allowCumulativeChanges)
			{
				if (CumulativeHash != null && MachineProfile.CompareHash(CumulativeHash, profile, fileMoved) <= CumulativeTolerance)
				{
					string comparableHash = profile.GetComparableHash(false, null);
					if (comparableHash != _cumulativeHash)
					{
						_cumulativeHash = comparableHash;
						OnChanged(new ChangeEventArgs("CumulativeHash", this));
					}
					return true;
				}
				return false;
			}
			return false;
		}

		public bool CheckProfile(SecureLicenseContext context)
		{
			return CheckProfile(context, false);
		}

		internal bool ReadFromXml(XmlReader reader)
		{
			Check.NotNull(reader, "reader");
			_name = reader.GetAttribute("name");
			string attribute = reader.GetAttribute("refid");
			if (attribute == null)
			{
				return false;
			}
			_referenceId = SafeToolbox.FastParseInt32(attribute);
			attribute = reader.GetAttribute("tolerance");
			if (attribute != null)
			{
				_tolerance = SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_tolerance = 3;
			}
			attribute = reader.GetAttribute("cumulativeTolerance");
			if (attribute != null)
			{
				_cumulativeTolerance = SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_cumulativeTolerance = -1;
			}
			attribute = reader.GetAttribute("machineType");
			if (attribute != null)
			{
				_machineType = (MachineType)SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_machineType = MachineType.Any;
			}
			attribute = reader.GetAttribute("invalidateOnFileMoved");
			if (attribute != null)
			{
				_invalidOnFileMoved = (attribute == "true");
			}
			else
			{
				_invalidOnFileMoved = false;
			}
			_hash = reader.GetAttribute("hash");
			_serverActivationId = reader.GetAttribute("serverActivationId");
			_unlockCode = reader.GetAttribute("unlockCode");
			_unlockHash = reader.GetAttribute("unlockHash");
			_cumulativeHash = reader.GetAttribute("cumulativeHash");
			_allowCumulativeChanges = (reader.GetAttribute("allowCumulativeChanges") == "true");
			_assignedName = reader.GetAttribute("assignedName");
			attribute = reader.GetAttribute("unlockDate");
			if (attribute != null)
			{
				_unlockDate = SafeToolbox.FastParseSortableDate(attribute);
			}
			else
			{
				_unlockDate = DateTime.MinValue;
			}
			_hardware.Clear();
			reader.Read();
			if (reader.IsStartElement() && reader.Name == "Entry")
			{
				while (!reader.EOF)
				{
					if (reader.IsStartElement())
					{
						if (reader.Name == "Entry")
						{
							MachineProfileEntry machineProfileEntry = new MachineProfileEntry();
							machineProfileEntry.ReadFromXml(reader);
							_hardware.Add(machineProfileEntry);
						}
						else
						{
							reader.Skip();
						}
						continue;
					}
					reader.Read();
					break;
				}
			}
			else if (_hardware.Count == 0)
			{
				_hardware = MachineProfile.GetDefaultProfile().HardwareList;
			}
			return true;
		}

		internal void WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			Check.NotNull(writer, "writer");
			writer.WriteStartElement("Profile");
			writer.WriteAttributeString("refid", _referenceId.ToString());
			writer.WriteAttributeString("tolerance", _tolerance.ToString());
			if (_cumulativeTolerance != -1)
			{
				writer.WriteAttributeString("cumulativeTolerance", _cumulativeTolerance.ToString());
			}
			if (_allowCumulativeChanges)
			{
				writer.WriteAttributeString("allowCumulativeChanges", "true");
			}
			if (_name != null)
			{
				writer.WriteAttributeString("name", _name);
			}
			if (_machineType != 0)
			{
				int machineType = (int)_machineType;
				writer.WriteAttributeString("machineType", machineType.ToString());
			}
			if (_invalidOnFileMoved)
			{
				writer.WriteAttributeString("invalidateOnFileMoved", "true");
			}
			if (_hash != null)
			{
				writer.WriteAttributeString("hash", _hash);
			}
			if (_serverActivationId != null)
			{
				writer.WriteAttributeString("serverActivationId", _serverActivationId);
			}
			if (signing == LicenseSaveType.Normal && _assignedName != null)
			{
				writer.WriteAttributeString("assignedName", _assignedName);
			}
			if (signing == LicenseSaveType.Normal && _unlockHash != null)
			{
				writer.WriteAttributeString("unlockHash", _unlockHash);
			}
			if (signing == LicenseSaveType.Normal && _unlockCode != null)
			{
				writer.WriteAttributeString("unlockCode", _unlockCode);
			}
			if (signing == LicenseSaveType.Normal && _unlockDate != DateTime.MinValue)
			{
				writer.WriteAttributeString("unlockDate", SafeToolbox.FormatSortableDate(_unlockDate));
			}
			if (signing == LicenseSaveType.Normal && _cumulativeHash != null)
			{
				writer.WriteAttributeString("cumulativeHash", _cumulativeHash);
			}
			if (!MachineProfile.CheckIsDefault(_hardware))
			{
				foreach (MachineProfileEntry item in (IEnumerable)_hardware)
				{
					item.WriteToXml(writer);
				}
			}
			writer.WriteEndElement();
		}

		public MachineProfile MakeMachineProfile()
		{
			return new MachineProfile(_hardware);
		}

		public bool UnlockByCode(SecureLicenseContext context, string unlockCode)
		{
			return UnlockByCode(context, unlockCode, null);
		}

		internal bool UnlockByCode(SecureLicenseContext context, string unlockCode, string hash)
		{
			Check.NotNull(unlockCode, "unlockCode");
			if (_limit != null)
			{
				string[] deactivated = _limit.GetDeactivated();
				if (deactivated != null)
				{
					string[] array = deactivated;
					foreach (string text in array)
					{
						if (text.StartsWith("UC.") && string.Compare(text, 3, unlockCode, 0, unlockCode.Length, true) == 0)
						{
							return false;
						}
					}
				}
			}
			if (hash == null)
			{
				hash = MakeMachineProfile().GetComparableHash(false, null);
			}
			DateTime t;
			if (!context.CheckActivationCode(unlockCode, _limit, this, hash, out t))
			{
				return false;
			}
			if (t < context.CurrentDateAndTime)
			{
				return false;
			}
			_unlockHash = hash;
			_unlockCode = unlockCode;
			_unlockDate = context.CurrentDateAndTime;
			_cumulativeHash = null;
			_activatedThisTime = true;
			Limit.NotifyUnlocked(context, this);
			OnChanged(new ChangeEventArgs("SerialNumber", this));
			return true;
		}

		public bool Unlock(string hash, IServerRequestContext context)
		{
			AssertNotReadOnly();
			if (!CanUnlock(hash))
			{
				return false;
			}
			_unlockCode = null;
			_unlockHash = null;
			_cumulativeHash = null;
			_unlockDate = ((context == null) ? DateTime.UtcNow : context.CurrentDateAndTime);
			_assignedName = ((context == null) ? null : context.MachineName);
			_serverActivationId = Guid.NewGuid().ToString("N");
			Hash = hash;
			return true;
		}

		public bool Unlock(string hash)
		{
			return Unlock(hash, null);
		}

		public bool CanUnlock(string hash)
		{
			switch (_machineType)
			{
			case MachineType.Desktop:
				if (!MachineProfile.IsLaptopHash(hash))
				{
					break;
				}
				return false;
			case MachineType.Laptop:
				if (MachineProfile.IsLaptopHash(hash))
				{
					break;
				}
				return false;
			}
			return true;
		}

		internal void UpdateFromServer(ActivationProfile copy)
		{
			_hash = copy.Hash;
			_unlockCode = null;
			_unlockHash = null;
			_cumulativeHash = null;
			_unlockDate = copy._unlockDate;
			_tolerance = copy._tolerance;
			_serverActivationId = copy._serverActivationId;
			_activatedThisTime = true;
			OnChanged(new ChangeEventArgs("Hash", this));
		}

		public override string ToString()
		{
			string text = null;
			text = ((_name == null) ? SR.GetString("M_Profile" + _machineType.ToString(), _referenceId) : string.Format("{0} ({1})", _name, _referenceId));
			if (IsEmpty)
			{
				return text;
			}
			return text + '*';
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void Clear()
		{
			_unlockCode = null;
			_unlockDate = DateTime.MinValue;
			_unlockHash = null;
			_cumulativeHash = null;
		}
	}
}
