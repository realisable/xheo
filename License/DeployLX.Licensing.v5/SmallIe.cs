using System;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security;
using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	[Clsid("{8856f961-340a-11d0-a96b-00c04fd705a2}")]
	internal sealed class SmallIe : AxHost, DWebBrowserEvents2
	{
		public delegate void TextChangedHandler(object sender, string text);

		public delegate void ProgressChangedHandler(object sender, int progress, int max);

		public delegate void CommandStateChangedHandler(object sender, IeToolbarCommand command, bool enabled);

		public delegate void SecureIconChangedHandler(object sender, SecureLockIcon newState);

		public delegate bool BeforeNavigateHandler(object sender, string url, string targetFrame, byte[] postData, string headers);

		[Flags]
		public enum IeToolbarCommand
		{
			NavigateForward = 0x1,
			NavigateBackward = 0x0,
			UpdateCommands = -1
		}

		public enum SecureLockIcon
		{
			Unsecure,
			Mixed,
			UnknownBits,
			Bits40,
			Bits56,
			Fortezza,
			Bits128
		}

		public enum OleCmdExecOption
		{
			DoDefault,
			DontPromptUser = 2,
			PromptUser = 1,
			ShowHelp = 3
		}

		public enum OleCmdId
		{
			AllowUilessSaveAs = 46,
			ClearSelection = 18,
			Close = 45,
			Copy = 12,
			Cut = 11,
			Delete = 33,
			DontDownloadCss = 47,
			EnableInteraction = 36,
			Find = 0x20,
			FocusViewControls = 57,
			FocusViewControlsQuery,
			GetPrintTemplate = 52,
			GetZoomRange = 20,
			HideToolbars = 24,
			HttpEquiv = 34,
			HttpEquivDone,
			New = 2,
			OnToolbarActivated = 0x1F,
			OnUnload = 37,
			Open = 1,
			PageActionBlocked = 55,
			PageActionUiQuery,
			PageSetup = 8,
			Paste = 13,
			PasteSpecial,
			PreRefresh = 39,
			Print = 6,
			Print2 = 49,
			PrintPreview = 7,
			PrintPreview2 = 50,
			Properties = 10,
			PropertyBag2 = 38,
			Redo = 0x10,
			Refresh = 22,
			Save = 3,
			SaveAs,
			SaveCopyAs,
			SelectAll = 17,
			SetDownloadState = 29,
			SetPrintTemplate = 51,
			SetProgressMax = 25,
			SetProgressPos,
			SetProgressText,
			SetTitle,
			ShowFind = 42,
			ShowMessage = 41,
			ShowPageActionMenu = 59,
			ShowPageSetup = 43,
			ShowPrint,
			ShowScriptError = 40,
			Spell = 9,
			Stop = 23,
			StopDownload = 30,
			Undo = 0xF,
			UpdateCommands = 21,
			UpdatePageStatus = 48,
			Zoom = 19
		}

		[StructLayout(LayoutKind.Sequential, Size = 1)]
		private struct HR
		{
			public const uint S_OK = 0u;

			public const uint S_FALSE = 1u;

			public const uint E_PENDING = 2147483658u;

			public const uint E_NOINTERFACE = 2147500034u;

			public const uint CLASS_E_NOAGGREGATION = 2147746064u;

			public const uint INET_E_USE_DEFAULT_PROTOCOLHANDLER = 2148270097u;

			public const uint INET_E_OBJECT_NOT_FOUND = 2148270086u;

			public const uint INET_E_QUERYOPTION_UNKNOWN = 2148270099u;
		}

		[ComImport]
		[Guid("00000001-0000-0000-C000-000000000046")]
		[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
		private interface IClassFactory
		{
			[PreserveSig]
			uint CreateInstance([In] IntPtr pUnkOuter, [In] ref Guid riid, out IntPtr ppvObject);

			[PreserveSig]
			uint LockServer([In] bool fLock);
		}

		[ComImport]
		[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
		[Guid("79eac9e7-baf9-11ce-8c82-00aa004ba90b")]
		private interface IInternetSession
		{
			[PreserveSig]
			uint RegisterNamespace([In] [MarshalAs(UnmanagedType.Interface)] IClassFactory pCF, [In] ref Guid rclsid, [In] [MarshalAs(UnmanagedType.LPWStr)] string pwzProtocol, [In] uint cPatterns, [In] IntPtr ppwzPatterns, [In] uint dwReserved);

			void UnregisterNamespace([In] [MarshalAs(UnmanagedType.Interface)] IClassFactory pCF, [In] [MarshalAs(UnmanagedType.LPWStr)] string pwzProtocol);

			void RegisterMimeFilter([In] [MarshalAs(UnmanagedType.Interface)] IClassFactory pCF, [In] ref Guid rclsid, [In] [MarshalAs(UnmanagedType.LPWStr)] string pwzType);

			void UnregisterMimeFilter([In] [MarshalAs(UnmanagedType.Interface)] IClassFactory pCF, [In] [MarshalAs(UnmanagedType.LPWStr)] string pwzType);

			void CreateBinding([In] IntPtr pBC, [In] [MarshalAs(UnmanagedType.LPWStr)] string swzUrl, [In] IntPtr pUnkOuter, [Out] IntPtr ppUnk, [Out] IntPtr ppOInetProt, [In] uint dwOption);

			void SetSessionOption([In] uint dwOption, [In] [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 2)] byte[] pBuffer, [In] uint dwBufferLength, [In] uint dwReserved);

			void GetSessionOption([In] uint dwOption, [In] [Out] [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 2)] byte[] pBuffer, [In] [Out] ref uint pdwBufferLength, [In] uint dwReserved);
		}

		[ComImport]
		[Guid("79eac9e4-baf9-11ce-8c82-00aa004ba90b")]
		[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
		private interface IInternetProtocol
		{
			[PreserveSig]
			uint Start([In] [MarshalAs(UnmanagedType.LPWStr)] string szUrl, [In] [MarshalAs(UnmanagedType.Interface)] IInternetProtocolSink pOIProtSink, [In] [MarshalAs(UnmanagedType.Interface)] IInternetBindInfo pOIBindInfo, [In] uint grfPI, [In] IntPtr dwReserved);

			[PreserveSig]
			uint Continue([In] IntPtr pProtocolData);

			[PreserveSig]
			uint Abort([In] uint hrReason, [In] uint dwOptions);

			[PreserveSig]
			uint Terminate([In] uint dwOptions);

			[PreserveSig]
			uint Suspend();

			[PreserveSig]
			uint Resume();

			[PreserveSig]
			uint Read([In] IntPtr pv, [In] uint cb, out uint pcbRead);

			[PreserveSig]
			uint Seek([In] long dlibMove, [In] uint dwOrigin, out ulong plibNewPosition);

			[PreserveSig]
			uint LockRequest([In] uint dwOptions);

			[PreserveSig]
			uint UnlockRequest();
		}

		[ComImport]
		[Guid("79eac9e5-baf9-11ce-8c82-00aa004ba90b")]
		[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
		private interface IInternetProtocolSink
		{
			[PreserveSig]
			uint Switch([In] IntPtr pProtocolData);

			[PreserveSig]
			uint ReportProgress([In] uint ulStatusCode, [In] [MarshalAs(UnmanagedType.LPWStr)] string szStatusText);

			[PreserveSig]
			uint ReportData([In] uint grfBSCF, [In] uint ulProgress, [In] uint ulProgressMax);

			[PreserveSig]
			uint ReportResult([In] uint hrResult, [In] uint dwError, [In] [MarshalAs(UnmanagedType.LPWStr)] string szResult);
		}

		[ComImport]
		[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
		[Guid("79eac9e1-baf9-11ce-8c82-00aa004ba90b")]
		private interface IInternetBindInfo
		{
			[PreserveSig]
			uint GetBindInfo(out uint grfBINDF, [In] [Out] IntPtr pbindinfo);

			[PreserveSig]
			uint GetBindString([In] uint ulStringType, [In] [Out] [MarshalAs(UnmanagedType.LPWStr)] ref string ppwzStr, [In] uint cEl, [In] [Out] ref uint pcElFecthed);
		}

		[ComImport]
		[Guid("79eac9ec-baf9-11ce-8c82-00aa004ba90b")]
		[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
		private interface IInternetProtocolInfo
		{
			[PreserveSig]
			uint ParseUrl([MarshalAs(UnmanagedType.LPWStr)] string pwzUrl, uint ParseAction, uint dwParseFlags, IntPtr pwzResult, uint cchResult, out uint pcchResult, uint dwReserved);

			[PreserveSig]
			uint CombineUrl([MarshalAs(UnmanagedType.LPWStr)] string pwzBaseUrl, [MarshalAs(UnmanagedType.LPWStr)] string pwzRelativeUrl, uint dwCombineFlags, IntPtr pwzResult, uint cchResult, out uint pcchResult, uint dwReserved);

			[PreserveSig]
			uint CompareUrl([MarshalAs(UnmanagedType.LPWStr)] string pwzUrl1, [MarshalAs(UnmanagedType.LPWStr)] string pwzUrl2, uint dwCompareFlags);

			[PreserveSig]
			uint QueryInfo([MarshalAs(UnmanagedType.LPWStr)] string pwzUrl, uint QueryOption, uint dwQueryFlags, IntPtr pBuffer, uint cbBuffer, ref uint pcbBuf, uint dwReserved);
		}

		[Guid("12A06489-7498-4492-BB48-12D35A470AE9")]
		private sealed class ProtocolClassFactory : IClassFactory, IInternetProtocolInfo
		{
			private static readonly byte[] _true = BitConverter.GetBytes(1);

			private static readonly byte[] _false = new byte[4];

			public uint CreateInstance(IntPtr pUnkOuter, ref Guid riid, out IntPtr ppvObject)
			{
				ppvObject = IntPtr.Zero;
				if (pUnkOuter != IntPtr.Zero)
				{
					return 2147746064u;
				}
				ppvObject = IntPtr.Zero;
				if (riid == Marshal.GenerateGuidForType(typeof(IInternetProtocol)))
				{
					ClrProtocol o = new ClrProtocol();
					ppvObject = Marshal.GetComInterfaceForObject(o, typeof(IInternetProtocol));
					return 0u;
				}
				return 2147500034u;
			}

			public uint LockServer(bool fLock)
			{
				return 0u;
			}

			public uint ParseUrl(string pwzUrl, uint ParseAction, uint dwParseFlags, IntPtr pwzResult, uint cchResult, out uint pcchResult, uint dwReserved)
			{
				pcchResult = 0u;
				return 2148270097u;
			}

			public uint CombineUrl(string pwzBaseUrl, string pwzRelativeUrl, uint dwCombineFlags, IntPtr pwzResult, uint cchResult, out uint pcchResult, uint dwReserved)
			{
				pcchResult = 0u;
				return 2148270097u;
			}

			public uint CompareUrl(string pwzUrl1, string pwzUrl2, uint dwCompareFlags)
			{
				return 2148270097u;
			}

			public uint QueryInfo(string pwzUrl, uint QueryOption, uint dwQueryFlags, IntPtr pBuffer, uint cbBuffer, ref uint pcbBuf, uint dwReserved)
			{
				if (QueryOption == 7)
				{
					IDictionaryEnumerator enumerator = _protocols.GetEnumerator();
					try
					{
						while (enumerator.MoveNext())
						{
							ICustomProtocol customProtocol = ((DictionaryEntry)enumerator.Current).Value as ICustomProtocol;
							if (string.Compare(pwzUrl, 0, customProtocol.Scheme, 0, customProtocol.Scheme.Length, true) != 0)
							{
								continue;
							}
							Marshal.Copy(customProtocol.Navigate ? _true : _false, 0, pBuffer, 4);
							pcbBuf = 4u;
							return 0u;
						}
					}
					finally
					{
						IDisposable disposable = enumerator as IDisposable;
						if (disposable != null)
						{
							disposable.Dispose();
						}
					}
				}
				return 2148270097u;
			}
		}

		[Guid("22F8F04F-556F-4DE5-90D6-C3817ED609E8")]
		private sealed class ClrProtocol : IDisposable, IInternetProtocol
		{
			private IInternetProtocolSink _sink;

			private Stream _stream;

			private byte[] _readBuffer = new byte[8192];

			public void Dispose()
			{
				if (_stream != null)
				{
					_stream.Close();
				}
				GC.SuppressFinalize(this);
			}

			public uint Start(string szUrl, IInternetProtocolSink pOIProtSink, IInternetBindInfo pOIBindInfo, uint grfPI, IntPtr dwReserved)
			{
				switch (grfPI)
				{
				case 1u:
					return 0u;
				default:
					return 2148270097u;
				case 0u:
				{
					_sink = pOIProtSink;
					_sink.ReportProgress(1u, "Locating " + szUrl);
					Uri uri = new Uri(szUrl);
					ICustomProtocol customProtocol = null;
					IDictionaryEnumerator enumerator = _protocols.GetEnumerator();
					try
					{
						while (enumerator.MoveNext())
						{
							ICustomProtocol customProtocol2 = ((DictionaryEntry)enumerator.Current).Value as ICustomProtocol;
							if (string.Compare(uri.Scheme, customProtocol2.Scheme, true) == 0 && customProtocol2.CanProcessUri(uri))
							{
								customProtocol = customProtocol2;
								_stream = customProtocol.GetResourceStream(uri);
								break;
							}
						}
					}
					finally
					{
						IDisposable disposable = enumerator as IDisposable;
						if (disposable != null)
						{
							disposable.Dispose();
						}
					}
					if (customProtocol != null && !customProtocol.Navigate)
					{
						_sink.ReportData(4u, 0u, 0u);
						_sink.ReportResult(0u, 204u, "");
						return 0u;
					}
					if (_stream == null)
					{
						_sink.ReportResult(2148270086u, 404u, "Not found.");
						return 2148270086u;
					}
					_sink.ReportData(4u, 0u, (uint)_stream.Length);
					_sink.ReportProgress(13u, "");
					return 0u;
				}
				}
			}

			public uint Continue(IntPtr pProtocolData)
			{
				return 0u;
			}

			public uint Abort(uint hrReason, uint dwOptions)
			{
				return 0u;
			}

			public uint Terminate(uint dwOptions)
			{
				if (_stream != null)
				{
					_stream.Close();
					_stream = null;
				}
				return 0u;
			}

			public uint Suspend()
			{
				return 0u;
			}

			public uint Resume()
			{
				return 0u;
			}

			public uint Read(IntPtr pv, uint cb, out uint pcbRead)
			{
				if (_stream == null)
				{
					pcbRead = 0u;
					return 1u;
				}
				cb = (uint)Math.Min(cb, _readBuffer.Length);
				pcbRead = (uint)_stream.Read(_readBuffer, 0, (int)cb);
				Marshal.Copy(_readBuffer, 0, pv, (int)pcbRead);
				_sink.ReportProgress(5u, "Downloading...");
				if (pcbRead == 0)
				{
					_sink.ReportData(4u, 100u, 100u);
					_sink.ReportResult(0u, 200u, "200 SUCCESS");
					if (_stream != null)
					{
						_stream.Close();
					}
					_stream = null;
				}
				if (pcbRead >= cb)
				{
					return 0u;
				}
				return 1u;
			}

			public uint Seek(long dlibMove, uint dwOrigin, out ulong plibNewPosition)
			{
				plibNewPosition = 0uL;
				return 0u;
			}

			public uint LockRequest(uint dwOptions)
			{
				return 0u;
			}

			public uint UnlockRequest()
			{
				return 0u;
			}
		}

		private object _ocx;

		private Type _ocxType;

		private string _firstUrl;

		private ConnectionPointCookie _cookie;

		private bool _canGoBack;

		private bool _canGoForward;

		private static ProtocolClassFactory _pcf;

		internal static Hashtable _protocols;

		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public string LocationUrl
		{
			get
			{
				if (_ocxType == null)
				{
					return null;
				}
				return _ocxType.InvokeMember("LocationURL", BindingFlags.Instance | BindingFlags.Public | BindingFlags.GetProperty, null, _ocx, new object[0]) as string;
			}
			set
			{
				string url = null;
				if (value == null || value.ToString().Length == 0)
				{
					url = "about:blank";
				}
				Navigate(url);
			}
		}

		public bool CanGoBack
		{
			get
			{
				return _canGoBack;
			}
		}

		public bool CanGoForward
		{
			get
			{
				return _canGoForward;
			}
		}

		public event TextChangedHandler StatusTextChanged;

		public event ProgressChangedHandler ProgressChanged;

		public event CommandStateChangedHandler CommandStateChanged;

		public event EventHandler DownloadComplete;

		public event EventHandler DownloadBegin;

		public event TextChangedHandler TitleChanged;

		public event TextChangedHandler PropertyChanged;

		public event SecureIconChangedHandler SecureIconChanged;

		public event BeforeNavigateHandler BeforeNavigate;

		public SmallIe()
			: base("8856f961-340a-11d0-a96b-00c04fd705a2")
		{
			BackgroundImageLayout = ImageLayout.None;
		}

		static SmallIe()
		{
			_protocols = new Hashtable();
			RegisterCustomProtocol("asmres", new AsmResProtocol());
			RegisterCustomProtocol("licres", new LicResProtocol());
		}

		protected override void AttachInterfaces()
		{
			_ocx = base.GetOcx();
			if (_ocx == null)
			{
				throw new NotSupportedException();
			}
			_ocxType = _ocx.GetType();
		}

		protected override void CreateSink()
		{
			_cookie = new ConnectionPointCookie(_ocx, this, typeof(DWebBrowserEvents2));
			if (_firstUrl != null)
			{
				Navigate(_firstUrl);
			}
		}

		protected override void DetachSink()
		{
			try
			{
				if (_cookie != null)
				{
					_cookie.Disconnect();
				}
			}
			catch
			{
			}
		}

		public void Navigate(string url)
		{
			if (_ocxType == null)
			{
				_firstUrl = url;
			}
			else if (string.Compare(url, 0, "html://", 0, 7, true) == 0)
			{
				SetDocumentHtml(url.Substring(7));
			}
			else
			{
				bool containsFocus;
				if (containsFocus = base.ContainsFocus)
				{
					base.SelectNextControl(this, true, true, true, true);
				}
				object[] args = new object[5]
				{
					url,
					Missing.Value,
					Missing.Value,
					Missing.Value,
					Missing.Value
				};
				if (url == LocationUrl)
				{
					RefreshBrowser();
				}
				else
				{
					_ocxType.InvokeMember("Navigate", BindingFlags.Instance | BindingFlags.Public | BindingFlags.InvokeMethod, null, _ocx, args);
				}
				if (containsFocus)
				{
					base.Focus();
				}
			}
		}

		public void SetDocumentHtml(string html)
		{
			if (_ocx != null)
			{
				bool containsFocus;
				if (containsFocus = base.ContainsFocus)
				{
					base.SelectNextControl(this, true, true, true, true);
				}
				Navigate("about:blank");
				RefreshBrowser();
				object obj = _ocxType.InvokeMember("Document", BindingFlags.Instance | BindingFlags.Public | BindingFlags.GetProperty, null, _ocx, new object[0]);
				Type type = obj.GetType();
				type.InvokeMember("write", BindingFlags.Instance | BindingFlags.Public | BindingFlags.InvokeMethod, null, obj, new object[1]
				{
					html
				});
				type.InvokeMember("close", BindingFlags.Instance | BindingFlags.Public | BindingFlags.InvokeMethod, null, obj, new object[0]);
				RefreshBrowser();
				if (containsFocus)
				{
					base.Focus();
				}
			}
		}

		public void RefreshBrowser()
		{
			try
			{
				_ocxType.InvokeMember("Refresh", BindingFlags.Instance | BindingFlags.Public | BindingFlags.InvokeMethod, null, _ocx, new object[0]);
			}
			catch (Exception)
			{
			}
		}

		public void GoBack()
		{
			try
			{
				_ocxType.InvokeMember("GoBack", BindingFlags.Instance | BindingFlags.Public | BindingFlags.InvokeMethod, null, _ocx, new object[0]);
			}
			catch
			{
			}
		}

		public void GoForward()
		{
			try
			{
				_ocxType.InvokeMember("GoForward", BindingFlags.Instance | BindingFlags.Public | BindingFlags.InvokeMethod, null, _ocx, new object[0]);
			}
			catch
			{
			}
		}

		public void Stop()
		{
			try
			{
				_ocxType.InvokeMember("Stop", BindingFlags.Instance | BindingFlags.Public | BindingFlags.InvokeMethod, null, _ocx, new object[0]);
			}
			catch
			{
			}
		}

		public void Print(bool dontPrompt)
		{
			ExecWB(OleCmdId.Print, (!dontPrompt) ? OleCmdExecOption.PromptUser : OleCmdExecOption.DontPromptUser);
		}

		public void Print()
		{
			Print(false);
		}

		public void ExecWB(OleCmdId commandId, OleCmdExecOption option)
		{
			object[] args = new object[4]
			{
				commandId,
				option,
				Missing.Value,
				Missing.Value
			};
			_ocxType.InvokeMember("ExecWB", BindingFlags.Instance | BindingFlags.Public | BindingFlags.InvokeMethod, null, _ocx, args);
		}

		void DWebBrowserEvents2.StatusTextChange(string Text)
		{
			if (this.StatusTextChanged != null)
			{
				this.StatusTextChanged(this, Text);
			}
		}

		void DWebBrowserEvents2.ProgressChange(int Progress, int ProgressMax)
		{
			if (this.ProgressChanged != null)
			{
				this.ProgressChanged(this, Progress, ProgressMax);
			}
		}

		void DWebBrowserEvents2.CommandStateChange(int Command, bool Enable)
		{
			if (this.CommandStateChanged != null)
			{
				switch (Command)
				{
				case 0:
					_canGoBack = Enable;
					break;
				case 1:
					_canGoForward = Enable;
					break;
				}
				this.CommandStateChanged(this, (IeToolbarCommand)Command, Enable);
			}
		}

		void DWebBrowserEvents2.DownloadBegin()
		{
			if (this.DownloadBegin != null)
			{
				this.DownloadBegin(this, EventArgs.Empty);
			}
		}

		void DWebBrowserEvents2.DownloadComplete()
		{
			if (this.DownloadComplete != null)
			{
				this.DownloadComplete(this, EventArgs.Empty);
			}
		}

		void DWebBrowserEvents2.TitleChange(string Text)
		{
			if (this.TitleChanged != null)
			{
				this.TitleChanged(this, Text);
			}
		}

		void DWebBrowserEvents2.PropertyChange(string szProperty)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, szProperty);
			}
		}

		void DWebBrowserEvents2.BeforeNavigate2(object pDisp, ref object URL, ref object Flags, ref object TargetFrameName, ref object PostData, ref object Headers, ref bool Cancel)
		{
			if (this.BeforeNavigate != null && this.BeforeNavigate(this, URL as string, TargetFrameName as string, PostData as byte[], Headers as string))
			{
				Stop();
				Cancel = true;
			}
		}

		void DWebBrowserEvents2.SetSecureLockIcon(int secureLockIcon)
		{
			if (this.SecureIconChanged != null)
			{
				this.SecureIconChanged(this, (SecureLockIcon)secureLockIcon);
			}
		}

		[SecuritySafeCritical]
		public static void RegisterCustomProtocol(string name, ICustomProtocol protocol)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (protocol == null)
			{
				throw new ArgumentNullException("protocol");
			}
			if (_pcf == null)
			{
				_pcf = new ProtocolClassFactory();
			}
			IInternetSession internetSession = null;
			if (0L == CoInternetGetSession(0u, out internetSession, 0u))
			{
				Guid guid = Marshal.GenerateGuidForType(typeof(ClrProtocol));
				int num = (int)internetSession.RegisterNamespace(_pcf, ref guid, protocol.Scheme, 0u, IntPtr.Zero, 0u);
				if (num != 0L)
				{
					Marshal.ThrowExceptionForHR(num);
				}
				_protocols.Add(name, protocol);
			}
		}

		protected override void OnVisibleChanged(EventArgs e)
		{
			base.OnVisibleChanged(e);
			if (base.Visible && base.IsHandleCreated)
			{
				base.RecreateHandle();
			}
		}

		[DllImport("urlmon")]
		private static extern int CoInternetGetSession(uint dwSessionMode, out IInternetSession ppIInternetSession, uint reserved);
	}
}
