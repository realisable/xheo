using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Reflection;
using System.Reflection.Emit;
using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	public class ErrorReportPanel : SplitPanel
	{
		private string _title;

		private string _subTitle;

		private Button _details;

		private bool _startDetailsVisible;

		private ValidationRecordCollection _records;

		private static readonly Bitmap _error;

		private static readonly Bitmap _warning;

		private static readonly Bitmap _notice;

		private ShadowLabel _errorMessage;

		private TreeList _detailsTree;

		private TabControl _detailsTabs;

		private TabPage _detailsPage;

		private TabPage _assembliesPage;

		private TabPage _systemInfoPage;

		private ListView _assemblyList;

		private ColumnHeader _assemblyHeader;

		private ColumnHeader _versionHeader;

		private ColumnHeader _fileVersionHeader;

		private ColumnHeader _locationHeader;

		private TextBox _systemInfo;

		private BufferedPanel _nextStepsPanel;

		private ShadowLabel _nextStepsTitle;

		private ShadowLabel _nsEmail;

		private ShadowLabel _nsCopy;

		private ColumnHeader _frameworkVersionHeader;

		private IContainer components;

		private bool _detailsInitialized;

		public ErrorReportPanel(string title, string subTitle, string message, ValidationRecordCollection records)
			: this(title, subTitle, message, records, false)
		{
		}

		public ErrorReportPanel(string title, string subTitle, string message, ValidationRecordCollection records, bool startDetailsVisible)
			: base(null)
		{
			InitializeComponent();
			_detailsPage.DockPadding.All = 3;
			_detailsPage.DockPadding.Top = 8;
			_assembliesPage.DockPadding.All = 3;
			_systemInfoPage.DockPadding.Top = 10;
			_errorMessage.Text = message;
			_records = records;
			_title = title;
			_subTitle = subTitle;
			KeepSupportVisible = true;
			_startDetailsVisible = startDetailsVisible;
		}

		static ErrorReportPanel()
		{
			_error = (Images.Error_png as Bitmap);
			_warning = (Images.Warning_png as Bitmap);
			_notice = (Images.Notice_png as Bitmap);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			_errorMessage = new ShadowLabel();
			_detailsTree = new TreeList();
			_detailsTabs = new TabControl();
			_detailsPage = new TabPage();
			_assembliesPage = new TabPage();
			_assemblyList = new ListView();
			_assemblyHeader = new ColumnHeader();
			_versionHeader = new ColumnHeader();
			_fileVersionHeader = new ColumnHeader();
			_frameworkVersionHeader = new ColumnHeader();
			_locationHeader = new ColumnHeader();
			_systemInfoPage = new TabPage();
			_systemInfo = new TextBox();
			_nextStepsPanel = new BufferedPanel();
			_nsEmail = new ShadowLabel();
			_nsCopy = new ShadowLabel();
			_nextStepsTitle = new ShadowLabel();
			base.BodyPanel.SuspendLayout();
			_detailsTabs.SuspendLayout();
			_detailsPage.SuspendLayout();
			_assembliesPage.SuspendLayout();
			_systemInfoPage.SuspendLayout();
			_nextStepsPanel.SuspendLayout();
			base.SuspendLayout();
			base.SidePanel.Location = new Point(0, 0);
			base.SidePanel.Size = new Size(170, 421);
			base.BodyPanel.Controls.Add(_nextStepsPanel);
			base.BodyPanel.Controls.Add(_detailsTabs);
			base.BodyPanel.Controls.Add(_errorMessage);
			base.BodyPanel.Location = new Point(170, 0);
			base.BodyPanel.Size = new Size(514, 421);
			_errorMessage.Anchor = (AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right);
			_errorMessage.Location = new Point(16, 16);
			_errorMessage.Name = "_errorMessage";
			_errorMessage.Size = new Size(482, 71);
			_errorMessage.TabIndex = 11;
			_errorMessage.Text = "label1";
			_errorMessage.ThemeFont = ThemeFont.Medium;
			_detailsTree.BackColor = SystemColors.Window;
			_detailsTree.BackgroundImageLayout = ImageLayout.None;
			_detailsTree.Dock = DockStyle.Fill;
			_detailsTree.Location = new Point(0, 0);
			_detailsTree.Margin = new Padding(0);
			_detailsTree.Name = "_detailsTree";
			_detailsTree.Size = new Size(474, 290);
			_detailsTree.TabIndex = 12;
			_detailsTabs.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			_detailsTabs.Controls.Add(_detailsPage);
			_detailsTabs.Controls.Add(_assembliesPage);
			_detailsTabs.Controls.Add(_systemInfoPage);
			_detailsTabs.Location = new Point(16, 87);
			_detailsTabs.Name = "_detailsTabs";
			_detailsTabs.SelectedIndex = 0;
			_detailsTabs.Size = new Size(482, 318);
			_detailsTabs.TabIndex = 14;
			_detailsTabs.Visible = false;
			_detailsPage.BackColor = SystemColors.Window;
			_detailsPage.Controls.Add(_detailsTree);
			_detailsPage.Location = new Point(4, 24);
			_detailsPage.Margin = new Padding(0);
			_detailsPage.Name = "_detailsPage";
			_detailsPage.Size = new Size(474, 290);
			_detailsPage.TabIndex = 0;
			_detailsPage.Text = "#UI_DetailsTab";
			_assembliesPage.BackColor = SystemColors.Window;
			_assembliesPage.Controls.Add(_assemblyList);
			_assembliesPage.Location = new Point(4, 24);
			_assembliesPage.Margin = new Padding(0);
			_assembliesPage.Name = "_assembliesPage";
			_assembliesPage.Size = new Size(474, 290);
			_assembliesPage.TabIndex = 1;
			_assembliesPage.Text = "#UI_AssembliesTab";
			_assemblyList.BorderStyle = BorderStyle.None;
			_assemblyList.Columns.AddRange(new ColumnHeader[5]
			{
				_assemblyHeader,
				_versionHeader,
				_fileVersionHeader,
				_frameworkVersionHeader,
				_locationHeader
			});
			_assemblyList.Dock = DockStyle.Fill;
			_assemblyList.FullRowSelect = true;
			_assemblyList.HeaderStyle = ColumnHeaderStyle.Nonclickable;
			_assemblyList.Location = new Point(0, 0);
			_assemblyList.Margin = new Padding(0);
			_assemblyList.Name = "_assemblyList";
			_assemblyList.Size = new Size(474, 290);
			_assemblyList.TabIndex = 0;
			_assemblyList.UseCompatibleStateImageBehavior = false;
			_assemblyList.View = View.Details;
			_assemblyHeader.Text = "Assembly";
			_assemblyHeader.Width = 200;
			_versionHeader.Text = "Version";
			_versionHeader.Width = 90;
			_fileVersionHeader.Text = "File Version";
			_fileVersionHeader.Width = 110;
			_frameworkVersionHeader.Text = ".NET";
			_frameworkVersionHeader.Width = 37;
			_locationHeader.Text = "Location";
			_locationHeader.Width = 300;
			_systemInfoPage.BackColor = SystemColors.Window;
			_systemInfoPage.Controls.Add(_systemInfo);
			_systemInfoPage.Location = new Point(4, 24);
			_systemInfoPage.Name = "_systemInfoPage";
			_systemInfoPage.Size = new Size(474, 290);
			_systemInfoPage.TabIndex = 2;
			_systemInfoPage.Text = "#UI_SystemInfoTab";
			_systemInfo.BackColor = SystemColors.Window;
			_systemInfo.BorderStyle = BorderStyle.None;
			_systemInfo.Dock = DockStyle.Fill;
			_systemInfo.Location = new Point(0, 0);
			_systemInfo.Multiline = true;
			_systemInfo.Name = "_systemInfo";
			_systemInfo.ReadOnly = true;
			_systemInfo.ScrollBars = ScrollBars.Both;
			_systemInfo.Size = new Size(474, 290);
			_systemInfo.TabIndex = 0;
			_nextStepsPanel.BackColor = Color.Transparent;
			_nextStepsPanel.BackgroundImageLayout = ImageLayout.None;
			_nextStepsPanel.Controls.Add(_nsEmail);
			_nextStepsPanel.Controls.Add(_nsCopy);
			_nextStepsPanel.Controls.Add(_nextStepsTitle);
			_nextStepsPanel.Location = new Point(65, 124);
			_nextStepsPanel.Name = "_nextStepsPanel";
			_nextStepsPanel.PaintFakeBackground = true;
			_nextStepsPanel.Size = new Size(379, 86);
			_nextStepsPanel.TabIndex = 17;
			_nsEmail.Anchor = (AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right);
			_nsEmail.AutoSize = true;
			_nsEmail.Location = new Point(15, 55);
			_nsEmail.Name = "_nsEmail";
			_nsEmail.Size = new Size(184, 15);
			_nsEmail.TabIndex = 22;
			_nsEmail.Text = "#UI_GenerateDetailedEmailReport";
			_nsEmail.TextOffset = new Point(0, -2);
			_nsEmail.ThemeFont = ThemeFont.Medium;
			_nsEmail.Url = "#";
			_nsEmail.Click += _nsEmail_Click;
			_nsCopy.Anchor = (AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right);
			_nsCopy.AutoSize = true;
			_nsCopy.Location = new Point(15, 29);
			_nsCopy.Name = "_nsCopy";
			_nsCopy.Size = new Size(159, 15);
			_nsCopy.TabIndex = 20;
			_nsCopy.Text = "#UI_CopyDetailsToClipboard";
			_nsCopy.TextOffset = new Point(0, -2);
			_nsCopy.ThemeFont = ThemeFont.Medium;
			_nsCopy.Url = "#";
			_nsCopy.Click += _nsCopy_Click;
			_nextStepsTitle.Anchor = (AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right);
			_nextStepsTitle.AutoSize = true;
			_nextStepsTitle.Location = new Point(0, 0);
			_nextStepsTitle.Name = "_nextStepsTitle";
			_nextStepsTitle.Size = new Size(82, 15);
			_nextStepsTitle.TabIndex = 16;
			_nextStepsTitle.Text = "#UI_NextSteps";
			_nextStepsTitle.TextOffset = new Point(0, -2);
			_nextStepsTitle.ThemeFont = ThemeFont.Medium;
			base.AutoScaleDimensions = new SizeF(96f, 96f);
			base.DefaultIconName = "DefaultWarningIcon,DefaultIcon";
			base.Name = "ErrorReportPanel";
			base.BodyPanel.ResumeLayout(false);
			_detailsTabs.ResumeLayout(false);
			_detailsPage.ResumeLayout(false);
			_assembliesPage.ResumeLayout(false);
			_systemInfoPage.ResumeLayout(false);
			_systemInfoPage.PerformLayout();
			_nextStepsPanel.ResumeLayout(false);
			_nextStepsPanel.PerformLayout();
			base.ResumeLayout(false);
		}

		private void ShowDetails()
		{
			if (!_detailsInitialized)
			{
				AddDetailRecord(_records, _detailsTree.Nodes, true);
				Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
				Array.Sort(assemblies, new AssemblyComparer());
				if (base.SuperForm.Context.SupportInfo.IncludeAssemblies && base.SuperForm.Context.SupportInfo.IncludeDetails)
				{
					Assembly[] array = assemblies;
					int num = 0;
					while (true)
					{
						if (num >= array.Length)
						{
							break;
						}
						Assembly assembly = array[num];
						ListViewItem listViewItem = new ListViewItem();
						listViewItem.UseItemStyleForSubItems = false;
						try
						{
							AssemblyName name = assembly.GetName();
							listViewItem.Text = name.Name;
							if (!listViewItem.Text.StartsWith("mscorlib") && !listViewItem.Text.StartsWith("System") && !listViewItem.Text.StartsWith("Microsoft.") && !listViewItem.Text.StartsWith("vshost"))
							{
								listViewItem.SubItems[0].Font = new Font(Font, FontStyle.Bold);
							}
							ParsedVersion assemblyVersion = TypeHelper.GetAssemblyVersion(assembly);
							listViewItem.SubItems.Add(name.Version.ToString(), SystemColors.GrayText, Color.Empty, null);
							listViewItem.SubItems.Add(assemblyVersion.ToString());
							listViewItem.SubItems.Add(assemblyVersion.ToString("f"));
							if (!(assembly is AssemblyBuilder) && assembly.GetType().Name != "InternalAssemblyBuilder")
							{
								if (assembly.GlobalAssemblyCache)
								{
									listViewItem.SubItems.Add("GAC");
								}
								else if (string.Compare("file://", 0, assembly.CodeBase, 0, 7, true, CultureInfo.InvariantCulture) == 0)
								{
									listViewItem.SubItems.Add(assembly.Location);
								}
								else
								{
									listViewItem.SubItems.Add(assembly.CodeBase);
								}
							}
							else
							{
								listViewItem.SubItems.Add(SR.GetString("M_DynamicAssembly"), SystemColors.GrayText, Color.Empty, null);
							}
						}
						catch (Exception ex)
						{
							listViewItem.Text = "ERROR: " + ex.Message;
							listViewItem.ForeColor = Color.Red;
						}
						_assemblyList.Items.Add(listViewItem);
						num++;
					}
					_assemblyHeader.Width = -1;
					_versionHeader.Width = -1;
					_fileVersionHeader.Width = -2;
					_locationHeader.Width = -1;
				}
				else
				{
					_detailsTabs.TabPages.Remove(_assembliesPage);
				}
				if (base.SuperForm.Context.SupportInfo.IncludeSystemInfo && base.SuperForm.Context.SupportInfo.IncludeDetails)
				{
					_systemInfo.Text = Toolbox.MakeSystemInfoString();
				}
				else
				{
					_detailsTabs.TabPages.Remove(_systemInfoPage);
				}
				base.SidePanel.Controls.Add(_nextStepsPanel);
				_nextStepsPanel.SetBounds(0, 135, 160, 250);
				_nextStepsPanel.PaintFakeBackground = false;
				_nsCopy.ThemeFont = ThemeFont.Normal;
				_nsCopy.Font = null;
				_nsCopy.Text = SR.GetString("UI_CopyToClipboard");
				_nsEmail.ThemeFont = ThemeFont.Normal;
				_nsEmail.Font = null;
				_nsEmail.Text = SR.GetString("UI_CopyToEmail");
				_nextStepsTitle.Left += 10;
				_detailsInitialized = true;
				_detailsTabs.Visible = true;
				if (_details != null)
				{
					_details.Visible = false;
				}
			}
		}

		private void AddDetailRecord(ValidationRecordCollection records, TreeListNodeCollection nodes, bool expand)
		{
			foreach (ValidationRecord item in (IEnumerable)records)
			{
				TreeListNode treeListNode = new TreeListNode();
				treeListNode.Text = item.Message;
				treeListNode.IsExpanded = (expand && item.Severity != ErrorSeverity.Low);
				nodes.Add(treeListNode);
				switch (item.Severity)
				{
				case ErrorSeverity.NotSet:
				case ErrorSeverity.Low:
					treeListNode.Image = _notice;
					break;
				case ErrorSeverity.Normal:
					treeListNode.Image = _warning;
					break;
				case ErrorSeverity.High:
					treeListNode.Image = _error;
					break;
				}
				AddDetailRecord(item.SubRecords, treeListNode.Nodes, false);
			}
		}

		protected internal override void InitializePanel()
		{
			Button button = base.AddBottomButton("#UI_OK");
			button.Click += ok_Click;
			button.Focus();
			base._superForm.AcceptButton = button;
			base._superForm.CancelButton = button;
			if (base.SuperForm.Context.SupportInfo.IncludeDetails)
			{
				_details = base.AddBottomButton("UI_ShowDetails");
				_details.Click += _details_Click;
			}
			else
			{
				_nextStepsPanel.Visible = false;
			}
			if (_startDetailsVisible)
			{
				ShowDetails();
			}
			if (_title != null)
			{
				base.SetHeader(_title, _subTitle);
			}
			_nsCopy.Image = Images.Bullet_png;
			_nsEmail.Image = Images.Bullet_png;
			_nextStepsTitle.Image = Images.Help_png;
			if (base.SuperForm.Context.SupportInfo.Email == null)
			{
				_nsEmail.Visible = false;
			}
		}

		private void ok_Click(object sender, EventArgs e)
		{
			base.Return(FormResult.Success);
		}

		private void _details_Click(object sender, EventArgs e)
		{
			ShowDetails();
		}

		private void _nsCopy_Click(object sender, EventArgs e)
		{
			string data = Toolbox.MakeDiagnosticReportString(_errorMessage.Text, _records, base.SuperForm.Context);
			try
			{
				ClipboardToolbox.SetClipboard(DataFormats.Text, data);
				base.ShowMessageBox("UI_DetailsCopied", "UI_DetailsCopiedTitle", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
			}
			catch (Exception ex)
			{
				MessageBoxEx.ShowException(ex);
			}
		}

		private void _nsEmail_Click(object sender, EventArgs e)
		{
			string message = Toolbox.MakeDiagnosticReportString(_errorMessage.Text, _records, base.SuperForm.Context);
			try
			{
				if (!Mapi.SendMail(base.SuperForm.Context.SupportInfo.Email, null, SR.GetString("UI_DetailsReport"), message, null, null))
				{
					base.ShowMessageBox("E_CouldNotCreateEmail", null, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				}
			}
			catch (Exception ex)
			{
				MessageBoxEx.ShowException(ex);
			}
		}
	}
}
