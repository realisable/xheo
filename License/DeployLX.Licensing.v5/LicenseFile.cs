using System;
using System.Collections;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.IO.IsolatedStorage;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	[ComVisible(true)]
	public sealed class LicenseFile : IDisposable, ICloneable, IChange
	{
		private bool _isReadOnly;

		private ChangeEventHandler _changed;

		private ChangeEventHandler _bubbleHandler;

		private bool _disposing;

		private string _id = Guid.NewGuid().ToString("N");

		private LicenseValuesDictionary _metaValues;

		private LicenseValuesDictionary _editorState = new LicenseValuesDictionary();

		internal string _location;

		private string _isfLocation;

		private ulong _fileId;

		private string _keyName;

		private string _originalLocation;

		private string _suggestedSaveLocation;

		private SaveLocations _saveTo;

		private SecureLicenseCollection _licenses;

		private LocationState _locationState;

		private int _releaseVersion;

		private bool _autoIncrementReleaseVersion = true;

		private LicenseResourceCollection _resources;

		private LocationDispositions _disposition;

		private SuperFormTheme _superFormTheme;

		private SecureLicense _templateLicense;

		public string Id
		{
			get
			{
				return _id;
			}
			set
			{
				AssertNotReadOnly();
				if (_id != value)
				{
					_id = value;
					OnChanged("Id");
				}
			}
		}

		[Browsable(false)]
		public bool IsEmbedded
		{
			get
			{
				return (_disposition & LocationDispositions.Embedded) != LocationDispositions.NotSet;
			}
		}

		public LicenseValuesDictionary MetaValues
		{
			get
			{
				return _metaValues;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public LicenseValuesDictionary EditorState
		{
			get
			{
				return _editorState;
			}
		}

		[Browsable(false)]
		public string Location
		{
			get
			{
				return _location;
			}
		}

		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public string KeyName
		{
			get
			{
				return _keyName;
			}
			set
			{
				if (!(value == _keyName))
				{
					_keyName = value;
					OnChanged("KeyName");
				}
			}
		}

		public string DisplayLocation
		{
			get
			{
				if (_location == null)
				{
					return SR.GetString("M_NA");
				}
				if (_disposition == LocationDispositions.Embedded)
				{
					return SR.GetString("M_CurrentAssembly");
				}
				return _location;
			}
		}

		public string OriginalLocation
		{
			get
			{
				return _originalLocation;
			}
			set
			{
				if (_originalLocation != value)
				{
					_originalLocation = value;
					OnChanged("OriginalLocation");
				}
			}
		}

		public string SuggestedSaveLocation
		{
			get
			{
				return _suggestedSaveLocation;
			}
			set
			{
				if (_suggestedSaveLocation != value)
				{
					_suggestedSaveLocation = value;
					OnChanged("SuggestedSaveLocation");
				}
			}
		}

		public SaveLocations SaveTo
		{
			get
			{
				return _saveTo;
			}
			set
			{
				AssertNotReadOnly();
				if (_saveTo != value)
				{
					_saveTo = value;
					OnChanged("SaveTo");
				}
			}
		}

		public SecureLicenseCollection Licenses
		{
			get
			{
				return _licenses;
			}
		}

		public SecureLicenseCollection Editions
		{
			get
			{
				return Licenses;
			}
		}

		public LocationState LocationState
		{
			get
			{
				return _locationState;
			}
		}

		public int ReleaseVersion
		{
			get
			{
				return _releaseVersion;
			}
			set
			{
				AssertNotReadOnly();
				if (_releaseVersion != value)
				{
					_releaseVersion = value;
					OnChanged("ReleaseVersion");
				}
			}
		}

		public bool AutoIncrementReleaseVersion
		{
			get
			{
				return _autoIncrementReleaseVersion;
			}
			set
			{
				AssertNotReadOnly();
				if (_autoIncrementReleaseVersion != value)
				{
					_autoIncrementReleaseVersion = value;
					OnChanged("AutoIncrementReleaseVersion");
				}
			}
		}

		public LicenseResourceCollection Resources
		{
			get
			{
				return _resources;
			}
		}

		public LocationDispositions Disposition
		{
			get
			{
				return _disposition;
			}
		}

		public SuperFormTheme SuperFormTheme
		{
			get
			{
				if (_superFormTheme == null)
				{
					SetTheme(null);
				}
				return _superFormTheme;
			}
			set
			{
				AssertNotReadOnly();
				if (_superFormTheme != value)
				{
					SetTheme(value);
					OnChanged("SuperFormTheme");
				}
			}
		}

		public SecureLicense TemplateLicense
		{
			get
			{
				return _templateLicense;
			}
			set
			{
				AssertNotReadOnly();
				if (_templateLicense != value)
				{
					if (_templateLicense != null)
					{
						((IChange)_templateLicense).Changed -= TemplateLicense_Changed;
					}
					_templateLicense = value;
					if (value != null)
					{
						value.IsTemplate = true;
					}
					if (_templateLicense != null)
					{
						_templateLicense._licenseFile = this;
						((IChange)_templateLicense).Changed += TemplateLicense_Changed;
					}
					OnChanged("TemplateLicense");
				}
			}
		}

		event ChangeEventHandler IChange.Changed
		{
			add
			{
				lock (this)
				{
					_changed = (ChangeEventHandler)Delegate.Combine(_changed, value);
				}
			}
			remove
			{
				lock (this)
				{
					_changed = (ChangeEventHandler)Delegate.Remove(_changed, value);
				}
			}
		}

		private void OnChanged(ChangeEventArgs e)
		{
			if (_changed != null)
			{
				_changed(this, e);
			}
		}

		private void OnChanged(object sender, string property, object item, CollectionChangeAction action)
		{
			if (_changed != null)
			{
				ChangeEventArgs changeEventArgs = new ChangeEventArgs(property, this, item, action);
				changeEventArgs.BubbleStack.Push(sender);
				_changed(this, changeEventArgs);
			}
		}

		private void OnChanged(string property)
		{
			if (_changed != null)
			{
				ChangeEventArgs changeEventArgs = new ChangeEventArgs(property, this);
				changeEventArgs.BubbleStack.Push(this);
				_changed(this, changeEventArgs);
			}
		}

		public LicenseFile()
		{
			_licenses = new SecureLicenseCollection(this);
			_licenses.Changed += _licenses_Changed;
			_metaValues = new LicenseValuesDictionary();
			_metaValues.Changed += _metaValues_Changed;
			_resources = new LicenseResourceCollection();
			_resources.Changed += _resources_Changed;
		}

		private void _resources_Changed(object sender, CollectionEventArgs e)
		{
			OnChanged(sender, "Resources", e.Element, e.Action);
			AttachChangeBubbler(e);
		}

		private void _licenses_Changed(object sender, CollectionEventArgs e)
		{
			OnChanged(sender, "Licenses", e.Element, e.Action);
			AttachChangeBubbler(e);
		}

		private void AttachChangeBubbler(CollectionEventArgs e)
		{
			IChange change = e.Element as IChange;
			if (e.Action == CollectionChangeAction.Add)
			{
				if (_bubbleHandler == null)
				{
					_bubbleHandler = BubbleChanged;
				}
				change.Changed += _bubbleHandler;
			}
			else if (e.Action == CollectionChangeAction.Remove)
			{
				change.Changed -= _bubbleHandler;
			}
		}

		private void BubbleChanged(object sender, ChangeEventArgs e)
		{
			e.BubbleStack.Push(sender);
			OnChanged(e);
		}

		private void _metaValues_Changed(object sender, CollectionEventArgs e)
		{
			OnChanged(sender, "MetaValues", e.Element, e.Action);
		}

		public LicenseFile(string fileName)
			: this()
		{
			Load(fileName);
		}

		public LicenseFile(string fileName, bool tryShared)
			: this()
		{
			Load(fileName, tryShared);
		}

		public void Dispose()
		{
			if (!_disposing)
			{
				_disposing = true;
				foreach (SecureLicense item in (IEnumerable)_licenses)
				{
					item.Dispose();
				}
				_licenses.Clear();
				GC.SuppressFinalize(this);
				_disposing = false;
			}
		}

		private void SetTheme(SuperFormTheme value)
		{
			if (value == null)
			{
				value = new SuperFormTheme(SuperFormTheme.DefaultBaseColor);
			}
			if (_superFormTheme != null)
			{
				_superFormTheme.Changed -= _superFormTheme_Changed;
			}
			_superFormTheme = value;
			_superFormTheme.Changed += _superFormTheme_Changed;
		}

		private void _superFormTheme_Changed(object sender, ChangeEventArgs e)
		{
			e.BubbleStack.Push(sender);
			if (e.Name == null)
			{
				e.Name = "SuperFormTheme";
			}
			OnChanged(e);
		}

		private void TemplateLicense_Changed(object sender, ChangeEventArgs e)
		{
			e.BubbleStack.Push(sender);
			OnChanged(e);
		}

		public void MakeReadOnly()
		{
			_isReadOnly = true;
		}

		internal void SetAsEmbedded()
		{
			if (Check.CheckCalledByThisAssembly())
			{
				_disposition |= LocationDispositions.Embedded;
			}
		}

		private void AssertNotReadOnly()
		{
			if (!_isReadOnly)
			{
				return;
			}
			throw new SecureLicenseException("E_ReadOnlyObject", "LicenseFile");
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public ICollection GetLicensesForComponent(string componentType)
		{
			ArrayList arrayList = new ArrayList();
			foreach (SecureLicense item in (IEnumerable)_licenses)
			{
				if (item.CanLicenseComponent(componentType))
				{
					arrayList.Add(item);
				}
			}
			return arrayList;
		}

		public void Clear()
		{
			_metaValues.Clear();
			_licenses.Clear();
			_location = null;
			_saveTo = SaveLocations.NotSet;
			_releaseVersion = 0;
			_disposition = LocationDispositions.NotSet;
		}

		public void Load(string fileName, bool tryShared)
		{
			if (fileName == null)
			{
				throw new ArgumentNullException("fileName");
			}
			if (tryShared && !fileName.StartsWith("isolated:"))
			{
				fileName = Path.GetFullPath(fileName);
				if (IsfFileExists(fileName))
				{
					try
					{
						Load("isolated:" + fileName);
						return;
					}
					catch
					{
					}
				}
				if (!IsSharedMirroredFile(fileName))
				{
					string text = MakeSharedName(fileName);
					if (File.Exists(text))
					{
						try
						{
							Load(text);
							return;
						}
						catch
						{
						}
					}
				}
			}
			Load(fileName);
		}

		public void Load(string fileName)
		{
			if (fileName == null)
			{
				throw new ArgumentNullException("fileName");
			}
			try
			{
				if (fileName.StartsWith("isolated:"))
				{
					if (SafeToolbox.IsFipsEnabled)
					{
						throw new SecureLicenseException("E_ISNotFipsCompliant");
					}
					IsolatedStorageFileStream isolatedStorageFileStream = null;
					try
					{
						string path = IsfGetFileName(fileName.Substring(9));
						IsolatedStorageFile isfStore = GetIsfStore();
						isolatedStorageFileStream = new IsolatedStorageFileStream(path, FileMode.Open, FileAccess.Read, isfStore);
						Load(isolatedStorageFileStream);
						_location = fileName;
						_isfLocation = null;
						try
						{
							string text = isolatedStorageFileStream.GetType().InvokeMember("m_FullPath", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetField, null, isolatedStorageFileStream, new object[0]) as string;
							if (text != null)
							{
								_isfLocation = text;
							}
						}
						catch
						{
						}
					}
					finally
					{
						if (isolatedStorageFileStream != null)
						{
							isolatedStorageFileStream.Close();
						}
					}
				}
				else
				{
					FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
					try
					{
						Load(fileStream);
					}
					finally
					{
						fileStream.Close();
					}
					_location = Path.GetFullPath(fileName);
				}
			}
			catch (Exception ex)
			{
				SecureLicenseException ex2 = ex as SecureLicenseException;
				if (ex2 != null)
				{
					throw new SecureLicenseException("E_CouldNotLoadLicenseFile", ex, ex2.Severity);
				}
				throw new SecureLicenseException("E_CouldNotLoadLicenseFile", ex);
			}
		}

		public void Load(Stream stream)
		{
			if (stream == null)
			{
				throw new ArgumentNullException("stream");
			}
			Clear();
			int num = stream.ReadByte();
			XmlReader reader;
			if (num == 88)
			{
				byte[] array = new byte[4];
				stream.Read(array, 0, array.Length);
				byte[] array2 = new byte[BitConverter.ToInt32(array, 0)];
				stream.Read(array2, 0, array2.Length);
				using (SymmetricAlgorithm symmetricAlgorithm = SafeToolbox.GetAes(false))
				{
					symmetricAlgorithm.IV = new byte[16];
					symmetricAlgorithm.Key = new byte[32];
					Buffer.BlockCopy(Images.ProtectedSmall_png_, 0, symmetricAlgorithm.IV, 0, symmetricAlgorithm.IV.Length);
					Buffer.BlockCopy(Images.ProtectedSmall_png_, symmetricAlgorithm.IV.Length, symmetricAlgorithm.Key, 0, symmetricAlgorithm.Key.Length);
					array2 = symmetricAlgorithm.CreateDecryptor().TransformFinalBlock(array2, 0, array2.Length);
				}
				reader = new XmlTextReader(new MemoryStream(array2, 0, array2.Length, false, false));
			}
			else
			{
				while (num != 60 && num != -1)
				{
					num = stream.ReadByte();
				}
				stream.Seek(-1L, SeekOrigin.Current);
				reader = new XmlTextReader(stream);
			}
			LoadFromXml(reader);
			ulong fileId = GetFileId(stream);
			if (_fileId != 0L)
			{
				_locationState = ((_fileId != fileId) ? LocationState.Altered : LocationState.Unaltered);
			}
			else
			{
				_locationState = LocationState.Unknown;
			}
		}

		internal void Save(string fileName, bool useSharedOnFail, LicenseSaveType signing, bool markAsSystem, bool validationSave)
		{
			bool flag = false;
			if (fileName == null)
			{
				if (_location != null && _location.Length > 0)
				{
					fileName = _location;
					flag = true;
					goto IL_0043;
				}
				throw new ArgumentNullException("fileName");
			}
			flag = (string.Compare(fileName, _location, true) == 0);
			goto IL_0043;
			IL_0043:
			if (fileName.IndexOf('.') == -1)
			{
				fileName = Path.ChangeExtension(fileName, ".lic");
			}
			if (!fileName.StartsWith("isolated:"))
			{
				fileName = Path.GetFullPath(fileName);
			}
			if (_location != null && string.Compare(fileName, _location) == 0 && !validationSave && _isReadOnly)
			{
				throw new SecureLicenseException("E_CannotOverwriteValidatedLicense");
			}
			if (fileName.StartsWith("isolated:"))
			{
				if (SafeToolbox.IsFipsEnabled)
				{
					throw new SecureLicenseException("E_ISNotFipsCompliant");
				}
				IsolatedStorageFileStream isolatedStorageFileStream = null;
				try
				{
					string path = IsfGetFileName(fileName.Substring(9));
					using (IsolatedStorageFile isolatedStorageFile = GetIsfStore())
					{
						if (flag && _isfLocation != null && markAsSystem)
						{
							File.SetAttributes(_isfLocation, FileAttributes.Normal);
						}
						isolatedStorageFile.CreateDirectory(IsfGetDirectoryName(Path.GetDirectoryName(fileName.Substring(9))));
						isolatedStorageFileStream = new IsolatedStorageFileStream(path, FileMode.OpenOrCreate, FileAccess.Write, isolatedStorageFile);
						Save(isolatedStorageFileStream, false, validationSave, signing);
						_isfLocation = null;
						try
						{
							string text = isolatedStorageFileStream.GetType().InvokeMember("m_FullPath", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetField, null, isolatedStorageFileStream, new object[0]) as string;
							if (text != null)
							{
								_isfLocation = text;
								if (markAsSystem)
								{
									File.SetAttributes(text, FileAttributes.ReadOnly | FileAttributes.System);
								}
							}
						}
						catch (Exception exception)
						{
							SecureLicenseContext resolveContext = SecureLicenseContext.ResolveContext;
							if (resolveContext != null && resolveContext.RequestInfo.DeveloperMode)
							{
								resolveContext.ReportError("E_CouldNotCompleteSave", this, exception, ErrorSeverity.Low);
							}
						}
					}
					_location = fileName;
				}
				finally
				{
					if (isolatedStorageFileStream != null)
					{
						isolatedStorageFileStream.Close();
					}
				}
			}
			else
			{
				try
				{
					fileName = Path.Combine(Config.SharedFolder, Path.GetFileName(fileName));

					fileName = Path.GetFullPath(fileName);
					if (File.Exists(fileName))
					{
						File.SetAttributes(fileName, FileAttributes.Normal);
					}
					string directoryName = Path.GetDirectoryName(fileName);
					if (!Directory.Exists(directoryName))
					{
						Directory.CreateDirectory(directoryName);
						SafeToolbox.GrantEveryoneAccess(directoryName);
					}

					FileStream fileStream = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.Write);
					try
					{
						Save(fileStream, false, validationSave, signing);
						if (markAsSystem)
						{
							try
							{
								SafeToolbox.GrantEveryoneAccess(fileName);
								File.SetAttributes(fileName, FileAttributes.ReadOnly | FileAttributes.Hidden | FileAttributes.System);
							}
							catch (Exception exception2)
							{
								SecureLicenseContext resolveContext2 = SecureLicenseContext.ResolveContext;
								if (resolveContext2 != null && resolveContext2.RequestInfo.DeveloperMode)
								{
									resolveContext2.ReportError("E_CouldNotCompleteSave", this, exception2, ErrorSeverity.Low);
								}
							}
						}
					}
					finally
					{
						fileStream.Close();
					}
					_location = Path.GetFullPath(fileName);
				}
				catch
				{
					if (useSharedOnFail)
					{
						try
						{
							if (!IsSharedMirroredFile(fileName))
							{
								if (File.Exists(fileName))
								{
									_originalLocation = fileName;
								}
								EnsureSharedMirrorFolder();
								Save(MakeSharedName(fileName), false, signing, markAsSystem, validationSave);
								return;
							}
						}
						catch (Exception exception3)
						{
							SecureLicenseContext resolveContext3 = SecureLicenseContext.ResolveContext;
							if (resolveContext3 != null && resolveContext3.RequestInfo.DeveloperMode)
							{
								resolveContext3.ReportError("E_CouldNotCompleteSave", this, exception3, ErrorSeverity.Low);
							}
						}
						if (fileName != null && fileName.Length > 0 && !fileName.StartsWith("isolated:") && File.Exists(fileName))
						{
							_originalLocation = fileName;
						}
						//Save("isolated:" + fileName, false, signing, markAsSystem, validationSave);
						Save(fileName, false, signing, markAsSystem, validationSave);
						goto end_IL_026f;
					}
					throw;
					end_IL_026f:;
				}
			}
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void Save(string fileName, bool useSharedOnFail, LicenseSaveType signing)
		{
			Save(fileName, useSharedOnFail, signing, false, false);
		}

		public void Save(Stream stream, bool dontEncrypt)
		{
			Save(stream, dontEncrypt, false, LicenseSaveType.Normal);
		}

		private void Save(Stream stream, bool dontEncrypt, bool updateFileId, LicenseSaveType signing)
		{
			if (updateFileId)
			{
				_fileId = GetFileId(stream);
			}
			string text = ToXmlString(signing);
			if (dontEncrypt)
			{
				StreamWriter streamWriter = new StreamWriter(stream, new UTF8Encoding(false));
				streamWriter.Write(text);
				streamWriter.Flush();
				if (stream.Length - stream.Position > 0L)
				{
					try
					{
						stream.SetLength(stream.Position);
					}
					catch
					{
						byte[] array = new byte[stream.Length - stream.Position];
						stream.Write(array, 0, array.Length);
						streamWriter.Close();
					}
				}
			}
			else
			{
				stream.WriteByte(88);
				byte[] bytes = Encoding.UTF8.GetBytes(text);
				byte[] array2 = default(byte[]);
				using (SymmetricAlgorithm symmetricAlgorithm = SafeToolbox.GetAes(false))
				{
					symmetricAlgorithm.IV = new byte[16];
					symmetricAlgorithm.Key = new byte[32];
					Buffer.BlockCopy(Images.ProtectedSmall_png_, 0, symmetricAlgorithm.IV, 0, symmetricAlgorithm.IV.Length);
					Buffer.BlockCopy(Images.ProtectedSmall_png_, symmetricAlgorithm.IV.Length, symmetricAlgorithm.Key, 0, symmetricAlgorithm.Key.Length);
					array2 = symmetricAlgorithm.CreateEncryptor().TransformFinalBlock(bytes, 0, bytes.Length);
				}
				Array.Clear(bytes, 0, bytes.Length);
				byte[] bytes2 = BitConverter.GetBytes(array2.Length);
				stream.Write(bytes2, 0, bytes2.Length);
				stream.Write(array2, 0, array2.Length);
				if (stream.Length - stream.Position > 0L)
				{
					try
					{
						stream.SetLength(stream.Position);
					}
					catch
					{
						byte[] array3 = new byte[stream.Length - stream.Position];
						Random random = new Random();
						random.NextBytes(array3);
						stream.Write(array3, 0, array3.Length);
					}
				}
			}
		}

		public void Save(Stream stream)
		{
			Save(stream, false);
		}

		private static ulong GetFileId(Stream stream)
		{
			try
			{
				FileStream fileStream = stream as FileStream;
				if (fileStream != null)
				{
					for (int i = 0; i < 2; i++)
					{
						if (i == 1)
						{
							long position = stream.Position;
							long num = 1024L - stream.Length;
							if (num > 0L && stream.CanWrite)
							{
								byte[] array = new byte[num];
								Random random = new Random();
								random.NextBytes(array);
								fileStream.Seek(0L, SeekOrigin.End);
								fileStream.Write(array, 0, array.Length);
								fileStream.Seek(position, SeekOrigin.Begin);
							}
						}
						fileStream.Flush();
						byte[] ioStatusBlock = new byte[16];
						byte[] array2 = new byte[8];
						ulong[] array3 = new ulong[16];
						uint num2 = 0u;
						IntPtr intPtr = IntPtr.Zero;
						IsolatedStorageFileStream isolatedStorageFileStream = fileStream as IsolatedStorageFileStream;
						if (isolatedStorageFileStream != null)
						{
							FieldInfo field = isolatedStorageFileStream.GetType().GetField("m_fs", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetField);
							if (field != null)
							{
								FileStream fileStream2 = field.GetValue(isolatedStorageFileStream) as FileStream;
								if (fileStream2 != null)
								{
									intPtr = fileStream2.Handle;
								}
							}
						}
						else
						{
							intPtr = fileStream.Handle;
						}
						if (intPtr != IntPtr.Zero)
						{
							num2 = SafeNativeMethods.NtFsControlFile(intPtr, IntPtr.Zero, IntPtr.Zero, IntPtr.Zero, ioStatusBlock, 589939u, array2, (uint)array2.Length, array3, (uint)(array3.Length * 8));
						}
						switch (num2)
						{
						case 3221225489u:
							break;
						case 0u:
							return array3[3];
						default:
							return 0uL;
						}
					}
				}
			}
			catch
			{
			}
			return 0uL;
		}

		public void Save(string fileName)
		{
			Save(fileName, false, LicenseSaveType.Normal);
		}

		public void Save()
		{
			Save((string)null);
		}

		public void Save(bool useSharedOnFail)
		{
			Save(null, useSharedOnFail, LicenseSaveType.Normal);
		}

		public void Delete(bool includeOriginal, bool removeSerialNumbers)
		{
			string location = _location;
			if (location != null)
			{
				if (location.StartsWith("isolated:"))
				{
					if (!SafeToolbox.IsFipsEnabled)
					{
						using (new Wow64Guard())
						{
							if (_isfLocation != null && File.Exists(_isfLocation))
							{
								SafeToolbox.GrantEveryoneAccess(_isfLocation);
								File.SetAttributes(_isfLocation, FileAttributes.Normal);
								File.Delete(_isfLocation);
							}
							else
							{
								string path = IsfGetFileName(location.Substring(9));
								using (IsolatedStorageFile isolatedStorageFile = GetIsfStore())
								{
									isolatedStorageFile.DeleteFile(Path.Combine(IsfGetDirectoryName(Path.GetDirectoryName(location.Substring(9))), path));
								}
							}
						}
					}
				}
				else
				{
					using (new Wow64Guard())
					{
						if (File.Exists(location))
						{
							SafeToolbox.GrantEveryoneAccess(location);
							File.SetAttributes(location, FileAttributes.Normal);
						}
						File.Delete(location);
					}
				}
				if (removeSerialNumbers)
				{
					foreach (SecureLicense item in (IEnumerable)_licenses)
					{
						item.ClearSerialNumbersAndActivationData(false);
					}
				}
				if (includeOriginal && _originalLocation != null)
				{
					try
					{
						LicenseFile licenseFile = new LicenseFile(_originalLocation, true);
						if (licenseFile != null)
						{
							licenseFile.Delete(true, removeSerialNumbers);
						}
					}
					catch (SecureLicenseException ex)
					{
						if (!(ex.ErrorId != "E_CouldNotLoadLicenseFile"))
						{
							goto end_IL_0157;
						}
						throw;
						end_IL_0157:;
					}
				}
			}
		}

		public string ToXmlString()
		{
			return ToXmlString(LicenseSaveType.SignatureCheck);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public string ToXmlString(LicenseSaveType signing)
		{
			using (MemoryStream memoryStream = new MemoryStream(1024))
			{
				XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, new UTF8Encoding(false));
				xmlTextWriter.WriteStartDocument();
				xmlTextWriter.Indentation = 1;
				xmlTextWriter.IndentChar = '\t';
				xmlTextWriter.Formatting = Formatting.Indented;
				xmlTextWriter.WriteStartElement("Licenses");
				xmlTextWriter.WriteAttributeString("id", _id.ToString());
				if (_keyName != null)
				{
					xmlTextWriter.WriteAttributeString("keyName", _keyName);
				}
				if (_saveTo != 0)
				{
					XmlTextWriter xmlTextWriter2 = xmlTextWriter;
					int saveTo = (int)_saveTo;
					xmlTextWriter2.WriteAttributeString("saveTo", saveTo.ToString());
				}
				if (_releaseVersion != 0)
				{
					xmlTextWriter.WriteAttributeString("release", _releaseVersion.ToString());
				}
				if (!_autoIncrementReleaseVersion)
				{
					xmlTextWriter.WriteAttributeString("autoIncrementReleaseVersion", "false");
				}
				if (_fileId != 0L)
				{
					xmlTextWriter.WriteAttributeString("fileId", SharedToolbox.ByteToString(BitConverter.GetBytes(_fileId), null, 9, 0, 8, true));
				}
				if (_originalLocation != null)
				{
					xmlTextWriter.WriteAttributeString("originalLocation", _originalLocation);
				}
				if (_suggestedSaveLocation != null)
				{
					xmlTextWriter.WriteAttributeString("suggestedSaveLocation", _suggestedSaveLocation);
				}
				foreach (SecureLicense item in (IEnumerable)_licenses)
				{
					if (item.WriteToXml(xmlTextWriter, LicenseSaveType.Normal, null))
					{
						continue;
					}
					throw new SecureLicenseException("E_CannotSaveLicenseInvalid", null, ErrorSeverity.Normal, item);
				}
				_metaValues.WriteToXml(xmlTextWriter, "MetaValues");
				_editorState.WriteToXml(xmlTextWriter, "EditorState");
				if (_superFormTheme != null)
				{
					_superFormTheme.WriteToXml(xmlTextWriter, signing);
				}
				_resources.WriteToXml(xmlTextWriter, signing);
				if (_templateLicense != null)
				{
					xmlTextWriter.WriteStartElement("Template");
					_templateLicense.WriteToXml(xmlTextWriter, LicenseSaveType.Normal, null);
					xmlTextWriter.WriteEndElement();
				}
				xmlTextWriter.Close();
				return Encoding.UTF8.GetString(memoryStream.ToArray());
			}
		}

		public void FromXmlString(string xml)
		{
			XmlTextReader xmlTextReader = new XmlTextReader(xml, XmlNodeType.Element, null);
			LoadFromXml(xmlTextReader);
			xmlTextReader.Close();
		}

		private void LoadFromXml(XmlReader reader)
		{
			_licenses.Clear();
			_metaValues.Clear();
			_resources.Clear();
            if (_superFormTheme != null)
            {
                _superFormTheme.Changed -= BubbleChanged;
                _superFormTheme = null;
            }
            AssertNotReadOnly();
			reader.MoveToContent();
			if (!reader.EOF)
			{
				if (reader.Name == "Licenses")
				{
					_id = reader.GetAttribute("id");
					if (_id == null)
					{
						_id = Guid.NewGuid().ToString("N");
					}
					_keyName = reader.GetAttribute("keyName");
					string attribute = reader.GetAttribute("saveTo");
					if (attribute != null)
					{
						_saveTo = (SaveLocations)SafeToolbox.FastParseInt32(attribute);
					}
					else
					{
						_saveTo = SaveLocations.NotSet;
					}
					attribute = reader.GetAttribute("fileId");
					if (attribute != null)
					{
						_fileId = BitConverter.ToUInt64(SharedToolbox.StringToByte(attribute, null, 9), 0);
					}
					else
					{
						_fileId = 0uL;
					}
					attribute = reader.GetAttribute("release");
					if (attribute != null)
					{
						_releaseVersion = SafeToolbox.FastParseInt32(attribute);
					}
					else
					{
						_releaseVersion = 0;
					}
					_autoIncrementReleaseVersion = (reader.GetAttribute("autoIncrementReleaseVersion") != "false");
					_originalLocation = reader.GetAttribute("originalLocation");
					_suggestedSaveLocation = reader.GetAttribute("suggestedSaveLocation");
					reader.Read();
					while (!reader.EOF)
					{
						reader.MoveToContent();
						if (reader.IsStartElement())
						{
							switch (reader.Name)
							{
							case "License":
							{
								SecureLicense secureLicense2 = new SecureLicense();
								secureLicense2._licenseFile = this;
								secureLicense2.ReadFromXml(reader, null);
								_licenses.Add(secureLicense2);
								break;
							}
							case "MetaValues":
								_metaValues.ReadFromXml(reader);
								break;
							case "EditorState":
								_editorState.ReadFromXml(reader);
								break;
							case "Resources":
								_resources.ReadFromXml(reader);
								break;
							case "Theme":
							{
								SuperFormTheme superFormTheme = new SuperFormTheme();
								superFormTheme.ReadFromXml(reader);
								if (superFormTheme.ShouldSerialize())
								{
									_superFormTheme = superFormTheme;
									_superFormTheme.Changed += BubbleChanged;
								}
								break;
							}
							case "Template":
								reader.Read();
								if (reader.IsStartElement("License"))
								{
									SecureLicense secureLicense = new SecureLicense();
									secureLicense._licenseFile = this;
									secureLicense.ReadFromXml(reader, new byte[32]);
									_templateLicense = secureLicense;
									_templateLicense._licenseFile = this;
								}
								break;
							default:
								reader.Skip();
								break;
							}
							continue;
						}
						reader.Read();
						break;
					}
				}
				else if (reader.Name == "License")
				{
					SecureLicense secureLicense3 = new SecureLicense();
					secureLicense3.ReadFromXml(reader, null);
					_licenses.Add(secureLicense3);
				}
				if (_keyName == null && Licenses.Count > 0)
				{
					_keyName = Licenses[0].KeyName;
				}
			}
		}

		public void WriteToResponse(string fileName, bool direct)
		{
			if (!HttpContext.Current.Response.BufferOutput)
			{
				throw new SecureLicenseException("E_ResponseMustBeBuffered");
			}
			HttpContext.Current.Response.Clear();
			if (HttpContext.Current.Request.Browser.Browser.IndexOf("MSIE") > -1)
			{
				HttpContext.Current.Response.ContentType = "application/ms-download";
			}
			else
			{
				HttpContext.Current.Response.ContentType = "application/octet-stream";
			}
			if (!direct)
			{
				HttpContext.Current.Response.AppendHeader("Content-disposition", string.Format(CultureInfo.InvariantCulture, "attachment;fileName=\"{0}\"", fileName));
			}
			HttpContext.Current.Response.Write(ToXmlString());
		}

		public void WriteToResponse(string fileName)
		{
			WriteToResponse(fileName, false);
		}

		public void WriteToResponse()
		{
			WriteToResponse((Location == null || Location.Length == 0) ? "License.lic" : Path.GetFileName(Location), false);
		}

		internal void SaveOnValid(SecureLicenseContext context, bool asSystem)
		{
			lock (SecureLicenseManager.AsyncLock)
			{
				if (context != null)
				{
					foreach (SecureLicense item in (IEnumerable)_licenses)
					{
						context.EnsureEncryption(item);
					}
				}
				bool flag;
				if (!(flag = ((_saveTo & SaveLocations.SharedFolder) != SaveLocations.NotSet)) && _saveTo == SaveLocations.NotSet && _location != null && IsEmbedded && !_location.StartsWith(Config.SharedFolder, StringComparison.InvariantCultureIgnoreCase))
				{
					flag = true;
				}
				if (flag)
				{
					string text = _suggestedSaveLocation;
					if (text == null)
					{
						text = Path.Combine(Config.SharedFolder, (_location != null) ? Path.GetFileNameWithoutExtension(_location) : Guid.NewGuid().ToString("N"));
					}
					if (Path.GetExtension(text) != ".lic")
					{
						text += ".lic";
					}
					if (_originalLocation == null)
					{
						_originalLocation = _location;
					}
					Save(text, true, LicenseSaveType.Normal, !SafeToolbox.IsWebRequest && asSystem, true);
				}
				else if (_location != null)
				{
					Save(_location, true, LicenseSaveType.Normal, !SafeToolbox.IsWebRequest && asSystem, true);
				}
				_locationState = LocationState.Unaltered;
			}
		}

		internal static string IsfGetDirectoryName(string path)
		{
			return SharedToolbox.GetHashCode(Path.GetFullPath(path)).ToString("X", CultureInfo.InvariantCulture);
		}

		internal static string IsfGetFileName(string path)
		{
			if (path != null && path.Length != 0)
			{
				return SharedToolbox.GetHashCode(Path.GetFullPath(path.ToLower(CultureInfo.InvariantCulture))).ToString("X", CultureInfo.InvariantCulture);
			}
			return SharedToolbox.GetHashCode("").ToString("X", CultureInfo.InvariantCulture);
		}

		internal static bool IsfFileExists(string path)
		{
			if (SafeToolbox.IsFipsEnabled)
			{
				return false;
			}
			try
			{
				if (IsfDirectoryExists(Path.GetDirectoryName(path)))
				{
					using (IsolatedStorageFile isolatedStorageFile = GetIsfStore())
					{
						return isolatedStorageFile.GetFileNames(IsfGetFileName(path)).Length > 0;
					}
				}
				return false;
			}
			catch
			{
			}
			return false;
		}

		internal static bool IsfDirectoryExists(string path)
		{
			if (SafeToolbox.IsFipsEnabled)
			{
				return false;
			}
			try
			{
				using (IsolatedStorageFile isolatedStorageFile = GetIsfStore())
				{
					return isolatedStorageFile.GetDirectoryNames(IsfGetDirectoryName(path)).Length > 0;
				}
			}
			catch
			{
			}
			return false;
		}

		internal static IsolatedStorageFile GetIsfStore()
		{
			IsolatedStorageFile isolatedStorageFile = null;
			try
			{
				isolatedStorageFile = IsolatedStorageFile.GetUserStoreForAssembly();
				isolatedStorageFile.GetDirectoryNames("*.test");
				return isolatedStorageFile;
			}
			catch
			{
			}
			return IsolatedStorageFile.GetUserStoreForDomain();
		}

		internal static string MakeSharedName(string path)
		{
			string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(path);
			return Path.Combine(Config.SharedFolder, string.Format("Mirror\\{0}.{1:X}.lic", fileNameWithoutExtension, path.ToLower().GetHashCode()));
		}

		private static bool IsSharedMirroredFile(string path)
		{
			string text = Path.Combine(Config.SharedFolder, "Mirror");
			return string.Compare(path, 0, text, 0, text.Length, true) == 0;
		}

		private static void EnsureSharedMirrorFolder()
		{
			string path = Path.Combine(Config.SharedFolder, "Mirror");
			if (!Directory.Exists(path))
			{
				Directory.CreateDirectory(path);
			}
		}

		public object Clone()
		{
			LicenseFile licenseFile = new LicenseFile();
			licenseFile._disposition = _disposition;
			licenseFile._location = _location;
			licenseFile._metaValues = new LicenseValuesDictionary();
			licenseFile._saveTo = _saveTo;
			licenseFile._id = _id;
			licenseFile._keyName = _keyName;
			if (_superFormTheme != null)
			{
				licenseFile._superFormTheme = new SuperFormTheme(_superFormTheme, _superFormTheme.Colors["Base"]);
			}
			if (_resources != null)
			{
				licenseFile._resources = new LicenseResourceCollection();
				foreach (LicenseResource item in (IEnumerable)_resources)
				{
					licenseFile._resources.Add(item);
				}
			}
			foreach (DictionaryEntry metaValue in _metaValues)
			{
				licenseFile._metaValues.Add(metaValue.Key as string, metaValue.Value as string);
			}
			foreach (SecureLicense item2 in (IEnumerable)_licenses)
			{
				SecureLicense secureLicense2 = new SecureLicense();
				secureLicense2.FromXmlString(item2.ToXmlString());
				licenseFile.Licenses.Add(secureLicense2);
			}
			return licenseFile;
		}
	}
}
