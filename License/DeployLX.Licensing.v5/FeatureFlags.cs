using System;

namespace DeployLX.Licensing.v5
{
	[Flags]
	public enum FeatureFlags
	{
		Feature0 = 0x1,
		Feature1 = 0x2,
		Feature2 = 0x4,
		Feature3 = 0x8,
		Feature4 = 0x10,
		Feature5 = 0x20,
		Feature6 = 0x40,
		Feature7 = 0x80,
		Feature8 = 0x100,
		Feature9 = 0x200,
		Feature10 = 0x400,
		Feature11 = 0x800,
		Feature12 = 0x1000,
		Feature13 = 0x2000,
		Feature14 = 0x4000,
		Feature15 = 0x8000,
		Feature16 = 0x10000,
		Feature17 = 0x20000,
		Feature18 = 0x40000,
		Feature19 = 0x80000,
		Feature20 = 0x100000,
		Feature21 = 0x200000,
		Feature22 = 0x400000,
		Feature23 = 0x800000,
		Feature24 = 0x1000000,
		Feature25 = 0x2000000,
		Feature26 = 0x4000000,
		Feature27 = 0x8000000,
		Feature28 = 0x10000000,
		Feature29 = 0x20000000,
		Feature30 = 0x40000000,
		All = int.MaxValue,
		None = 0x0
	}
}
