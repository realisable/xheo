using System;
using System.Text;

namespace DeployLX.Licensing.v5
{
	public sealed class ParsedVersion : IComparable, IFormattable
	{
		private int _major;

		private int _minor;

		private int _build;

		private int _revision;

		private string _framework;

		private string _extra;

		private string _value;

		public int Major
		{
			get
			{
				return _major;
			}
			set
			{
				_major = value;
			}
		}

		public int Minor
		{
			get
			{
				return _minor;
			}
			set
			{
				_minor = value;
			}
		}

		public int Build
		{
			get
			{
				return _build;
			}
			set
			{
				_build = value;
			}
		}

		public int Revision
		{
			get
			{
				return _revision;
			}
			set
			{
				_revision = value;
			}
		}

		public string Framework
		{
			get
			{
				return _framework;
			}
		}

		public string Extra
		{
			get
			{
				return _extra;
			}
			set
			{
				_extra = value;
			}
		}

		public string Value
		{
			get
			{
				return _value;
			}
			set
			{
				Parse(value);
			}
		}

		public ParsedVersion(string value)
		{
			Parse(value);
		}

		public ParsedVersion(Version version)
		{
			_major = version.Major;
			_minor = version.Minor;
			_build = version.Build;
			_revision = version.Revision;
			_framework = PickFrameworkVersion(_build);
		}

		public void Parse(string value)
		{
			_major = -1;
			_minor = -1;
			_build = -1;
			_revision = -1;
			_framework = null;
			_extra = null;
			_value = value;
			if (value != null && value.Length > 0)
			{
				string[] array = value.Split('.', ' ', '\t');
				try
				{
					for (int i = 0; i < array.Length; i++)
					{
						string text = array[i].Trim();
						if (text.Length != 0)
						{
							if (_major == -1)
							{
								_major = SafeToolbox.FastParseInt32(text);
							}
							else if (_minor == -1)
							{
								_minor = SafeToolbox.FastParseInt32(text);
							}
							else if (text.Length > 0 && (text[0] == 'R' || text[0] == 'r'))
							{
								if (text.Length == 1)
								{
									for (i++; i < array.Length; i++)
									{
										text = array[i].Trim();
										if (text.Length != 0)
										{
											break;
										}
									}
									if (text.Length > 0)
									{
										_revision = SafeToolbox.FastParseInt32(text);
									}
								}
								else
								{
									_revision = SafeToolbox.FastParseInt32(text.Substring(1));
								}
							}
							else if (_build == -1)
							{
								_build = SafeToolbox.FastParseInt32(text);
								_framework = PickFrameworkVersion(_build);
							}
							else if (_revision == -1)
							{
								_revision = SafeToolbox.FastParseInt32(text);
							}
							else
							{
								if (_extra != null)
								{
									_extra += ' ';
								}
								_extra += text;
							}
						}
					}
				}
				catch (Exception innerException)
				{
					throw new SecureLicenseException("E_UnknownVersionValue", innerException, value);
				}
			}
		}

		private static string PickFrameworkVersion(int build)
		{
			switch (build)
			{
			case 1000:
			case 3300:
				return "1.0";
			case 3000:
			case 4506:
			case 6920:
				return "3.0";
			case 1100:
			case 5000:
				return "1.1";
			case 4000:
			case 30319:
				return "4.0";
			default:
				return null;
			case 2000:
			case 3600:
			case 50727:
				return "2.0";
			case 3500:
			case 21022:
			case 30729:
				return "3.5";
			}
		}

		public override string ToString()
		{
			return ToString(null, null);
		}

		public string ToString(string format)
		{
			return ToString(format, null);
		}

		public string ToString(string format, IFormatProvider formatProvider)
		{
			StringBuilder stringBuilder = new StringBuilder(16);
			if (format == null)
			{
				format = "d";
			}
			for (int i = 0; i < format.Length; i++)
			{
				char c = format[i];
				switch (c)
				{
				case 'b':
					stringBuilder.Append(Math.Max(_build, 0));
					break;
				case 'd':
					if (_major >= 0)
					{
						stringBuilder.Append(_major);
						if (_minor >= 0)
						{
							stringBuilder.Append('.');
							stringBuilder.Append(_minor);
							if (_framework == null && _build >= 0)
							{
								if (_build >= 0)
								{
									stringBuilder.Append('.');
									stringBuilder.Append(_build);
									if (_revision >= 0)
									{
										stringBuilder.Append('.');
										stringBuilder.Append(_revision);
									}
								}
							}
							else if (_revision >= 0)
							{
								stringBuilder.Append(" r");
								stringBuilder.Append(_revision);
							}
						}
					}
					if (_extra != null)
					{
						stringBuilder.Append(' ');
						stringBuilder.Append(_extra);
					}
					break;
				case 'f':
					stringBuilder.Append(_framework);
					break;
				case '\\':
					if (i + 1 < format.Length)
					{
						stringBuilder.Append(format[i + 1]);
					}
					i++;
					break;
				case 'M':
					stringBuilder.Append(Math.Max(_major, 0));
					break;
				case 'v':
					stringBuilder.Append(_value);
					break;
				case 'x':
					stringBuilder.Append(_extra);
					break;
				case 'p':
					IfAppend(stringBuilder, _major);
					stringBuilder.Append('.');
					IfAppend(stringBuilder, _minor);
					stringBuilder.Append('.');
					IfAppend(stringBuilder, _build);
					stringBuilder.Append('.');
					IfAppend(stringBuilder, _revision);
					if (_extra != null)
					{
						stringBuilder.Append(' ');
						stringBuilder.Append(_extra);
					}
					break;
				default:
					stringBuilder.Append(c);
					break;
				case 'r':
					stringBuilder.Append(Math.Max(_revision, 0));
					break;
				case 'm':
					stringBuilder.Append(Math.Max(_minor, 0));
					break;
				}
			}
			return stringBuilder.ToString();
		}

		private static void IfAppend(StringBuilder result, int value)
		{
			if (value >= 0)
			{
				string str = "00000000";
				str += value.ToString();
				result.Append(str.Substring(str.Length - 8));
			}
			else
			{
				result.Append("        ");
			}
		}

		public int CompareTo(ParsedVersion version)
		{
			return CompareTo((object)version);
		}

		public int CompareTo(Version version)
		{
			return CompareTo((object)version);
		}

		public int CompareTo(object obj)
		{
			if (obj == null)
			{
				return 1;
			}
			ParsedVersion parsedVersion = obj as ParsedVersion;
			if (parsedVersion == null)
			{
				if (obj is string)
				{
					try
					{
						parsedVersion = new ParsedVersion(obj as string);
					}
					catch
					{
						return 1;
					}
				}
				else if (obj is Version)
				{
					try
					{
						parsedVersion = new ParsedVersion((Version)obj);
					}
					catch
					{
						return 1;
					}
				}
				if (parsedVersion == null)
				{
					return 1;
				}
			}
			int num = Major.CompareTo(parsedVersion.Major);
			if (num != 0)
			{
				return num;
			}
			num = Minor.CompareTo(parsedVersion.Minor);
			if (num != 0)
			{
				return num;
			}
			num = Build.CompareTo(parsedVersion.Build);
			if (num != 0)
			{
				return num;
			}
			num = Revision.CompareTo(parsedVersion.Revision);
			if (num != 0)
			{
				return num;
			}
			if (Extra == null)
			{
				if (parsedVersion.Extra != null)
				{
					return -1;
				}
				return 0;
			}
			return Extra.CompareTo(parsedVersion.Extra);
		}

		public Version ToVersion()
		{
			return new Version(_value);
		}
	}
}
