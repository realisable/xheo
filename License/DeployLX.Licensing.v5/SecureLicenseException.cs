using System;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using System.Runtime.Serialization;
using System.Security;
using System.Security.Permissions;
using System.Text;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public class SecureLicenseException : Exception, ISerializable
	{
		private string _detailedMessage;

		private string _message;

		private readonly object[] _args;

		private readonly string _errorId;

		private readonly ErrorSeverity _severity = ErrorSeverity.Normal;

		private static readonly StaticResources _sr = new StaticResources("DeployLX.Licensing.v5");

		private readonly NameValueCollection _additionalInformation = new NameValueCollection();

		private readonly ValidationRecordCollection _validationRecords = new ValidationRecordCollection();

		public override string Message
		{
			get
			{
				return _message;
			}
		}

		public string ErrorId
		{
			get
			{
				return _errorId;
			}
		}

		public ErrorSeverity Severity
		{
			get
			{
				return _severity;
			}
		}

		public string DetailedMessage
		{
			get
			{
				return _detailedMessage;
			}
			set
			{
				_detailedMessage = value;
			}
		}

		public ValidationRecordCollection ValidationRecords
		{
			get
			{
				return _validationRecords;
			}
		}

		public SecureLicenseException()
		{
		}

		public SecureLicenseException(string errorId, Exception innerException, ErrorSeverity severity, params object[] args)
			: base(null, innerException)
		{
			_message = errorId;
			_errorId = errorId;
			_args = args;
			_severity = severity;
			Initialize();
		}

		public SecureLicenseException(string errorId, Exception innerException, params object[] args)
			: this(errorId, innerException, ErrorSeverity.Normal, args)
		{
		}

		public SecureLicenseException(string errorId, Exception innerException)
			: this(errorId, innerException, ErrorSeverity.Normal)
		{
		}

		public SecureLicenseException(string errorId, params object[] args)
			: this(errorId, null, ErrorSeverity.Normal, args)
		{
		}

		public SecureLicenseException(string errorId)
			: this(errorId, null, ErrorSeverity.Normal)
		{
		}

		protected SecureLicenseException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
			if (info != null)
			{
				_message = info.GetString("message");
				_errorId = info.GetString("errorId");
				_detailedMessage = info.GetString("detailedMessage");
				_severity = (ErrorSeverity)info.GetValue("severity", typeof(ErrorSeverity));
			}
		}

		[SecurityCritical]
		[SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info != null)
			{
				Initialize();
				base.GetObjectData(info, context);
				info.AddValue("message", _message);
				info.AddValue("errorId", _errorId);
				info.AddValue("detailedMessage", _detailedMessage);
				info.AddValue("severity", _severity);
			}
		}

		private void Initialize()
		{
			string text = null;
			//StaticResources staticResources = ResolveStaticResources();
			if (_detailedMessage == null)
			{
				string text2 = _errorId + "Detailed";
				//text = staticResources.GetString(text2, true, _args);
				if (text != null && text.Length != 0 && !(text == text2))
				{
					_detailedMessage = text;
				}
				else
				{
					_detailedMessage = null;
				}
			}
			if (_message == _errorId)
			{
				//text = staticResources.GetString(_errorId, true, _args);
				if (text != null && text.Length >= 0 && text != _errorId)
				{
					_message = text;
				}
				else
				{
					text = _sr.GetString(_errorId, true, _args);
					if (text != null && text.Length >= 0)
					{
						_message = text;
					}
				}
			}
			if (_args != null)
			{
				for (int i = 0; i < _args.Length; i++)
				{
					_additionalInformation.Add("Arg" + i, (_args[i] == null) ? null : _args[i].ToString());
				}
			}
		}

		private StaticResources ResolveStaticResources()
		{
			if (base.GetType().Assembly == typeof(SecureLicense).Assembly)
			{
				return _sr;
			}
			Type type = base.GetType();
			Assembly assembly = type.Assembly;
			string name = assembly.GetName().Name;
			StaticResources sRForNameByExistingResources = GetSRForNameByExistingResources(assembly, name);
			if (sRForNameByExistingResources != null)
			{
				return sRForNameByExistingResources;
			}
			sRForNameByExistingResources = GetSRForNameByExistingResources(assembly, name.Substring(0, name.LastIndexOf('.')));
			return sRForNameByExistingResources ?? _sr;
		}

		private StaticResources GetSRForNameByExistingResources(Assembly myAsm, string resourceName)
		{
			string value = resourceName + ".resources";
			string[] manifestResourceNames = myAsm.GetManifestResourceNames();
			int num = 0;
			while (true)
			{
				if (num < manifestResourceNames.Length)
				{
					string text = manifestResourceNames[num];
					if (text.EndsWith(resourceName))
					{
						break;
					}
					if (text.EndsWith(value))
					{
						break;
					}
					num++;
					continue;
				}
				return null;
			}
			return new StaticResources(resourceName);
		}

		public override string ToString()
		{
			Initialize();
			string text = FormatException(this);
			if (_validationRecords.Count > 0)
			{
				return string.Format("{0}\r\n----\r\n{1}", text, _validationRecords);
			}
			return text;
		}

		public static string FormatException(Exception ex)
		{
			return FormatException(ex, false);
		}

		public static string FormatException(Exception ex, bool debugMode)
		{
			if (ex == null)
			{
				return "";
			}
			StringBuilder stringBuilder = new StringBuilder();
			string name = ex.GetType().Name;
			if (ex.Message != null && ex.Message.Length > 0)
			{
				stringBuilder.AppendFormat(CultureInfo.InvariantCulture, "{0}: {1}", name, ex.Message);
			}
			else
			{
				stringBuilder.Append(name);
			}
			SecureLicenseException ex2 = ex as SecureLicenseException;
			if (ex2 != null && ex2.DetailedMessage != null)
			{
				stringBuilder.Append(Environment.NewLine);
				stringBuilder.Append('\t');
				stringBuilder.Append(ex2.DetailedMessage);
			}
			if (ex.InnerException != null)
			{
				stringBuilder.Append(" ---> ");
				stringBuilder.Append(FormatException(ex.InnerException));
				stringBuilder.Append(Environment.NewLine);
				stringBuilder.Append("   --- EOE ---");
				stringBuilder.Append(Environment.NewLine);
			}
			if (ex.StackTrace != null)
			{
				stringBuilder.Append(Environment.NewLine);
				stringBuilder.Append(FormatStackTrace(ex, debugMode));
			}
			return stringBuilder.ToString();
		}

		public static string FormatStackTrace(Exception ex)
		{
			return FormatStackTrace(ex, false);
		}

		public static string FormatStackTrace(Exception ex, bool debugMode)
		{
			if (ex == null)
			{
				return "";
			}
			if (!debugMode)
			{
				return ex.StackTrace;
			}
			StackTrace stack = new StackTrace(ex, true);
			return FormatStackTrace(stack, ex.Message);
		}

		internal static string FormatStackTrace(StackTrace stack, string message)
		{
			string format = "{0}({1},{2}): {3} ";
			string newLine = Environment.NewLine;
			StringBuilder stringBuilder = new StringBuilder(255);
			int num = 0;
			while (true)
			{
				if (num >= stack.FrameCount)
				{
					break;
				}
				StackFrame frame = stack.GetFrame(num);
				stringBuilder.Append("    ");
				if (frame.GetILOffset() != -1)
				{
					string text = null;
					try
					{
						text = frame.GetFileName();
					}
					catch (SecurityException)
					{
					}
					if (text != null)
					{
						stringBuilder.AppendFormat(CultureInfo.InvariantCulture, format, text, frame.GetFileLineNumber(), frame.GetFileColumnNumber() - 1, message);
					}
				}
				MethodBase method = frame.GetMethod();
				Type declaringType = method.DeclaringType;
				if (declaringType != null)
				{
					string @namespace = declaringType.Namespace;
					if (@namespace != null)
					{
						stringBuilder.Append(@namespace);
						if (stringBuilder != null)
						{
							stringBuilder.Append(".");
						}
					}
					stringBuilder.Append(declaringType.Name);
					stringBuilder.Append(".");
				}
				stringBuilder.Append(method.Name);
				MethodInfo methodInfo = method as MethodInfo;
				if (methodInfo != null && methodInfo.IsGenericMethod)
				{
					Type[] genericArguments = methodInfo.GetGenericArguments();
					stringBuilder.Append("<");
					int i = 0;
					bool flag = true;
					for (; i < genericArguments.Length; i++)
					{
						if (!flag)
						{
							stringBuilder.Append(",");
						}
						else
						{
							flag = false;
						}
						stringBuilder.Append(genericArguments[i].Name);
					}
					stringBuilder.Append(">");
				}
				stringBuilder.Append("(");
				ParameterInfo[] parameters = method.GetParameters();
				for (int j = 0; j < parameters.Length; j++)
				{
					string str = "<UnknownType>";
					if (parameters[j].ParameterType != null)
					{
						str = parameters[j].ParameterType.Name;
					}
					stringBuilder.Append(((j != 0) ? ", " : "") + str + " " + parameters[j].Name);
				}
				stringBuilder.Append(")");
				if (num != stack.FrameCount - 1)
				{
					stringBuilder.Append(newLine);
				}
				num++;
			}
			return stringBuilder.ToString();
		}
	}
}
