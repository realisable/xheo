namespace DeployLX.Licensing.v5
{
	public enum FormResult
	{
		Unknown,
		Success,
		Failure,
		Incomplete,
		NotShown,
		Retry,
		Closed,
		DontReshowPanel = 0x80000
	}
}
