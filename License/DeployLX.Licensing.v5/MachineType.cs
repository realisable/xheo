namespace DeployLX.Licensing.v5
{
	public enum MachineType
	{
		Any,
		Desktop,
		Laptop,
		Physical,
		Virtual
	}
}
