using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Security;

namespace DeployLX.Licensing.v5
{
	internal sealed class Mapi
	{
		[SuppressUnmanagedCodeSecurity]
		private sealed class Win32
		{
			public struct MapiMessage
			{
				public uint Reserved;

				public string Subject;

				public string NoteText;

				public string MessageType;

				public string DateReceived;

				public string ConversationID;

				public uint Flags;

				public IntPtr Originator;

				public uint RecipCount;

				public IntPtr Recips;

				public uint FileCount;

				public IntPtr Files;
			}

			public struct MapiRecipDesc
			{
				public uint Reserved;

				public uint RecipClass;

				public string Name;

				public string Address;

				public uint EIDSize;

				public IntPtr EntryID;
			}

			public struct MapiFileDesc
			{
				public uint Reserved;

				public int Flags;

				public int Position;

				public string PathName;

				public string FileName;

				public int FileType;
			}

			private Win32()
			{
			}

			[DllImport("mapi32", CharSet = CharSet.Ansi, SetLastError = true)]
			public static extern int MAPISendMail(IntPtr Session, IntPtr uiParam, ref MapiMessage message, int flags, int reserved);
		}

		private Mapi()
		{
		}

		[SecuritySafeCritical]
		public static bool SendMail(string to, string from, string subject, string message, string attachmentName, byte[] attachment)
		{
			try
			{
				Win32.MapiMessage mapiMessage = default(Win32.MapiMessage);
				mapiMessage.Reserved = 0u;
				mapiMessage.Subject = subject;
				mapiMessage.NoteText = message;
				mapiMessage.MessageType = null;
				mapiMessage.DateReceived = null;
				mapiMessage.ConversationID = null;
				mapiMessage.Flags = 0u;
				mapiMessage.Originator = IntPtr.Zero;
				mapiMessage.RecipCount = 0u;
				mapiMessage.Recips = IntPtr.Zero;
				mapiMessage.FileCount = 0u;
				mapiMessage.Files = IntPtr.Zero;
				Win32.MapiRecipDesc mapiRecipDesc = default(Win32.MapiRecipDesc);
				mapiRecipDesc.Reserved = 0u;
				mapiRecipDesc.RecipClass = 1u;
				mapiRecipDesc.Name = to;
				mapiRecipDesc.Address = "SMTP:" + to;
				mapiRecipDesc.EIDSize = 0u;
				mapiRecipDesc.EntryID = IntPtr.Zero;
				int cb = Marshal.SizeOf(mapiRecipDesc);
				mapiMessage.Recips = Marshal.AllocHGlobal(cb);
				mapiMessage.RecipCount = 1u;
				Marshal.StructureToPtr(mapiRecipDesc, mapiMessage.Recips, false);
				if (attachmentName != null)
				{
					Win32.MapiFileDesc mapiFileDesc = default(Win32.MapiFileDesc);
					mapiFileDesc.Reserved = 0u;
					mapiFileDesc.Flags = 0;
					mapiFileDesc.Position = -1;
					mapiFileDesc.FileName = Path.GetFileName(attachmentName);
					mapiFileDesc.FileType = 0;
					if (attachment != null)
					{
						using (FileStream fileStream = new FileStream(mapiFileDesc.PathName = Path.GetTempFileName(), FileMode.Create, FileAccess.Write, FileShare.None))
						{
							fileStream.Write(attachment, 0, attachment.Length);
						}
					}
					else
					{
						mapiFileDesc.PathName = Path.GetFullPath(attachmentName);
					}
					mapiMessage.Files = Marshal.AllocHGlobal(Marshal.SizeOf(mapiFileDesc));
					Marshal.StructureToPtr(mapiFileDesc, mapiMessage.Files, false);
					mapiMessage.FileCount = 1u;
				}
				int errorCode = Win32.MAPISendMail(IntPtr.Zero, IntPtr.Zero, ref mapiMessage, 9, 0);
				int lastWin32Error = Marshal.GetLastWin32Error();
				Marshal.ThrowExceptionForHR(errorCode);
				if (lastWin32Error != 0)
				{
					Marshal.ThrowExceptionForHR(Marshal.GetHRForLastWin32Error());
				}
				Marshal.FreeHGlobal(mapiMessage.Recips);
			}
			catch (COMException)
			{
				return false;
			}
			catch (FileNotFoundException)
			{
				return false;
			}
			return true;
		}
	}
}
