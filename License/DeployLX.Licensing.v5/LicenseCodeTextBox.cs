using System;
using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public class LicenseCodeTextBox : PromptMaskedEdit
	{
		private string _characterSet;

		private string _characterSetUpper;

		public string CharacterSet
		{
			get
			{
				return _characterSet;
			}
			set
			{
				_characterSet = value;
				_characterSetUpper = null;
			}
		}

		internal override IsValidCharDelegate ParseMaskCharacter(char c, int position)
		{
			if (c == 'C')
			{
				return this.IsValidCharacter;
			}
			return base.ParseMaskCharacter(c, position);
		}

		private bool IsValidCharacter(ref char c, CharacterCasing casing)
		{
			if (_characterSet == null)
			{
				return false;
			}
			if (casing == CharacterCasing.Upper)
			{
				char c2 = char.ToUpper(c);
				if (_characterSetUpper == null)
				{
					_characterSetUpper = _characterSet.ToUpper();
				}
				if (_characterSetUpper.IndexOf(c) == -1 && _characterSetUpper.IndexOf(c2) == -1)
				{
					return false;
				}
				c = c2;
				return true;
			}
			return _characterSet.IndexOf(c) != -1;
		}
	}
}
