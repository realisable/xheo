using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Threading;
using System.Web.Services.Protocols;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	[LimitEditor("LicenseServerLimitEditor", IconResource = "asmres://DeployLX.Licensing.DlxAddIn.v5/DeployLX.Licensing.DlxAddIn.v5.Resources.Limits.LicenseServer.png")]
	public class LicenseServerLimit : DelayedLimit, IServerLimit
	{
		public class ValidationStateIds
		{
			public const string Verified = "Verified";

			public const string Denied = "Denied";

			public const string Disconnected = "Disconnected";
		}

		private readonly UriCollection _servers;

		private int _serverRetries;

		private int _allowedFailures;

		private bool _isPeriodic;

		private int _cachePeriodLength = 24;

		private DateTime _cachePeriodExpires = DateTime.MinValue;

		private string _validatedMachine;

		private static readonly List<string> _validationStates;

		private string _state;

		[Obsolete("Use the Disconnected substate instead.")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public int AllowedFailures
		{
			get
			{
				return _allowedFailures;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_allowedFailures != value)
				{
					_allowedFailures = value;
					base.OnChanged("AllowedFailures");
				}
			}
		}

		[Browsable(false)]
		public DateTime CachePeriodExpires
		{
			get
			{
				return _cachePeriodExpires;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_cachePeriodExpires != value)
				{
					_cachePeriodExpires = value;
					base.OnChanged("CachePeriodExpires");
				}
			}
		}

		public int CachePeriodLength
		{
			get
			{
				return _cachePeriodLength;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_cachePeriodLength != value)
				{
					_cachePeriodLength = value;
					base.OnChanged("CachePeriodLength");
				}
			}
		}

		public override string Description
		{
			get
			{
				return SR.GetString("M_LicenseServerLimitDescription");
			}
		}

		public override IList<string> ValidationStates
		{
			get
			{
				return _validationStates;
			}
		}

		public override string State
		{
			get
			{
				return _state ?? "Disconnected";
			}
		}

		public bool IsPeriodic
		{
			get
			{
				return _isPeriodic;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_isPeriodic != value)
				{
					_isPeriodic = value;
					base.OnChanged("IsPeriodic");
				}
			}
		}

		public override string Name
		{
			get
			{
				return "License Server";
			}
		}

		public int ServerRetries
		{
			get
			{
				return _serverRetries;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_serverRetries != value)
				{
					_serverRetries = value;
					base.OnChanged("ServerRetries");
				}
			}
		}

		public UriCollection Servers
		{
			get
			{
				return _servers;
			}
		}

		[Browsable(false)]
		public string ValidatedMachine
		{
			get
			{
				return _validatedMachine;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_validatedMachine != value)
				{
					_validatedMachine = value;
					base.OnChanged("ValidatedMachine");
				}
			}
		}

		public LicenseServerLimit()
		{
			_servers = new UriCollection();
			_servers.Changed += _addresses_Changed;
		}

		static LicenseServerLimit()
		{
			_validationStates = new List<string>();
			_validationStates.Add("Verified");
			_validationStates.Add("Denied");
			_validationStates.Add("Disconnected");
		}

		protected internal override void MakeReadOnly()
		{
			base.MakeReadOnly();
			_servers.MakeReadOnly();
		}

		public ValidationResult ValidateAtServer(SecureLicenseContext context, bool reportError)
		{
			return ValidateAtServerInternal(context, reportError, null);
		}

		public AsyncValidationRequest ValidateAtServerAsync(SecureLicenseContext context, bool reportError, EventHandler completedHandler)
		{
			return new AsyncValidationRequest(ValidateAtServerInternal, context, reportError, null, completedHandler);
		}

		public bool RenewPeriod(string machineProfile, IServerRequestContext context)
		{
			base.AssertNotReadOnly();
			if (_isPeriodic && machineProfile != null)
			{
				DateTime dateTime = (context == null) ? DateTime.UtcNow : context.CurrentDateAndTime;
				CachePeriodExpires = dateTime.AddHours((double)_cachePeriodLength);
				ValidatedMachine = machineProfile;
				return true;
			}
			return false;
		}

		public bool RenewPeriod(string machineProfile)
		{
			return RenewPeriod(machineProfile, null);
		}

		protected internal override ValidationResult DelayValidate(SecureLicenseContext context)
		{
			bool flag = true;
			bool flag2 = false;
			if (context.Items[base.UniqueId + ".Tried"] != null && _state != null)
			{
				if (!(_state == "Verified") && !base.Limits.HasStateLimits(_state))
				{
					return ValidationResult.Invalid;
				}
				return ValidationResult.Valid;
			}
			context.Items[base.UniqueId + ".Tried"] = this;
			_state = "Denied";
			if (!_isPeriodic)
			{
				ValidationResult validationResult = DoValidateAtServer(context);
				if (validationResult == ValidationResult.Invalid && base.Limits.HasStateLimits(_state))
				{
					return ValidationResult.Valid;
				}
				return validationResult;
			}
			if (_validatedMachine != null && _cachePeriodExpires > context.CurrentDateAndTime && MachineProfile.CompareHash(_validatedMachine, MachineProfile.Profile, false) == 0)
			{
				flag = false;
				flag2 = true;
				_state = "Verified";
			}
			if (flag)
			{
				ValidationResult validationResult2 = DoValidateAtServer(context);
				if (validationResult2 == ValidationResult.Retry)
				{
					return validationResult2;
				}
				flag2 = (validationResult2 == ValidationResult.Valid);
			}
			else
			{
				object persistentData = base.GetPersistentData("LastAutoCheck", null, PersistentDataLocationType.SharedOrUser);
				if ((persistentData == null || new DateTime((long)persistentData).Date < DateTime.Now.Date) && base.License.RuntimeState[base.UniqueId + "AutoRenew"] == null)
				{
					base.License.RuntimeState[base.UniqueId + "AutoRenew"] = true;
					ValidateAtServerAsync(context, false, RenewFinished);
				}
			}
			if (!flag2 && !base.Limits.HasStateLimits(_state))
			{
				return ValidationResult.Invalid;
			}
			return ValidationResult.Valid;
		}

		protected override ValidationResult DelayGranted(SecureLicenseContext context)
		{
			ValidationResult validationResult = DelayValidate(context);
			if (validationResult != ValidationResult.Valid)
			{
				return validationResult;
			}
			return ValidationResult.Valid;
		}

		private ValidationResult DoValidateAtServer(SecureLicenseContext context)
		{
			ValidationResult validationResult = ValidateAtServer(context, true);
			switch (validationResult)
			{
			case ValidationResult.Retry:
				return validationResult;
			case ValidationResult.Valid:
				_state = "Verified";
				break;
			}
			return validationResult;
		}

		protected override bool ReadFromXml(XmlReader reader)
		{
			if (!base.ReadFromXml(reader))
			{
				return false;
			}
			_servers.Clear();
			string attribute = reader.GetAttribute("allowedFailures");
			if (attribute != null)
			{
				_allowedFailures = SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_allowedFailures = 0;
			}
			attribute = reader.GetAttribute("serverRetries");
			if (attribute != null)
			{
				_serverRetries = SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_serverRetries = 0;
			}
			_isPeriodic = (reader.GetAttribute("isPeriodic") == "true");
			if (_isPeriodic)
			{
				attribute = reader.GetAttribute("cachePeriodLength");
				if (attribute != null)
				{
					_cachePeriodLength = SafeToolbox.FastParseInt32(attribute);
				}
				else
				{
					_cachePeriodLength = 24;
				}
				attribute = reader.GetAttribute("cachePeriodExpires");
				if (attribute != null)
				{
					_cachePeriodExpires = SafeToolbox.FastParseSortableDate(attribute);
				}
				else
				{
					_cachePeriodExpires = DateTime.MinValue;
				}
				_validatedMachine = reader.GetAttribute("validatedMachine");
			}
			if (reader.IsEmptyElement)
			{
				reader.Read();
				return true;
			}
			_servers.VerifyValues = false;
			reader.Read();
			while (!reader.EOF)
			{
				if (reader.IsStartElement())
				{
					switch (reader.Name)
					{
					case "Limit":
						if (base.ReadChildLimit(reader))
						{
							break;
						}
						return false;
					case "Server":
						attribute = reader.GetAttribute("address");
						if (attribute != null)
						{
							_servers.Add(SafeToolbox.XmlDecode(attribute));
							reader.Read();
							break;
						}
						return false;
					default:
						reader.Skip();
						break;
					}
					continue;
				}
				reader.Read();
				break;
			}
			_servers.VerifyValues = true;
			return true;
		}

		protected override bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			base.WriteToXml(writer, signing);
			if (_allowedFailures != 0)
			{
				writer.WriteAttributeString("allowedFailures", _allowedFailures.ToString());
			}
			if (_isPeriodic)
			{
				writer.WriteAttributeString("isPeriodic", "true");
				if (_cachePeriodLength != 24)
				{
					writer.WriteAttributeString("cachePeriodLength", _cachePeriodLength.ToString());
				}
				if (_validatedMachine != null)
				{
					writer.WriteAttributeString("validatedMachine", _validatedMachine);
				}
				if (_cachePeriodExpires != DateTime.MinValue)
				{
					writer.WriteAttributeString("cachePeriodExpires", SafeToolbox.FormatSortableDate(_cachePeriodExpires));
				}
			}
			if (_serverRetries > 0)
			{
				writer.WriteAttributeString("serverRetries", _serverRetries.ToString());
			}
			foreach (string item in (IEnumerable)_servers)
			{
				writer.WriteStartElement("Server");
				writer.WriteAttributeString("address", SafeToolbox.XmlEncode(item));
				writer.WriteEndElement();
			}
			return true;
		}

		private void _addresses_Changed(object sender, CollectionEventArgs e)
		{
			base.OnCollectionChanged(sender, "Servers", e);
		}

		private void RenewFinished(object sender, EventArgs e)
		{
			base.SetPersistentData("LastAutoCheck", DateTime.Now.Ticks);
		}

		private ValidationResult ValidateAtServerInternal(SecureLicenseContext context, bool reportError, object[] args)
		{
			ValidationRecord validationRecord = new ValidationRecord("E_CannotValidateAtServer", this, null, ErrorSeverity.Normal);
			if (_servers.Count == 0)
			{
				validationRecord.SubRecords.Add("E_NoServersDefined", this, null, ErrorSeverity.Low);
				return ValidationResult.Invalid;
			}
			try
			{
				bool flag = false;
				foreach (string item in (IEnumerable)_servers)
				{
					ServerResult serverResult = context.CallServer("VALIDATE", item, this, null, null, false);
					if (!serverResult.Success)
					{
						bool flag2 = false;
						bool flag3 = false;
						SecureLicenseException ex = serverResult.Exception as SecureLicenseException;
						if (serverResult.Exception != null)
						{
							if (ex != null && ex.ErrorId == "E_SimulateOffline")
							{
								flag2 = true;
							}
							else
							{
								if (serverResult.Exception.InnerException is WebException)
								{
									flag2 = true;
								}
								if (serverResult.Exception.InnerException is SoapException || (serverResult.Exception is SecureLicenseException && serverResult.Exception.InnerException == null))
								{
									flag3 = true;
								}
							}
						}
						if (!flag2 && flag3)
						{
							flag = true;
						}
						if (reportError)
						{
							if (ex != null && !context.RequestInfo.DeveloperMode)
							{
								validationRecord.SubRecords.Add(ex.ErrorId, this, null, ErrorSeverity.High, item);
							}
							else
							{
								validationRecord.SubRecords.Add("E_UnexpectedErrorFromServer", this, serverResult.Exception, ErrorSeverity.High, item);
							}
						}
						continue;
					}
					if (serverResult.Retry)
					{
						return ValidationResult.Retry;
					}
					if (serverResult.CopyLicense == null)
					{
						return ValidationResult.Valid;
					}
					LicenseServerLimit licenseServerLimit = serverResult.CopyLicense.Limits.FindLimitById(base.LimitId) as LicenseServerLimit;
					if (licenseServerLimit == null)
					{
						return context.RetryWith(serverResult.CopyLicense.LicenseFile, serverResult.CopyLicense.LicenseId);
					}
					lock (SecureLicenseManager.AsyncLock)
					{
						_cachePeriodExpires = licenseServerLimit._cachePeriodExpires;
						_validatedMachine = licenseServerLimit._validatedMachine;
						base.License.Signature = serverResult.CopyLicense.Signature;
						if (context.VerifySignature(base.License) != VerifySignatureResult.Valid)
						{
							return context.RetryWith(serverResult.CopyLicense.LicenseFile, serverResult.CopyLicense.LicenseId);
						}
					}
					return ValidationResult.Valid;
				}
				if (reportError)
				{
					context.ReportError(validationRecord);
				}
				_state = (flag ? "Denied" : "Disconnected");
				return ValidationResult.Invalid;
			}
			catch (ThreadAbortException exception)
			{
				validationRecord.SubRecords.Add("E_AbortedByUser", this, exception, ErrorSeverity.Low);
				return ValidationResult.Invalid;
			}
		}
	}
}
