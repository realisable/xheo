using System.ComponentModel;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[EditorBrowsable(EditorBrowsableState.Never)]
	public interface IXmlPersistable
	{
		bool WriteToXml(XmlWriter writer, LicenseSaveType singing);

		bool ReadFromXml(XmlReader reader);
	}
}
