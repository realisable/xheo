using System;
using System.ComponentModel;

namespace DeployLX.Licensing.v5
{
	public sealed class LicenseValidationRequestInfo
	{
		private bool _isReadOnly;

		private SupportInfo _supportInfo;

		private int _showFinalErrorReport;

		private LicenseFile _licenseFile;

		private bool _onlyCheckManualLicenseFile;

		private string[] _serialNumbers;

		private bool _shouldGetNewSerialNumber;

		private bool _developerMode;

		private bool _dontPersistToStorage;

		private bool _simulateOffline;

		private bool _dontShowDeveloperModeWarning;

		private bool _saveExternalSerials;

		private bool _shouldPersistCodes;

		private StringCollection _searchPaths = new StringCollection();

		private string[] _searchPaths_cached;

		private LicenseValuesDictionary _registrationInfo = new LicenseValuesDictionary();

		private LicenseValuesDictionary _additionalServerProperties = new LicenseValuesDictionary();

		private bool _noDelayed;

		private bool _disableTrials;

		private bool _disableCache;

		private string _elevationCommand;

		private bool _dontShowForms;

		private DateTime _testDate = DateTime.MinValue;

		private DateTime _testDateSet = DateTime.MinValue;

		public SupportInfo SupportInfo
		{
			get
			{
				return _supportInfo;
			}
			set
			{
				_supportInfo = value;
			}
		}

		public bool ShowFinalErrorReport
		{
			get
			{
				if (_showFinalErrorReport == 0 && SupportInfo != null)
				{
					return SupportInfo.ShowFinalErrorReport;
				}
				return _showFinalErrorReport != -1;
			}
			set
			{
				_showFinalErrorReport = (value ? 1 : (-1));
			}
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		[Obsolete("You should use the standard license resolution features. Please read http://goto.xheo.com/explicit-license-file-obsolete before using this property.")]
		public LicenseFile LicenseFile
		{
			get
			{
				return _licenseFile;
			}
			set
			{
				if (!_isReadOnly && (_licenseFile == null || _licenseFile == value))
				{
					_licenseFile = value;
					return;
				}
				throw new SecureLicenseException("E_ReadOnlyProperty", "LicenseFile");
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public string LicenseKey
		{
			get;
			set;
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public bool OnlyCheckManualLicenseFile
		{
			get
			{
				return _onlyCheckManualLicenseFile;
			}
			set
			{
				if (_isReadOnly)
				{
					throw new SecureLicenseException("E_ReadOnlyProperty", "OnlyCheckManualLicenseFile");
				}
				_onlyCheckManualLicenseFile = value;
			}
		}

		public string[] SerialNumbers
		{
			get
			{
				return _serialNumbers;
			}
			set
			{
				_serialNumbers = value;
			}
		}

		public string[] ExtensionCodes
		{
			get;
			set;
		}

		public bool ShouldGetNewSerialNumber
		{
			get
			{
				return _shouldGetNewSerialNumber;
			}
			set
			{
				_shouldGetNewSerialNumber = value;
			}
		}

		public bool DeveloperMode
		{
			get
			{
                return _developerMode;
			}
			set
			{
				if (_isReadOnly)
				{
					throw new SecureLicenseException("E_ReadOnlyProperty", "DeveloperMode");
				}
				_developerMode = value;
			}
		}

		public bool DontPersistToStorage
		{
			get
			{
				if (_dontPersistToStorage)
				{
					return _developerMode;
				}
				return false;
			}
			set
			{
				if (_isReadOnly)
				{
					throw new SecureLicenseException("E_ReadOnlyProperty", "DontPersistToStorage");
				}
				if (!_developerMode)
				{
					throw new SecureLicenseException("E_RequiresDeveloperMode");
				}
				_dontPersistToStorage = value;
			}
		}

		public bool SimulateOffline
		{
			get
			{
				if (_simulateOffline)
				{
					return _developerMode;
				}
				return false;
			}
			set
			{
				if (_isReadOnly)
				{
					throw new SecureLicenseException("E_ReadOnlyProperty", "SimulateOffline");
				}
				if (!_developerMode)
				{
					throw new SecureLicenseException("E_RequiresDeveloperMode");
				}
				_simulateOffline = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool DontShowDeveloperModeWarning
		{
			get
			{
				return _dontShowDeveloperModeWarning;
			}
			set
			{
				_dontShowDeveloperModeWarning = value;
			}
		}

		public bool SaveExternalSerials
		{
			get
			{
				return _saveExternalSerials;
			}
			set
			{
				_saveExternalSerials = value;
			}
		}

		[Obsolete("This was a legacy option to enable old behavior that is no longer used by DeployLX. Property has no effect.")]
		public bool ShouldPersistCodes
		{
			get
			{
				return _shouldPersistCodes;
			}
			set
			{
				_shouldPersistCodes = value;
			}
		}

		public string[] SearchPaths
		{
			get
			{
				if (_searchPaths_cached != null)
				{
					return _searchPaths_cached;
				}
				_searchPaths_cached = new string[_searchPaths.Count];
				_searchPaths.CopyTo(_searchPaths_cached, 0);
				return _searchPaths_cached;
			}
		}

		public LicenseValuesDictionary RegistrationInfo
		{
			get
			{
				return _registrationInfo;
			}
		}

		public LicenseValuesDictionary AdditionalServerProperties
		{
			get
			{
				return _additionalServerProperties;
			}
		}

		public bool NoDelayed
		{
			get
			{
				return _noDelayed;
			}
			set
			{
				_noDelayed = value;
			}
		}

		public bool DisableTrials
		{
			get
			{
				return _disableTrials;
			}
			set
			{
				_disableTrials = value;
			}
		}

		public bool DisableCache
		{
			get
			{
				return _disableCache;
			}
			set
			{
				_disableCache = value;
			}
		}

		public string ElevationCommand
		{
			get
			{
				return _elevationCommand;
			}
			set
			{
				if (!_isReadOnly && (_elevationCommand == null || !(_elevationCommand != value)))
				{
					_elevationCommand = value;
					return;
				}
				throw new SecureLicenseException("E_ReadOnlyProperty", "ElevationCommand");
			}
		}

		public bool DontShowForms
		{
			get
			{
				return _dontShowForms;
			}
			set
			{
				_dontShowForms = value;
			}
		}

		public DateTime TestDate
		{
			get
			{
				if (_testDate == DateTime.MinValue)
				{
					return _testDate;
				}
				return _testDate + (DateTime.UtcNow - _testDateSet);
			}
			set
			{
				value = value.ToUniversalTime();
				if (value < DateTime.UtcNow)
				{
					throw new SecureLicenseException("E_TestDateEarlierThanSystem");
				}
				_testDate = value;
				_testDateSet = DateTime.UtcNow;
				SecureLicenseContext.WriteDiagnosticToContext("Test Date set to: {0}", value);
			}
		}

		public ShouldValidateLicenseFilter LicenseFilter
		{
			get;
			set;
		}

		public LicenseValidationLogger Logger
		{
			get;
			set;
		}

		public ShowFormFilterHandler ShowFormFilter
		{
			get;
			set;
		}

		public void AddSearchPath(string path)
		{
			Check.NotNull(path, "path");
			_searchPaths_cached = null;
			_searchPaths.Add(path);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public void MakeReadOnly()
		{
			_isReadOnly = true;
		}

		internal void EnableDeveloperMode()
		{
			_developerMode = true;
		}
	}
}
