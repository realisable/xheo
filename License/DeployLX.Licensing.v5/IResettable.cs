namespace DeployLX.Licensing.v5
{
	public interface IResettable
	{
		void Reset(SecureLicenseContext context, int value);
	}
}
