using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	public class SplitPanel : SuperFormPanel
	{
		private sealed class SplitBufferedPanel : BufferedPanel
		{
			protected override void OnPaint(PaintEventArgs e)
			{
				SuperFormPanel superFormPanel = base.Parent as SuperFormPanel;
				if (superFormPanel != null)
				{
					GraphicsState gstate = e.Graphics.Save();
					e.Graphics.TranslateTransform((float)(-base.Left), (float)(-base.Top));
					superFormPanel.PaintPanelBackground(e);
					e.Graphics.Restore(gstate);
				}
				else
				{
					base.OnPaint(e);
				}
			}
		}

		protected GradientPanel SidePanel;

		protected BufferedPanel BodyPanel;

		private PictureBox _icon;

		private BufferedPictureBox _logo;

		private GradientPanel _sideSplitter;

		private Image _iconImage;

		private Image _logoImage;

		private string _defaultIconName = "DefaultIcon";

		private bool _usedDefaultIcon;

		[Category("Appearance")]
		public Image Icon
		{
			get
			{
				if (!base.IsHandleCreated)
				{
					return _iconImage;
				}
				return _icon.Image;
			}
			set
			{
				if (base.IsHandleCreated)
				{
					_icon.Image = base.SuperForm.GetScaledImage(value, _icon.Image);
					if (value != null)
					{
						_logo.Visible = false;
						_logo.Image = base.SuperForm.GetScaledImage(null, _logo.Image);
					}
					_icon.Visible = (!_logo.Visible && _icon.Image != null);
				}
				else
				{
					_iconImage = value;
				}
				_usedDefaultIcon = false;
			}
		}

		[Category("Appearance")]
		public Image Logo
		{
			get
			{
				if (!base.IsHandleCreated)
				{
					return _logoImage;
				}
				return _logo.Image;
			}
			set
			{
				if (base.IsHandleCreated)
				{
					_logo.Image = base.SuperForm.GetScaledImage(value, _logo.Image);
					_logo.Visible = (value != null);
					_icon.Visible = (!_logo.Visible && _icon.Image != null);
				}
				else
				{
					_logoImage = value;
				}
				_usedDefaultIcon = false;
			}
		}

		[DefaultValue("DefaultIcon")]
		[Category("Appearance")]
		public string DefaultIconName
		{
			get
			{
				return _defaultIconName;
			}
			set
			{
				_defaultIconName = value;
			}
		}

		protected SplitPanel(Limit limit)
			: base(limit)
		{
			base.DockPadding.All = 0;
			InitializeComponent();
			SidePanel.BackgroundImageLayout = ImageLayout.None;
			BodyPanel.BackgroundImageLayout = ImageLayout.None;
			SidePanel.OffsetBackgroundImage = true;
			SidePanel.Paint += SidePanel_Paint;
		}

		private void SidePanel_Paint(object sender, PaintEventArgs e)
		{
			if (base.DesignMode)
			{
				e.Graphics.FillRectangle(Brushes.Black, new Rectangle(0, base.Height - 21, 170, 21));
			}
		}

		private SplitPanel()
			: this(null)
		{
			if (LicenseManager.UsageMode == LicenseUsageMode.Designtime)
			{
				return;
			}
			throw new NotSupportedException("You must implement a constructor that takes a single limit as an argument.");
		}

		private void InitializeComponent()
		{
			SidePanel = new GradientPanel();
			_logo = new BufferedPictureBox();
			_icon = new PictureBox();
			_sideSplitter = new GradientPanel();
			BodyPanel = new SplitBufferedPanel();
			SidePanel.SuspendLayout();
			base.SuspendLayout();
			SidePanel.BackColor2 = Color.FromArgb(91, 151, 226);
			SidePanel.Controls.Add(_logo);
			SidePanel.Controls.Add(_icon);
			SidePanel.Controls.Add(_sideSplitter);
			SidePanel.Dock = DockStyle.Left;
			SidePanel.GradientOffset = new Point(0, 30);
			SidePanel.GradientScale = new Point(200, 75);
			SidePanel.GradientStyle = GradientStyle.Radial;
			SidePanel.Image = null;
			SidePanel.ImageOffset = new Point(15, 45);
			SidePanel.Location = new Point(8, 8);
			SidePanel.Name = "SidePanel";
			SidePanel.Size = new Size(170, 405);
			SidePanel.TabIndex = 1;
			_logo.BackColor = Color.Transparent;
			_logo.Location = new Point(20, 20);
			_logo.Name = "_logo";
			_logo.OffsetBackgroundImage = false;
			_logo.Size = new Size(130, 115);
			_logo.SizeMode = PictureBoxSizeMode.CenterImage;
			_logo.TabIndex = 3;
			_logo.TabStop = false;
			_logo.Visible = false;
			_icon.BackColor = Color.Transparent;
			_icon.Location = new Point(92, 30);
			_icon.Name = "_icon";
			_icon.Size = new Size(48, 48);
			_icon.SizeMode = PictureBoxSizeMode.CenterImage;
			_icon.TabIndex = 1;
			_icon.TabStop = false;
			_icon.Visible = false;
			_sideSplitter.BackColor2 = Color.FromArgb(233, 253, 255);
			_sideSplitter.Dock = DockStyle.Right;
			_sideSplitter.GradientAngle = 90f;
			_sideSplitter.GradientOffset = new Point(0, 25);
			_sideSplitter.GradientScale = new Point(100, 75);
			_sideSplitter.GradientStyle = GradientStyle.Reflected;
			_sideSplitter.Image = null;
			_sideSplitter.ImageOffset = new Point(0, 0);
			_sideSplitter.Location = new Point(169, 0);
			_sideSplitter.Name = "_sideSplitter";
			_sideSplitter.Size = new Size(1, 405);
			_sideSplitter.TabIndex = 0;
			BodyPanel.Dock = DockStyle.Fill;
			BodyPanel.Location = new Point(178, 8);
			BodyPanel.Name = "BodyPanel";
			BodyPanel.Size = new Size(498, 405);
			BodyPanel.TabIndex = 0;
			base.Controls.Add(BodyPanel);
			base.Controls.Add(SidePanel);
			base.Name = "SplitPanel";
			base.Size = new Size(684, 421);
			SidePanel.ResumeLayout(false);
			base.ResumeLayout(false);
		}

		protected override void OnHandleCreated(EventArgs e)
		{
			base.OnHandleCreated(e);
			if (_iconImage != _icon.Image)
			{
				Icon = _iconImage;
			}
			if (_logoImage != _logo.Image)
			{
				Logo = _logoImage;
			}
			_icon.Visible = (!_logo.Visible && _iconImage != null);
		}

		protected internal override void UpdateFromTheme()
		{
			base.UpdateFromTheme();
			SidePanel.BackColor = base._superForm.Theme.Colors["SidePanel", new string[2]
			{
				"Panel",
				"Base"
			}];
			SidePanel.BackColor2 = base._superForm.Theme.Colors["SideFade", new string[1]
			{
				"BaseHighHigh"
			}];
			_sideSplitter.BackColor2 = base._superForm.Theme.Colors["SideSplitter", new string[1]
			{
				"BaseHighlight"
			}];
			_sideSplitter.BackColor = base._superForm.Theme.Colors["SideSplitterFade", new string[3]
			{
				"SidePanel",
				"Panel",
				"Base"
			}];
			Bitmap bitmap = base._superForm.Theme.Images["Side"];
			if (bitmap == null)
			{
				_sideSplitter.Visible = true;
				SidePanel.BackgroundImage = null;
			}
			else
			{
				_sideSplitter.Visible = false;
				SidePanel.BackgroundImage = base.SuperForm.GetScaledImage(bitmap, SidePanel.BackgroundImage);
			}
			SidePanel.Image = base.SuperForm.GetScaledImage(base._superForm.Theme.Images["SideBrand"], SidePanel.Image);
			if (!_usedDefaultIcon)
			{
				if (_logo.Image != null)
				{
					return;
				}
				if (_icon.Image != null)
				{
					return;
				}
				if (_logoImage != null)
				{
					return;
				}
				if (_iconImage != null)
				{
					return;
				}
			}
			if (_defaultIconName != null)
			{
				string[] array = _defaultIconName.Split(',');
				Image image = base._superForm.Theme.Images[array[0], array];
				if (image != null)
				{
					if (image.Width <= 48 && image.Height <= 48)
					{
						Icon = image;
					}
					else
					{
						Logo = image;
					}
				}
				else
				{
					string[] array2 = array;
					foreach (string text in array2)
					{
						switch (text)
						{
						case "DefaultWarningIcon":
							Icon = Images.BigWarning_png;
							break;
						case "DefaultInfoIcon":
							Icon = Images.BigInfo_png;
							break;
						default:
							Icon = Images.BigKeys_png;
							break;
						}
						if (Icon != null)
						{
							break;
						}
					}
				}
			}
			else
			{
				Icon = Images.BigKeys_png;
			}
			_usedDefaultIcon = true;
		}
	}
}
