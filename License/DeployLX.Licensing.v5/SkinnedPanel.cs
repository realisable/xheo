using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public sealed class SkinnedPanel : BufferedPanel
	{
		private static readonly Bitmap _defaultSkin;

		private Bitmap _skin;

		[Category("Appearance")]
		public Bitmap Skin
		{
			get
			{
				return _skin ?? _defaultSkin;
			}
			set
			{
				if (_skin != value)
				{
					_skin = value;
					base.Invalidate();
				}
			}
		}

		public SkinnedPanel()
		{
			base.SetStyle(ControlStyles.ResizeRedraw | ControlStyles.SupportsTransparentBackColor, true);
		}

		static SkinnedPanel()
		{
			_defaultSkin = (Images.PanelBack_png as Bitmap);
		}

		private bool ShouldSerializeSkin()
		{
			if (_skin != null)
			{
				return _skin != _defaultSkin;
			}
			return false;
		}

		private void ResetSkin()
		{
			_skin = null;
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);
			Graphics graphics = e.Graphics;
			int num = 16;
			int num2 = 16;
			int num3 = 16;
			int num4 = 20;
			Bitmap skin = Skin;
			Rectangle clientRectangle = base.ClientRectangle;
			if (clientRectangle.Width >= 32 && clientRectangle.Height >= num3 + num4)
			{
				using (Image image = ImageEffects.Clone(skin, new Rectangle(0, 0, num2, num3), PixelFormat.Format32bppArgb))
				{
					graphics.DrawImage(image, 0, 0);
				}
				using (Image image2 = ImageEffects.Clone(skin, new Rectangle(skin.Width - num, 0, num, num3), PixelFormat.Format32bppArgb))
				{
					graphics.DrawImage(image2, clientRectangle.Right - num, 0);
				}
				using (Image image3 = ImageEffects.Clone(skin, new Rectangle(skin.Width - num, skin.Height - num4, num, num4), PixelFormat.Format32bppArgb))
				{
					graphics.DrawImage(image3, clientRectangle.Right - num, clientRectangle.Bottom - num4);
				}
				using (Image image4 = ImageEffects.Clone(skin, new Rectangle(0, skin.Height - num4, num2, num4), PixelFormat.Format32bppArgb))
				{
					graphics.DrawImage(image4, 0, clientRectangle.Bottom - num4);
				}
				graphics.DrawImage(skin, new RectangleF((float)num2, 0f, (float)(clientRectangle.Width - num2 - num), (float)num3), new RectangleF((float)num2, 0f, (float)(skin.Width - num2 - num - 1), (float)num3), GraphicsUnit.Pixel);
				graphics.DrawImage(skin, new RectangleF((float)num2, (float)(clientRectangle.Bottom - num4), (float)(clientRectangle.Width - num2 - num), (float)num4), new RectangleF((float)num2, (float)(skin.Height - num4), (float)(skin.Width - num2 - num - 1), (float)num4), GraphicsUnit.Pixel);
				graphics.DrawImage(skin, new RectangleF(0f, (float)num3, (float)num2, (float)(clientRectangle.Height - num3 - num4)), new RectangleF(0f, (float)num3, (float)num2, (float)(skin.Height - num3 - num4 - 1)), GraphicsUnit.Pixel);
				graphics.DrawImage(skin, new RectangleF((float)(clientRectangle.Right - num), (float)num3, (float)num, (float)(clientRectangle.Height - num3 - num4)), new RectangleF((float)(skin.Width - num), (float)num3, (float)num, (float)(skin.Height - num3 - num4 - 1)), GraphicsUnit.Pixel);
				graphics.DrawImage(skin, new RectangleF((float)num2, (float)num3, (float)(clientRectangle.Right - num - num2), (float)(clientRectangle.Height - num3 - num4)), new RectangleF((float)num2, (float)num3, (float)(skin.Width - num - num2 - 1), (float)(skin.Height - num3 - num4 - 1)), GraphicsUnit.Pixel);
			}
		}
	}
}
