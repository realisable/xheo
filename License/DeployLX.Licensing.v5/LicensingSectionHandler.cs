using Microsoft.Win32;
using System;
using System.Configuration;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	public sealed class LicensingSectionHandler : IConfigurationSectionHandler
	{
		public object Create(object parent, object configContext, XmlNode section)
		{
			LicenseConfigurationSettings licenseConfigurationSettings = (parent is LicenseConfigurationSettings) ? (parent as LicenseConfigurationSettings) : new LicenseConfigurationSettings();
			if (section == null)
			{
				return licenseConfigurationSettings;
			}
			foreach (XmlAttribute attribute in section.Attributes)
			{
				switch (attribute.LocalName)
				{
				case "sharedFolder":
					licenseConfigurationSettings.SharedFolder = attribute.Value;
					break;
				case "disableTrials":
					try
					{
						licenseConfigurationSettings.DisableTrials = XmlConvert.ToBoolean(attribute.Value.Trim());
					}
					catch (FormatException inner2)
					{
						throw new ConfigurationErrorsException(SR.GetString("E_InvalidConfigurationValue", attribute.Value, "disableTrials", "True, False"), inner2, section);
					}
					break;
				case "serverConnectTimeout":
					try
					{
						licenseConfigurationSettings.ServerConnectTimeout = XmlConvert.ToInt32(attribute.Value.Trim());
					}
					catch (FormatException inner)
					{
						throw new ConfigurationErrorsException(SR.GetString("E_InvalidConfigurationValue", attribute.Value, "serverConnectTimeout", "True, False"), inner, section);
					}
					break;
				default:
					throw new ConfigurationErrorsException(SR.GetString("E_UnknownConfigurationAttribute", attribute.LocalName, "licensing"), section);
				}
			}
			foreach (XmlNode childNode in section.ChildNodes)
			{
				if (childNode.NodeType == XmlNodeType.Element)
				{
					if (!(childNode.Name == "proxy"))
					{
						if (childNode.LocalName == "serialNumber")
						{
							string text = null;
							string text2 = null;
							string text3 = null;
							foreach (XmlAttribute attribute2 in childNode.Attributes)
							{
								switch (attribute2.LocalName)
								{
								case "registry":
									if (text2 == null)
									{
										text3 = attribute2.Value;
										break;
									}
									throw new ConfigurationErrorsException(SR.GetString("E_ConfigurationCannotUseAnd", attribute2.LocalName, "value", "serialNumber"), section);
								case "value":
									if (text3 == null)
									{
										text2 = attribute2.Value;
										break;
									}
									throw new ConfigurationErrorsException(SR.GetString("E_ConfigurationCannotUseAnd", attribute2.LocalName, "registry", "serialNumber"), section);
								case "application":
									text = attribute2.Value.Trim();
									break;
								default:
									throw new ConfigurationErrorsException(SR.GetString("E_UnknownConfigurationAttribute", attribute2.LocalName, "SerialNumber"), section);
								}
							}
							if (text == null && text3 == null)
							{
								throw new ConfigurationErrorsException(SR.GetString("E_MissingConfigurationAttribute", "application", "serialNumber"), childNode);
							}
							if (text != null && text2 == null && text3 == null)
							{
								throw new ConfigurationErrorsException(SR.GetString("E_MissingConfigurationAttribute", "value", "serialNumber"), childNode);
							}
							try
							{
								if (text3 != null)
								{
									using (new Wow64Guard())
									{
										using (RegistryKey registryKey = SafeToolbox.GetRegistryKey(text3, false))
										{
											if (registryKey != null)
											{
												string[] valueNames = registryKey.GetValueNames();
												foreach (string text4 in valueNames)
												{
													if (text == null || string.Compare(text4, 0, text, 0, text.Length, true) == 0)
													{
														string text5 = registryKey.GetValue(text4) as string;
														if (text5 != null)
														{
															int num = text4.IndexOf(':');
															if (num == -1)
															{
																licenseConfigurationSettings.SerialNumbers.Add(text4, text5);
															}
															else
															{
																licenseConfigurationSettings.SerialNumbers.Add(text4.Substring(0, num), text5);
															}
														}
													}
												}
											}
										}
									}
								}
								else if (text2.StartsWith("registry:"))
								{
									using (new Wow64Guard())
									{
										using (RegistryKey registryKey2 = SafeToolbox.GetRegistryKey(text2, false))
										{
											text2 = ((registryKey2 == null) ? null : (registryKey2.GetValue(null) as string));
										}
									}
									if (text2 != null)
									{
										licenseConfigurationSettings.SerialNumbers.Add(text, text2);
									}
								}
								else
								{
									licenseConfigurationSettings.SerialNumbers.Add(text, text2);
								}
							}
							catch (ConfigurationException)
							{
								throw;
							}
							catch (Exception inner3)
							{
								throw new ConfigurationErrorsException(SR.GetString("E_ConfigurationUnexpected"), inner3, section);
							}
							continue;
						}
						throw new ConfigurationErrorsException(SR.GetString("E_InvalidConfigurationTag", childNode.Name), childNode);
					}
					string text6 = null;
					string text7 = null;
					string password = null;
					string address = null;
					foreach (XmlAttribute attribute3 in childNode.Attributes)
					{
						switch (attribute3.LocalName)
						{
						case "address":
							if (attribute3.Value != null)
							{
								address = attribute3.Value.Trim();
							}
							break;
						case "domain":
							if (attribute3.Value != null)
							{
								text7 = attribute3.Value.Trim();
							}
							break;
						case "password":
							if (attribute3.Value != null)
							{
								password = attribute3.Value.Trim();
							}
							break;
						case "username":
							if (attribute3.Value != null)
							{
								text6 = attribute3.Value.Trim();
							}
							break;
						default:
							throw new ConfigurationErrorsException(SR.GetString("E_UnknownConfigurationAttribute", attribute3.LocalName, "proxy"), section);
						}
					}
					if (text7 == null && text6 != null)
					{
						Toolbox.SplitUserName(text6, out text6, out text7);
					}
					licenseConfigurationSettings.Proxy = Toolbox.MakeWebProxy(address, text6, password, text7);
				}
			}
			return licenseConfigurationSettings;
		}
	}
}
