using System;

namespace DeployLX.Licensing.v5
{
	public class ValidationHelpEventArgs : EventArgs
	{
		private SecureLicenseContext _context;

		private Limit _limit;

		private SuperFormPanel _panel;

		public SecureLicenseContext Context
		{
			get
			{
				return _context;
			}
			set
			{
				_context = value;
			}
		}

		public Limit Limit
		{
			get
			{
				return _limit;
			}
			set
			{
				_limit = value;
			}
		}

		public SuperFormPanel Panel
		{
			get
			{
				return _panel;
			}
			set
			{
				_panel = value;
			}
		}

		public ValidationHelpEventArgs(SuperFormPanel panel)
		{
			_panel = panel;
			if (panel != null)
			{
				_context = panel.SuperForm.Context;
				_limit = panel.Limit;
			}
		}

		public ValidationHelpEventArgs()
		{
		}
	}
}
