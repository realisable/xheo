using System;
using System.Drawing;
using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	internal class TextImagePainter
	{
		public class TextSettings
		{
			public string Value
			{
				get;
				set;
			}

			public Color Color
			{
				get;
				set;
			}

			public Font Font
			{
				get;
				set;
			}

			public Point Offset
			{
				get;
				set;
			}

			public ContentAlignment Alignment
			{
				get;
				set;
			}

			public TextFormatFlags Format
			{
				get;
				set;
			}
		}

		public class ImageSettings
		{
			public Image Value
			{
				get;
				set;
			}

			public ContentAlignment Alignment
			{
				get;
				set;
			}

			public int Padding
			{
				get;
				set;
			}

			public Point Offset
			{
				get;
				set;
			}
		}

		private class Layout
		{
			public Rectangle TextBounds;

			public Rectangle ImageBounds;
		}

		public TextSettings Txt
		{
			get;
			private set;
		}

		public ImageSettings Img
		{
			get;
			private set;
		}

		public TextImageRelation TextImageRelation
		{
			get;
			set;
		}

		public Graphics Graphics
		{
			get;
			private set;
		}

		public DropShadow Shadow
		{
			get;
			set;
		}

		public Rectangle Bounds
		{
			get;
			set;
		}

		public bool Disabled
		{
			get;
			set;
		}

		public TextImagePainter()
		{
			Txt = new TextSettings();
			Img = new ImageSettings();
		}

		public void Draw(Graphics g)
		{
			if (g == null)
			{
				throw new ArgumentNullException("g");
			}
			Graphics = g;
			if ((Txt.Value == null || Txt.Value.Length == 0) && Img.Value == null)
			{
				return;
			}
			if (Txt.Font == null)
			{
				throw new ArgumentNullException("font", "Must provide font");
			}
			if (Bounds == Rectangle.Empty)
			{
				throw new ArgumentNullException("bounds", "Must provide bounds");
			}
			if (Txt.Format == TextFormatFlags.Default)
			{
				Txt.Format = ImageEffects.BuildTextFormatFlags(Txt.Alignment, false);
			}
			Layout layout = LayoutImageAndText();
			Image image = Img.Value;
			try
			{
				if (Disabled && image != null)
				{
					image = ImageEffects.MakeTransparent(image, 80);
				}
				ImageEffects.DrawImageWithShadow(g, image, Shadow, layout.ImageBounds);
				Color color = Txt.Color;
				if (Disabled)
				{
					color = Color.FromArgb(128, color);
				}
				ImageEffects.DrawStringWithShadow(g, Txt.Value, Txt.Font, color, layout.TextBounds, Shadow, Txt.Format);
			}
			finally
			{
				if (image != Img.Value)
				{
					image.Dispose();
				}
			}
		}

		private Layout LayoutImageAndText()
		{
			Layout layout = new Layout();
			layout.ImageBounds = Bounds;
			layout.TextBounds = Bounds;
			Layout layout2 = layout;
			if (Img.Value != null && !(Img.Value.Size == Size.Empty) && Txt.Value != null && Txt.Value.Length != 0 && TextImageRelation != 0)
			{
				Rectangle rectangle = CalculateAvailableTextBounds();
				Size size = TextRenderer.MeasureText(Txt.Value, Txt.Font, rectangle.Size, Txt.Format);
				layout2.TextBounds = new Rectangle(0, 0, size.Width, size.Height);
				layout2.ImageBounds = new Rectangle(0, 0, Img.Value.Width, Img.Value.Height);
				MakeFinalBoundsAdjustments(layout2);
				ApplyOffsets(layout2);
				return layout2;
			}
			if (Img.Value != null)
			{
				layout2.ImageBounds = AlignRectangle(Img.Value.Size, Bounds, Txt.Format);
			}
			if (Txt.Value != null)
			{
				layout2.TextBounds = AlignRectangle(TextRenderer.MeasureText(Txt.Value, Txt.Font, Bounds.Size, Txt.Format), Bounds, Txt.Format);
			}
			return layout2;
		}

		private void ApplyOffsets(Layout layout)
		{
			layout.ImageBounds.Offset(Img.Offset);
			layout.TextBounds.Offset(Txt.Offset);
		}

		private void MakeFinalBoundsAdjustments(Layout layout)
		{
			if (!(layout.ImageBounds == Rectangle.Empty))
			{
				Rectangle rectangle = CalculateTargetLayoutBounds(layout);
				switch (TextImageRelation)
				{
				case TextImageRelation.TextBeforeImage:
					layout.ImageBounds = AlignRectangle(layout.ImageBounds.Size, new Rectangle(rectangle.X + layout.TextBounds.Width + Img.Padding, rectangle.Y, layout.ImageBounds.Width, rectangle.Height), Txt.Format);
					layout.TextBounds = AlignRectangle(layout.TextBounds.Size, new Rectangle(rectangle.X, rectangle.Y, layout.TextBounds.Width, rectangle.Height), Txt.Format);
					break;
				case TextImageRelation.ImageAboveText:
					layout.ImageBounds = AlignRectangle(layout.ImageBounds.Size, new Rectangle(rectangle.X, rectangle.Y, rectangle.Width, layout.ImageBounds.Height), Txt.Format);
					layout.TextBounds = AlignRectangle(layout.TextBounds.Size, new Rectangle(rectangle.X, rectangle.Y + layout.ImageBounds.Height + Img.Padding, rectangle.Width, layout.TextBounds.Height), Txt.Format);
					break;
				case TextImageRelation.TextAboveImage:
					layout.ImageBounds = AlignRectangle(layout.ImageBounds.Size, new Rectangle(rectangle.X, rectangle.Y + layout.TextBounds.Height + Img.Padding, rectangle.Width, layout.ImageBounds.Height), Txt.Format);
					layout.TextBounds = AlignRectangle(layout.TextBounds.Size, new Rectangle(rectangle.X, rectangle.Y, rectangle.Width, layout.TextBounds.Height), Txt.Format);
					break;
				case TextImageRelation.ImageBeforeText:
					layout.ImageBounds = AlignRectangle(layout.ImageBounds.Size, new Rectangle(rectangle.X, rectangle.Y, layout.ImageBounds.Width, rectangle.Height), Txt.Format);
					layout.TextBounds = AlignRectangle(layout.TextBounds.Size, new Rectangle(rectangle.X + layout.ImageBounds.Width + Img.Padding, rectangle.Y, layout.TextBounds.Width, rectangle.Height), Txt.Format);
					break;
				}
			}
		}

		private Rectangle CalculateTargetLayoutBounds(Layout layout)
		{
			Size size = Size.Empty;
			switch (TextImageRelation)
			{
			case TextImageRelation.Overlay:
				size = new Size(Math.Max(layout.ImageBounds.Width, layout.TextBounds.Width), Math.Max(layout.ImageBounds.Height, layout.TextBounds.Height));
				break;
			case TextImageRelation.ImageAboveText:
			case TextImageRelation.TextAboveImage:
				size = new Size(Math.Max(layout.ImageBounds.Width, layout.TextBounds.Width), layout.ImageBounds.Height + layout.TextBounds.Height + Img.Padding);
				break;
			case TextImageRelation.ImageBeforeText:
			case TextImageRelation.TextBeforeImage:
				size = new Size(layout.ImageBounds.Width + Img.Padding + layout.TextBounds.Width, Math.Max(layout.ImageBounds.Height, layout.TextBounds.Height));
				break;
			}
			return AlignRectangle(size, Bounds, Txt.Format);
		}

		private Rectangle CalculateAvailableTextBounds()
		{
			Rectangle bounds = Bounds;
			if (Img.Value != null)
			{
				switch (TextImageRelation)
				{
				case TextImageRelation.ImageAboveText:
				case TextImageRelation.TextAboveImage:
					bounds.Height -= Img.Value.Height + Img.Padding;
					break;
				case TextImageRelation.ImageBeforeText:
				case TextImageRelation.TextBeforeImage:
					bounds.Width -= Img.Value.Width + Img.Padding;
					break;
				}
			}
			return bounds;
		}

		private static Rectangle AlignRectangle(Size size, Rectangle bounds, TextFormatFlags format)
		{
			Rectangle result = new Rectangle(bounds.X, bounds.Y, size.Width, size.Height);
			if ((format & TextFormatFlags.HorizontalCenter) == TextFormatFlags.HorizontalCenter)
			{
				result.X += (bounds.Width - result.Width) / 2;
			}
			if ((format & TextFormatFlags.Right) == TextFormatFlags.Right)
			{
				result.X += bounds.Width - result.Width;
			}
			if ((format & TextFormatFlags.VerticalCenter) == TextFormatFlags.VerticalCenter)
			{
				result.Y += (bounds.Height - result.Height) / 2;
			}
			if ((format & TextFormatFlags.Bottom) == TextFormatFlags.Bottom)
			{
				result.Y += bounds.Height - result.Height;
			}
			return result;
		}
	}
}
