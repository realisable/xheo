using System;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	[LimitEditor("DesignOrRuntimeLimitEditor", IconResource = "asmres://DeployLX.Licensing.DlxAddIn.v5/DeployLX.Licensing.DlxAddIn.v5.Resources.Limits.Designtime.png")]
	public class DesigntimeLimit : DesigntimeOrRuntimeLimit
	{
		public override string Description
		{
			get
			{
				return SR.GetString("M_DesigntimeLimitDescription");
			}
		}

		public override string Name
		{
			get
			{
				return "Designtime";
			}
		}

		public override ValidationResult Validate(SecureLicenseContext context)
		{
			if (base.IsHostedInDesigner(context))
			{
				return base.Validate(context);
			}
			return context.ReportError("E_DesigntimeOnly", this, null, ErrorSeverity.Low);
		}

		public override PeekResult Peek(SecureLicenseContext context)
		{
			if (!base.IsHostedInDesigner(context))
			{
				return PeekResult.Invalid;
			}
			return base.Peek(context);
		}
	}
}
