namespace DeployLX.Licensing.v5
{
	public delegate void LicenseValidationLogger(SecureLicenseContext context, string diagnosticReport);
}
