using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public class BufferedPictureBox : PictureBox
	{
		private bool _offsetBackgroundImage;

		[Category("Appearence")]
		public bool OffsetBackgroundImage
		{
			get
			{
				return _offsetBackgroundImage;
			}
			set
			{
				_offsetBackgroundImage = value;
				base.Invalidate();
			}
		}

		public BufferedPictureBox()
		{
			base.SetStyle(ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer, true);
			BackgroundImageLayout = ImageLayout.None;
		}

		protected override void OnPaintBackground(PaintEventArgs e)
		{
			if (BackgroundImage != null && _offsetBackgroundImage)
			{
				int num = base.Left;
				int num2 = base.Top;
				Control parent = base.Parent;
				while (parent != null && !(parent is Form))
				{
					num += parent.Left;
					num2 += parent.Top;
					parent = parent.Parent;
				}
				e.Graphics.DrawImage(BackgroundImage, 0, 0, new Rectangle(num, num2, base.Width, base.Height), GraphicsUnit.Pixel);
			}
			else
			{
				base.OnPaintBackground(e);
			}
		}
	}
}
