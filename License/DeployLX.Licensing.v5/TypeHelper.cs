using System;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using System.Reflection.Emit;
using System.Text.RegularExpressions;

namespace DeployLX.Licensing.v5
{
	public sealed class TypeHelper
	{
		private TypeHelper()
		{
		}

		public static string GetNonVersionedAssemblyName(Assembly asm)
		{
			return GetNonVersionedAssemblyName(asm.FullName);
		}

		public static string GetNonVersionedAssemblyName(string assemblyName)
		{
			int num = assemblyName.IndexOf("Version=");
			if (num == -1)
			{
				return assemblyName;
			}
			num = assemblyName.LastIndexOf(',', num);
			if (num == -1)
			{
				return null;
			}
			int num2 = assemblyName.IndexOf(',', num + 1);
			if (num2 == -1)
			{
				return assemblyName.Substring(0, num);
			}
			return assemblyName.Substring(0, num) + assemblyName.Substring(num2);
		}

		public static string GetNonVersionedAssemblyQualifiedName(Type type)
		{
			return type.FullName + ", " + GetNonVersionedAssemblyName(type.Assembly);
		}

		public static Type FindType(string typeName, bool throwOnError, string defaultNamespace)
		{
			Type type = null;
			try
			{
				type = Type.GetType(typeName, false, true);
			}
			catch (Exception)
			{
			}
			if (type == null)
			{
				if (defaultNamespace != null && defaultNamespace[defaultNamespace.Length - 1] != '.')
				{
					defaultNamespace += '.';
				}
				int num = typeName.IndexOf(',');
				if (num > -1)
				{
					string text = typeName.Substring(0, num);
					string text2 = typeName.Substring(num + 1).Trim();
					Assembly assembly = null;
					try
					{
						assembly = Assembly.Load(text2);
					}
					catch
					{
					}
					if (assembly == null)
					{
						try
						{
							assembly = Assembly.Load(text2);
						}
						catch
						{
						}
					}
					if (assembly == null)
					{
						try
						{
							text2 = Regex.Replace(text2, ",\\s*Version\\s?=\\s?[0-9\\.]*\\s*", ",");
							assembly = Assembly.Load(text2);
						}
						catch
						{
						}
					}
					if (assembly == null && text2.IndexOf(',') < 0)
					{
						Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
						int num2 = 0;
						while (num2 < assemblies.Length)
						{
							Assembly assembly2 = assemblies[num2];
							if (string.Compare(assembly2.GetName().Name, text2, true) != 0)
							{
								num2++;
								continue;
							}
							assembly = assembly2;
							break;
						}
					}
					if (assembly != null)
					{
						type = assembly.GetType(text, throwOnError, true);
						if (type == null && defaultNamespace != null)
						{
							type = assembly.GetType(defaultNamespace + text, throwOnError, true);
						}
					}
				}
				else
				{
					Assembly[] assemblies2 = AppDomain.CurrentDomain.GetAssemblies();
					foreach (Assembly assembly3 in assemblies2)
					{
						type = assembly3.GetType(typeName, false, true);
						if (type != null)
						{
							break;
						}
						if (defaultNamespace != null)
						{
							type = assembly3.GetType(defaultNamespace + typeName, false, true);
						}
						if (type != null)
						{
							break;
						}
					}
				}
			}
			if (type == null && throwOnError)
			{
				type = Type.GetType(typeName, true, true);
			}
			return type;
		}

		public static Type FindType(string typeName, bool throwOnError)
		{
			return FindType(typeName, throwOnError, null);
		}

		public static ParsedVersion GetAssemblyVersion(Assembly assembly)
		{
			Check.NotNull(assembly, "assembly");
			AssemblyFileVersionAttribute assemblyFileVersionAttribute = Attribute.GetCustomAttribute(assembly, typeof(AssemblyFileVersionAttribute), true) as AssemblyFileVersionAttribute;
			if (assemblyFileVersionAttribute == null)
			{
				try
				{
					return GetFileVersionInfo(assembly);
				}
				catch
				{
				}
				return new ParsedVersion(new Version());
			}
			return new ParsedVersion(assemblyFileVersionAttribute.Version);
		}

		private static ParsedVersion GetFileVersionInfo(Assembly assembly)
		{
			FileVersionInfo fileVersionInfo = null;
			if (!(assembly is AssemblyBuilder) && assembly.GetType().Name != "InternalAssemblyBuilder" && string.Compare("file", 0, assembly.CodeBase, 0, 4, true, CultureInfo.InvariantCulture) == 0)
			{
				fileVersionInfo = FileVersionInfo.GetVersionInfo(new Uri(assembly.CodeBase).LocalPath);
				if (fileVersionInfo != null)
				{
					return new ParsedVersion(fileVersionInfo.FileVersion);
				}
			}
			return new ParsedVersion(assembly.GetName().Version.ToString());
		}
	}
}
