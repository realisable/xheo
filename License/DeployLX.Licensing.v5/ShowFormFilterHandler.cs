namespace DeployLX.Licensing.v5
{
	public delegate bool ShowFormFilterHandler(ISuperFormLimit limit, SecureLicenseContext context, bool defaultResult);
}
