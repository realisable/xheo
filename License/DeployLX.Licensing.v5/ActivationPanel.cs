using System;
using System.Drawing;
using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	public class ActivationPanel : SplitPanel
	{
		private BufferedPanel _statusPanel;

		private Button _extend;

		private ShadowLabel _activateTitle;

		private SkinnedButton _activateManually;

		private SkinnedButton _activateOnline;

		private Button _activateLater;

		private ShadowLabel _notice;

		private ShadowLabel _newSerial;

		private ShadowLabel _moreAboutActivationLink;

		private SkinnedPanel _registrationPanel;

		private Label _nicknameLabel;

		private TextBox _nicknameText;

		private ShadowLabel _requiredSummary;

		private Timer _continueTimer;

		private int _continueDelay;

		public ActivationPanel(ActivationLimit limit)
			: base(limit)
		{
			InitializeComponent();
		}

		protected override void Dispose(bool disposing)
		{
			if (_continueTimer != null)
			{
				_continueTimer.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			_statusPanel = new BufferedPanel();
			_extend = new Button();
			_activateTitle = new ShadowLabel();
			_activateOnline = new SkinnedButton();
			_activateManually = new SkinnedButton();
			_notice = new ShadowLabel();
			_requiredSummary = new ShadowLabel();
			_newSerial = new ShadowLabel();
			_moreAboutActivationLink = new ShadowLabel();
			_registrationPanel = new SkinnedPanel();
			_nicknameLabel = new Label();
			_nicknameText = new TextBox();
			base.SidePanel.SuspendLayout();
			base.BodyPanel.SuspendLayout();
			_registrationPanel.SuspendLayout();
			base.SuspendLayout();
			base.SidePanel.Controls.Add(_statusPanel);
			base.SidePanel.Controls.Add(_extend);
			base.SidePanel.Location = new Point(0, 0);
			base.SidePanel.Size = new Size(170, 421);
			base.SidePanel.Controls.SetChildIndex(_extend, 0);
			base.SidePanel.Controls.SetChildIndex(_statusPanel, 0);
			base.BodyPanel.Controls.Add(_registrationPanel);
			base.BodyPanel.Controls.Add(_moreAboutActivationLink);
			base.BodyPanel.Controls.Add(_newSerial);
			base.BodyPanel.Controls.Add(_requiredSummary);
			base.BodyPanel.Controls.Add(_notice);
			base.BodyPanel.Controls.Add(_activateManually);
			base.BodyPanel.Controls.Add(_activateOnline);
			base.BodyPanel.Controls.Add(_activateTitle);
			base.BodyPanel.Location = new Point(170, 0);
			base.BodyPanel.Size = new Size(514, 421);
			_statusPanel.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left);
			_statusPanel.BackColor = Color.Transparent;
			_statusPanel.BackgroundImageLayout = ImageLayout.None;
			_statusPanel.Location = new Point(20, 141);
			_statusPanel.Name = "_statusPanel";
			_statusPanel.Size = new Size(130, 217);
			_statusPanel.TabIndex = 4;
			_extend.Anchor = (AnchorStyles.Bottom | AnchorStyles.Left);
			_extend.FlatStyle = FlatStyle.System;
			_extend.Location = new Point(20, 364);
			_extend.Name = "_extend";
			_extend.Size = new Size(130, 23);
			_extend.TabIndex = 0;
			_extend.Text = "#UI_EnterExtensionCode";
			_extend.UseVisualStyleBackColor = false;
			_extend.Click += _extend_Click;
			_activateTitle.AutoEllipsis = true;
			_activateTitle.Location = new Point(14, 16);
			_activateTitle.Name = "_activateTitle";
			_activateTitle.Size = new Size(482, 34);
			_activateTitle.TabIndex = 0;
			_activateTitle.Text = "UI_ActivateTitle";
			_activateTitle.ThemeColor = new string[2]
			{
				"PanelTitleText",
				"Text"
			};
			_activateTitle.ThemeFont = ThemeFont.Title;
			_activateOnline.Anchor = (AnchorStyles.Bottom | AnchorStyles.Left);
			_activateOnline.Cursor = Cursors.Hand;
			_activateOnline.Location = new Point(16, 204);
			_activateOnline.Name = "_activateOnline";
			_activateOnline.ShouldDrawFocus = false;
			_activateOnline.Size = new Size(482, 36);
			_activateOnline.TabIndex = 0;
			_activateOnline.Text = "#UI_ActivateOnline";
			_activateOnline.TextAlign = ContentAlignment.MiddleLeft;
			_activateOnline.TextImageRelation = TextImageRelation.ImageBeforeText;
			_activateOnline.UseVisualStyleBackColor = false;
			_activateOnline.Click += _activateOnline_Click;
			_activateManually.Anchor = (AnchorStyles.Bottom | AnchorStyles.Left);
			_activateManually.Cursor = Cursors.Hand;
			_activateManually.Location = new Point(16, 256);
			_activateManually.Name = "_activateManually";
			_activateManually.ShouldDrawFocus = false;
			_activateManually.Size = new Size(482, 36);
			_activateManually.TabIndex = 1;
			_activateManually.Text = "#UI_ActivateManually";
			_activateManually.TextAlign = ContentAlignment.MiddleLeft;
			_activateManually.TextImageRelation = TextImageRelation.ImageBeforeText;
			_activateManually.UseVisualStyleBackColor = false;
			_activateManually.Click += _activateManually_Click;
			_notice.Location = new Point(16, 50);
			_notice.Name = "_notice";
			_notice.Size = new Size(482, 54);
			_notice.TabIndex = 0;
			_notice.ThemeFont = ThemeFont.Medium;
			_requiredSummary.Location = new Point(13, 367);
			_requiredSummary.Name = "_requiredSummary";
			_requiredSummary.Size = new Size(482, 17);
			_requiredSummary.TabIndex = 15;
			_requiredSummary.Text = "#UI_AboutActivationRequiredSimpleSummary";
			_requiredSummary.TextAlign = ContentAlignment.MiddleLeft;
			_newSerial.Location = new Point(21, 309);
			_newSerial.Name = "_newSerial";
			_newSerial.Size = new Size(261, 16);
			_newSerial.TabIndex = 17;
			_newSerial.Text = "#UI_EnterNewSerialActivation";
			_newSerial.Url = "#";
			_newSerial.Click += _newSerial_Click;
			_moreAboutActivationLink.Location = new Point(13, 392);
			_moreAboutActivationLink.Name = "_moreAboutActivationLink";
			_moreAboutActivationLink.Size = new Size(482, 17);
			_moreAboutActivationLink.TabIndex = 18;
			_moreAboutActivationLink.Text = "#UI_TellMeMoreActivation";
			_moreAboutActivationLink.TextAlign = ContentAlignment.MiddleLeft;
			_moreAboutActivationLink.Url = "#";
			_moreAboutActivationLink.Click += _tellMeMore_LinkClicked;
			_registrationPanel.Anchor = (AnchorStyles.Bottom | AnchorStyles.Left);
			_registrationPanel.BackColor = Color.Transparent;
			_registrationPanel.BackgroundImageLayout = ImageLayout.None;
			_registrationPanel.Controls.Add(_nicknameLabel);
			_registrationPanel.Controls.Add(_nicknameText);
			_registrationPanel.Location = new Point(13, 106);
			_registrationPanel.Name = "_registrationPanel";
			_registrationPanel.PaintFakeBackground = true;
			_registrationPanel.Size = new Size(488, 87);
			_registrationPanel.TabIndex = 19;
			_nicknameLabel.AutoSize = true;
			_nicknameLabel.Location = new Point(15, 15);
			_nicknameLabel.Name = "_nicknameLabel";
			_nicknameLabel.Size = new Size(164, 15);
			_nicknameLabel.TabIndex = 2;
			_nicknameLabel.Text = "#UI_ActivationNicknameNote";
			_nicknameText.Location = new Point(15, 37);
			_nicknameText.Name = "_nicknameText";
			_nicknameText.Size = new Size(458, 23);
			_nicknameText.TabIndex = 1;
			base.AutoScaleDimensions = new SizeF(96f, 96f);
			base.Name = "ActivationPanel";
			base.SidePanel.ResumeLayout(false);
			base.BodyPanel.ResumeLayout(false);
			_registrationPanel.ResumeLayout(false);
			_registrationPanel.PerformLayout();
			base.ResumeLayout(false);
		}

		protected internal override void LoadPanel(SecureLicenseContext context)
		{
			base.LoadPanel(context);
			ActivationLimit activationLimit = base.Limit as ActivationLimit;
			if (activationLimit.LogoResource != null)
			{
				base.Logo = Toolbox.GetImage(activationLimit.LogoResource, context);
			}
			_notice.Text = SR.GetString("UI_ActivationNotice", base.SuperForm.Context.SupportInfo.Product);
			_activateTitle.Text = SR.GetString("UI_ActivateTitle", base.SuperForm.Context.SupportInfo.Product);
			_nicknameText.Font = base.SuperForm.FieldFont;
			try
			{
				ActivationProfileCollection profilesForThisMachine = activationLimit.GetProfilesForThisMachine();
				if (profilesForThisMachine.Count > 0 && profilesForThisMachine[0].AssignedName != null)
				{
					_nicknameText.Text = profilesForThisMachine[0].AssignedName;
				}
				else
				{
					_nicknameText.Text = Environment.MachineName;
				}
			}
			catch (InvalidOperationException)
			{
			}
			_activateOnline.Image = Images.Go_png;
			_activateManually.Image = Images.Go_png;
			_requiredSummary.Image = Images.Notice_png;
			_moreAboutActivationLink.Image = Images.Notice_png;
			_newSerial.Visible = activationLimit.License.CanUnlockBySerial;
			if (activationLimit.Servers.Count == 0)
			{
				_activateOnline.Enabled = false;
				_activateOnline.Visible = false;
				_activateManually.Top -= 52;
			}
			if (activationLimit.CanActivateByCode && !activationLimit.HideManualButton)
			{
				return;
			}
			_activateManually.Enabled = false;
			_activateManually.Visible = false;
		}

		protected internal override void InitializePanel()
		{
			ActivationLimit activationLimit = base.Limit as ActivationLimit;
			Button button = base.AddBottomButton("#UI_Cancel");
			button.Click += _cancel_Click;
			base._superForm.CancelButton = button;
			_activateLater = base.AddBottomButton("#UI_ActivateLater");
			_activateLater.Click += _activateLater_Click;
			if (!activationLimit.CanDelayActivation(base._superForm.Context))
			{
				_activateLater.Visible = false;
				_activateLater.Enabled = false;
			}
			else if (activationLimit.ContinueDelay > 0)
			{
				if (_continueTimer == null)
				{
					_continueTimer = new Timer();
					_continueTimer.Tick += _continueTimer_Tick;
				}
				_continueTimer.Interval = 1000;
				_continueDelay = activationLimit.ContinueDelay;
				_continueTimer.Start();
				_activateLater.Enabled = false;
				base.CanClose = false;
			}
			bool flag = false;
			int top = 0;
			_statusPanel.Controls.Clear();
			Limit[] array = ((ActivationLimit)base.Limit).GetStateLimits("Unactivated").FindExtendableLimits();
			if (array != null)
			{
				Limit[] array2 = array;
				for (int i = 0; i < array2.Length; i++)
				{
					IExtendableLimit extendableLimit = (IExtendableLimit)array2[i];
					int num;
					int num2;
					int num3;
					extendableLimit.GetUseRange(base._superForm.Context, out num, out num2, out num3);
					string useDescription = extendableLimit.GetUseDescription(base._superForm.Context);
					if (useDescription != null)
					{
						ShadowLabel shadowLabel = new ShadowLabel();
						shadowLabel.Width = _statusPanel.Width;
						shadowLabel.Top = top;
						if (array.Length < 4)
						{
							shadowLabel.Height = 43;
							shadowLabel.ThemeFont = ThemeFont.Medium;
						}
						else
						{
							shadowLabel.Height = 30;
						}
						shadowLabel.TextAlign = ContentAlignment.BottomLeft;
						shadowLabel.Text = useDescription;
						shadowLabel.BackColor = Color.Transparent;
						_statusPanel.Controls.Add(shadowLabel);
						top = shadowLabel.Bottom + 4;
						shadowLabel.BringToFront();
					}
					if (num3 >= 0 && num2 > 0)
					{
						ProgressBar progressBar = new ProgressBar();
						progressBar.Width = _statusPanel.Width;
						progressBar.Top = top;
						if (array.Length < 4)
						{
							progressBar.Height = 24;
						}
						else
						{
							progressBar.Height = 16;
						}
						progressBar.Minimum = 0;
						progressBar.Maximum = num2 - num;
						progressBar.Value = Math.Max(num2 - num3, num);
						progressBar.Step = 1;
						progressBar.Style = ProgressBarStyle.Continuous;
						_statusPanel.Controls.Add(progressBar);
						top = progressBar.Bottom + 4;
						progressBar.BringToFront();
					}
					flag |= extendableLimit.CanExtend;
				}
			}
			_extend.Visible = (flag && !activationLimit.HideExtendButton);
			base.AddLicenseAgreementLink();
		}

		protected internal override void PanelShown(bool safeToChange)
		{
			base.PanelShown(safeToChange);
			base.SelectNextControl(_notice, true, true, true, true);
			if (((ActivationLimit)base.Limit).HasActivated && safeToChange && base._firstInitialization)
			{
				base.ShowMessageBox("UI_ReactivationNotice", null, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
			}
		}

		protected virtual void HandleActivateLater()
		{
			if (((ActivationLimit)base.Limit).CanDelayActivation(base._superForm.Context))
			{
				base.Return(FormResult.Success);
			}
		}

		protected virtual void HandleActivateOnline()
		{
			base.SuperForm.Context.Items["MachineName"] = _nicknameText.Text;
			base.ShowPage("ACTIVATEONLINE", HandleOnlinePanelResult, false);
		}

		private void HandleOnlinePanelResult(FormResult result)
		{
			switch (result)
			{
			case FormResult.Success:
				base.Return(FormResult.Success);
				break;
			case FormResult.Failure:
				base.Return(FormResult.Failure);
				break;
			}
		}

		protected virtual void HandleActivateManually()
		{
			base.SuperForm.Context.Items["MachineName"] = _nicknameText.Text;
			base.ShowPage("ACTIVATEMANUALLY", HandleManualPanelResult, false);
		}

		private void HandleManualPanelResult(FormResult result)
		{
			switch (result)
			{
			case FormResult.Success:
				base.Return(FormResult.Success);
				break;
			case FormResult.Failure:
				base.Return(FormResult.Failure);
				break;
			}
		}

		protected virtual void HandleExtend()
		{
			Limit[] array = ((ActivationLimit)base.Limit).GetStateLimits("Unactivated").FindExtendableLimits(true);
			if (array != null && array.Length != 0)
			{
				base.ShowPage("EXTEND", HandleExtendResult);
			}
		}

		private void HandleExtendResult(FormResult result)
		{
			switch (result)
			{
			case FormResult.Success:
				_activateLater.Enabled = ((ActivationLimit)base.Limit).CanDelayActivation(base._superForm.Context);
				break;
			case FormResult.Failure:
				base.Return(FormResult.Failure);
				break;
			}
		}

		protected virtual void HandleTellMeMore()
		{
			base.ShowPage("ACTIVATEOINFO", null);
		}

		private void _continueTimer_Tick(object sender, EventArgs e)
		{
			_continueDelay--;
			if (_continueDelay > 0)
			{
				_activateLater.Text = string.Format("{0} ({1})", SR.GetString("UI_ActivateLater"), _continueDelay);
			}
			else
			{
				_continueTimer.Stop();
				_activateLater.Enabled = ((ActivationLimit)base.Limit).CanDelayActivation(base._superForm.Context);
				_activateLater.Text = SR.GetString("UI_ActivateLater");
				base.CanClose = true;
			}
		}

		private void _activateLater_Click(object sender, EventArgs e)
		{
			HandleActivateLater();
		}

		private void _cancel_Click(object sender, EventArgs e)
		{
			base.Return(FormResult.Failure);
		}

		private void _extend_Click(object sender, EventArgs e)
		{
			HandleExtend();
		}

		private void _tellMeMore_LinkClicked(object sender, EventArgs e)
		{
			HandleTellMeMore();
		}

		private void _activateOnline_Click(object sender, EventArgs e)
		{
			HandleActivateOnline();
		}

		private void _activateManually_Click(object sender, EventArgs e)
		{
			HandleActivateManually();
		}

		protected internal override void UpdateFromTheme()
		{
			base.UpdateFromTheme();
			_activateOnline.Font = base.SuperForm.MediumFont;
			_activateManually.Font = base.SuperForm.MediumFont;
		}

		protected internal override void HandleKeyDown(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
			case Keys.F6:
				e.Handled = true;
				_extend_Click(_extend, EventArgs.Empty);
				break;
			case Keys.F7:
				if (((ActivationLimit)base.Limit).CanActivateByCode)
				{
					_activateManually_Click(_activateManually, EventArgs.Empty);
				}
				break;
			}
			base.HandleKeyDown(sender, e);
		}

		private void _newSerial_Click(object sender, EventArgs e)
		{
			HandleNewSerial();
		}
	}
}
