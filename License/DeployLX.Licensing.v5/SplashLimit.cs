using System;
using System.Collections.Specialized;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	[LimitEditor("SplashLimitEditor", Category = "Super Form Limits", IconResource = "asmres://DeployLX.Licensing.DlxAddIn.v5/DeployLX.Licensing.DlxAddIn.v5.Resources.Limits.Splash.png")]
	public class SplashLimit : SuperFormLimit
	{
		private bool _showOnce = true;

		private bool _showFirstTimeOnly;

		private string _splashResource;

		private int _continueDelay;

		public bool ShowOnce
		{
			get
			{
				return _showOnce;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_showOnce != value)
				{
					_showOnce = value;
					base.OnChanged("ShowOnce");
				}
			}
		}

		public bool ShowFirstTimeOnly
		{
			get
			{
				return _showFirstTimeOnly;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_showFirstTimeOnly != value)
				{
					_showFirstTimeOnly = value;
					base.OnChanged("ShowFirstTimeOnly");
				}
			}
		}

		public string SplashResource
		{
			get
			{
				return _splashResource;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_splashResource != value)
				{
					SharedToolbox.ValidateUrl(value, true);
					_splashResource = value;
					base.OnChanged("SplashResource");
				}
			}
		}

		public int ContinueDelay
		{
			get
			{
				return _continueDelay;
			}
			set
			{
				base.AssertNotReadOnly();
				Check.GreaterThanEqual(value, 0, "ContinueDelay");
				if (_continueDelay != value)
				{
					_continueDelay = value;
					base.OnChanged("ContinueDelay");
				}
			}
		}

		public override string Description
		{
			get
			{
				return SR.GetString("M_SplashLimitDescription");
			}
		}

		public override string Name
		{
			get
			{
				return "Splash";
			}
		}

		public override bool IsGui
		{
			get
			{
				return true;
			}
		}

		public override ValidationResult Validate(SecureLicenseContext context)
		{
			if (base.ResolveShouldShowForm(context) && context.CanShowWindowsForm)
			{
				switch (ShowForm(context, "DEFAULT"))
				{
				case FormResult.Retry:
					return ValidationResult.Retry;
				default:
					return ValidationResult.Invalid;
				case FormResult.Success:
					break;
				}
				if (_showFirstTimeOnly)
				{
					base.SetPersistentData("SHOWN", true, PersistentDataLocationType.SharedOrUser);
				}
			}
			return base.Validate(context);
		}

		protected override bool ResolveDefaultShouldShowForm(SecureLicenseContext context)
		{
			if (base.FormShown && (_showOnce || _showFirstTimeOnly))
			{
				return false;
			}
			if (_showFirstTimeOnly && (bool)base.GetPersistentData("SHOWN", false, PersistentDataLocationType.SharedOrUser))
			{
				return false;
			}
			return true;
		}

		public override PeekResult Peek(SecureLicenseContext context)
		{
			if (base.ResolveShouldShowForm(context))
			{
				return PeekResult.NeedsUser;
			}
			return base.Peek(context);
		}

		protected override bool ReadFromXml(XmlReader reader)
		{
			_splashResource = SafeToolbox.XmlDecode(reader.GetAttribute("splashResource"));
			string attribute = reader.GetAttribute("continueDelay");
			if (attribute != null)
			{
				_continueDelay = SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_continueDelay = 0;
			}
			_showOnce = (reader.GetAttribute("showOnce") != "false");
			_showFirstTimeOnly = (reader.GetAttribute("showFirstTimeOnly") == "true");
			if (!base.ReadFromXml(reader))
			{
				return false;
			}
			return base.ReadChildLimits(reader);
		}

		protected override bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			if (_splashResource != null)
			{
				writer.WriteAttributeString("splashResource", SafeToolbox.XmlEncode(_splashResource));
			}
			if (_continueDelay > 0)
			{
				writer.WriteAttributeString("continueDelay", _continueDelay.ToString());
			}
			if (!_showOnce)
			{
				writer.WriteAttributeString("showOnce", "false");
			}
			if (_showFirstTimeOnly)
			{
				writer.WriteAttributeString("showFirstTimeOnly", "true");
			}
			return base.WriteToXml(writer, signing);
		}

		protected override SuperFormPanel CreatePanel(string pageId, SecureLicenseContext context)
		{
			if (((StringDictionary)base.CustomForms)[pageId] == null && (pageId == "DEFAULT" || pageId == null))
			{
				return new SplashPanel(this);
			}
			return base.CreatePanel(pageId, context);
		}
	}
}
