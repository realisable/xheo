using System;
using System.Collections;
using System.Collections.Specialized;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	[LimitEditor("OrLimitEditor", IconResource = "asmres://DeployLX.Licensing.DlxAddIn.v5/DeployLX.Licensing.DlxAddIn.v5.Resources.Limits.Or.png")]
	public class OrLimit : Limit
	{
		private bool _checkLastValidFirst = true;

		public override string Description
		{
			get
			{
				return SR.GetString("M_OrLimitDescription");
			}
		}

		public override string Name
		{
			get
			{
				return "Or";
			}
		}

		public bool CheckLastValidFirst
		{
			get
			{
				return _checkLastValidFirst;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_checkLastValidFirst != value)
				{
					_checkLastValidFirst = value;
					base.OnChanged("CheckLastValidFirst");
				}
			}
		}

		public override ValidationResult Granted(SecureLicenseContext context)
		{
			return CheckLimits(context, true);
		}

		private ValidationResult CheckLimits(SecureLicenseContext context, bool granted)
		{
			string key = base.UniqueId + ".Last";
			string text = ((StringDictionary)base.License.MetaValues)[key];
			if (text != null)
			{
				Limit limit = base.Limits[text];
				if (limit != null)
				{
					ValidationResult validationResult = granted ? limit.Granted(context) : limit.Validate(context);
					if (validationResult != 0)
					{
						return validationResult;
					}
				}
			}
			ArrayList arrayList = new ArrayList();
			ArrayList arrayList2 = new ArrayList();
			foreach (Limit item in (IEnumerable)base.Limits)
			{
				if (item.License == null)
				{
					item.License = base.License;
				}
				switch (item.Peek(context))
				{
				case PeekResult.Unknown:
				case PeekResult.Invalid:
					arrayList2.Add(item);
					break;
				default:
				{
					ValidationResult validationResult2 = granted ? item.Granted(context) : item.Validate(context);
					if (validationResult2 == ValidationResult.Invalid)
					{
						break;
					}
					if (item.LimitId != text)
					{
						((StringDictionary)base.License.MetaValues)[key] = item.LimitId;
					}
					return validationResult2;
				}
				case PeekResult.NeedsUser:
					arrayList.Add(item);
					break;
				}
			}
			foreach (Limit item2 in arrayList)
			{
				ValidationResult validationResult3 = granted ? item2.Granted(context) : item2.Validate(context);
				if (validationResult3 == ValidationResult.Invalid)
				{
					continue;
				}
				if (!(item2.LimitId != text))
				{
					return validationResult3;
				}
				((StringDictionary)base.License.MetaValues)[key] = item2.LimitId;
				break;
			}
			foreach (Limit item3 in arrayList2)
			{
				ValidationResult validationResult4 = granted ? item3.Granted(context) : item3.Validate(context);
				if (validationResult4 == ValidationResult.Invalid)
				{
					continue;
				}
				if (!(item3.LimitId != text))
				{
					return validationResult4;
				}
				((StringDictionary)base.License.MetaValues)[key] = item3.LimitId;
				break;
			}
			return context.ReportError("E_NoValidChildLimits", this);
		}

		public override ValidationResult Validate(SecureLicenseContext context)
		{
			return CheckLimits(context, false);
		}

		public override PeekResult Peek(SecureLicenseContext context)
		{
			if (!base.HasChildLimits)
			{
				return PeekResult.Valid;
			}
			PeekResult peekResult = PeekResult.Unknown;
			foreach (Limit item in (IEnumerable)base.Limits)
			{
				PeekResult peekResult2 = item.Peek(context);
				if (peekResult2 > peekResult)
				{
					peekResult = peekResult2;
				}
				if (peekResult2 == PeekResult.Valid)
				{
					return peekResult2;
				}
			}
			return peekResult;
		}

		protected override bool ReadFromXml(XmlReader reader)
		{
			_checkLastValidFirst = (reader.GetAttribute("checkLastValidFirst") != "false");
			return base.ReadChildLimits(reader);
		}

		protected override bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			if (!_checkLastValidFirst)
			{
				writer.WriteAttributeString("checkLastValidFirst", "false");
			}
			return true;
		}
	}
}
