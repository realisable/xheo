namespace DeployLX.Licensing.v5
{
	public enum PeekResult
	{
		Unknown,
		Invalid = 2,
		NeedsUser = 4,
		Valid = 8
	}
}
