using System;
using System.Net;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public sealed class ServerResult
	{
		private bool _success;

		private Exception _exception;

		private SecureLicense _copyLicense;

		private bool _retry;

		public bool Success
		{
			get
			{
				return _success;
			}
		}

		public Exception Exception
		{
			get
			{
				return _exception;
			}
		}

		public SecureLicense CopyLicense
		{
			get
			{
				return _copyLicense;
			}
		}

		public bool Retry
		{
			get
			{
				return _retry;
			}
		}

		public bool ConnectionError
		{
			get;
			set;
		}

		public ServerResult(bool success, Exception exception, SecureLicense copyLicense, bool retry)
		{
			_success = success;
			_exception = exception;
			_copyLicense = copyLicense;
			_retry = retry;
			ConnectionError = (exception != null && exception.InnerException is WebException);
		}
	}
}
