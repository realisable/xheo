using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	public class ActivateOnlinePanel : SuperFormPanel
	{
		private AsyncValidationRequest _request;

		private Timer _timer;

		private Button _cancel;

		private Button _goBack;

		private Button _retry;

		private ProgressBar _progressing;

		private ShadowLabel _activatingNotice;

		private Panel _errorPanel;

		private ShadowLabel _errorNotice;

		private Button _details;

		private PictureBox _errorImage;

		private ShadowLabel _successLabel;

		public ActivateOnlinePanel(ActivationLimit limit)
			: base(limit)
		{
			InitializeComponent();
			try
			{
				_progressing.Style = ProgressBarStyle.Marquee;
			}
			catch
			{
				_timer = new Timer();
				_timer.Interval = 1000;
				_timer.Tick += _timer_Tick;
			}
			_errorImage.Image = Images.BigError7_png;
		}

		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			_progressing = new ProgressBar();
			_activatingNotice = new ShadowLabel();
			_errorPanel = new Panel();
			_errorImage = new PictureBox();
			_details = new SkinnedButton();
			_errorNotice = new ShadowLabel();
			_successLabel = new ShadowLabel();
			_errorPanel.SuspendLayout();
			((ISupportInitialize)_errorImage).BeginInit();
			base.SuspendLayout();
			_progressing.Location = new Point(146, 160);
			_progressing.Maximum = 10;
			_progressing.Name = "_progressing";
			_progressing.Size = new Size(392, 23);
			_progressing.TabIndex = 0;
			_activatingNotice.AutoSize = true;
			_activatingNotice.BackColor = Color.Transparent;
			_activatingNotice.Location = new Point(147, 131);
			_activatingNotice.Name = "_activatingNotice";
			_activatingNotice.Size = new Size(0, 13);
			_activatingNotice.TabIndex = 1;
			_activatingNotice.ThemeFont = ThemeFont.Medium;
			_errorPanel.BackColor = Color.Transparent;
			_errorPanel.Controls.Add(_errorImage);
			_errorPanel.Controls.Add(_details);
			_errorPanel.Controls.Add(_errorNotice);
			_errorPanel.Location = new Point(146, 212);
			_errorPanel.Name = "_errorPanel";
			_errorPanel.Size = new Size(395, 116);
			_errorPanel.TabIndex = 3;
			_errorPanel.Visible = false;
			_errorImage.Location = new Point(0, 0);
			_errorImage.Name = "_errorImage";
			_errorImage.Size = new Size(48, 48);
			_errorImage.TabIndex = 6;
			_errorImage.TabStop = false;
			_details.Location = new Point(297, 88);
			_details.Name = "_details";
			_details.Size = new Size(95, 26);
			_details.TabIndex = 5;
			_details.Text = "#UI_Details";
			_details.Click += _details_Click;
			_errorNotice.Cursor = Cursors.Default;
			_errorNotice.Location = new Point(54, 0);
			_errorNotice.Name = "_errorNotice";
			_errorNotice.Size = new Size(341, 72);
			_errorNotice.TabIndex = 2;
			_errorNotice.Text = "#UI_ActivationOnlineErrors";
			_errorNotice.Url = null;
			_errorNotice.ThemeFont = ThemeFont.Medium;
			_successLabel.AutoSize = true;
			_successLabel.BackColor = Color.Transparent;
			_successLabel.Location = new Point(147, 131);
			_successLabel.Name = "_successLabel";
			_successLabel.Size = new Size(115, 15);
			_successLabel.TabIndex = 4;
			_successLabel.Text = "#UI_ActivationSuccess";
			_successLabel.ThemeFont = ThemeFont.Medium;
			_successLabel.Visible = false;
			base.Controls.Add(_successLabel);
			base.Controls.Add(_errorPanel);
			base.Controls.Add(_activatingNotice);
			base.Controls.Add(_progressing);
			base.Name = "ActivateOnlinePanel";
			_errorPanel.ResumeLayout(false);
			((ISupportInitialize)_errorImage).EndInit();
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		protected override void DestroyHandle()
		{
			base.DestroyHandle();
			if (_request != null && !_request.IsComplete)
			{
				_request.Cancel();
			}
		}

		protected void StartRequest()
		{
			ActivationLimit activationLimit = base.Limit as ActivationLimit;
			_activatingNotice.Text = SR.GetString("UI_ActivatingOnlineNotice");
			_request = activationLimit.ActivateAtServerAsync(base.SuperForm.Context, true, null, Completed);
			if (_timer != null)
			{
				_timer.Enabled = true;
			}
		}

		protected internal override void InitializePanel()
		{
			if (base.FirstInitialization)
			{
				_cancel = base.AddBottomButton("UI_Stop", 30);
			}
			else
			{
				_cancel = base.AddBottomButton("UI_Cancel", 30);
			}
			_cancel.Click += _cancel_Click;
			_retry = base.AddBottomButton("UI_Retry");
			_retry.Click += _retry_Click;
			_retry.Visible = !base.FirstInitialization;
			_goBack = base.AddBottomButton("UI_GoBack");
			_goBack.Image = Images.Back_png;
			_goBack.TextImageRelation = TextImageRelation.ImageBeforeText;
			_goBack.Click += _goBack_Click;
			_goBack.Visible = !base.FirstInitialization;
			base.SuperForm.CancelButton = _cancel;
			base.SuperForm.AcceptButton = _retry;
		}

		private void Completed(object sender, EventArgs e)
		{
			if (base.Parent != null)
			{
				switch (_request.GetResult())
				{
				case ValidationResult.Canceled:
					base.Return(FormResult.Incomplete);
					break;
				case ValidationResult.Retry:
					base.Return(FormResult.Retry);
					break;
				default:
					base.BeginInvoke(new MethodInvoker(ShowErrorPanel));
					break;
				case ValidationResult.Valid:
					base.BeginInvoke(new MethodInvoker(ShowOK));
					break;
				}
			}
		}

		private void ShowOK()
		{
			base.ShowControlsWithEffects(TriBoolValue.Default, _successLabel, _progressing, _cancel, _activatingNotice);
			base.ClearBottomControls();
			Button button = base.AddBottomButton("UI_OK");
			button.Click += ok_Click;
			base.SuperForm.AcceptButton = button;
			button.Select();
		}

		private void ShowErrorPanel()
		{
			_cancel.Text = SR.GetString("UI_Cancel");
			base.ShowControlsWithEffects(TriBoolValue.Yes, _errorPanel, _retry, _goBack);
			_retry.Select();
			_progressing.Style = ProgressBarStyle.Blocks;
		}

		private void _retry_Click(object sender, EventArgs e)
		{
			_cancel.Text = SR.GetString("UI_Stop");
			base.ShowControlsWithEffects(TriBoolValue.No, _errorPanel, _retry, _goBack);
			if (_timer != null)
			{
				_timer.Enabled = true;
			}
			else
			{
				_progressing.Style = ProgressBarStyle.Marquee;
			}
			StartRequest();
		}

		private void _details_Click(object sender, EventArgs e)
		{
			LicenseEventArgs licenseEventArgs = new LicenseEventArgs(base.Limit.License, base._superForm.Context, base._superForm.Context.LatestValidationRecord);
			SecureLicenseManager.OnErrorOccurred(licenseEventArgs);
			if (!licenseEventArgs.Handled)
			{
				ValidationRecordCollection records = new ValidationRecordCollection(base._superForm.Context.LatestValidationRecord);
				base.ShowPanel(new ErrorReportPanel(null, null, SR.GetString("UI_ErrorsConnectingToServer"), records, true), null);
			}
		}

		private void ok_Click(object sender, EventArgs e)
		{
			base.Return((FormResult)524289);
		}

		private void _cancel_Click(object sender, EventArgs e)
		{
			if (_request.IsComplete)
			{
				base.Return(FormResult.Incomplete);
			}
			else
			{
				_request.Cancel();
			}
		}

		private void _timer_Tick(object sender, EventArgs e)
		{
			_progressing.Increment(1);
			if (_progressing.Value == _progressing.Maximum)
			{
				_progressing.Value = _progressing.Minimum;
			}
			if (_request.IsComplete)
			{
				_timer.Enabled = false;
				_progressing.Value = _progressing.Minimum;
			}
		}

		private void _goBack_Click(object sender, EventArgs e)
		{
			base.Return(FormResult.Incomplete);
		}

		protected internal override void PanelShown(bool safeToChange)
		{
			base.PanelShown(safeToChange);
			if (base._firstInitialization && safeToChange)
			{
				StartRequest();
			}
		}
	}
}
