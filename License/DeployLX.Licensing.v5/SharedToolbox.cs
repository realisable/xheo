using Microsoft.Win32;
using System;
using System.Collections;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace DeployLX.Licensing.v5
{
	internal sealed class SharedToolbox
	{
		private class BrowserHelper : StaHelper
		{
			private string _url;

			private bool _newWindow;

			public BrowserHelper(string url, bool newWindow)
			{
				_url = url;
				_newWindow = newWindow;
			}

			protected override void Work()
			{
				try
				{
					string text = _url.ToLower(CultureInfo.InvariantCulture);
					if (_newWindow && !text.StartsWith(Uri.UriSchemeMailto))
					{
						using (RegistryKey registryKey = Registry.ClassesRoot.OpenSubKey("HTTP\\shell\\open\\command", false))
						{
							if (registryKey != null)
							{
								string text2 = registryKey.GetValue(null) as string;
								if (text2 != null)
								{
									text2 = ((text2.IndexOf("%1") <= -1) ? (text2 + " " + _url) : text2.Replace("%1", _url));
									string arguments = null;
									if (text2.StartsWith("\""))
									{
										int num = text2.IndexOf('"', 1);
										if (num > -1)
										{
											arguments = text2.Substring(num + 1).Trim();
											text2 = text2.Substring(1, num - 1);
										}
									}
									else
									{
										int num2 = text2.IndexOf(' ');
										if (num2 > -1)
										{
											arguments = text2.Substring(num2 + 1);
											text2 = text2.Substring(0, num2);
										}
									}
									Process.Start(text2, arguments);
									return;
								}
							}
						}
					}
					Process.Start(_url);
				}
				catch (Exception)
				{
					Process.Start(_url);
				}
			}
		}

		internal const string SafeBigRadix = "U9VWT2FG3Q7RS0AC1DEYMNX6P8HJ4KL5";

		internal const string DefaultRadix = "012345689ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!@#$%^*()_+-=[]{}|;:,.?/`~";

		internal const string MessyDefaultRadix = "7$,lsj*0mkyL._JV3b{Y;^HO~nSK8W@?[w`9F%RP(!qiC52DA&/4v:p)ZcU-6T|Med§GN=g'hoE}+]zQBft¶ax#rI1X";

		internal const string HexRadix = "0123456789ABCDEF";

		private static readonly int[] _crctable = new int[256]
		{
			0,
			49345,
			49537,
			320,
			49921,
			960,
			640,
			49729,
			50689,
			1728,
			1920,
			51009,
			1280,
			50625,
			50305,
			1088,
			52225,
			3264,
			3456,
			52545,
			3840,
			53185,
			52865,
			3648,
			2560,
			51905,
			52097,
			2880,
			51457,
			2496,
			2176,
			51265,
			55297,
			6336,
			6528,
			55617,
			6912,
			56257,
			55937,
			6720,
			7680,
			57025,
			57217,
			8000,
			56577,
			7616,
			7296,
			56385,
			5120,
			54465,
			54657,
			5440,
			55041,
			6080,
			5760,
			54849,
			53761,
			4800,
			4992,
			54081,
			4352,
			53697,
			53377,
			4160,
			61441,
			12480,
			12672,
			61761,
			13056,
			62401,
			62081,
			12864,
			13824,
			63169,
			63361,
			14144,
			62721,
			13760,
			13440,
			62529,
			15360,
			64705,
			64897,
			15680,
			65281,
			16320,
			16000,
			65089,
			64001,
			15040,
			15232,
			64321,
			14592,
			63937,
			63617,
			14400,
			10240,
			59585,
			59777,
			10560,
			60161,
			11200,
			10880,
			59969,
			60929,
			11968,
			12160,
			61249,
			11520,
			60865,
			60545,
			11328,
			58369,
			9408,
			9600,
			58689,
			9984,
			59329,
			59009,
			9792,
			8704,
			58049,
			58241,
			9024,
			57601,
			8640,
			8320,
			57409,
			40961,
			24768,
			24960,
			41281,
			25344,
			41921,
			41601,
			25152,
			26112,
			42689,
			42881,
			26432,
			42241,
			26048,
			25728,
			42049,
			27648,
			44225,
			44417,
			27968,
			44801,
			28608,
			28288,
			44609,
			43521,
			27328,
			27520,
			43841,
			26880,
			43457,
			43137,
			26688,
			30720,
			47297,
			47489,
			31040,
			47873,
			31680,
			31360,
			47681,
			48641,
			32448,
			32640,
			48961,
			32000,
			48577,
			48257,
			31808,
			46081,
			29888,
			30080,
			46401,
			30464,
			47041,
			46721,
			30272,
			29184,
			45761,
			45953,
			29504,
			45313,
			29120,
			28800,
			45121,
			20480,
			37057,
			37249,
			20800,
			37633,
			21440,
			21120,
			37441,
			38401,
			22208,
			22400,
			38721,
			21760,
			38337,
			38017,
			21568,
			39937,
			23744,
			23936,
			40257,
			24320,
			40897,
			40577,
			24128,
			23040,
			39617,
			39809,
			23360,
			39169,
			22976,
			22656,
			38977,
			34817,
			18624,
			18816,
			35137,
			19200,
			35777,
			35457,
			19008,
			19968,
			36545,
			36737,
			20288,
			36097,
			19904,
			19584,
			35905,
			17408,
			33985,
			34177,
			17728,
			34561,
			18368,
			18048,
			34369,
			33281,
			17088,
			17280,
			33601,
			16640,
			33217,
			32897,
			16448
		};

		private static readonly byte[] _ed = new byte[3]
		{
			1,
			0,
			1
		};

		private SharedToolbox()
		{
		}

		public static string ByteToString(byte[] data, string characterSet, int radix, int offset, int count, bool padToLongest)
		{
			if (data == null)
			{
				return null;
			}
			if (characterSet == null)
			{
				characterSet = "012345689ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!@#$%^*()_+-=[]{}|;:,.?/`~";
			}
			StringBuilder stringBuilder = new StringBuilder(data.Length);
			byte[] array = new byte[count];
			Buffer.BlockCopy(data, offset, array, 0, count);
			if (radix < 0)
			{
				radix = characterSet.Length;
			}
			int num = 0;
			int num2 = 0;
			do
			{
				int num3 = 0;
				num = 0;
				for (int i = num2; i < count; i++)
				{
					num3 <<= 8;
					num3 |= array[i];
					array[i] = (byte)(num3 / radix);
					num3 %= radix;
					num |= array[i];
				}
				if (array[num2] == 0)
				{
					num2++;
				}
				stringBuilder.Append(characterSet[num3]);
			}
			while (num > 0);
			if (padToLongest)
			{
				int num4 = CalculateLongestStringForByte(array.Length, characterSet, radix);
				while (stringBuilder.Length < num4)
				{
					stringBuilder.Append(characterSet[0]);
				}
			}
			char[] array2 = stringBuilder.ToString().ToCharArray();
			Array.Reverse(array2);
			return new string(array2);
		}

		public static string ByteToString(byte[] data, string characterSet, int radix)
		{
			if (data == null)
			{
				return null;
			}
			return ByteToString(data, characterSet, radix, 0, data.Length, false);
		}

		public static string ByteToString(byte[] data, string characterSet)
		{
			return ByteToString(data, characterSet, -1);
		}

		public static string ByteToString(byte[] data)
		{
			return ByteToString(data, null, -1);
		}

		public static byte[] StringToByte(string data, string characterSet, int radix)
		{
			if (data == null)
			{
				return null;
			}
			if (characterSet == null)
			{
				characterSet = "012345689ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!@#$%^*()_+-=[]{}|;:,.?/`~";
			}
			if (radix < 0)
			{
				radix = characterSet.Length;
			}
			int[] array = new int[256];
			for (int i = 0; i < radix; i++)
			{
				array[characterSet[i]] = i;
			}
			byte[] array2 = new byte[data.Length];
			for (int j = 0; j < data.Length; j++)
			{
				int num = array[data[j]];
				int num2 = 0;
				for (int num3 = array2.Length - 1; num3 >= 0; num3--)
				{
					num2 += array2[num3] * radix;
					array2[num3] = (byte)num2;
					num2 >>= 8;
				}
				num2 = num;
				for (int num4 = array2.Length - 1; num4 >= 0; num4--)
				{
					num2 += array2[num4];
					array2[num4] = (byte)num2;
					num2 >>= 8;
				}
			}
			double num5 = Math.Floor((double)data.Length * Math.Log10((double)radix)) / Math.Log10(2.0);
			int k;
			for (k = (int)Math.Round(num5 / 8.0); array2.Length - k > 0 && array2[array2.Length - k - 1] != 0; k++)
			{
			}
			byte[] array3 = new byte[k];
			Buffer.BlockCopy(array2, array2.Length - k, array3, 0, array3.Length);
			return array3;
		}

		public static byte[] StringToByte(string data, string characterSet)
		{
			return StringToByte(data, characterSet, -1);
		}

		public static int CalculateLongestStringForByte(int dataLength, string characterSet, int radix)
		{
			return CalculateLongestStringForBit(dataLength * 8, characterSet, radix);
		}

		public static int CalculateLongestStringForBit(int msbPos, string characterSet, int radix)
		{
			if (radix < 0)
			{
				Check.NotNull(characterSet, "characterSet");
				radix = characterSet.Length;
			}
			return (int)Math.Ceiling((double)msbPos * Math.Log10(2.0) / Math.Log10((double)radix));
		}

		internal static byte[] Scramble(byte[] b, int index, int count)
		{
			byte[] array = new byte[b.Length];
			int num = (count != 2147483647) ? (index + count) : count;
			if (num < 0)
			{
				num = 2147483647;
			}
			if (index > 0)
			{
				Buffer.BlockCopy(b, 0, array, 0, index);
			}
			int i;
			for (i = index; i < num && i + 4 <= array.Length; i += 4)
			{
				array[i] = (byte)(b[i + 1] >> 4 | b[i + 3] << 4);
				array[i + 1] = (byte)(b[i + 2] >> 4 | b[i] << 4);
				array[i + 2] = (byte)(b[i + 3] >> 4 | b[i + 1] << 4);
				array[i + 3] = (byte)(b[i] >> 4 | b[i + 2] << 4);
			}
			if (i < b.Length)
			{
				Buffer.BlockCopy(b, i, array, i, b.Length - i);
			}
			return array;
		}

		internal static byte[] Scramble(byte[] b)
		{
			return Scramble(b, 0, b.Length);
		}

		public static int GetHashCode(string s)
		{
			uint num = 5381u;
			int length = s.Length;
			for (int i = 0; i < length; i++)
			{
				num = ((num << 5) + num ^ s[i]);
			}
			return (int)num;
		}

		public static int GetShortHashCode(string s)
		{
			return Crc16(Encoding.UTF8.GetBytes((s == null) ? "" : s));
		}

		public static int Crc16(byte[] data, int offset, int length)
		{
			Check.NotNull(data, "data");
			if (offset > data.Length)
			{
				throw new ArgumentOutOfRangeException("offset", offset, null);
			}
			if (offset + length > data.Length)
			{
				throw new ArgumentOutOfRangeException("length", length, null);
			}
			int num = 0;
			for (int i = offset; i < offset + length; i++)
			{
				num = (num >> 8 ^ _crctable[(num & 0xFF) ^ data[i]]);
			}
			return num & 0xFFFF;
		}

		public static int Crc16(byte[] data)
		{
			Check.NotNull(data, "data");
			return Crc16(data, 0, data.Length);
		}

		public static Uri ResolveUrl(string source, SupportInfo info, SecureLicense license, Type licensedType)
		{
			if (source == null)
			{
				return null;
			}
			if (source.IndexOf("://") == -1)
			{
				source = "http://" + source;
			}
			string text = null;
			ActivationLimit activationLimit = (license == null) ? null : (license.Limits[typeof(ActivationLimit)] as ActivationLimit);
			if (activationLimit != null)
			{
				if (SecureLicenseContext.ResolveContext != null)
				{
					string key = "MachineProfile." + activationLimit.LimitId;
					text = (SecureLicenseContext.ResolveContext.Items[key] as string);
					if (text == null && activationLimit.Profiles.Count > 0)
					{
						ActivationProfile activationProfile = activationLimit.ActiveProfile;
						if (activationProfile == null)
						{
							activationProfile = activationLimit.Profiles[0];
						}
						text = activationProfile.MakeMachineProfile().Hash;
						SecureLicenseContext.ResolveContext.Items[key] = text;
					}
				}
				else
				{
					ActivationProfile activationProfile2 = activationLimit.ActiveProfile;
					if (activationProfile2 == null)
					{
						activationProfile2 = activationLimit.Profiles[0];
					}
					text = activationProfile2.MakeMachineProfile().Hash;
				}
			}
			else if (SecureLicenseContext.ResolveContext != null)
			{
				text = (SecureLicenseContext.ResolveContext.Items["MachineProfile"] as string);
				if (text == null)
				{
					text = MachineProfile.Profile.Hash;
					SecureLicenseContext.ResolveContext.Items["MachineProfile"] = text;
				}
			}
			else
			{
				text = MachineProfile.Profile.Hash;
			}
			string text2 = null;
			if (license != null && source.IndexOf("{12}") > -1)
			{
				StringBuilder stringBuilder = new StringBuilder();
				foreach (DictionaryEntry item in license.RegistrationInfo)
				{
					if (string.Compare(item.Key as string, "SerialNumber", true) != 0)
					{
						if (stringBuilder.Length > 0)
						{
							stringBuilder.Append('&');
						}
						stringBuilder.Append(Uri.EscapeDataString((item.Key as string) ?? ""));
						stringBuilder.Append('=');
						stringBuilder.Append(Uri.EscapeDataString((item.Value as string) ?? ""));
					}
				}
				text2 = stringBuilder.ToString();
			}
			return new Uri(string.Format(source, (license == null) ? null : Uri.EscapeDataString(license.LicenseId), (license == null) ? null : license.SerialNumber, (license == null) ? null : license.SerialNumberInfo.Prefix, text, (licensedType == null) ? null : Uri.EscapeDataString(licensedType.Assembly.FullName), Uri.EscapeDataString(CultureInfo.CurrentUICulture.Name), (info == null) ? null : Uri.EscapeDataString(info.Product ?? ""), (info == null) ? null : Uri.EscapeDataString(info.ProductVersion ?? ""), (info == null) ? null : Uri.EscapeDataString(info.Company ?? ""), (info == null) ? null : Uri.EscapeDataString(info.Website ?? ""), (licensedType == null) ? null : licensedType.Assembly.Location, (licensedType == null || string.IsNullOrEmpty(licensedType.Assembly.Location)) ? null : Path.GetDirectoryName((licensedType.Assembly.Location != "") ? licensedType.Assembly.Location : null), text2));
		}

		public static Uri ResolveUrl(string source, SecureLicenseContext context)
		{
			if (context == null)
			{
				context = SecureLicenseContext.ResolveContext;
			}
			return ResolveUrl(source, (context == null) ? null : context.SupportInfo, (context == null) ? null : context.ValidatingLicense, (context == null) ? null : context.LicensedType);
		}

		public static bool ValidateUrl(string url, bool throwException)
		{
			if (url != null && url.Length != 0)
			{
				if (!Regex.IsMatch(url, "^(((((((http://)|(https://)|(ftp://)|(asmres://)|(licres://))((([a-zA-Z0-9\\*\\?][\\-_a-zA-Z0-9\\*\\?]{0,62}\\.)*[a-zA-Z0-9\\*\\?][\\-_a-zA-Z0-9\\*\\?]{0,62}(\\.[a-zA-Z0-9\\*\\?]{2,5})?)|((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])))(\\:[0-9]{1,5})?(~?/([\\.\\-_a-zA-Z0-9~%={} ]*))*((/?[\\.\\-_a-zA-Z0-9~%\\?&#={}]* ))?(\\?.*)?))|(file://((([A-Za-z]:\\\\)|(\\\\\\\\[A-Za-z0-9]{0,16})\\\\)?(([A-Za-z0-9\\.\\-_\\[\\]\\(\\)\\$ {}/]*)[\\\\/])*([A-Za-z0-9\\.\\-_\\[\\]\\(\\)\\$ {}/]*)))))\\|)*((((((http://)|(https://)|(ftp://)|(asmres://)|(licres://))((([a-zA-Z0-9\\*\\?][\\-_a-zA-Z0-9\\*\\?]{0,62}\\.)*[a-zA-Z0-9\\*\\?][\\-_a-zA-Z0-9\\*\\?]{0,62}(\\.[a-zA-Z0-9\\*\\?]{2,5})?)|((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])))(\\:[0-9]{1,5})?(~?/([\\.\\-_a-zA-Z0-9~%={} ]*))*((/?[\\.\\-_a-zA-Z0-9~%\\?&#={}]* ))?(\\?.*)?))|(file://((([A-Za-z]:\\\\)|(\\\\\\\\[A-Za-z0-9]{0,16})\\\\)?(([A-Za-z0-9\\.\\-_\\[\\]\\(\\)\\$ {}/]*)[\\\\/])*([A-Za-z0-9\\.\\-_\\[\\]\\(\\)\\$ {}/]*)))))$", RegexOptions.Singleline | RegexOptions.CultureInvariant))
				{
					if (throwException)
					{
						throw new SecureLicenseException("E_InvalidUrl", url);
					}
					return false;
				}
				return true;
			}
			return true;
		}

		public static void OpenUrl(string url, bool newWindow)
		{
			BrowserHelper browserHelper = new BrowserHelper(url, newWindow);
			browserHelper.Go();
		}

		public static string Rot13(string value)
		{
			StringBuilder stringBuilder = new StringBuilder(value.Length);
			foreach (char c in value)
			{
				char c2 = c;
				if (c >= 'a' && c <= 'z')
				{
					c2 = (char)(c2 - 97);
					c2 = (char)(c2 + 13);
					c2 = (char)((int)c2 % 26);
					c2 = (char)(c2 + 97);
				}
				else if (c >= 'A' && c <= 'Z')
				{
					c2 = (char)(c2 - 65);
					c2 = (char)(c2 + 13);
					c2 = (char)((int)c2 % 26);
					c2 = (char)(c2 + 65);
				}
				stringBuilder.Append(c2);
			}
			return stringBuilder.ToString();
		}

		public static object GetProvider(byte[] key, SecureLicenseContext context)
		{
			RSAParameters rSAParameters = default(RSAParameters);
			rSAParameters.Modulus = key;
			rSAParameters.Exponent = _ed;
			CspParameters cspParameters = new CspParameters(1, "Microsoft Enhanced Cryptographic Provider v1.0", null);
			try
			{
				return TryCsp(ref rSAParameters, cspParameters, true, context);
			}
			catch (Exception innerException)
			{
				cspParameters.ProviderName = null;
				object obj = TryCsp(ref rSAParameters, cspParameters, false, context);
				if (obj != null)
				{
					return obj;
				}
				cspParameters.Flags = CspProviderFlags.UseMachineKeyStore;
				cspParameters.ProviderName = "Microsoft Enhanced Cryptographic Provider v1.0";
				obj = TryCsp(ref rSAParameters, cspParameters, false, context);
				if (obj != null)
				{
					return obj;
				}
				cspParameters.Flags = CspProviderFlags.UseMachineKeyStore;
				cspParameters.ProviderName = null;
				obj = TryCsp(ref rSAParameters, cspParameters, false, context);
				if (obj != null)
				{
					return obj;
				}
				cspParameters.Flags = CspProviderFlags.NoFlags;
				cspParameters.KeyContainerName = "DLX_v3";
				cspParameters.ProviderName = null;
				obj = TryCsp(ref rSAParameters, cspParameters, false, context);
				if (obj != null)
				{
					return obj;
				}
				cspParameters.Flags = CspProviderFlags.UseMachineKeyStore;
				cspParameters.KeyContainerName = "DLX_v3";
				obj = TryCsp(ref rSAParameters, cspParameters, false, context);
				if (obj == null)
				{
					throw new SecureLicenseException("E_RSADenied", innerException);
				}
				return obj;
			}
		}

		private static object TryCsp(ref RSAParameters p, CspParameters csp, bool throwOnError, SecureLicenseContext context)
		{
			try
			{
				RSACryptoServiceProvider rSACryptoServiceProvider = rSACryptoServiceProvider = new RSACryptoServiceProvider(384, csp);
				try
				{
					rSACryptoServiceProvider.PersistKeyInCsp = false;
				}
				catch
				{
				}
				rSACryptoServiceProvider.ImportParameters(p);
				return rSACryptoServiceProvider;
			}
			catch (Exception exception)
			{
				if (context != null && context.RequestInfo != null && context.RequestInfo.DeveloperMode)
				{
					context.ReportError("E_UnexpectedValidate", context, exception, ErrorSeverity.High);
				}
				if (!throwOnError)
				{
					goto end_IL_002a;
				}
				throw;
				end_IL_002a:;
			}
			return null;
		}
	}
}
