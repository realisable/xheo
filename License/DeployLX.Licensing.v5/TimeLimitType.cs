namespace DeployLX.Licensing.v5
{
	public enum TimeLimitType
	{
		AtATime,
		Cumulative,
		FromFirstUse,
		SpecificDate
	}
}
