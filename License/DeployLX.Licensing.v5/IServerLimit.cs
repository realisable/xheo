namespace DeployLX.Licensing.v5
{
	public interface IServerLimit
	{
		UriCollection Servers
		{
			get;
		}

		int ServerRetries
		{
			get;
			set;
		}
	}
}
