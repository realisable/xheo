using System;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public sealed class MachineProfileEntry : ICloneable, IChange
	{
		private bool _isReadOnly;

		private MachineProfileEntryType _type;

		private string _displayName;

		private int _weight = 2;

		private int _partialMatchWeight = -1;

		private int _fileMovedWeight = -1;

		private IMachineProfileEntryMaker _entryMaker;

		public MachineProfileEntryType Type
		{
			get
			{
				return _type;
			}
		}

		public string DisplayName
		{
			get
			{
				if (_displayName == null)
				{
					return _type.ToString();
				}
				return _displayName;
			}
			set
			{
				AssertNotReadOnly();
				if (_displayName != value)
				{
					_displayName = value;
					OnChanged(new ChangeEventArgs("DisplayName", this));
				}
			}
		}

		public int Weight
		{
			get
			{
				return _weight;
			}
			set
			{
				AssertNotReadOnly();
				if (_weight != value)
				{
					_weight = value;
					OnChanged(new ChangeEventArgs("Weight", this));
				}
			}
		}

		public int PartialMatchWeight
		{
			get
			{
				if (_partialMatchWeight != -1)
				{
					return _partialMatchWeight;
				}
				return _weight;
			}
			set
			{
				AssertNotReadOnly();
				if (_partialMatchWeight != value)
				{
					_partialMatchWeight = value;
					OnChanged(new ChangeEventArgs("PartialMatchWeight", this));
				}
			}
		}

		public int FileMovedWeight
		{
			get
			{
				if (_fileMovedWeight != -1)
				{
					return _fileMovedWeight;
				}
				return _weight;
			}
			set
			{
				_fileMovedWeight = value;
			}
		}

		internal IMachineProfileEntryMaker EntryMaker
		{
			get
			{
				return _entryMaker;
			}
			set
			{
				AssertNotReadOnly();
				if (_entryMaker != null)
				{
					throw new SecureLicenseException("E_ReadOnlyObject", "MachineProfileEntry");
				}
				if (_entryMaker != value)
				{
					_entryMaker = value;
					OnChanged(new ChangeEventArgs("EntryMaker", this));
				}
			}
		}

		public event ChangeEventHandler Changed;

		private void OnChanged(ChangeEventArgs e)
		{
			if (this.Changed != null)
			{
				this.Changed(this, e);
			}
		}

		public MachineProfileEntry(MachineProfileEntryType type, int weight, int partialWeight, int fileMovedWeight)
		{
			_type = type;
			_weight = weight;
			_partialMatchWeight = partialWeight;
			_fileMovedWeight = fileMovedWeight;
		}

		public MachineProfileEntry()
		{
		}

		public override string ToString()
		{
			return DisplayName;
		}

		public void MakeReadOnly()
		{
			_isReadOnly = true;
		}

		private void AssertNotReadOnly()
		{
			if (!_isReadOnly)
			{
				return;
			}
			throw new SecureLicenseException("E_ReadOnlyObject", "MachineProfileEntry");
		}

		public void WriteToXml(XmlWriter writer)
		{
			Check.NotNull(writer, "writer");
			writer.WriteStartElement("Entry");
			int type = (int)_type;
			writer.WriteAttributeString("type", type.ToString());
			writer.WriteAttributeString("weight", _weight.ToString());
			if (_partialMatchWeight != -1 && _partialMatchWeight != _weight)
			{
				writer.WriteAttributeString("partialWeight", _partialMatchWeight.ToString());
			}
			if (_fileMovedWeight != -1 && _fileMovedWeight != _weight)
			{
				writer.WriteAttributeString("movedWeight", _fileMovedWeight.ToString());
			}
			if (_displayName != null)
			{
				writer.WriteAttributeString("displayName", _displayName);
			}
			if (_entryMaker != null)
			{
				writer.WriteAttributeString("entryMaker", _entryMaker.GetType().AssemblyQualifiedName);
			}
			writer.WriteEndElement();
		}

		public bool ReadFromXml(XmlReader reader)
		{
			Check.NotNull(reader, "reader");
			string attribute = reader.GetAttribute("type");
			if (attribute != null)
			{
				_type = (MachineProfileEntryType)SafeToolbox.FastParseInt32(attribute);
				attribute = reader.GetAttribute("weight");
				if (attribute != null)
				{
					_weight = SafeToolbox.FastParseInt32(attribute);
				}
				else
				{
					_weight = 1;
				}
				attribute = reader.GetAttribute("partialWeight");
				if (attribute != null)
				{
					_partialMatchWeight = SafeToolbox.FastParseInt32(attribute);
				}
				else
				{
					_partialMatchWeight = -1;
				}
				attribute = reader.GetAttribute("movedWeight");
				if (attribute != null)
				{
					_fileMovedWeight = SafeToolbox.FastParseInt32(attribute);
				}
				else
				{
					_fileMovedWeight = -1;
				}
				_displayName = reader.GetAttribute("displayName");
				attribute = reader.GetAttribute("entryMaker");
				if (attribute != null)
				{
					Type type = TypeHelper.FindType(attribute, true);
					_entryMaker = (Activator.CreateInstance(type) as IMachineProfileEntryMaker);
				}
				reader.Read();
				return true;
			}
			return false;
		}

		object ICloneable.Clone()
		{
			return base.MemberwiseClone();
		}
	}
}
