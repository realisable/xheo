using System;
using System.Collections;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public class StringCollection : WithEventsCollection
	{
		private bool _caseSensitive;

		public string this[int index]
		{
			get
			{
				return base.List[index] as string;
			}
			set
			{
				base.List[index] = value;
			}
		}

		public bool CaseSensitive
		{
			get
			{
				return _caseSensitive;
			}
			set
			{
				_caseSensitive = value;
			}
		}

		public int Add(string item)
		{
			return base.List.Add(item);
		}

		public void AddRange(string[] items)
		{
			if (items != null)
			{
				foreach (string item in items)
				{
					Add(item);
				}
			}
		}

		public void AddRange(StringCollection items)
		{
			if (items != null)
			{
				foreach (string item in (IEnumerable)items)
				{
					Add(item);
				}
			}
		}

		public void Insert(int index, string item)
		{
			base.List.Insert(index, item);
		}

		public void Remove(string item)
		{
			base.List.Remove(item);
		}

		public void CopyTo(string[] array, int index)
		{
			base.List.CopyTo(array, index);
		}

		public int IndexOf(string item)
		{
			int num = base.List.IndexOf(item);
			if (!_caseSensitive && num < 0)
			{
				num = 0;
				while (true)
				{
					if (num < Count)
					{
						if (string.Compare(base.List[num] as string, item, true) == 0)
						{
							break;
						}
						num++;
						continue;
					}
					return -1;
				}
				return num;
			}
			return num;
		}

		public bool Contains(string item)
		{
			return IndexOf(item) >= 0;
		}

		public void WriteToXml(XmlWriter writer, string collectionName, string itemName, string attributeName)
		{
			WriteToXml(writer, collectionName, itemName, attributeName, 0, Count);
		}

		public virtual void WriteToXml(XmlWriter writer, string collectionName, string itemName, string attributeName, int index, int count)
		{
			if (count != 0 && Count != 0)
			{
				Check.NotNull(writer, "writer");
				if (itemName == null)
				{
					itemName = "Item";
				}
				if (attributeName == null)
				{
					attributeName = "value";
				}
				if (collectionName != null)
				{
					writer.WriteStartElement(collectionName);
				}
				for (int i = index; i < count; i++)
				{
					writer.WriteStartElement(itemName);
					writer.WriteAttributeString(attributeName, this[i]);
					writer.WriteEndElement();
				}
				if (collectionName != null)
				{
					writer.WriteEndElement();
				}
			}
		}

		public virtual bool ReadFromXml(XmlReader reader, string collectionName, string itemName, string attributeName, bool clear)
		{
			Check.NotNull(reader, "reader");
			if (itemName == null)
			{
				itemName = "Item";
			}
			if (attributeName == null)
			{
				attributeName = "value";
			}
			if (collectionName != null)
			{
				if (reader.Name != collectionName)
				{
					return false;
				}
				reader.Read();
			}
			if (clear)
			{
				Clear();
			}
			while (!reader.EOF)
			{
				reader.MoveToContent();
				if (reader.IsStartElement())
				{
					if (reader.Name == itemName)
					{
						string attribute = reader.GetAttribute(attributeName);
						if (attribute != null)
						{
							Add(attribute);
						}
						reader.Read();
						if (collectionName == null)
						{
							break;
						}
					}
					else
					{
						reader.Read();
					}
					continue;
				}
				reader.Read();
				break;
			}
			if (collectionName != null && reader.Name == collectionName)
			{
				reader.Read();
			}
			return true;
		}
	}
}
