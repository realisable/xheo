namespace DeployLX.Licensing.v5
{
	public enum AdminRunRequirement
	{
		No,
		ApplicationsAndServices,
		All
	}
}
