namespace DeployLX.Licensing.v5
{
	public enum SoundEvent
	{
		NotSet,
		NoSound,
		SystemAsterisk,
		SystemExclamation = 4,
		SystemExit = 8,
		SystemHand = 0x10,
		SystemNotification = 0x20,
		SystemQuestion = 0x40,
		SystemStart = 0x80,
		SystemDefault = 0x100
	}
}
