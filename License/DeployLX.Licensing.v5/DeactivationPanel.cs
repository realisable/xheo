using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	public class DeactivationPanel : SplitPanel
	{
		private ShadowLabel _activateTitle;

		private ShadowLabel _notice;

		private ShadowLabel _warning;

		private BufferedPictureBox _serverNoticeIcon;

		private ShadowLabel _serverNotice;

		public DeactivationPanel(ActivationLimit activationLimit)
			: base(activationLimit)
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			_activateTitle = new ShadowLabel();
			_notice = new ShadowLabel();
			_warning = new ShadowLabel();
			_serverNotice = new ShadowLabel();
			_serverNoticeIcon = new BufferedPictureBox();
			base.BodyPanel.SuspendLayout();
			((ISupportInitialize)_serverNoticeIcon).BeginInit();
			base.SuspendLayout();
			base.SidePanel.Location = new Point(0, 0);
			base.SidePanel.Size = new Size(170, 421);
			base.BodyPanel.Controls.Add(_serverNoticeIcon);
			base.BodyPanel.Controls.Add(_serverNotice);
			base.BodyPanel.Controls.Add(_warning);
			base.BodyPanel.Controls.Add(_notice);
			base.BodyPanel.Controls.Add(_activateTitle);
			base.BodyPanel.Location = new Point(170, 0);
			base.BodyPanel.Size = new Size(514, 421);
			_activateTitle.Location = new Point(14, 16);
			_activateTitle.Name = "_activateTitle";
			_activateTitle.Size = new Size(482, 34);
			_activateTitle.TabIndex = 0;
			_activateTitle.Text = "#UI_DeactivateTitle";
			_activateTitle.ThemeColor = new string[2]
			{
				"PanelTitleText",
				"Text"
			};
			_notice.Location = new Point(16, 50);
			_notice.Name = "_notice";
			_notice.Size = new Size(482, 117);
			_notice.TabIndex = 0;
			_notice.ThemeFont = ThemeFont.Medium;
			_warning.Location = new Point(16, 357);
			_warning.Name = "_warning";
			_warning.Size = new Size(482, 48);
			_warning.TabIndex = 13;
			_warning.Text = "#UI_DeactivationMachineWarning";
			_warning.TextAlign = ContentAlignment.BottomLeft;
			_serverNotice.Location = new Point(38, 168);
			_serverNotice.Name = "_serverNotice";
			_serverNotice.Size = new Size(460, 40);
			_serverNotice.TabIndex = 14;
			_serverNotice.Text = "#UI_DeactivationServerNotice";
			_serverNotice.Visible = false;
			_serverNoticeIcon.BackColor = Color.Transparent;
			_serverNoticeIcon.BackgroundImageLayout = ImageLayout.None;
			_serverNoticeIcon.Location = new Point(16, 168);
			_serverNoticeIcon.Name = "_serverNoticeIcon";
			_serverNoticeIcon.OffsetBackgroundImage = false;
			_serverNoticeIcon.Size = new Size(16, 16);
			_serverNoticeIcon.TabIndex = 15;
			_serverNoticeIcon.TabStop = false;
			_serverNoticeIcon.Visible = false;
			base.AutoScaleDimensions = new SizeF(96f, 96f);
			base.Name = "DeactivationPanel";
			base.BodyPanel.ResumeLayout(false);
			((ISupportInitialize)_serverNoticeIcon).EndInit();
			base.ResumeLayout(false);
		}

		protected internal override void LoadPanel(SecureLicenseContext context)
		{
			base.LoadPanel(context);
			ActivationLimit activationLimit = base.Limit as ActivationLimit;
			if (activationLimit.LogoResource != null)
			{
				base.Logo = Toolbox.GetImage(activationLimit.LogoResource, context);
			}
			if (activationLimit.Servers.Count > 0)
			{
				_serverNoticeIcon.Visible = true;
				_serverNoticeIcon.Image = Images.Warning_png;
				_serverNotice.Visible = true;
			}
		}

		protected virtual void HandleTellMeMore()
		{
			base.ShowPage("ACTIVATEOINFO", null);
		}

		protected internal override void InitializePanel()
		{
			_notice.Text = SR.GetString("UI_DeactivationNotice", base.SuperForm.Context.SupportInfo.Product);
			Button button = base.AddBottomButton("#UI_Cancel", 30);
			button.Click += _cancel_Click;
			base._superForm.CancelButton = button;
			button = base.AddBottomButton("#UI_Deactivate");
			button.Click += _continue_Click;
			base._superForm.AcceptButton = button;
			Label label = base.AddBottomLabel("UI_TellMeMoreActivation", "#", Images.Notice_png);
			label.Click += _tellMeMore_LinkClicked;
			base.AddLicenseAgreementLink();
		}

		private void _continue_Click(object sender, EventArgs e)
		{
			if (base.ShowMessageBox(SR.GetString("UI_DeactivationConfirmation", base.SuperForm.Context.SupportInfo.Product), "UI_ValidationNoticeTitle", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
			{
				base.ShowPanel(new DoDeactivationPanel(base.Limit as ActivationLimit), HandleDeactivation, true);
			}
		}

		private void HandleDeactivation(FormResult result)
		{
			base.Return(result);
		}

		private void _cancel_Click(object sender, EventArgs e)
		{
			base.Return(FormResult.Failure);
		}

		private void _tellMeMore_LinkClicked(object sender, EventArgs e)
		{
			HandleTellMeMore();
		}

		protected internal override void UpdateFromTheme()
		{
			base.UpdateFromTheme();
			_activateTitle.Font = base.SuperForm.TitleFont;
		}

		protected override void HandleEula()
		{
			base.ShowPage("EULA", null);
		}
	}
}
