using System;
using System.IO;
using System.Reflection;

namespace DeployLX.Licensing.v5
{
	internal sealed class AsmResProtocol : ICustomProtocol
	{
		public string Scheme
		{
			get
			{
				return "asmres";
			}
		}

		public bool Navigate
		{
			get
			{
				return true;
			}
		}

		public bool CanProcessUri(Uri uri)
		{
			if (uri == (Uri)null)
			{
				throw new ArgumentNullException("uri");
			}
			return true;
		}

		public Stream GetResourceStream(Uri uri)
		{
			if (uri == (Uri)null)
			{
				throw new ArgumentNullException("uri");
			}
			Assembly assembly = null;
			try
			{
				assembly = Assembly.Load(uri.Host);
				if (assembly == null)
				{
					return null;
				}
			}
			catch
			{
				try
				{
					Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
					int num = 0;
					while (num < assemblies.Length)
					{
						Assembly assembly2 = assemblies[num];
						if (string.Compare(uri.Host, assembly2.GetName().Name, true) != 0)
						{
							num++;
							continue;
						}
						assembly = assembly2;
						break;
					}
					if (assembly == null)
					{
						return null;
					}
				}
				catch
				{
					return null;
				}
			}
			int i;
			for (i = 0; uri.PathAndQuery.Length > i && (uri.PathAndQuery[i] == '/' || uri.PathAndQuery[i] == '\\'); i++)
			{
			}
			string text = uri.PathAndQuery.Substring(i);
			text = text.Replace('/', '.');
			text = text.Replace('\\', '.');
			Stream manifestResourceStream = assembly.GetManifestResourceStream(text);
			if (manifestResourceStream != null)
			{
				return manifestResourceStream;
			}
			string[] manifestResourceNames = assembly.GetManifestResourceNames();
			int num2 = 0;
			string text2;
			while (true)
			{
				if (num2 < manifestResourceNames.Length)
				{
					text2 = manifestResourceNames[num2];
					if (text2.Length >= text.Length && string.Compare(text2, text2.Length - text.Length, text, 0, text.Length, true) == 0)
					{
						break;
					}
					num2++;
					continue;
				}
				return null;
			}
			return assembly.GetManifestResourceStream(text2);
		}
	}
}
