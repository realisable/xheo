using System.Collections;
using System.Reflection;

namespace DeployLX.Licensing.v5
{
	internal sealed class AssemblyComparer : IComparer
	{
		public int Compare(object x, object y)
		{
			if (x == null)
			{
				if (y != null)
				{
					return -1;
				}
				return 0;
			}
			if (y == null)
			{
				return 1;
			}
			if (object.ReferenceEquals(x, y))
			{
				return 0;
			}
			Assembly assembly = x as Assembly;
			Assembly assembly2 = y as Assembly;
			AssemblyName name = assembly.GetName();
			AssemblyName name2 = assembly2.GetName();
			int num = name.Name.CompareTo(name2.Name);
			if (num != 0)
			{
				return num;
			}
			num = name2.Version.CompareTo(name2.Version);
			if (num != 0)
			{
				return num;
			}
			if (name.KeyPair == null && name2.KeyPair != null)
			{
				return -1;
			}
			if (name2.KeyPair == null)
			{
				return 1;
			}
			string codeBase = name.CodeBase;
			string codeBase2 = name2.CodeBase;
			if (codeBase == null)
			{
				if (y != null)
				{
					return -1;
				}
				return 0;
			}
			if (codeBase2 == null)
			{
				return 1;
			}
			return codeBase.CompareTo(codeBase2);
		}
	}
}
