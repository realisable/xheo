namespace DeployLX.Licensing.v5
{
	public delegate ValidationResult AsyncValidationMethod(SecureLicenseContext context, bool reportError, object[] args);
}
