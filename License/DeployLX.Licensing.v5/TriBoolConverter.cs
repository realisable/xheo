using System;
using System.ComponentModel;
using System.ComponentModel.Design.Serialization;
using System.Globalization;
using System.Reflection;

namespace DeployLX.Licensing.v5
{
	internal sealed class TriBoolConverter : TypeConverter
	{
		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			if (sourceType != typeof(string) && sourceType != typeof(bool) && sourceType != typeof(TriBoolValue))
			{
				return base.CanConvertFrom(context, sourceType);
			}
			return true;
		}

		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			if (destinationType != typeof(string) && destinationType != typeof(bool) && destinationType != typeof(TriBoolValue) && destinationType != typeof(InstanceDescriptor))
			{
				return base.CanConvertTo(context, destinationType);
			}
			return true;
		}

		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			string text = value as string;
			if (text != null)
			{
				return new TriBool((TriBoolValue)Enum.Parse(typeof(TriBoolValue), text, true));
			}
			if (value is bool)
			{
				return new TriBool((bool)value);
			}
			if (value is TriBoolValue)
			{
				return new TriBool((TriBoolValue)value);
			}
			return base.ConvertFrom(context, culture, value);
		}

		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			if (destinationType == null)
			{
				throw new ArgumentNullException("destinationType");
			}
			if (destinationType == typeof(string))
			{
				if (value == null)
				{
					return null;
				}
				return ((TriBool)value).ToString(null, culture);
			}
			if (destinationType == typeof(bool))
			{
				return (bool)(TriBool)value;
			}
			if (destinationType == typeof(TriBoolValue))
			{
				return ((TriBool)value).Value;
			}
			if (destinationType == typeof(InstanceDescriptor))
			{
				TriBool triBool = (TriBool)value;
				object[] arguments = new object[1]
				{
					triBool.Value
				};
				Type[] types = new Type[1]
				{
					typeof(TriBoolValue)
				};
				ConstructorInfo constructor = typeof(TriBool).GetConstructor(types);
				return new InstanceDescriptor(constructor, arguments);
			}
			return base.ConvertTo(context, culture, value, destinationType);
		}

		public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
		{
			return true;
		}

		public override bool GetStandardValuesExclusive(ITypeDescriptorContext context)
		{
			return true;
		}

		public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
		{
			return new StandardValuesCollection(new TriBool[3]
			{
				new TriBool(TriBoolValue.Default),
				new TriBool(TriBoolValue.Yes),
				new TriBool(TriBoolValue.No)
			});
		}
	}
}
