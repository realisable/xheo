namespace DeployLX.Licensing.v5
{
	public enum HtmlLocation
	{
		Top,
		Bottom,
		Inline
	}
}
