using System;
using System.Collections;
using System.Text;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public sealed class UriCollection : WithEventsCollection
	{
		public static readonly string[] UrlSchemes = new string[2]
		{
			"http",
			"https"
		};

		public static readonly string[] ResourceSchemes = new string[6]
		{
			"http",
			"asmres",
			"licres",
			"https",
			"file",
			"ftp"
		};

		public string this[int index]
		{
			get
			{
				return base.List[index] as string;
			}
			set
			{
				base.List[index] = value;
			}
		}

		public bool VerifyValues
		{
			get;
			set;
		}

		public UriCollection()
		{
			VerifyValues = true;
		}

		protected override void OnChanging(CollectionEventArgs e)
		{
			string url = e.Element as string;
			if (VerifyValues)
			{
				SharedToolbox.ValidateUrl(url, true);
			}
			base.OnChanging(e);
		}

		public int Add(string uri)
		{
			return base.List.Add(uri);
		}

		public void AddRange(string[] uris)
		{
			if (uris != null)
			{
				foreach (string uri in uris)
				{
					Add(uri);
				}
			}
		}

		public void AddList(string list)
		{
			if (list != null && list.Length != 0)
			{
				string[] array = list.Split('|');
				string[] array2 = array;
				foreach (string text in array2)
				{
					if (text.Trim().Length != 0)
					{
						Add(text);
					}
				}
			}
		}

		public void AddRange(UriCollection uris)
		{
			if (uris != null)
			{
				foreach (string item in (IEnumerable)uris)
				{
					Add(item);
				}
			}
		}

		public void Insert(int index, string uri)
		{
			base.List.Insert(index, uri);
		}

		public void Remove(string uri)
		{
			base.List.Remove(uri);
		}

		public void CopyTo(string[] array, int index)
		{
			base.List.CopyTo(array, index);
		}

		public int IndexOf(string uri)
		{
			return base.List.IndexOf(uri);
		}

		public bool Contains(string uri)
		{
			return base.List.Contains(uri);
		}

		public string GetList()
		{
			if (Count == 0)
			{
				return null;
			}
			StringBuilder stringBuilder = new StringBuilder();
			foreach (string item in (IEnumerable)this)
			{
				if (stringBuilder.Length > 0)
				{
					stringBuilder.Append('|');
				}
				stringBuilder.Append(item);
			}
			return stringBuilder.ToString();
		}

		public static UriCollection FromList(string list)
		{
			if (list != null && list.Length != 0)
			{
				string[] array = list.Split('|');
				UriCollection uriCollection = new UriCollection();
				string[] array2 = array;
				foreach (string text in array2)
				{
					string text2 = text.Trim();
					SharedToolbox.ValidateUrl(text2, true);
					if (text2.Length != 0)
					{
						uriCollection.Add(text2);
					}
				}
				if (uriCollection.Count == 0)
				{
					return null;
				}
				return uriCollection;
			}
			return null;
		}

		public override string ToString()
		{
			return GetList();
		}
	}
}
