using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public sealed class Skin : IXmlPersistable
	{
		private bool _isReadOnly;

		private Bitmap _image;

		private string _imageEncoding;

		private string _imageUrl;

		private Bitmap _flatImage;

		private Color _textColor = Color.Black;

		private int _edgeLeftWidth = 1;

		private int _edgeRightWidth = 1;

		private int _edgeTopHeight = 1;

		private int _edgeBottomHeight = 1;

		private EdgeScaling _edgeScalingTop;

		private EdgeScaling _edgeScalingBottom;

		private EdgeScaling _edgeScalingRight;

		private EdgeScaling _edgeScalingLeft;

		private EdgeScaling _edgeScalingCenter;

		private int _textOffsetX;

		private int _textOffsetY;

		private int _downTextDeltaY = 1;

		private int _downTextDeltaX = 1;

		private DropShadow _shadow = new DropShadow();

		public bool IsReadOnly
		{
			get
			{
				return _isReadOnly;
			}
		}

		public Bitmap Image
		{
			get
			{
				return _image;
			}
			set
			{
				_image = value;
				_imageEncoding = null;
				_imageUrl = null;
				OnChanged(EventArgs.Empty);
			}
		}

		public Bitmap FlatImage
		{
			get
			{
				return _flatImage;
			}
			set
			{
				_flatImage = value;
				OnChanged(EventArgs.Empty);
			}
		}

		public Color TextColor
		{
			get
			{
				return _textColor;
			}
			set
			{
				if (_textColor != value)
				{
					_textColor = value;
					OnChanged(EventArgs.Empty);
				}
			}
		}

		[DefaultValue(1)]
		public int EdgeLeftWidth
		{
			get
			{
				return _edgeLeftWidth;
			}
			set
			{
				if (value < 1)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				_edgeLeftWidth = value;
				OnChanged(EventArgs.Empty);
			}
		}

		[DefaultValue(1)]
		public int EdgeRightWidth
		{
			get
			{
				return _edgeRightWidth;
			}
			set
			{
				if (value < 1)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				_edgeRightWidth = value;
				OnChanged(EventArgs.Empty);
			}
		}

		[DefaultValue(1)]
		public int EdgeTopHeight
		{
			get
			{
				return _edgeTopHeight;
			}
			set
			{
				if (value < 1)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				_edgeTopHeight = value;
				OnChanged(EventArgs.Empty);
			}
		}

		[DefaultValue(1)]
		public int EdgeBottomHeight
		{
			get
			{
				return _edgeBottomHeight;
			}
			set
			{
				if (value < 1)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				_edgeBottomHeight = value;
				OnChanged(EventArgs.Empty);
			}
		}

		[DefaultValue(EdgeScaling.Stretch)]
		public EdgeScaling EdgeScalingTop
		{
			get
			{
				return _edgeScalingTop;
			}
			set
			{
				_edgeScalingTop = value;
				OnChanged(EventArgs.Empty);
			}
		}

		[DefaultValue(EdgeScaling.Stretch)]
		public EdgeScaling EdgeScalingBottom
		{
			get
			{
				return _edgeScalingBottom;
			}
			set
			{
				_edgeScalingBottom = value;
				OnChanged(EventArgs.Empty);
			}
		}

		[DefaultValue(EdgeScaling.Stretch)]
		public EdgeScaling EdgeScalingRight
		{
			get
			{
				return _edgeScalingRight;
			}
			set
			{
				_edgeScalingRight = value;
				OnChanged(EventArgs.Empty);
			}
		}

		[DefaultValue(EdgeScaling.Stretch)]
		public EdgeScaling EdgeScalingLeft
		{
			get
			{
				return _edgeScalingLeft;
			}
			set
			{
				_edgeScalingLeft = value;
				OnChanged(EventArgs.Empty);
			}
		}

		[DefaultValue(EdgeScaling.Stretch)]
		public EdgeScaling EdgeScalingCenter
		{
			get
			{
				return _edgeScalingCenter;
			}
			set
			{
				_edgeScalingCenter = value;
				OnChanged(EventArgs.Empty);
			}
		}

		[DefaultValue(0)]
		public int TextOffsetX
		{
			get
			{
				return _textOffsetX;
			}
			set
			{
				_textOffsetX = value;
				OnChanged(EventArgs.Empty);
			}
		}

		[DefaultValue(0)]
		public int TextOffsetY
		{
			get
			{
				return _textOffsetY;
			}
			set
			{
				_textOffsetY = value;
				OnChanged(EventArgs.Empty);
			}
		}

		[DefaultValue(1)]
		public int DownTextDeltaY
		{
			get
			{
				return _downTextDeltaY;
			}
			set
			{
				_downTextDeltaY = value;
				OnChanged(EventArgs.Empty);
			}
		}

		[DefaultValue(1)]
		public int DownTextDeltaX
		{
			get
			{
				return _downTextDeltaX;
			}
			set
			{
				_downTextDeltaX = value;
				OnChanged(EventArgs.Empty);
			}
		}

		public DropShadow Shadow
		{
			get
			{
				return _shadow;
			}
			set
			{
				if (_shadow != null)
				{
					_shadow.Changed += _shadow_Changed;
				}
				if (value == null)
				{
					value = new DropShadow();
				}
				_shadow = value;
				_shadow.Changed += _shadow_Changed;
			}
		}

		public event EventHandler Changed;

		private void OnChanged(EventArgs e)
		{
			if (this.Changed != null)
			{
				this.Changed(this, e);
			}
		}

		public Skin()
		{
			_shadow.Changed += _shadow_Changed;
		}

		public Skin(Skin skin)
		{
			_downTextDeltaX = skin._downTextDeltaX;
			_downTextDeltaY = skin._downTextDeltaY;
			_edgeBottomHeight = skin._edgeBottomHeight;
			_edgeLeftWidth = skin._edgeLeftWidth;
			_edgeRightWidth = skin._edgeRightWidth;
			_edgeScalingBottom = skin._edgeScalingBottom;
			_edgeScalingCenter = skin._edgeScalingCenter;
			_edgeScalingLeft = skin._edgeScalingLeft;
			_edgeScalingRight = skin._edgeScalingRight;
			_edgeScalingTop = skin._edgeScalingTop;
			_edgeTopHeight = skin._edgeTopHeight;
			_image = skin._image;
			_textColor = skin._textColor;
			_shadow.Changed -= _shadow_Changed;
			_shadow = new DropShadow(skin.Shadow);
			_shadow.Changed += _shadow_Changed;
		}

		private void _shadow_Changed(object sender, EventArgs e)
		{
			OnChanged(e);
		}

		private bool ShouldSerializeShadow()
		{
			return _shadow.ShouldSerialize();
		}

		private void ResetShadow()
		{
			_shadow.Reset();
		}

		public override string ToString()
		{
			return "";
		}

		public bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			Check.NotNull(writer, "writer");
			if (!ShouldSerialize(true))
			{
				return false;
			}
			writer.WriteStartElement("Skin");
			if (ShouldSerialize(false))
			{
				if (_textColor != Color.Black)
				{
					writer.WriteAttributeString("tc", ImageEffects.ToRGBHex(_textColor));
				}
				if (_edgeLeftWidth != 1)
				{
					writer.WriteAttributeString("elw", XmlConvert.ToString(_edgeLeftWidth));
				}
				if (_edgeRightWidth != 1)
				{
					writer.WriteAttributeString("erw", XmlConvert.ToString(_edgeRightWidth));
				}
				if (_edgeTopHeight != 1)
				{
					writer.WriteAttributeString("eth", XmlConvert.ToString(_edgeTopHeight));
				}
				if (_edgeBottomHeight != 1)
				{
					writer.WriteAttributeString("ebh", XmlConvert.ToString(_edgeBottomHeight));
				}
				if (_edgeScalingLeft != 0)
				{
					writer.WriteAttributeString("els", XmlConvert.ToString((int)_edgeScalingLeft));
				}
				if (_edgeScalingRight != 0)
				{
					writer.WriteAttributeString("ers", XmlConvert.ToString((int)_edgeScalingRight));
				}
				if (_edgeScalingTop != 0)
				{
					writer.WriteAttributeString("ets", XmlConvert.ToString((int)_edgeScalingTop));
				}
				if (_edgeScalingBottom != 0)
				{
					writer.WriteAttributeString("ebs", XmlConvert.ToString((int)_edgeScalingBottom));
				}
				if (_edgeScalingCenter != 0)
				{
					writer.WriteAttributeString("ecs", XmlConvert.ToString((int)_edgeScalingCenter));
				}
				if (_textOffsetX != 0)
				{
					writer.WriteAttributeString("ox", XmlConvert.ToString(_textOffsetX));
				}
				if (_textOffsetY != 0)
				{
					writer.WriteAttributeString("oy", XmlConvert.ToString(_textOffsetY));
				}
				if (_downTextDeltaX != 1)
				{
					writer.WriteAttributeString("dox", XmlConvert.ToString(_downTextDeltaX));
				}
				if (_downTextDeltaY != 1)
				{
					writer.WriteAttributeString("doy", XmlConvert.ToString(_downTextDeltaY));
				}
				if (_imageEncoding != null || _imageUrl != null)
				{
					writer.WriteStartElement("Image");
					if (_imageUrl != null)
					{
						writer.WriteAttributeString("url", _imageUrl);
					}
					if (_imageUrl == null)
					{
						if (_imageEncoding == null)
						{
							using (MemoryStream memoryStream = new MemoryStream(_image.Width * _image.Height))
							{
								_image.Save(memoryStream, ImageFormat.Png);
								_imageEncoding = Convert.ToBase64String(memoryStream.ToArray());
							}
						}
						writer.WriteString(_imageEncoding);
					}
					writer.WriteEndElement();
				}
			}
			if (ShouldSerializeShadow())
			{
				_shadow.WriteToXml(writer, signing);
			}
			writer.WriteEndElement();
			return true;
		}

		public bool ShouldSerialize(bool includeChildren)
		{
			if (!(_textColor != Color.Black) && _edgeLeftWidth == 1 && _edgeRightWidth == 1 && _edgeTopHeight == 1 && _edgeBottomHeight == 1 && _edgeScalingLeft == EdgeScaling.Stretch && _edgeScalingRight == EdgeScaling.Stretch && _edgeScalingTop == EdgeScaling.Stretch && _edgeScalingBottom == EdgeScaling.Stretch && _edgeScalingCenter == EdgeScaling.Stretch && _textOffsetX == 0 && _textOffsetY == 0 && _downTextDeltaX == 1 && _downTextDeltaY == 1 && _imageEncoding == null && _imageUrl == null)
			{
				if (includeChildren && ShouldSerializeShadow())
				{
					return true;
				}
				return false;
			}
			return true;
		}

		public bool ReadFromXml(XmlReader reader)
		{
			Check.NotNull(reader, "reader");
			if (reader.Name != "Skin")
			{
				return false;
			}
			string attribute = reader.GetAttribute("tc");
			if (attribute != null)
			{
				_textColor = ImageEffects.FromRGBHex(attribute);
			}
			else
			{
				_textColor = Color.Black;
			}
			attribute = reader.GetAttribute("elw");
			if (attribute != null)
			{
				_edgeLeftWidth = SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_edgeLeftWidth = 1;
			}
			attribute = reader.GetAttribute("erw");
			if (attribute != null)
			{
				_edgeRightWidth = SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_edgeRightWidth = 1;
			}
			attribute = reader.GetAttribute("eth");
			if (attribute != null)
			{
				_edgeTopHeight = SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_edgeTopHeight = 1;
			}
			attribute = reader.GetAttribute("ebh");
			if (attribute != null)
			{
				_edgeBottomHeight = SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_edgeBottomHeight = 1;
			}
			attribute = reader.GetAttribute("els");
			if (attribute != null)
			{
				_edgeScalingLeft = (EdgeScaling)SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_edgeScalingLeft = EdgeScaling.Stretch;
			}
			attribute = reader.GetAttribute("ers");
			if (attribute != null)
			{
				_edgeScalingRight = (EdgeScaling)SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_edgeScalingRight = EdgeScaling.Stretch;
			}
			attribute = reader.GetAttribute("ets");
			if (attribute != null)
			{
				_edgeScalingTop = (EdgeScaling)SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_edgeScalingTop = EdgeScaling.Stretch;
			}
			attribute = reader.GetAttribute("ebs");
			if (attribute != null)
			{
				_edgeScalingBottom = (EdgeScaling)SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_edgeScalingBottom = EdgeScaling.Stretch;
			}
			attribute = reader.GetAttribute("ecs");
			if (attribute != null)
			{
				_edgeScalingCenter = (EdgeScaling)SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_edgeScalingCenter = EdgeScaling.Stretch;
			}
			attribute = reader.GetAttribute("ox");
			if (attribute != null)
			{
				_textOffsetX = SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_textOffsetX = 0;
			}
			attribute = reader.GetAttribute("oy");
			if (attribute != null)
			{
				_textOffsetY = SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_textOffsetY = 0;
			}
			attribute = reader.GetAttribute("dox");
			if (attribute != null)
			{
				_downTextDeltaX = SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_downTextDeltaX = 1;
			}
			attribute = reader.GetAttribute("doy");
			if (attribute != null)
			{
				_downTextDeltaY = SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_downTextDeltaY = 1;
			}
			_image = null;
			_imageUrl = null;
			_imageEncoding = null;
			if (reader.IsEmptyElement)
			{
				_shadow.Reset();
				reader.Read();
				return true;
			}
			reader.Read();
			while (!reader.EOF)
			{
				reader.MoveToContent();
				if (reader.IsStartElement())
				{
					switch (reader.Name)
					{
					case "Shadow":
						if (_shadow.ReadFromXml(reader))
						{
							break;
						}
						return false;
					case "Image":
						_imageUrl = reader.GetAttribute("url");
						if (_imageUrl != null)
						{
							_image = (Toolbox.GetImage(_imageUrl, null) as Bitmap);
							if (_image != null)
							{
								reader.Skip();
								break;
							}
							return false;
						}
						if (!reader.IsEmptyElement)
						{
							reader.Read();
							_imageEncoding = reader.ReadString();
							reader.Read();
							using (MemoryStream stream = new MemoryStream(Convert.FromBase64String(_imageEncoding)))
							{
								_image = (System.Drawing.Image.FromStream(stream) as Bitmap);
							}
							reader.Read();
							break;
						}
						return false;
					default:
						reader.Skip();
						break;
					}
					continue;
				}
				reader.Read();
				break;
			}
			return true;
		}

		public void MakeReadOnly()
		{
			MakeReadOnly(true);
		}

		internal void MakeReadOnly(bool readOnly)
		{
			_isReadOnly = readOnly;
		}
	}
}
