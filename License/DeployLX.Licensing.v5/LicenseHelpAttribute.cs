using System;
using System.Collections;
using System.Reflection;

namespace DeployLX.Licensing.v5
{
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
	public sealed class LicenseHelpAttribute : Attribute
	{
		internal SupportInfo _info = new SupportInfo();

		private static Hashtable _cachedInfo = new Hashtable();

		public string Email
		{
			get
			{
				return _info.Email;
			}
			set
			{
				_info.Email = value;
			}
		}

		public string Website
		{
			get
			{
				return _info.Website;
			}
			set
			{
				_info.Website = value;
			}
		}

		public string Company
		{
			get
			{
				return _info.Company;
			}
			set
			{
				_info.Company = value;
			}
		}

		public string Phone
		{
			get
			{
				return _info.Phone;
			}
			set
			{
				_info.Phone = value;
			}
		}

		public string Product
		{
			get
			{
				return _info.Product;
			}
			set
			{
				_info.Product = value;
			}
		}

		public string ProductVersion
		{
			get
			{
				return _info.ProductVersion;
			}
			set
			{
				_info.ProductVersion = value;
			}
		}

		public bool ShowFinalErrorReport
		{
			get
			{
				return _info.ShowFinalErrorReport;
			}
			set
			{
				_info.ShowFinalErrorReport = value;
			}
		}

		public string FailureReportUrl
		{
			get
			{
				return _info.FailureReportUrl;
			}
			set
			{
				_info.FailureReportUrl = value;
			}
		}

		public bool UseFormEffects
		{
			get
			{
				return _info.UseFormEffects;
			}
			set
			{
				_info.UseFormEffects = value;
			}
		}

		public bool IncludeDetails
		{
			get
			{
				return _info.IncludeDetails;
			}
			set
			{
				_info.IncludeDetails = value;
			}
		}

		public bool IncludeAssemblies
		{
			get
			{
				return _info.IncludeAssemblies;
			}
			set
			{
				_info.IncludeAssemblies = value;
			}
		}

		public bool IncludeSystemInfo
		{
			get
			{
				return _info.IncludeSystemInfo;
			}
			set
			{
				_info.IncludeSystemInfo = value;
			}
		}

		public bool DontCheckClock
		{
			get
			{
				return _info.DontCheckClock;
			}
			set
			{
				_info.DontCheckClock = value;
			}
		}

		public LicenseHelpAttribute()
		{
			_info.InitToNothing();
		}

		private static SupportInfo GetSupportInfo(Assembly theAssembly)
		{
			if (theAssembly != null)
			{
				object obj = _cachedInfo[theAssembly.FullName];
				if (obj != null)
				{
					return (SupportInfo)obj;
				}
			}
			SupportInfo supportInfo = new SupportInfo();
			if (theAssembly == null)
			{
				return supportInfo;
			}
			try
			{
				LicenseHelpAttribute licenseHelpAttribute = Attribute.GetCustomAttribute(theAssembly, typeof(LicenseHelpAttribute), false) as LicenseHelpAttribute;
				if (licenseHelpAttribute != null)
				{
					supportInfo = licenseHelpAttribute._info;
				}
				else
				{
					supportInfo.InitToNothing();
				}
				if (supportInfo.Company == null)
				{
					AssemblyCompanyAttribute assemblyCompanyAttribute = Attribute.GetCustomAttribute(theAssembly, typeof(AssemblyCompanyAttribute), false) as AssemblyCompanyAttribute;
					if (assemblyCompanyAttribute != null && assemblyCompanyAttribute.Company != null && assemblyCompanyAttribute.Company.Length > 0)
					{
						supportInfo.Company = assemblyCompanyAttribute.Company;
					}
					else
					{
						supportInfo.Company = SR.GetString("M_TheManufacturer");
					}
				}
			}
			catch
			{
				supportInfo.InitToNothing();
				supportInfo.Company = SR.GetString("M_TheManufacturer");
			}
			if (supportInfo.Product == null)
			{
				try
				{
					AssemblyProductAttribute assemblyProductAttribute = Attribute.GetCustomAttribute(theAssembly, typeof(AssemblyProductAttribute), true) as AssemblyProductAttribute;
					if (assemblyProductAttribute != null)
					{
						supportInfo.Product = assemblyProductAttribute.Product;
					}
					if (supportInfo.Product == null || supportInfo.Product.Length == 0)
					{
						AssemblyTitleAttribute assemblyTitleAttribute = Attribute.GetCustomAttribute(theAssembly, typeof(AssemblyTitleAttribute), true) as AssemblyTitleAttribute;
						if (assemblyTitleAttribute != null)
						{
							supportInfo.Product = assemblyTitleAttribute.Title;
						}
						if (supportInfo.Product == null || supportInfo.Product.Length == 0)
						{
							supportInfo.Product = theAssembly.GetName().Name;
						}
					}
				}
				catch
				{
				}
			}
			if (supportInfo.ProductVersion == null)
			{
				supportInfo.ProductVersion = TypeHelper.GetAssemblyVersion(theAssembly).ToString();
			}
			if (supportInfo.Phone != null && supportInfo.Phone.Length == 0)
			{
				supportInfo.Phone = null;
			}
			if (supportInfo.Website != null && supportInfo.Website.Length == 0)
			{
				supportInfo.Website = null;
			}
			if (supportInfo.Email != null && supportInfo.Email.Length == 0)
			{
				supportInfo.Email = null;
			}
			_cachedInfo[theAssembly.FullName] = supportInfo;
			return supportInfo;
		}

		public static SupportInfo GetSupportInfo(Type theType)
		{
			try
			{
				return GetSupportInfo((theType == null) ? null : theType.Assembly);
			}
			catch
			{
				return new SupportInfo();
			}
		}
	}
}
