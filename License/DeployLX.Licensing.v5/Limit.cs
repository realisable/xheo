using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security;
using System.Security.Permissions;
using System.Text;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	[ComVisible(true)]
	public abstract class Limit : IXmlPersistable, IDisposable, IComparable, ICloneable, ISerializable, IChange
	{
		[NonSerialized]
		private SecureLicense _license;

		internal string _loadReference;

		internal readonly string _instanceId = Guid.NewGuid().ToString();

		private ChangeEventHandler _changed;

		private bool _isDisposed;

		private string _nickname;

		private string _limitId = Guid.NewGuid().ToString("N");

		private string _uniqueId;

		private LimitCollection _limits;

		private ChangeEventHandler _bubbleHandler;

		internal LimitCollection _parentCollection;

		internal bool _wasValidated;

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		[Browsable(false)]
		public bool IsDisposed
		{
			get
			{
				return _isDisposed;
			}
		}

		[Browsable(false)]
		public abstract string Description
		{
			get;
		}

		[Browsable(false)]
		public virtual bool IsGui
		{
			get
			{
				if (HasChildLimits)
				{
					foreach (Limit item in (IEnumerable)Limits)
					{
						if (item.IsGui)
						{
							return true;
						}
					}
				}
				return false;
			}
		}

		[Browsable(false)]
		public abstract string Name
		{
			get;
		}

		public string Nickname
		{
			get
			{
				return _nickname;
			}
			set
			{
				if (!(_nickname == value))
				{
					_nickname = value;
					OnChanged("Nickname");
				}
			}
		}

		public string LimitId
		{
			get
			{
				return _limitId;
			}
			set
			{
				AssertNotReadOnly();
				if (_limitId != value)
				{
					if (value == null)
					{
						throw new ArgumentNullException(SR.GetString("E_PropertyCannotBeNull", "LimitId"));
					}
					_limitId = value;
					OnChanged("LimitId");
				}
			}
		}

		[Browsable(false)]
		public string UniqueId
		{
			get
			{
				if (_uniqueId == null)
				{
					_uniqueId = License.LicenseId + '.' + _limitId;
				}
				return _uniqueId;
			}
		}

		[Browsable(false)]
		public SecureLicense License
		{
			get
			{
				return _license;
			}
			set
			{
				AssertNotReadOnly();
				_license = value;
				OnChanged("License");
				OnLicenseAssigned(value);
			}
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		[Browsable(false)]
		public virtual string QualifiedTypeName
		{
			get
			{
				if (_loadReference != null)
				{
					return _loadReference;
				}
				return base.GetType().AssemblyQualifiedName;
			}
		}

		[Browsable(false)]
		public LimitCollection Limits
		{
			get
			{
				if (_limits == null)
				{
					_limits = new LimitCollection(_license, this);
					_limits.Changed += _limits_Changed;
				}
				if (_license != null && _license.IsReadOnly && !_limits.IsReadOnly)
				{
					_limits.MakeReadOnly();
				}
				return _limits;
			}
		}

		[Browsable(false)]
		public LimitCollection ParentCollection
		{
			get
			{
				return _parentCollection;
			}
		}

		[Browsable(false)]
		public bool HasChildLimits
		{
			get
			{
				if (_limits != null)
				{
					return _limits.Count > 0;
				}
				return false;
			}
		}

		[Browsable(false)]
		public virtual string SummaryText
		{
			get
			{
				return null;
			}
		}

		[Browsable(false)]
		public string NameAndSummary
		{
			get
			{
				string displayName = DisplayName;
				if (SummaryText == null)
				{
					return displayName;
				}
				return string.Format("{0}, ({1})", displayName, SummaryText);
			}
		}

		[Browsable(false)]
		public string DisplayName
		{
			get
			{
				return (_nickname == null) ? Name : ((_nickname.IndexOf(Name, StringComparison.OrdinalIgnoreCase) == -1) ? string.Format("{0} ({1})", _nickname, Name) : _nickname);
			}
		}

		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool WasValidated
		{
			get
			{
				return _wasValidated;
			}
		}

		[Browsable(false)]
		public virtual string State
		{
			get
			{
				return null;
			}
		}

		[Browsable(false)]
		public virtual IList<string> ValidationStates
		{
			get
			{
				return null;
			}
		}

		event ChangeEventHandler IChange.Changed
		{
			add
			{
				lock (this)
				{
					_changed = (ChangeEventHandler)Delegate.Combine(_changed, value);
				}
			}
			remove
			{
				lock (this)
				{
					_changed = (ChangeEventHandler)Delegate.Remove(_changed, value);
				}
			}
		}

		protected void OnCollectionChanged(object sender, string property, CollectionEventArgs e)
		{
			if (_changed != null)
			{
				ChangeEventArgs e2 = new ChangeEventArgs(property, this, e.Element, e.Action);
				_changed(this, e2);
			}
			if (e.Element is IChange && _license != null)
			{
				License.AttachChangeBubbler(e);
			}
		}

		protected void OnChanged(string property)
		{
			if (_changed != null)
			{
				ChangeEventArgs e = new ChangeEventArgs(property, this);
				_changed(this, e);
			}
		}

		protected void OnChanged(ChangeEventArgs e)
		{
			if (_changed != null)
			{
				_changed(this, e);
			}
		}

		protected Limit()
		{
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			_isDisposed = true;
			if (HasChildLimits)
			{
				foreach (Limit item in (IEnumerable)Limits)
				{
					item.Dispose(disposing);
				}
			}
		}

		internal void DisposeInternal(bool disposing)
		{
			Dispose(disposing);
		}

		~Limit()
		{
			Dispose(false);
		}

		protected virtual void OnLicenseAssigned(SecureLicense value)
		{
			if (_limits != null)
			{
				_limits.License = value;
				foreach (Limit item in (IEnumerable)_limits)
				{
					item.License = value;
				}
			}
		}

		private void _limits_Changed(object sender, CollectionEventArgs e)
		{
			OnCollectionChanged(sender, "Limits", e);
			IChange change = e.Element as IChange;
			if (e.Action == CollectionChangeAction.Add)
			{
				if (_bubbleHandler == null)
				{
					_bubbleHandler = BubbleChanged;
				}
				change.Changed += _bubbleHandler;
			}
			else if (e.Action == CollectionChangeAction.Remove)
			{
				change.Changed -= _bubbleHandler;
			}
		}

		private void BubbleChanged(object sender, ChangeEventArgs e)
		{
			OnChanged(e);
		}

		[DebuggerHidden]
		[DebuggerStepThrough]
		public virtual ValidationResult Validate(SecureLicenseContext context)
		{
			Check.NotNull(context, "context");
			return CheckChildLimits(context);
		}

		private ValidationResult CheckChildLimits(SecureLicenseContext context)
		{
			Check.NotNull(context, "context");
			if (!HasChildLimits)
			{
				return ValidationResult.Valid;
			}
			return _limits.Validate(context);
		}

		public virtual ValidationResult Granted(SecureLicenseContext context)
		{
			Check.NotNull(context, "context");
			return GrantChildLimits(context);
		}

		private ValidationResult GrantChildLimits(SecureLicenseContext context)
		{
			if (HasChildLimits)
			{
				foreach (Limit item in (IEnumerable)_limits)
				{
					context.WriteDiagnostic("Granting limit {0} [{1}]", item, item.LimitId);
					ValidationResult validationResult = item.Granted(context);
					context.WriteDiagnostic("\tLimit {0} [{1}] returned {2}", item, item.LimitId, validationResult);
					if (validationResult != ValidationResult.Valid)
					{
						return validationResult;
					}
					item._wasValidated = true;
				}
			}
			return ValidationResult.Valid;
		}

		public virtual PeekResult Peek(SecureLicenseContext context)
		{
			return PeekChildLimits(context);
		}

		private PeekResult PeekChildLimits(SecureLicenseContext context)
		{
			if (HasChildLimits)
			{
				return _limits.Peek(context);
			}
			return PeekResult.Valid;
		}

		internal bool BaseWriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			Check.NotNull(writer, "writer");
			writer.WriteStartElement("Limit");
			writer.WriteAttributeString("type", GetLimitName());
			if ((_loadReference != null || base.GetType().Assembly.FullName != typeof(Limit).Assembly.FullName) && QualifiedTypeName != null)
			{
				writer.WriteAttributeString("netType", QualifiedTypeName);
			}
			writer.WriteAttributeString("id", _limitId);
			if (signing == LicenseSaveType.Normal && _nickname != null && _nickname.Length > 0)
			{
				writer.WriteAttributeString("nickname", _nickname);
			}
			if (!WriteToXml(writer, signing))
			{
				return false;
			}
			if (HasChildLimits)
			{
				foreach (Limit item in (IEnumerable)_limits)
				{
					if (!item.BaseWriteToXml(writer, signing))
					{
						return false;
					}
				}
			}
			writer.WriteEndElement();
			return true;
		}

		protected abstract bool WriteToXml(XmlWriter writer, LicenseSaveType signing);

		bool IXmlPersistable.WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			return BaseWriteToXml(writer, signing);
		}

		internal bool BaseReadFromXml(XmlReader reader)
		{
			Check.NotNull(reader, "reader");
			reader.MoveToContent();
			if (reader.Name != "Limit")
			{
				return false;
			}
			string attribute = reader.GetAttribute("type");
			if (attribute == null)
			{
				throw new SecureLicenseException("E_MissingXmlAttribute", "type", "Limit");
			}
			if (string.Compare(attribute, GetLimitName(), false, CultureInfo.InvariantCulture) != 0)
			{
				throw new SecureLicenseException("E_LimitValueMismatch", attribute, "type");
			}
			_limitId = reader.GetAttribute("id");
			if (_limitId == null)
			{
				_limitId = Guid.NewGuid().ToString("N");
			}
			_nickname = reader.GetAttribute("nickname");
			if (!ReadFromXml(reader))
			{
				return false;
			}
			return true;
		}

		protected abstract bool ReadFromXml(XmlReader reader);

		bool IXmlPersistable.ReadFromXml(XmlReader reader)
		{
			return BaseReadFromXml(reader);
		}

		protected bool ReadChildLimit(XmlReader reader)
		{
			Limit limit = ReadLimit(reader, _license);
			if (limit != null)
			{
				Limits.Add(limit);
				return true;
			}
			return false;
		}

		protected bool ReadChildLimits(XmlReader reader)
		{
			if (reader.IsEmptyElement)
			{
				reader.Read();
				return true;
			}
			reader.Read();
			reader.MoveToContent();
			while (!reader.EOF)
			{
				if (reader.IsStartElement())
				{
					if (reader.Name == "Limit")
					{
						if (ReadChildLimit(reader))
						{
							continue;
						}
						return false;
					}
					reader.Read();
					continue;
				}
				reader.Read();
				break;
			}
			return true;
		}

		public static Limit ReadLimit(XmlReader reader, SecureLicense license)
		{
			reader.MoveToContent();
			if (reader.IsStartElement() && reader.Name == "Limit")
			{
				string attribute = reader.GetAttribute("netType");
				if (attribute == null)
				{
					attribute = reader.GetAttribute("type");
				}
				if (attribute == null)
				{
					return null;
				}
				Limit limit = CreateLimitFromName(attribute);
				if (limit != null)
				{
					limit.License = license;
				}
				if (limit != null && limit.BaseReadFromXml(reader))
				{
					return limit;
				}
				return null;
			}
			return null;
		}

		protected internal virtual string GetLimitName()
		{
			string name = base.GetType().Name;
			if (name.EndsWith("Limit"))
			{
				return name.Substring(0, name.Length - 5);
			}
			return Name.Replace(" ", "");
		}

		public virtual string GetNameAndSummary(int maxSummary)
		{
			string displayName = DisplayName;
			string text = SummaryText;
			if (text == null)
			{
				return displayName;
			}
			if (text.Length > maxSummary)
			{
				text = ((maxSummary > 3) ? (text.Substring(0, maxSummary - 3) + "...") : text.Substring(0, maxSummary));
			}
			return string.Format("{0}, ({1})", displayName, text);
		}

		protected void AssertNotReadOnly()
		{
			if (_license != null)
			{
				_license.AssertNotReadOnly();
			}
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public int GetOrdinal()
		{
			int num = 0;
			if (_license == null)
			{
				return -1;
			}
			return GetOrdinal(_license.Limits, this, ref num);
		}

		private static int GetOrdinal(LimitCollection limits, Limit limit, ref int ordinal)
		{
			foreach (Limit item in (IEnumerable)limits)
			{
				if (item == limit)
				{
					return ordinal;
				}
				ordinal++;
				if (item.HasChildLimits)
				{
					int ordinal2 = GetOrdinal(item.Limits, limit, ref ordinal);
					if (ordinal2 == -1)
					{
						continue;
					}
					return ordinal2;
				}
			}
			return -1;
		}

		public static Limit CreateLimitFromName(string name)
		{
			Check.NotNull(name, "name");
			switch (name)
			{
			case "Activation":
				return new ActivationLimit();
			case "Application":
				return new ApplicationLimit();
			case "ApplicationType":
				return new ApplicationTypeLimit();
			case "Beta":
				return new BetaLimit();
			case "Floating":
				throw new NotSupportedException("E_FloatingNotSupported");
			case "Designtime":
				return new DesigntimeLimit();
			case "Runtime":
				return new RuntimeLimit();
			case "DomainName":
				return new DomainNameLimit();
			case "Feature":
				return new FeatureLimit();
			case "FeatureFilter":
				return new FeatureFilterLimit();
			case "HtmlBranded":
				return new HtmlBrandedLimit();
			case "Invalid":
				return new InvalidLimit();
			case "IPAddress":
				return new IPAddressLimit();
			case "LicenseServer":
				return new LicenseServerLimit();
			case "Online":
				return new OnlineLimit();
			case "Or":
				return new OrLimit();
			case "OS":
				return new OSLimit();
			case "Publisher":
				return new PublisherLimit();
			case "Registration":
				return new RegistrationLimit();
			case "RemoteDesktop":
				return new RemoteDesktopLimit();
			case "Reset":
				return new ResetLimit();
			case "Script":
				return new ScriptLimit();
			case "Service":
				return new ServiceLimit();
			case "Sessions":
				return new SessionsLimit();
			case "Splash":
				return new SplashLimit();
			case "State":
				return new StateLimit();
			case "Time":
				return new TimeLimit();
			case "TimeServer":
				return new TimeServerLimit();
			case "Trial":
				return new TrialLimit();
			case "Type":
				return new TypeLimit();
			case "Upgrade":
				return new UpgradeLimit();
			case "Use":
				return new UseLimit();
			case "Version":
				return new VersionLimit();
			case "VirtualMachine":
				return new VirtualMachineLimit();
			default:
			{
				Limit limit = null;
				bool flag = false;
				try
				{
					Type type = null;
					if (name.IndexOf('.') == -1)
					{
						Assembly executingAssembly = Assembly.GetExecutingAssembly();
						type = executingAssembly.GetType(typeof(Limit).Namespace + "." + name + "Limit", false, true);
						if (type == null)
						{
							type = Type.GetType(typeof(Limit).Namespace + "." + name + "Limit," + Assembly.GetExecutingAssembly().GetName().Name, false, true);
						}
					}
					if (type == null)
					{
						if (name.StartsWith("DeployLX.Licensing.Nlm.v4"))
						{
							string text = name.Replace(".v4", ".v5");
							text = text.Replace("4.0.3500.0", "5.0.3500.0");
							text = text.Replace("4.1.3500.0", "5.0.3500.0");
							if (Environment.Version.Major == 4 && Environment.Version.Minor == 0)
							{
								string typeName = text.Replace("5.0.3500.0", "5.0.4000.0");
								type = (TypeHelper.FindType(typeName, false) ?? TypeHelper.FindType(text, true));
							}
							else
							{
								type = TypeHelper.FindType(text, true);
							}
						}
						else
						{
							type = TypeHelper.FindType(name, true);
						}
						flag = true;
					}
					object obj = Activator.CreateInstance(type);
					limit = (obj as Limit);
					if (flag && limit != null)
					{
						limit._loadReference = name;
					}
					if (limit == null)
					{
						if (obj != null)
						{
							Type type2 = type;
							while (type2 != null)
							{
								if (!(type2.FullName == "DeployLX.Licensing.v3.Limit"))
								{
									type2 = type2.BaseType;
									continue;
								}
								throw new SecureLicenseException("E_3xCustomLimit", type);
							}
						}
						throw new SecureLicenseException("E_LimitDoesNotExist", name);
					}
					return limit;
				}
				catch (LicenseException)
				{
					throw;
				}
				catch (SecureLicenseException)
				{
					throw;
				}
				catch (Exception innerException)
				{
					throw new SecureLicenseException("E_LimitDoesNotExist", innerException, name);
				}
			}
			}
		}

		protected object GetPersistentData(string key, object defaultValue, PersistentDataLocationType location)
		{
			if (License == null)
			{
				if (defaultValue != null)
				{
					return defaultValue;
				}
				throw new SecureLicenseException("E_PropertyCannotBeNull", "License");
			}
			object persistentData = License.GetPersistentData(this, key, location);
			SecureLicenseContext.WriteDiagnosticToContext("GetPersistentData( key = {0}, defaultValue = {1}, location = {2} ) == {3}", key, defaultValue, location, persistentData);
			if (persistentData == null)
			{
				return defaultValue;
			}
			return persistentData;
		}

		protected object GetPersistentData(string key, PersistentDataLocationType location)
		{
			return GetPersistentData(key, null, location);
		}

		protected object GetPersistentData(string key)
		{
			return GetPersistentData(key, null, PersistentDataLocationType.SharedOrUser);
		}

		protected void SetPersistentData(string key, object value, PersistentDataLocationType location)
		{
			if (License == null)
			{
				throw new SecureLicenseException("E_PropertyCannotBeNull", "License");
			}
			if (value != null)
			{
				Type type = value.GetType();
				if (!type.IsPrimitive && !(value is string) && (!type.IsArray || (!type.GetElementType().IsPrimitive && type.GetElementType() != typeof(string))))
				{
					throw new SecureLicenseException("E_PersistentDataMustBePrimitive");
				}
			}
			License.SetPersistentData(this, key, value, location);
		}

		protected void SetPersistentData(string key, object value)
		{
			SetPersistentData(key, value, PersistentDataLocationType.SharedOrUser);
		}

		void IChange.MakeReadOnly()
		{
			MakeReadOnly();
		}

		protected internal virtual void MakeReadOnly()
		{
			if (HasChildLimits)
			{
				foreach (Limit item in (IEnumerable)_limits)
				{
					item.MakeReadOnly();
				}
			}
		}

		protected void AssertMinimumVersion(Version minVersion)
		{
			if (License != null && License.Version < minVersion)
			{
				throw new SecureLicenseException("E_LimitRequiresVersion", Name, minVersion);
			}
		}

		protected void AssertFeatureVersion(Version minVersion, string feature)
		{
			if (License != null && License.Version < minVersion)
			{
				throw new SecureLicenseException("E_LimitFeatureRequiresVersion", feature, Name, minVersion);
			}
		}

		public override string ToString()
		{
			return NameAndSummary;
		}

		public virtual int CompareTo(Limit limit)
		{
			if (!base.GetType().IsAssignableFrom(limit.GetType()))
			{
				if (limit.License == License && License != null)
				{
					return License.Limits.CalculateOrdinal(this).CompareTo(License.Limits.CalculateOrdinal(limit));
				}
				return Name.CompareTo(limit.Name);
			}
			StringBuilder stringBuilder = new StringBuilder();
			StringBuilder stringBuilder2 = new StringBuilder();
			using (StringWriter w = new StringWriter(stringBuilder))
			{
				XmlTextWriter xmlTextWriter = new XmlTextWriter(w);
				BaseWriteToXml(xmlTextWriter, LicenseSaveType.Normal);
				xmlTextWriter.Close();
			}
			using (StringWriter w2 = new StringWriter(stringBuilder2))
			{
				XmlTextWriter xmlTextWriter2 = new XmlTextWriter(w2);
				limit.BaseWriteToXml(xmlTextWriter2, LicenseSaveType.Normal);
				xmlTextWriter2.Close();
			}
			StringBuilder stringBuilder3 = stringBuilder;
			while (true)
			{
				string text = stringBuilder3.ToString();
				for (int num = text.IndexOf(" id=\""); num > -1; num = text.IndexOf(" id=\"", num))
				{
					int num2 = text.IndexOf("\"", num + 5);
					if (num2 != -1)
					{
						stringBuilder3.Remove(num, num2 - num + 1);
						text = stringBuilder3.ToString();
					}
					else
					{
						num += 5;
					}
				}
				if (stringBuilder3 == stringBuilder2)
				{
					break;
				}
				stringBuilder3 = stringBuilder2;
			}
			return stringBuilder.ToString().CompareTo(stringBuilder2.ToString());
		}

		int IComparable.CompareTo(object obj)
		{
			if (obj == null)
			{
				return 1;
			}
			if (!(obj is Limit))
			{
				throw new InvalidCastException();
			}
			return CompareTo(obj as Limit);
		}

		object ICloneable.Clone()
		{
			return InnerClone();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		protected virtual Limit InnerClone()
		{
			Limit limit = Activator.CreateInstance(base.GetType()) as Limit;
			StringBuilder stringBuilder = new StringBuilder();
			using (StringWriter w = new StringWriter(stringBuilder))
			{
				XmlTextWriter xmlTextWriter = new XmlTextWriter(w);
				BaseWriteToXml(xmlTextWriter, LicenseSaveType.Normal);
				xmlTextWriter.Close();
			}
			XmlTextReader xmlTextReader = new XmlTextReader(stringBuilder.ToString(), XmlNodeType.Element, null);
			limit.BaseReadFromXml(xmlTextReader);
			xmlTextReader.Close();
			GenerateNewLimitId(limit);
			Dictionary<int, Limit> dictionary = new Dictionary<int, Limit>();
			int num = 0;
			MapOrdinals(dictionary, this, ref num);
			Dictionary<int, Limit> dictionary2 = new Dictionary<int, Limit>();
			num = 0;
			MapOrdinals(dictionary2, limit, ref num);
			Dictionary<Limit, Limit> dictionary3 = new Dictionary<Limit, Limit>();
			foreach (KeyValuePair<int, Limit> item in dictionary)
			{
				dictionary3[dictionary2[item.Key]] = item.Value;
			}
			foreach (KeyValuePair<Limit, Limit> item2 in dictionary3)
			{
				item2.Key.ResolveCloneLinks(dictionary3);
			}
			return limit;
		}

		private void MapOrdinals(IDictionary<int, Limit> limits, Limit limit, ref int ox)
		{
			limits[ox++] = limit;
			if (limit.HasChildLimits)
			{
				foreach (Limit item in (IEnumerable)limit.Limits)
				{
					MapOrdinals(limits, item, ref ox);
				}
			}
		}

		protected virtual void ResolveCloneLinks(IDictionary<Limit, Limit> clones)
		{
		}

		private void GenerateNewLimitId(Limit root)
		{
			root.LimitId = Guid.NewGuid().ToString("N");
			if (root.HasChildLimits)
			{
				foreach (Limit item in (IEnumerable)root.Limits)
				{
					GenerateNewLimitId(item);
				}
			}
		}

		[SecurityCritical]
		[SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			using (StringWriter stringWriter = new StringWriter())
			{
				XmlTextWriter writer = new XmlTextWriter(stringWriter);
				BaseWriteToXml(writer, LicenseSaveType.Normal);
				info.AddValue("Limit", stringWriter.ToString());
			}
		}

		private Limit(SerializationInfo info, StreamingContext context)
		{
			XmlTextReader reader = new XmlTextReader(info.GetString("Limit"), XmlNodeType.Element, null);
			BaseReadFromXml(reader);
		}
	}
}
