namespace DeployLX.Licensing.v5
{
	public enum MachineProfileEntryType
	{
		NotSet,
		Cpu,
		Memory,
		MACAddress,
		SystemDrive,
		CDRom,
		VideoCard,
		Scsi,
		Ide,
		Motherboard,
		Custom1 = 100,
		Custom2,
		Custom3,
		Custom4,
		TypeMask = 0xFFFFFFF,
		PartialFlag = 0x40000000
	}
}
