using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public sealed class SuperFormTheme : IXmlPersistable, IChange
	{
		public static readonly Color DefaultBaseColor = Color.FromArgb(55, 106, 162);

		private readonly ImageBag _images = new ImageBag();

		private object _formIcon;

		private string _formIconLocation;

		private readonly ColorBag _colors = new ColorBag();

		private Skin _buttonSkin;

		private readonly PropertyBag _properties = new PropertyBag();

		private int _updateCount;

		private bool _isReadOnly;

		public ImageBag Images
		{
			get
			{
				return _images;
			}
		}

		public Icon FormIcon
		{
			get
			{
				return (_formIcon ?? (_formIcon = ResolveFormIcon())) as Icon;
			}
		}

		public string FormIconLocation
		{
			get
			{
				return _formIconLocation;
			}
			set
			{
				if (!(_formIconLocation == value) && !_isReadOnly)
				{
					_formIconLocation = value;
					_formIcon = null;
					OnChanged(new ChangeEventArgs("FormIconLocation", this));
				}
			}
		}

		public ColorBag Colors
		{
			get
			{
				return _colors;
			}
		}

		public Skin ButtonSkin
		{
			get
			{
				return _buttonSkin;
			}
			set
			{
				if (_isReadOnly)
				{
					throw new InvalidOperationException(SR.GetString("E_ReadOnlyObject", "Theme"));
				}
				if (_buttonSkin != null)
				{
					_buttonSkin.Changed -= _buttonSkin_Changed;
				}
				_buttonSkin = value;
				if (value != null)
				{
					_buttonSkin.Changed += _buttonSkin_Changed;
				}
			}
		}

		public PropertyBag Properties
		{
			get
			{
				return _properties;
			}
		}

		public event ChangeEventHandler Changed;

		private void OnChanged(ChangeEventArgs e)
		{
			if (this.Changed != null && _updateCount == 0)
			{
				this.Changed(this, e);
			}
		}

		public SuperFormTheme()
		{
			_images.Changed += _images_Changed;
			_colors.Changed += _colors_Changed;
			_properties.Changed += _properties_Changed;
		}

		private void _properties_Changed(object sender, ChangeEventArgs e)
		{
			e.BubbleStack.Push(sender);
			e.Name = "Properties";
			OnChanged(e);
		}

		private void _colors_Changed(object sender, ChangeEventArgs e)
		{
			e.BubbleStack.Push(sender);
			e.Name = "Colors";
			OnChanged(e);
		}

		private void _images_Changed(object sender, ChangeEventArgs e)
		{
			e.BubbleStack.Push(sender);
			e.Name = "Images";
			OnChanged(e);
		}

		public SuperFormTheme(Color baseColor)
			: this()
		{
			SetBaseColor(baseColor, true);
		}

		public SuperFormTheme(SuperFormTheme theme, Color newBase)
			: this()
		{
			IDictionaryEnumerator enumerator = theme._colors.Dictionary.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					DictionaryEntry dictionaryEntry = (DictionaryEntry)enumerator.Current;
					_colors.Dictionary.Add(dictionaryEntry.Key, dictionaryEntry.Value);
				}
			}
			finally
			{
				IDisposable disposable = enumerator as IDisposable;
				if (disposable != null)
				{
					disposable.Dispose();
				}
			}
			IDictionaryEnumerator enumerator2 = theme._images.Dictionary.GetEnumerator();
			try
			{
				while (enumerator2.MoveNext())
				{
					DictionaryEntry dictionaryEntry2 = (DictionaryEntry)enumerator2.Current;
					_images.Dictionary.Add(dictionaryEntry2.Key, dictionaryEntry2.Value);
				}
			}
			finally
			{
				IDisposable disposable2 = enumerator2 as IDisposable;
				if (disposable2 != null)
				{
					disposable2.Dispose();
				}
			}
			IDictionaryEnumerator enumerator3 = theme._properties.Dictionary.GetEnumerator();
			try
			{
				while (enumerator3.MoveNext())
				{
					DictionaryEntry dictionaryEntry3 = (DictionaryEntry)enumerator3.Current;
					_properties.Dictionary.Add(dictionaryEntry3.Key, dictionaryEntry3.Value);
				}
			}
			finally
			{
				IDisposable disposable3 = enumerator3 as IDisposable;
				if (disposable3 != null)
				{
					disposable3.Dispose();
				}
			}
			if (theme._buttonSkin != null)
			{
				_buttonSkin = new Skin(theme._buttonSkin);
			}
			if (newBase != Color.Empty)
			{
				SetBaseColor(newBase, false);
			}
			_formIconLocation = theme._formIconLocation;
		}

		private object ResolveFormIcon()
		{
			if (_formIconLocation != null)
			{
				string formIconLocation = _formIconLocation;
				using (Stream stream = Toolbox.GetResource(ref formIconLocation, null))
				{
					if (stream != null)
					{
						return new Icon(stream);
					}
				}
			}
			return DeployLX.Licensing.v5.Images.Shield_ico;
		}

		private void _buttonSkin_Changed(object sender, EventArgs e)
		{
			OnChanged(new ChangeEventArgs("ButtonSkin", this));
		}

		public void BeginUpdate()
		{
			Interlocked.Increment(ref _updateCount);
		}

		public void EndUpdate()
		{
			int num = Interlocked.Decrement(ref _updateCount);
			if (num < 0)
			{
				throw new InvalidOperationException(SR.GetString("E_UnbalancedUpdate"));
			}
			if (num == 0)
			{
				OnChanged(new ChangeEventArgs("Theme", this, "[ALL]", CollectionChangeAction.Refresh));
			}
		}

		public void SetBaseColor(Color color, bool overwriteExisting)
		{
			_colors.SetBaseColor(color, overwriteExisting);
		}

		public void MakeReadOnly()
		{
			_images.MakeReadOnly();
			_colors.MakeReadOnly();
			_properties.MakeReadOnly();
			if (_buttonSkin != null)
			{
				_buttonSkin.MakeReadOnly();
			}
			_isReadOnly = true;
		}

		public bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			Check.NotNull(writer, "writer");
			if (!ShouldSerialize())
			{
				return true;
			}
			writer.WriteStartElement("Theme");
			if (_formIconLocation != null)
			{
				writer.WriteAttributeString("formIconLocation", _formIconLocation);
			}
			_colors.WriteToXml(writer, signing);
			_images.WriteToXml(writer, signing);
			_properties.WriteToXml(writer, signing);
			if (_buttonSkin != null)
			{
				_buttonSkin.WriteToXml(writer, signing);
			}
			writer.WriteEndElement();
			return true;
		}

		public bool ShouldSerialize()
		{
			if (!_colors.ShouldSerialize() && !_images.ShouldSerialize() && !_properties.ShouldSerialize() && (_buttonSkin == null || !_buttonSkin.ShouldSerialize(true)) && _formIconLocation == null)
			{
				return false;
			}
			return true;
		}

		public bool ReadFromXml(XmlReader reader)
		{
			if (_isReadOnly)
			{
				throw new InvalidOperationException(SR.GetString("E_ReadOnlyObject", "Theme"));
			}
			if (_buttonSkin != null)
			{
				_buttonSkin.Changed -= _buttonSkin_Changed;
			}
			_buttonSkin = null;
			_colors.Dictionary.Clear();
			_images.Dictionary.Clear();
			_properties.Dictionary.Clear();
			if (_buttonSkin != null)
			{
				_buttonSkin.Changed -= _buttonSkin_Changed;
				_buttonSkin = null;
			}
			reader.MoveToContent();
			_formIconLocation = reader.GetAttribute("formIconLocation");
			if (reader.Name == "Theme" && !reader.IsEmptyElement)
			{
				reader.Read();
				while (!reader.EOF)
				{
					reader.MoveToContent();
					if (reader.IsStartElement())
					{
						switch (reader.Name)
						{
						case "Skin":
							_buttonSkin = new Skin();
							if (_buttonSkin.ReadFromXml(reader))
							{
								_buttonSkin.Changed += _buttonSkin_Changed;
								break;
							}
							return false;
						case "Properties":
							if (_properties.ReadFromXml(reader))
							{
								break;
							}
							return false;
						case "Images":
							if (_images.ReadFromXml(reader))
							{
								break;
							}
							return false;
						case "Colors":
							if (_colors.ReadFromXml(reader))
							{
								break;
							}
							return false;
						default:
							reader.Skip();
							break;
						}
						continue;
					}
					reader.Read();
					break;
				}
			}
			else
			{
				reader.Read();
			}
			Color color = _colors["Base"];
			if (color == Color.Empty)
			{
				_colors.SetBaseColor(DefaultBaseColor, false);
			}
			else
			{
				_colors.SetBaseColor(color, false);
			}
			return true;
		}
	}
}
