namespace DeployLX.Licensing.v5
{
	public delegate bool ShouldValidateLicenseFilter(SecureLicense license, SecureLicenseContext context);
}
