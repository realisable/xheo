using System.Collections;
using System.Text;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[LimitEditor("StateLimitEditor", IconResource = "asmres://DeployLX.Licensing.DlxAddIn.v5/DeployLX.Licensing.DlxAddIn.v5.Resources.Limits.Filter.png", Category = "State and Filters")]
	public class StateLimit : Limit
	{
		private readonly StringCollection _states;

		private bool _multiStatesMustAllMatch;

		public override string Description
		{
			get
			{
				return SR.GetString("M_StateLimitDescription");
			}
		}

		public override string Name
		{
			get
			{
				return "State";
			}
		}

		public StringCollection States
		{
			get
			{
				return _states;
			}
		}

		public bool MultiStatesMustAllMatch
		{
			get
			{
				return _multiStatesMustAllMatch;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_multiStatesMustAllMatch != value)
				{
					_multiStatesMustAllMatch = value;
					base.OnChanged("MultiStatesMustAllMatch");
				}
			}
		}

		public override string SummaryText
		{
			get
			{
				if (_states.Count == 0)
				{
					return "";
				}
				StringBuilder stringBuilder = new StringBuilder();
				for (int i = 0; i < _states.Count; i++)
				{
					if (stringBuilder.Length > 0)
					{
						if (_states.Count > 2 && i != _states.Count - 1)
						{
							stringBuilder.Append(", ");
						}
						else if (_multiStatesMustAllMatch)
						{
							stringBuilder.Append(" and ");
						}
						else
						{
							stringBuilder.Append(" or ");
						}
					}
					stringBuilder.Append(_states[i]);
				}
				return stringBuilder.ToString();
			}
		}

		public StateLimit()
		{
			_states = new StringCollection();
			_states.Changed += _states_Changed;
		}

		protected internal override void MakeReadOnly()
		{
			base.MakeReadOnly();
			_states.MakeReadOnly();
		}

		public override ValidationResult Validate(SecureLicenseContext context)
		{
			Limit limit = base.ParentCollection.Owner as Limit;
			if (limit == null)
			{
				return context.ReportError("E_StateParentNotLimit", this);
			}
			if (!IsLimitInDesiredState(context, limit))
			{
				return ValidationResult.Valid;
			}
			return base.Validate(context);
		}

		protected bool IsLimitInDesiredState(SecureLicenseContext context, Limit limit)
		{
			if (limit.State == null)
			{
				return false;
			}
			if (_states.Contains(limit.State))
			{
				return true;
			}
			string[] array = limit.State.Split(';');
			if (array.Length == 1)
			{
				return false;
			}
			if (_multiStatesMustAllMatch)
			{
				string[] array2 = array;
				int num = 0;
				while (true)
				{
					if (num < array2.Length)
					{
						string text = array2[num];
						if (!_states.Contains(text.Trim()))
						{
							break;
						}
						num++;
						continue;
					}
					return true;
				}
				return false;
			}
			string[] array3 = array;
			int num2 = 0;
			while (true)
			{
				if (num2 < array3.Length)
				{
					string text2 = array3[num2];
					if (_states.Contains(text2.Trim()))
					{
						break;
					}
					num2++;
					continue;
				}
				return false;
			}
			return true;
		}

		public override ValidationResult Granted(SecureLicenseContext context)
		{
			Limit limit = base.ParentCollection.Owner as Limit;
			if (limit == null)
			{
				return context.ReportError("E_StateParentNotLimit", this);
			}
			if (!IsLimitInDesiredState(context, limit))
			{
				return ValidationResult.Valid;
			}
			return base.Granted(context);
		}

		protected override bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			if (signing == LicenseSaveType.Signing)
			{
				base.AssertMinimumVersion(SecureLicense.v4_0);
				Limit limit = base.ParentCollection.Owner as Limit;
				if (limit == null)
				{
					throw new SecureLicenseException("E_StateParentNotLimit");
				}
				if (limit.ValidationStates == null)
				{
					throw new SecureLicenseException("E_LimitDoesNotSupportMultipleStates", limit.Name);
				}
				if (_states.Count == 0)
				{
					throw new SecureLicenseException("E_MissingStates");
				}
				foreach (string item in (IEnumerable)_states)
				{
					if (limit.ValidationStates.Contains(item))
					{
						continue;
					}
					throw new SecureLicenseException("E_UnkownState", limit.Name, item);
				}
			}
			if (_multiStatesMustAllMatch)
			{
				writer.WriteAttributeString("multiStatesMustAllMatch", "true");
			}
			_states.WriteToXml(writer, "States", "State", "name");
			return true;
		}

		protected override bool ReadFromXml(XmlReader reader)
		{
			_states.Clear();
			_multiStatesMustAllMatch = (reader.GetAttribute("multiStatesMustAllMatch") == "true");
			if (reader.IsEmptyElement)
			{
				reader.Read();
				return true;
			}
			reader.Read();
			while (!reader.EOF)
			{
				if (reader.IsStartElement())
				{
					switch (reader.Name)
					{
					case "Limit":
						if (base.ReadChildLimit(reader))
						{
							break;
						}
						return false;
					case "States":
						if (_states.ReadFromXml(reader, "States", "State", "name", false))
						{
							break;
						}
						return false;
					default:
						reader.Skip();
						break;
					}
					continue;
				}
				reader.Read();
				break;
			}
			return true;
		}

		private void _states_Changed(object sender, CollectionEventArgs e)
		{
			base.OnCollectionChanged(sender, "States", e);
		}

		public override string GetNameAndSummary(int maxSummary)
		{
			string summaryText = SummaryText;
			string displayName = base.DisplayName;
			if (displayName == Name && summaryText != null && summaryText.Length != 0)
			{
				if (summaryText.Length <= maxSummary)
				{
					return summaryText;
				}
				if (maxSummary <= 3)
				{
					return summaryText.Substring(0, maxSummary);
				}
				return summaryText.Substring(0, maxSummary - 3) + "...";
			}
			return base.GetNameAndSummary(maxSummary);
		}
	}
}
