using System;
using System.ComponentModel;
using System.Drawing;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public sealed class DropShadow : IXmlPersistable
	{
		private bool _visible;

		private int _offsetY;

		private int _offsetX;

		private Color _color;

		private int _opacity;

		private int _size;

		[NotifyParentProperty(true)]
		[DefaultValue(true)]
		[Description("Indicates if the drop shadow is visible.")]
		public bool Visible
		{
			get
			{
				return _visible;
			}
			set
			{
				_visible = value;
				OnChanged(EventArgs.Empty);
			}
		}

		[Category("Appearance")]
		[DefaultValue(1)]
		[Description("Offset in the Y direction to move the shadow from the original object.")]
		[NotifyParentProperty(true)]
		public int OffsetY
		{
			get
			{
				return _offsetY;
			}
			set
			{
				_offsetY = value;
				OnChanged(EventArgs.Empty);
			}
		}

		[Category("Appearance")]
		[Description("Offset in the X direction to move the shadow from the original object.")]
		[DefaultValue(1)]
		[NotifyParentProperty(true)]
		public int OffsetX
		{
			get
			{
				return _offsetX;
			}
			set
			{
				_offsetX = value;
				OnChanged(EventArgs.Empty);
			}
		}

		[DefaultValue(typeof(Color), "Black")]
		[NotifyParentProperty(true)]
		[Category("Appearance")]
		[Description("Color of the shadow.")]
		public Color Color
		{
			get
			{
				return _color;
			}
			set
			{
				_color = value;
				OnChanged(EventArgs.Empty);
			}
		}

		[DefaultValue(64)]
		[Category("Appearance")]
		[Description("The opacity of the shadow from 0 - 255.")]
		[NotifyParentProperty(true)]
		public int Opacity
		{
			get
			{
				return _opacity;
			}
			set
			{
				if (value >= 0 && value <= 255)
				{
					_opacity = value;
					OnChanged(EventArgs.Empty);
					return;
				}
				throw new ArgumentOutOfRangeException("value");
			}
		}

		[NotifyParentProperty(true)]
		[DefaultValue(3)]
		[Category("Appearance")]
		[Description("The size of the blur applied to the shadow.")]
		public int Size
		{
			get
			{
				return _size;
			}
			set
			{
				_size = value;
				OnChanged(EventArgs.Empty);
			}
		}

		public event EventHandler Changed;

		private void OnChanged(EventArgs e)
		{
			if (this.Changed != null)
			{
				this.Changed(this, e);
			}
		}

		public DropShadow()
		{
			Reset();
		}

		public DropShadow(DropShadow shadow)
		{
			OffsetX = shadow.OffsetX;
			Color = shadow.Color;
			OffsetY = shadow.OffsetY;
			Opacity = shadow.Opacity;
			Size = shadow.Size;
			Visible = shadow.Visible;
		}

		public override string ToString()
		{
			return "";
		}

		public bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			Check.NotNull(writer, "writer");
			if (!ShouldSerialize())
			{
				return true;
			}
			writer.WriteStartElement("Shadow");
			if (_offsetX != 1)
			{
				writer.WriteAttributeString("ox", XmlConvert.ToString(_offsetX));
			}
			if (_offsetY != 1)
			{
				writer.WriteAttributeString("oy", XmlConvert.ToString(_offsetY));
			}
			if (_color != Color.Black)
			{
				writer.WriteAttributeString("c", ImageEffects.ToRGBHex(_color));
			}
			if (_opacity != 64)
			{
				writer.WriteAttributeString("o", XmlConvert.ToString(_opacity));
			}
			if (_size != 3)
			{
				writer.WriteAttributeString("s", XmlConvert.ToString(_size));
			}
			if (!_visible)
			{
				writer.WriteAttributeString("v", XmlConvert.ToString(_visible));
			}
			writer.WriteEndElement();
			return true;
		}

		public bool ReadFromXml(XmlReader reader)
		{
			Check.NotNull(reader, "reader");
			if (reader.Name != "Shadow")
			{
				return false;
			}
			string attribute = reader.GetAttribute("ox");
			if (attribute != null)
			{
				_offsetX = SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_offsetX = 1;
			}
			attribute = reader.GetAttribute("oy");
			if (attribute != null)
			{
				_offsetY = SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_offsetY = 1;
			}
			attribute = reader.GetAttribute("c");
			if (attribute != null)
			{
				_color = ImageEffects.FromRGBHex(attribute);
			}
			else
			{
				_color = Color.Black;
			}
			attribute = reader.GetAttribute("o");
			if (attribute != null)
			{
				_opacity = SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_opacity = 64;
			}
			attribute = reader.GetAttribute("s");
			if (attribute != null)
			{
				_size = SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_size = 3;
			}
			_visible = (reader.GetAttribute("v") != "false");
			return true;
		}

		public bool ShouldSerialize()
		{
			if (_offsetY == 1 && _offsetX == 1 && !(_color != Color.Black) && _opacity == 64 && _size == 3 && _visible)
			{
				return false;
			}
			return true;
		}

		public void Reset()
		{
			_opacity = 64;
			_offsetX = 1;
			_offsetY = 1;
			_color = Color.Black;
			_size = 3;
			_visible = true;
		}
	}
}
