namespace DeployLX.Licensing.v5
{
	public enum ThemeFont
	{
		Normal,
		Bold,
		Medium,
		Title,
		Header,
		Field
	}
}
