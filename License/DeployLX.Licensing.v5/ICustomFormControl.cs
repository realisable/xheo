using System;
using System.ComponentModel;

namespace DeployLX.Licensing.v5
{
	public interface ICustomFormControl : IDisposable, IComponent
	{
		bool ValidateField(string field);

		void AddValues(string prefix, LicenseValuesDictionary values);

		void InitializeFields(string prefix, LicenseValuesDictionary values);
	}
}
