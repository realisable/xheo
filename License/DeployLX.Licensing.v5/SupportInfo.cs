using System;

namespace DeployLX.Licensing.v5
{
	public sealed class SupportInfo : ICloneable
	{
		private string _company;

		private string _url;

		private string _phone;

		private string _email;

		private bool _showWindow = true;

		private string _failureReportUrl;

		private bool _useFormEffects = true;

		private string _product;

		private string _productVersion;

		private bool _includeDetails = true;

		private bool _includeAssemblies = true;

		private bool _includeSystemInfo = true;

		private bool _dontCheckClock;

		public string Company
		{
			get
			{
				return _company;
			}
			set
			{
				_company = value;
			}
		}

		public string Website
		{
			get
			{
				return _url;
			}
			set
			{
				_url = value;
			}
		}

		public string Phone
		{
			get
			{
				return _phone;
			}
			set
			{
				_phone = value;
			}
		}

		public string Email
		{
			get
			{
				return _email;
			}
			set
			{
				_email = value;
			}
		}

		public bool ShowFinalErrorReport
		{
			get
			{
				return _showWindow;
			}
			set
			{
				_showWindow = value;
			}
		}

		public string FailureReportUrl
		{
			get
			{
				return _failureReportUrl;
			}
			set
			{
				_failureReportUrl = value;
			}
		}

		public bool UseFormEffects
		{
			get
			{
				if (Toolbox.CanGetControlAsBitmap())
				{
					return _useFormEffects;
				}
				return false;
			}
			set
			{
				_useFormEffects = value;
			}
		}

		public string Product
		{
			get
			{
				return _product;
			}
			set
			{
				_product = value;
			}
		}

		public string ProductVersion
		{
			get
			{
				return _productVersion;
			}
			set
			{
				_productVersion = value;
			}
		}

		public bool IncludeDetails
		{
			get
			{
				return _includeDetails;
			}
			set
			{
				_includeDetails = value;
			}
		}

		public bool IncludeAssemblies
		{
			get
			{
				return _includeAssemblies;
			}
			set
			{
				_includeAssemblies = value;
			}
		}

		public bool IncludeSystemInfo
		{
			get
			{
				return _includeSystemInfo;
			}
			set
			{
				_includeSystemInfo = value;
			}
		}

		public bool DontCheckClock
		{
			get
			{
				return _dontCheckClock;
			}
			set
			{
				_dontCheckClock = value;
			}
		}

		public void InitToNothing()
		{
			_company = null;
			_email = null;
			_phone = null;
			_url = null;
			_showWindow = true;
			_useFormEffects = !Toolbox.IsTerminalServices;
			_includeAssemblies = true;
			_includeDetails = true;
			_includeSystemInfo = true;
			_product = null;
			_productVersion = null;
		}

		public SupportInfo Clone()
		{
			return base.MemberwiseClone() as SupportInfo;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		public void MergeInfo(SupportInfo info)
		{
			if (Product == null)
			{
				Product = info.Product;
			}
			if (Company == null)
			{
				Company = info.Company;
			}
			if (Email == null)
			{
				Email = info.Email;
			}
			if (FailureReportUrl == null)
			{
				FailureReportUrl = info.FailureReportUrl;
			}
			if (Website == null)
			{
				Website = info.Website;
			}
			if (Phone == null)
			{
				Phone = info.Phone;
			}
		}
	}
}
