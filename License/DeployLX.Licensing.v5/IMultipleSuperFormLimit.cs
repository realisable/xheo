namespace DeployLX.Licensing.v5
{
	public interface IMultipleSuperFormLimit : ISuperFormLimit
	{
		LicenseValuesDictionary CustomForms
		{
			get;
		}
	}
}
