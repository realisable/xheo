using System;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	[LimitEditor("ServiceLimitEditor", IconResource = "asmres://DeployLX.Licensing.DlxAddIn.v5/DeployLX.Licensing.DlxAddIn.v5.Resources.Limits.Service.png")]
	public class ServiceLimit : Limit
	{
		private StringCollection _serviceNames;

		private ServiceTypes _allowed;

		public StringCollection ServiceNames
		{
			get
			{
				return _serviceNames;
			}
		}

		public ServiceTypes Allowed
		{
			get
			{
				return _allowed;
			}
			set
			{
				base.AssertNotReadOnly();
				if (_allowed != value)
				{
					_allowed = value;
					base.OnChanged("Allowed");
				}
			}
		}

		public override string Description
		{
			get
			{
				return SR.GetString("M_ServiceLimitDescription");
			}
		}

		public override string Name
		{
			get
			{
				return "Service";
			}
		}

		public override string SummaryText
		{
			get
			{
				return _allowed.ToString();
			}
		}

		public ServiceLimit()
		{
			_serviceNames = new StringCollection();
			_serviceNames.Changed += _serviceNames_Changed;
		}

		private void _serviceNames_Changed(object sender, CollectionEventArgs e)
		{
			base.OnCollectionChanged(sender, "ServiceNames", e);
		}

		protected ServiceTypes GetServiceType(SecureLicenseContext context)
		{
			if (_serviceNames.Count > 0)
			{
				Assembly entryAssembly = Assembly.GetEntryAssembly();
				string text = null;
				if (entryAssembly != null)
				{
					text = Path.GetFileNameWithoutExtension(entryAssembly.Location);
				}
				if (text == null || text.Length == 0)
				{
					try
					{
						text = Environment.GetCommandLineArgs()[0];
						text = ((text == null || text.Length != 0) ? Path.GetFileNameWithoutExtension(text) : null);
					}
					catch
					{
					}
				}
				if (text != null)
				{
					foreach (string item in (IEnumerable)_serviceNames)
					{
						if (string.Compare(item, text, true) == 0)
						{
							return ServiceTypes.Custom;
						}
						if (item.Length > 4 && string.Compare(item, item.Length - 4, ".exe", 0, 4, true) == 0 && string.Compare(item, 0, text, 0, text.Length, true) == 0)
						{
							return ServiceTypes.Custom;
						}
					}
				}
			}
			if (context.IsServiceRequest)
			{
				return ServiceTypes.NTService;
			}
			if (context.IsWebRequest)
			{
				if (context.GetWebPage() != null)
				{
					return ServiceTypes.WebServer;
				}
				return ServiceTypes.XmlWebService;
			}
			return ServiceTypes.NotService;
		}

		protected bool IsServiceValid(SecureLicenseContext context, bool reportError)
		{
			ServiceTypes serviceType = GetServiceType(context);
			if ((serviceType & _allowed) != 0)
			{
				return true;
			}
			if (reportError)
			{
				if (_allowed == ServiceTypes.NotService)
				{
					context.ReportError("E_CannotUseOnServerOrService", this);
				}
				else if ((_allowed & ServiceTypes.NotService) == ServiceTypes.NotSet)
				{
					context.ReportError("E_MustUseOnServerOrService", this, SafeToolbox.MakeEnumList(_allowed, null));
				}
				else
				{
					context.ReportError("E_ServerOrServiceMismatch", this);
				}
			}
			return false;
		}

		public override ValidationResult Validate(SecureLicenseContext context)
		{
			if (IsServiceValid(context, true))
			{
				return base.Validate(context);
			}
			return ValidationResult.Invalid;
		}

		public override PeekResult Peek(SecureLicenseContext context)
		{
			if (!IsServiceValid(context, false))
			{
				return PeekResult.Invalid;
			}
			return base.Peek(context);
		}

		protected override bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			if (_allowed != 0)
			{
				int allowed = (int)_allowed;
				writer.WriteAttributeString("allowed", allowed.ToString());
			}
			_serviceNames.WriteToXml(writer, null, "Service", "name");
			return true;
		}

		protected override bool ReadFromXml(XmlReader reader)
		{
			_serviceNames.Clear();
			string attribute = reader.GetAttribute("allowed");
			if (attribute != null)
			{
				_allowed = (ServiceTypes)SafeToolbox.FastParseInt32(attribute);
			}
			else
			{
				_allowed = ServiceTypes.NotSet;
			}
			if (reader.IsEmptyElement)
			{
				reader.Read();
				return true;
			}
			reader.Read();
			while (!reader.EOF)
			{
				if (reader.IsStartElement())
				{
					switch (reader.Name)
					{
					case "Limit":
						if (base.ReadChildLimit(reader))
						{
							break;
						}
						return false;
					case "Service":
						if (_serviceNames.ReadFromXml(reader, null, "Service", "name", false))
						{
							break;
						}
						return false;
					default:
						reader.Skip();
						break;
					}
					continue;
				}
				reader.Read();
				break;
			}
			return true;
		}
	}
}
