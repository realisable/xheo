using System;
using System.Collections;
using System.Xml;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public sealed class PropertyBag : Bag
	{
		public string this[string name]
		{
			get
			{
				return GetValue(name) as string;
			}
			set
			{
				base.SetValue(name, value);
			}
		}

		public string this[string name, params string[] inherit]
		{
			get
			{
				return GetValue(name, inherit) as string;
			}
		}

		public PropertyBag()
			: base("Property")
		{
		}

		internal override bool WriteToXml(XmlWriter writer, LicenseSaveType signing)
		{
			Check.NotNull(writer, "writer");
			if (!ShouldSerialize())
			{
				return true;
			}
			writer.WriteStartElement("Properties");
			IDictionaryEnumerator enumerator = base.Dictionary.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					DictionaryEntry dictionaryEntry = (DictionaryEntry)enumerator.Current;
					if (dictionaryEntry.Value != null)
					{
						writer.WriteStartElement("Property");
						writer.WriteAttributeString("name", dictionaryEntry.Key as string);
						writer.WriteAttributeString("value", dictionaryEntry.Value as string);
						writer.WriteEndElement();
					}
				}
			}
			finally
			{
				IDisposable disposable = enumerator as IDisposable;
				if (disposable != null)
				{
					disposable.Dispose();
				}
			}
			writer.WriteEndElement();
			return true;
		}

		internal override bool ReadFromXml(XmlReader reader)
		{
			Check.NotNull(reader, "reader");
			base.AssertNotReadOnly();
			base.Dictionary.Clear();
			if (reader.Name != "Properties")
			{
				return false;
			}
			if (reader.IsEmptyElement)
			{
				reader.Read();
				return true;
			}
			reader.Read();
			while (!reader.EOF)
			{
				reader.MoveToContent();
				if (reader.IsStartElement())
				{
					string name;
					if ((name = reader.Name) != null && name == "Property")
					{
						string attribute = reader.GetAttribute("name");
						string attribute2 = reader.GetAttribute("value");
						if (attribute != null && attribute2 != null)
						{
							base.Dictionary[attribute] = attribute2;
							reader.Read();
							continue;
						}
						return false;
					}
					reader.Skip();
					continue;
				}
				reader.Read();
				break;
			}
			return true;
		}
	}
}
