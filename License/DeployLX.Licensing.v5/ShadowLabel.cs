using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	[Serializable]
	public sealed class ShadowLabel : ThemeLabel
	{
		private DropShadow _dropShadow;

		[Category("Appearance")]
		public DropShadow DropShadow
		{
			get
			{
				return _dropShadow;
			}
			set
			{
				if (_dropShadow != null)
				{
					_dropShadow.Changed -= _dropShadow_Changed;
				}
				if (value == null)
				{
					value = new DropShadow();
				}
				_dropShadow = value;
				_dropShadow.Changed += _dropShadow_Changed;
				base.Invalidate();
			}
		}

		protected override CreateParams CreateParams
		{
			get
			{
				CreateParams createParams = base.CreateParams;
				createParams.ClassName = null;
				return createParams;
			}
		}

		public ShadowLabel()
		{
			base.SetStyle(ControlStyles.UserPaint | ControlStyles.ResizeRedraw | ControlStyles.SupportsTransparentBackColor | ControlStyles.AllPaintingInWmPaint | ControlStyles.CacheText | ControlStyles.OptimizedDoubleBuffer, true);
			BackColor = Color.Empty;
			DropShadow = null;
		}

		internal override TextImagePainter GetPainter()
		{
			TextImagePainter painter = base.GetPainter();
			painter.Shadow = _dropShadow;
			return painter;
		}

		private void _dropShadow_Changed(object sender, EventArgs e)
		{
			base.Invalidate();
		}

		private bool ShouldSerializeDropShadow()
		{
			return _dropShadow.ShouldSerialize();
		}

		private void ResetDropShadow()
		{
			_dropShadow.Reset();
		}
	}
}
