namespace DeployLX.Licensing.v5
{
	public enum SecureStorageRequirement
	{
		NotRequired,
		RequiredExceptWeb,
		Required
	}
}
