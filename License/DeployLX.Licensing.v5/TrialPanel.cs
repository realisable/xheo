using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace DeployLX.Licensing.v5
{
	public class TrialPanel : SplitPanel
	{
		private BufferedPictureBox _trialImage;

		private ShadowLabel _productLabel;

		private SkinnedButton _extend;

		private ShadowLabel _versionLabel;

		private ShadowLabel _companyLabel;

		private BufferedPanel _statusPanel;

		private ShadowLabel _warningLabel;

		private Timer _continueTimer;

		private int _continueDelay;

		private Button _try;

		public TrialPanel(TrialLimit limit)
			: base(limit)
		{
			InitializeComponent();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public TrialPanel()
			: base(null)
		{
		}

		protected override void Dispose(bool disposing)
		{
			if (_continueTimer != null)
			{
				_continueTimer.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			_trialImage = new BufferedPictureBox();
			_productLabel = new ShadowLabel();
			_extend = new SkinnedButton();
			_companyLabel = new ShadowLabel();
			_versionLabel = new ShadowLabel();
			_statusPanel = new BufferedPanel();
			_warningLabel = new ShadowLabel();
			base.SidePanel.SuspendLayout();
			base.BodyPanel.SuspendLayout();
			((ISupportInitialize)_trialImage).BeginInit();
			base.SuspendLayout();
			base.SidePanel.Controls.Add(_statusPanel);
			base.SidePanel.Controls.Add(_extend);
			base.SidePanel.Location = new Point(0, 0);
			base.SidePanel.Size = new Size(170, 421);
			base.SidePanel.Controls.SetChildIndex(_extend, 0);
			base.SidePanel.Controls.SetChildIndex(_statusPanel, 0);
			base.BodyPanel.Controls.Add(_warningLabel);
			base.BodyPanel.Controls.Add(_versionLabel);
			base.BodyPanel.Controls.Add(_companyLabel);
			base.BodyPanel.Controls.Add(_productLabel);
			base.BodyPanel.Controls.Add(_trialImage);
			base.BodyPanel.Location = new Point(170, 0);
			base.BodyPanel.Size = new Size(514, 421);
			_trialImage.BackColor = Color.Transparent;
			_trialImage.BackgroundImageLayout = ImageLayout.None;
			_trialImage.Dock = DockStyle.Fill;
			_trialImage.Location = new Point(0, 0);
			_trialImage.Name = "_trialImage";
			_trialImage.OffsetBackgroundImage = false;
			_trialImage.Size = new Size(514, 421);
			_trialImage.SizeMode = PictureBoxSizeMode.CenterImage;
			_trialImage.TabIndex = 0;
			_trialImage.TabStop = false;
			_productLabel.AutoEllipsis = true;
			_productLabel.Location = new Point(40, 48);
			_productLabel.Name = "_productLabel";
			_productLabel.Size = new Size(452, 87);
			_productLabel.TabIndex = 1;
			_productLabel.Text = "Product Title";
			_productLabel.TextAlign = ContentAlignment.BottomLeft;
			_productLabel.ThemeFont = ThemeFont.Title;
			_extend.Anchor = (AnchorStyles.Bottom | AnchorStyles.Left);
			_extend.Location = new Point(20, 364);
			_extend.Name = "_extend";
			_extend.ShouldDrawFocus = false;
			_extend.Size = new Size(130, 23);
			_extend.TabIndex = 3;
			_extend.Text = "#UI_EnterExtensionCode";
			_extend.UseVisualStyleBackColor = false;
			_extend.Click += _extend_Click;
			_companyLabel.Location = new Point(107, 135);
			_companyLabel.Name = "_companyLabel";
			_companyLabel.Size = new Size(184, 39);
			_companyLabel.TabIndex = 3;
			_companyLabel.Text = "Company";
			_companyLabel.TextAlign = ContentAlignment.BottomLeft;
			_companyLabel.ThemeFont = ThemeFont.Medium;
			_versionLabel.Location = new Point(107, 174);
			_versionLabel.Name = "_versionLabel";
			_versionLabel.Size = new Size(184, 23);
			_versionLabel.TabIndex = 4;
			_statusPanel.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left);
			_statusPanel.BackColor = Color.Transparent;
			_statusPanel.BackgroundImageLayout = ImageLayout.None;
			_statusPanel.Location = new Point(20, 141);
			_statusPanel.Name = "_statusPanel";
			_statusPanel.Size = new Size(130, 217);
			_statusPanel.TabIndex = 4;
			_warningLabel.Location = new Point(19, 321);
			_warningLabel.Name = "_warningLabel";
			_warningLabel.Size = new Size(482, 84);
			_warningLabel.TabIndex = 4;
			_warningLabel.Text = "#UI_CopyrightWarning";
			_warningLabel.TextAlign = ContentAlignment.BottomLeft;
			base.AutoScaleDimensions = new SizeF(96f, 96f);
			base.Name = "TrialPanel";
			base.SidePanel.ResumeLayout(false);
			base.BodyPanel.ResumeLayout(false);
			((ISupportInitialize)_trialImage).EndInit();
			base.ResumeLayout(false);
		}

		protected internal override void LoadPanel(SecureLicenseContext context)
		{
			base.LoadPanel(context);
			TrialLimit trialLimit = base.Limit as TrialLimit;
			if (trialLimit.LogoResource != null)
			{
				base.Logo = Toolbox.GetImage(trialLimit.LogoResource, context);
			}
			if (trialLimit.BitmapResource != null)
			{
				_trialImage.Image = base.SuperForm.GetScaledImage(Toolbox.GetImage(trialLimit.BitmapResource, context), _trialImage.Image);
			}
			if (_trialImage.Image == null)
			{
				_trialImage.Visible = false;
				_productLabel.Text = context.SupportInfo.Product;
				if (context.SupportInfo.Company != null)
				{
					_companyLabel.Text = context.SupportInfo.Company;
				}
				else
				{
					_companyLabel.Visible = false;
				}
				_versionLabel.Text = string.Format("{0}: {1}", SR.GetString("UI_Version"), context.SupportInfo.ProductVersion);
			}
			else
			{
				_productLabel.Visible = false;
				_companyLabel.Visible = false;
				_versionLabel.Visible = false;
				_warningLabel.Visible = false;
			}
		}

		protected override void OnParentChanged(EventArgs e)
		{
			base.OnParentChanged(e);
			if (base.Parent == null && _continueTimer != null)
			{
				_continueTimer.Stop();
			}
		}

		protected internal override void InitializePanel()
		{
			Button button = base.AddBottomButton("#UI_Cancel", 30);
			button.Click += _cancel_Click;
			base._superForm.CancelButton = button;
			_try = base.AddBottomButton("#UI_Try");
			_try.Click += _try_Click;
			base._superForm.AcceptButton = _try;
			TrialLimit trialLimit = base.Limit as TrialLimit;
			if (trialLimit.ShowRegisterIfAvailable && trialLimit.License.Limits.FindLimitByType(typeof(RegistrationLimit), true) != null)
			{
				button = base.AddBottomButton("UI_Register");
				button.Click += _register_Click;
			}
			if (!trialLimit.CanContinueTrial(base._superForm.Context))
			{
				_try.Enabled = false;
			}
			else if (trialLimit.ContinueDelay > 0)
			{
				object obj = base._superForm.Context.Items["TrialDelay"];
				if (obj == null)
				{
					obj = trialLimit.ContinueDelay;
				}
				if (_continueTimer == null)
				{
					_continueTimer = new Timer();
					_continueTimer.Tick += _continueTimer_Tick;
				}
				_continueTimer.Interval = 1000;
				_continueDelay = (int)obj;
				_continueTimer.Start();
				_try.Enabled = false;
				base.CanClose = false;
			}
			if (trialLimit.PurchaseUrl != null)
			{
				button = base.AddBottomButton("UI_BuyNow");
				button.Click += _buyNow_Click;
			}
			if (trialLimit.Terms != null || trialLimit.InfoResource != null)
			{
				button = base.AddBottomButton("UI_MoreInfo");
				button.Click += _moreInfo_Click;
			}
			bool flag = false;
			int top = 0;
			_statusPanel.Controls.Clear();
			Limit[] array = base.Limit.Limits.FindExtendableLimits();
			if (array != null)
			{
				Limit[] array2 = array;
				for (int i = 0; i < array2.Length; i++)
				{
					IExtendableLimit extendableLimit = (IExtendableLimit)array2[i];
					int num;
					int num2;
					int num3;
					extendableLimit.GetUseRange(base._superForm.Context, out num, out num2, out num3);
					string useDescription = extendableLimit.GetUseDescription(base._superForm.Context);
					if (useDescription != null)
					{
						ShadowLabel shadowLabel = new ShadowLabel();
						shadowLabel.Width = _statusPanel.Width;
						shadowLabel.Top = top;
						if (array.Length < 4)
						{
							shadowLabel.Height = 43;
							shadowLabel.ThemeFont = ThemeFont.Medium;
						}
						else
						{
							shadowLabel.Height = 30;
						}
						shadowLabel.TextAlign = ContentAlignment.BottomLeft;
						shadowLabel.Text = useDescription;
						shadowLabel.BackColor = Color.Transparent;
						_statusPanel.Controls.Add(shadowLabel);
						top = shadowLabel.Bottom + 4;
						shadowLabel.BringToFront();
					}
					if (num3 >= 0 && num2 > 0)
					{
						ProgressBar progressBar = new ProgressBar();
						progressBar.Width = _statusPanel.Width;
						progressBar.Top = top;
						if (array.Length < 4)
						{
							progressBar.Height = 24;
						}
						else
						{
							progressBar.Height = 16;
						}
						progressBar.Minimum = 0;
						progressBar.Maximum = num2 - num;
						progressBar.Value = Math.Max(num2 - num3, num);
						progressBar.Step = 1;
						progressBar.Style = ProgressBarStyle.Continuous;
						_statusPanel.Controls.Add(progressBar);
						top = progressBar.Bottom + 4;
						progressBar.BringToFront();
					}
					flag |= extendableLimit.CanExtend;
				}
			}
			_extend.Visible = (flag && !trialLimit.HideExtendButton);
			_try.Focus();
		}

		protected virtual void HandleExtend()
		{
			Limit[] array = base.Limit.Limits.FindExtendableLimits(true);
			if (array != null && array.Length != 0)
			{
				base.ShowPage("EXTEND", HandleExtendResult);
			}
		}

		private void HandleExtendResult(FormResult result)
		{
			if (result == FormResult.Success)
			{
				_try.Enabled = ((TrialLimit)base.Limit).CanContinueTrial(base._superForm.Context);
			}
		}

		protected virtual void HandleMoreInfo()
		{
			TrialLimit trialLimit = base.Limit as TrialLimit;
			if (trialLimit.Terms == null && trialLimit.InfoResource == null)
			{
				return;
			}
			if (trialLimit.Terms == null)
			{
				base.ShowPanel(new InfoPanel(Toolbox.ResolveUrl(trialLimit.InfoResource, base._superForm.Context).ToString(), null), null);
			}
			else
			{
				base.ShowPanel(new InfoPanel(trialLimit.Terms, Toolbox.ResolveUrl(trialLimit.InfoResource, base._superForm.Context)), null);
			}
		}

		protected virtual void HandleTry()
		{
			if (((TrialLimit)base.Limit).CanContinueTrial(base._superForm.Context))
			{
				base.Return(FormResult.Success);
			}
		}

		protected virtual void HandleRegister()
		{
			RegistrationLimit registrationLimit = base.Limit.License.Limits.FindLimitByType(typeof(RegistrationLimit), true) as RegistrationLimit;
			if (registrationLimit != null)
			{
				base._skipPopOnSuccess = true;
				base.ShowPage(registrationLimit, null, HandleRegisterResult, true);
			}
		}

		private void HandleRegisterResult(FormResult result)
		{
			switch (result)
			{
			case FormResult.Retry:
				base.Return(FormResult.Retry);
				break;
			case FormResult.Success:
				base.Return(FormResult.Success);
				break;
			}
		}

		private void _buyNow_Click(object sender, EventArgs e)
		{
			HandleBuyNow();
		}

		private void _moreInfo_Click(object sender, EventArgs e)
		{
			HandleMoreInfo();
		}

		private void _try_Click(object sender, EventArgs e)
		{
			HandleTry();
		}

		private void _continueTimer_Tick(object sender, EventArgs e)
		{
			_continueDelay--;
			base._superForm.Context.Items["TrialDelay"] = _continueDelay;
			if (_continueDelay > 0)
			{
				_try.Text = string.Format("{0} ({1})", SR.GetString("UI_Try"), _continueDelay);
			}
			else
			{
				_continueTimer.Stop();
				_try.Enabled = ((TrialLimit)base.Limit).CanContinueTrial(base._superForm.Context);
				_try.Text = SR.GetString("UI_Try");
				base.CanClose = true;
			}
		}

		private void _cancel_Click(object sender, EventArgs e)
		{
			base.Return(FormResult.Failure);
		}

		private void _extend_Click(object sender, EventArgs e)
		{
			HandleExtend();
		}

		private void _register_Click(object sender, EventArgs e)
		{
			HandleRegister();
		}

		protected internal override void UpdateFromTheme()
		{
			base.UpdateFromTheme();
			_warningLabel.ForeColor = Color.FromArgb(128, _warningLabel.ForeColor);
		}

		protected internal override void HandleKeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.F6)
			{
				e.Handled = true;
				_extend_Click(_extend, EventArgs.Empty);
			}
			base.HandleKeyDown(sender, e);
		}
	}
}
