using DeployLX.Licensing.v5;
using System;
using System.Diagnostics;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;
using System.Web;

[assembly: CompilationRelaxations(8)]
[assembly: AssemblyCompany("XHEO® INC")]
[assembly: AssemblyCopyright("© 2002-2012 XHEO® INC. All rights reserved.")]
//[assembly: Debuggable(DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints)]
[assembly: AssemblyInformationalVersion("DIAGNOSTIC:2013.10.22.0356")]
[assembly: ComVisible(false)]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows = true)]
[assembly: AssemblyProduct("DeployLX® Licensing v5.0 Release")]
[assembly: AssemblyTitle("DeployLX® License Management and Creation Tools v5.0 [.NET 2.0] Release")]
[assembly: AssemblyConfiguration("Release")]
[assembly: AssemblyTrademark("XHEO, DeployLX and DeployLX® Licensing are either trademarks or registered trademarks of XHEO INC.")]
//[assembly: AllowPartiallyTrustedCallers]
[assembly: SatelliteContractVersion("5.0.0.0")]
[assembly: AssemblyFileVersion("5.0.2000.5827")]
[assembly: LicenseKey("$m|+k$qe&'§t=(nkhl%$(91hc$[xds'/mK81=2?:)8%Ib~I.,H%~B?.-@Y^qsd§f50TsIB_M$B$Kj%W8Rcahy2iTI?}o&vL=l+oDjwfT1rZJKj@&)yC.T¶+'/l^_?pMwj6{O`=rfa&_NaK[/§e?+Dj'Y'pxsE!F0W1Pm)lf5(2iJVJ(XPGVz]5[4UH15Z;Ty[QV!xGWjW}(zHJ/}!%`$BD,YPGEX3D};'IK@/D9!3+X'TANxKj¶t5a9W&B@_p6x)cX(+cF65aUn2WO0hjW`B_FjJ}{$%NIIY]T)qTp§+@,S:;1%*6q}ah?J:H#w1Zl14G|F%~/UPgebl!X¶x_w¶)M#bRKia[[$EN,t|@fV5YFR{Fkr74O+*Wv`P#AiY=,gx;§OXYg`^b7?FV+vyzkBRrmkYIYd¶rTYO3/LLiz~:Z§Rb17U^VxhA/gYP9YhY(X$%GU9tk%E=N$hzNy#2Vx=pRs]gx/*5Ry[`|R`d-EU-7gak~]Zg#pIh,'O_jeeXH)(T*z,kX(Jl*r+x6x+%)-,~+(fOyMKrZNaQR~iX0o¶0!cOYB|.TiyIL8yrnha}fIFcJl77Pn`a^t")]
[assembly: AssemblyDescription("Management support library for DeployLX Licensing.")]
//[assembly: LicenseHelp(Website = "http://goto.xheo.com/support", Email = "support@xheo.com", Product = "DeployLX® Licensing", FailureReportUrl = "http://license.xheo.com/licenseserver.asmx")]
[assembly: CLSCompliant(true)]
//[assembly: SecurityPermission(SecurityAction.RequestMinimum, UnmanagedCode = true, Execution = true, ControlAppDomain = true, ControlThread = true, SerializationFormatter = true)]
//[assembly: EnvironmentPermission(SecurityAction.RequestMinimum, Unrestricted = true)]
//[assembly: RegistryPermission(SecurityAction.RequestMinimum, Read = "HKEY_CURRENT_USER", Write = "HKEY_CURRENT_USER")]
//[assembly: RegistryPermission(SecurityAction.RequestMinimum, Read = "HKEY_USERS\\.DEFAULT", Write = "HKEY_USERS\\.DEFAULT")]
//[assembly: SecurityPermission(SecurityAction.RequestMinimum, SkipVerification = true)]
//[assembly: PermissionSet(SecurityAction.RequestOptional, Name = "FullTrust", Unrestricted = true)]
//[assembly: IsolatedStorageFilePermission(SecurityAction.RequestOptional, UserQuota = 1048576L)]
//[assembly: SecurityPermission(SecurityAction.RequestOptional, Unrestricted = true)]
//[assembly: FileIOPermission(SecurityAction.RequestOptional, Unrestricted = true)]
//[assembly: AspNetHostingPermission(SecurityAction.RequestOptional, Level = AspNetHostingPermissionLevel.Minimal)]
//[assembly: RegistryPermission(SecurityAction.RequestOptional, Read = "HKEY_LOCAL_MACHINE", Write = "HKEY_LOCAL_MACHINE")]
//[assembly: UIPermission(SecurityAction.RequestOptional, Clipboard = UIPermissionClipboard.OwnClipboard, Window = UIPermissionWindow.AllWindows)]
[assembly: AssemblyVersion("5.0.2000.0")]
