using System;
using System.Threading;

namespace DeployLX.Licensing.v5
{
	internal abstract class StaHelper
	{
		private ManualResetEvent _complete = new ManualResetEvent(false);

		private bool _dontRetryWorkOnFailed;

		public bool DontRetryWorkOnFailed
		{
			get
			{
				return _dontRetryWorkOnFailed;
			}
			set
			{
				_dontRetryWorkOnFailed = value;
			}
		}

		public void Go()
		{
			Thread thread = new Thread(DoWork);
			thread.SetApartmentState(ApartmentState.STA);
			thread.IsBackground = true;
			thread.Name = ToString();
			thread.Start();
		}

		public void GoAndWait()
		{
			Go();
			_complete.WaitOne();
		}

		private void DoWork()
		{
			try
			{
				Work();
			}
			catch (Exception ex)
			{
				if (_dontRetryWorkOnFailed)
				{
					throw;
				}
				try
				{
					Thread.Sleep(1000);
					Work();
				}
				catch
				{
					//MessageBoxEx.ShowException(ex);
				}
			}
			finally
			{
				_complete.Set();
			}
		}

		protected abstract void Work();
	}
}
