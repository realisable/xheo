using DeployLX.Licensing.v5;

internal class SR
{
	private static readonly StaticResources _sr = new StaticResources("DeployLX.Licensing.Management.v5");

	private SR()
	{
	}

	public static string GetString(string name, params object[] args)
	{
		return _sr.GetString(name, true, args);
	}

	public static void LocalizeObject(object root, bool includeChildren, params object[] args)
	{
		_sr.LocalizeObject(root, true, includeChildren, args);
	}
}
