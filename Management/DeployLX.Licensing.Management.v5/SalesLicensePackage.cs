using DeployLX.Licensing.v5;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Xml;

namespace DeployLX.Licensing.Management.v5
{
	public class SalesLicensePackage
	{
		public static readonly Version v5_0 = new Version(5, 0);

		public static readonly Version CurrentVersion = v5_0;

		public LicenseFile LicenseFile
		{
			get;
			set;
		}

		public LicenseKey LicenseKey
		{
			get;
			set;
		}

		public Version Version
		{
			get;
			set;
		}

		public SalesLicensePackage()
		{
			Version = CurrentVersion;
		}

		public void WriteToXml(XmlTextWriter writer)
		{
			if (writer == null)
			{
				throw new ArgumentNullException("writer");
			}
			if (LicenseFile == null)
			{
				throw new LicenseManagementException("E_MissingProperty", "LicenseFile");
			}
			if (LicenseKey == null)
			{
				throw new LicenseManagementException("E_MissingProperty", "LicenseKey");
			}
			writer.WriteStartElement("LicensePackage");
			writer.WriteAttributeString("version", Version.ToString());
			using (MemoryStream memoryStream = new MemoryStream())
			{
				LicenseFile.Save(memoryStream);
				byte[] inArray = memoryStream.ToArray();
				writer.WriteElementString("LicenseFile", Convert.ToBase64String(inArray));
			}
			using (MemoryStream memoryStream2 = new MemoryStream())
			{
				LicenseKey.Save(memoryStream2, true);
				byte[] inArray2 = memoryStream2.ToArray();
				writer.WriteElementString("LicenseKey", Convert.ToBase64String(inArray2));
			}
			writer.WriteEndElement();
		}

		public void ReadFromXml(XmlTextReader reader)
		{
			if (reader == null)
			{
				throw new ArgumentNullException("reader");
			}
			LicenseFile = null;
			LicenseKey = null;
			reader.MoveToContent();
			if (reader.Name != "LicensePackage")
			{
				throw new LicenseManagementException("E_InvalidLicensePackage");
			}
			string attribute = reader.GetAttribute("version");
			if (attribute != null && !(attribute != CurrentVersion.ToString()))
			{
				reader.ReadStartElement("LicensePackage");
				while (true)
				{
					if (!reader.EOF)
					{
						reader.MoveToContent();
						if (!reader.IsStartElement())
						{
							break;
						}
						switch (reader.Name)
						{
						case "LicenseKey":
						{
							string s2 = reader.ReadElementContentAsString();
							using (MemoryStream stream2 = new MemoryStream(Convert.FromBase64String(s2)))
							{
								LicenseKey = new LicenseKey(stream2);
							}
							break;
						}
						case "LicenseFile":
						{
							string s = reader.ReadElementContentAsString();
							using (MemoryStream stream = new MemoryStream(Convert.FromBase64String(s)))
							{
								LicenseFile = new LicenseFile();
								LicenseFile.Load(stream);
							}
							break;
						}
						default:
							throw new LicenseManagementException("E_InvalidLicensePackage");
						}
						continue;
					}
					return;
				}
				reader.Read();
				return;
			}
			throw new LicenseManagementException("E_InvalidLicensePackage");
		}

		public void Load(Stream stream, string password)
		{
			byte[] array = new byte[8];
			stream.Read(array, 0, 3);
			if (Encoding.UTF8.GetString(array, 0, 3) != "LSP")
			{
				throw new LicenseManagementException("E_InvalidLicensePackage");
			}
			if (stream.Read(array, 0, 8) != 8)
			{
				throw new LicenseManagementException("E_InvalidLicensePackage");
			}
			PasswordDeriveBytes passwordDeriveBytes = new PasswordDeriveBytes(password, new byte[16]);
			using (TripleDESCryptoServiceProvider tripleDESCryptoServiceProvider = new TripleDESCryptoServiceProvider())
			{
				tripleDESCryptoServiceProvider.IV = array;
				tripleDESCryptoServiceProvider.Key = passwordDeriveBytes.CryptDeriveKey("TripleDES", "SHA1", 0, array);
				using (CryptoStream input = new CryptoStream(stream, tripleDESCryptoServiceProvider.CreateDecryptor(), CryptoStreamMode.Read))
				{
					using (XmlTextReader reader = new XmlTextReader(input))
					{
						ReadFromXml(reader);
					}
				}
			}
		}

		public void Load(string fileName, string password)
		{
			using (FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
			{
				Load(stream, password);
			}
		}

		public void Save(Stream stream, string password)
		{
			using (TripleDESCryptoServiceProvider tripleDESCryptoServiceProvider = new TripleDESCryptoServiceProvider())
			{
				PasswordDeriveBytes passwordDeriveBytes = new PasswordDeriveBytes(password, new byte[16]);
				tripleDESCryptoServiceProvider.GenerateIV();
				stream.Write(Encoding.UTF8.GetBytes("LSP"), 0, 3);
				stream.Write(tripleDESCryptoServiceProvider.IV, 0, tripleDESCryptoServiceProvider.IV.Length);
				tripleDESCryptoServiceProvider.Key = passwordDeriveBytes.CryptDeriveKey("TripleDES", "SHA1", 0, tripleDESCryptoServiceProvider.IV);
				using (CryptoStream cryptoStream = new CryptoStream(stream, tripleDESCryptoServiceProvider.CreateEncryptor(), CryptoStreamMode.Write))
				{
					using (XmlTextWriter xmlTextWriter = new XmlTextWriter(cryptoStream, new UTF8Encoding(false)))
					{
						xmlTextWriter.Formatting = Formatting.Indented;
						if (stream.Position == 0L)
						{
							xmlTextWriter.WriteStartDocument();
						}
						WriteToXml(xmlTextWriter);
						xmlTextWriter.Close();
						cryptoStream.Close();
					}
				}
			}
		}

		public void Save(string fileName, string password)
		{
			using (FileStream stream = new FileStream(fileName, FileMode.Create, FileAccess.Write))
			{
				Save(stream, password);
			}
		}
	}
}
