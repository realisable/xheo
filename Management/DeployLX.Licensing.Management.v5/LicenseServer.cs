using DeployLX.Licensing.v5;
using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Web;

//Realisable - Remove so we can use Azure Functions over REST
//using System.Web.Caching;
//using System.Web.Services;
//using System.Web.Services.Protocols;
//using System.Windows.Forms;
using Newtonsoft.Json;
using Realisable.Utils.Net;

namespace DeployLX.Licensing.Management.v5
{
    //Realisable - Remove so we can use Azure Functions over REST
    //[WebService(Namespace = "http://www.xheo.com/licensing/v3_0", Description = "Implements a license server for the DeployLX Licensing® copy protection system. Additional info at http://www.deploylx.com.", Name = "DeployLX Licensing® Server")]
    public class LicenseServer //: WebService
	{
		private class ChangeMonitor : IDisposable
		{
			private SecureLicense _license;

			private ChangeEventHandler _handler;

			public ChangeMonitor(SecureLicense license)
			{
				_license = license;
				_handler = ChangeMonitor_Changed;
				((IChange)license.LicenseFile).Changed += _handler;
			}

			private void ChangeMonitor_Changed(object sender, ChangeEventArgs e)
			{
				SecureLicense secureLicense = e.Element as SecureLicense;
				LicenseFile licenseFile = e.Element as LicenseFile;
				Limit limit = e.Element as Limit;
				if (secureLicense == null && limit != null)
				{
					secureLicense = limit.License;
				}
				if (secureLicense != null && secureLicense != _license)
				{
					throw new LicenseManagementException("E_CannotModifyOtherLicense");
				}
				if (licenseFile != null)
				{
					throw new LicenseManagementException("E_MustCreateNewLicenseFile");
				}
				if (secureLicense != null)
				{
					switch (e.Name)
					{
					case "EditorState":
					case "Signature":
					case "MetaValues":
					case "RegistrationInfo":
					case "KeyName":
					case "IsNew":
						break;
					default:
						secureLicense.EditorState.Remove("__SIGNED__");
						break;
					}
				}
			}

			public void Dispose()
			{
				((IChange)_license.LicenseFile).Changed -= _handler;
			}
		}

		private string _keyFolder;

		private string[] _deployLxSerialNumbers;

		private bool _autoRegister = true;

		private bool _autoValidate;

		private bool _developerMode;

		public string KeyFolder
		{
			get
			{
				if (_keyFolder != null && _keyFolder.Length != 0)
				{
					return _keyFolder;
				}
				return Configuration.KeyFolder;
			}
			set
			{
				_keyFolder = value;
			}
		}

        //Realisable - Add - Allows us to inject a stream as opposed to a path for the key.
        //Even though we're not forcing the license check for DLX we still need to use the license key to 
        //resign the license.
        public Stream KeyStream
        {
            get; set;
        }


		public string[] DeployLxSerialNumbers
		{
			get
			{
				return _deployLxSerialNumbers;
			}
			set
			{
				_deployLxSerialNumbers = value;
			}
		}

		public bool AutoRegister
		{
			get
			{
				return _autoRegister;
			}
			set
			{
				_autoRegister = value;
			}
		}

		public bool AutoValidate
		{
			get
			{
				return _autoValidate;
			}
			set
			{
				_autoValidate = value;
			}
		}

		public bool DeveloperMode
		{
			get
			{
				return _developerMode;
			}
			set
			{
				_developerMode = value;
			}
		}

		public byte[] Dispatch(string command, byte[] licenseXml, string licenseId, string limitId, string key, string clientCulture, string properties, string salt)
		{
			CultureInfo cultureInfo = null;
			try
			{
				try
				{
					if (clientCulture != null && clientCulture != CultureInfo.CurrentUICulture.Name)
					{
						cultureInfo = CultureInfo.CurrentUICulture;
						Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(clientCulture);
					}
				}
				catch (Exception ex)
				{
					UnhandledException(ex, null, "SETCULTURE");
				}
				Check.NotNull(command, "command");
				Check.NotNull(clientCulture, "clientCulture");
				Check.NotNull(salt, "salt");
				OnBeforeDispatch();
				//SecureLicense secureLicense = LicenseKey.AssertLicense(_deployLxSerialNumbers);
				//if (((StringDictionary)secureLicense.Values)["servers"] != null)
				//{
				//	throw new LicenseManagementException("E_NotLicensedEdition", null, ErrorSeverity.High, ((StringDictionary)secureLicense.Values)["servers"], "use a license server");
				//}
				Limit limit = null;
				LicenseValuesDictionary licenseValuesDictionary = new LicenseValuesDictionary();
				bool flag = _developerMode;
				try
				{
					if (properties != null)
					{
						licenseValuesDictionary.FromXmlString(properties);
					}
					if (!flag)
					{
						string text = ((StringDictionary)licenseValuesDictionary)["DeveloperMode"];
						if (text != null)
						{
							byte[] b = Convert.FromBase64String(text);
							string @string = Encoding.UTF8.GetString(DeployLX.Licensing.v5.SharedToolbox.Scramble(b));
							flag = (salt == @string);
						}
					}
					if (command == "FAILUREREPORT")
					{
						string text2 = ((StringDictionary)licenseValuesDictionary)["Version"];
						ProcessFailureReport(((StringDictionary)licenseValuesDictionary)["FAILUREREPORT"], ((StringDictionary)licenseValuesDictionary)["MachineProfile"], (text2 == null || text2.Length == 0) ? new Version(1, 0) : new Version(text2), licenseValuesDictionary);
						return null;
					}
				}
				catch (Exception ex2)
				{
					Exception ex3 = UnhandledException(ex2, null, command);
					if (ex3 != null && ex3 != ex2)
					{
						throw ex3;
					}
					throw;
				}
				Check.NotNull(licenseXml, "licenseXml");
				Check.NotNull(licenseId, "licenseId");
				Check.NotNull(key, "name");
				ServerRequestContext serverRequestContext = null;
				try
				{
					//Realisable - Remove - We cannot have this installed on the server, so remove the check.
					//string path = Path.Combine(KeyFolder, key);

					//LicenseKey licenseKey = base.Context.Cache["DLXKey." + key] as LicenseKey;
					//if (licenseKey == null)
					//{
					//	licenseKey = new LicenseKey(path);
					//	licenseKey.DeployLxSerialNumbers = _deployLxSerialNumbers;
					//	licenseKey._tag = true;
					//	base.Context.Cache.Add("DLXKey." + key, licenseKey, new CacheDependency(licenseKey.Location), Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(60.0), CacheItemPriority.Normal, null);
					//}

					//Realisable - Use KeyStream
					LicenseKey licenseKey = new LicenseKey(KeyStream)
                    {
                        DeployLxSerialNumbers = _deployLxSerialNumbers,
                        _tag = true
                    };
 
                    LicenseFile licenseFile  = new LicenseFile();
					using (MemoryStream stream = new MemoryStream(licenseXml))
					{
						licenseFile.Load(stream);
					}
					SecureLicense secureLicense2 = licenseFile.Licenses[licenseId];
                    //Realisable - Remove - No cache in Azure functions.
                    //HttpContext.Current.Cache["DeployLX.LastLicense"] = secureLicense2;
                    //HttpContext.Current.Cache["DeployLX.LastLicenseFile"] = licenseFile;
                    foreach (SecureLicense item in (IEnumerable)licenseFile.Licenses)
					{
						try
						{
							if (licenseKey.ValidateSignature(item) == VerifySignatureResult.Valid)
							{
								licenseKey.DecryptLicense(item);
							}
						}
						catch (Exception ex4)
						{
							UnhandledException(ex4, null, command);
						}
					}
					serverRequestContext = new ServerRequestContext(command, secureLicense2, null, licenseValuesDictionary, licenseKey, flag);
					if (secureLicense2 == null)
					{
						throw UnhandledException(new LicenseManagementException("E_InvalidLicenseId", null, ErrorSeverity.High, licenseId), serverRequestContext, command);
					}
					limit = (serverRequestContext.ReferenceLimit = secureLicense2.Limits.FindLimitById(limitId));
					if (licenseKey.ValidateSignature(secureLicense2) != VerifySignatureResult.Valid)
					{
						throw UnhandledException(new SecureLicenseException("E_InvalidSignature", null, ErrorSeverity.High, secureLicense2.LicenseId, secureLicense2.LicenseFile.Location), serverRequestContext, command);
					}
					if (licenseFile.SaveTo == SaveLocations.NotSet && licenseFile.SuggestedSaveLocation == null)
					{
						licenseFile.SaveTo = SaveLocations.OriginalLocation;
					}
					secureLicense2.MetaValues.RemoveWithPrefix("__MODIFIEDATSERVER");
					OnBeforeCommand(serverRequestContext);
					try
					{
						using (new ChangeMonitor(secureLicense2))
						{
							bool flag2 = false;
							try
							{
								string a;
								if ((a = command) != null && (a == "VALIDATE" || a == "ACTIVATE" || a == "EXTEND") && PerformAutoRegisterInternal(serverRequestContext))
								{
									if (serverRequestContext.ReferenceLimit == null && limit != null)
									{
										goto IL_0455;
									}
									if (serverRequestContext.NewLicenseFile != null && serverRequestContext.NewLicenseFile.Licenses[secureLicense2.LicenseId] == null)
									{
										goto IL_0455;
									}
								}
								goto end_IL_03eb;
								IL_0455:
								flag2 = true;
								end_IL_03eb:;
							}
							catch (Exception ex5)
							{
								Exception ex6 = UnhandledException(ex5, serverRequestContext, "AUTOREGISTER");
								if (ex6 != null && ex6 != ex5)
								{
									throw ex6;
								}
								throw;
							}
							try
							{
								string a2;
								if (!flag2 && (a2 = command) != null && (a2 == "REGISTER" || a2 == "ACTIVATE" || a2 == "EXTEND") && PerformAutoValidateInternal(serverRequestContext))
								{
									if (serverRequestContext.ReferenceLimit == null && limit != null)
									{
										goto IL_04ec;
									}
									if (serverRequestContext.NewLicenseFile != null && serverRequestContext.NewLicenseFile.Licenses[secureLicense2.LicenseId] == null)
									{
										goto IL_04ec;
									}
								}
								goto end_IL_047e;
								IL_04ec:
								flag2 = true;
								end_IL_047e:;
							}
							catch (Exception ex7)
							{
								Exception ex8 = UnhandledException(ex7, serverRequestContext, "AUTOVALIDATE");
								if (ex8 != null && ex8 != ex7)
								{
									throw ex8;
								}
								throw;
							}
							try
							{
								if (!flag2)
								{
									switch (command)
									{
									case "EXTEND":
										flag2 = Extend(serverRequestContext, serverRequestContext.ReferenceLimit as IExtendableLimit);
										break;
									case "REGISTER":
										flag2 = Register(serverRequestContext, serverRequestContext.License.RegistrationInfo, false);
										break;
									case "DEACTIVATE":
										flag2 = Deactivate(serverRequestContext, serverRequestContext.ReferenceLimit as ActivationLimit, (DeactivationPhase)Enum.Parse(typeof(DeactivationPhase), ((StringDictionary)licenseValuesDictionary)["DeactivationPhase"]), ((StringDictionary)licenseValuesDictionary)["ConfirmationCode"]);
										break;
									case "ACTIVATE":
										flag2 = Activate(serverRequestContext, serverRequestContext.ReferenceLimit as ActivationLimit);
										break;
									case "VALIDATE":
										flag2 = Validate(serverRequestContext);
										break;
									default:
										flag2 = ExecuteCustomCommand(serverRequestContext);
										break;
									}
								}
							}
							catch (Exception ex9)
							{
								Exception ex10 = UnhandledException(ex9, serverRequestContext, command);
								if (ex10 != null && ex10 != ex9)
								{
									throw ex10;
								}
								throw;
							}
							if (!flag2)
							{
								throw UnhandledException(new LicenseManagementException("E_CouldNotPerformValidationRequest", command), serverRequestContext, command);
							}
						}
					}
					catch (Exception exception)
					{
						if (serverRequestContext != null)
						{
							serverRequestContext.Exception = exception;
						}
						throw;
					}
					finally
					{
						OnAfterCommand(serverRequestContext);
					}
					if (serverRequestContext.NewLicenseFile == null && ((StringDictionary)secureLicense2.MetaValues)["__MODIFIED__"] != "true" && ((StringDictionary)secureLicense2.MetaValues)["__SUCCESSMESSAGE"] == null)
					{
						return licenseKey.SignSalt(salt);
					}
					if (serverRequestContext.NewLicenseFile != null)
					{
						((StringDictionary)serverRequestContext.NewLicenseFile.MetaValues)["__NEWLICENSE"] = "true";
						using (MemoryStream memoryStream = new MemoryStream())
						{
							foreach (SecureLicense item2 in (IEnumerable)serverRequestContext.NewLicenseFile.Licenses)
							{
								if (item2.IsNew)
								{
									licenseKey.SignLicense(item2);
								}
								else if (((StringDictionary)item2.MetaValues)["__MODIFIED__"] == "true" && licenseKey.ValidateSignature(item2) != VerifySignatureResult.DifferentKeys)
								{
									licenseKey.SignLicense(item2);
								}
							}
							secureLicense2.MetaValues.Remove("__MODIFIED__");
							serverRequestContext.NewLicenseFile.Save(memoryStream);
							return memoryStream.ToArray();
						}
					}
					secureLicense2.MetaValues.Remove("__MODIFIED__");
					licenseKey.SignLicense(secureLicense2);
					return Encoding.UTF8.GetBytes(licenseFile.ToXmlString());
				}
				catch (Exception ex11)
				{
					Exception ex12 = UnhandledException(ex11, serverRequestContext, "GENERALFAILURE");
					if (ex12 != null && ex12 != ex11)
					{
						throw ex12;
					}
					throw;
				}
			}
			finally
			{
				OnAfterDispatch();
				try
				{
					if (cultureInfo != null)
					{
						Thread.CurrentThread.CurrentUICulture = cultureInfo;
					}
					else
					{
						Thread.CurrentThread.CurrentUICulture = CultureInfo.InvariantCulture;
					}
				}
				catch (Exception ex13)
				{
					UnhandledException(ex13, null, "RESTORECULTURE");
				}
			}
		}

		protected virtual void OnBeforeCommand(ServerRequestContext context)
		{
		}

		protected virtual void OnAfterCommand(ServerRequestContext context)
		{
		}

		protected virtual void OnBeforeDispatch()
		{
		}

		protected virtual void OnAfterDispatch()
		{
		}

		public virtual Exception UnhandledException(Exception ex, ServerRequestContext context, string stage)
		{
			return ex;
		}

		protected void NotifyModification(Limit limit)
		{
			if (limit != null)
			{
				((StringDictionary)limit.License.MetaValues)["__MODIFIED__"] = "true";
				((StringDictionary)limit.License.MetaValues)["__MODIFIEDATSERVER." + limit.LimitId] = "true";
			}
		}

		protected void NotifyModification(SecureLicense license)
		{
			if (license != null)
			{
				((StringDictionary)license.MetaValues)["__MODIFIED__"] = "true";
				((StringDictionary)license.MetaValues)["__MODIFIEDATSERVER"] = "true";
			}
		}

		protected void ShowSuccessMessage(Limit limit, string message)
		{
			if (limit != null)
			{
				((StringDictionary)limit.License.MetaValues)["__SUCCESSMESSAGE"] = message;
			}
		}

		protected void ShowSuccessMessage(SecureLicense license, string message)
		{
			if (license != null)
			{
				((StringDictionary)license.MetaValues)["__SUCCESSMESSAGE"] = message;
			}
		}

		protected virtual bool Validate(ServerRequestContext context, bool autoValidate)
		{
			LicenseServerLimit licenseServerLimit = context.ReferenceLimit as LicenseServerLimit;
			if (licenseServerLimit != null && licenseServerLimit.IsPeriodic)
			{
				licenseServerLimit.RenewPeriod(context.MachineProfileHash, context);
				NotifyModification(licenseServerLimit);
			}
			return true;
		}

		protected virtual bool Validate(ServerRequestContext context)
		{
			return Validate(context, false);
		}

		private bool PerformAutoValidateInternal(ServerRequestContext context)
		{
			if (_autoValidate)
			{
				if (!Validate(context, true))
				{
					return false;
				}
				if (context.NewLicenseFile != null && context.ReferenceLimit != null)
				{
					string limitId = context.ReferenceLimit.LimitId;
					foreach (SecureLicense item in (IEnumerable)context.NewLicenseFile.Licenses)
					{
						context.ReferenceLimit = item.Limits[limitId];
						if (context.ReferenceLimit == null)
						{
							continue;
						}
						context.License = context.ReferenceLimit.License;
						break;
					}
				}
			}
			return true;
		}

		protected virtual bool Activate(ServerRequestContext context, ActivationLimit referenceLimit)
		{
			if (referenceLimit != null)
			{
				ActivationProfile activationProfile = null;
				ActivationProfile activationProfile2 = null;
				int num = -1;
				if (((StringDictionary)context.Properties)["ActiveProfile"] != null)
				{
					num = int.Parse(((StringDictionary)context.Properties)["ActiveProfile"]);
				}
				if (num != -1)
				{
					foreach (ActivationProfile item in (IEnumerable)referenceLimit.Profiles)
					{
						if (item.ReferenceId != num)
						{
							continue;
						}
						activationProfile2 = item;
						break;
					}
				}
				if (activationProfile2 == null)
				{
					foreach (ActivationProfile item2 in (IEnumerable)referenceLimit.Profiles)
					{
						if (item2.Hash == null && item2.CanUnlock(context.MachineProfileHash))
						{
							activationProfile2 = item2;
						}
						if (item2.CanUnlock(context.MachineProfileHash) && (activationProfile == null || item2.UnlockDate < activationProfile.UnlockDate))
						{
							activationProfile = item2;
						}
					}
				}
				if (activationProfile2 == null && activationProfile != null)
				{
					activationProfile2 = activationProfile;
				}
				if (activationProfile2 != null && CanActivate(context, referenceLimit, ref activationProfile2))
				{
					if (activationProfile2 != null && activationProfile2.Unlock(context.MachineProfileHash, context))
					{
						RecordActivation(context, referenceLimit, activationProfile2);
						NotifyModification(referenceLimit);
						goto IL_017b;
					}
					throw new LicenseManagementException(referenceLimit.CanDeactivate ? "E_CannotFindAvailableProfileDeactivateAllowed" : "E_CannotFindAvailableProfile");
				}
				throw new LicenseManagementException(referenceLimit.CanDeactivate ? "E_CannotFindAvailableProfileDeactivateAllowed" : "E_CannotFindAvailableProfile");
			}
			goto IL_017b;
			IL_017b:
			return true;
		}

		protected virtual bool CanActivate(ServerRequestContext context, ActivationLimit limit, ref ActivationProfile suggestedProfile)
		{
			return suggestedProfile.CanUnlock(context.MachineProfileHash);
		}

		protected virtual bool CheckProfile(ServerRequestContext context, ActivationLimit limit, string storedHash, DateTime lastActivated, int machineId, out ActivationProfile profile)
		{
			profile = null;
			foreach (ActivationProfile item in (IEnumerable)limit.Profiles)
			{
				if (item.ReferenceId != machineId)
				{
					continue;
				}
				profile = item;
				break;
			}
			if (profile == null)
			{
				return false;
			}
			if (lastActivated.AddMonths(3) < context.CurrentDateAndTime)
			{
				return true;
			}
			int num = MachineProfile.CompareHash(context.MachineProfileHash, storedHash, false, profile.Hardware);
			if (num <= profile.Tolerance)
			{
				profile.Hash = storedHash;
				context.MachineProfileHash = storedHash;
				NotifyModification(limit);
				return true;
			}
			return false;
		}

		protected virtual void RecordActivation(ServerRequestContext context, ActivationLimit limit, ActivationProfile profile)
		{
		}

		protected virtual bool Deactivate(ServerRequestContext context, ActivationLimit referenceLimit, DeactivationPhase phase, string confirmationCode)
		{
			ActivationProfile activationProfile = null;
			int num = 2147483647;
			foreach (ActivationProfile item in (IEnumerable)referenceLimit.Profiles)
			{
				if (item.Hash != null && item.CanUnlock(context.MachineProfileHash))
				{
					int num2 = MachineProfile.CompareHash(item.Hash, context.MachineProfileHash, false, item.Hardware);
					if (num2 < num)
					{
						activationProfile = item;
						num = num2;
					}
				}
			}
			if (activationProfile != null && CanDeactivate(context, referenceLimit, phase, ref activationProfile))
			{
				if (phase == DeactivationPhase.Commit)
				{
					RecordDeactivation(context, referenceLimit, activationProfile);
				}
				return true;
			}
			throw new LicenseManagementException("E_CannotFindAvailableProfile");
		}

		protected virtual bool CanDeactivate(ServerRequestContext context, ActivationLimit limit, DeactivationPhase phase, ref ActivationProfile suggestedProfile)
		{
			return true;
		}

		protected virtual void RecordDeactivation(ServerRequestContext context, ActivationLimit limit, ActivationProfile profile)
		{
		}

		protected virtual bool Extend(ServerRequestContext context, IExtendableLimit limit)
		{
			if (limit != null)
			{
				int extensionAmount;
				if (!CanExtend(context, limit, out extensionAmount))
				{
					return false;
				}
				if (!context.IsModified)
				{
					limit.UpdateAtServer(context.Properties, extensionAmount);
					NotifyModification(limit as Limit);
				}
				RecordExtension(context, limit);
				return true;
			}
			return false;
		}

		[Obsolete("Override the Extend method directly.")]
		protected virtual bool CanExtend(ServerRequestContext context, IExtendableLimit limit, out int extensionAmount)
		{
			extensionAmount = -1;
			return false;
		}

		[Obsolete("Override the Extend method directly.")]
		protected virtual void RecordExtension(ServerRequestContext context, IExtendableLimit limit)
		{
		}

		protected virtual bool Register(ServerRequestContext context, LicenseValuesDictionary registrationInfo, bool autoRegister)
		{
			return true;
		}

		private bool PerformAutoRegisterInternal(ServerRequestContext context)
		{
			if (_autoRegister)
			{
				if (!Register(context, context.License.RegistrationInfo, true))
				{
					return false;
				}
				if (context.NewLicenseFile != null && context.ReferenceLimit != null)
				{
					string limitId = context.ReferenceLimit.LimitId;
					foreach (SecureLicense item in (IEnumerable)context.NewLicenseFile.Licenses)
					{
						context.ReferenceLimit = item.Limits[limitId];
						if (context.ReferenceLimit == null)
						{
							continue;
						}
						context.License = context.ReferenceLimit.License;
						break;
					}
				}
			}
			return true;
		}

		protected virtual void ProcessFailureReport(string report, string machineProfileHash, Version reportFormat, LicenseValuesDictionary properties)
		{
		}

		protected virtual bool ExecuteCustomCommand(ServerRequestContext context)
		{
			throw new LicenseManagementException("E_InvalidDispatchCommand", null, ErrorSeverity.High, context.Command);
		}

		public static ServerResult CallServer(string command, string address, Type licensedType, Limit referenceLimit, SecureLicense referenceLicense, LicenseValuesDictionary properties)
		{
			Check.NotNull(command, "command");
			Check.NotNull(address, "address");
			SecureLicenseContext.ResolveContext = SecureLicenseContext.CreateResolveContext(referenceLicense);
			Uri uri = DeployLX.Licensing.v5.SharedToolbox.ResolveUrl(address, SecureLicenseContext.ResolveContext);
			SecureLicenseContext.ResolveContext = null;
			//LicenseServerProxy licenseServerProxy = new LicenseServerProxy();
			if (properties == null)
			{
				properties = new LicenseValuesDictionary();
			}
			if (((StringDictionary)properties)["MachineProfile"] == null)
			{
				((StringDictionary)properties)["MachineProfile"] = MachineProfile.Profile.GetComparableHash(false, null);
			}
			else
			{
				((StringDictionary)properties)["MachineProfile.Absolute"] = MachineProfile.Profile.GetComparableHash(false, null);
			}
			if (licensedType != null)
			{
				((StringDictionary)properties)["LicensedType"] = licensedType.FullName;
				((StringDictionary)properties)["LicensedAssembly"] = licensedType.Assembly.FullName;
				((StringDictionary)properties)["LicensedAssemblyName"] = licensedType.Assembly.GetName().Name;
				((StringDictionary)properties)["LicensedAssemblyVersion"] = licensedType.Assembly.GetName().Version.ToString();
				AssemblyFileVersionAttribute assemblyFileVersionAttribute = Attribute.GetCustomAttribute(licensedType.Assembly, typeof(AssemblyFileVersionAttribute)) as AssemblyFileVersionAttribute;
				if (assemblyFileVersionAttribute != null)
				{
					((StringDictionary)properties)["LicensedAssemblyFileVersion"] = assemblyFileVersionAttribute.Version;
				}
			}
			((StringDictionary)properties)["IsServiceRequest"] = "false";
			((StringDictionary)properties)["IsWebRequest"] = "false";
			((StringDictionary)properties)["IsManagerRequest"] = "true";
			//licenseServerProxy.Url = uri.ToString();
			//licenseServerProxy.Proxy = Config.Proxy;
			//licenseServerProxy.AllowAutoRedirect = true;
			//licenseServerProxy.Timeout = Config.ServerConnectTimeout;
			string salt = Guid.NewGuid().ToString("N");
			bool flag = true;
			bool flag2 = false;
			Exception exception = null;
			LicenseFile licenseFile = null;
			SecureLicense secureLicense = null;
			SecureLicense secureLicense2 = (referenceLicense == null) ? ((referenceLimit == null) ? null : referenceLimit.License) : referenceLicense;
			if (referenceLicense != null && referenceLimit != null && referenceLicense.Limits.FindLimitById(referenceLimit.LimitId) != referenceLimit)
			{
				throw new SecureLicenseException("E_ReferenceLimitMustBeInReferenceLicense");
			}
			try
			{
				byte[] licenseXml = null;
				if (secureLicense2 != null)
				{
					using (MemoryStream memoryStream = new MemoryStream())
					{
						secureLicense2.LicenseFile.Save(memoryStream);
						licenseXml = memoryStream.ToArray();
					}
				}

				ResettableStringRequestByteReturnHttpRequest request = new ResettableStringRequestByteReturnHttpRequest(uri.ToString(), "utf-8")
                {
                    AllowAutoRedirect = true,
                    Timeout = Config.ServerConnectTimeout,
                    Method = "POST"
                };
                LicenseHttpRequest licenseRequest = new LicenseHttpRequest
                {
                    Command = command,
                    LicenseXml = licenseXml,
                    LicenseId = (secureLicense2 == null) ? null : secureLicense2.LicenseId,
                    LimitId = (referenceLimit == null) ? null : referenceLimit.LimitId,
                    Key = (secureLicense2 == null) ? null : secureLicense2.KeyName,
                    CurrentCulture = Thread.CurrentThread.CurrentUICulture.ToString(),
                    Properties = properties.ToXmlString(),
                    Salt = salt
                };
                request.WriteContentToRequest(JsonConvert.SerializeObject(licenseRequest));

				byte[] array = request.GetResponse().Value;


                //byte[] array = licenseServerProxy.Dispatch(command, licenseXml, (secureLicense2 == null) ? null : secureLicense2.LicenseId, (referenceLimit == null) ? null : referenceLimit.LimitId, (secureLicense2 == null) ? null : secureLicense2.KeyName, Thread.CurrentThread.CurrentUICulture.ToString(), properties.ToXmlString(), salt);
				if (array != null && array.Length != 0)
				{
					if (array[0] != 255)
					{
						using (MemoryStream stream = new MemoryStream(array))
						{
							licenseFile = new LicenseFile();
							licenseFile.Load(stream);
						}
						if (flag = (licenseFile.Licenses.Count > 0))
						{
							if (((StringDictionary)licenseFile.MetaValues)["__NEWLICENSE"] == "true")
							{
								flag2 = true;
								licenseFile.MetaValues.Remove("__NEWLICENSE");
							}
							else if (!(licenseFile.Id != secureLicense2.LicenseFile.Id) && licenseFile.Licenses[secureLicense2.LicenseId] != null)
							{
								secureLicense = licenseFile.Licenses[secureLicense2.LicenseId];
								bool flag3 = false;
								bool flag4 = false;
								string strA = "__MODIFIEDATSERVER." + referenceLimit.LimitId;
								foreach (DictionaryEntry metaValue in secureLicense.MetaValues)
								{
									string text = metaValue.Key as string;
									if (text != null && string.Compare(text, 0, "__MODIFIEDATSERVER", 0, 16, true) == 0)
									{
										flag3 = true;
										if (string.Compare(strA, text, true) == 0)
										{
											flag4 = true;
											continue;
										}
										flag2 = true;
										break;
									}
								}
								flag2 |= (flag3 && !flag4);
							}
							else
							{
								flag2 = true;
							}
							if (secureLicense != null)
							{
								secureLicense.MetaValues.RemoveWithPrefix("__MODIFIEDATSERVER");
							}
						}
					}
				}
				else
				{
					flag = false;
				}
				if (!flag)
				{
					exception = new SecureLicenseException("E_InvalidResponseFromServer");
				}
			}
			catch (HttpException ex)
			{
				exception = new SecureLicenseException(ValidationRecord.PreprocessException(ex));
				flag = false;
			}
			catch (ThreadAbortException)
			{
				flag = false;
			}
			catch (WebException ex3)
			{
				exception = new SecureLicenseException(ex3.Message, ex3, ErrorSeverity.Normal);
				flag = false;
			}
			catch (Exception ex4)
			{
				exception = new SecureLicenseException(ex4.Message, ex4, ErrorSeverity.High);
				flag = false;
			}
			//if (flag2 && MessageBoxEx.Show("M_ServerGeneratedNewLicense", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
			//{
			//	using (SaveFileDialog saveFileDialog = new SaveFileDialog())
			//	{
			//		saveFileDialog.FileName = licenseFile.SuggestedSaveLocation;
			//		saveFileDialog.Filter = "License Files (*.lic)|*.lic|All Files (*.*)|*.*";
			//		if (saveFileDialog.ShowDialog() == DialogResult.OK)
			//		{
			//			licenseFile.Save(saveFileDialog.FileName, false, LicenseSaveType.Normal);
			//		}
			//	}
			//}
			//if (secureLicense != null && ((StringDictionary)secureLicense.MetaValues)["__SUCCESSMESSAGE"] != null)
			//{
			//	string text2 = ((StringDictionary)secureLicense.MetaValues)["__SUCCESSMESSAGE"];
			//	secureLicense.MetaValues.Remove("__SUCCESSMESSAGE");
			//	try
			//	{
			//		MessageBoxEx.Show(text2, "#UI_ValidationNoticeTitle", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
			//	}
			//	catch (Exception)
			//	{
			//	}
			//}
			return new ServerResult(flag, exception, secureLicense, flag2);
		}
	}
}
