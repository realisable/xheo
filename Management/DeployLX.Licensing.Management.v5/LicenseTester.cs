using DeployLX.Licensing.v5;
using Microsoft.CSharp;
using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Reflection;
using System.Threading;
//using System.Windows.Forms;

namespace DeployLX.Licensing.Management.v5
{
	public class LicenseTester
	{
		private int _cacheId;

		private Assembly _testAssembly;

		private LicenseKey _testKey;

		private ManualResetEvent _finishedEvent = new ManualResetEvent(false);

		private Thread _testThread;

		private SecureLicense _result;

		private Exception _exception;

		private bool _hasTested;

		public LicenseFile File
		{
			get;
			set;
		}

		public SecureLicense Result
		{
			get
			{
				return _result;
			}
		}

		public Exception Exception
		{
			get
			{
				return _exception;
			}
		}

		public SupportInfo SupportInfo
		{
			get;
			set;
		}

		public bool IgnorPreviousState
		{
			get;
			set;
		}

		public DateTime TestDate
		{
			get;
			set;
		}

		public string TestSerialNumber
		{
			get;
			set;
		}

		public bool HasTested
		{
			get
			{
				return _hasTested;
			}
		}

		public bool IsTesting
		{
			get
			{
				return _testThread != null;
			}
		}

		public event EventHandler Finished;

		public LicenseTester()
		{
			SupportInfo = new SupportInfo
			{
				Company = "Acme",
				Email = "support@example.com",
				Website = "http://example.com/support",
				Phone = "(555) 123-4567"
			};
			TestDate = DateTime.MinValue;
		}

		protected virtual void OnFinished(EventArgs e)
		{
			if (this.Finished != null)
			{
				this.Finished(this, e);
			}
		}

		public void Test()
		{
			Test(false);
		}

		public void Test(bool async)
		{
			_result = null;
			_hasTested = false;
			if (!IsTesting && File != null)
			{
				LicenseKey licenseKey = ResolveKey();
				if (licenseKey != null)
				{
					Version version = ResolveAssemblyVersionFromLicenses();
					ResolveAssembly(licenseKey, version);
					if (_testAssembly != null)
					{
						if (async)
						{
							Thread thread = new Thread(TestThread);
							thread.SetApartmentState(ApartmentState.STA);
							thread.Name = "License Test Thread";
							thread.IsBackground = true;
							_testThread = thread;
							thread.Start();
						}
						else
						{
							RunTest();
							OnFinished(EventArgs.Empty);
						}
					}
				}
			}
		}

		public void Wait()
		{
			if (_testThread != null)
			{
				_testThread.Join();
			}
		}

		private Version ResolveAssemblyVersionFromLicenses()
		{
			Version version = new Version(0, 0);
			Version version2 = new Version(32767, 32767);
			foreach (SecureLicense item in (IEnumerable)File.Licenses)
			{
				Limit[] array = item.Limits.FindLimitsByType(typeof(VersionLimit), true);
				Limit[] array2 = array;
				foreach (Limit limit in array2)
				{
					VersionLimit versionLimit = limit as VersionLimit;
					if (versionLimit.Minimum > version)
					{
						version = versionLimit.Minimum;
					}
					if (versionLimit.Maximum < version2)
					{
						version2 = versionLimit.Maximum;
					}
				}
			}
			if (!(version > version2))
			{
				return version;
			}
			return version2;
		}

		private void TestThread()
		{
			try
			{
				//using (Form form = new Form())
				//{
				//	form.Text = "Testing license";
				//	form.FormBorderStyle = FormBorderStyle.FixedToolWindow;
				//	form.Width = 150;
				//	form.Height = 50;
				//	form.StartPosition = FormStartPosition.CenterScreen;
				//	Label label = new Label();
				//	label.Text = "Please wait...";
				//	label.Dock = DockStyle.Fill;
				//	label.TextAlign = ContentAlignment.MiddleCenter;
				//	form.Controls.Add(label);
				//	form.Activated += frm_Activated;
				//	Application.Run(form);
				//}
			}
			finally
			{
				_testThread = null;
				OnFinished(EventArgs.Empty);
			}
		}

		private void frm_Activated(object sender, EventArgs e)
		{
			//Form form = sender as Form;
			//RunTest();
			//form.Close();
		}

		private void RunTest()
		{
			if (!_hasTested)
			{
				_hasTested = true;
				try
				{
					Type type = _testAssembly.GetType("LicenseTester");
					LicenseValidationRequestInfo licenseValidationRequestInfo = new LicenseValidationRequestInfo();
					licenseValidationRequestInfo.SupportInfo = SupportInfo;
					licenseValidationRequestInfo.OnlyCheckManualLicenseFile = true;
					licenseValidationRequestInfo.DeveloperMode = true;
					licenseValidationRequestInfo.DontShowDeveloperModeWarning = true;
					licenseValidationRequestInfo.DisableCache = true;
					licenseValidationRequestInfo.LicenseFile = (File.Clone() as LicenseFile);
					if (IgnorPreviousState)
					{
						foreach (SecureLicense item in (IEnumerable)licenseValidationRequestInfo.LicenseFile.Licenses)
						{
							_testKey.DecryptLicense(item);
							if (!item.IsStillEncrypted)
							{
								SecureLicense secureLicense2 = item;
								secureLicense2.LicenseId = secureLicense2.LicenseId + ":" + Guid.NewGuid();
								_testKey.SignLicense(item);
							}
						}
					}
					if (TestDate > DateTime.UtcNow)
					{
						licenseValidationRequestInfo.TestDate = TestDate;
					}
					if (TestSerialNumber != null)
					{
						licenseValidationRequestInfo.SerialNumbers = new string[1]
						{
							TestSerialNumber
						};
					}
					object obj = null;
					obj = Activator.CreateInstance(type);
					_result = (type.InvokeMember("Test", BindingFlags.Instance | BindingFlags.Public | BindingFlags.InvokeMethod, null, obj, new object[1]
					{
						licenseValidationRequestInfo
					}) as SecureLicense);
				}
				catch (TargetInvocationException ex)
				{
					_exception = ex.InnerException;
				}
				catch (Exception exception)
				{
					Exception ex2 = _exception = exception;
				}
			}
		}

		private void ResolveAssembly(LicenseKey key, Version version)
		{
			int num = key.Name.GetHashCode() ^ version.GetHashCode();
			if (num != _cacheId && _testAssembly != null && _testKey.Name != key.Name)
			{
				_testKey.Dispose();
				_testAssembly = null;
				_testKey = null;
			}
			if (_testAssembly == null)
			{
				_testAssembly = CreateTestAssembly(key, version);
				_cacheId = num;
			}
		}

		private LicenseKey ResolveKey()
		{
			string text = null;
			foreach (SecureLicense item in (IEnumerable)File.Licenses)
			{
				if (text == null)
				{
					text = item.KeyName;
					continue;
				}
				if (!(text != item.KeyName))
				{
					continue;
				}
				throw new InvalidOperationException("Cannot test licenses with multiple keys.");
			}
			if (text != null && text.Length != 0)
			{
				return new LicenseKey(text);
			}
			return null;
		}

		private Assembly CreateTestAssembly(LicenseKey key, Version version)
		{
			_testKey = key;
			CodeCompileUnit codeCompileUnit = new CodeCompileUnit();
			CodeNamespace codeNamespace = new CodeNamespace("");
			codeCompileUnit.Namespaces.Add(codeNamespace);
			codeCompileUnit.ReferencedAssemblies.Add(typeof(SecureLicense).Assembly.Location);
			codeCompileUnit.ReferencedAssemblies.Add(typeof(LicenseProvider).Assembly.Location);
			codeCompileUnit.AssemblyCustomAttributes.Add(new CodeAttributeDeclaration(new CodeTypeReference(typeof(LicenseKeyAttribute)), new CodeAttributeArgument(new CodeSnippetExpression(key.MakeDeployLXKeyString(false)))));
			codeCompileUnit.AssemblyCustomAttributes.Add(new CodeAttributeDeclaration(new CodeTypeReference(typeof(LicenseHelpAttribute)), new CodeAttributeArgument("Product", new CodePrimitiveExpression("Test Assembly"))));
			codeCompileUnit.AssemblyCustomAttributes.Add(new CodeAttributeDeclaration(new CodeTypeReference(typeof(AssemblyVersionAttribute)), new CodeAttributeArgument(new CodePrimitiveExpression(version.ToString()))));
			CodeTypeDeclaration codeTypeDeclaration = new CodeTypeDeclaration("LicenseTester");
			codeNamespace.Types.Add(codeTypeDeclaration);
			CodeMemberMethod codeMemberMethod = new CodeMemberMethod();
			codeMemberMethod.Name = "Test";
			codeMemberMethod.Attributes = MemberAttributes.Public;
			codeMemberMethod.ReturnType = new CodeTypeReference(typeof(SecureLicense));
			codeTypeDeclaration.Members.Add(codeMemberMethod);
			codeMemberMethod.Parameters.Add(new CodeParameterDeclarationExpression(new CodeTypeReference(typeof(LicenseValidationRequestInfo)), "info"));
			codeMemberMethod.Statements.Add(new CodeMethodInvokeExpression(new CodeTypeReferenceExpression(typeof(SecureLicenseManager)), "ResetCacheForType", new CodeMethodInvokeExpression(null, "GetType")));
			codeMemberMethod.Statements.Add(new CodeMethodReturnStatement(new CodeMethodInvokeExpression(new CodeTypeReferenceExpression(typeof(SecureLicenseManager)), "Validate", new CodeThisReferenceExpression(), new CodePrimitiveExpression(null), new CodeVariableReferenceExpression("info"))));
			CSharpCodeProvider cSharpCodeProvider = new CSharpCodeProvider();
			CompilerParameters compilerParameters = new CompilerParameters();
			compilerParameters.GenerateInMemory = true;
			compilerParameters.IncludeDebugInformation = true;
			CompilerResults compilerResults = cSharpCodeProvider.CompileAssemblyFromDom(compilerParameters, codeCompileUnit);
			if (compilerResults.Errors.Count > 0)
			{
				foreach (CompilerError error in compilerResults.Errors)
				{
					CompilerError compilerError = error;
				}
				return null;
			}
			return compilerResults.CompiledAssembly;
		}
	}
}
