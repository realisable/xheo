using System;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;

namespace DeployLX.Licensing.Management.v5
{
	[SuppressUnmanagedCodeSecurity]
	[SecuritySafeCritical]
	internal sealed class SafeNativeMethods
	{
		public struct EXPLICIT_ACCESS
		{
			public int AccessPermissions;

			public int AccessMode;

			public int Inheritance;

			public TRUSTEE Trustee;
		}

		public struct TRUSTEE
		{
			public IntPtr MultipleTrustee;

			public int MultipleTrusteeOperation;

			public int TrusteeForm;

			public int TrusteeType;

			public string Name;
		}

		private SafeNativeMethods()
		{
		}

		[DllImport("kernel32", SetLastError = true)]
		public static extern IntPtr LoadLibraryExW([MarshalAs(UnmanagedType.LPWStr)] string filename, IntPtr hFile, uint flags);

		[DllImport("kernel32", SetLastError = true)]
		public static extern int FreeLibrary(IntPtr hModule);

		[DllImport("user32", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern int GetSystemMetrics(int nIndex);

		[DllImport("Kernel32.dll", SetLastError = true)]
		public static extern int CreateFile(string fileName, int desiredAccess, int shareMode, IntPtr securityAttributes, int creationDisposition, int flagsAndAttributes, IntPtr templateFile);

		[DllImport("Kernel32.dll")]
		public static extern bool CloseHandle(int handle);

		[DllImport("Kernel32.dll", SetLastError = true)]
		public static extern bool DeviceIoControl(int handle, int ioControlCode, IntPtr inBuffer, int inBufferSize, [Out] IntPtr outBuffer, int outBufferSize, ref int bytesReturned, IntPtr overlapped);

		[DllImport("Kernel32.dll", SetLastError = true)]
		public static extern int QueryDosDevice(string deviceName, StringBuilder targetPath, int max);

		[DllImport("iphlpapi")]
		public static extern int GetAdaptersInfo(IntPtr pAdapterInfo, ref uint pOutBufLen);

		[DllImport("kernel32")]
		public static extern void GlobalMemoryStatus(IntPtr memstat);

		[DllImport("kernel32", SetLastError = true)]
		public static extern int GlobalMemoryStatusEx(IntPtr memstat);

		public static int MakeHRForWin32Err(int win32)
		{
			return win32 | -2147024896;
		}

		[DllImport("advapi32", SetLastError = true)]
		public static extern int SetEntriesInAcl(int countOfExplicitEntries, [In] [Out] ref EXPLICIT_ACCESS explicitEntries, IntPtr oldAcl, out IntPtr newAcl);

		[DllImport("advapi32", SetLastError = true)]
		public static extern int SetNamedSecurityInfo(string name, int objectType, int securityInfo, IntPtr sidOwner, IntPtr sidGroup, IntPtr dacl, IntPtr sacl);

		[DllImport("advapi32", SetLastError = true)]
		public static extern int GetNamedSecurityInfo(string name, int objectType, int securityInfo, [Out] IntPtr sidOwner, [Out] IntPtr sidGroup, out IntPtr dacl, [Out] IntPtr sacl, out IntPtr securityDescriptor);
	}
}
