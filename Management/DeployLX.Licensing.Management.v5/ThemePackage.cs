using DeployLX.Licensing.v5;
using System;
using System.Collections;
using System.IO;
using System.Text;
using System.Xml;

namespace DeployLX.Licensing.Management.v5
{
	public class ThemePackage
	{
		public SuperFormTheme Theme
		{
			get;
			private set;
		}

		public LicenseResourceCollection Resources
		{
			get;
			private set;
		}

		public string Location
		{
			get;
			private set;
		}

		public ThemePackage()
		{
			Theme = new SuperFormTheme();
			Resources = new LicenseResourceCollection();
		}

		public static ThemePackage CreateFromLicenseFile(LicenseFile licenseFile)
		{
			if (licenseFile == null)
			{
				throw new ArgumentNullException("licenseFile");
			}
			ThemePackage themePackage = new ThemePackage();
			DeployLX.Licensing.v5.Toolbox.CopyXmlPersistable(licenseFile.SuperFormTheme, themePackage.Theme, LicenseSaveType.Signing);
			IDictionaryEnumerator enumerator = themePackage.Theme.Images.Dictionary.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					string uriString = ((DictionaryEntry)enumerator.Current).Value as string;
					Uri uri = new Uri(uriString);
					string text = uri.Scheme.ToLower();
					string a;
					if ((a = text) != null && a == "licres")
					{
						ImportLicenseResource(themePackage, uri, licenseFile);
					}
				}
				return themePackage;
			}
			finally
			{
				IDisposable disposable = enumerator as IDisposable;
				if (disposable != null)
				{
					disposable.Dispose();
				}
			}
		}

		public void ApplyToLicenseFile(LicenseFile licenseFile)
		{
			DeployLX.Licensing.v5.Toolbox.CopyXmlPersistable(Theme, licenseFile.SuperFormTheme, LicenseSaveType.Signing);
			foreach (LicenseResource item in (IEnumerable)Resources)
			{
				licenseFile.Resources.SetBinary(item.Name, item.Data);
			}
		}

		public void Save(Stream stream)
		{
			if (stream == null)
			{
				throw new ArgumentNullException("stream");
			}
			using (XmlTextWriter writer = new XmlTextWriter(stream, new UTF8Encoding(false)))
			{
				PrepWriter(writer);
				Save(writer);
			}
		}

		public void Save(XmlWriter writer)
		{
			if (writer == null)
			{
				throw new ArgumentNullException("writer");
			}
			writer.WriteStartElement("ThemePackage");
			Theme.WriteToXml(writer, LicenseSaveType.Signing);
			Resources.WriteToXml(writer, LicenseSaveType.Signing);
			writer.WriteEndElement();
			writer.Flush();
		}

		public void Save(string path)
		{
			if (path != null && path.Length != 0)
			{
				Location = Path.GetFullPath(path);
				using (XmlTextWriter writer = new XmlTextWriter(path, new UTF8Encoding(false)))
				{
					PrepWriter(writer);
					Save(writer);
				}
				return;
			}
			throw new ArgumentNullException("path");
		}

		public void Load(XmlReader reader)
		{
			if (reader == null)
			{
				throw new ArgumentNullException("reader");
			}
			reader.MoveToContent();
			if (!(reader.Name != "ThemePackage") && !reader.IsEmptyElement)
			{
				reader.Read();
				while (true)
				{
					if (!reader.EOF)
					{
						reader.MoveToContent();
						if (!reader.IsStartElement())
						{
							break;
						}
						switch (reader.Name)
						{
						case "Resources":
							Resources = new LicenseResourceCollection();
							Resources.ReadFromXml(reader);
							break;
						case "Theme":
							Theme = new SuperFormTheme();
							Theme.ReadFromXml(reader);
							break;
						default:
							reader.Skip();
							break;
						}
						continue;
					}
					return;
				}
				reader.Read();
			}
			else
			{
				reader.Skip();
			}
		}

		public void Load(Stream stream)
		{
			if (stream == null)
			{
				throw new ArgumentNullException("stream");
			}
			using (XmlTextReader reader = new XmlTextReader(stream))
			{
				Load(reader);
			}
		}

		public void Load(string path)
		{
			if (path != null && path.Length != 0)
			{
				Location = Path.GetFullPath(path);
				using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
				{
					Load(stream);
				}
				return;
			}
			throw new ArgumentNullException("path");
		}

		private static void ImportLicenseResource(ThemePackage package, Uri uri, LicenseFile licenseFile)
		{
			if (!(uri.Host != "licensefile"))
			{
				string name = uri.AbsolutePath.Substring(1);
				byte[] binary = licenseFile.Resources.GetBinary(name);
				if (binary != null)
				{
					package.Resources.SetBinary(name, binary);
				}
			}
		}

		private static void PrepWriter(XmlTextWriter writer)
		{
			writer.IndentChar = '\t';
			writer.Indentation = 1;
			writer.Formatting = Formatting.Indented;
			writer.WriteStartDocument(true);
		}
	}
}
