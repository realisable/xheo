using DeployLX.Licensing.v5;
using System;
using System.Collections;
using System.Collections.Specialized;

namespace DeployLX.Licensing.Management.v5
{
	public class ServerRequestContext : IServerRequestContext
	{
		private Hashtable _state = new Hashtable();

		private string _command;

		private SecureLicense _license;

		private LicenseFile _newLicenseFile;

		private Limit _referenceLimit;

		private LicenseValuesDictionary _properties;

		private LicenseKey _licenseKey;

		private Version _licensedAssemblyVersion;

		private bool _developerMode;

		private DateTime _testDateSet = DateTime.MinValue;

		public Hashtable State
		{
			get
			{
				return _state;
			}
		}

		public string Command
		{
			get
			{
				return _command;
			}
		}

		public SecureLicense License
		{
			get
			{
				return _license;
			}
			set
			{
				_license = value;
			}
		}

		public bool IsModified
		{
			get
			{
				if (_newLicenseFile == null)
				{
					if (_license != null)
					{
						return ((StringDictionary)_license.MetaValues)["__MODIFIED__"] == "true";
					}
					return false;
				}
				return true;
			}
		}

		public string SerialNumber
		{
			get
			{
				if (_license.SerialNumber != null)
				{
					return _license.SerialNumber;
				}
				return _license.LicenseId;
			}
		}

		public LicenseFile NewLicenseFile
		{
			get
			{
				return _newLicenseFile;
			}
			set
			{
				_newLicenseFile = value;
			}
		}

		public Limit ReferenceLimit
		{
			get
			{
				return _referenceLimit;
			}
			set
			{
				_referenceLimit = value;
			}
		}

		public string MachineProfileHash
		{
			get
			{
				return ((StringDictionary)_properties)["MachineProfile"];
			}
			set
			{
				((StringDictionary)_properties)["MachineProfile"] = value;
			}
		}

		public string AbsoluteMachineProfileHash
		{
			get
			{
				string text = ((StringDictionary)_properties)["MachineProfile.Absolute"];
				if (text == null)
				{
					return MachineProfileHash;
				}
				return text;
			}
		}

		public string MachineName
		{
			get
			{
				return ((StringDictionary)_properties)["MachineName"];
			}
		}

		public LicenseValuesDictionary Properties
		{
			get
			{
				return _properties;
			}
		}

		public LicenseKey LicenseKey
		{
			get
			{
				return _licenseKey;
			}
		}

		public bool IsClientServiceRequest
		{
			get
			{
				return ((StringDictionary)_properties)["IsServiceRequest"] == "True";
			}
		}

		public bool IsClientWebRequest
		{
			get
			{
				return ((StringDictionary)_properties)["IsWebRequest"] == "True";
			}
		}

		public string LicensedType
		{
			get
			{
				return ((StringDictionary)_properties)["LicensedType"];
			}
		}

		public string LicensedAssembly
		{
			get
			{
				return ((StringDictionary)_properties)["LicensedAssembly"];
			}
		}

		public string LicensedAssemblyName
		{
			get
			{
				return ((StringDictionary)_properties)["LicensedAssemblyName"];
			}
		}

		public Version LicensedAssemblyVersion
		{
			get
			{
				if (_licensedAssemblyVersion == (Version)null)
				{
					_licensedAssemblyVersion = new Version(((StringDictionary)_properties)["LicensedAssemblyVersion"]);
				}
				return _licensedAssemblyVersion;
			}
		}

		public string LicensedAssemblyFileVersion
		{
			get
			{
				return ((StringDictionary)_properties)["LicensedAssemblyFileVersion"];
			}
		}

		public bool DeveloperMode
		{
			get
			{
				return _developerMode;
			}
		}

		public DateTime CurrentDateAndTime
		{
			get
			{
				if (_testDateSet == DateTime.MinValue)
				{
					return DateTime.UtcNow;
				}
				return DateTime.UtcNow + (_testDateSet - DateTime.UtcNow);
			}
		}

		public Exception Exception
		{
			get;
			set;
		}

		internal ServerRequestContext(string command, SecureLicense license, Limit referenceLimit, LicenseValuesDictionary properties, LicenseKey licenseKey, bool developerMode)
		{
			_command = command;
			_license = license;
			_referenceLimit = referenceLimit;
			_properties = properties;
			_licenseKey = licenseKey;
			_developerMode = developerMode;
			string text = ((StringDictionary)properties)["LicenseTestDate"];
			if (text != null)
			{
				DateTime dateTime = DateTime.Parse(text);
				dateTime = dateTime.ToUniversalTime();
				TimeSpan t = dateTime - DateTime.UtcNow;
				if (t < TimeSpan.FromMinutes(-20.0))
				{
					throw new LicenseManagementException("E_TestDateEarlierThanSystem");
				}
				_testDateSet = dateTime;
			}
		}

		public int GetExtendableValueFromClient(IExtendableLimit limit)
		{
			if (limit == null)
			{
				return 0;
			}
			Limit limit2 = limit as Limit;
			string text = ((StringDictionary)_properties)[limit2.LimitId + ".ExtendableValue"];
			if (text == null)
			{
				return 0;
			}
			return int.Parse(text);
		}

		public int GetCurrentExtendableValueFromClient(IExtendableLimit limit)
		{
			if (limit == null)
			{
				return 0;
			}
			Limit limit2 = limit as Limit;
			string text = ((StringDictionary)_properties)[limit2.LimitId + ".CurrentValue"];
			if (text == null)
			{
				return 0;
			}
			return int.Parse(text);
		}
	}
}
