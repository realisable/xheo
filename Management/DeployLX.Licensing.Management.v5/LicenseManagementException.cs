using DeployLX.Licensing.v5;
using System;
using System.Runtime.Serialization;
using System.Security;
using System.Security.Permissions;

namespace DeployLX.Licensing.Management.v5
{
	[Serializable]
	public class LicenseManagementException : SecureLicenseException
	{
		public LicenseManagementException()
		{
		}

		public LicenseManagementException(string errorId, Exception innerException, ErrorSeverity severity, params object[] args)
			: base(errorId, innerException, severity, args)
		{
		}

		public LicenseManagementException(string errorId, Exception innerException, params object[] args)
			: this(errorId, innerException, ErrorSeverity.Normal, args)
		{
		}

		public LicenseManagementException(string errorId, Exception innerException)
			: this(errorId, innerException, ErrorSeverity.Normal)
		{
		}

		public LicenseManagementException(string errorId, params object[] args)
			: this(errorId, null, ErrorSeverity.Normal, args)
		{
		}

		public LicenseManagementException(string errorId)
			: this(errorId, null, ErrorSeverity.Normal)
		{
		}

		protected LicenseManagementException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}

		[SecurityCritical]
		[SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
		}
	}
}
