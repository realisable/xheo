namespace DeployLX.Licensing.Management.v5
{
	public interface ILicenseCodeSupport
	{
		string CharacterSet
		{
			get;
		}

		byte[] MakeCode(byte[] data);

		byte[] ParseCode(byte[] code);
	}
}
