using DeployLX.Licensing.v5;
using Microsoft.Win32;
using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Security;
using System.Threading;
using System.Web;

namespace DeployLX.Licensing.Management.v5
{
	internal sealed class Toolbox
	{
		private class BrowserHelper
		{
			private string _url;

			private bool _newWindow;

			public BrowserHelper(string url, bool newWindow)
			{
				_url = url;
				_newWindow = newWindow;
			}

			public void Show()
			{
				Thread thread = new Thread(ShowThread);
				thread.SetApartmentState(ApartmentState.STA);
				thread.IsBackground = true;
				thread.Name = "Browser Helper - " + _url;
				thread.Start();
			}

			private void ShowThread()
			{
				try
				{
					if (_newWindow)
					{
						using (RegistryKey registryKey = Registry.ClassesRoot.OpenSubKey("HTTP\\shell\\open\\command", false))
						{
							if (registryKey != null)
							{
								string text = registryKey.GetValue(null) as string;
								if (text != null)
								{
									if (text.IndexOf("%1") > -1)
									{
										text.Replace("%1", _url);
									}
									else
									{
										text = text + " " + _url;
									}
									string arguments = null;
									if (text.StartsWith("\""))
									{
										int num = text.IndexOf('"', 1);
										if (num > -1)
										{
											arguments = text.Substring(num + 1).Trim();
											text = text.Substring(1, num - 1);
										}
									}
									else
									{
										int num2 = text.IndexOf(' ');
										if (num2 > -1)
										{
											arguments = text.Substring(num2 + 1);
											text = text.Substring(0, num2);
										}
									}
									Process.Start(text, arguments);
									return;
								}
							}
						}
					}
					Process.Start(_url);
				}
				catch (Exception)
				{
					Process.Start(_url);
				}
			}
		}

		internal const string SafeBigRadix = "U9VWT2FG3Q7RS0AC1DEYMNX6P8HJ4KL5";

		internal const string DefaultRadix = "012345689ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!@#$%^*()_+-=[]{}|;:,.?/`~";

		internal const string MessyDefaultRadix = "7$,lsj*0mkyL._JV3b{Y;^HO~nSK8W@?[w`9F%RP(!qiC52DA&/4v:p)ZcU-6T|Med§GN=g'hoE}+]zQBft¶ax#rI1X";

		private static int _isTerminalServices;

		[ThreadStatic]
		private static int _threadIsWeb;

		private static int _iisIsAvailable;

		public static bool IsTerminalServices
		{
			get
			{
				if (_isTerminalServices == -1)
				{
					lock (typeof(Configuration))
					{
						if (_isTerminalServices == -1)
						{
							if ((Environment.OSVersion.Platform & PlatformID.WinCE) != 0)
							{
								_isTerminalServices = ((SafeNativeMethods.GetSystemMetrics(4096) != 0) ? 1 : 0);
							}
							else
							{
								_isTerminalServices = 0;
							}
						}
					}
				}
				return _isTerminalServices == 1;
			}
		}

		public static bool ShouldUseWebLogic
		{
			get
			{
				if (_threadIsWeb == 0)
				{
					_threadIsWeb = ((HttpContext.Current != null) ? 1 : 2);
				}
				return _threadIsWeb == 1;
			}
		}

		public static bool IsWebRequest
		{
			get
			{
				if (!IisIsAvailable)
				{
					return false;
				}
				if (ShouldUseWebLogic)
				{
					return true;
				}
				if (HttpContext.Current != null)
				{
					_threadIsWeb = 1;
					return true;
				}
				return false;
			}
		}

		public static bool IisIsAvailable
		{
			get
			{
				if (_iisIsAvailable == -1)
				{
					InitLib();
				}
				return _iisIsAvailable == 1;
			}
		}

		private Toolbox()
		{
		}

		static Toolbox()
		{
			_isTerminalServices = -1;
			_iisIsAvailable = -1;
			InitLib();
		}

		[SuppressUnmanagedCodeSecurity]
		internal static void InitLib()
		{
			lock (typeof(Configuration))
			{
				_iisIsAvailable = 0;
				try
				{
					try
					{
						if (HttpContext.Current != null)
						{
							_iisIsAvailable = 1;
							return;
						}
					}
					catch
					{
					}
					string text = RuntimeEnvironment.GetRuntimeDirectory() + "aspnet_isapi.dll";
					if (File.Exists(text))
					{
						IntPtr intPtr = SafeNativeMethods.LoadLibraryExW(text, IntPtr.Zero, 2u);
						if (intPtr == IntPtr.Zero)
						{
							intPtr = SafeNativeMethods.LoadLibraryExW(text, IntPtr.Zero, 2u);
						}
						if (intPtr != IntPtr.Zero && SafeNativeMethods.FreeLibrary(intPtr) != 0)
						{
							_iisIsAvailable = 1;
							SecureLicenseContext.WriteDiagnosticToContext("SUCCESS: IIS is available.");
						}
					}
				}
				catch
				{
				}
			}
		}

		public static void SplitUsername(string winUsername, out string username, out string domain)
		{
			Check.NotNull(winUsername, "winUsername");
			username = null;
			domain = null;
			int num = winUsername.IndexOf('\\');
			if (num > -1)
			{
				domain = winUsername.Substring(0, num);
				username = winUsername.Substring(num + 1);
			}
			else
			{
				username = winUsername;
			}
		}

		[ComVisible(false)]
		public static string GetFullPath(string path)
		{
			return Path.Combine(Path.GetDirectoryName(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile), path);
		}

		public static void OpenUrl(string url, bool newWindow)
		{
			BrowserHelper browserHelper = new BrowserHelper(url, newWindow);
			browserHelper.Show();
		}

		public static CultureInfo SelectCulture(CultureInfo culture)
		{
			CultureInfo currentUICulture = Thread.CurrentThread.CurrentUICulture;
			if (culture.IsNeutralCulture)
			{
				CultureInfo[] cultures = CultureInfo.GetCultures(CultureTypes.SpecificCultures);
				int num = 0;
				while (num < cultures.Length)
				{
					CultureInfo cultureInfo = cultures[num];
					if (!cultureInfo.Name.StartsWith(culture.Name))
					{
						num++;
						continue;
					}
					culture = cultureInfo;
					break;
				}
				if (culture.IsNeutralCulture)
				{
					return currentUICulture;
				}
			}
			Thread.CurrentThread.CurrentUICulture = culture;
			return currentUICulture;
		}

		public static CultureInfo SelectWebCulture()
		{
			CultureInfo currentUICulture = Thread.CurrentThread.CurrentUICulture;
			if (!IsWebRequest)
			{
				return currentUICulture;
			}
			try
			{
				HttpContext current = HttpContext.Current;
				string[] userLanguages = current.Request.UserLanguages;
				if (userLanguages != null && userLanguages.Length > 0)
				{
					string[] array = userLanguages;
					int num = 0;
					while (true)
					{
						if (num >= array.Length)
						{
							break;
						}
						string text = array[num];
						try
						{
							int num2 = text.IndexOf(';');
							CultureInfo culture = new CultureInfo((num2 == -1) ? text : text.Substring(0, num2));
							culture = SelectCulture(culture);
							if (CultureInfo.CurrentUICulture != currentUICulture)
							{
								return culture;
							}
						}
						catch (ArgumentException)
						{
						}
						num++;
					}
				}
				return currentUICulture;
			}
			catch
			{
				return currentUICulture;
			}
		}

		internal static Stream GetEmbeddedResource(Type type, string name, bool ignoreCase, out string foundResourceName)
		{
			string text = (type == null) ? "" : type.Namespace;
			foundResourceName = null;
			if (text == null)
			{
				text = "";
			}
			while (true)
			{
				string text2 = (text.Length > 0) ? (text + '.' + name) : name;
				SecureLicenseContext.WriteDiagnosticToContext("Searching for: " + text2);
				Stream manifestResourceStream = type.Assembly.GetManifestResourceStream(text2);
				if (manifestResourceStream == null)
				{
					if (text.Length == 0)
					{
						break;
					}
					int num = text.IndexOf('.');
					text = ((num <= -1) ? "" : text.Substring(0, num));
					continue;
				}
				foundResourceName = text2;
				return manifestResourceStream;
			}
			Stream stream = null;
			Stream stream2 = null;
			string text3 = Path.GetExtension(name);
			if (ignoreCase)
			{
				text3 = text3.ToLower(CultureInfo.InvariantCulture);
			}
			try
			{
				string value = ignoreCase ? name.ToLower(CultureInfo.InvariantCulture) : name;
				string[] manifestResourceNames = type.Assembly.GetManifestResourceNames();
				int num2 = 0;
				while (num2 < manifestResourceNames.Length)
				{
					string text4 = manifestResourceNames[num2];
					string text5 = ignoreCase ? text4.ToLower(CultureInfo.InvariantCulture) : text4;
					if (!text5.EndsWith(value))
					{
						if (text5.EndsWith(text3))
						{
							stream = type.Assembly.GetManifestResourceStream(text4);
							stream2 = stream;
							foundResourceName = text4;
						}
						num2++;
						continue;
					}
					stream2 = type.Assembly.GetManifestResourceStream(text4);
					foundResourceName = text4;
					return stream2;
				}
				return stream;
			}
			finally
			{
				if (stream != null && stream != stream2)
				{
					stream.Close();
				}
			}
		}

		internal static Stream GetEmbeddedResource(Type type, string name, out string foundResourceName)
		{
			return GetEmbeddedResource(type, name, false, out foundResourceName);
		}

		public static RegistryKey GetRegistryKey(string key, bool writeAccess)
		{
			Check.NotNull(key, "code");
			int num = key.StartsWith("registry:") ? 9 : 0;
			if (num == 0 && key.Length < 5)
			{
				goto IL_0039;
			}
			if (num == 9 && key.Length < 14)
			{
				goto IL_0039;
			}
			RegistryKey registryKey = null;
			bool flag = false;
			if (string.Compare(key, num, "HKLM", 0, 4, true, CultureInfo.InvariantCulture) == 0)
			{
				registryKey = Registry.LocalMachine;
			}
			else if (string.Compare(key, num, "HKCU", 0, 4, true, CultureInfo.InvariantCulture) == 0)
			{
				registryKey = Registry.CurrentUser;
			}
			else if (string.Compare(key, num, "HKCR", 0, 4, true, CultureInfo.InvariantCulture) == 0)
			{
				registryKey = Registry.ClassesRoot;
			}
			else if (string.Compare(key, num, "HK*U", 0, 4, true, CultureInfo.InvariantCulture) == 0)
			{
				registryKey = Registry.CurrentUser;
				flag = true;
			}
			else if (string.Compare(key, num, "HK*M", 0, 4, true, CultureInfo.InvariantCulture) == 0)
			{
				registryKey = Registry.LocalMachine;
				flag = true;
			}
			if (registryKey == null)
			{
				throw new ArgumentException(SR.GetString("E_InvalidRegistryKey", key), "code");
			}
			string name = key.Substring(num + 5);
			RegistryKey registryKey2 = registryKey.OpenSubKey(name, writeAccess);
			if (flag && registryKey2 == null)
			{
				registryKey2 = ((registryKey != Registry.CurrentUser) ? Registry.CurrentUser.OpenSubKey(name, writeAccess) : Registry.LocalMachine.OpenSubKey(name, writeAccess));
			}
			return registryKey2;
			IL_0039:
			throw new ArgumentException(SR.GetString("E_InvalidRegistryKey", key), "code");
		}

		public static void GrantEveryoneAccess(string path)
		{
			if (path != null && path.Length != 0 && File.Exists(path))
			{
				IntPtr zero = IntPtr.Zero;
				IntPtr zero2 = IntPtr.Zero;
				IntPtr zero3 = IntPtr.Zero;
				try
				{
					int namedSecurityInfo;
					if ((namedSecurityInfo = SafeNativeMethods.GetNamedSecurityInfo(path, 1, 4, IntPtr.Zero, IntPtr.Zero, out zero, IntPtr.Zero, out zero2)) == 0)
					{
						SafeNativeMethods.EXPLICIT_ACCESS eXPLICIT_ACCESS = default(SafeNativeMethods.EXPLICIT_ACCESS);
						eXPLICIT_ACCESS.AccessPermissions = 2097151;
						eXPLICIT_ACCESS.AccessMode = 1;
						eXPLICIT_ACCESS.Inheritance = 1;
						eXPLICIT_ACCESS.Trustee.Name = "EVERYONE";
						eXPLICIT_ACCESS.Trustee.MultipleTrustee = IntPtr.Zero;
						eXPLICIT_ACCESS.Trustee.TrusteeForm = 1;
						eXPLICIT_ACCESS.Trustee.TrusteeType = 5;
						if ((namedSecurityInfo = SafeNativeMethods.SetEntriesInAcl(1, ref eXPLICIT_ACCESS, zero, out zero3)) == 0)
						{
							if ((namedSecurityInfo = SafeNativeMethods.SetNamedSecurityInfo(path, 1, 4, IntPtr.Zero, IntPtr.Zero, zero3, IntPtr.Zero)) != 0)
							{
								Marshal.ThrowExceptionForHR(SafeNativeMethods.MakeHRForWin32Err(namedSecurityInfo));
							}
						}
						else
						{
							Marshal.ThrowExceptionForHR(SafeNativeMethods.MakeHRForWin32Err(namedSecurityInfo));
						}
					}
					else
					{
						Marshal.ThrowExceptionForHR(SafeNativeMethods.MakeHRForWin32Err(namedSecurityInfo));
					}
				}
				finally
				{
					if (zero2 != IntPtr.Zero)
					{
						Marshal.FreeHGlobal(zero2);
					}
					if (zero3 != IntPtr.Zero)
					{
						Marshal.FreeHGlobal(zero3);
					}
				}
			}
		}

		internal static byte[] Scramble(byte[] b, int index, int count)
		{
			byte[] array = new byte[b.Length];
			Buffer.BlockCopy(b, 0, array, 0, b.Length);
			for (int i = index; i < index + count && i + 4 <= array.Length; i++)
			{
				Array.Reverse(array, i, 4);
			}
			return array;
		}

		internal static byte[] Scramble(byte[] b)
		{
			return Scramble(b, 0, b.Length);
		}

		public static DateTime GetToday(bool beginning)
		{
			DateTime date = DateTime.UtcNow.Date;
			if (!beginning)
			{
				return date.AddTicks(863999990000L);
			}
			return date;
		}
	}
}
