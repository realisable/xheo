using System.ComponentModel;
using System.Configuration;
using System.Xml;

namespace DeployLX.Licensing.Management.v5
{
	public class LicensingSectionHandler : IConfigurationSectionHandler
	{
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public object Create(object parent, object configContext, XmlNode section)
		{
			LicenseConfigurationSettings licenseConfigurationSettings = (parent is LicenseConfigurationSettings) ? (parent as LicenseConfigurationSettings) : new LicenseConfigurationSettings();
			if (section == null)
			{
				return licenseConfigurationSettings;
			}
			foreach (XmlAttribute attribute in section.Attributes)
			{
				switch (attribute.LocalName)
				{
				case "developerMode":
					licenseConfigurationSettings.DeveloperMode = attribute.Value.Trim();
					break;
				case "keys":
					licenseConfigurationSettings.KeyFolder = attribute.Value.Trim();
					break;
				default:
					throw new ConfigurationErrorsException(SR.GetString("E_UnknownConfigurationAttribute", attribute.LocalName, "licenseManagement"), section);
				}
			}
			return licenseConfigurationSettings;
		}
	}
}
