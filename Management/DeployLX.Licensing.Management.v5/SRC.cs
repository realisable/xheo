namespace DeployLX.Licensing.Management.v5
{
	public static class SRC
	{
		public const string E_InvalidKeySize = "E_InvalidKeySize";

		public const string E_InvalidKeyStream = "E_InvalidKeyStream";

		public const string E_KeyMustBe384ForSignature = "E_KeyMustBe384ForSignature";

		public const string E_KeyMustBe1024ForTrial = "E_KeyMustBe1024ForTrial";

		public const string E_CharacterSetTooShort = "E_CharacterSetTooShort";

		public const string E_UnknownAlgorithm = "E_UnknownAlgorithm";

		public const string E_CannotDetermineAlgorithm = "E_CannotDetermineAlgorithm";

		public const string E_CharacterSetContainsDash = "E_CharacterSetContainsDash";

		public const string E_InvalidConfigurationValue = "E_InvalidConfigurationValue";

		public const string E_UnknownConfigurationAttribute = "E_UnknownConfigurationAttribute";

		public const string E_MissingConfigurationAttribute = "E_MissingConfigurationAttribute";

		public const string E_InvalidConfigurationTag = "E_InvalidConfigurationTag";

		public const string E_ConfigurationCannotUseAnd = "E_ConfigurationCannotUseAnd";

		public const string E_ConfigurationUnexpected = "E_ConfigurationUnexpected";

		public const string E_CannotSignEncrypted = "E_CannotSignEncrypted";

		public const string E_InvalidUrl = "E_InvalidUrl";

		public const string E_InvalidRegistryKey = "E_InvalidRegistryKey";

		public const string E_MaxSeed = "E_MaxSeed";

		public const string E_DuplicateLimitIds = "E_DuplicateLimitIds";

		public const string E_FirstIndexMustBeUsedBeforeSecond = "E_FirstIndexMustBeUsedBeforeSecond";

		public const string E_MaxEncodedIndex = "E_MaxEncodedIndex";

		public const string E_MaxEncodedValue = "E_MaxEncodedValue";

		public const string E_InvalidSerialNumberSeed = "E_InvalidSerialNumberSeed";

		public const string E_InvalidDispatchCommand = "E_InvalidDispatchCommand";

		public const string E_InvalidLicenseId = "E_InvalidLicenseId";

		public const string E_CouldNotPerformValidationRequest = "E_CouldNotPerformValidationRequest";

		public const string E_CannotFindAvailableProfile = "E_CannotFindAvailableProfile";

		public const string E_CannotFindAvailableProfileDeactivateAllowed = "E_CannotFindAvailableProfileDeactivateAllowed";

		public const string E_MaximumDataExceeded = "E_MaximumDataExceeded";

		public const string E_MustCreateNewLicenseFile = "E_MustCreateNewLicenseFile";

		public const string E_CannotModifyOtherLicense = "E_CannotModifyOtherLicense";

		public const string E_MustImplement = "E_MustImplement";

		public const string E_LicenseNotAuthorizedForMonitor = "E_LicenseNotAuthorizedForMonitor";

		public const string E_NoSponsorConfigured = "E_NoSponsorConfigured";

		public const string E_CouldNotCreateSponsor = "E_CouldNotCreateSponsor";

		public const string E_CannotResignWithTrial = "E_CannotResignWithTrial";

		public const string E_TestDateEarlierThanSystem = "E_TestDateEarlierThanSystem";

		public const string E_NotLicensedEdition = "E_NotLicensedEdition";

		public const string E_FileNotFound = "E_FileNotFound";

		public const string E_KeysNotFound = "E_KeysNotFound";

		public const string M_ServerGeneratedNewLicense = "M_ServerGeneratedNewLicense";

		public const string E_MissingProperty = "E_MissingProperty";

		public const string E_InvalidLicensePackage = "E_InvalidLicensePackage";
	}
}
