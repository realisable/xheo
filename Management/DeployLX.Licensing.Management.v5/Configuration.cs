using System;
using System.Configuration;
using System.IO;
using System.Web;

namespace DeployLX.Licensing.Management.v5
{
	public abstract class Configuration
	{
		internal const string CONFIG_SECTION = "licenseManagement";

		internal const string REGISTRY_KEY = "Software\\XHEO\\DeployLX\\v5.0\\Management";

		private static LicenseConfigurationSettings _settings;

		private static string _keyFolder;

		public static string KeyFolder
		{
			get
			{
				if (_keyFolder == null)
				{
					lock (typeof(Configuration))
					{
						if (_keyFolder == null)
						{
							string text = Settings.KeyFolder;
							if (text == null || text.Length == 0)
							{
								text = ((!Toolbox.IsWebRequest) ? "XHEO INC\\DeployLX\\5.0\\Keys" : "~/Keys");
							}
							text = ((!Toolbox.IsWebRequest || Path.IsPathRooted(text)) ? Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), text) : HttpContext.Current.Request.MapPath(text));
							try
							{
								text = Path.GetFullPath(text);
							}
							catch
							{
							}
							_keyFolder = text;
						}
					}
				}
				return _keyFolder;
			}
			set
			{
				_keyFolder = value;
			}
		}

		public static string DeveloperMode
		{
			get
			{
				return Settings.DeveloperMode;
			}
		}

		private static LicenseConfigurationSettings Settings
		{
			get
			{
				if (_settings == null)
				{
					lock (typeof(Configuration))
					{
						_settings = ((ConfigurationManager.GetSection("licenseManagement") as LicenseConfigurationSettings) ?? new LicenseConfigurationSettings());
					}
				}
				return _settings;
			}
		}

		private Configuration()
		{
		}
	}
}
